/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function abrirVisor(id, ext) {
    $('div.modal2').omniWindow().trigger('show');

    var win = $(window.parent);
    var top = win.scrollTop();
    var str = '';
    
    $('div.modal2').css('top', top + 10);
    $('#visor_cont').empty();
    $('#visor_cont').css("height","90%");
    $('#visor_nav').css("height","10%");

    str = ext.toLowerCase();
    switch (str.trim()) {
        case "jpg":
        case "png":
        case "gif":
            $('#visor_cont').append('<img id="visor_img_cont" src="/InformePreventivoIP/faces/modarchivos/DGira_Preview_Archivo?id=' + id + '" />');
                                                                           
            $('#visor_nav').show();
            break;
        case "dwg":
        case "pdf":
            $('#visor_cont').append('<iframe scrolling="auto" height="99.5%" width="100%" src="/InformePreventivoIP/faces/resources/js/Viewer/index.html#/InformePreventivoIP/faces/modarchivos/DGira_Preview_Archivo?id=' + id + '&ext=.pdf" frameborder="0" />');
            //$('#visor_cont').append('<iframe scrolling="auto" height="99.5%" width="100%" src="/IP/resources/js/Viewer/index.html#/IP/modarchivos/DGira_Preview_Archivo?id=' + id + '&ext=.pdf" frameborder="0" />');
            $('#visor_nav').hide();
            $('#visor_cont').css("height","100%");
            $('#visor_nav').css("height","0px");
            break;
    }

}

var degrees = 0;
var scale = 1;

function zoomVisor(opcion) {
    switch (opcion) {
        case 1:
            scale += .2;
            break;
        case 2:
            scale += -.2;
            break;
    }

    if (scale < .5) {
        scale = .5;
    }
    if (scale > 2) {
        scale = 2;
    }

    $('#visor_img_cont').css({'-webkit-transform': 'scale(' + scale + ',' + scale + ')',
        '-moz-transform': 'scale(' + scale + ',' + scale + ')',
        '-ms-transform': 'scale(' + scale + ',' + scale + ')',
        'transform': 'scale(' + scale + ',' + scale + ')'});

}

function rotarVisor(opcion) {
    switch (opcion) {
        case 1:
            degrees += 90;
            break;
        case 2:
            degrees -= 90;
            break;
    }

    $('#visor_img_cont').css({'-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        'transform': 'rotate(' + degrees + 'deg)',
        'top': '-200px'});

}

function cerrarVisor() {
    $('div.modal2').trigger('hide');
}


