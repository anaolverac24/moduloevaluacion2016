function contar(form,name,result) 
{// alert("form: " +form.name+"   name: "+name+"  result: "+result);
  var n,t;
  n = document.forms[form][name].value.length; // alert(n);
  t = 5000;
  if (n > t) {
	alert('A revasado el limite de caracteres permitidos(5,000 caracteres)');
    document.forms[form][name].value = document.forms[form][name].value.substring(0, t);
	
  }
  else {
    document.forms[form][result].value = t-n;
  }
}

function addRow(tableID,seccion) 
{

	   var table = document.getElementById(tableID);
	   var rowCount = table.rows.length;
	   var row = table.insertRow(rowCount);
	   var cell1 = "";
	   var element1 = "";
	   var elementAux = "";
	   var suma=0;
	   
	   
	   cell1 = row.insertCell(0);
	   element1 = document.createElement("img");
	   element1.src = "../../resources/images/gobmx/icons/panel-collapsed.png";	   
	   element1.id = rowCount;
	   element1.alt= "borrar";
           element1.title="borrar title";		   
	   //element1.className = "miCheck";  	   
	   crearEvento(element1, "click", function()
	   {	//alert("entro: "); 
			//alert('rown count: '+ rowCount);	   
			if (confirm("Esta usted seguro de borrar el registro?"))
			{   //borra fila en cuestion			
                            table.deleteRow(rowCount);
                            rowCount--;
                            if(seccion === 'superfProy')
                            {
                                var table2 = document.getElementById(tableID);
                                var rowCount2 = table.rows.length; 
                                var sum = 0;
                                var sum2 = 0;
                                for(var i=0; i<rowCount2; i++) 
                                {                                       
                                    var row2 = table2.rows[i];					
                                    //alert(row2.cells[2].childNodes[0]+'  i:'+i);					
                                    var txtTotPredObr = row2.cells[2].childNodes[0];
                                    var cmbPredObr = row2.cells[1].childNodes[0];
                                    if(null !== txtTotPredObr) 
                                    {
                                        if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Obra') 
                                        { 
                                            if(txtTotPredObr.id === 'supTot') 
                                            { sum = parseInt(sum) + parseInt(txtTotPredObr.value);}
                                        }
                                        if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Predio') 
                                        { 
                                            if(txtTotPredObr.id === 'supTot') 
                                            { sum2 = parseInt(sum2) + parseInt(txtTotPredObr.value);}
                                        }						 
                                    }
                                }			   
                                document.getElementById("totObra").value = sum;	
                                document.getElementById("totPredio").value = sum2;
                            //finaliza realiza suma de total de predio y obra
                            }
                            //finaliza realiza suma de total de predio y obra	
                            return true; 
			}
			else{ return false; }				
		});
	   cell1.appendChild(element1); 	   
	   if(seccion === 'etapas')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = "";
		   
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "Archivo";
		   element2.disabled = true;
		   cell2.appendChild(element2);	   
		   
	   }
	   if(seccion === 'sustancias')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "TipoSustancia";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "0";
		   cell3.appendChild(element2);
		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);
		   element2 = document.createElement("input");	
		   element2.type = "button";
		   element2.value = "Adjunar archivo";
		   cell5.appendChild(element2);	   
		   
	   }
	   if(seccion === 'residuos')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo contaminante";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Etapa";
		   element2.add(option);
		   cell3.appendChild(element2);	
		   
		   var cell4 = row.insertCell(3);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "0";		   
		   cell4.appendChild(element2);
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell5.appendChild(element2);		     
		   
		   var cell6 = row.insertCell(5);
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.className = "textArea";	
		   element2.rows="1";			   
		   cell6.appendChild(element2);	  
		   
		   
	   }
	   if(seccion === 'adjuntaArch')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "Archivo";
		   element2.disabled = true;
		   cell2.appendChild(element2);
	   } 
	   if(seccion === 'impactoAmbiental')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo Impacto";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("textarea");	
		   cell3.appendChild(element2);	
		   
		   var cell4 = row.insertCell(3);
		   element2 = document.createElement("textarea");	   
		   cell4.appendChild(element2);
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("textarea");	
		   cell5.appendChild(element2);			   
		   
	   }
	   if(seccion === 'planosAdc')
	   { 
	   		var element2 = "";	
			
		   	var cell3 = row.insertCell(1);
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.rows="1";		   
		   cell3.appendChild(element2);			   
		   
		   var cell2 = row.insertCell(2); 			   	   
		   element2 = document.createElement("input");	
		   element2.type = "file";	
		   cell2.appendChild(element2);	 	   
		   
	   }
	   if(seccion === 'superfProy')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "- Seleccione -";
		   element2.add(option);
		   option = document.createElement("option");
		   option.text = "Obra";
		   element2.add(option);		  
		   option = document.createElement("option");
		   option.text = "Predio";
		   element2.add(option);
                   element2.id="cmbPO";
                   
                   
                   crearEvento(element2, "change", function(){
			//inicia realiza suma de total de predio y obra	
                        var table2 = document.getElementById(tableID);
                        var rowCount2 = table.rows.length; 
                        var sum = 0;
                        var sum2 = 0;
                        for(var i=0; i<rowCount2; i++) 
                        {                                       
                            var row2 = table2.rows[i];					
                            //alert(row2.cells[2].childNodes[0]+'  i:'+i);					
                            var txtTotPredObr = row2.cells[2].childNodes[0];
                            var cmbPredObr = row2.cells[1].childNodes[0];
                            if(null !== txtTotPredObr) 
                            {
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Obra') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum = parseInt(sum) + parseInt(txtTotPredObr.value);}
                                }
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Predio') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum2 = parseInt(sum2) + parseInt(txtTotPredObr.value);}
                                }						 
                            }
                        }			   
                        document.getElementById("totObra").value = sum;	
                        document.getElementById("totPredio").value = sum2;
                    //finaliza realiza suma de total de predio y obra
			
		   });
                   cell2.appendChild(element2);	
                   
                   
                   
                 
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.id = "supTot";
		   element2.value = "0";		   
		   crearEvento(element2, "change", function(){
			//inicia realiza suma de total de predio y obra	
                        var table2 = document.getElementById(tableID);
                        var rowCount2 = table.rows.length; 
                        var sum = 0;
                        var sum2 = 0;
                        for(var i=0; i<rowCount2; i++) 
                        {                                       
                            var row2 = table2.rows[i];					
                            //alert(row2.cells[2].childNodes[0]+'  i:'+i);					
                            var txtTotPredObr = row2.cells[2].childNodes[0];
                            var cmbPredObr = row2.cells[1].childNodes[0];
                            if(null !== txtTotPredObr) 
                            {
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Obra') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum = parseInt(sum) + parseInt(txtTotPredObr.value);}
                                }
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Predio') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum2 = parseInt(sum2) + parseInt(txtTotPredObr.value);}
                                }						 
                            }
                        }			   
                        document.getElementById("totObra").value = sum;	
                        document.getElementById("totPredio").value = sum2;
                    //finaliza realiza suma de total de predio y obra
			
		   });
                   cell3.appendChild(element2);			

		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.rows="1";	
		   cell5.appendChild(element2);			   
	   }	   
}

function deleteRow(tableID)
{
	   try 
	   {
		   var table = document.getElementById(tableID);
		   var rowCount = table.rows.length; 
	
		   for(var i=0; i<rowCount; i++) 
		   {
				var row = table.rows[i];
				
				
				
				var chkbox = row.cells[0].childNodes[0].childNodes[0];
				if(null != chkbox && true == chkbox.checked) 
				{	//alert(i);
					 table.deleteRow(i);
					 rowCount--;
					 i--;
				}
		   }
	
	   }catch(e) { alert(e);  }
}



/*

function addRow(tableID,seccion) 
{

	   var table = document.getElementById(tableID);
	   var rowCount = table.rows.length;
	   var row = table.insertRow(rowCount);
	   var cell1 = "";
	   var element1 = "";
	   var elementAux = "";
	   var suma=0;
	   
	   elementAux = document.createElement("div");	
	   elementAux.className="contenedorMiCheck";
	   
	   cell1 = row.insertCell(0);
	   element1 = document.createElement("input");
	   element1.type = "checkbox";	
	   
	   
	   element1.className = "miCheck";  	   
	   crearEvento(element1, "click", function()
	   {	//alert("entro: "+tableID);   
		   try 
		   {  
			   var table = document.getElementById(tableID);
			   var rowCount = table.rows.length; 
				
			   for(var i=0; i<rowCount; i++) 
			   {
					var row = table.rows[i];					
					//alert(row.cells[0].childNodes[0].childNodes[0]+'  i:'+i);					
					var chkbox = row.cells[0].childNodes[0].childNodes[0];
					if(null != chkbox && true == chkbox.checked) 
					{
						 table.deleteRow(i);
						 rowCount--;
						 i--;
					}
			   }
		
		   }catch(e) { alert(e);  }	
		});
	   elementAux.appendChild(element1); 
	   cell1.appendChild(elementAux); 
	
	   
	   if(seccion == 'etapas')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = "";
		   
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "Archivo";
		   element2.disabled = true;
		   cell2.appendChild(element2);	   
		   
	   }
	   if(seccion == 'sustancias')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "TipoSustancia";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "0";
		   cell3.appendChild(element2);
		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);
		   element2 = document.createElement("input");	
		   element2.type = "button";
		   element2.value = "Adjunar archivo";
		   cell5.appendChild(element2);	   
		   
	   }
	   if(seccion == 'residuos')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo contaminante";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Etapa";
		   element2.add(option);
		   cell3.appendChild(element2);	
		   
		   var cell4 = row.insertCell(3);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "0";		   
		   cell4.appendChild(element2);
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell5.appendChild(element2);		     
		   
		   var cell6 = row.insertCell(5);
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.className = "textArea";	
		   element2.rows="1";			   
		   cell6.appendChild(element2);	  
		   
		   
	   }
	   if(seccion == 'adjuntaArch')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "Archivo";
		   element2.disabled = true;
		   cell2.appendChild(element2);
	   } 
	   if(seccion == 'impactoAmbiental')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo Impacto";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("textarea");	
		   cell3.appendChild(element2);	
		   
		   var cell4 = row.insertCell(3);
		   element2 = document.createElement("textarea");	   
		   cell4.appendChild(element2);
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("textarea");	
		   cell5.appendChild(element2);			   
		   
	   }
	   if(seccion == 'planosAdc')
	   { 
	   		var element2 = "";	
			
		   	var cell3 = row.insertCell(1);
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.rows="1";		   
		   cell3.appendChild(element2);			   
		   
		   var cell2 = row.insertCell(2); 			   	   
		   element2 = document.createElement("input");	
		   element2.type = "file";	
		   cell2.appendChild(element2);	 	   
		   
	   }
	   if(seccion == 'superfProy')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "- Seleccione -";
		   element2.add(option);
		   option = document.createElement("option");
		   option.text = "Obra";
		   element2.add(option);		  
		   option = document.createElement("option");
		   option.text = "Predio";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.id = "supTot";
		   element2.value = "0";		   
		   crearEvento(element2, "change", function(){
			// Hacemos la sumatoria
			suma = document.getElementById("totObra").value;  //alert(this.value);
			suma = parseInt(suma) + parseInt(this.value);
			document.getElementById("totObra").value = suma;
			});
			cell3.appendChild(element2);			

		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.rows="1";	
		   cell5.appendChild(element2);			   
	   }	   
}

function deleteRow(tableID)
{
	   try 
	   {
		   var table = document.getElementById(tableID);
		   var rowCount = table.rows.length; 
	
		   for(var i=0; i<rowCount; i++) 
		   {
				var row = table.rows[i];
				
				
				
				var chkbox = row.cells[0].childNodes[0].childNodes[0];
				if(null != chkbox && true == chkbox.checked) 
				{	//alert(i);
					 table.deleteRow(i);
					 rowCount--;
					 i--;
				}
		   }
	
	   }catch(e) { alert(e);  }
}
*/




function crearEvento(elemento, evento, funcion) 
{
    if (elemento.addEventListener) 
	{ elemento.addEventListener(evento, funcion, false);} 
	else 
	{ elemento.attachEvent("on" + evento, funcion);}
}

function evaluaCuestNorma(forma,seccion)  //
{
	//div Cuestionario1
	// radio Preg1Nor116   Preg2Nor116   Preg3Nor116
	/*alert(forma.name);
	if(forma.Preg1Nor116[1].checked)
	{  alert('holaaa'); } */
	
	document.getElementById('Tabla1').style.visibility="hidden";
	document.getElementById('Tabla2').style.visibility="hidden";
	document.getElementById('Tabla3').style.visibility="hidden";
	document.getElementById('Tabla4').style.visibility="hidden";
	document.getElementById('Tabla5').style.visibility="hidden";
	document.getElementById('Tabla6').style.visibility="hidden";
	document.getElementById('Tabla7').style.visibility="hidden";
	document.getElementById('Tabla8').style.visibility="hidden";
	document.getElementById('Tabla9').style.visibility="hidden";
	document.getElementById('DescripcionParque').style.visibility="hidden";		
	document.getElementById('DescripcionOrd').style.visibility="hidden";
	document.getElementById('DescripcionDes').style.visibility="hidden";	
	
	//norma 116
	if(seccion == "Cuestionario1" && forma.Preg1Nor116[1].checked && forma.Preg2Nor116[1].checked && forma.Preg3Nor116[1].checked)
	{		//alert('seccion: ' + seccion + '   formaP1: ' + forma.Preg1Nor116[1].checked+'    formaP2: ' + forma.Preg2Nor116[1].checked + '   formaP3: ' + forma.Preg3Nor116[1].checked);
		document.getElementById('Tabla1').style.visibility="visible";
		
	} 
	if((seccion == "Cuestionario1" && forma.Preg1Nor116[0].checked) 
	    || (seccion == "Cuestionario1" && forma.Preg2Nor116[0].checked) 
		|| (seccion == "Cuestionario1" && forma.Preg3Nor116[0].checkede))
	{ alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); }
	//norma 120
	if(seccion == "Cuestionario2")
	{	
		document.getElementById('NomCatCli').style.visibility="hidden";
		document.getElementById('TipoDeClimas').style.visibility="hidden";
		document.getElementById('TipoDeVegetacion').style.visibility="hidden";		
		document.getElementById('ResSIPreg4Nor120').style.visibility="hidden";
		document.getElementById('ResNOPreg4Nor120').style.visibility="hidden";
		document.getElementById('NomPreg4Nor120').style.visibility="hidden";
		document.getElementById('Preg4Nor120').style.visibility="hidden";		
		forma.Preg4Nor120[0].style.visibility="hidden";
		forma.Preg4Nor120[1].style.visibility="hidden";
		document.getElementById('NomCatNor').style.visibility="hidden";
		document.getElementById('CatalogoDeLeyesMineras').style.visibility="hidden";
		document.getElementById('Tabla2').style.visibility="hidden";
	
		if(forma.Preg1Nor120[1].checked) //no a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			document.getElementById('NomCatCli').style.visibility="visible";
			document.getElementById('TipoDeClimas').style.visibility="visible";
			document.getElementById('TipoDeVegetacion').style.visibility="visible";
			
			// sino no se leccionana nada de los cambos, se procede a IP
			if(forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text == "Ninguno" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text == "Ninguno")
			{
				document.getElementById('NomPreg4Nor120').style.visibility="visible";	
				forma.Preg4Nor120[0].style.visibility="visible";
				forma.Preg4Nor120[1].style.visibility="visible";
				document.getElementById('ResSIPreg4Nor120').style.visibility="visible";				
				document.getElementById('ResNOPreg4Nor120').style.visibility="visible";		
				document.getElementById('Preg4Nor120').style.visibility="visible";
				
				if (forma.Preg4Nor120[0].checked) // si selecciona si a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
					document.getElementById('NomCatNor').style.visibility="visible";
					document.getElementById('CatalogoDeLeyesMineras').style.visibility="visible";  //Ninguno
					//document.getElementById('Tabla2').style.visibility="visible";
					
					if(forma.CatalogoDeLeyesMineras.options[forma.CatalogoDeLeyesMineras.selectedIndex].text != "Ninguno" && forma.CatalogoDeLeyesMineras.options[forma.CatalogoDeLeyesMineras.selectedIndex].text != "- Seleccione -")
					{						
						alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
					}
					else
					{
						document.getElementById('Tabla2').style.visibility="visible";
							
					}
				}
				if (forma.Preg4Nor120[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
					document.getElementById('Tabla2').style.visibility="visible";
					
				}
				
			}
			//de lo contrario pocede MIA
			if((forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text == "Ninguno" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text != "Ninguno") 
				|| (forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text != "Ninguno" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text == "Ninguno"))
			{
				alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
			}
			
		}
		else if(forma.Preg1Nor120[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{	
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
		}	
	}
	//norma 129
	if(seccion == "Cuestionario3" && forma.Preg1Nor129[1].checked && forma.Preg2Nor129[1].checked && forma.Preg3Nor129[1].checked)
	{		//alert('seccion: ' + seccion + '   formaP1: ' + forma.Preg1Nor116[1].checked+'    formaP2: ' + forma.Preg2Nor116[1].checked + '   formaP3: ' + forma.Preg3Nor116[1].checked);
		document.getElementById('Tabla3').style.visibility="visible";
		
	} 
	if((seccion == "Cuestionario3" && forma.Preg1Nor129[0].checked) 
	    || (seccion == "Cuestionario3" && forma.Preg2Nor129[0].checked) 
		|| (seccion == "Cuestionario3" && forma.Preg3Nor129[0].checkede))
	{ alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); }
	
	//norma 141
	if(seccion == "Cuestionario5")
	{	
	
		document.getElementById('NomPreg2Nor141').style.visibility="hidden";
		document.getElementById('ResSIPreg2Nor141').style.visibility="hidden";
		document.getElementById('ResNOPreg2Nor141').style.visibility="hidden";		
		document.getElementById('Preg2Nor141').style.visibility="hidden";
		forma.Preg2Nor141[0].style.visibility="hidden";
		forma.Preg2Nor141[1].style.visibility="hidden";
		document.getElementById('NomCatNor141').style.visibility="hidden";
		document.getElementById('CatalogoDeLeyesMineras141').style.visibility="hidden";
		document.getElementById('Tabla5').style.visibility="hidden";
	
		if(forma.Preg1Nor141[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 141
		{						
				document.getElementById('NomPreg2Nor141').style.visibility="visible";	
				forma.Preg2Nor141[0].style.visibility="visible";
				forma.Preg2Nor141[1].style.visibility="visible";
				document.getElementById('ResSIPreg2Nor141').style.visibility="visible";				
				document.getElementById('ResNOPreg2Nor141').style.visibility="visible";		
				
				
				if (forma.Preg2Nor141[0].checked) // si selecciona si a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{			
					document.getElementById('NomCatNor141').style.visibility="visible";
					document.getElementById('CatalogoDeLeyesMineras141').style.visibility="visible";  //Ninguno
					//document.getElementById('Tabla2').style.visibility="visible";
					
					if(forma.CatalogoDeLeyesMineras141.options[forma.CatalogoDeLeyesMineras141.selectedIndex].text != "Ninguno" && forma.CatalogoDeLeyesMineras141.options[forma.CatalogoDeLeyesMineras141.selectedIndex].text != "- Seleccione -")
					{						
						alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
					}
					else
					{
						document.getElementById('Tabla5').style.visibility="visible";
							
					}
				}
				if (forma.Preg2Nor141[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
					document.getElementById('Tabla5').style.visibility="visible";
					
				}
			
		}
		else if(forma.Preg1Nor141[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
		}	
	}
	//norma 150
	if(seccion == "Cuestionario7")
	{	
		document.getElementById('NomCatCli150').style.visibility="hidden";
		document.getElementById('TipoDeClimas150').style.visibility="hidden";
		document.getElementById('TipoDeVegetacion150').style.visibility="hidden";		
		document.getElementById('ResSIPreg4Nor150').style.visibility="hidden";
		document.getElementById('ResNOPreg4Nor150').style.visibility="hidden";
		document.getElementById('NomPreg4Nor150').style.visibility="hidden";
		document.getElementById('Preg4Nor150').style.visibility="hidden";		
		forma.Preg4Nor150[0].style.visibility="hidden";
		forma.Preg4Nor150[1].style.visibility="hidden";
		
		
		document.getElementById('ResSIPreg5Nor150').style.visibility="hidden";
		document.getElementById('ResNOPreg5Nor150').style.visibility="hidden";
		document.getElementById('NomPreg5Nor150').style.visibility="hidden";
		document.getElementById('Preg5Nor150').style.visibility="hidden";	
		forma.Preg5Nor150[0].style.visibility="hidden";
		forma.Preg5Nor150[1].style.visibility="hidden";
		document.getElementById('Tabla7').style.visibility="hidden";
	
		if(forma.Preg1Nor150[1].checked) //no a pregunta  �Es zona agr�cola o ganadera? de norma 150
		{
			document.getElementById('NomCatCli150').style.visibility="visible";
			document.getElementById('TipoDeClimas150').style.visibility="visible";
			document.getElementById('TipoDeVegetacion150').style.visibility="visible";
			
			// sino no se leccionana nada de los cambos, se procede a IP
			if(forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text == "Ninguno" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text == "Ninguno")
			{
				document.getElementById('NomPreg4Nor150').style.visibility="visible";	
				forma.Preg4Nor150[0].style.visibility="visible";
				forma.Preg4Nor150[1].style.visibility="visible";
				document.getElementById('ResSIPreg4Nor150').style.visibility="visible";				
				document.getElementById('ResNOPreg4Nor150').style.visibility="visible";		
				document.getElementById('NomPreg5Nor150').style.visibility="visible";	
				forma.Preg5Nor150[0].style.visibility="visible";
				forma.Preg5Nor150[1].style.visibility="visible";
				document.getElementById('ResSIPreg5Nor150').style.visibility="visible";				
				document.getElementById('ResNOPreg5Nor150').style.visibility="visible";	
				
				if (forma.Preg4Nor150[0].checked || forma.Preg5Nor150[0].checked) // si selecciona si a pregunta �Se trata de un �rea natural protegida? de norna 150
				{						
						alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 		
						document.getElementById('Tabla7').style.visibility="hidden";
				}
				if (forma.Preg4Nor150[1].checked &&  forma.Preg5Nor150[1].checked) // si selecciona no a  pregunta �Se trata de un �rea natural protegida? de norna 150
				{
					document.getElementById('Tabla7').style.visibility="visible";
					
				}
				
			}
			//de lo contrario pocede MIA
			if((forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text == "Ninguno" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text != "Ninguno") 
				|| (forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text != "Ninguno" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text == "Ninguno"))
			{
				alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
			}
			
		}
		else if(forma.Preg1Nor150[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
		}	
	}
	
	//norma 155
	if(seccion == "Cuestionario8")
	{	
	
		document.getElementById('NomPreg2Nor155').style.visibility="hidden";
		document.getElementById('ResSIPreg2Nor155').style.visibility="hidden";
		document.getElementById('ResNOPreg2Nor155').style.visibility="hidden";		
		document.getElementById('Preg2Nor155').style.visibility="hidden";		
		document.getElementById('NomCatNor155').style.visibility="hidden";
		document.getElementById('CatalogoDeLeyesMineras155').style.visibility="hidden";
		document.getElementById('Tabla8').style.visibility="hidden";
		forma.Preg2Nor155[0].style.visibility="hidden";
		forma.Preg2Nor155[1].style.visibility="hidden";
		
		if(forma.Preg1Nor155[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 155
		{						
				document.getElementById('NomPreg2Nor155').style.visibility="visible";	
				forma.Preg2Nor155[0].style.visibility="visible";
				forma.Preg2Nor155[1].style.visibility="visible";
				document.getElementById('ResSIPreg2Nor155').style.visibility="visible";				
				document.getElementById('ResNOPreg2Nor155').style.visibility="visible";		
				
				
				if (forma.Preg2Nor155[0].checked) // si selecciona si a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{			
					document.getElementById('NomCatNor155').style.visibility="visible";
					document.getElementById('CatalogoDeLeyesMineras155').style.visibility="visible";  //Ninguno
					//document.getElementById('Tabla2').style.visibility="visible";
					
					if(forma.CatalogoDeLeyesMineras155.options[forma.CatalogoDeLeyesMineras155.selectedIndex].text != "Ninguno" && forma.CatalogoDeLeyesMineras155.options[forma.CatalogoDeLeyesMineras155.selectedIndex].text != "- Seleccione -")
					{						
						alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
					}
					else
					{
						document.getElementById('Tabla8').style.visibility="visible";
							
					}
				}
				if (forma.Preg2Nor155[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
					document.getElementById('Tabla8').style.visibility="visible";
					
				}
			
		}
		else if(forma.Preg1Nor155[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
		}	
	}
	
	//norma 159
	if(seccion == "Cuestionario9")
	{	
	
		document.getElementById('NomPreg2Nor159').style.visibility="hidden";
		document.getElementById('ResSIPreg2Nor159').style.visibility="hidden";
		document.getElementById('ResNOPreg2Nor159').style.visibility="hidden";		
		document.getElementById('Preg2Nor159').style.visibility="hidden";		
		forma.Preg2Nor159[0].style.visibility="hidden";
		forma.Preg2Nor159[1].style.visibility="hidden";
		document.getElementById('NomCatNor159').style.visibility="hidden";
		document.getElementById('CatalogoDeLeyesMineras159').style.visibility="hidden";
		document.getElementById('Tabla9').style.visibility="hidden";
	
		if(forma.Preg1Nor159[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 159
		{						
				document.getElementById('NomPreg2Nor159').style.visibility="visible";	
				forma.Preg2Nor159[0].style.visibility="visible";
				forma.Preg2Nor159[1].style.visibility="visible";
				document.getElementById('ResSIPreg2Nor159').style.visibility="visible";				
				document.getElementById('ResNOPreg2Nor159').style.visibility="visible";		
				
				
				if (forma.Preg2Nor159[0].checked) // si selecciona si a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{			
					document.getElementById('NomCatNor159').style.visibility="visible";
					document.getElementById('CatalogoDeLeyesMineras159').style.visibility="visible";  //Ninguno
					//document.getElementById('Tabla2').style.visibility="visible";
					
					if(forma.CatalogoDeLeyesMineras159.options[forma.CatalogoDeLeyesMineras159.selectedIndex].text != "Ninguno" && forma.CatalogoDeLeyesMineras159.options[forma.CatalogoDeLeyesMineras159.selectedIndex].text != "- Seleccione -")
					{						
						alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
					}
					else
					{
						document.getElementById('Tabla9').style.visibility="visible";
							
					}
				}
				if (forma.Preg2Nor159[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
					document.getElementById('Tabla9').style.visibility="visible";
					
				}
			
		}
		else if(forma.Preg1Nor159[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
		}	
	}
	
	
	
	
	/*<option value="1" onClick="muestracapaCuestionarios('Cuestionario1');">Norma 116</option> <!--   muestracapaTablas('Tabla1') -->
	<option value="2" onClick="muestracapaCuestionarios('Cuestionario2'); ">Norma 120</option><!--  muestracapaTablas('Tabla2')  --->
	<option value="3" onClick="muestracapaCuestionarios('Cuestionario3'); ">Norma 129</option><!--  muestracapaTablas('Tabla3')  --->	
	<option value="4" onClick="muestracapaCuestionarios('Cuestionario4'); ">Norma 130</option><!--  muestracapaTablas('Tabla4')  --->
	<option value="5" onClick="muestracapaCuestionarios('Cuestionario5'); ">Norma 141</option><!--  muestracapaTablas('Tabla5')  --->
	<option value="6" onClick="muestracapaCuestionarios('Cuestionario6'); ">Norma 149</option><!--  muestracapaTablas('Tabla6')  --->
	<option value="7" onClick="muestracapaCuestionarios('Cuestionario7'); ">Norma 150</option><!--  muestracapaTablas('Tabla7')  --->
	<option value="8" onClick="muestracapaCuestionarios('Cuestionario8'); ">Norma 155</option><!-- muestracapaTablas('Tabla8')   --->
	<option value="9" onClick="muestracapaCuestionarios('Cuestionario9'); ">Norma 159</option> <!-- muestracapaTablas('Tabla9')   -->	*/
}

function borrado()
{
    if (confirm("Esta usted seguro de borrar el registro?"))
  { return true; }
  else{ return false; }	  
}



















