function infoAdicional(bInfoAdicional, bInfoOpcional, componente) {
    if ( bInfoAdicional ) {
        $("#" + componente).append("<span style='vertical-align: -50%'>Información solicitada</span>");
        $("#" + componente).css('background-color', 'red');
        $("#" + componente).css('color', 'white');
        $("#" + componente).css('font-size', '16px');
        $("#" + componente).css('text-align', 'center');
        $("#" + componente).css('vertical-align', 'middle');
        $("#" + componente).parent().css('border', '1px solid red');
    } else if( bInfoOpcional ) {
        $("#" + componente).append("<span style='vertical-align: -50%'>Información Complementaria</span>");
        $("#" + componente).css('background-color', 'blue');
        $("#" + componente).css('color', 'white');
        $("#" + componente).css('font-size', '16px');
        $("#" + componente).css('text-align', 'center');
        $("#" + componente).parent().css('border', '1px solid blue');
    }  
}


/*
var path = "/ModuloEvaluacionV2/InformacionAdicional?info=";

function infoAdicional(componente, capitulo, subCapitulo, seccion, apartado, folio, serial) {
    
    var path2 = path + capitulo + "," + subCapitulo + "," + seccion + "," + apartado + "," + folio + "," + serial;

    $.ajax({url: path2, async: false, success: function (result) {
            switch (result.trim()) {
                case "1":
                    $("#" + componente).append("<div>Información solicitada</div>");
                    $("#" + componente).css('background-color', 'red');
                    $("#" + componente).css('color', 'white');
                    $("#" + componente).css('font-size', '16px');
                    $("#" + componente).css('text-align', 'center');
                    $("#" + componente).parent().css('border', '1px solid red');
                    break;
                case "2":
                    $("#" + componente).append("<div>Información modificada por el usuario</div>");
                    $("#" + componente).css('background-color', 'red');
                    $("#" + componente).css('color', 'white');
                    $("#" + componente).css('font-size', '16px');
                    $("#" + componente).css('text-align', 'center');
                    $("#" + componente).parent().css('border', '1px solid red');
                    break;
            }

        }});
}

*/