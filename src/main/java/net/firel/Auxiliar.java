package net.firel;

import java.rmi.RemoteException;

public class Auxiliar {
	 private String entidad;
	 private String operador;
	 private String clave;
	 private String digestion;
	 private String tipoCodificacion;
	 // referencia
	 // aviso
	 // cadena (B64)
	 private String url;
	 
	 public Auxiliar(String url,String entidad, String operador, String clave, String digestion, String tipoCodificacion) {
		super();
		this.url=url;
		this.entidad = entidad;
		this.operador = operador;
		this.clave = clave;
		this.digestion = digestion;
		this.tipoCodificacion = tipoCodificacion;
	}
	public Auxiliar() {
		super();
	}

	 public BcAgregaCadenaRespuesta agregaCadena(String referencia, String aviso, String cadenaBase64) throws RemoteException, Exception {
		 
		    WebServiceImplProxy port = new WebServiceImplProxy(this.url);
			BcAgregaCadenaRespuesta respuesta = port.bcAgregaCadena(this.entidad, this.operador,this.clave, this.digestion,this.tipoCodificacion, referencia, aviso, cadenaBase64 );
			System.out.println(respuesta.getEstado() + " " + respuesta.getDescripcion() + " "+ respuesta.getIdentificador() + " " + respuesta.getTransferenciaOperacion());
			return respuesta;
		 
	 }
	 
	 public DetallesFirmado getFirmas(long transferenciaOperacion,String identificador,String referencia) throws RemoteException, Exception{
		 
	 	 WebServiceImplProxy port = new WebServiceImplProxy(this.url);
		 DetallesFirmado detalles = port.BCResultadoC006(entidad, operador, clave, transferenciaOperacion, identificador, referencia);
		 
		 if(detalles != null){
			 // System.out.println(detalles.getEstado()+" "+detalles.getDescripcion()+" "+detalles.getEstadoDescripcion()+" "+detalles.getEstadoTransferencia());
			 System.out.println("\n");
			 Firmas[] firmas=detalles.getFirmas();
			 if(firmas != null && firmas.length>0){
			      for(Firmas f:firmas){
			    	System.out.println(f.getCurp()+" "+f.getDescripcion()+" "+f.getEstadoSatelital()+" "+f.getFecha()+" "+f.getFirma()+" "+f.getNombre()+" "+f.getNumeroSerie()+" "+f.getRfc());
			      }
			 }
		 }
		 return detalles;		 
	 }
	 
	 public DetallesOperacion cierraLoteFirmas(long ebgTransferenciaOperacion,String evcIdentificador,String evcReferencia,String evcEstadoTransferencia,String evcEstadoDescripcion) throws RemoteException, Exception{
		 
		WebServiceImplProxy port = new WebServiceImplProxy(this.url);
		DetallesOperacion op=port.BCCierreC006(this.entidad, this.operador, this.clave, ebgTransferenciaOperacion, evcIdentificador, evcReferencia, evcEstadoTransferencia, evcEstadoDescripcion );
		return op;	 
		 
	 }
	 
			
	}

