package net.firel;

public class WebServiceImplProxy implements net.firel.WebServiceImpl {
  private String _endpoint = null;
  private net.firel.WebServiceImpl webServiceImpl = null;
  
  public WebServiceImplProxy() {
    _initWebServiceImplProxy();
  }
  
  public WebServiceImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initWebServiceImplProxy();
  }
  
  private void _initWebServiceImplProxy() {
    try {
      webServiceImpl = (new net.firel.WebServiceImplServiceLocator()).getWebServiceImplPort();
      if (webServiceImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)webServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)webServiceImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (webServiceImpl != null)
      ((javax.xml.rpc.Stub)webServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public net.firel.WebServiceImpl getWebServiceImpl() {
    if (webServiceImpl == null)
      _initWebServiceImplProxy();
    return webServiceImpl;
  }
  
  public net.firel.BcAgregaCadenaRespuesta bcAgregaCadena(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, java.lang.String evcTipoDigestion, java.lang.String evcTipoCodificacion, java.lang.String evcReferencia, java.lang.String evcAviso, java.lang.String etxCadena) throws java.rmi.RemoteException{
    if (webServiceImpl == null)
      _initWebServiceImplProxy();
    return webServiceImpl.bcAgregaCadena(evcEntidad, evcUsuarioOper, evcClave, evcTipoDigestion, evcTipoCodificacion, evcReferencia, evcAviso, etxCadena);
  }
  
  public net.firel.DetallesOperacion BCCierreC006(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, long ebgTransferenciaOperacion, java.lang.String evcIdentificador, java.lang.String evcReferencia, java.lang.String evcEstadoTransferencia, java.lang.String evcEstadoDescripcion) throws java.rmi.RemoteException{
    if (webServiceImpl == null)
      _initWebServiceImplProxy();
    return webServiceImpl.BCCierreC006(evcEntidad, evcUsuarioOper, evcClave, ebgTransferenciaOperacion, evcIdentificador, evcReferencia, evcEstadoTransferencia, evcEstadoDescripcion);
  }
  
  public net.firel.DetallesFirmado BCResultadoC006(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, long ebgTransferenciaOperacion, java.lang.String evcIdentificador, java.lang.String evcReferencia) throws java.rmi.RemoteException{
    if (webServiceImpl == null)
      _initWebServiceImplProxy();
    return webServiceImpl.BCResultadoC006(evcEntidad, evcUsuarioOper, evcClave, ebgTransferenciaOperacion, evcIdentificador, evcReferencia);
  }
  
  public net.firel.DetallesOperacion bcAgregaPfx(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, java.lang.String evcFirmante, byte[] eimArchivoPkcs12, java.lang.String evcPfxClave) throws java.rmi.RemoteException{
    if (webServiceImpl == null)
      _initWebServiceImplProxy();
    return webServiceImpl.bcAgregaPfx(evcEntidad, evcUsuarioOper, evcClave, evcFirmante, eimArchivoPkcs12, evcPfxClave);
  }
  
  
}