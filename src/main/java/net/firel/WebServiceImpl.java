/**
 * WebServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package net.firel;

public interface WebServiceImpl extends java.rmi.Remote {
    public net.firel.BcAgregaCadenaRespuesta bcAgregaCadena(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, java.lang.String evcTipoDigestion, java.lang.String evcTipoCodificacion, java.lang.String evcReferencia, java.lang.String evcAviso, java.lang.String etxCadena) throws java.rmi.RemoteException;
    public net.firel.DetallesOperacion BCCierreC006(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, long ebgTransferenciaOperacion, java.lang.String evcIdentificador, java.lang.String evcReferencia, java.lang.String evcEstadoTransferencia, java.lang.String evcEstadoDescripcion) throws java.rmi.RemoteException;
    public net.firel.DetallesFirmado BCResultadoC006(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, long ebgTransferenciaOperacion, java.lang.String evcIdentificador, java.lang.String evcReferencia) throws java.rmi.RemoteException;
    public net.firel.DetallesOperacion bcAgregaPfx(java.lang.String evcEntidad, java.lang.String evcUsuarioOper, java.lang.String evcClave, java.lang.String evcFirmante, byte[] eimArchivoPkcs12, java.lang.String evcPfxClave) throws java.rmi.RemoteException;
}
