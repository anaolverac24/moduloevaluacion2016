/**
 * DetallesFirmado.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package net.firel;

public class DetallesFirmado  implements java.io.Serializable {
    private java.lang.String descripcion;

    private int estado;

    private java.lang.String estadoDescripcion;

    private java.lang.String estadoTransferencia;

    private net.firel.Firmas[] firmas;
    
    private java.lang.String momento;
    
    private java.lang.String momentoFirma;

    public DetallesFirmado() {
    }

    public DetallesFirmado(
           java.lang.String descripcion,
           int estado,
           java.lang.String estadoDescripcion,
           java.lang.String estadoTransferencia,
           net.firel.Firmas[] firmas, 
           java.lang.String momento,
           java.lang.String momentoFirma) {
           this.descripcion = descripcion;
           this.estado = estado;
           this.estadoDescripcion = estadoDescripcion;
           this.estadoTransferencia = estadoTransferencia;
           this.firmas = firmas;
           this.momento = momento;
           this.momentoFirma = momentoFirma;
    }


    /**
     * Gets the descripcion value for this DetallesFirmado.
     * 
     * @return descripcion
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }


    /**
     * Sets the descripcion value for this DetallesFirmado.
     * 
     * @param descripcion
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }


    /**
     * Gets the estado value for this DetallesFirmado.
     * 
     * @return estado
     */
    public int getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this DetallesFirmado.
     * 
     * @param estado
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }


    /**
     * Gets the estadoDescripcion value for this DetallesFirmado.
     * 
     * @return estadoDescripcion
     */
    public java.lang.String getEstadoDescripcion() {
        return estadoDescripcion;
    }


    /**
     * Sets the estadoDescripcion value for this DetallesFirmado.
     * 
     * @param estadoDescripcion
     */
    public void setEstadoDescripcion(java.lang.String estadoDescripcion) {
        this.estadoDescripcion = estadoDescripcion;
    }


    /**
     * Gets the estadoTransferencia value for this DetallesFirmado.
     * 
     * @return estadoTransferencia
     */
    public java.lang.String getEstadoTransferencia() {
        return estadoTransferencia;
    }


    /**
     * Sets the estadoTransferencia value for this DetallesFirmado.
     * 
     * @param estadoTransferencia
     */
    public void setEstadoTransferencia(java.lang.String estadoTransferencia) {
        this.estadoTransferencia = estadoTransferencia;
    }


    /**
     * Gets the firmas value for this DetallesFirmado.
     * 
     * @return firmas
     */
    public net.firel.Firmas[] getFirmas() {
        return firmas;
    }


    /**
     * Sets the firmas value for this DetallesFirmado.
     * 
     * @param firmas
     */
    public void setFirmas(net.firel.Firmas[] firmas) {
        this.firmas = firmas;
    }

    public net.firel.Firmas getFirmas(int i) {
        return this.firmas[i];
    }

    public void setFirmas(int i, net.firel.Firmas _value) {
        this.firmas[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DetallesFirmado)) return false;
        DetallesFirmado other = (DetallesFirmado) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.descripcion==null && other.getDescripcion()==null) || 
             (this.descripcion!=null &&
              this.descripcion.equals(other.getDescripcion()))) &&
            this.estado == other.getEstado() &&
            ((this.estadoDescripcion==null && other.getEstadoDescripcion()==null) || 
             (this.estadoDescripcion!=null &&
              this.estadoDescripcion.equals(other.getEstadoDescripcion()))) &&
            ((this.estadoTransferencia==null && other.getEstadoTransferencia()==null) || 
             (this.estadoTransferencia!=null &&
              this.estadoTransferencia.equals(other.getEstadoTransferencia()))) &&
            ((this.firmas==null && other.getFirmas()==null) || 
             (this.firmas!=null &&
              java.util.Arrays.equals(this.firmas, other.getFirmas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescripcion() != null) {
            _hashCode += getDescripcion().hashCode();
        }
        _hashCode += getEstado();
        if (getEstadoDescripcion() != null) {
            _hashCode += getEstadoDescripcion().hashCode();
        }
        if (getEstadoTransferencia() != null) {
            _hashCode += getEstadoTransferencia().hashCode();
        }
        if (getFirmas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFirmas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFirmas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DetallesFirmado.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://firel.net/", "detallesFirmado"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoDescripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoDescripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoTransferencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoTransferencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firmas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "firmas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://firel.net/", "firmas"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * @return the momento
     */
    public java.lang.String getMomento() {
        return momento;
    }

    /**
     * @return the momentoFirma
     */
    public java.lang.String getMomentoFirma() {
        return momentoFirma;
    }

    /**
     * @param momento the momento to set
     */
    public void setMomento(java.lang.String momento) {
        this.momento = momento;
    }

    /**
     * @param momentoFirma the momentoFirma to set
     */
    public void setMomentoFirma(java.lang.String momentoFirma) {
        this.momentoFirma = momentoFirma;
    }

}
