/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.HashMap;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import mx.gob.semarnat.ws.servicio.Archivo;
import mx.gob.semarnat.ws.servicio.FileDescription;
import mx.gob.semarnat.ws.servicio.FileList;
import mx.gob.semarnat.ws.servicio.WSConsume;
import mx.gob.semarnat.ws.servicio.WSConsumeService;
import org.apache.commons.ssl.Base64;
import org.apache.log4j.Logger;

/**
 *
 * @author eescalona
 * @date 26/03/2015
 * 
 */

/*
    Empleo de herramienta JDK para importar objetos a partir de la dirección wsdl
    C:\Program Files\Java\jdk1.7.0_60\bin>wsimport -keep -verbose -s "C:\eescalona\Workspace\ModuloEvaluacionV2\src\java" -p mx.gob.semarnat.ws.servicio http://app2.semarnat.gob.mx:8080/WSMigrate/WSConsume?wsdl
 */
public class WSRecuperaArchivos {
    private static final Logger logger = Logger.getLogger(WSRecuperaArchivos.class.getName());
    
    public static HashMap<Long,String> obtenerListaArchivos(int folio, int estatus) {
        HashMap<Long,String> mapArchivos = new HashMap<>();
        try {
            URL url = new URL(Utils.wsRecuperacionArchivos);
            WSConsumeService servicio = new WSConsumeService(url);
            WSConsume consume = servicio.getWSConsumePort();

            FileList archivos = consume.getListFiles(folio, estatus);
            
            if (archivos.getEstado() == GenericConstants.WS_RESP_OK) {
                
                for (Archivo archivo : archivos.getArchivo()) {
                    logger.debug("Id : " + archivo.getId() + " | nombreTramite: " + archivo.getNombreTramite());
                    mapArchivos.put(archivo.getId(), archivo.getNombreTramite());
                }
                
            } else {
                logger.info("WS request estatus: " + archivos.getDescripcion());
            }
            
        } catch (Exception e) {
            logger.error("Ocurrió un error durante la obtención del listado de archivos del webservice", e);
        }
        
        return mapArchivos;
        
    }
    
    public static File obtenerArchivo(int id) {
        try {
            URL url = new URL(Utils.wsRecuperacionArchivos);
            WSConsumeService servicio = new WSConsumeService(url);
            WSConsume consume = servicio.getWSConsumePort();

            FileDescription archivo = consume.getFile(id);
            
            if (archivo.getEstado() == GenericConstants.WS_RESP_OK) {
                logger.debug("WS archivo obtenido: " + archivo.getNombre());
                //Generación del archivo
                File fArch = new File(archivo.getNombre());
                fArch.createNewFile();
                
                byte[] bArch = Base64.decodeBase64(archivo.getB64File().getBytes());
                
                FileOutputStream fos = new FileOutputStream(fArch);
                fos.write(bArch);
                fos.close();
                
                return fArch;
                
            } else {
                logger.info("WS request estatus: " + archivo.getDescripcion());
                return null;
            }
            
        } catch (Exception e) {
            logger.error("Ocurrió un error durante la obtención de un archivo del webservice", e);
            return null;
        }
    }
}
