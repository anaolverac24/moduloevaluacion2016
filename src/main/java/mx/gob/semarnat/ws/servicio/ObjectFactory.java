
package mx.gob.semarnat.ws.servicio;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.gob.semarnat.ws.servicio package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetFileResponse_QNAME = new QName("http://net.firel.ws/", "getFileResponse");
    private final static QName _GetListFiles_QNAME = new QName("http://net.firel.ws/", "getListFiles");
    private final static QName _GetFile_QNAME = new QName("http://net.firel.ws/", "getFile");
    private final static QName _GetListFilesResponse_QNAME = new QName("http://net.firel.ws/", "getListFilesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.gob.semarnat.ws.servicio
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetFile }
     * 
     */
    public GetFile createGetFile() {
        return new GetFile();
    }

    /**
     * Create an instance of {@link GetListFilesResponse }
     * 
     */
    public GetListFilesResponse createGetListFilesResponse() {
        return new GetListFilesResponse();
    }

    /**
     * Create an instance of {@link GetFileResponse }
     * 
     */
    public GetFileResponse createGetFileResponse() {
        return new GetFileResponse();
    }

    /**
     * Create an instance of {@link GetListFiles }
     * 
     */
    public GetListFiles createGetListFiles() {
        return new GetListFiles();
    }

    /**
     * Create an instance of {@link FileDescription }
     * 
     */
    public FileDescription createFileDescription() {
        return new FileDescription();
    }

    /**
     * Create an instance of {@link FileList }
     * 
     */
    public FileList createFileList() {
        return new FileList();
    }

    /**
     * Create an instance of {@link Archivo }
     * 
     */
    public Archivo createArchivo() {
        return new Archivo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://net.firel.ws/", name = "getFileResponse")
    public JAXBElement<GetFileResponse> createGetFileResponse(GetFileResponse value) {
        return new JAXBElement<GetFileResponse>(_GetFileResponse_QNAME, GetFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://net.firel.ws/", name = "getListFiles")
    public JAXBElement<GetListFiles> createGetListFiles(GetListFiles value) {
        return new JAXBElement<GetListFiles>(_GetListFiles_QNAME, GetListFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://net.firel.ws/", name = "getFile")
    public JAXBElement<GetFile> createGetFile(GetFile value) {
        return new JAXBElement<GetFile>(_GetFile_QNAME, GetFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListFilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://net.firel.ws/", name = "getListFilesResponse")
    public JAXBElement<GetListFilesResponse> createGetListFilesResponse(GetListFilesResponse value) {
        return new JAXBElement<GetListFilesResponse>(_GetListFilesResponse_QNAME, GetListFilesResponse.class, null, value);
    }

}
