package mx.gob.semarnat.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.primefaces.context.RequestContext;

import mx.go.semarnat.services.ServicioDocument;
import mx.go.semarnat.services.ServicioUnidadAdministrativa;
import mx.go.semarnat.services.UnidadAdministrativa;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.mia.servicios.ServicioTokenGO;
import mx.gob.semarnat.mia.servicios.dgiramiae.ServicioResutando;
import mx.gob.semarnat.mia.servicios.evaluacion.ServicioOpinionTecnicaProyecto;
import mx.gob.semarnat.model.dgira_miae.Document;
import mx.gob.semarnat.model.dgira_miae.OpinionTecnicaProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.Resultando;
import mx.gob.semarnat.model.seguridad.TokenGO;

@SuppressWarnings("serial")
@ManagedBean(name = "opinionTecnicaBean")
@ViewScoped
public class OpinionTecnicaBean extends BaseBean implements Serializable{

	private final String MOD_EDITAR = "Editar registro";
	private String bitacoraProyecto;
        private String bitacoraProyectoUrl;
        private String turnado;
	private String folioProyecto;
	private Integer idusuario;
	private String document_id;
	private String template;
	private List<UnidadAdministrativa> listaResultandos = new ArrayList<UnidadAdministrativa>();
	private List<UnidadAdministrativa> unidadesSelecionados = new ArrayList<UnidadAdministrativa>();
	
	private String nombreDependencia;
	private String cargo;
	private String nombbreDestinatario;
	private long idSeleccionado;
	
	private String tituloModal;
	
	//servicios
	private ServicioUnidadAdministrativa sua;
	private ServicioOpinionTecnicaProyecto sotp;
	
	public OpinionTecnicaBean() {
		
		inicializarBean();
	}
	
	private void inicializarBean() {
            FacesContext fContext = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("userFolioProy");
            idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            document_id = req.getParameter("documentId");
            template = req.getParameter("template");
            if(template.equals("OP"))
                template = "Oficio de Solicitud de Opinión Técnica";
            bitacoraProyecto = req.getParameter("numBitacora");
            System.out.println("folioProyecto: " + folioProyecto);
            System.out.println("idusuario: " + idusuario);
            System.out.println("document_id: " + document_id);
            System.out.println("template: " + template);
            System.out.println("bitacoraProyecto: " + bitacoraProyecto);
            sua = new ServicioUnidadAdministrativa();
            sotp = new ServicioOpinionTecnicaProyecto();
            listaResultandos = sua.buscarEntidadesOpinionTecnica(bitacoraProyecto);
            boolean edita;
            for (UnidadAdministrativa get : listaResultandos) {
                try {
                    edita = sotp.findExpedient(bitacoraProyecto, get.getId());
                    get.setOficio(edita);
                }catch (MiaExcepcion ex) {
                    Logger.getLogger(OpinionTecnicaBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
                bitacoraProyectoUrl = req.getParameter("numBitacora");
                turnado = "true".equals(req.getParameter("turnado")) ? "1":"0";
	}

	public void abrirModalEditarRegistro(ActionEvent e) {
		System.out.println("abrirModalResultandoEditarRegistro");
		tituloModal = MOD_EDITAR;
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String idActividad = params.get("idSeleccion").toString();
		int position = Integer.parseInt(idActividad);
		System.out.println("Position: " + position);
		UnidadAdministrativa r = listaResultandos.get(position);
		limpiarFormulario();
		cargarValores(r);
		RequestContext.getCurrentInstance().reset("resultForm:grid");
		RequestContext.getCurrentInstance().execute("PF('dlogResultando').show();");
	}
	
	private void limpiarFormulario() {
		nombreDependencia = null;
		cargo = null;
		nombbreDestinatario = null;
		idSeleccionado = 0;
	}
	
	public void guardar(ActionEvent e) {
		System.out.println("Click en guardar");
		sua = new ServicioUnidadAdministrativa();
		
		try {
			UnidadAdministrativa item = sua.findById(idSeleccionado);
			System.out.println("----> " + item.getId());
			item.setNombbreDestinatario(nombbreDestinatario);
			System.out.println("----> " + item.getNombbreDestinatario());
			
			sua.saveUnidad(item);
			listaResultandos = sua.buscarEntidadesOpinionTecnica(bitacoraProyecto);
			RequestContext.getCurrentInstance().execute("PF('dlogResultando').hide();");
			addMessage("growl", FacesMessage.SEVERITY_INFO, "Registro editado exitosamente");
		} catch (MiaExcepcion e1) {
			e1.printStackTrace();
			// Fallo
			addMessage("growl", FacesMessage.SEVERITY_ERROR, "No se pudo guardar el registro");
		}
	}
	
	private void addMessage(String componentId, Severity severity, String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(componentId, new FacesMessage(severity, message, message));
	}
	
	private void cargarValores(UnidadAdministrativa r) {
		nombreDependencia = r.getNombreDependencia();
		cargo = r.getCargo();
		nombbreDestinatario = r.getNombbreDestinatario();
		idSeleccionado = r.getId();
	}
	
	public void generaOficioUnico(){
            sua = new ServicioUnidadAdministrativa();
            sotp = new ServicioOpinionTecnicaProyecto();
            ////Marcar las dependencias seleccionadas
            Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            String idActividad = params.get("idSeleccion").toString();
            int position = Integer.parseInt(idActividad);
            System.out.println("Position: " + position);
            UnidadAdministrativa unidadAdministrativa = listaResultandos.get(position);

            System.out.println(bitacoraProyecto);
            System.out.println(unidadAdministrativa.getId());
            try {
                sotp.updateSeleccion(bitacoraProyecto, unidadAdministrativa.getId());

                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                String goApiURL = servletContext.getInitParameter("goApi_url");
                String goURL = servletContext.getInitParameter("go_url");
                System.out.println("Generar Oficio: " + template);
                System.out.println("IdUsuario: " + idusuario);
                System.out.println("GoApi URL: " + goApiURL);
                RequestContext context = RequestContext.getCurrentInstance();

                HttpURLConnection conn;

                OutputStream os;
                ServicioTokenGO servTkGo = new ServicioTokenGO();
                ServicioDocument servDoc = new ServicioDocument();
                try {
                    String valueUtf8NP = bitacoraProyecto + "-" + new String(unidadAdministrativa.getNombreDependencia().getBytes("UTF-8"));
                    Document doc = servDoc.getDocByIdByType(document_id, folioProyecto);
                    TokenGO token = servTkGo.getToken(idusuario);
                    if (token != null && (doc == null || doc.id == 0)) {
                        int idDoc = position + 1;
                        URL url = new URL(goApiURL + "/expedients");
                        conn = (HttpURLConnection) url.openConnection();
                        conn.setDoOutput(true);
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json");
                        conn.setRequestProperty("x-user-token", token.getToken());
                        String input = "{\"expedient\":{\"project_id\":\"" + bitacoraProyecto + "\",\"name\":\"" + new String(template.getBytes("UTF-8")) + " - " + valueUtf8NP
                                    + "\",\"body\":null,\"header\":null,\"footer\":null,\"document_id\":\"" + document_id + "\",\"assignee_id\":null,\"document_web\":\"" + idDoc + "\",\"discussions_attributes\":[]}}";

                        System.out.println("json ---->>> " + input);
                        os = conn.getOutputStream();
                        os.write(input.getBytes());
                        os.flush();
                        System.out.println("os" + os.toString() + " conn msg:>_ " + conn.getResponseMessage() + " to string:>_ " + conn.toString());
                        os.close();
                        System.out.println("POST Response Code :: " + conn.getResponseCode());
                        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                        }
                        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                        String id = "";
                        String output;
                        while ((output = br.readLine()) != null) {
                            JSONParser parser = new JSONParser();
                            JSONObject json = (JSONObject) parser.parse(output);
                            json = (JSONObject) parser.parse(json.get("expedient").toString());
                            System.out.println(json.get("id"));
                            id = json.get("id").toString();
                        }
                        conn.disconnect();

                        URL urlE = new URL(goURL + "/editor/expedient/" + id + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
                        System.out.println("url :: " + urlE.toString());

                        context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP + "')");

                        sotp.updateDesSeleccion(bitacoraProyecto, unidadAdministrativa.getId(), id);
                        
                        listaResultandos = sua.buscarEntidadesOpinionTecnica(bitacoraProyecto);
                        boolean edita;
                        for (UnidadAdministrativa get : listaResultandos) {
                            try {
                                edita = sotp.findExpedient(bitacoraProyecto, get.getId());
                                get.setOficio(edita);
                            }catch (MiaExcepcion ex) {
                                Logger.getLogger(OpinionTecnicaBean.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        
                    } else {
                        if (doc != null) {
                            URL urlE = new URL(goURL + "/editor/expedient/" + doc.getId().longValue() + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
                            System.out.println("url :: " + urlE.toString());
                            context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP+ "')");
                        }
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } catch (MiaExcepcion e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }	
	}
	
        public void editaOficio(){
            try {
                sua = new ServicioUnidadAdministrativa();
                sotp = new ServicioOpinionTecnicaProyecto();
                ////Marcar las dependencias seleccionadas
                Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
                String idActividad = params.get("idSeleccion").toString();
                int position = Integer.parseInt(idActividad);
                System.out.println("Position: " + position);
                UnidadAdministrativa unidadAdministrativa = listaResultandos.get(position);
                
                System.out.println(bitacoraProyecto);
                System.out.println(unidadAdministrativa.getId());
                
                OpinionTecnicaProyecto opinion = sotp.findOficio(bitacoraProyecto, unidadAdministrativa.getId());
                
                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                String goURL = servletContext.getInitParameter("go_url");
                ServicioTokenGO servTkGo = new ServicioTokenGO();
                TokenGO token = servTkGo.getToken(idusuario);
                RequestContext context = RequestContext.getCurrentInstance();
                String valueUtf8NP = bitacoraProyecto + "-" + new String(unidadAdministrativa.getNombreDependencia().getBytes("UTF-8"));
                
                URL urlE = new URL(goURL + "/editor/expedient/" + opinion.getOpiniontOficio() + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
                System.out.println("url :: " + urlE.toString());
                context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP+ "')");
            } catch (MiaExcepcion ex) {
                Logger.getLogger(OpinionTecnicaBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(OpinionTecnicaBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(OpinionTecnicaBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
	public List<UnidadAdministrativa> getListaResultandos() {
		return listaResultandos;
	}

	public void setListaResultandos(List<UnidadAdministrativa> listaResultandos) {
		this.listaResultandos = listaResultandos;
	}

	public String getNombreDependencia() {
		return nombreDependencia;
	}

	public void setNombreDependencia(String nombreDependencia) {
		this.nombreDependencia = nombreDependencia;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getNombbreDestinatario() {
		return nombbreDestinatario;
	}

	public void setNombbreDestinatario(String nombbreDestinatario) {
		this.nombbreDestinatario = nombbreDestinatario;
	}

	public String getTituloModal() {
		return tituloModal;
	}

	public void setTituloModal(String tituloModal) {
		this.tituloModal = tituloModal;
	}

	public List<UnidadAdministrativa> getUnidadesSelecionados() {
		return unidadesSelecionados;
	}

	public void setUnidadesSelecionados(List<UnidadAdministrativa> unidadesSelecionados) {
		this.unidadesSelecionados = unidadesSelecionados;
	}
        
        
            /**
     * @return the bitacoraProyectoUrl
     */
    public String getBitacoraProyectoUrl() {
        String temp = bitacoraProyectoUrl.replaceAll("/", "%2f");
        return temp;
    }

    /**
     * @param bitacoraProyectoUrl the bitacoraProyectoUrl to set
     */
    public void setBitacoraProyectoUrl(String bitacoraProyectoUrl) {
        this.bitacoraProyectoUrl = bitacoraProyectoUrl;
    }

    /**
     * @return the turnado
     */
    public String getTurnado() {
        return turnado;
    }

    /**
     * @param turnado the turnado to set
     */
    public void setTurnado(String turnado) {
        this.turnado = turnado;
    }
	
	
}
