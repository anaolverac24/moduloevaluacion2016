package mx.gob.semarnat.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.MenuDao;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
/**
 *
 * @author Rengerden
 */

@SuppressWarnings("serial")
@ManagedBean(name = "menuView")
@ViewScoped
public class Menuview implements Serializable{
    private static final Logger logger = Logger.getLogger(Menuview.class.getName());
    
    private final MenuDao MenuDao= new MenuDao();
    private final BitacoraDao BitacoraDao = new BitacoraDao();
    private String bitaProyecto;
    private String folioProyecto;
    private short serialProyecto;
    private String cveProyecto;
    private char DirTramReq;
    private String dirEstabl;
    private String Ubicacion;
    private Integer cveTipTram;
    private String fechaFofiIngre;
    private String situaTram;
    private String subsector;
    private String promoTram;
    private String TipoTramite;
    private String img="V.png";
    
    //Indicadores
    private Integer diasPrevNotif;
    private Integer diasPrevProceso;
    private Integer diasInfoNotif;
    private Integer diasInfoProceso;
    
    private List<Historial> HistorialI00010;
    private Integer numHistorialI00010;
    
    @SuppressWarnings("rawtypes")
	public Menuview() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        numHistorialI00010 = 0;
        
//        List<String> roles = (List<String>) sesion.getAttribute("rol");
        String rol =  (String) sesion.getAttribute("rol");
        
//        for (String rol : roles) {
            if (rol.equals("dg")) //Director General
            {  bitaProyecto = params.get("bitaNum").toString();  }
            else
            {  bitaProyecto = context.getExternalContext().getSessionMap().get("bitacoraProy").toString(); }			
//		}       
        
        
        if(context.getExternalContext().getSessionMap().get("userFolioProy") != null)
        {
            folioProyecto = (String) context.getExternalContext().getSessionMap().get("userFolioProy");
            serialProyecto = (Short) context.getExternalContext().getSessionMap().get("userSerialProy");

            System.out.println("folio: " + folioProyecto);
            DirTramReq = MenuDao.DirTramReq(folioProyecto);
            System.out.println("Tramite Requerido: " + DirTramReq);
            promoTram = MenuDao.promTram(folioProyecto);
            System.out.println("NomPromovente: " + promoTram);
            
//            if(promoTram.isEmpty()){
//                promoTram=MenuDao.nomMora(Integer.valueOf(folioProyecto));
//                if(promoTram.isEmpty()){   
//                    promoTram = "No se encontraron datos del promovente";
//                }
//            }
//            else
//            {
//                promoTram = MenuDao.promTram(Integer.valueOf(folioProyecto));
//            }
//            System.out.println("NomPromovente: " + promoTram);
        }
        
        HistorialI00010 = BitacoraDao.numRegHistStatus(bitaProyecto,"I00010");
        if(!HistorialI00010.isEmpty())
        {
            numHistorialI00010 = HistorialI00010.size();
        }
        datosBitacora = BitacoraDao.detalleProyCap(bitaProyecto);
        if(datosBitacora.size()>0)
        {   
           for (Object o : datosBitacora) {               
                idProm = o.toString();
            }
            if(idProm != null)
            {
                promoTraml = BitacoraDao.nombrePromovente(idProm);
                if(promoTraml.size()>0)
                {
                    for (Object o : promoTraml) {               
                        promoTram = o.toString();
                    }
                }
            }
            else
            { promoTram = "No se encontraron datos del promovente"; }                

        }
        else
        { 
        	promoTram = "No se encontraron datos del promovente"; 
        }

        cveProyecto = MenuDao.cveTramite(bitaProyecto);
        context.getExternalContext().getSessionMap().put("cveProyecto", cveProyecto);
        nombProyecto = MenuDao.nombProy(bitaProyecto);
        cveTipTram = MenuDao.cveTipTramBita(bitaProyecto);
        
        System.out.println("\nMENUVIEW");
        System.out.println( "bitaProyecto: " + bitaProyecto);
        System.out.println("NomPromovente: " + promoTram);
        System.out.println("cve: " + cveProyecto);
        
        
        System.out.println("TipoTramite: " + cveTipTram );
        fechaFofiIngre = MenuDao.fechaIng(bitaProyecto);
        System.out.println("FechaIngreso: " + fechaFofiIngre );
        situaTram = MenuDao.situaTram(bitaProyecto);
        System.out.println("SituaTram: " + situaTram);
        subsector = MenuDao.subsecTram(bitaProyecto);
        System.out.println("Subsector: " + subsector);
        
        if(context.getExternalContext().getSessionMap().get("userFolioProy") != null)
        {
            promoTram = MenuDao.promTram(folioProyecto);
            if(promoTram.isEmpty()){
                promoTram=MenuDao.nomMora(folioProyecto);
                if(promoTram.isEmpty()){   
                    promoTram = "No se encontraron datos del promovente";
                }
            }
            System.out.println("NomPromovente: " + promoTram);
        }
        
        diasTramite = MenuDao.diasTramite(bitaProyecto);
        System.out.println("Dias Tamite " + diasTramite);
        diasProceso = MenuDao.diasProceso(bitaProyecto);
        System.out.println("DiasProceso " + diasProceso);
        
        
        diasTramite = MenuDao.diasTramite(bitaProyecto);
        System.out.println("Dias Tamite " + diasTramite);
        diasProceso = MenuDao.diasProceso(bitaProyecto);
        System.out.println("DiasProceso " + diasProceso);
        
//        if(context.getExternalContext().getSessionMap().get("userFolioProy") != null)
//        {
//            promoTram = MenuDao.promTram(Integer.valueOf(folioProyecto));
//            if(promoTram.isEmpty()){
//                promoTram=MenuDao.nomMora(Integer.valueOf(folioProyecto));
//                 if(promoTram.isEmpty()){   
//                    promoTram = "No se encontraron datos del promovente";
//                }
//            }
//            System.out.println("NomPromovente: " + promoTram);
//        }
        
        switch(subsector){
            case "Agropecuario":{
                img="A.jpg";
                break;
            }
            case "Forestal":{
                img="F.jpg";
                break;
            }
            case "Minero":{
                img="M.jpg";
                break;
            }
            case "Pesquero":{
                img="P.jpg";
                break;
            }
            case "Hidraulico":{
                img="H.jpg";
                break;
            }
            case "Industrial":{
                img="I.jpg";
                break;
            }
            case "Comunicaciones":{
                img="V.jpg";
                break;
            }
            case "Desarrollo urbano":{
                img="U.jpg";
                break;
            }
            case "Energia-Electricidad":{
                img="E.jpg";
                break;
            }
            case "Energia-Gaseras":{
                img="G.jpg";
                break;
            }
            case "Energia-Petroleo":{
                img="X.jpg";
                break;
            }
            case "Turismo":{
                img="T.jpg";
                break;
            }
            default:{
                img="IP.jpg";
            break;
            }
        }
                
        
        
        switch(cveTipTram){
        
            case 63:{
                TipoTramite = "IP";
            break;
            }
            case 64:{
                TipoTramite = "MIA-P Mod A";
            break;
            }
            case 65:{
                TipoTramite = "MIA-P Mod B";
            break;
            }
            case 66:{
                TipoTramite = "MIA-R Mod A";
            break;
            }
            case 67:{
                TipoTramite = "MIA-R Mod A";
            break;
            }
            default:{
            TipoTramite = "No Reconocido";
            break;
            }
        }
        
        if(context.getExternalContext().getSessionMap().get("userFolioProy") != null)
        {
            switch(DirTramReq){

                case 'N':{
                    System.out.println("Ubicacion Escrita");
                    Ubicacion = MenuDao.dirUbicDesc(folioProyecto);
                    System.out.println("Direccion: " + Ubicacion);
                    break;                
                }
                case 'S':{
                    System.out.println("Ubicacion Establecida");
                    dirEstabl = MenuDao.dirTramEst(folioProyecto);
                    break;
                }

                default:{
                    System.out.println("Sin domicilio registrado");
                }
            }
        }
        
        //Indicadores visuales
        this.diasInfoNotif = BitacoraDao.diasNotificacion(bitaProyecto, GenericConstants.ID_DIAS_NOTIFICACION_INFO_ADICIONAL, false);
        this.diasInfoProceso = BitacoraDao.diasNotificacion(bitaProyecto, GenericConstants.ID_DIAS_NOTIFICACION_INFO_ADICIONAL, true);
        this.diasPrevNotif = BitacoraDao.diasNotificacion(bitaProyecto, GenericConstants.ID_DIAS_NOTIFICACION_PREVENCION, false);
        this.diasPrevProceso = BitacoraDao.diasNotificacion(bitaProyecto, GenericConstants.ID_DIAS_NOTIFICACION_PREVENCION, true);
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }

    public String getTipoTramite() {
        return TipoTramite;
    }

    public String getPromoTram() {
        return promoTram;
    }

    public void setPromoTram(String promoTram) {
        this.promoTram = promoTram;
    }

    public MenuDao getMenuDao() {
        return MenuDao;
    }

    public BitacoraDao getBitacoraDao() {
        return BitacoraDao;
    }

    public String getBitaProyecto() {
        return bitaProyecto;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public String getCveProyecto() {
        return cveProyecto;
    }

    public String getDirEstabl() {
        return dirEstabl;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public Integer getCveTipTram() {
        return cveTipTram;
    }

    public String getFechaFofiIngre() {
        return fechaFofiIngre;
    }

    public String getSituaTram() {
        return situaTram;
    }

    public char getDirTramReq() {
        return DirTramReq;
    }

    public String getSubsector() {
        return subsector;
    }

    public String getImg() {
        return img;
    }

    private String nombProyecto;
    private BigDecimal diasProceso;
    private Integer diasTramite;
    private List<Object[]> datosBitacora;
    private List<Object[]> promoTraml;
    private String idProm=null;
    private HttpSession sesion = Util.getSession();
 
    


    /**
     * @return the nombProyecto
     */
    public String getNombProyecto() {
        return nombProyecto;
    }

    /**
     * @param nombProyecto the nombProyecto to set
     */
    public void setNombProyecto(String nombProyecto) {
        this.nombProyecto = nombProyecto;
    }

    /**
     * @return the diasProceso
     */
    public BigDecimal getDiasProceso() {
        return diasProceso;
    }

    /**
     * @param diasProceso the diasProceso to set
     */
    public void setDiasProceso(BigDecimal diasProceso) {
        this.diasProceso = diasProceso;
    }

    /**
     * @return the diasTramite
     */
    public Integer getDiasTramite() {
        return diasTramite;
    }

    /**
     * @param diasTramite the diasTramite to set
     */
    public void setDiasTramite(Integer diasTramite) {
        this.diasTramite = diasTramite;
    }

    public Integer getDiasPrevNotif() {
        return diasPrevNotif;
    }

    public void setDiasPrevNotif(Integer diasPrevNotif) {
        this.diasPrevNotif = diasPrevNotif;
    }

    public Integer getDiasPrevProceso() {
        return diasPrevProceso;
    }

    public void setDiasPrevProceso(Integer diasPrevProceso) {
        this.diasPrevProceso = diasPrevProceso;
    }

    public Integer getDiasInfoNotif() {
        return diasInfoNotif;
    }

    public void setDiasInfoNotif(Integer diasInfoNotif) {
        this.diasInfoNotif = diasInfoNotif;
    }

    public Integer getDiasInfoProceso() {
        return diasInfoProceso;
    }

    public void setDiasInfoProceso(Integer diasInfoProceso) {
        this.diasInfoProceso = diasInfoProceso;
    }

    /**
     * @return the numHistorialI00010
     */
    public Integer getNumHistorialI00010() {
        return numHistorialI00010;
    }

    /**
     * @param numHistorialI00010 the numHistorialI00010 to set
     */
    public void setNumHistorialI00010(Integer numHistorialI00010) {
        this.numHistorialI00010 = numHistorialI00010;
    }

}
