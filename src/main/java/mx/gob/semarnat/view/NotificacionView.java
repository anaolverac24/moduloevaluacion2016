/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpSession;

import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.CadenaFirmaDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.PersistenceManager;
import mx.gob.semarnat.dao.PoolEntityManagers;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.dao.procedures.SegundasVias;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.CadenaFirma;
import mx.gob.semarnat.model.bitacora.CadenaFirmaLog;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.dgira_miae.NotificacionGobiernoProyecto;
import mx.gob.semarnat.model.dgira_miae.NotificacionGobiernoProyectoPK;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.secure.AppSession;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import net.firel.BcAgregaCadenaRespuesta;
import net.firel.DetallesFirmado;
import net.firel.Firmas;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.itextpdf.text.DocumentException;
import java.io.File;
import java.util.logging.Level;
import mx.go.semarnat.services.ServicioUnidadAdministrativa;
import mx.go.semarnat.services.UnidadAdministrativa;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;

/**
 * Maneja los metodos y proceso de notificacion a gobierno, como firma y turnado
 * como corrección
 *
 * @author Rodrigo
 */
@ManagedBean(name = "notificacionView")
@ViewScoped
public class NotificacionView {

    private static final Logger logger = Logger.getLogger(NotificacionView.class.getName());
    @SuppressWarnings("unused")
    private final EntityManager em = PersistenceManager.getEmfMIAE().createEntityManager();

    private NotificacionHelper notificacionSelect;
    private final DgiraMiaeDaoGeneral daoDGIRA = new DgiraMiaeDaoGeneral();
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private List<NotificacionHelper> notificacion = new ArrayList<>();
    private List<CatEstatusDocumento> subSectorCat;
    //private final List<CatDependenciaOficio> listaDependencias;
    private String bitaProyecto;

    @ManagedProperty(value = "#{detalleView}")
    private DetalleProyectoView detalleView;
    private String cveTipTram;
    private Integer idTipTram;
    private String edoGestTram;
    private String idArea;
    private Integer idusuario;
    private String comentarioTurnado;

    private String estatusTurnadoDocto;

    private final HttpSession sesion = Util.getSession();

    private List<Historial> HistorialCIS303;
    private Boolean deInfoAdic = false; //bandera que indica si un tramite ya vino de una suspencion por info adicional
    private List<Historial> NoHistorialAT0022;
    private List<Historial> NoHistorialCIS106;
    private List<Historial> NoHistorial200501;

    private List<Object[]> comentariosTab;
    private HashMap<String, List<Object[]>> mapComentarios = new HashMap<String, List<Object[]>>();

    private StreamedContent file;
    private short idEstadoDocto;
    private short sIdDocumento;
    private String idEntidad;

    // variables para turnar opiniones tecnicas por el DG
    private short idDocumento = 0;
    private String tipoDocId = "";
    private short tipoDoc = 0;
    // valor 0 o 1 que indica si esta bitacora (pantalla de checklist) biene de una invocación del DG
    private int isUrlDG = 0;
    // valor 1 o 2 que indica si es la primer bandeja o segunda bandeja del DG ( si no es del dg ni si quiera se trae el valor en los parametros de la url 
    private int bandeja = 1; // por defecto es la bandeja 1
    private final DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
    
    DocumentoRespuestaDgira documento = new DocumentoRespuestaDgira();
    
    private String nombreOfic = "";
    private String urlOfic = "";
    private SegundasVias segundasVias = new SegundasVias();
    private Bitacora bitacora2;
    private SinatDgira sinatDgira;
    private String folio;
    private List<Object[]> maxDocsDgiraFlujoHist = new ArrayList<Object[]>();
    private DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
    private DocumentoDgiraFlujo docsDgiraFlujo;
    private CatEstatusDocumento catEstatusDoc;
    private DocumentoDgiraFlujoHist docsDgiraFlujoHist;
    private int tipoSuspencion = 0;
    private int tipoApertura = 0;
    
    private ServicioUnidadAdministrativa sua = new ServicioUnidadAdministrativa();

    @SuppressWarnings({"unchecked", "rawtypes"})
    public NotificacionView() {
        try {
            logger.debug(Utils.obtenerLogConstructor("Ini"));
            
            System.out.println("\n\nNotificacionView..");
            
            FacesContext fContext = FacesContext.getCurrentInstance();
            ArrayList<Object> aResp;
            
            Tbarea tbarea = (Tbarea) sesion.getAttribute("areaDelegacionUsuario");
            idEntidad = tbarea.getIdentidadfederativa();
            
            // esta informacion tiene valores en la sesion cuando lo invoca el dg, en otros roles no biene en la url por tanto en detalleproyectoview no se cachan los parametros de la url
            isUrlDG = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("isUrlDG").toString());
            bandeja = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("bandeja").toString());
            
            System.out.println("isUrlDG: " + isUrlDG);
            System.out.println("bandeja: " + bandeja);
            
            bitaProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
            cveTipTram = (String) fContext.getExternalContext().getSessionMap().get("cveTramite");
            idTipTram = (Integer) fContext.getExternalContext().getSessionMap().get("idTramite");
            edoGestTram = (String) fContext.getExternalContext().getSessionMap().get("edoGestTramite");
            //listaDependencias = (List<CatDependenciaOficio>) daoDGIRA.listado(CatDependenciaOficio.class);
            //<editor-fold desc="Carga notificaciones" defaultstate="collapsed">
            idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu");
            idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            
            HistorialCIS303 = daoBitacora.numRegHistStatus(bitaProyecto, "CIS303");
            NoHistorialAT0022 = daoBitacora.numRegHistStatus(bitaProyecto, "AT0022");
            NoHistorialCIS106 = daoBitacora.numRegHistStatus(bitaProyecto, "CIS106");
            NoHistorial200501 = daoBitacora.numRegHistStatus(bitaProyecto, "200501");
            if (!NoHistorialAT0022.isEmpty() || !NoHistorialCIS106.isEmpty() || !NoHistorial200501.isEmpty() || HistorialCIS303.size() == 1) //si el tamanio es igual a 1, indica que el tramite ya esta suspendido por info adicional
            {
                deInfoAdic = true;
            } else {
                deInfoAdic = false;
            }
            
            notificacion = new ArrayList();
            String ligaDoctoDG = "";
            boolean editable = true;
            DocumentoDgiraFlujo doctoFlujo;
            String documentoId_ = "";
            
            List<UnidadAdministrativa> proyectoEntidadList = sua.buscarEntidadesNotiicacionGobierno(bitaProyecto);
            
            List <NotificacionGobiernoProyecto> ops = (List<NotificacionGobiernoProyecto>) daoDGIRA.lista_namedQuery("NotificacionGobiernoProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"});
            
            sua.cargarNotificacionGobiernoProyecto(proyectoEntidadList, ops, bitaProyecto);
            
            ops = (List<NotificacionGobiernoProyecto>) daoDGIRA.lista_namedQuery("NotificacionGobiernoProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"});
            int i = 0;
            for (NotificacionGobiernoProyecto op : ops) {
                ligaDoctoDG = "";
                aResp = verificaEstadoDocto(op.getNotificacionGobiernoProyectoPK().getNotificaciongId(), GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS);
                
                if (aResp.get(0).toString().length() > 0) {
                    File oficio = new File((String) aResp.get(0));
                    System.out.println(oficio.exists());
                    if (oficio.exists()) {
                        ligaDoctoDG = (String) aResp.get(0);
                    } else {
                        ligaDoctoDG = "/preview?tipo=oficio&bitaProyecto=" + bitaProyecto 
                                +"&idDocumento=" + op.getNotificacionGobiernoProyectoPK().getNotificaciongId() 
                                + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS;
                    }
                }
                
                editable = (Boolean) aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir
                
                documentoId_ = op.getNotificacionGobiernoProyectoPK().getNotificaciongId();
                
                doctoFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram,
                        GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS, edoGestTram, documentoId_);
                
                if (doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null) {
                    
                    if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO) {
                        
                        ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora=" + bitaProyecto + "&clavetramite=" + cveTipTram + "&idtramite=" + idTipTram + "&tipodoc=" + GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS + "&entidadfederativa=" + edoGestTram + "&documentoid=" + documentoId_;
                        editable = false; // para ocultar los botones de firmar, turnar y corregir
                        
                    } else {
                        if (isUrlDG == 1 && bandeja == 2) { // bandeja 1
                            //editable = false; // para ocultar los botones de firmar, turnar y corregir
                        }
                        
                    }
                }
                
                System.out.println("\nOficio Notificación - documentoId: " + documentoId_);
                System.out.println("Oficio Notificación - ligaDocto: " + ligaDoctoDG);
                System.out.println("Oficio Notificación - editable: " + editable);
                
                String[] gobierno = proyectoEntidadList.get(i).getFundamentoLegalInterno().split("-");
                notificacion.add(new NotificacionHelper(op, Integer.parseInt(op.getNotificacionGobiernoProyectoPK().getNotificaciongId()),
                        ligaDoctoDG, editable, (String) aResp.get(2), (boolean) aResp.get(3), proyectoEntidadList.get(i).getEstadoId(), gobierno[0], proyectoEntidadList.get(i).getMunicipioId(), gobierno[1]));
                i++;
            }
            if (notificacion.isEmpty()) {
                notificacion.add(new NotificacionHelper(new NotificacionGobiernoProyecto(),
                        daoDGIRA.generarIDDoctoRespDGIRA(bitaProyecto, GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS),
                        "", false, "", false, "", "", "", ""));
            }
            
            //Llenado de catalogo de estados de documento
            if (sesion != null) {
                if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
                {
                    subSectorCat = daoDGIRA.getCatStatusDocs();
                }
                if (sesion.getAttribute("rol").toString().equals("da")) //Director de Area
                {
                    subSectorCat = daoDGIRA.getCatStatusDocs2();
                }
                if (sesion.getAttribute("rol").toString().equals("sd")) //Sub Director
                {
                    subSectorCat = daoDGIRA.getCatStatusDocs2();
                }
                if (sesion.getAttribute("rol").toString().equals("eva")) //Evaluador
                {
                    subSectorCat = daoDGIRA.getCatStatusDocs3();
                }
            }
            
            List<Object[]> lstComentarios;
//        comentariosTab = daoBitacora.obtenerObservacionesOficiosTodos(bitaProyecto, GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS);
            comentariosTab = daoBitacora.obtenerObservacionesOficiosTodos(bitaProyecto, GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS, idEntidad);
            for (Object[] obj : comentariosTab) {
                String idDoc = (String) obj[6];
                
                lstComentarios = mapComentarios.get(idDoc);
                if (lstComentarios == null) {
                    lstComentarios = new ArrayList<>();
                }
                lstComentarios.add(obj);
                
                mapComentarios.put(idDoc, lstComentarios);
            }
            
            logger.debug(Utils.obtenerLogConstructor("Fin"));
            //</editor-fold>
        } catch (MiaExcepcion ex) {
            java.util.logging.Logger.getLogger(NotificacionView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private ArrayList<Object> verificaEstadoDocto(String idDocumento, short tipoOficio) {
        DocumentoRespuestaDgira doctoResp;
        DocumentoDgiraFlujo doctoFlujo;
        boolean isEditable;
        String perfilUsr;
        ArrayList<Object> aResp = null;
        String observacion = "";
        boolean isFirmado = false;
        //Turnado
        //Obtiene la liga de acceso al documento en caso de existir
        doctoResp = daoDGIRA.getDocumentotoRespuestaDgira(bitaProyecto,
                idDocumento, tipoOficio);

        doctoFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram,
                tipoOficio, edoGestTram, idDocumento);
        System.out.println(bitaProyecto + "," + cveTipTram + "," + idTipTram + "," + tipoOficio + "," + edoGestTram + "," + idDocumento);

        isEditable = false;
        String perfilEnSesion = sesion.getAttribute("rol").toString();
        String perfilSig;

        if (doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null) {

//			UsuarioRolDAO usuarioRolDAO = new UsuarioRolDAO();
//			List<UsuarioRol> rolesUsuario = new ArrayList<UsuarioRol>();
//			
//        	try {
//        		//Se consulta la lista de roles del usuario que envia.
//				rolesUsuario = usuarioRolDAO.consultarRolesByIdUsuario(doctoFlujo.getIdUsuarioEnvio());
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}        	
            try {
                perfilUsr = doctoFlujo.getRolId().getNombreCorto().trim();
            } catch (NullPointerException exnp) {
                perfilUsr = daoBitacora.getPerfilUsuario(doctoFlujo.getIdUsuarioEnvio());
            }
            if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() != GenericConstants.ESTATUS_DOCTO_FIRMADO) {

                // perfilSig = perfilSiguiente(perfilEnSesion, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId(), rolesUsuario);
                // perfilSig = perfilSiguiente(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                perfilSig = AppSession.siguienteRol(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                isEditable = perfilSig.compareToIgnoreCase(perfilEnSesion) == 0;

                observacion = doctoFlujo.getDocumentoDgiraObservacion() != null ? doctoFlujo.getDocumentoDgiraObservacion() : "";
            } else {
                isFirmado = true;
            }
        } else if (perfilEnSesion.compareToIgnoreCase("eva") == 0) {
            //Por defecto, cuando no tiene flujo comienza por el evaluador
            isEditable = true;
        }

        aResp = new ArrayList<>();
        aResp.add(doctoResp != null ? doctoResp.getDocumentoUrl() : "");//urlDocto
        aResp.add(isEditable);
        aResp.add(observacion);
        aResp.add(isFirmado);

        return aResp;
    }

    @SuppressWarnings("unused")
    private String perfilSiguiente(String perfilActual, short estatusDocto) {
        String perfilSig = "";

        if (perfilActual.compareToIgnoreCase("DG") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "dg";
            }
            if (estatusDocto == 2) {
                perfilSig = "da";
            }
        } else if (perfilActual.compareToIgnoreCase("DA") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "dg";
            }
            if (estatusDocto == 2) {
                perfilSig = "sd";
            }
        } else if (perfilActual.compareToIgnoreCase("SD") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "da";
            }
            if (estatusDocto == 2) {
                perfilSig = "eva";
            }
        } else if (perfilActual.compareToIgnoreCase("JD") == 0 || perfilActual.compareToIgnoreCase("eva") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "sd";
            }
        } else {
            perfilSig = perfilActual;
        }

        return perfilSig;

        // return AppSession.siguienteRol(perfilActual, estatusDocto);
    }

//    /**
//     * Permite realizar el flujo, correcion y firma de un oficio a partir de la lista de roles de un usuario.
//     * @param perfilActual del usuario.
//     * @param estatusDocto que indicia en que situacion esta el documento.
//     * @param usuarioRols con los roles del usuario.
//     * @return la lista de roles del usuario.
//     */
//    private String perfilSiguiente(String perfilActual, short estatusDocto, List<UsuarioRol> usuarioRols) {
//      return AppSession.siguienteRol(perfilActual, estatusDocto, usuarioRols);
//    }     
    @SuppressWarnings("unused")
    public void turnarDocumento() {
//        short sIdDocumento = 0;

        System.out.println("turnaDoctoConsultaPublica");

        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();

        short sTipoDoc = GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS;

        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        Integer idAux = 0;
        short maxIdHisto = 0;
        Bitacora bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial subdirector;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        String folio = null;

        List<Object[]> maxDocsDgiraFlujoHist;
        DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
        DocumentoDgiraFlujo docsDgiraFlujo;
        CatEstatusDocumento catEstatusDoc;
        DocumentoDgiraFlujoHist docsDgiraFlujoHist;
        String observacionesDocDgira = this.comentarioTurnado;

        //bitaProyecto = null;
        if (bitaProyecto != null) {
            try {
                proy = daoV.verificaERA2(bitaProyecto);
                if (proy != null) {
                    if (proy.getProyectoPK() != null) {
                        ctxt.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
                        ctxt.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
                        folio = proy.getProyectoPK().getFolioProyecto();
                    }
                }
            } catch (Exception ar) {
                System.out.println("Problema: " + ar.getMessage());
            }

        }

        if (bitaProyecto != null) {
            //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

            maxDocsDgiraFlujoHist = daoDGIRA.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento));
            for (Object o : maxDocsDgiraFlujoHist) {
                idAux = Integer.parseInt(o.toString());
                maxIdHisto = idAux.shortValue();
            }

            docsDgiraFlujoHistEdit = daoDGIRA.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento), maxIdHisto);

            if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                daoDGIRA.modifica(docsDgiraFlujoHistEdit);
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
            docsDgiraFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento));

            if (docsDgiraFlujo != null) {
                if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                    banderaNew = 0;
                }// registro nuevo
                else {
                    banderaNew = 1;
                }

            } else {
                banderaNew = 1;
            }

            if (banderaNew == 1) {
                docsDgiraFlujo = new DocumentoDgiraFlujo(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento));
            }

            catEstatusDoc = daoDGIRA.idCatEstatusDoc(idEstadoDocto);
            docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
            docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
            docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
            docsDgiraFlujo.setIdAreaRecibe(null);
            docsDgiraFlujo.setIdUsuarioRecibe(null);
            docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
            docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
            docsDgiraFlujo.setDocumentoDgiraObservacion(observacionesDocDgira);

            // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
            UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
            System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
            System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
            System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
            System.out.println("rol     :" + ur.getRolId().getNombreCorto());
            System.out.println("rolId     :" + ur.getRolId().getIdRol());

            // Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
            CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());

            // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
            docsDgiraFlujo.setRolId(rol);

            try {
                if (banderaNew == 1) {
                    System.out.println("banderaNew = 1");
                    daoDGIRA.agrega(docsDgiraFlujo);
                }
                if (banderaNew == 0) {
                    System.out.println("banderaNew = 0");
                    daoDGIRA.modifica(docsDgiraFlujo);
                }

                maxIdHisto = (short) (idAux + 1);
                docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento), maxIdHisto);
                docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                docsDgiraFlujoHist.setDocumentoDgiraObservHist(observacionesDocDgira);
                try {
                    System.out.println("Llamando al metodo agrega de daoDGIRA...");
                    daoDGIRA.agrega(docsDgiraFlujoHist);
                } catch (Exception ex) {
                    System.out.println("Error al llamar al metodo agrega: " + ex.toString());
                    error = error + 1;
                }
            } catch (Exception e) {
                System.out.println("Error al llamar al metodo agrega o modifica: " + e.toString());
                error = error + 1;
            }

            if (error == 0) {
                System.out.println("Errore : 0, continua...");
                ArrayList<Object> aResp = verificaEstadoDocto(Short.toString(sIdDocumento), GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS);
                for (NotificacionHelper otp : notificacion) {
                    if (otp.getId().intValue() == Short.valueOf(sIdDocumento).intValue()) {
                        otp.setEditable((Boolean) aResp.get(1));
                        otp.setLigaDocto((String) aResp.get(0));
                        otp.setFirmado((Boolean) aResp.get(3));
                    }
                }

                System.out.println("Termina turnado de oficio");

                //Comienza procedimiento de segundas vias en otros casos, aqui­ no aplica
                //reqContEnv.execute("alert('Oficio turnado exitosamente')");
                ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));

                //------------------------------------------------------------
            } else {
                System.out.println("Se registro un error!");
                //reqContEnv.execute("alert('El turnado del oficio de falla, intente nuevamente')");
                ctxt.addMessage("growl", new FacesMessage("El turnado del oficio de falló, intente nuevamente."));
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
            this.estatusTurnadoDocto = "0";
            this.comentarioTurnado = "";
        }

    }

    /**
     * *
     * Metodo que realiza un guardado del documento, cambiandole la situacion
     * correspondiente a la firma o corrección
     *
     * @throws IOException
     * @throws DocumentException
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void turnarDocumentoDG() throws IOException, DocumentException {

        System.out.println("\n\ncambioSituacionDG..");
        System.out.println("tipoDoc : " + tipoDoc);
        System.out.println("tipoDocId : " + tipoDocId);
        System.out.println("idDocumento : " + idDocumento);

        //tipoDoc=7;
        suspPorInfoAdic(tipoDoc);

        ArrayList<Object> aResp;
        notificacion = new ArrayList();
        String ligaDoctoDG = "";
        boolean editable = true;
//          DocumentoDgiraFlujo doctoFlujo;
        String documentoId_ = "";
        List<UnidadAdministrativa> proyectoEntidadList = sua.buscarEntidadesNotiicacionGobierno(bitaProyecto);
        int i = 0;
        for (NotificacionGobiernoProyecto op : (List<NotificacionGobiernoProyecto>) daoDGIRA.lista_namedQuery("NotificacionGobiernoProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"})) {

            aResp = verificaEstadoDocto(op.getNotificacionGobiernoProyectoPK().getNotificaciongId(), GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS);
            File oficio = new File((String) aResp.get(0));
            System.out.println(oficio.exists());
            if (oficio.exists()) {
                ligaDoctoDG = (String) aResp.get(0);
            } else {
                ligaDoctoDG = "/preview?tipo=oficio&bitaProyecto=" + bitaProyecto 
                        +"&idDocumento=" + op.getNotificacionGobiernoProyectoPK().getNotificaciongId() 
                        + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS;
            }
            editable = (Boolean) aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir       

            documentoId_ = op.getNotificacionGobiernoProyectoPK().getNotificaciongId();

//            doctoFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS, edoGestTram, documentoId_);
//              if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
//                  
//                  if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
            // al oficio que acabo de turnar le actualizo la liga al pdf y oculto los botones de turnado
            if (tipoDocId.equals(documentoId_)) {
                ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora=" + bitaProyecto + "&clavetramite=" + cveTipTram + "&idtramite=" + idTipTram + "&tipodoc=" + GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS + "&entidadfederativa=" + edoGestTram + "&documentoid=" + documentoId_;
                editable = false; // para ocultar los botones de firmar, turnar y corregir
            }
//                  }
//              }

            System.out.println("\nOficio Notificación - documentoId: " + documentoId_);
            System.out.println("Oficio Notificación - ligaDocto: " + ligaDoctoDG);
            System.out.println("Oficio Notificación - editable: " + editable);

            String[] gobierno = proyectoEntidadList.get(i).getFundamentoLegalInterno().split("-");
            
            notificacion.add(new NotificacionHelper(op, Integer.parseInt(op.getNotificacionGobiernoProyectoPK().getNotificaciongId()),
                    ligaDoctoDG, editable, (String) aResp.get(2), (boolean) aResp.get(3),proyectoEntidadList.get(i).getEstadoId(),gobierno[0],
                    proyectoEntidadList.get(i).getMunicipioId(),gobierno[1]));
            i++;
        }

    }

    @SuppressWarnings("unused")
    public void suspPorInfoAdic(short tipoOfic) {

        System.out.println("suspPorInfoAdic..");
        System.out.println("tipoDocId : " + tipoDocId);
        System.out.println("idDocumento : " + idDocumento);

        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        Integer idAux = 0;
        short maxIdHisto = 0;
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial remitente;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        Historial situacATOO22 = null;
        Historial situacI00010 = null;
        Integer resultado = 0;
        Integer resultVersion = -1;
        tipoDoc = tipoOfic;
        Boolean exitoCambSituac = false;
        //tipoDoc = 4 Oficio de prevencion
        //tipoDoc = 7 Oficio de Información adicional
        Integer soloTurnOfic = -1;
        SegundasViasView segundasViasView = new SegundasViasView("");

        FacesContext ctxt = FacesContext.getCurrentInstance();

        if (bitaProyecto != null) {

            //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------
            maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
            for (Object o : maxDocsDgiraFlujoHist) {
                idAux = Integer.parseInt(o.toString());
                maxIdHisto = idAux.shortValue();
            }

            docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);

            if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                daoDg.modifica(docsDgiraFlujoHistEdit);
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
            docsDgiraFlujo = daoDg.docsFlujoDgira(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);

            if (docsDgiraFlujo != null) {
                if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                    banderaNew = 0;
                }// registro nuevo
                else {
                    banderaNew = 1;
                }

            } else {
                banderaNew = 1;
            }

            if (banderaNew == 1) {
                docsDgiraFlujo = new DocumentoDgiraFlujo(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
            }

            catEstatusDoc = daoDg.idCatEstatusDoc(idDocumento);
            docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
            docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
            docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
            docsDgiraFlujo.setIdAreaRecibe(null);
            docsDgiraFlujo.setIdUsuarioRecibe(null);
            docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
            docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
            docsDgiraFlujo.setDocumentoDgiraObservacion(comentarioTurnado);

            // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
            UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
//            		System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
//            		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
//            		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
//            		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
//            		System.out.println("rolId     :" + ur.getRolId().getIdRol());

            // Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
            CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());

            // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
            docsDgiraFlujo.setRolId(rol);

            try {
                if (banderaNew == 1) {
                    daoDg.agrega(docsDgiraFlujo);
                }
                if (banderaNew == 0) {
                    daoDg.modifica(docsDgiraFlujo);
                }

                maxIdHisto = (short) (idAux + 1);
                docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                docsDgiraFlujoHist.setDocumentoDgiraObservHist(comentarioTurnado);
                try {
                    daoDg.agrega(docsDgiraFlujoHist);
                } catch (Exception ex) {
                    error = error + 1;
                }
            } catch (Exception e) {
                error = error + 1;
            }

            if (error == 0) {
                System.out.println("Terminó turnado de oficio");

                if (idDocumento == 4) //si el estatus del documento es 4=firmado
                {
                    if (tipoDoc == 7)//si es oficio de Información adicional
                    {
                        tipoSuspencion = 1;
                        tipoApertura = 2;
                        System.out.println("Comienza turnado de Oficio de Info. Adic. idDocumento: " + idDocumento);

                        if (bitacora2 != null) {
                            System.out.println("comienza cambios de situación");
                            //Situaciones para colocar tramite en situación de suspencion por Información adicional                                    
                            if (cambioSituacion("AT0031", "I00010", getBitaProyecto()) == 1)//eva-sd  
                            {
                                resultado = resultado + 1;
                                if (cambioSituacion("AT0032", "AT0027", getBitaProyecto()) == 1)//sd-da
                                {
//                                            resultado = resultado + 1;
//                                            if(cambioSituacion("AT0004","dadg") == 1) //da-dg 
//                                            {
                                    resultado = resultado + 1;
                                    if (cambioSituacion("AT0008", "dg", getBitaProyecto()) == 1)//dg-dg 
                                    {
                                        resultado = resultado + 1;
                                        if (cambioSituacion("ATIF01", "dgecc", getBitaProyecto()) == 1)//dg-ecc 
                                        {
                                            resultado = resultado + 1;
                                        }
                                    }
//                                            }
                                }
                            }
                            if (resultado == 4) {
                                try {
                                    if (folio != null) {

                                        //CREACION DE VERSION 3 DE TRAMITE, solo aplica para oficio de Información adicional
                                        //si el resultVersion es 0= bien 1=mal
                                        resultVersion = segundasViasView.procVersiones(folio, (short) 3);

                                        ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                        if (resultVersion == 0) {
                                            exitoCambSituac = true;
                                            System.out.println("Termino ingreso de version 3 de la MIA");
                                        } else {
                                            exitoCambSituac = false;
                                            System.out.println("No Termino ingreso de version 3 de la MIA");
                                        }

                                    } else {
                                        exitoCambSituac = true;
                                    }
                                } catch (Exception ex) {
                                    System.out.println("Problema con inserción de situaciones: " + ex.getMessage());
                                    ctxt.addMessage("growl", new FacesMessage("Trámite no turnado, intentelo mas tarde."));
                                    exitoCambSituac = false;
                                }

                            } else {
                                exitoCambSituac = false;
                            }
                        }
                    }
                    if (tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5)//si es oficio de prevencion (3=pago de derechos  4=requisitos cofemer)
                    {
                        tipoSuspencion = 2;
                        tipoApertura = 1;
                        System.out.println("Comienza turnado de Oficio de prevención idDocumento: " + idDocumento);
                        if (bitacora2 != null) {
                            System.out.println("comienza cambios de situación");
                            if (bitacora2.getBitaTipoIngreso().trim().equals("W")) // tramites electronicos
                            {
                                //es -- por que se turna asi mismo
                                if (cambioSituacion("AT0022", "sdecc", getBitaProyecto()) == 1)//if(cambioSituacion("AT0022","I00008") == 1)
                                {
                                    resultado = resultado + 1;
                                }
                            }
                            if (bitacora2.getBitaTipoIngreso().trim().equals("-")) //tramites fisicos
                            {
                                //es -- por que se turna del SD al ECC
                                if (cambioSituacion("AT0022", "sdecc", getBitaProyecto()) == 1) {
                                    resultado = resultado + 1;
                                }
                            }

                            if (resultado == 1) {
                                exitoCambSituac = true;
                            } else {
                                exitoCambSituac = false;
                            }
                        }
                    }
                    if (tipoDoc == 1 || tipoDoc == 2 || tipoDoc == 8 || tipoDoc == 9 || tipoDoc == 10 || tipoDoc == 11)// estos oficios no van a segundas vias ni afectan el flujo del PEIA
                    {
                        exitoCambSituac = true;
                    }

                    soloTurnOfic = 0;
                } else // para cuando no se trata del firmado del documento
                {
                    soloTurnOfic = 1;
                }

                if (exitoCambSituac) {
                    if (folio != null) {
                        if (tipoDoc == 7 || tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5) //segundas vias solo aplica a oficio de info. adicional y de prevencion
                        {
                            //comienza tramite de segundas vias  
                            try { //1=iNFORMACION ADICIONAL 2=PREEVENCION
                                segundasVias.proc(bitaProyecto, tipoSuspencion, folio, tipoApertura, tipoDoc);
                                ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                System.out.println("Termino de segundas vías");

                            } catch (Exception e) {
                                e.printStackTrace();
                                ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, inténtelo mas tarde."));
                                System.out.println("Problema con segundas vías: " + e.getMessage());
                            }
                        } else {
                            ctxt.addMessage("growl", new FacesMessage("oficio firmado exitosamente"));
                        }
                    } else {
                        ctxt.addMessage("growl", new FacesMessage("Trámite turnado exitosamente."));
                    }
                } else {
                    if (soloTurnOfic == 0) //si se trata de turnado de tramite en esatdo de firma de DG
                    {
                        System.out.println("Problema con cambio de situación de trámite, no se corrio procedimeinto de segundas vías");
                        ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, intentelo mas tarde.Trámite no notificado a promovente."));
                    } else//si se trata de turnado de tramite en esatdo de NO firma de DG
                    {
                        System.out.println("Se completo turnado de oficio (No es situación de firmado de DG)");
                        ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));
                    }
                }
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                // turnaDocsBtn = true; 
            } else {
                ctxt.addMessage("growl", new FacesMessage("El turnado del oficio falló, intente nuevamente."));
            }
        }

    }

    /**
     * *
     * Metodo que realiza un guardado del documento, cambiandole la situacion
     * correspondiente a la firma o corrección
     *
     * @param situacion
     * @param situacAnterior
     * @return
     */
    @SuppressWarnings("unused")
    public Integer cambioSituacion(String situacion, String situacAnterior, String bitacora) {
        //situacion: Situacion a la que se desea cambiar el tramite
        //situacAnterior: Situacion en donde el tramite fue turnado, de aqui se obtiene el idAreaEnvia y idUsuarioEnvio(a quien se le habilitara el tramite cunado SINATEC libere el tramite de prenencion o algun paro de reloj )
        Historial remitente; ///idArea idusuario
        Historial remitente2;
        Integer bandera = 0;
        String idAreaRecibe = "";
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitacora);
        DocumentoDgiraFlujoHist docsDgiraFlujoHist2;

        if (bitacora != null && bitacora2 != null) {
            try {
                if (!situacAnterior.equals("--")) { //02000000
                    if (situacAnterior.equals("dg") || situacAnterior.equals("dadg") || situacAnterior.equals("dgecc") || situacAnterior.equals("sdecc")) {
                        if (situacAnterior.equals("dg")) {
                            if (idArea.length() > 0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                            {
                                idArea = idArea.trim();
                                //idusuario=idusuario;
                                idAreaRecibe = idArea.trim();
                            } else {
                                idArea = "02000000";
                                idusuario = 3943;
                                idAreaRecibe = "02000000";
                            }
                        }
                        if (situacAnterior.equals("dadg")) {
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto, "AT0027");//
                            if (remitente != null) {
                                idArea = remitente.getHistoAreaRecibe();
                                idusuario = remitente.getHistoIdUsuarioRecibe();

                                if (idArea.length() > 0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                {
                                    idAreaRecibe = idArea.trim();
                                } else {
                                    idAreaRecibe = "02000000";
                                }
                            }
                        }
                        if (situacAnterior.equals("dgecc")) {
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto, "CIS104");
                            if (remitente != null) {
                                if (idArea.length() > 0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                {
                                    idArea = idArea.trim();
                                    //idusuario=idusuario;
                                } else {
                                    idArea = "02000000";
                                    idusuario = 3943;
                                }
                                idAreaRecibe = remitente.getHistoAreaEnvio();
                            }
                        }
                        if (situacAnterior.equals("sdecc")) //SD a ECC
                        {
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto, "CIS104");
                            //traer el Id de usuario del subdirector
                            docsDgiraFlujoHist2 = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, (short) 1);

                            if (remitente != null) {
                                idArea = docsDgiraFlujoHist2.getIdAreaEnvioHist();
                                idusuario = docsDgiraFlujoHist2.getIdUsuarioEnvioHist();
                                idAreaRecibe = remitente.getHistoAreaEnvio();
                            }
                        }

                    } else {
                        remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto, situacAnterior);
                        if (remitente != null) {
                            idArea = remitente.getHistoAreaRecibe();
                            if (remitente.getHistoIdUsuarioRecibe() != null) {
                                idusuario = remitente.getHistoIdUsuarioRecibe();
                            }
                            idAreaRecibe = remitente.getHistoAreaEnvio();
                        }
                    }
                } else {
                    idAreaRecibe = idArea;
                }//para situaciones AT0002, AT0003 y AT0011
                //FacesMessage message=null;                   
                sinatDgira = new SinatDgira();
                sinatDgira.setId(0);
                sinatDgira.setSituacion(situacion);
                sinatDgira.setIdEntidadFederativa(bitacora2.getBitaEntidadGestion());
                sinatDgira.setIdClaveTramite(bitacora2.getBitaTipotram()); //STRING BITA_TIPOTRAM
                sinatDgira.setIdTramite(bitacora2.getBitaIdTramite());
                sinatDgira.setIdAreaEnvio(idArea.trim());
                sinatDgira.setIdAreaRecibe(idAreaRecibe.trim());
                sinatDgira.setIdUsuarioEnvio(idusuario);
                //sinatDgira.setIdUsuarioRecibe(1340);
                sinatDgira.setGrupoTrabajo(new Short("0"));
                sinatDgira.setBitacora(bitacora);
                sinatDgira.setFecha(new Date());

                daoBitacora.agrega(sinatDgira);

                //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite turnado correctamente.", null);                    
                System.out.println("Terminó turnado exitosamente, situación: " + situacion);
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                bandera = 1;
            } catch (Exception er) {
                bandera = 0;
                er.printStackTrace();
                //FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido turnar el tramite, intente mas tarde.", null);
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                System.out.println("Error en turnado a situación " + situacion + " de trámite :  " + er.getMessage());
            }
        } else {
            bandera = 0;
        }
        return bandera;
    }

    //<editor-fold defaultstate="collapsed" desc="Agregar Opinion.">
    /**
     * Agrega un nuevo elemento a la tabla
     */
    public void agregarNotificacion() {
        if (!notificacion.isEmpty()) {
            int i = notificacion.get(notificacion.size() - 1).getId();
            i++;
            notificacion.add(new NotificacionHelper(new NotificacionGobiernoProyecto(), i));
        } else {
            notificacion.add(new NotificacionHelper(new NotificacionGobiernoProyecto(),
                    daoDGIRA.generarIDDoctoRespDGIRA(bitaProyecto, GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS)));

        }
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Quita registro de opinion">
    /**
     * Elimina un elemento de la tabla
     */
    public void quitarNotificacion() {
        System.out.println("idQuitar: " + notificacionSelect.id);
        System.out.println("quita: " + notificacion.remove(notificacionSelect));
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Guardar registros">
    /**
     * guarda los elementos capturados de la tabla
     */
    @SuppressWarnings("unchecked")
    public void guardarNotificacion() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        FacesContext ctxt = FacesContext.getCurrentInstance();
        Integer noGuard = 0;
        ArrayList<Object> aResp;

        for (NotificacionGobiernoProyecto otp : (List<NotificacionGobiernoProyecto>) daoDGIRA.lista_namedQuery("NotificacionGobiernoProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"})) {
            daoDGIRA.elimina(NotificacionGobiernoProyecto.class, otp.getNotificacionGobiernoProyectoPK());
        }
        for (NotificacionHelper otp : notificacion) {
            otp.notificacion.setNotificacionGobiernoProyectoPK(new NotificacionGobiernoProyectoPK(bitaProyecto, otp.id.toString()));
            try {
                daoDGIRA.agrega(otp.notificacion);
                noGuard = 1;
            } catch (Exception e) {
                noGuard = 0;
            }

            aResp = verificaEstadoDocto(otp.getNotificacion().getNotificacionGobiernoProyectoPK().getNotificaciongId(),
                    GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS);
            File oficio = new File((String) aResp.get(0));
                    System.out.println(oficio.exists());
                    if (oficio.exists()) {
                        otp.setLigaDocto((String) aResp.get(0));
                    } else {
                        otp.setLigaDocto("/preview?tipo=oficio&bitaProyecto=" + bitaProyecto 
                                +"&idDocumento=" + otp.getNotificacion().getNotificacionGobiernoProyectoPK().getNotificaciongId() 
                                + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS);
                    }
            
            otp.setEditable((Boolean) aResp.get(1));
            otp.setFirmado((Boolean) aResp.get(3));

        }
        if (noGuard == 1) {
            ctxt.addMessage("growl", new FacesMessage("Notificación a gobierno fue guardada exitosamente."));
        }

        //TODO
        detalleView.setBanderaNot(1);
        detalleView.opinionTecnicaCompleta();
        if (detalleView.getActiveTab() == 0) {
            reqcontEnv.execute("alert('Es requerido llenar toda la informacion para poder turnar, verifique su información e intente nuevamente.');");
        }
        reqcontEnv.execute("PF('myTabView').select(" + detalleView.getActiveTab() + ");");

    }//</editor-fold>

    /**
     * Descarga el jar para firma y registra la cadena firma si no existe o en
     * su caso muestra el token al usuario
     */
    @SuppressWarnings("unused")
    public void siFirmar(int idDocumento) {
        this.sIdDocumento = (short) idDocumento;
        System.out.println("\n\nsi firmar: " + idDocumento);
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/resources/SignerClientAll.jar");
        file = new DefaultStreamedContent(stream, "application/java-archive", "SignerClientAll.jar");

        FacesContext fContext = FacesContext.getCurrentInstance();
        String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
        String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
        String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
        int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
        // String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
        tipoDocId = String.valueOf(sIdDocumento);
        tipoDoc = GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS;

        System.out.println("-------------------------------------------------------------------------");
        System.out.println("folio :" + folio);
        System.out.println("claveProyecto :" + claveProyecto);
        System.out.println("bitacora :" + bitacora);
        System.out.println("claveTramite :" + claveTramite);
        System.out.println("idTramite :" + idTramite);
        System.out.println("tipoDocId :" + tipoDoc);
        System.out.println("documentoId : " + tipoDocId);
        System.out.println("edoGestTramite : " + edoGestTram);
        System.out.println("rol usuario : " + fContext.getExternalContext().getSessionMap().get("rol")); //sesion.getAttribute("rol").toString());
        System.out.println("idAreaUsu : " + fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
        System.out.println("idUsu : " + fContext.getExternalContext().getSessionMap().get("idUsu"));
        System.out.println("-------------------------------------------------------------------------");

        // busca si ya existe el registro de cadena firma...
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS, tipoDocId);

        String msg_numero_token = Utils.obtenerConstante(GenericConstants.FIRMAR_NUMERO_TOKEN);
        String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.FIRMAR_NO_REGISTRO_CADENAFIRMA);

        if (firma != null) {

            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));

        } else {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            Date thisDate = new Date();
//	        String fecha = dateFormat.format(thisDate);
//	        System.out.println("fecha :" + fecha);

            String descripcion_documento = "Notificación a Gobierno";
            System.out.println("descripcion :" + descripcion_documento);
//	        String destinatario = "Destinatario nuevo";
//	        System.out.println("destinatario :" + destinatario);
//	        String iniciales = "LDSM/ASR/AVA/GBA/JCTT";
//	        System.out.println("iniciales :" + iniciales);

            String infoaFirmar = "||" + bitacora + "|" + descripcion_documento + "||";
            String infoaFirmarB64 = Base64.encodeBase64String(infoaFirmar.getBytes());
            System.out.println("encodeB64 :" + infoaFirmarB64);

            System.out.println("\n\nagregaCadena..");
            CadenaFirmaDao cadenaDao = new CadenaFirmaDao();
            BcAgregaCadenaRespuesta agregacadena = null;

            try {
                agregacadena = cadenaDao.agregaCadena(folio, infoaFirmarB64);

                if (agregacadena != null) {

                    firma = new CadenaFirma();

                    // firma.setClave(clave);
                    firma.setClaveProyecto(claveProyecto);
                    firma.setBitacora(bitacora);
                    firma.setClaveTramite2(cveTipTram);
                    firma.setIdTramite2(idTipTram);
                    firma.setTipoDocId(tipoDoc);
                    firma.setDocumentoId(tipoDocId);

                    firma.setFolioProyecto(folio);
                    firma.setB64(infoaFirmarB64);
                    firma.setIdentificador(agregacadena.getIdentificador());
                    firma.setOperacion(String.valueOf(agregacadena.getTransferenciaOperacion()));

                    new CadenaFirmaDao().agrega(firma);

                    // despues de agregar la cadena de firma, se consulta para mostrar el identificador al usuario..
                    firma = null;
                    // firmaDao = new CadenaFirmaDao();
                    // firma = getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);					 
                    firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);

                    if (firma != null) {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
                    } else {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
                    }

                } else {

                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
                }
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
            }
        }
    }

    /**
     * *
     * Metodo que realiza el turnado del oficio recientemente firmado
     *
     * @throws Exception
     */
    public void turnarOficio(int idDocumento_) {
        this.sIdDocumento = (short) idDocumento_;
        System.out.println("\n\nturna oficio Notificacion a gobierno..");

        FacesContext fContext = FacesContext.getCurrentInstance();
        String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
        String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
        String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
        int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
        // String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
        tipoDocId = String.valueOf(sIdDocumento);
        tipoDoc = GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS;
        String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();

        System.out.println("-------------------------------------------------------------------------");
        System.out.println("folio :" + folio);
        System.out.println("claveProyecto :" + claveProyecto);
        // System.out.println(fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
        // System.out.println(fContext.getExternalContext().getSessionMap().get("idUsu"));
        System.out.println("bitacora :" + bitacora);
        System.out.println("claveTramite :" + claveTramite);
        System.out.println("idTramite :" + idTramite);
        System.out.println("tipoDocId :" + tipoDoc);
        System.out.println("documentoId : " + tipoDocId);
        System.out.println("edoGestTramite : " + edoGestTram);
        System.out.println("rol : " + rol);
        System.out.println("-------------------------------------------------------------------------");

        // esta variable es como el idDocumento pero solo se usa para turnar cuando no es el DG
        idEstadoDocto = GenericConstants.ESTATUS_DOCTO_AUTORIZADO;

        if (isUrlDG == 1) {
            idDocumento = GenericConstants.ESTATUS_DOCTO_FIRMADO; // 4 firmado
        } else {
            idDocumento = GenericConstants.ESTATUS_DOCTO_AUTORIZADO; // 3 autorizado ( turnar )
        }
        // url que apunta al oficio dentro del folio( carpeta ). para este caso sera para ponerle esta url al codigo qr
        String ligaQR = Utils.ruta_liga_codigoqr + bitaProyecto;

        List<DocumentoRespuestaDgira> lista = daoDg.docsRespDgiraBusq2(bitacora, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
        for (DocumentoRespuestaDgira x : lista) {
            documento = x;
//            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
//            urlOfic = x.getDocumentoUrl();
        }

        System.out.println("idDocumento : " + idDocumento);
        System.out.println("urlOfic : " + urlOfic);
        System.out.println("nombreOfic : " + nombreOfic);
        System.out.println("ligaQR : " + ligaQR);
        System.out.println("-------------------------------------------------------------------------");

        String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
        String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
        String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
        String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);

        // busca si ya existe el registro de cadena firma...		
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        CadenaFirma cadenafirma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);

        if (cadenafirma == null) {
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_hay_token_registrado));
            return;
        }

        CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenafirma, rol);

        DetallesFirmado detallefirmado = null;
        try {

            detallefirmado = firmaDao.getFirmas(cadenafirma);

            if (detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() == 98)) {
                System.out.println("\nno responde el servicio web o no hay firmas...");
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
                return;
            } else if (detallefirmado != null && detallefirmado.getEstado() == 0 && (detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)) {

                // cadenafirmaLog puede ser null
                // detallefirmado si trae firmas del ws
                Firmas firmaws = firmaDao.GetFirmaWS(cadenafirma, detallefirmado);

                // para este caso si no hay firma del ws decimos que no ha firmado el usuario...
                if (firmaws == null) {
                    System.out.println("ultima firma válida : null");
                    System.out.println("\nel usuario no ha firmado...");
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
                    return;
                } else {
                    System.out.println("ultima firma válida : " + firmaws.getRfc() + " - " + firmaws.getFecha());
                    System.out.println("el usuario ya firmó y se guardará en cadenafirmalog...");
                    // SE ASUME QUE SI FIRMÓ YA por tanto primero agregamos el registro de cadenafirmalog correspondiente...
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
                    Date fcha = formatter.parse(firmaws.getFecha());
                    boolean resp = false;

                    if (cadenafirmaLog == null) {

                        CadenaFirmaLog firmaLog = new CadenaFirmaLog();
                        firmaLog.setCadenaFirma(cadenafirma);
                        firmaLog.setEstatus((short) 1);
                        firmaLog.setRol(rol);
                        firmaLog.setFecha(fcha);
                        firmaLog.setFirma(firmaws.getFirma());
                        firmaLog.setRfc(firmaws.getRfc());

                        resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
                        System.out.println("firmalog agregada: " + resp);

                    } else {
                        // se actualiza cadenafirmalog del rol en sesion
                        cadenafirmaLog.setRfc(firmaws.getRfc());
                        cadenafirmaLog.setFecha(fcha);
                        cadenafirmaLog.setFirma(firmaws.getFirma());
                        resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
                        System.out.println("firmalog modificada: " + resp);

                    }

                    if (resp == true) {

                        // y mandamos turnar...
                        if (idDocumento == GenericConstants.ESTATUS_DOCTO_AUTORIZADO) {

                            turnarDocumento();

                        } else {
                            //documento.setDocumentoUbicacion(urlOfic);
                            //documento.setDocumentoNombre(nombreOfic);
                            firmaDao.editarPdf(documento, cadenafirma, ligaQR);
                            firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(), cadenafirma.getFolioProyecto());
                            turnarDocumentoDG();
                        }

                    } else {
                        throw new Exception("Error: al guardar el log de la firma para el rol actual");
                    }
                }

            } else {
                // pudiera ser un error -99 ( no hay conexion con el servicio buscriptográfico )
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_se_realizo_turnado));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
        }
    }

    /**
     * *
     * Metodo que realiza la correccion del oficio como tal
     */
    public void corrigeOficio(int idDocumento) {
        // this.sIdDocumento = (short) idDocumento;
        System.out.println("\n\ncorrige oficio Notificacion a gobierno..: " + this.sIdDocumento);
        System.out.println("comentarios : " + comentarioTurnado);
        idEstadoDocto = GenericConstants.ESTATUS_DOCTO_CORRECCION; // a correccion

        try {
            turnarDocumento();

            System.out.println("\nActual ROL con que el usuario entró a la bitácora : " + AppSession.GetRolSeleccionado());
            ArrayList<UsuarioRol> lista = AppSession.GetListaRolesObject();
            System.out.println("\ninformación de lista de roles del usuario:");
            for (UsuarioRol obj : lista) {

                System.out.println("usuario :" + obj.getUsuarioId().getIdusuario());
                System.out.println("idArea  :" + obj.getUsuarioId().getIdarea().trim());
                System.out.println("rol     :" + obj.getRolId().getNombreCorto());
                System.out.println("rolId     :" + obj.getRolId().getIdRol());
                System.out.println("-----------");

            }

            UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
            System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
            System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
            System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
            System.out.println("rol     :" + ur.getRolId().getNombreCorto());
            System.out.println("rolId     :" + ur.getRolId().getIdRol());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * *
     * este metodo se utiliza como auxiliar ( para setear la propiedad
     * sIdDocumento ) ya que cuando guardas el dialogo de comentarios de
     * corrección no esta mandando el id. En cambio cuando recien le cliclo
     * "corregir" y decir que si quiero corregir, si puedo mandar el id del
     * registro actual Despues se invoca "corrigeOficio" ya con el valor seteado
     * y no ocupa el que le esta mandando porq en realidad va con 0 siempre
     *
     * @param idDocumento
     */
    public void setearactualid(int idDocumento) {

        this.sIdDocumento = (short) idDocumento;
        System.out.println("\n\nsetearactualid : " + this.sIdDocumento);
    }

    /**
     * Descarga el file
     *
     * @return
     */
    public StreamedContent getFile() {
        System.out.println("Entro a notificaciones");
        return file;
    }

    //<editor-fold defaultstate="collapsed" desc="getters&setters">
    public void setNotificacion(List<NotificacionHelper> notificacion) {
        this.notificacion = notificacion;
    }

    public NotificacionHelper getNotificacionSelect() {
        return notificacionSelect;
    }

    public void setNotificacionSelect(NotificacionHelper notificacionSelect) {
        this.notificacionSelect = notificacionSelect;
    }

    public List<NotificacionHelper> getNotificacion() {
        return notificacion;
    }

////    public List<CatDependenciaOficio> getListaDependencias() {
////        return listaDependencias;
////    }
    public String getBitaProyecto() {
        return bitaProyecto;
    }

    /**
     * @return the cveTipTram
     */
    public String getCveTipTram() {
        return cveTipTram;
    }

    /**
     * @param cveTipTram the cveTipTram to set
     */
    public void setCveTipTram(String cveTipTram) {
        this.cveTipTram = cveTipTram;
    }

    /**
     * @return the idTipTram
     */
    public Integer getIdTipTram() {
        return idTipTram;
    }

    /**
     * @param idTipTram the idTipTram to set
     */
    public void setIdTipTram(Integer idTipTram) {
        this.idTipTram = idTipTram;
    }

    /**
     * @return the edoGestTram
     */
    public String getEdoGestTram() {
        return edoGestTram;
    }

    /**
     * @param edoGestTram the edoGestTram to set
     */
    public void setEdoGestTram(String edoGestTram) {
        this.edoGestTram = edoGestTram;
    }

    /**
     * @return the deInfoAdic
     */
    public Boolean getDeInfoAdic() {
        return deInfoAdic;
    }

    /**
     * @param deInfoAdic the deInfoAdic to set
     */
    public void setDeInfoAdic(Boolean deInfoAdic) {
        this.deInfoAdic = deInfoAdic;
    }

    public List<CatEstatusDocumento> getSubSectorCat() {
        return subSectorCat;
    }

    public void setSubSectorCat(List<CatEstatusDocumento> subSectorCat) {
        this.subSectorCat = subSectorCat;
    }

    public String getEstatusTurnadoDocto() {
        return estatusTurnadoDocto;
    }

    public void setEstatusTurnadoDocto(String estatusTurnadoDocto) {
        this.estatusTurnadoDocto = estatusTurnadoDocto;
    }

    public void setEstatusTurnadoDocto() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");

        this.estatusTurnadoDocto = strIdEstadoDocto;
    }

    public String getComentarioTurnado() {
        return comentarioTurnado;
    }

    public void setComentarioTurnado(String comentarioTurnado) {
        this.comentarioTurnado = comentarioTurnado;
    }

    public void setComentarioTurnado() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        String strComentarios = ctxt.getExternalContext().getRequestParameterMap().get("comentTurnado");

        this.comentarioTurnado = strComentarios;
    }

    public List<Object[]> getComentariosTab() {
        return comentariosTab;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Clase OpinionTecnicaHelper">
    /**
     * Clase de apoyo para la opcion de "Otra" en las dependencias
     */
    public class NotificacionHelper {

        private NotificacionGobiernoProyecto notificacion;
        private Boolean otra = false;
        private Integer id;
        private String observacion;

        private String estatusDocto;
        private String ligaDocto;
        private boolean editable; //Indica si el documento se puede editar, de acuerdo al perfil del usuario y estatus del docto
        private boolean firmado;
        
        private String nombreEstado;
        private String estadoId;
        
        private String nombreMunicipio;
        private String municipioId;

        public NotificacionHelper(NotificacionGobiernoProyecto opinion, Integer id) {
            this.notificacion = opinion;
            //this.otra = otra;
            this.id = id;
        }

        public NotificacionHelper(NotificacionGobiernoProyecto opinion, Integer id, String ligaDocto, Boolean editable,
                String observacion, boolean firmado, String estadoId, String nombreEstado, String municipioId, String nombreMunicipio) {
            this.notificacion = opinion;
            this.id = id;
            this.observacion = observacion;
            this.ligaDocto = ligaDocto;
            this.editable = editable;
            this.firmado = firmado;
            this.nombreEstado = nombreEstado;
            this.estadoId = estadoId;
            this.nombreMunicipio = nombreMunicipio;
            this.municipioId = municipioId;
        }

        public NotificacionGobiernoProyecto getNotificacion() {
            return notificacion;
        }

        public void setNotificacion(NotificacionGobiernoProyecto notificacion) {
            this.notificacion = notificacion;
        }

        public Boolean getOtra() {
            if (notificacion != null) {
                if (notificacion.getCatDependenciaId() != null) {
                    otra = notificacion.getCatDependenciaId().getCatDependenciaId().equals(new Short("9999"));
                }
            } else {
                otra = false;
            }
            return otra;
        }

        public void setOtra(Boolean otra) {
            this.otra = otra;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEstatusDocto() {
            return estatusDocto;
        }

        public void setEstatusDocto() {
            FacesContext ctxt = FacesContext.getCurrentInstance();
            String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");

            this.estatusDocto = strIdEstadoDocto;
        }

        public void setEstatusDocto(String estatusDocto) {
            this.estatusDocto = estatusDocto;
        }

        public String getLigaDocto() {
            return ligaDocto;
        }

        public void setLigaDocto(String ligaDocto) {
            this.ligaDocto = ligaDocto;
        }

        public boolean isEditable() {
            return editable;
        }

        public void setEditable(boolean editable) {
            this.editable = editable;
        }

        public String getObservacion() {
            return observacion;
        }

        public void setObservacion(String observacion) {
            this.observacion = observacion;
        }

        public boolean isFirmado() {
            return firmado;
        }

        public void setFirmado(boolean firmado) {
            this.firmado = firmado;
        }

        /**
         * @return the nombreEstado
         */
        public String getNombreEstado() {
            return nombreEstado;
        }

        /**
         * @param nombreEstado the nombreEstado to set
         */
        public void setNombreEstado(String nombreEstado) {
            this.nombreEstado = nombreEstado;
        }

        /**
         * @return the estadoId
         */
        public String getEstadoId() {
            return estadoId;
        }

        /**
         * @param estadoId the estadoId to set
         */
        public void setEstadoId(String estadoId) {
            this.estadoId = estadoId;
        }

        /**
         * @return the nombreMunicipio
         */
        public String getNombreMunicipio() {
            return nombreMunicipio;
        }

        /**
         * @param nombreMunicipio the nombreMunicipio to set
         */
        public void setNombreMunicipio(String nombreMunicipio) {
            this.nombreMunicipio = nombreMunicipio;
        }

        /**
         * @return the municipioId
         */
        public String getMunicipioId() {
            return municipioId;
        }

        /**
         * @param municipioId the municipioId to set
         */
        public void setMunicipioId(String municipioId) {
            this.municipioId = municipioId;
        }
    }
    //</editor-fold>

    @SuppressWarnings("unused")
    public void muestraObservaciones() {
        String idDocumento, strTipoDocto;
        short sTipoDocto;
        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();

        try {
            idDocumento = ctxt.getExternalContext().getRequestParameterMap().get("idDocumento");
            sTipoDocto = GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS;
            //comentariosTab = daoBitacora.obtenerObservacionesOficios(bitaProyecto, sTipoDocto, idDocumento);
            comentariosTab = mapComentarios.get(idDocumento);

        } catch (Exception e) {
            reqContEnv.execute("alert('Ocurrio un error durante la obtencion de las observaciones, intente nuevamente')");
        }
    }

    public short getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(short idDocumento) {
        this.idDocumento = idDocumento;
    }

    public DetalleProyectoView getDetalleView() {
        return detalleView;
    }

    public void setDetalleView(DetalleProyectoView detalleView) {
        this.detalleView = detalleView;
    }

}
