/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.itextpdf.text.DocumentException;

import mx.go.semarnat.services.ServicioUnidadAdministrativa;
import mx.go.semarnat.services.UnidadAdministrativa;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.CadenaFirmaDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.PersistenceManager;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.dao.procedures.SegundasVias;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.CadenaFirma;
import mx.gob.semarnat.model.bitacora.CadenaFirmaLog;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.catalogos.CatTratamientos;
import mx.gob.semarnat.model.catalogos.DelegacionMunicipio;
import mx.gob.semarnat.model.catalogos.EntidadFederativa;
import mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
//import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.dgira_miae.OpinionTecnicaProyecto;
import mx.gob.semarnat.model.dgira_miae.OpinionTecnicaProyectoPK;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.secure.AppSession;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import net.firel.BcAgregaCadenaRespuesta;
import net.firel.DetallesFirmado;
import net.firel.Firmas;

/**
 *  Maneja todos los flujos de opinion tecnica como firma, turnado y corrección
 * @author Rodrigo
 */
@ManagedBean(name = "opinionTecnica")
@ViewScoped
public class OpinionTecnicaView {

    private static final Logger logger = Logger.getLogger(OpinionTecnicaView.class.getName());
    @SuppressWarnings("unused")
	private final EntityManager em = PersistenceManager.getEmfMIAE().createEntityManager();
    private OpinionTecnicaHelper opinionTecnicaSelect;
    private final DgiraMiaeDaoGeneral daoDGIRA = new DgiraMiaeDaoGeneral();
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<OpinionTecnicaHelper> opinionTecnica = new ArrayList();
    private List<UnidadAdministrativa> listaDependencias;
    
    private List<SelectItem> itemsDependencias = new ArrayList<>();
    
    private String bitaProyecto;
    private List<Object[]> docsOpTec = new ArrayList<Object[]>();
    private String cveProyecto;
    private BitacoraDao dao = new BitacoraDao();
    private OpinionTecnicaProyecto opinion2;
    private List<Object[]> fechDocum;
    @ManagedProperty ( value = "#{detalleView}")
    private DetalleProyectoView detalleView;
    private String cveTipTram;
    private Integer idTipTram;
    private String edoGestTram;
    private String idArea;
    private Integer idusuario;

    private List<Object[]> documentoRespuestaDgira = new ArrayList<Object[]>();
    private Boolean turnaDocs = false;
    private Boolean NOturnaDocs = false;
    private List<CatEstatusDocumento> subSectorCat; //
    private DocumentoDgiraFlujoHist docsDgiraFlujoHist;
    private final HttpSession sesion = Util.getSession();
    private short idDocumento = 0;
    private String areaEnvio = null;
    private String estatusTurnadoDoctoOT;
    private String estatusTurnadoDocto;
    private String comentarioTurnado;

    private List<Historial> HistorialCIS303;
    private Boolean deInfoAdic = false; //bandera que indica si un tramite ya vino de una suspencion por info adicional
    private List<Historial> NoHistorialAT0022;
    private List<Historial> NoHistorialCIS106;
    private List<Historial> NoHistorial200501;
    private List<Historial> NoHistorialIRA020;
    private List<Historial> NoHistorialATIF01;
    private String nombTipoOfic = "";
    
    private short sIdDocumento;
    private StreamedContent fileOpinion;
    private short idEstadoDocto;

    private List<Object[]> comentariosTab;
    private HashMap<String, List<Object[]>> mapComentarios = new HashMap<String, List<Object[]>>();
	private String idEntidad;
	private Tbarea tbarea;


	// variables para turnar opiniones tecnicas por el DG
	private String tipoDocId = "";
    private short tipoDoc = 0;
    // valor 0 o 1 que indica si esta bitacora (pantalla de checklist) biene de una invocación del DG
    private int isUrlDG = 0;
    // valor 1 o 2 que indica si es la primer bandeja o segunda bandeja del DG ( si no es del dg ni si quiera se trae el valor en los parametros de la url 
    private int bandeja = 1; // por defecto es la bandeja 1
    private final DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
    
    DocumentoRespuestaDgira documento = new DocumentoRespuestaDgira();
    
    private String nombreOfic=""; 
    private String urlOfic="";
    private String ubicacionOfic = "";
    private SegundasVias segundasVias = new SegundasVias();
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private Bitacora bitacora2;
    private SinatDgira sinatDgira;
    private String folio;
    
    private ServicioUnidadAdministrativa sua;
    private UnidadAdministrativa unidadSeleccionado = new UnidadAdministrativa();
    private List<EntidadFederativa> listEstados;
    private List<DelegacionMunicipio> listMunicipios;
    private List<CatTratamientos> listTratamientos;
    private List<SelectItem> itemsEstados;
    private List<SelectItem> itemsMunicipios;
    private List<SelectItem> itemsTratamientos;
    
    /**
     * Constructor: precarga los elementos guardados
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public OpinionTecnicaView() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        System.out.println("\n\nOpinionTecnicaView..");

        FacesContext fContext = FacesContext.getCurrentInstance();
        ArrayList<Object> aResp;

        tbarea = (Tbarea) sesion.getAttribute("areaDelegacionUsuario");
        idEntidad = tbarea.getIdentidadfederativa();
        
     // esta informacion tiene valores en la sesion cuando lo invoca el dg, en otros roles no biene en la url por tanto en detalleproyectoview no se cachan los parametros de la url
        isUrlDG = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("isUrlDG").toString());
        bandeja = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("bandeja").toString());
        
        System.out.println("isUrlDG: " + isUrlDG);
        System.out.println("bandeja: " + bandeja);
        ServicioUnidadAdministrativa sua = new ServicioUnidadAdministrativa();
        bitaProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
         
        DgiraMiaeDaoGeneral miaeDao = new DgiraMiaeDaoGeneral();
        listEstados = miaeDao.getEstados();	
        itemsEstados = new ArrayList<>();
        for (EntidadFederativa entidadFederativa : listEstados) {
            SelectItem name = new SelectItem(entidadFederativa.getIdEntidadFederativa(), entidadFederativa.getNombreEntidadFederativa());
            itemsEstados.add(name);
        }
        
        listTratamientos = miaeDao.getTratamientos();
                itemsTratamientos = new ArrayList<>();
                for (CatTratamientos tratamientos : listTratamientos) {
                    SelectItem name = new SelectItem(tratamientos.getId(), tratamientos.getDegree());
                    getItemsTratamientos().add(name);
                }
        
        cargaListDependencias();
        
        cveTipTram = (String) fContext.getExternalContext().getSessionMap().get("cveTramite");
        idTipTram = (Integer) fContext.getExternalContext().getSessionMap().get("idTramite");
        edoGestTram = (String) fContext.getExternalContext().getSessionMap().get("edoGestTramite");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu");
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");

        cveProyecto = dao.cveTramite(bitaProyecto);
        if (cveProyecto != null) {
//            docsOpTec = dao.consultaPublica(cveProyecto);
        	  docsOpTec = dao.consultaPublica(cveProyecto, idEntidad);
        }  //documentos y oficios  

        String ofcResp = "";
        Date ofcFechResp = null;

        HistorialCIS303 = dao.numRegHistStatus(bitaProyecto, "CIS303");
        NoHistorialAT0022 = dao.numRegHistStatus(bitaProyecto, "AT0022");
        NoHistorialCIS106 = dao.numRegHistStatus(bitaProyecto, "CIS106");
        NoHistorial200501 = dao.numRegHistStatus(bitaProyecto, "200501");
        NoHistorialIRA020 = dao.numRegHistStatus(bitaProyecto, "IRA020");
        NoHistorialATIF01 = dao.numRegHistStatus(bitaProyecto, "ATIF01");

        if (!sesion.getAttribute("rol").toString().equals("eva")) //Director General
        {
            if (!NoHistorialAT0022.isEmpty() || !NoHistorialCIS106.isEmpty() || !NoHistorial200501.isEmpty() || HistorialCIS303.size() == 1 || NoHistorialIRA020.size() == 1) //si el tamano es igual a 1, indica que el tramite ya esta suspendido por info adicional
            {
                deInfoAdic = true;
            } else {
                deInfoAdic = false;
            }
        } else {
            if (NoHistorialAT0022.isEmpty() && NoHistorialCIS106.isEmpty() && NoHistorialATIF01.isEmpty()) //si no estubo en prevencion ni en Info Adicional
            {
                deInfoAdic = true;
            } else {

            }
        }

        nombTipoOfic = " Opinion técnica";
        opinionTecnica = new ArrayList();
        String ligaDoctoDG = "";
        boolean editable = true;
        DocumentoDgiraFlujo doctoFlujo;
		String documentoId_ = "";
        
        List <OpinionTecnicaProyecto> otpl = (List<OpinionTecnicaProyecto>) daoDGIRA.lista_namedQuery("OpinionTecnicaProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"}); 
        for (OpinionTecnicaProyecto op : otpl ) {
            ligaDoctoDG = "";
            //Turnado
            aResp = verificaEstadoDocto(Short.toString(op.getOpinionTecnicaProyectoPK().getOpiniontId()), GenericConstants.TIPO_OFICIO_OPINION_TECNICA);
            if (aResp.get(0).toString().length() > 0) {
                File oficio = new File((String) aResp.get(0));
                System.out.println(oficio.exists());
                if (oficio.exists()) {
                    ligaDoctoDG = (String) aResp.get(0);
                } else {
                    ligaDoctoDG = "/preview?tipo=oficio&bitaProyecto=" + bitaProyecto 
                            +"&idDocumento=" + op.getOpinionTecnicaProyectoPK().getOpiniontId() 
                            + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_OPINION_TECNICA;
                }
            }
                        
            editable = (Boolean)aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir
            
            documentoId_ = Short.toString(op.getOpinionTecnicaProyectoPK().getOpiniontId());
            
            
            doctoFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, 
            		GenericConstants.TIPO_OFICIO_OPINION_TECNICA, edoGestTram, documentoId_);
            
                        
            if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
                
                if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
                	
                	ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_OPINION_TECNICA+"&entidadfederativa="+edoGestTram+"&documentoid="+documentoId_;
                	editable = false; // para ocultar los botones de firmar, turnar y corregir
                	
                }else{
                	if(isUrlDG == 1 && bandeja == 2){ // bandeja 1
                		editable = false; // para ocultar los botones de firmar, turnar y corregir
                	}
                	
                }
            }
            
            System.out.println("\nOficio Opinión - documentoId: " + documentoId_ );
            System.out.println("Oficio Opinión - ligaDocto: " + ligaDoctoDG );
            System.out.println("Oficio Opinión - editable: " + editable );
            
            opinionTecnica.add(new OpinionTecnicaHelper(op, false, (int) op.getOpinionTecnicaProyectoPK().getOpiniontId(),
                    op.getOpiniontOficioNumeroR(), op.getOpiniontOficioFechaR(), ligaDoctoDG, editable, (String) aResp.get(2), (Boolean) aResp.get(3)));
        }
        if (opinionTecnica.isEmpty()) {
            OpinionTecnicaProyecto opinion = new OpinionTecnicaProyecto();
            opinion.setCatDependenciaId(listaDependencias.get(0));
            opinionTecnica.add(new OpinionTecnicaHelper(opinion, false,
                    //daoDGIRA.generarIDDoctoRespDGIRA(bitaProyecto, GenericConstants.TIPO_OFICIO_OPINION_TECNICA), 
                    1,
                    ofcResp, ofcFechResp, "", false, "", false));//Sin url ya que no existe el doctu aun, pero es editable al poder ser generado
        }

        if (sesion != null) {   //JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
            if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
            {
                subSectorCat = daoDGIRA.getCatStatusDocs();
            }
            if (sesion.getAttribute("rol").toString().equals("da")) //Director de area
            {
                subSectorCat = daoDGIRA.getCatStatusDocs2();
            }
            if (sesion.getAttribute("rol").toString().equals("sd")) //Sub Director
            {
                subSectorCat = daoDGIRA.getCatStatusDocs2();
            }
            if (sesion.getAttribute("rol").toString().equals("eva")) //Evaluador
            {
                subSectorCat = daoDGIRA.getCatStatusDocs3();
            }
        }

        List<Object[]> lstComentarios;
//        comentariosTab = dao.obtenerObservacionesOficiosTodos(bitaProyecto, GenericConstants.TIPO_OFICIO_OPINION_TECNICA);
        comentariosTab = dao.obtenerObservacionesOficiosTodos(bitaProyecto, GenericConstants.TIPO_OFICIO_OPINION_TECNICA, idEntidad);
        for (Object[] obj : comentariosTab) {
            String idDoc = (String) obj[6];

            lstComentarios = mapComentarios.get(idDoc);
            if (lstComentarios == null) {
                lstComentarios = new ArrayList<>();
            }
            lstComentarios.add(obj);

            mapComentarios.put(idDoc, lstComentarios);
        }

            logger.debug(Utils.obtenerLogConstructor("Fin"));
    }//</editor-fold>

    public void cargaListDependencias() {
        
        sua = new ServicioUnidadAdministrativa();
        listaDependencias = sua.buscarEntidadesOpinionTecnica();
        
        for (UnidadAdministrativa unidadAdministrativa : listaDependencias) {
            SelectItem name = new SelectItem(unidadAdministrativa.getId(),unidadAdministrativa.getNombreCorto());
            itemsDependencias.add(name);
        }
    }
    
        
    @SuppressWarnings("unused")
	private String perfilSiguiente(String perfilActual, short estatusDocto) {
        String perfilSig = "";

                
        if (perfilActual.compareToIgnoreCase("DG") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "dg";
            }
            if (estatusDocto == 2) {
                perfilSig = "da";
            }
        } else if (perfilActual.compareToIgnoreCase("DA") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "dg";
            }
            if (estatusDocto == 2) {
                perfilSig = "sd";
            }
        } else if (perfilActual.compareToIgnoreCase("SD") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "da";
            }
            if (estatusDocto == 2) {
                perfilSig = "eva";
            }
        } else if (perfilActual.compareToIgnoreCase("JD") == 0 || perfilActual.compareToIgnoreCase("eva") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "sd";
            }
        } else {
            perfilSig = perfilActual;
        }
        /*
         switch(perfilSig) {
         case "sd":
         leyenda = "Subdirector";
         break;
         case "eva":
         leyenda = "Evaluador";
         break;
         case "da":
         leyenda = "Director de Area";
         break;
         case "dg":
         leyenda = "Director General";
         }
        
         return leyenda;
         */
        return perfilSig;

    }
    
    
	private ArrayList<Object> verificaEstadoDocto(String idDocumento, short tipoOficio) {
    	
    	System.out.println("\nverificaEstadoDocto("+idDocumento + "," + tipoOficio + ")");
        DocumentoRespuestaDgira doctoResp;
        DocumentoDgiraFlujo doctoFlujo;
        boolean isEditable;
        String perfilUsr;
        ArrayList<Object> aResp = null;
        String observacion = "";
        boolean isFirmado = false;
        //Turnado
        //Obtiene la liga de acceso al documento en caso de existir
        doctoResp = daoDGIRA.getDocumentotoRespuestaDgira(bitaProyecto,
                idDocumento, tipoOficio);

        doctoFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram,
                tipoOficio, edoGestTram, idDocumento);

        isEditable = false;
        String perfilEnSesion = sesion.getAttribute("rol").toString();
        String perfilSig;

        if (doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null) {

//			UsuarioRolDAO usuarioRolDAO = new UsuarioRolDAO();
//			List<UsuarioRol> rolesUsuario = new ArrayList<UsuarioRol>();			
//        	try {
//        		//Se consulta la lista de roles del usuario que envia.
//				rolesUsuario = usuarioRolDAO.consultarRolesByIdUsuario(doctoFlujo.getIdUsuarioEnvio());
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
        	
            try{
            	perfilUsr = doctoFlujo.getRolId().getNombreCorto().trim();
            	}catch(NullPointerException exnp){
            		perfilUsr = dao.getPerfilUsuario(doctoFlujo.getIdUsuarioEnvio());
            	}
            if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() != GenericConstants.ESTATUS_DOCTO_FIRMADO) {
            	//perfilSig = AppSession.siguienteRol(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId(), rolesUsuario);
            	//perfilSig = perfilSiguiente(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                perfilSig = AppSession.siguienteRol(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                isEditable = perfilSig.compareToIgnoreCase(perfilEnSesion) == 0;
                observacion = doctoFlujo.getDocumentoDgiraObservacion() != null ? doctoFlujo.getDocumentoDgiraObservacion() : "";
            } else {
                isFirmado = true;
            }
        } else if (perfilEnSesion.compareToIgnoreCase("eva") == 0) {
            //Por defecto, cuando no tiene flujo comienza por el evaluador
            isEditable = true;
        }

        System.out.println("isEditable :" + isEditable);
        
        aResp = new ArrayList<>();
        aResp.add(doctoResp != null ? doctoResp.getDocumentoUrl() : "");//urlDocto
        aResp.add(isEditable);
        aResp.add(observacion);
        aResp.add(isFirmado);

        return aResp;
    }
	
	
	/***
     * este metodo se utiliza como auxiliar ( para setear la propiedad sIdDocumento ) ya que cuando guardas el dialogo de comentarios de corrección
     * no esta mandando el id.  En cambio cuando recien le cliclo "corregir" y decir que si quiero corregir, si puedo mandar el id del registro actual
     * Despues se invoca "corrigeOficio" ya con el valor seteado y no ocupa el que le esta mandando porq en realidad va con 0 siempre
     * @param idDocumento
     */
    public void setearactualid(int idDocumento){
    	
    	this.sIdDocumento = (short) idDocumento;
    	System.out.println("\n\nsetearactualid : " + this.sIdDocumento);
    }
	
	/***
     * Metodo que realiza la correccion del oficio como tal
     */
    public void corrigeOficio(int idDocumento){
    	// this.sIdDocumento = (short) idDocumento;
    	System.out.println("\n\ncorrige oficio Opinion tecnica.. : " + this.sIdDocumento);
    	System.out.println("comentarios : " + comentarioTurnado);
    	idEstadoDocto = GenericConstants.ESTATUS_DOCTO_CORRECCION; // a correccion
    	
    	try {
			    		
//    		System.out.println("\nActual ROL con que corrige : " + AppSession.GetRolSeleccionado());    		
//    		ArrayList<UsuarioRol> lista = AppSession.GetListaRolesObject();
//    		System.out.println("\ninformación de lista de roles del usuario:");
//    		for(UsuarioRol  obj : lista){
//    			
//    			System.out.println("usuario :" + obj.getUsuarioId().getIdusuario());
//    			System.out.println("idArea  :" + obj.getUsuarioId().getIdarea().trim());
//    			System.out.println("rol     :" + obj.getRolId().getNombreCorto());
//    			System.out.println("rolId     :" + obj.getRolId().getIdRol());    			
//    			System.out.println("-----------");
//    			
//    		}
    		
//    		UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
//    		System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
//    		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
//    		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
//    		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
//    		System.out.println("rolId     :" + ur.getRolId().getIdRol());
    		
    		// turnar el documento
    		turnarDocumento();
    		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    

    @SuppressWarnings("unused")
	public void turnarDocumento() throws Exception {

    	System.out.println("turnarDocumento...");
    	

//        System.out.println("turnarDocumento opinion...");
//
        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();
//
//        try {
////            String strIdDocumento = ctxt.getExternalContext().getRequestParameterMap().get("idDocumento");
//            sIdDocumento = documentoId;
//
//            String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");//this.estatusTurnadoDoctoOT;
//            if (strIdEstadoDocto == null || (strIdEstadoDocto != null && strIdEstadoDocto.isEmpty())) {
//                // reqContEnv.execute("alert('Debe indicar el estado del documento para su turnado')");
//            	ctxt.addMessage("growl", new FacesMessage("Debe indicar el estado del documento para su turnado"));
//            	throw new Exception("Debe indicar el estado del documento para su turnado.");    
//            } else {
//                idEstadoDocto = Short.parseShort(strIdEstadoDocto); //GenericConstants.ESTATUS_DOCTO_AUTORIZADO;
//            }
//        } catch (Exception e) {
//        	e.printStackTrace();
//        	ctxt.addMessage("growl", new FacesMessage("Ocurrio un error durante el turnado, intente nuevamente"));
//            // reqContEnv.execute("alert('Ocurrio un error durante el turnado, intente nuevamente')");
//        	throw new Exception("Ocurrió un error durante el turnado, intente nuevamente.");
//        	
//        }

        short sTipoDoc = GenericConstants.TIPO_OFICIO_OPINION_TECNICA;

        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        Integer idAux = 0;
        short maxIdHisto = 0;
        Bitacora bitacora2 = (Bitacora) dao.busca(Bitacora.class, bitaProyecto);
        Historial subdirector;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        String folio = null;

        List<Object[]> maxDocsDgiraFlujoHist;
        DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
        DocumentoDgiraFlujo docsDgiraFlujo;
        CatEstatusDocumento catEstatusDoc;
        DocumentoDgiraFlujoHist docsDgiraFlujoHist;
        String observacionesDocDgira = this.comentarioTurnado;

        //bitaProyecto = null;
        if (bitaProyecto != null) {
            try {
                proy = daoV.verificaERA2(bitaProyecto);
                if (proy != null) {
                    if (proy.getProyectoPK() != null) {
                        ctxt.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
                        ctxt.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
                        folio = proy.getProyectoPK().getFolioProyecto();
                    }
                }
            } catch (Exception ar) {
                System.out.println("Problema: " + ar.getMessage());
            }

        }

        if (bitaProyecto != null && folio != null && idEstadoDocto != 0 && sIdDocumento != 0) {
            //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

            maxDocsDgiraFlujoHist = daoDGIRA.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento));
            for (Object o : maxDocsDgiraFlujoHist) {
                idAux = Integer.parseInt(o.toString());
                maxIdHisto = idAux.shortValue();
            }

            docsDgiraFlujoHistEdit = daoDGIRA.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento), maxIdHisto);

            if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                daoDGIRA.modifica(docsDgiraFlujoHistEdit);
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
            docsDgiraFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento));

            if (docsDgiraFlujo != null) {
                if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                    banderaNew = 0;
                }// registro nuevo
                else {
                    banderaNew = 1;
                }

            } else {
                banderaNew = 1;
            }

            if (banderaNew == 1) {
                docsDgiraFlujo = new DocumentoDgiraFlujo(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento));
            }

            catEstatusDoc = daoDGIRA.idCatEstatusDoc(idEstadoDocto);
            docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
            docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
            docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
            docsDgiraFlujo.setIdAreaRecibe(null);
            docsDgiraFlujo.setIdUsuarioRecibe(null);
            docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
            docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
            docsDgiraFlujo.setDocumentoDgiraObservacion(observacionesDocDgira);
            
         // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
            UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
//    		System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
//    		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
//    		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
//    		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
//    		System.out.println("rolId     :" + ur.getRolId().getIdRol());
    		
    		// Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
            CatRol rol = dao.getRolPorId(ur.getRolId().getIdRol());
            
            // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
           	docsDgiraFlujo.setRolId(rol);
                        

            try {
                if (banderaNew == 1) {
                    daoDGIRA.agrega(docsDgiraFlujo);
                }
                if (banderaNew == 0) {
                    daoDGIRA.modifica(docsDgiraFlujo);
                }

                maxIdHisto = (short) (idAux + 1);
                docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, Short.toString(sIdDocumento), maxIdHisto);
                docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                docsDgiraFlujoHist.setDocumentoDgiraObservHist(observacionesDocDgira);
                try {
                    daoDGIRA.agrega(docsDgiraFlujoHist);
                } catch (Exception ex) {
                    error = error + 1;
                }
            } catch (Exception e) {
            	e.printStackTrace();
                error = error + 1;
            }

            if (error == 0) {
                System.out.println("Terminó turnado de oficio");

                //Comienza procedimiento de segundas vias en otros casos, aqui no aplica
                // reqContEnv.execute("alert('Oficio turnado exitosamente')");
                ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));

                //------------------------------------------------------------
            } else {
                //reqContEnv.execute("alert('El turnado del oficio de fallo, intente nuevamente')");
                ctxt.addMessage("growl", new FacesMessage("El turnado del oficio falló, intente nuevamente."));
                throw new Exception("El turnado del oficio falló, intente nuevamente.");
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
            this.estatusTurnadoDoctoOT = "0";
            this.comentarioTurnado = "";

        }

    }
    
    
    /***
     * Metodo que realiza un guardado del documento, cambiandole la situacion correspondiente a la firma o corrección
     * @throws IOException
     * @throws DocumentException
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void turnarDocumentoDG() throws IOException, DocumentException
    {
    	
    	System.out.println("\n\nturnarDocumentoDG..");
    	System.out.println("tipoDoc : " + tipoDoc);
    	System.out.println("tipoDocId : " + tipoDocId);
    	System.out.println("idDocumento : " + idDocumento);
    	
    	   	
        //tipoDoc=7;
          suspPorInfoAdic(tipoDoc);
          
          ArrayList<Object> aResp;
          opinionTecnica = new ArrayList();
          String ligaDoctoDG = "";
          boolean editable = true;
          String documentoId_ = "";
          for (OpinionTecnicaProyecto op : (List<OpinionTecnicaProyecto>) daoDGIRA.lista_namedQuery("OpinionTecnicaProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"})) {

              //Turnado
        	  documentoId_ = Short.toString(op.getOpinionTecnicaProyectoPK().getOpiniontId());
              aResp = verificaEstadoDocto(documentoId_, GenericConstants.TIPO_OFICIO_OPINION_TECNICA);
              
              File oficio = new File((String) aResp.get(0));
                System.out.println(oficio.exists());
                if (oficio.exists()) {
                    ligaDoctoDG = (String) aResp.get(0);
                } else {
                    ligaDoctoDG = "/preview?tipo=oficio&bitaProyecto=" + bitaProyecto 
                            +"&idDocumento=" + op.getOpinionTecnicaProyectoPK().getOpiniontId() 
                            + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_OPINION_TECNICA;
                }
              editable = (Boolean)aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir       

           // al oficio que acabo de turnar le actualizo la liga al pdf y oculto los botones de turnado
              if(tipoDocId.equals(documentoId_)){
        		  ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_OPINION_TECNICA+"&entidadfederativa="+edoGestTram+"&documentoid="+documentoId_;
        		  editable = false; // para ocultar los botones de firmar, turnar y corregir
        	  }
              
              opinionTecnica.add(new OpinionTecnicaHelper(op, false, (int) op.getOpinionTecnicaProyectoPK().getOpiniontId(),
                      op.getOpiniontOficioNumeroR(), op.getOpiniontOficioFechaR(), ligaDoctoDG, editable, (String) aResp.get(2), (Boolean) aResp.get(3)));
          }               

    }
    
    
    
    
    @SuppressWarnings("unused")
	public void suspPorInfoAdic(short tipoOfic)
    {

        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        Integer idAux = 0;
        short maxIdHisto = 0;
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial remitente;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        Historial situacATOO22 = null;
        Historial situacI00010 = null;
        Integer resultado = 0;
        Integer resultVersion=-1;
        tipoDoc = tipoOfic;
        Boolean exitoCambSituac = false;
        //tipoDoc = 4 Oficio de prevencion
        //tipoDoc = 7 Oficio de Información adicional
        Integer soloTurnOfic=-1;
        List<Object[]> maxDocsDgiraFlujoHist  = new ArrayList<Object[]>();
        DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
        DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
        DocumentoDgiraFlujo docsDgiraFlujo;        
        CatEstatusDocumento catEstatusDoc;
        int tipoSuspencion=0; 
        int tipoApertura = 0;
        SegundasViasView segundasViasView = new SegundasViasView("");              
        
        FacesContext ctxt = FacesContext.getCurrentInstance();
        
            if(bitaProyecto != null)
            {
                
                    //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

                    maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    for (Object o : maxDocsDgiraFlujoHist) {
                        idAux =  Integer.parseInt(o.toString());
                        maxIdHisto = idAux.shortValue();
                    }

                    docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                    
                    if(docsDgiraFlujoHistEdit != null && maxIdHisto > 0)
                    {            
                        docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                        docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                        docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                        daoDg.modifica(docsDgiraFlujoHistEdit);
                    }


                    //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
                    docsDgiraFlujo = daoDg.docsFlujoDgira(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);

                    if (docsDgiraFlujo != null) {
                        if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                            banderaNew = 0;
                        }// registro nuevo
                        else {
                            banderaNew = 1;
                        }

                    } else {
                        banderaNew = 1;
                    }

                    if (banderaNew == 1) {
                        docsDgiraFlujo = new DocumentoDgiraFlujo(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    }

                    catEstatusDoc = daoDg.idCatEstatusDoc(idDocumento);
                    docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
                    docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
                    docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
                    docsDgiraFlujo.setIdAreaRecibe(null);
                    docsDgiraFlujo.setIdUsuarioRecibe(null);
                    docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
                    docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
                    docsDgiraFlujo.setDocumentoDgiraObservacion(comentarioTurnado);
                    
                 // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
                    UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
//            		System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
//            		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
//            		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
//            		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
//            		System.out.println("rolId     :" + ur.getRolId().getIdRol());
            		
            		// Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
                    CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());
                    
                    // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
                   	docsDgiraFlujo.setRolId(rol);

                    try {
                        if (banderaNew == 1) {
                            daoDg.agrega(docsDgiraFlujo);
                        }
                        if (banderaNew == 0) {
                            daoDg.modifica(docsDgiraFlujo);
                        }

                        maxIdHisto = (short) (idAux + 1);
                        docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                        docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                        docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                        docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                        docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                        docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                        docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                        docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                        docsDgiraFlujoHist.setDocumentoDgiraObservHist(comentarioTurnado);
                        try {
                            daoDg.agrega(docsDgiraFlujoHist);
                        } catch (Exception ex) {
                            error = error + 1;
                        }
                    } catch (Exception e) {
                        error = error + 1;
                    }

                    if (error == 0) 
                    {
                        System.out.println("Terminó turnado de oficio" );

                        if(idDocumento == 4) //si el estatus del documento es 4=frimado
                        {
                            if(tipoDoc == 7)//si es oficio de Información adicional
                            {
                                tipoSuspencion = 1;
                                tipoApertura=2;
                                System.out.println("Comienza turnado de Oficio de Info. Adic. idDocumento: " + idDocumento);
                                
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situación");
                                    //Situaciones para colocar tramite en situación de suspencion por Información adicional                                    
                                    if(cambioSituacion("AT0031","I00010", getBitaProyecto()) == 1)//eva-sd  
                                    {
                                        resultado = resultado + 1;
                                        if(cambioSituacion("AT0032","AT0027", getBitaProyecto()) == 1)//sd-da
                                        {
//                                            resultado = resultado + 1;
//                                            if(cambioSituacion("AT0004","dadg") == 1) //da-dg 
//                                            {
                                                resultado = resultado + 1;
                                                if(cambioSituacion("AT0008","dg", getBitaProyecto()) == 1)//dg-dg 
                                                {  
                                                    resultado = resultado + 1;
                                                    if(cambioSituacion("ATIF01","dgecc", getBitaProyecto()) == 1)//dg-ecc 
                                                    {
                                                        resultado = resultado + 1;                                                                
                                                    }                                                    
                                                }
//                                            }
                                        }
                                    }
                                    if(resultado == 4)
                                    { 
                                        try
                                        {
                                            if(folio!=null)
                                            {
                                                
                                                //CREACION DE VERSION 3 DE TRAMITE, solo aplica para oficio de Información adicional
                                                //si el resultVersion es 0= bien 1=mal
                                                resultVersion = segundasViasView.procVersiones(folio, (short)3);

                                                ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                                if(resultVersion == 0)
                                                { 
                                                    exitoCambSituac = true; 
                                                    System.out.println("Termino ingreso de version 3 de la MIA");
                                                }
                                                else {
                                                    exitoCambSituac = false; 
                                                    System.out.println("No Termino ingreso de version 3 de la MIA");
                                                }
                                                
                                            }
                                            else
                                            {
                                                exitoCambSituac = true; 
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            System.out.println("Problema con inserción de situaciones: " + ex.getMessage());
                                            ctxt.addMessage("growl", new FacesMessage("Trámite no turnado, intentelo mas tarde."));
                                            exitoCambSituac = false; 
                                        }
                                        
                                        
                                    }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5)//si es oficio de prevencion (3=pago de derechos  4=requisitos cofemer)
                            {
                                tipoSuspencion = 2;
                                tipoApertura=1;
                                System.out.println("Comienza turnado de Oficio de prevención idDocumento: " + idDocumento);
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situación");
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("W")) // tramites electronicos
                                    {
                                        //es -- por que se turna asi mismo
                                        if(cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)//if(cambioSituacion("AT0022","I00008") == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("-")) //tramites fisicos
                                    {
                                        //es -- por que se turna del SD al ECC
                                        if(cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    
                                    
                                    if(resultado == 1)
                                    { exitoCambSituac = true; }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 1 || tipoDoc == 2 ||tipoDoc == 8 || tipoDoc == 9 || tipoDoc == 10 || tipoDoc == 11)// estos oficios no van a segundas vias ni afectan el flujo del PEIA
                            {
                                exitoCambSituac = true;
                            }
                            
                            soloTurnOfic = 0;
                        }
                        else // para cuando no se trata del firmado del documento
                        {
                            soloTurnOfic = 1;
                        }
                        
                        
                        if(exitoCambSituac)
                        {
                            if(folio!=null)
                            {
                                if(tipoDoc == 7 || tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5) //segundas vias solo aplica a oficio de info. adicional y de prevencion
                                {
                                    //comienza tramite de segundas vias  
                                    try { //1=iNFORMACION ADICIONAL 2=PREEVENCION
                                        segundasVias.proc(bitaProyecto, tipoSuspencion,folio,tipoApertura,tipoDoc);
                                        ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                        System.out.println("Termino de segundas vías");                                

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, inténtelo mas tarde."));
                                        System.out.println("Problema con segundas vías: " + e.getMessage());
                                    }
                                }
                                else
                                {ctxt.addMessage("growl", new FacesMessage("oficio firmado exitosamente"));}
                            }
                            else
                            {
                                ctxt.addMessage("growl", new FacesMessage("Trámite turnado exitosamente."));
                            }
                        }
                        else
                        {
                            if(soloTurnOfic==0) //si se trata de turnado de tramite en esatdo de firma de DG
                            {
                                System.out.println("Problema con cambio de situación de trámite, no se corrio procedimeinto de segundas vías");
                                ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, intentelo mas tarde.Trámite no notificado a promovente."));
                            }
                            else//si se trata de turnado de tramite en esatdo de NO firma de DG
                            {
                                System.out.println("Se completo turnado de oficio (No es situación de firmado de DG)");
                                ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));
                            }
                        }
                        //FacesContext.getCurrentInstance().addMessage("messages", message);
                        //turnaDocsBtn = true; 
                    } else {
                        ctxt.addMessage("growl", new FacesMessage("El turnado del oficio falló, intente nuevamente."));
                    }
            }
        
    }
    
    
    
    /***
     * Metodo que realiza un guardado del documento, cambiandole la situacion correspondiente a la firma o corrección
     * @param situacion
     * @param situacAnterior
     * @return
     */
	@SuppressWarnings("unused")
	public Integer cambioSituacion(String situacion, String situacAnterior, String bitacora) 
    {
        //situacion: Situacion a la que se desea cambiar el tramite
        //situacAnterior: Situacion en donde el tramite fue turnado, de aqui se obtiene el idAreaEnvia y idUsuarioEnvio(a quien se le habilitara el tramite cunado SINATEC libere el tramite de prenencion o algun paro de reloj )
        Historial remitente; ///idArea idusuario
        Historial remitente2;
        Integer bandera=0;
        String idAreaRecibe="";
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitacora);
        DocumentoDgiraFlujoHist docsDgiraFlujoHist2;
        
        if (bitacora != null && bitacora2!= null) 
        {
            try 
            {       
                if(!situacAnterior.equals("--"))
                { //02000000
                    if(situacAnterior.equals("dg") || situacAnterior.equals("dadg") || situacAnterior.equals("dgecc") || situacAnterior.equals("sdecc"))
                    {
                        if(situacAnterior.equals("dg"))
                        {
                            if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                            {
                                idArea= idArea.trim();
                                //idusuario=idusuario;
                                idAreaRecibe =  idArea.trim();
                            }
                            else{
                                idArea= "02000000";
                                idusuario=3943;
                                idAreaRecibe =  "02000000";
                            }
                        }
                        if(situacAnterior.equals("dadg"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"AT0027");//
                            if(remitente != null)
                            {
                                idArea=remitente.getHistoAreaRecibe();
                                idusuario=remitente.getHistoIdUsuarioRecibe();   
                                                                
                                if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                { idAreaRecibe =  idArea.trim(); }
                                else
                                { idAreaRecibe = "02000000";  }
                            }
                        }
                        if(situacAnterior.equals("dgecc"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"CIS104");
                            if(remitente != null)
                            {
                                if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                {
                                    idArea= idArea.trim();
                                    //idusuario=idusuario;
                                }
                                else
                                {
                                    idArea= "02000000";
                                    idusuario=3943;
                                }
                                idAreaRecibe = remitente.getHistoAreaEnvio(); 
                            }
                        }
                        if(situacAnterior.equals("sdecc")) //SD a ECC
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"CIS104");
                            //traer el Id de usuario del subdirector
                            docsDgiraFlujoHist2 = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, (short)1 );
         
                            if(remitente != null)
                            {
                                idArea= docsDgiraFlujoHist2.getIdAreaEnvioHist() ;
                                idusuario = docsDgiraFlujoHist2.getIdUsuarioEnvioHist();                            
                                idAreaRecibe = remitente.getHistoAreaEnvio(); 
                            }
                        }
                            
                    }
                    else
                    {
                        remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,situacAnterior);
                        if(remitente != null)
                        {
                            idArea=remitente.getHistoAreaRecibe();
                            if(remitente.getHistoIdUsuarioRecibe() != null)
                            idusuario=remitente.getHistoIdUsuarioRecibe(); 
                            idAreaRecibe = remitente.getHistoAreaEnvio(); 
                        }
                    }
                }
                else
                {idAreaRecibe = idArea;}//para situaciones AT0002, AT0003 y AT0011
                //FacesMessage message=null;                   
                sinatDgira = new SinatDgira();
                sinatDgira.setId(0);
                sinatDgira.setSituacion(situacion);
                sinatDgira.setIdEntidadFederativa(bitacora2.getBitaEntidadGestion());
                sinatDgira.setIdClaveTramite(bitacora2.getBitaTipotram()); //STRING BITA_TIPOTRAM
                sinatDgira.setIdTramite(bitacora2.getBitaIdTramite());
                sinatDgira.setIdAreaEnvio(idArea.trim());
                sinatDgira.setIdAreaRecibe(idAreaRecibe.trim());
                sinatDgira.setIdUsuarioEnvio(idusuario);
                //sinatDgira.setIdUsuarioRecibe(1340);
                sinatDgira.setGrupoTrabajo(new Short("0"));
                sinatDgira.setBitacora(bitacora);
                sinatDgira.setFecha(new Date());

                dao.agrega(sinatDgira);
                
                
                //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite turnado correctamente.", null);                    
                System.out.println("Terminó turnado exitosamente, situación: " + situacion);               
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                bandera = 1;                
            } catch (Exception er) {
                bandera = 0;
                er.printStackTrace();
                //FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido turnar el tramite, intente mas tarde.", null);
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                System.out.println("Error en turnado a situación " + situacion + " de trámite :  " + er.getMessage() );
            }
        }
        else
        {
            bandera = 0;
        }
        return bandera;
    }
    
    

    //<editor-fold defaultstate="collapsed" desc="Agregar Opinion.">
    /**
     * Agrega un nuevo elemento a la tabla
     */
    public void agregarOpinion() {
        String ofcResp = "";
        Date ofcFechResp = null;
        if (!opinionTecnica.isEmpty()) {
            int i = opinionTecnica.get(opinionTecnica.size() - 1).getId();
            i++;
            OpinionTecnicaProyecto opinion = new OpinionTecnicaProyecto();
            opinion.setCatDependenciaId(listaDependencias.get(0));
            opinionTecnica.add(new OpinionTecnicaHelper(opinion, false, i, ofcResp, ofcFechResp));
        } else {
            opinionTecnica.add(new OpinionTecnicaHelper(new OpinionTecnicaProyecto(), false,
                    daoDGIRA.generarIDDoctoRespDGIRA(bitaProyecto, GenericConstants.TIPO_OFICIO_OPINION_TECNICA),
                    ofcResp, ofcFechResp));

        }
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Quita registro de opinion">
    /**
     * Elimina un elemento de la tabla
     */
    public void quitarOpinion() {
        System.out.println("idQuitar: " + opinionTecnicaSelect.id);
        System.out.println("quita: " + opinionTecnica.remove(opinionTecnicaSelect));
    }//</editor-fold>

    
    public void changeDependencia(OpinionTecnicaHelper analisis,long idActividad) {
        try {
            System.out.println("Position: " + idActividad);
            ServicioUnidadAdministrativa sua = new ServicioUnidadAdministrativa();
            UnidadAdministrativa newUnidad = sua.findById(idActividad);
            for (int i=0; i < opinionTecnica.size(); i++) {
                OpinionTecnicaHelper opinionTecnica1 = opinionTecnica.get(i);
                System.out.println("analisis : " + analisis.id);
                System.out.println("opinion tecnica 1 : " + opinionTecnica1.id);
                
                if (analisis.equals(opinionTecnica1)) {
                    System.out.println("analisis : " + analisis.opinion.getCatDependenciaId().getNombreCorto());
                    System.out.println("opinion tecnica 1 : " + opinionTecnica1.opinion.getCatDependenciaId().getNombreCorto());
                    opinionTecnica1.opinion.setCatDependenciaId(newUnidad);
                    opinionTecnica.set(i, opinionTecnica1);
                }
            }           
        } catch (MiaExcepcion ex) {
            java.util.logging.Logger.getLogger(OpinionTecnicaView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onEstadoChange() {
        DgiraMiaeDaoGeneral miaeDao = new DgiraMiaeDaoGeneral();
        System.out.println("estado seleccionado :::: " + unidadSeleccionado.getEstadoId());
        listMunicipios = miaeDao.getMunicipios(unidadSeleccionado.getEstadoId());
        itemsMunicipios = new ArrayList<>();
        for (DelegacionMunicipio municipio : listMunicipios) {
                SelectItem select = new SelectItem(municipio.getIdDelegacionMunicipio(), municipio.getNombreDelegacionMunicipio());
                getItemsMunicipios().add(select);
        }
        System.out.println("Municipios :::::::::::::::::::::: " + getItemsMunicipios());
    }
    
    public void validarDatos() {
        if (getUnidadSeleccionado().getNombreDependencia().equals(null) || getUnidadSeleccionado().getNombreCorto().equals(null) ||
            getUnidadSeleccionado().getDomicilio().equals(null) || getUnidadSeleccionado().getNombbreDestinatario().equals(null) ||
            getUnidadSeleccionado().getCargo().equals(null) || getUnidadSeleccionado().getTelefono().equals(null) ||
            getUnidadSeleccionado().getCorreo().equals(null)){
//    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Llena todos los campos"));
            System.out.print("faltan datos");
        } else {
        System.out.print("guardado");
            agregarRegistro();   		   		
        }
    }
    
    public void creaUnidadAdministrativa() {
        unidadSeleccionado = new UnidadAdministrativa();
    }
    
    public void agregarRegistro(){
        try {
            sua = new ServicioUnidadAdministrativa();
            System.out.println("unidadSeleccionado ----->>>" + getUnidadSeleccionado());			
            getUnidadSeleccionado().setEstado("activo");
            int intVal = 1;
            getUnidadSeleccionado().setOpinionTec(intVal);
            sua.saveUnidad(getUnidadSeleccionado());
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Se guardo exitosamente el registro") );
            RequestContext.getCurrentInstance().execute("PF('dlgAddUpdate').hide()");
            setUnidadSeleccionado(new UnidadAdministrativa());
            cargaListDependencias();
        } catch (MiaExcepcion e) {			
            e.printStackTrace();
        }
    }
    
    //<editor-fold defaultstate="collapsed" desc="Guardar registros">
    /**
     * guarda los elementos capturados de la tabla
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void guardarOpinion() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        FacesContext ctxt = FacesContext.getCurrentInstance();
        Integer noGuard = 0;
        String numOfic = "";
        Date fecha = null;
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<Object> aResp;

        for (OpinionTecnicaProyecto otp : (List<OpinionTecnicaProyecto>) daoDGIRA.lista_namedQuery("OpinionTecnicaProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"})) {
            daoDGIRA.elimina(OpinionTecnicaProyecto.class, otp.getOpinionTecnicaProyectoPK());
        }
        for (OpinionTecnicaHelper otp : opinionTecnica) {
            otp.opinion.setOpinionTecnicaProyectoPK(new OpinionTecnicaProyectoPK(bitaProyecto, new Short(otp.id.toString())));

            try {

                if (otp.getOpinion().getOpiniontOficioNumeroR() != null) {

                    List<Object[]> p = dao.fechaDoc(otp.getOpinion().getOpiniontOficioNumeroR());

                    if (p.size() > 0) {
                        for (Object o : p) {
                            numOfic = o.toString();
                        }
                    }

                    try {
                        fecha = formatoDelTexto.parse(numOfic);
                        otp.opinion.setOpiniontOficioFechaR(fecha);
                    } catch (Exception e) {
                    }
                }

                daoDGIRA.agrega(otp.opinion);
                noGuard = 1;
            } catch (Exception e) {
                noGuard = 0;
            }
        }
        if (noGuard == 1) {
            // volver a llenar la lista, para reflejar cambios
            opinionTecnica = new ArrayList();
            @SuppressWarnings("unused")
			int i = 1;
            for (OpinionTecnicaProyecto op : (List<OpinionTecnicaProyecto>) 
                daoDGIRA.lista_namedQuery("OpinionTecnicaProyecto.findByBitacoraProyecto", 
                new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"})) {
                aResp = verificaEstadoDocto(Short.toString(op.getOpinionTecnicaProyectoPK().getOpiniontId()),
                        GenericConstants.TIPO_OFICIO_OPINION_TECNICA);
                opinionTecnica.add(new OpinionTecnicaHelper(op, false,
                        Short.valueOf(op.getOpinionTecnicaProyectoPK().getOpiniontId()).intValue(),
                        op.getOpiniontOficioNumeroR(), op.getOpiniontOficioFechaR(),
                        (String) aResp.get(0), (Boolean) aResp.get(1), (String) aResp.get(2), (Boolean) aResp.get(3)));
                i++;
            }
            ctxt.addMessage("growl", new FacesMessage("Solicitud de opinión técnica fue guardada exitosamente."));
        }
        detalleView.setBanderaOpT(1);
        detalleView.opinionTecnicaCompleta();
        if(detalleView.getActiveTab() == 0)
            reqcontEnv.execute("alert('Es requerido llenar toda la informacion para poder turnar, verifique su información e intente nuevamente.');");
        reqcontEnv.execute("PF('myTabView').select("+ detalleView.getActiveTab() +");");
        
    }//</editor-fold>
    
    /**
     * Descarga el jar para firma y registra la cadena firma si no existe o en su caso muestra el token al usuario
     * @param idDocumento
     */
    public void siFirmar(int idDocumento)
    {
    	this.sIdDocumento = (short) idDocumento;
    	System.out.println("\n\nsi firmar");
    	InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/resources/SignerClientAll.jar");
		fileOpinion = new DefaultStreamedContent(stream, "application/java-archive", "SignerClientAll.jar");
		
		FacesContext fContext = FacesContext.getCurrentInstance();
		String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
		String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		String documentoId = String.valueOf(sIdDocumento);
//		String documentoId = "2";	
		
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDocId :" + GenericConstants.TIPO_OFICIO_OPINION_TECNICA);
		System.out.println("documentoId : " + documentoId);		
		System.out.println("edoGestTramite : " + edoGestTram);		
		System.out.println("rol usuario : " + fContext.getExternalContext().getSessionMap().get("rol")); //sesion.getAttribute("rol").toString());
		System.out.println("idAreaUsu : " + fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
		System.out.println("idUsu : " + fContext.getExternalContext().getSessionMap().get("idUsu"));
		System.out.println("-------------------------------------------------------------------------");
		
		
		// busca si ya existe el registro de cadena firma...
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, GenericConstants.TIPO_OFICIO_OPINION_TECNICA, documentoId);		
		
		String msg_numero_token = Utils.obtenerConstante(GenericConstants.FIRMAR_NUMERO_TOKEN);
		String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.FIRMAR_NO_REGISTRO_CADENAFIRMA);
		
		if(firma != null){
		
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));		
			
		}else{
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
	        Date thisDate = new Date();
	        @SuppressWarnings("unused")
			String fecha = dateFormat.format(thisDate);
	        
	        String descripcion_documento = "Opinión Tecnica";
	        System.out.println("descripcion :" + descripcion_documento);
//	        String destinatario = "Destinatario nuevo";
//	        System.out.println("destinatario :" + destinatario);
//	        String iniciales = "LDSM/ASR/AVA/GBA/JCTT";
//	        System.out.println("iniciales :" + iniciales);
	        
	        String infoaFirmar = "||"+bitacora+"|"+descripcion_documento+"||";
	        String infoaFirmarB64 = Base64.encodeBase64String(infoaFirmar.getBytes());
	        System.out.println("encodeB64 :" + infoaFirmarB64);
	        
			System.out.println("\n\nagregaCadena..");
			CadenaFirmaDao cadenaDao = new CadenaFirmaDao();
			BcAgregaCadenaRespuesta agregacadena = null;
			
			try {
				agregacadena = cadenaDao.agregaCadena(folio, infoaFirmarB64);
				
				if(agregacadena != null){
					 
					 firma = new CadenaFirma();

					 // firma.setClave(clave);
					 firma.setClaveProyecto(claveProyecto);
					 firma.setBitacora(bitacora);
					 firma.setClaveTramite2(cveTipTram);
					 firma.setIdTramite2(idTipTram);
					 firma.setTipoDocId(GenericConstants.TIPO_OFICIO_OPINION_TECNICA);
					 firma.setDocumentoId(documentoId);
					 
					 firma.setFolioProyecto(folio);
					 firma.setB64(infoaFirmarB64);
					 firma.setIdentificador(agregacadena.getIdentificador());
					 firma.setOperacion(String.valueOf(agregacadena.getTransferenciaOperacion()));
				 
					 new CadenaFirmaDao().agrega(firma);
					 
					 // despues de agregar la cadena de firma, se consulta para mostrar el identificador al usuario..
					 firma = null;
					 firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, GenericConstants.TIPO_OFICIO_OPINION_TECNICA, documentoId);
					 
					 if(firma != null){
						 FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
					 }else{
					   	 FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
					 }
					 	 
				}else{
						 
					FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
				}	
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
			}
		}
    }
    

    /***
     * Realiza el proceso de turnado del oficio de opinión técnica
     * @param idDocumento_
     */
    public void turnarOficio(int idDocumento_){
    	this.sIdDocumento = (short) idDocumento_;
    	System.out.println("\n\nturna opinion tecnica..");
    	
    	FacesContext fContext = FacesContext.getCurrentInstance();
        folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
        String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
        String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
        String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
        int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
        // String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
        tipoDoc = GenericConstants.TIPO_OFICIO_OPINION_TECNICA;
        tipoDocId = String.valueOf(sIdDocumento);

        String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();

        System.out.println("-------------------------------------------------------------------------");
        System.out.println("folio :" + folio);
        System.out.println("claveProyecto :" + claveProyecto);		
        System.out.println("bitacora :" + bitacora);
        System.out.println("claveTramite :" + claveTramite);
        System.out.println("idTramite :" + idTramite);
        System.out.println("tipoDocId :" + tipoDoc);
        System.out.println("documentoId : " + tipoDocId);		
        System.out.println("edoGestTramite : " + edoGestTram);		
        System.out.println("rol : " + rol);
        System.out.println("-------------------------------------------------------------------------");

        // este siempre es para turnar No del DG
        idEstadoDocto = GenericConstants.ESTATUS_DOCTO_AUTORIZADO;  	


        if(isUrlDG == 1)
                idDocumento = GenericConstants.ESTATUS_DOCTO_FIRMADO; // 4 firmado
        else
                idDocumento = GenericConstants.ESTATUS_DOCTO_AUTORIZADO; // 3 autorizado ( turnar )

        // url que apunta al oficio dentro del folio( carpeta ). para este caso sera para ponerle esta url al codigo qr
        String ligaQR = Utils.ruta_liga_codigoqr + bitaProyecto;

        List<DocumentoRespuestaDgira> lista = daoDg.docsRespDgiraBusq2(bitacora, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
        for (DocumentoRespuestaDgira x : lista) {
            documento = x;
//            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
//            urlOfic = x.getDocumentoUrl();
//            ubicacionOfic = x.getDocumentoUbicacion();
        }
		
        System.out.println("idDocumento : " + idDocumento);
        System.out.println("urlOfic : " + urlOfic);
        System.out.println("nombreOfic : " + nombreOfic);
        System.out.println("ligaQR : " + ligaQR);
        System.out.println("-------------------------------------------------------------------------");


        String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
        String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
        String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
        String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);

    	// busca si ya existe el registro de cadena firma...		
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        // CadenaFirma firma = getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);
        CadenaFirma cadenafirma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);		
		
        if(cadenafirma == null){		
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_hay_token_registrado));
            return;
        }    	
		
        CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenafirma, /*nivel,*/  rol);
    	
        DetallesFirmado detallefirmado = null;
        try{

            detallefirmado = firmaDao.getFirmas(cadenafirma);

            if(detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() == 98 )){				
                System.out.println("\nno responde el servicio web o no hay firmas...");
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));		
                return;				
            }else if (detallefirmado != null && detallefirmado.getEstado() == 0 && ( detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)){
				
                // cadenafirmaLog puede ser null
                // detallefirmado si trae firmas del ws
                Firmas firmaws = firmaDao.GetFirmaWS(cadenafirma, detallefirmado);

                // para este caso si no hay firma del ws decimos que no ha firmado el usuario...
                if(firmaws == null){
                    System.out.println("ultima firma válida : null");
                    System.out.println("\nel usuario no ha firmado...");
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
                    return;
                }else{
                    System.out.println("ultima firma válida : " + firmaws.getRfc() + " - " + firmaws.getFecha());
                    System.out.println("el usuario ya firmó y se guardará en cadenafirmalog...");
                    // SE ASUME QUE SI FIRMÓ YA por tanto primero agregamos el registro de cadenafirmalog correspondiente...
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
                    Date fcha = formatter.parse(firmaws.getFecha());
                    boolean resp = false;

                    if(cadenafirmaLog == null){
						
                        CadenaFirmaLog firmaLog = new CadenaFirmaLog();
                        firmaLog.setCadenaFirma(cadenafirma);
                        firmaLog.setEstatus((short)1);
                        firmaLog.setRol(rol);
                        firmaLog.setFecha(fcha);
                        firmaLog.setFirma(firmaws.getFirma());
                        firmaLog.setRfc(firmaws.getRfc());

                        resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
                        System.out.println("firmalog agregada: " + resp);

                    }else{
                        // se actualiza cadenafirmalog del rol en sesion
                        cadenafirmaLog.setRfc(firmaws.getRfc());
                        cadenafirmaLog.setFecha(fcha);
                        cadenafirmaLog.setFirma(firmaws.getFirma());
                        resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
                        System.out.println("firmalog modificada: " + resp);
						
                    }
					
                    if(resp == true){
						
                        // y mandamos turnar...
                        if(idDocumento == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){

                            turnarDocumento();

                        }else{
                            
//                            documento.setDocumentoUrl(urlOfic);
//                            documento.setDocumentoNombre(nombreOfic);
//                            documento.setDocumentoUbicacion(ubicacionOfic);
                            
                            firmaDao.editarPdf(documento, cadenafirma, ligaQR);
                            firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(),  cadenafirma.getFolioProyecto());
                            turnarDocumentoDG();
                        }

                    }else{
                        throw new Exception("Error: al guardar el log de la firma para el rol actual");
                    }
                }				
            }else{
                    // pudiera ser un error -99 ( no hay conexion con el servicio buscriptografico )
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_se_realizo_turnado));
            }  
        }catch(Exception ex){
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
        }
        
        ///CAMBIAR BOTONES DE FIRMAR Y TURNAR
        opinionTecnica.clear();
        List <OpinionTecnicaProyecto> otpl = (List<OpinionTecnicaProyecto>) daoDGIRA.lista_namedQuery("OpinionTecnicaProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"}); 
        for (OpinionTecnicaProyecto op : otpl ) {

            //Turnado
            ArrayList<Object> aResp = verificaEstadoDocto(Short.toString(op.getOpinionTecnicaProyectoPK().getOpiniontId()), GenericConstants.TIPO_OFICIO_OPINION_TECNICA);
            String ligaDoctoDG = "";
            boolean editable;
            if (aResp.get(0).toString().length() > 0) {
                File oficio = new File((String) aResp.get(0));
                System.out.println(oficio.exists());
                if (oficio.exists()) {
                    ligaDoctoDG = (String) aResp.get(0);
                } else {
                    ligaDoctoDG = "/preview?tipo=oficio&bitaProyecto=" + bitaProyecto 
                            +"&idDocumento=" + op.getOpinionTecnicaProyectoPK().getOpiniontId() 
                            + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_OPINION_TECNICA;
                }
            }
                        
            editable = (Boolean)aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir
            		
            String documentoId_ = Short.toString(op.getOpinionTecnicaProyectoPK().getOpiniontId());
            
            
            DocumentoDgiraFlujo doctoFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, 
            		GenericConstants.TIPO_OFICIO_OPINION_TECNICA, edoGestTram, documentoId_);
            
                        
            if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
                
                if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
                	
                	ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_OPINION_TECNICA+"&entidadfederativa="+edoGestTram+"&documentoid="+documentoId_;
                	editable = false; // para ocultar los botones de firmar, turnar y corregir
                	
                }else{
                	if(isUrlDG == 1 && bandeja == 2){ // bandeja 1
                		editable = false; // para ocultar los botones de firmar, turnar y corregir
                	}
                	
                }
            }
            
            System.out.println("\nOficio Opinión - documentoId: " + documentoId_ );
            System.out.println("Oficio Opinión - ligaDocto: " + ligaDoctoDG );
            System.out.println("Oficio Opinión - editable: " + editable );
            
            opinionTecnica.add(new OpinionTecnicaHelper(op, false, (int) op.getOpinionTecnicaProyectoPK().getOpiniontId(),
                    op.getOpiniontOficioNumeroR(), op.getOpiniontOficioFechaR(), ligaDoctoDG, editable, (String) aResp.get(2), (Boolean) aResp.get(3)));
        }
        
    }
    
    
 
    //<editor-fold defaultstate="collapsed" desc="getters&setters">
    public List<OpinionTecnicaHelper> getOpinionTecnica() {
        return opinionTecnica;
    }

    public void setOpinionTecnica(List<OpinionTecnicaHelper> opinionTecnica) {
        this.opinionTecnica = opinionTecnica;
    }

    public List<UnidadAdministrativa> getListaDependencias() {
        return listaDependencias;
    }

    public OpinionTecnicaHelper getOpinionTecnicaSelect() {
        return opinionTecnicaSelect;
    }

    public List<SelectItem> getItemsDependencias() {
        return itemsDependencias;
    }
    
    public String getBitaProyecto() {
        return bitaProyecto;
    }

    public void setOpinionTecnicaSelect(OpinionTecnicaHelper opinionTecnicaSelect) {
        System.out.println("id: " + opinionTecnicaSelect.id);
        this.opinionTecnicaSelect = opinionTecnicaSelect;
    }//</editor-fold>

    /**
     * @return the docsOpTec
     */
    public List<Object[]> getDocsOpTec() {
        return docsOpTec;
    }

    /**
     * @param docsOpTec the docsOpTec to set
     */
    public void setDocsOpTec(List<Object[]> docsOpTec) {
        this.docsOpTec = docsOpTec;
    }

    /**
     * @return the cveProyecto
     */
    public String getCveProyecto() {
        return cveProyecto;
    }

    /**
     * @param cveProyecto the cveProyecto to set
     */
    public void setCveProyecto(String cveProyecto) {
        this.cveProyecto = cveProyecto;
    }

    /**
     * @return the opinion2
     */
    public OpinionTecnicaProyecto getOpinion2() {
        return opinion2;
    }

    /**
     * @param opinion2 the opinion2 to set
     */
    public void setOpinion2(OpinionTecnicaProyecto opinion2) {
        this.opinion2 = opinion2;
    }

    /**
     * @return the fechDocum
     */
    public List<Object[]> getFechDocum() {
        return fechDocum;
    }

    /**
     * @param fechDocum the fechDocum to set
     */
    public void setFechDocum(List<Object[]> fechDocum) {
        this.fechDocum = fechDocum;
    }

    /**
     * @return the cveTipTram
     */
    public String getCveTipTram() {
        return cveTipTram;
    }

    /**
     * @param cveTipTram the cveTipTram to set
     */
    public void setCveTipTram(String cveTipTram) {
        this.cveTipTram = cveTipTram;
    }

    /**
     * @return the idTipTram
     */
    public Integer getIdTipTram() {
        return idTipTram;
    }

    /**
     * @param idTipTram the idTipTram to set
     */
    public void setIdTipTram(Integer idTipTram) {
        this.idTipTram = idTipTram;
    }

    /**
     * @return the edoGestTram
     */
    public String getEdoGestTram() {
        return edoGestTram;
    }

    /**
     * @param edoGestTram the edoGestTram to set
     */
    public void setEdoGestTram(String edoGestTram) {
        this.edoGestTram = edoGestTram;
    }

    /**
     * @return the turnaDocs
     */
    public Boolean getTurnaDocs() {
        return turnaDocs;
    }

    /**
     * @param turnaDocs the turnaDocs to set
     */
    public void setTurnaDocs(Boolean turnaDocs) {
        this.turnaDocs = turnaDocs;
    }

    /**
     * @return the NOturnaDocs
     */
    public Boolean getNOturnaDocs() {
        return NOturnaDocs;
    }

    /**
     * @param NOturnaDocs the NOturnaDocs to set
     */
    public void setNOturnaDocs(Boolean NOturnaDocs) {
        this.NOturnaDocs = NOturnaDocs;
    }

    /**
     * @return the documentoRespuestaDgira
     */
    public List<Object[]> getDocumentoRespuestaDgira() {
        return documentoRespuestaDgira;
    }

    /**
     * @param documentoRespuestaDgira the documentoRespuestaDgira to set
     */
    public void setDocumentoRespuestaDgira(List<Object[]> documentoRespuestaDgira) {
        this.documentoRespuestaDgira = documentoRespuestaDgira;
    }

    /**
     * @return the subSectorCat
     */
    public List<CatEstatusDocumento> getSubSectorCat() {
        return subSectorCat;
    }

    /**
     * @param subSectorCat the subSectorCat to set
     */
    public void setSubSectorCat(List<CatEstatusDocumento> subSectorCat) {
        this.subSectorCat = subSectorCat;
    }

    /**
     * @return the docsDgiraFlujoHist
     */
    public DocumentoDgiraFlujoHist getDocsDgiraFlujoHist() {
        return docsDgiraFlujoHist;
    }

    /**
     * @param docsDgiraFlujoHist the docsDgiraFlujoHist to set
     */
    public void setDocsDgiraFlujoHist(DocumentoDgiraFlujoHist docsDgiraFlujoHist) {
        this.docsDgiraFlujoHist = docsDgiraFlujoHist;
    }

    /**
     * @return the sesion
     */
    public HttpSession getSesion() {
        return sesion;
    }

    /**
     * @return the idDocumento
     */
    public short getIdDocumento() {
        return idDocumento;
    }

    /**
     * @param idDocumento the idDocumento to set
     */
    public void setIdDocumento(short idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     * @return the areaEnvio
     */
    public String getAreaEnvio() {
        return areaEnvio;
    }

    /**
     * @param areaEnvio the areaEnvio to set
     */
    public void setAreaEnvio(String areaEnvio) {
        this.areaEnvio = areaEnvio;
    }

    /**
     * @return the tipoDoc
     */
    public short getTipoDoc() {
        return tipoDoc;
    }

    /**
     * @param tipoDoc the tipoDoc to set
     */
    public void setTipoDoc(short tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    /**
     * @return the deInfoAdic
     */
    public Boolean getDeInfoAdic() {
        return deInfoAdic;
    }

    /**
     * @param deInfoAdic the deInfoAdic to set
     */
    public void setDeInfoAdic(Boolean deInfoAdic) {
        this.deInfoAdic = deInfoAdic;
    }

    public String getEstatusTurnadoDoctoOT() {
        return estatusTurnadoDoctoOT;
    }

    public void setEstatusTurnadoDoctoOT(String estatusTurnadoDoctoOT) {
        this.estatusTurnadoDoctoOT = estatusTurnadoDoctoOT;
    }

    public void setEstatusTurnadoDoctoOT() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");

        this.estatusTurnadoDoctoOT = strIdEstadoDocto;
    }

    public String getComentarioTurnado() {
        return comentarioTurnado;
    }

    public void setComentarioTurnado(String comentarioTurnado) {
        this.comentarioTurnado = comentarioTurnado;
    }

    public void setComentarioTurnado() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        String strComentarios = ctxt.getExternalContext().getRequestParameterMap().get("comentTurnado");

        this.comentarioTurnado = strComentarios;
    }

    /**
     * @return the nombTipoOfic
     */
    public String getNombTipoOfic() {
        return nombTipoOfic;
    }

    /**
     * @param nombTipoOfic the nombTipoOfic to set
     */
    public void setNombTipoOfic(String nombTipoOfic) {
        this.nombTipoOfic = nombTipoOfic;
    }

    public List<Object[]> getComentariosTab() {
        return comentariosTab;
    }

    /**
     * @return the unidadSeleccionado
     */
    public UnidadAdministrativa getUnidadSeleccionado() {
        return unidadSeleccionado;
    }

    /**
     * @param unidadSeleccionado the unidadSeleccionado to set
     */
    public void setUnidadSeleccionado(UnidadAdministrativa unidadSeleccionado) {
        this.unidadSeleccionado = unidadSeleccionado;
    }

    /**
     * @return the itemsEstados
     */
    public List<SelectItem> getItemsEstados() {
        return itemsEstados;
    }

    /**
     * @param itemsEstados the itemsEstados to set
     */
    public void setItemsEstados(List<SelectItem> itemsEstados) {
        this.itemsEstados = itemsEstados;
    }

    /**
     * @return the itemsMunicipios
     */
    public List<SelectItem> getItemsMunicipios() {
        return itemsMunicipios;
    }

    /**
     * @param itemsMunicipios the itemsMunicipios to set
     */
    public void setItemsMunicipios(List<SelectItem> itemsMunicipios) {
        this.itemsMunicipios = itemsMunicipios;
    }

    /**
     * @return the itemsTratamientos
     */
    public List<SelectItem> getItemsTratamientos() {
        return itemsTratamientos;
    }

    /**
     * @param itemsTratamientos the itemsTratamientos to set
     */
    public void setItemsTratamientos(List<SelectItem> itemsTratamientos) {
        this.itemsTratamientos = itemsTratamientos;
    }

    //<editor-fold defaultstate="collapsed" desc="Clase OpinionTecnicaHelper">
    /**
     * Clase de apoyo para la opcion de "Otra" en las dependencias
     */
    public class OpinionTecnicaHelper {

        private OpinionTecnicaProyecto opinion;
        private Boolean otra = false;
        private Integer id;
        private String oficResp;
        private Date oficFechResp;

        private String estatusDocto;
        private String ligaDocto;
        private boolean editable; //Indica si el documento se puede editar, de acuerdo al perfil del usuario y estatus del docto
        private boolean firmado;
        private String observacion;

        public OpinionTecnicaHelper(OpinionTecnicaProyecto opinion, Boolean otra, Integer id, String oficResp, Date oficFechResp) {
            this.opinion = opinion;
            this.otra = otra;
            this.id = id;
            this.oficResp = oficResp;
            this.oficFechResp = oficFechResp;
        }

        public OpinionTecnicaHelper(OpinionTecnicaProyecto opinion, Boolean otra, Integer id, String oficResp, Date oficFechResp,
                String ligaDocto, Boolean editable, String observacion, Boolean firmado) {
            this.opinion = opinion;
            this.otra = otra;
            this.id = id;
            this.oficResp = oficResp;
            this.oficFechResp = oficFechResp;

            this.ligaDocto = ligaDocto;
            this.editable = editable;
            this.firmado = firmado;
        }

        public OpinionTecnicaProyecto getOpinion() {
            return opinion;
        }

        public void setOpinion(OpinionTecnicaProyecto opinion) {
            this.opinion = opinion;
        }

        public Boolean getOtra() {
            if (opinion != null) {
                if (opinion.getCatDependenciaId() != null) {
                    otra = false;
                }
            } else {
                otra = false;
            }
            return otra;
        }

        public void setOtra(Boolean otra) {
            this.otra = otra;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return the oficResp
         */
        public String getOficResp() {
            return oficResp;
        }

        /**
         * @param oficResp the oficResp to set
         */
        public void setOficResp(String oficResp) {
            this.oficResp = oficResp;
        }

        /**
         * @return the oficFechResp
         */
        public Date getOficFechResp() {
            return oficFechResp;
        }

        /**
         * @param oficFechResp the oficFechResp to set
         */
        public void setOficFechResp(Date oficFechResp) {
            this.oficFechResp = oficFechResp;
        }

        public String getLigaDocto() {
            return ligaDocto;
        }

        public void setLigaDocto(String ligaDocto) {
            this.ligaDocto = ligaDocto;
        }

        public boolean isEditable() {
            return editable;
        }

        public void setEditable(boolean editable) {
            this.editable = editable;
        }

        public String getEstatusDocto() {
            return estatusDocto;
        }

        public void setEstatusDocto() {
            FacesContext ctxt = FacesContext.getCurrentInstance();
            String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");

            this.estatusDocto = strIdEstadoDocto;
        }

        public void setEstatusDocto(String estatusDocto) {
            this.estatusDocto = estatusDocto;
        }

        public boolean isFirmado() {
            return firmado;
        }

        public void setFirmado(boolean firmado) {
            this.firmado = firmado;
        }

        public String getObservacion() {
            return observacion;
        }

        public void setObservacion(String observacion) {
            this.observacion = observacion;
        }
    }
    //</editor-fold>

    public SelectItem[] getTipoProySelectOne() {
        SelectItem[] items = null;
        List<String[]> lista; //

        System.out.println("cveProyecto: " + cveProyecto);
        if (cveProyecto != null) {
            //cveProyecto = "03BS2012M0005";
//            lista = dao.oficOpTec(bitaProyecto, cveProyecto);
            lista = dao.oficOpTec(bitaProyecto, cveProyecto, tbarea);
            System.out.println("lista oficios opinion tecnica: " + lista.size());

            int size = lista.size() + 1;
            items = new SelectItem[size];
            int i = 1;
            //int j=0;

            items[0] = new SelectItem("-- Seleccione --");
            //i++;

            try {
                for (int j = 0; j < lista.size(); j++) {
                    items[i] = new SelectItem(lista.get(j));
                    i++;
                }
            } catch (Exception e) {
                //JOptionPane.showMessageDialog(null,  "  bitaProyecto: " + e.getMessage() , "Error", JOptionPane.INFORMATION_MESSAGE);
            }

        }

        return items;
    }

    public void selTurnadoDocsCheck() {
        turnaDocs = false; ////cveTipTram idTipTram edoGestTram, idTipoDoc, idDoc
        tipoDoc = 4;  //String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc
        documentoRespuestaDgira = daoDGIRA.docsRespDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
        if (documentoRespuestaDgira.size() > 0) {
            turnaDocs = false;
        } else {
            turnaDocs = true;

        }
        //JOptionPane.showMessageDialog(null, "Debe:  " + turnaDocs, "Error", JOptionPane.INFORMATION_MESSAGE);
    }

//    public void selTurnadoDocs2() {
//        turnaDocs = false; 
//        documentoRespuestaDgira = daoDGIRA.docsRespDgira(bitaProyecto);
//        if (documentoRespuestaDgira.size()>0) 
//        {
//            turnaDocs = true; 
//        }
//        else
//        {
//            turnaDocs = false; 
//            
//        }
//       // JOptionPane.showMessageDialog(null, "Debe:  " + turnaDocs, "Error", JOptionPane.INFORMATION_MESSAGE);
//    }
//    public void selTurnadoDocs() {
//        turnaDocs = false; 
//        documentoRespuestaDgira = daoDGIRA.docsRespDgira(bitaProyecto);
//        if (documentoRespuestaDgira.size()>0) 
//        {
//            turnaDocs = false; 
//        }
//        else
//        {
//            turnaDocs = true; 
//            
//        }
//        //JOptionPane.showMessageDialog(null, "Debe:  " + turnaDocs, "Error", JOptionPane.INFORMATION_MESSAGE);
//    }
    public SelectItem[] getEnvioSelectOne() {
        SelectItem[] items = null;
        items = new SelectItem[1];

        if (idDocumento > 0) {//JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
            if (sesion != null) {
                JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
                if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al director de Area
                    {
                        items[0] = new SelectItem("1", "Director General");
                    }
                    if (idDocumento == 2) //SI el doc esta en correccion, solo se podra turnar al evaluador
                    {
                        items[0] = new SelectItem("2", "Director de Área");
                    }
                }
                if (sesion.getAttribute("rol").toString().equals("da")) //Director de Area
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al director de Area
                    {
                        items[0] = new SelectItem("1", "Director General");
                    }
                    if (idDocumento == 2) //SI el doc esta en correccion, solo se podra turnar al evaluador
                    {
                        items[0] = new SelectItem("3", "SubDirector");
                    }
                }
                if (sesion.getAttribute("rol").toString().equals("sd")) //Sub Director
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al director de area
                    {
                        items[0] = new SelectItem("2", "Director de Área");
                    }
                    if (idDocumento == 2) //SI el doc esta en correccion, solo se podra turnar al evaluador
                    {
                        if (tipoDoc != 4 && tipoDoc != 3) {
                            items[0] = new SelectItem("4", "Evaluador");
                        } else {
                            items = new SelectItem[1];
                            items[0] = new SelectItem("0", "--Seleccione estatus--");
                        }
                    }

                }
                if (sesion.getAttribute("rol").toString().equals("eva")) //Evaluador
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al  subdirector
                    {
                        items[0] = new SelectItem("3", "SubDirector");
                    }
                }
            }
        }
        if (idDocumento == 0) {
            items[0] = new SelectItem("0", "--Seleccione estatus--");
        }

        return items;
    }

    @SuppressWarnings("unused")
	public void muestraObservaciones() {
		String idDocumento, strTipoDocto;
        short sTipoDocto;
        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();

        try {
            idDocumento = ctxt.getExternalContext().getRequestParameterMap().get("idDocumento");
            sTipoDocto = GenericConstants.TIPO_OFICIO_OPINION_TECNICA;
            //comentariosTab = dao.obtenerObservacionesOficios(bitaProyecto, sTipoDocto, idDocumento);
            comentariosTab = mapComentarios.get(idDocumento);

        } catch (Exception e) {
            reqContEnv.execute("alert('Ocurrio un error durante la obtencion de las observaciones, intente nuevamente')");
        }
    }

	public short getsIdDocumento() {
		return sIdDocumento;
	}

	public void setsIdDocumento(short sIdDocumento) {
		this.sIdDocumento = sIdDocumento;
	}
   

	public short getIdEstadoDocto() {
		return idEstadoDocto;
	}

	public void setIdEstadoDocto(short idEstadoDocto) {
		this.idEstadoDocto = idEstadoDocto;
	}

	public String getEstatusTurnadoDocto() {
		return estatusTurnadoDocto;
	}

	public void setEstatusTurnadoDocto(String estatusTurnadoDocto) {
		this.estatusTurnadoDocto = estatusTurnadoDocto;
	}

	public StreamedContent getFileOpinion() {
		return fileOpinion;
	}

	public void setFileOpinion(StreamedContent fileOpinion) {
		this.fileOpinion = fileOpinion;
	}

	/**
	 * @return the tbarea
	 */
	public Tbarea getTbarea() {
		return tbarea;
	}

	/**
	 * @param tbarea the tbarea to set
	 */
	public void setTbarea(Tbarea tbarea) {
		this.tbarea = tbarea;
	}


	public DetalleProyectoView getDetalleView() {
		return detalleView;
	}


	public void setDetalleView(DetalleProyectoView detalleView) {
		this.detalleView = detalleView;
	}

}
