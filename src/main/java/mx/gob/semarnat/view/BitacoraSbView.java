/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;

/**
 *
 * @author Paty
 */
@ManagedBean(name = "historialSD")
@ViewScoped
public class BitacoraSbView implements Serializable {
    private static final Logger logger = Logger.getLogger(BitacoraSbView.class.getName());

    private final BitacoraDao dao = new BitacoraDao();

    private List<Historial> listHistorial;
    private List<Bitacora> listBitacora;
    private Bitacora bitacora;
    private SinatDgira sinatDgira;
    private List<Object[]> todosProy = new ArrayList();
    private List<Object[]> detalleProy = new ArrayList();
    private List<Object[]> todosProyTurnados = new ArrayList();
    private String bitacoraNom;
    private String TunaEvaluador;
    private SelectItem[] cmbEvaluadores;

    //--------------------subdirector
//    private List<Object[]> todosProySubD = new ArrayList();
    private List<Object[]> detalleProySubD = new ArrayList();

    private String liga;
    private String idArea;
    private Integer idusuario;

//    private String bitaSelect;
    //<editor-fold desc="Constructor" defaultstate="collapsed">
    public BitacoraSbView() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");

        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
//        todosProySubD = dao.enRecepSubDirec(); //TODOS LOS TRAMITES CON ID DE SITUACION I00010

        //MOSTRAR DETALLE DE TRAMITE
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String bitacoraNum = req.getParameter("numBitacora");
        if (bitacoraNum != null) {
            bitacoraNom = bitacoraNum;
            detalleProySubD = dao.detalleProySubDirector(bitacoraNum);  //TODOS LO TRAMITES CON SITUACION AT0001 
        }

        cmbEvaluadores = todoDireccion();
        // pool error todosProyTurnados = dao.enRecepSubDirecTurnados(); //TODOS LOS TRAMITES CON ID DE SITUACION I00010

        //genracion de URL, para visor de Expediente Electronico        
        liga = "#";
        try {
            if (bitacoraNom != null) {
                bitacora = dao.datosBitaInd(bitacoraNom);
                if (bitacora != null) {
                    if (bitacora.getBitaTipotram().equals("MP") || bitacora.getBitaTipotram().equals("DM") || bitacora.getBitaTipotram().equals("MG") 
                    		|| bitacora.getBitaTipotram().equals("DL")) {
                        liga = "pantallaSubdirectorInsideCapMIA.xhtml?bitaNum=" + bitacoraNom;
                        //liga = "pantallaDaInsideCapMIA.xhtml?bitaNum="+bitacoraNom;
                    }
                    if (bitacora.getBitaTipotram().equals("IP")) {
                        liga = "pantallaSubdirectorInsideCapIP.xhtml?bitaNum=" + bitacoraNom;
                        //liga = "pantallaDaInsideCapIP.xhtml";
                    }
                }
            }

        } catch (Exception e) {
        }
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }//</editor-fold>

    //<editor-fold desc="Listado de proyectos a turnar" defaultstate="collapsed">
    public SelectItem[] todoDireccion() {
        List<Tbarea> lista = new ArrayList<Tbarea>();

        lista = dao.evaluadoresTodos(idArea);
        int size = lista.size() + 1;
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        items[0] = new SelectItem("0", "-- Seleccione --");
        i++;
        for (Tbarea x : lista) {

            items[i++] = new SelectItem(x.getTbareaPK().getIdarea(), x.getArea());
        }
        return items;

    }//</editor-fold>
    
    //<editor-fold desc="Referencia" defaultstate="collapsed">
    public void seleccionaReferencia() {

        //cambio de situacion de AT0001 A I00008, TURNADO DE DIRECTOR A SUBDIRECTOR
        if (TunaEvaluador.length() > 0 && bitacoraNom.length() > 0 && !TunaEvaluador.equals("0")) { 
        	//JOptionPane.showMessageDialog(null,  " Entro turnaEva: " + TunaEvaluador + "    bita:" + bitacoraNom , "Error", JOptionPane.INFORMATION_MESSAGE);
            try {
                bitacora = (Bitacora) dao.busca(Bitacora.class, new String(bitacoraNom));
                if (bitacora != null) {

                    sinatDgira = new SinatDgira();
                    sinatDgira.setId(0);
                    sinatDgira.setSituacion("I00010");
                    sinatDgira.setIdEntidadFederativa(bitacora.getBitaEntidadGestion());
                    sinatDgira.setIdClaveTramite(bitacora.getBitaTipotram()); //STRING BITA_TIPOTRAM
                    sinatDgira.setIdTramite(bitacora.getBitaIdTramite());
                    sinatDgira.setIdAreaEnvio(idArea.trim());
                    sinatDgira.setIdAreaRecibe(TunaEvaluador.trim());
                    sinatDgira.setIdUsuarioEnvio(idusuario);
                    sinatDgira.setIdUsuarioRecibe(1340);
                    sinatDgira.setGrupoTrabajo(new Short("0"));
                    sinatDgira.setBitacora(bitacoraNom);
                    sinatDgira.setFecha(new Date());
                    dao.agrega(sinatDgira);

                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Su información ha sido guardada con éxito.", null);
                    FacesContext.getCurrentInstance().addMessage("messages", message);
                }
            } catch (Exception er) {
                er.printStackTrace();

                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido guardar la información, Intente más tarde.", null);
                FacesContext.getCurrentInstance().addMessage("messages", message);
            }
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Seleccione al evaluador.", null);
            FacesContext.getCurrentInstance().addMessage("messages", message);
        }
    }//</editor-fold>

    //<editor-fold desc="Getters & Setters" defaulstate="collapsed">
    /**
     * @return the listHistorial
     */
    public List<Historial> getListHistorial() {
        return listHistorial;
    }

    /**
     * @param listHistorial the listHistorial to set
     */
    public void setListHistorial(List<Historial> listHistorial) {
        this.listHistorial = listHistorial;
    }

    /**
     * @return the bitacora
     */
    public Bitacora getBitacora() {
        return bitacora;
    }

    /**
     * @param bitacora the bitacora to set
     */
    public void setBitacora(Bitacora bitacora) {
        this.bitacora = bitacora;
    }

    /**
     * @return the todosProy
     */
    public List<Object[]> getTodosProy() {
        return todosProy;
    }

    /**
     * @param todosProy the todosProy to set
     */
    public void setTodosProy(List<Object[]> todosProy) {
        this.todosProy = todosProy;
    }

    /**
     * @return the detalleProy
     */
    public List<Object[]> getDetalleProy() {
        return detalleProy;
    }

    /**
     * @param detalleProy the detalleProy to set
     */
    public void setDetalleProy(List<Object[]> detalleProy) {
        this.detalleProy = detalleProy;
    }

    /**
     * @return the bitacoraNom
     */
    public String getBitacoraNom() {
        return bitacoraNom;
    }

    /**
     * @param bitacoraNom the bitacoraNom to set
     */
    public void setBitacoraNom(String bitacoraNom) {
        this.bitacoraNom = bitacoraNom;
    }

    /**
     * @return the todosProyTurnados
     */
    public List<Object[]> getTodosProyTurnados() {
        return todosProyTurnados;
    }

    /**
     * @param todosProyTurnados the todosProyTurnados to set
     */
    public void setTodosProyTurnados(List<Object[]> todosProyTurnados) {
        this.todosProyTurnados = todosProyTurnados;
    }

    /**
     * @return the todosProySubD
     */
//    public List<Object[]> getTodosProySubD() {
//        return todosProySubD;
//    }
//
//    /**
//     * @param todosProySubD the todosProySubD to set
//     */
//    public void setTodosProySubD(List<Object[]> todosProySubD) {
//        this.todosProySubD = todosProySubD;
//    }
    /**
     * @return the detalleProySubD
     */
    public List<Object[]> getDetalleProySubD() {
        return detalleProySubD;
    }

    /**
     * @param detalleProySubD the detalleProySubD to set
     */
    public void setDetalleProySubD(List<Object[]> detalleProySubD) {
        this.detalleProySubD = detalleProySubD;
    }

    /**
     * @return the liga
     */
    public String getLiga() {
        return liga;
    }

    /**
     * @param liga the liga to set
     */
    public void setLiga(String liga) {
        this.liga = liga;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

//    public String getBitaSelect() {
//        return bitaSelect;
//    }
//
//    public void setBitaSelect(String bitaSelect) {
//        this.bitaSelect = bitaSelect;
//    }
//    private static final DgiraMiaeDaoGeneral daoG = new DgiraMiaeDaoGeneral();
//
//    public String getSintesis() {
//        if (bitaSelect != null) {
//            EvaluacionProyecto ev = (EvaluacionProyecto) daoG.busca(EvaluacionProyecto.class, bitaSelect);
//            bitaSelect = null;
//            return ev.getEvaSistesisProyecto();
//        } else {
//            return "";
//        }
//    }
    public List<Bitacora> getListBitacora() {
        return listBitacora;
    }

    public void setListBitacora(List<Bitacora> listBitacora) {
        this.listBitacora = listBitacora;
    }

    /**
     * @return the TunaEvaluador
     */
    public String getTunaEvaluador() {
        return TunaEvaluador;
    }

    /**
     * @param TunaEvaluador the TunaEvaluador to set
     */
    public void setTunaEvaluador(String TunaEvaluador) {
        this.TunaEvaluador = TunaEvaluador;
    }

    /**
     * @return the cmbEvaluadores
     */
    public SelectItem[] getCmbEvaluadores() {
        return cmbEvaluadores;
    }

    /**
     * @param cmbEvaluadores the cmbEvaluadores to set
     */
    public void setCmbEvaluadores(SelectItem[] cmbEvaluadores) {
        this.cmbEvaluadores = cmbEvaluadores;
    }
    //</editor-fold>
    
}
