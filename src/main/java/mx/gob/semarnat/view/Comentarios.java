/*                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.MenuDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.ComentarioProyecto;
import mx.gob.semarnat.model.dgira_miae.ComentarioProyectoPK;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import mx.gob.semarnat.secure.Util;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Paty
 */
@ManagedBean(name = "Comentario")
@RequestScoped
public class Comentarios implements Serializable {
    private static final Logger logger = Logger.getLogger(Comentarios.class.getName());
    
    private final VisorDao dao = new VisorDao();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private Proyecto folioDetalle;
    private Proyecto proyecto;
    private ComentarioProyecto comentarioProy;
    private ComentarioProyecto comentarioProyCon;
    private Boolean infoAdicional;
    private String comentarioRes;
    private String comentarioIA;
    private String comentarioMT; 
    private Integer idUsu;
    private Short cap;
    private Integer idusuario;
    private short capituloId;
    private short subcapituloId;
    private short seccionId;
    private short apartadoId;
    private String comentarioTipo;
    
//    private List<Historial> NoHistorialAT0011;
//    private List<Historial> NoHistorialAT0003;
    
    private List<Historial> NoHistorialATIF01;     
    private List<Historial> NoHistorialATR001;
    
//    private List<Historial> NoHistorialCIS302;
//    private List<Historial> NoHistorialIRA020;
//    private List<Historial> NoHistorialCIS303;
    
    private Boolean permiteGuaradar = false;
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private final MenuDao MenuDao= new MenuDao();
    private Integer cveTipTram = 0;
    private Boolean esIP = false;
    private Boolean habilitaRes = false; 
    private Boolean habilitaIA = false;
    private Boolean habilitaMT = false;
    private HttpSession sesion = Util.getSession();
    

   @ManagedProperty("#{param.info}")
    private String info;

    @PostConstruct
    public void init() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        info = params.get("info").toString();
        String[] args = getInfo().split(",");

        if (args.length == 6) {
            capituloId = (short) 0;
            subcapituloId = (short) 0;
            seccionId = (short) 0;
            apartadoId = (short) 0;
            
            capituloId = new Short(args[0]);  
            subcapituloId = new Short(args[1]);
            seccionId = new Short(args[2]);
            apartadoId = new Short(args[3]);
            pramBita = args[4];
            idusuario = new Integer(args[5]); 
        }
        if (pramBita != null) 
        { 
            folioDetalle = dao.verificaERA2(pramBita);
//            NoHistorialAT0011 = daoBitacora.numRegHistStatus(pramBita, "AT0011");//EXPEDIENTE NO INTEGRADO
            NoHistorialATIF01 = daoBitacora.numRegHistStatus(pramBita, "ATIF01");//OFICIO DE INFORMACIÓN ADICIONAL
            NoHistorialATR001 = daoBitacora.numRegHistStatus(pramBita, "ATR001");//DOCUMENTO DE RESPUESTADE CPNCLUSIÓN TURNADO AL ECC
                    
//            NoHistorialIRA020 = daoBitacora.numRegHistStatus(pramBita, "IRA020"); 
//            NoHistorialCIS303 = daoBitacora.numRegHistStatus(pramBita, "CIS303");
//            NoHistorialAT0003 = daoBitacora.numRegHistStatus(pramBita, "AT0003"); 
            cveTipTram = MenuDao.cveTipTramBita(pramBita);        
            System.out.println("TipoTramite: " + cveTipTram );
            if(cveTipTram == 63)
            { esIP = false; }
            else
            { esIP = true; }
            System.out.println("esIP: " + esIP );
            
            if (sesion.getAttribute("rol").toString().equals("eva")) //evaluador
            {
                //-------------verificar si se puede guardar la sección de Info. Adicional-------------
                //Inicio=Evaluación  Final=Cuando se da resolución  El rol que puede realizar e guardado Rol=Evaluador
                if(NoHistorialATIF01.size()==1)
                { habilitaIA = false; }//no podra guardar
                if(NoHistorialATIF01.isEmpty())
                { habilitaIA = true; }  //si podrá guardar
                
                 //-------------verificar si se puede guardar la sección de resolutivo-------------
                //Inicio=Evaluación  Final=Cuando se da resolución  El rol que puede realizar e guardado Rol=Evaluador
                if(NoHistorialATR001.size()==1)
                { habilitaRes = false; }//no podra guardar
                if(NoHistorialATR001.isEmpty())
                { habilitaRes = true; }  //si podrá guardar
            }
            //-------------verificar si se puede guardar la sección de Memoria técnica-------------
            //Inicio=Turnado de ECC a área correspondiente Final=Cuando se da resolución  El rol que puede realizar el guardado Rol=Todos
            if(NoHistorialATR001.size()==1)
            { habilitaMT = false; }//no podra guardar
            if(NoHistorialATR001.isEmpty())
            { habilitaMT = true; }  //si podrá guardar
            
            System.out.println("habilitaIA1: " + habilitaIA + "   habilitaRes: " + habilitaRes + "  habilitaMT: " + habilitaMT);
            
            
//            //verificar que pestaña de los comentarios esta habilitada en cada rol
//            if(NoHistorialAT0011.isEmpty() && NoHistorialATIF01.isEmpty())//si no viene de tramite No integrado y de oficon de info. adiconal, se podran guardar las observaciones
//            {
//                permiteGuaradar = true;
//            }
//            else//si si viene de tramite No integrado o de oficon de info. adiconal, NO se podran guardar las observaciones
//            {
//                if(!NoHistorialATIF01.isEmpty())
//                {
//                    if((NoHistorialATIF01.size()==1 && NoHistorialIRA020.isEmpty()) || (NoHistorialATIF01.size()==1 && NoHistorialCIS303.isEmpty()))
//                    {
//                        permiteGuaradar = false;
//                    }
//                     if((NoHistorialATIF01.size()==1 && !NoHistorialIRA020.isEmpty()) || (NoHistorialATIF01.size()==1 && !NoHistorialCIS303.isEmpty()))
//                    { permiteGuaradar = true; }
//                }
//                else
//                { permiteGuaradar = false; }
//            }
//            System.out.println("NoHistorialAT0011: " + NoHistorialAT0011.size() + "   NoHistorialATIF01: " + NoHistorialATIF01.size() + "  NoHistorialIRA020: " + NoHistorialIRA020.size() + "  NoHistorialCIS303: " + NoHistorialCIS303.size() + "   permiteGuaradar: " + permiteGuaradar);
            if (folioDetalle != null)  
            {
                folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                //JOptionPane.showMessageDialog(null, "pramBita:  " + pramBita + "   idusuario: " + idusuario, "Error", JOptionPane.INFORMATION_MESSAGE);
                //----proyecto en sesion
                proyecto = dao.cargaProyecto(folioNum, serialNum);
                comentarioProy = dao.buscaComentario(pramBita, capituloId, subcapituloId, seccionId, apartadoId); //, idusuario
                if (comentarioProy != null)
                {
                    comentarioRes = comentarioProy.getComentarioResolutivo();
                    comentarioIA = comentarioProy.getComentarioInfoAdicional();
                    comentarioMT = comentarioProy.getComentarioMemoriaTecnica();
                    if(comentarioIA != null)
                    {
                        if(comentarioIA.length()>0 && !comentarioIA.equals("<br>"))
                        {  infoAdicional = true; }
                        else
                        {  infoAdicional = false; }
                     
                    }
                    else
                    {
                        infoAdicional = false; 
                    }
                }
                else
                {  infoAdicional = false; comentarioRes = ""; comentarioIA = ""; comentarioMT = ""; infoAdicional = false;}
            }
        } 
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }
    
    public Comentarios() {
      
    }
    
    public void guardarComentarioProy() {
        //JOptionPane.showMessageDialog(null,  "  msg: " + pramBita, "Error", JOptionPane.INFORMATION_MESSAGE);
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try 
        {   if(pramBita != null)
            {          
                comentarioProyCon = dao.buscaComentario(pramBita, capituloId, subcapituloId, seccionId, apartadoId); // , idusuario
                if(comentarioProyCon!=null && comentarioProyCon.getComentarioProyectoPK().getBitacoraProyecto() != null)
                { 
                    if(comentarioIA != null && comentarioIA.length()>0 && !comentarioIA.equals("<br>"))
                    {
                        //JOptionPane.showMessageDialog(null,  "error RRRR:  " + comentarioIA, "Error", JOptionPane.INFORMATION_MESSAGE);
                        comentarioProy.setInfoAdicional("1"); 
                                          
                    }
                    else
                    {
                        comentarioIA = "";
                        comentarioProy.setInfoAdicional("0");  
                    }
                    comentarioProy.setComentarioResolutivo(comentarioRes);  
                    comentarioProy.setComentarioMemoriaTecnica(comentarioMT);
                    comentarioProy.setComentarioInfoAdicional(comentarioIA); 
                    dao.merge(comentarioProy); 
                }
                else
                {                   
                    comentarioProy = new ComentarioProyecto();   
                    comentarioProy.setComentarioProyectoPK(new ComentarioProyectoPK(pramBita, capituloId, subcapituloId, seccionId, apartadoId , idusuario));
                                              //this.comentarioProyectoPK = new ComentarioProyectoPK(bitacoraProyecto, capituloId, subcapituloId, seccionId, apartadoId, comentarioIdusuario);

                    if(comentarioIA.length()>0 && !comentarioIA.equals("<br>"))
                    {
                            comentarioProy.setInfoAdicional("1");                   
                    }
                    else
                    {
                        comentarioIA="";
                        comentarioProy.setInfoAdicional("0");  
                    }                

                    comentarioProy.setComentarioResolutivo(comentarioRes);  
                    comentarioProy.setComentarioMemoriaTecnica(comentarioMT);
                    comentarioProy.setComentarioInfoAdicional(comentarioIA);                
                    dao.agrega(comentarioProy); 
                }
                reqcontEnv.execute("alert('Su información ha sido guardada con éxito.');refrescar()");
            }            
        } catch (Exception e) {
            e.printStackTrace();
            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
        }
    }    
 
    /**
     * @return the comentarioProy
     */
    public ComentarioProyecto getComentarioProy() {
        return comentarioProy;
    }

    /**
     * @param comentarioProy the comentarioProy to set
     */
    public void setComentarioProy(ComentarioProyecto comentarioProy) {
        this.comentarioProy = comentarioProy;
    }

    /**
     * @return the infoAdicional
     */
    public Boolean getInfoAdicional() {
        return infoAdicional;
    }

    /**
     * @param infoAdicional the infoAdicional to set
     */
    public void setInfoAdicional(Boolean infoAdicional) {
        this.infoAdicional = infoAdicional;
    }

    

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }
   
    /**
     * @return the comentarioRes
     */
    public String getComentarioRes() {
        return comentarioRes;
    }

    /**
     * @param comentarioRes the comentarioRes to set
     */
    public void setComentarioRes(String comentarioRes) {
        this.comentarioRes = comentarioRes;
    }

    /**
     * @return the comentarioIA
     */
    public String getComentarioIA() {
        return comentarioIA;
    }

    /**
     * @param comentarioIA the comentarioIA to set
     */
    public void setComentarioIA(String comentarioIA) {
        this.comentarioIA = comentarioIA;
    }

    /**
     * @return the comentarioMT
     */
    public String getComentarioMT() {
        return comentarioMT;
    }

    /**
     * @param comentarioMT the comentarioMT to set
     */
    public void setComentarioMT(String comentarioMT) {
        this.comentarioMT = comentarioMT;
    }

    /**
     * @return the comentarioTipo
     */
    public String getComentarioTipo() {
        return comentarioTipo;
    }

    /**
     * @param comentarioTipo the comentarioTipo to set
     */
    public void setComentarioTipo(String comentarioTipo) {
        this.comentarioTipo = comentarioTipo;
    }

    /**
     * @return the permiteGuaradar
     */
    public Boolean getPermiteGuaradar() {
        return permiteGuaradar;
    }

    /**
     * @param permiteGuaradar the permiteGuaradar to set
     */
    public void setPermiteGuaradar(Boolean permiteGuaradar) {
        this.permiteGuaradar = permiteGuaradar;
    }

    /**
     * @return the esIP
     */
    public Boolean getEsIP() {
        return esIP;
    }

    /**
     * @param esIP the esIP to set
     */
    public void setEsIP(Boolean esIP) {
        this.esIP = esIP;
    }

    /**
     * @return the habilitaRes
     */
    public Boolean getHabilitaRes() {
        return habilitaRes;
    }

    /**
     * @param habilitaRes the habilitaRes to set
     */
    public void setHabilitaRes(Boolean habilitaRes) {
        this.habilitaRes = habilitaRes;
    }

    /**
     * @return the habilitaIA
     */
    public Boolean getHabilitaIA() {
        return habilitaIA;
    }

    /**
     * @param habilitaIA the habilitaIA to set
     */
    public void setHabilitaIA(Boolean habilitaIA) {
        this.habilitaIA = habilitaIA;
    }

    /**
     * @return the habilitaMT
     */
    public Boolean getHabilitaMT() {
        return habilitaMT;
    }

    /**
     * @param habilitaMT the habilitaMT to set
     */
    public void setHabilitaMT(Boolean habilitaMT) {
        this.habilitaMT = habilitaMT;
    }
}
