/*
 * 
 */
package mx.gob.semarnat.view;
//<editor-fold defaultstate="collapsed" desc="Imports">

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.secure.IsInRole;
import mx.gob.semarnat.secure.Util;//</editor-fold>
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;

/**
 *
 * @author Rodrigo
 */
@SuppressWarnings("serial")
@ManagedBean(name = "proyectosView")
@ViewScoped
public class TablaProyectosView implements Serializable{
    private static final Logger logger = Logger.getLogger(TablaProyectosView.class.getName());
    
    private String bitaSelect;
    private String titulo;
    private String titulo2;
    private String sintesis;
    private final BitacoraDao dao = new BitacoraDao();
    private List<BandejaProyecto> lstBandejaTodos;
    private List<BandejaProyecto> lstBandejaTurnados;
    private List<BandejaProyecto> lstFilterBandejaTodos;
    private List<BandejaProyecto> lstFilterBandejaTurnados;

    @ManagedProperty("#{isInRole}")
    private IsInRole isInRole;
    
    @SuppressWarnings("unused")
	@PostConstruct
    public void init() {      
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        String vista = null;
        
        System.out.println("ventaventaventaventa"+ vista);
        
        HttpSession sesion = Util.getSession();
        String rol = sesion.getAttribute("rol").toString();
        vista= sesion.getAttribute("vista").toString();
        Tbarea tbarea = (Tbarea) sesion.getAttribute("areaDelegacionUsuario");
//        ArrayList<String> rol2 = (ArrayList<String>) sesion.getAttribute("rol");
//        List<String> roles = (List<String>) sesion.getAttribute("rol");
        
        List<Bitacora> listBitacora;
        String idArea;
        Integer idusuario;
        Bitacora bitacora;
        SinatDgira sinatDgira;
        
        //llena lista con los proyectos turnados
        titulo = "Proyectos Pendientes por Turnar";
        //llena la lista de acuerdo al rol en el que es asignado
        
//        for (String rol : roles) {
        	System.out.println(rol.toUpperCase());
                if(vista != null){
	        switch (rol.toUpperCase()) {
	            case "DA":
	                listBitacora = dao.enECC(); //TODOS LOS TRÃ�MITE CON SITUACIÃ“N CIS104
	                FacesContext fContext = FacesContext.getCurrentInstance();
	                fContext.getExternalContext().getSessionMap().get("userFolioProy");
	
	                idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
	                idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
//		                      System.out.println("----------------->T BAREA: " + tbarea);
                        if(vista.equals("uno")){
                            lstBandejaTodos = llenaDatosBandeja(dao.enRecepDirector2(tbarea));
                            lstBandejaTurnados = llenaDatosBandeja(new ArrayList<Object[]>());
                        }else{
                            lstBandejaTurnados = llenaDatosBandeja(dao.enRecepDirectorTurnados(tbarea));
                            lstBandejaTodos = llenaDatosBandeja(new ArrayList<Object[]>());
                        }
                        //todosProy = dao.enRecepDirector2();//11 y 12
	                //proyectosTurnados = dao.enRecepDirectorTurnados(); //11 y 12
	                titulo2 = "Proyectos en proceso";
	                
	                if (isInRole != null) {
		    			isInRole.setDirectorGeneral(false);
		    			isInRole.setDirectorArea(true);
		    			isInRole.setSubDirector(false);
		    			isInRole.setEvaluador(false);						
					}
	                	                
	                break;
	            case "SD":
                        if(vista.equals("uno")){
                            lstBandejaTodos = llenaDatosBandeja(dao.enRecepSubDirec(tbarea));
                            lstBandejaTurnados = llenaDatosBandeja(new ArrayList<Object[]>());
                        }else{
                            lstBandejaTurnados = llenaDatosBandeja(dao.enRecepSubDirecTurnados(tbarea));
                            lstBandejaTodos = llenaDatosBandeja(new ArrayList<Object[]>());
                        }	                
//todosProy = dao.enRecepSubDirec(); //12 y 13
	                //proyectosTurnados = dao.enRecepSubDirecTurnados(); //11 y 12
	                titulo2 = "Proyectos en proceso";

                
	                if (isInRole != null) {
	                	isInRole.setDirectorGeneral(false);
	                	isInRole.setDirectorArea(false);
	                	isInRole.setSubDirector(true);
	                	isInRole.setEvaluador(false);
	                }
	                
	                break;
	            case "EVA":
                        if(vista.equals("uno")){
                            lstBandejaTodos = llenaDatosBandeja(dao.enRecepEvaluador(tbarea));
                            lstBandejaTurnados = llenaDatosBandeja(new ArrayList<Object[]>());
                        }else{
                            lstBandejaTurnados = llenaDatosBandeja(dao.enRecepEvaluadorTurnados(tbarea));
                            lstBandejaTodos = llenaDatosBandeja(new ArrayList<Object[]>());
                        }
//todosProy = dao.enRecepEvaluador();//11 y 12
	                titulo = "Proyectos en Evaluación";
	                //proyectosTurnados = dao.enRecepEvaluadorTurnados();//11 y 12
	                titulo2 = "Proyectos en proceso";
	                                        
	                if (isInRole != null) {
		                isInRole.setDirectorGeneral(false);
		    			isInRole.setDirectorArea(false);
		    			isInRole.setSubDirector(false);
		    			isInRole.setEvaluador(true);  
	                }
	                
	                break;
	            case "DG":
                        if(vista.equals("uno")){
                            lstBandejaTodos = llenaDatosBandeja(dao.enRecepDGOfic((short)4, tbarea));
                            lstBandejaTurnados = llenaDatosBandeja(new ArrayList<Object[]>());
                        }else{
                            lstBandejaTurnados = llenaDatosBandeja(dao.enRecepDGOficFirmados((short)4, tbarea));
                            lstBandejaTodos = llenaDatosBandeja(new ArrayList<Object[]>());
                        }
	                
	                //todosProy = dao.enRecepDGOfic((short)4);//12 y 13
	                titulo = "Proyectos con Oficio pendiente de firma";
	                //proyectosTurnados = dao.enRecepDGOficFirmados((short)4); //12 y 13
	                titulo2 = "Proyectos con Oficio firmado";
	                
	                if (isInRole != null) {
		    			isInRole.setDirectorGeneral(true);
		    			isInRole.setDirectorArea(false);
		    			isInRole.setSubDirector(false);
		    			isInRole.setEvaluador(false);
	                }
	                
	                break;
	        }
                }
			
//		}
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));        
	}   
    

    //<editor-fold defaultstate="collapsed" desc="Getter & setter">

    public String getBitaSelect() {
        return bitaSelect;
    }

    public void setBitaSelect(String bitaSelect) {
        this.bitaSelect = bitaSelect;
    }
    private  final DgiraMiaeDaoGeneral daoG = new DgiraMiaeDaoGeneral();

    public String getSintesis() {
        if (bitaSelect != null) {
            EvaluacionProyecto ev = (EvaluacionProyecto) daoG.busca(EvaluacionProyecto.class, bitaSelect);
            bitaSelect = null;
            sintesis = ev.getEvaSistesisProyecto();
        } else {
            sintesis = "";
        }
        return sintesis;
    }

    public void setSintesis(String sintesis) {
        this.sintesis = sintesis;
    }
    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the titulo2
     */
    public String getTitulo2() {
        return titulo2;
    }

    /**
     * @param titulo2 the titulo2 to set
     */
    public void setTitulo2(String titulo2) {
        this.titulo2 = titulo2;
    }
    
    public List<BandejaProyecto> getLstBandejaTodos() {
        return lstBandejaTodos;
    }

    public List<BandejaProyecto> getLstBandejaTurnados() {
        return lstBandejaTurnados;
    }
    //</editor-fold>

    public List<BandejaProyecto> getLstFilterBandejaTodos() {
        return lstFilterBandejaTodos;
    }

    public void setLstFilterBandejaTodos(List<BandejaProyecto> lstFilterBandejaTodos) {
        this.lstFilterBandejaTodos = lstFilterBandejaTodos;
    }

    public List<BandejaProyecto> getLstFilterBandejaTurnados() {
        return lstFilterBandejaTurnados;
    }

    public void setLstFilterBandejaTurnados(List<BandejaProyecto> lstFilterBandejaTurnados) {
        this.lstFilterBandejaTurnados = lstFilterBandejaTurnados;
    }
    
    
	public class BandejaProyecto implements Serializable {
        //BITACORA.BITACORA
        private String bitaNumero;//[0]
        private String bitaIdProm;//[1]
        private String bitaSituacion;//[2]
        private Date bitaFechRegistro;//[6]
        private Date bitaFmaxResolucion;//[7]
        private long bitaDiasProceso;//[12]
        private Integer bitaDiasTramite;//[13]
        //BITACORA_TEMATICA.PROYECTO
        private String cve;//[3]
        private String nombreProyecto;//[4]
        //PADRON.DATOS_FISCALES
        private String nombreUsuario;//[5]
        //CATALOGO_FLUJOS.TBSITUACION
        private String descripcion;//[9]
        //Calculados
        private String descTramite;//[8]
        private String tipoTramite;//[10]
        private double porcentaje;//[14]
        private String urlDG;//[11]

        public BandejaProyecto(Object[] obj) {
            this.bitaNumero = (String)obj[0];
            this.bitaIdProm = (String)obj[1];
            this.bitaSituacion = (String)obj[2];
            this.cve = (String)obj[3];
            this.nombreProyecto = (String)obj[4];
            this.nombreUsuario = (String)obj[5];
            this.bitaFechRegistro = (Date)obj[6];
            this.bitaFmaxResolucion = (Date)obj[7];
            this.descTramite = (String)obj[8];
            this.descripcion = (String)obj[9];
            this.tipoTramite = (String)obj[10];
            this.urlDG = obj[11].toString();//obj[11] instanceof java.lang.String ? obj[11].toString() : obj[11].toString();
//            System.out.println("String: " + obj[12]);
            this.bitaDiasProceso = obj[12] instanceof java.lang.String ? 
                    (long)0 : 
                    Long.parseLong(obj[12].toString());
            this.bitaDiasTramite = Integer.parseInt(obj[13].toString());
            this.porcentaje = Double.parseDouble("" + obj[14].toString());
        }

        private BandejaProyecto() {
           // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public String getBitaNumero() {
            return bitaNumero;
        }

        public void setBitaNumero(String bitaNumero) {
            this.bitaNumero = bitaNumero;
        }

        public String getBitaIdProm() {
            return bitaIdProm;
        }

        public void setBitaIdProm(String bitaIdProm) {
            this.bitaIdProm = bitaIdProm;
        }

        public String getBitaSituacion() {
            return bitaSituacion;
        }

        public void setBitaSituacion(String bitaSituacion) {
            this.bitaSituacion = bitaSituacion;
        }

        public Date getBitaFechRegistro() {
            return bitaFechRegistro;
        }

        public void setBitaFechRegistro(Date bitaFechRegistro) {
            this.bitaFechRegistro = bitaFechRegistro;
        }

        public Date getBitaFmaxResolucion() {
            return bitaFmaxResolucion;
        }

        public void setBitaFmaxResolucion(Date bitaFmaxResolucion) {
            this.bitaFmaxResolucion = bitaFmaxResolucion;
        }

        public long getBitaDiasProceso() {
            return bitaDiasProceso;
        }

        public void setBitaDiasProceso(long bitaDiasProceso) {
            this.bitaDiasProceso = bitaDiasProceso;
        }

        public Integer getBitaDiasTramite() {
            return bitaDiasTramite;
        }

        public void setBitaDiasTramite(Integer bitaDiasTramite) {
            this.bitaDiasTramite = bitaDiasTramite;
        }

        public String getCve() {
            return cve;
        }

        public void setCve(String cve) {
            this.cve = cve;
        }

        public String getNombreProyecto() {
            return nombreProyecto;
        }

        public void setNombreProyecto(String nombreProyecto) {
            this.nombreProyecto = nombreProyecto;
        }

        public String getNombreUsuario() {
            return nombreUsuario;
        }

        public void setNombreUsuario(String nombreUsuario) {
            this.nombreUsuario = nombreUsuario;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getDescTramite() {
            return descTramite;
        }

        public void setDescTramite(String descTramite) {
            this.descTramite = descTramite;
        }

        public String getTipoTramite() {
            return tipoTramite;
        }

        public void setTipoTramite(String tipoTramite) {
            this.tipoTramite = tipoTramite;
        }

        public double getPorcentaje() {
            return porcentaje;
        }

        public void setPorcentaje(long porcentaje) {
            this.porcentaje = porcentaje;
        }

        public String getUrlDG() {
            return urlDG;
        }

        public void setUrlDG(String urlDG) {
            this.urlDG = urlDG;
        }
        
    }
    
    private List<BandejaProyecto> llenaDatosBandeja(List<Object[]> lstObj) {
        List<BandejaProyecto> lstProyecto = new ArrayList<BandejaProyecto>();
        for (Object[] obj : lstObj) {
            lstProyecto.add(new BandejaProyecto(obj));
        }
        
        return lstProyecto;
    }

	/**
	 * @return the isInRole
	 */
	public IsInRole getIsInRole() {
		return isInRole;
	}

	/**
	 * @param isInRole the isInRole to set
	 */
	public void setIsInRole(IsInRole isInRole) {
		this.isInRole = isInRole;
	}
}
