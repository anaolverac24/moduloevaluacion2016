/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.EspecificacionAnexo;
import mx.gob.semarnat.model.dgira_miae.SustanciaAnexo;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "modArchivosView")
@RequestScoped
public class ModArchivosView {
    private static final Logger logger = Logger.getLogger(ModArchivosView.class.getName());

    private String folioProyecto;
    private String especifiacionId;
    private Short serialProyecto;
    private Short normaId;

    private List<EspecificacionAnexo> especificacionAnexos = new ArrayList();
    private List<AnexosProyecto> anexosProyecto = new ArrayList();
    private List<SustanciaAnexo> sustanciasProyecto = new ArrayList();
    private Boolean verEspAnexos;
    private Boolean verAnexos;
    private Boolean verSustancias;
    private VisorDao dao;

    @ManagedProperty("#{param.info}")
    private String info;

    @PostConstruct
    public void init() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        String[] args = info.split(",");
        dao = new VisorDao();
        if (args.length == 4) {
            folioProyecto = args[1];
            especifiacionId = args[0];
            serialProyecto = new Short(args[2]);
            normaId = new Short(args[3]);
            especificacionAnexos = dao.getEspecifiacionesAnexos(folioProyecto, especifiacionId, serialProyecto, normaId);
        }

        if (args.length == 6) {
            String folioProyecto = "";
            Short serialProyecto = (short) 0;
            short capituloId = (short) 0;
            short subcapituloId = (short) 0;
            short seccionId = (short) 0;
            short apartadoId = (short) 0;
            capituloId = new Short(args[0]);
            subcapituloId = new Short(args[1]);
            seccionId = new Short(args[2]);
            apartadoId = new Short(args[3]);
            folioProyecto = args[4];
            serialProyecto = new Short(args[5]);
            anexosProyecto = dao.getAnexos(capituloId, subcapituloId, seccionId, apartadoId, folioProyecto, serialProyecto);

        }
        if (args.length == 3) {
            Short sustanciaProyId;
            folioProyecto = args[0];
            serialProyecto = new Short(args[1]);
            sustanciaProyId = new Short(args[2]);
            sustanciasProyecto = dao.getSustanciaAnexos(folioProyecto, serialProyecto, sustanciaProyId);
        }

        if (especificacionAnexos.isEmpty()) {
            verEspAnexos = false;
        } else {
            verEspAnexos = true;
        }
        if (anexosProyecto.isEmpty()) {
            verAnexos = false;
        } else {
            verAnexos = true;
        }

        verSustancias = sustanciasProyecto.isEmpty();
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }

    public ModArchivosView() {
//        System.out.println("info " + info);
    }

    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return the especificacionAnexos
     */
    public List<EspecificacionAnexo> getEspecificacionAnexos() {
        return especificacionAnexos;
    }

    /**
     * @param especificacionAnexos the especificacionAnexos to set
     */
    public void setEspecificacionAnexos(List<EspecificacionAnexo> especificacionAnexos) {
        this.especificacionAnexos = especificacionAnexos;
    }

    /**
     * @return the verEspAnexos
     */
    public Boolean getVerEspAnexos() {
        return verEspAnexos;
    }

    /**
     * @param verEspAnexos the verEspAnexos to set
     */
    public void setVerEspAnexos(Boolean verEspAnexos) {
        this.verEspAnexos = verEspAnexos;
    }

    /**
     * @return the anexosProyecto
     */
    public List<AnexosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<AnexosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    /**
     * @return the verAnexos
     */
    public Boolean getVerAnexos() {
        return verAnexos;
    }

    /**
     * @param verAnexos the verAnexos to set
     */
    public void setVerAnexos(Boolean verAnexos) {
        this.verAnexos = verAnexos;
    }

    /**
     * @return the sustanciasProyecto
     */
    public List<SustanciaAnexo> getSustanciasProyecto() {
        return sustanciasProyecto;
    }

    /**
     * @param sustanciasProyecto the sustanciasProyecto to set
     */
    public void setSustanciasProyecto(List<SustanciaAnexo> sustanciasProyecto) {
        this.sustanciasProyecto = sustanciasProyecto;
    }

    /**
     * @return the verSustancias
     */
    public Boolean getVerSustancias() {
        return verSustancias;
    }

    /**
     * @param verSustancias the verSustancias to set
     */
    public void setVerSustancias(Boolean verSustancias) {
        this.verSustancias = verSustancias;
    }
}
