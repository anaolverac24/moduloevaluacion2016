/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.PersistenceManager;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.dgira_miae.EspecificacionAnexo;
import mx.gob.semarnat.model.dgira_miae.EstudiosEspProy;
import mx.gob.semarnat.model.dgira_miae.SustanciaAnexo;
import mx.gob.semarnat.utils.Utils;

/**
 *
 * @author mauricio
 */
public class Preview extends HttpServlet {
    private static final Logger logger = Logger.getLogger(Preview.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        String a = request.getParameter("idt");
        String tipo = request.getParameter("tipo"); 
        String folio = request.getParameter("folio"); 
        String serial = request.getParameter("serial"); 
        String nombre = ";";
        EntityManager em = PersistenceManager.getEmfMIAE().createEntityManager();
        String filePath = "";
        String ext = "";

        if (tipo.equals("archivo_proyecto")) {
        	System.out.println("Entra a archivo proyecto con id :: " + a.trim());
            Query q = em.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.seqId = :url");
            q.setParameter("url", Short.valueOf(a.trim()));
            ArchivosProyecto e;
            try {
                e = (ArchivosProyecto) q.getSingleResult();
            } catch (Exception err) {
                System.out.println("No ENCONTRADA");
                Query q2 = em.createQuery("SELECT e FROM ArchivosProyecto e");
                List<ArchivosProyecto> l = q2.getResultList();
                e = l.get(0);
            }
            filePath = e.getUrl();
            ext = e.getExtension().toLowerCase().trim();
        } else if (tipo.equals("general")) {
            Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.anexoId = :url");
            q.setParameter("url", Short.valueOf(a.trim()));
            AnexosProyecto e;
            try {
                e = (AnexosProyecto) q.getSingleResult();
            } catch (Exception err) {
                System.out.println("No ENCONTRADA");
                Query q2 = em.createQuery("SELECT e FROM AnexosProyecto e");
                List<AnexosProyecto> l = q2.getResultList();
                e = l.get(0);
            }
            filePath = e.getAnexoUrl();
            ext = e.getAnexoExtension().toLowerCase().trim();
        } else if (tipo.equals("sustancia")) {
            Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.sustanciaAnexoId = :url");
            q.setParameter("url", Short.valueOf(a.trim()));
            SustanciaAnexo s;
            try {
                s = (SustanciaAnexo) q.getSingleResult();
                nombre = s.getAnexoNombre();
            } catch (Exception e) {
                System.out.println("NO encontrado");
                Query q2 = em.createQuery("SELECT s FROM SustanciaAnexo s");
                List<SustanciaAnexo> l = q2.getResultList();
                s = l.get(0);
            }
            filePath = s.getAnexoUrl();
            ext = s.getAnexoExtension().toLowerCase().trim();
        }else if (tipo.equals("estudio")) {
            Query q = em.createQuery("SELECT e FROM EstudiosEspProy e WHERE e.estudiosEspProyPK.folioSerial = :folio AND e.estudiosEspProyPK.serialProyecto = :serial AND e.estudiosEspProyPK.estudioId = :url AND e.anexoNombre IS NOT NULL");
            q.setParameter("folio",folio);
            q.setParameter("serial",Short.valueOf(serial.trim()));
            q.setParameter("url", Short.valueOf(a.trim()));
            EstudiosEspProy e;
            try {
                e = (EstudiosEspProy) q.getSingleResult();
                nombre = e.getAnexoNombre();
            } catch (Exception ex) {
                System.out.println("NO encontrado");
                Query q2 = em.createQuery("SELECT s FROM EstudiosEspProy s");
                List<EstudiosEspProy> l = q2.getResultList();
                e = l.get(0);
            }
            filePath = e.getAnexoUrl();
            ext = e.getAnexoExtension().toLowerCase().trim();
        } else if(tipo.equals("oficio")){
            System.out.println("Estp servira para un oficio");
            String bitaProyecto = request.getParameter("bitaProyecto"); 
            String idDocumento = request.getParameter("idDocumento"); 
            String tipoOficio = request.getParameter("tipoOficio"); 
            
            DgiraMiaeDaoGeneral daoDGIRA = new DgiraMiaeDaoGeneral();
            DocumentoRespuestaDgira docRespuesta = daoDGIRA.getDocumentotoRespuestaDgira(bitaProyecto,
                idDocumento, Short.valueOf(tipoOficio));
            filePath = docRespuesta.getDocumentoUbicacion();
            ext = "pdf";
        } else {

            Query q = em.createQuery("SELECT e FROM EspecificacionAnexo e WHERE e.especificacionAnexoPK.especificacionAnexoId = :url");
            q.setParameter("url", Short.valueOf(a.trim()));
            EspecificacionAnexo e;
            try {
                e = (EspecificacionAnexo) q.getSingleResult();
            } catch (Exception err) {
                System.out.println("No ENCONTRADA");
                Query q2 = em.createQuery("SELECT e FROM EspecificacionAnexo e");
                List<EspecificacionAnexo> l = q2.getResultList();
                e = l.get(0);
            }
            filePath = e.getEspecAnexoUrl();
            ext = e.getEspecAnexoExtension().toLowerCase().trim();
        }

        System.out.println("" + filePath);
        File downloadFile = new File(filePath);
        FileInputStream inStream = new FileInputStream(downloadFile);

        String mimeType = "application/octet";
        System.out.println("ext " + ext);
        if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
            mimeType = "image/" + ext;
        }
        if (ext.equals("pdf")) {
            mimeType = "application/pdf";
        }

        response.setContentType(mimeType);
        System.out.println("mimeType " + mimeType);

        response.setContentLength((int) downloadFile.length());
        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inStream.close();
        outStream.close();
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
