/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.Util;

import mx.gob.semarnat.model.dgira_miae.RepLegalProyecto;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;

/**
 *
 * @author Rodrigo
 */
public class RepLegalu {
    private Vexdatosusuariorep vexdatosusuariorep;
    private RepLegalProyecto repLegal;
    
    /**
     * @return the vexdatosusuariorep
     */
    public Vexdatosusuariorep getVexdatosusuariorep() {
        return vexdatosusuariorep;
    }

    /**
     * @param vexdatosusuariorep the vexdatosusuariorep to set
     */
    public void setVexdatosusuariorep(Vexdatosusuariorep vexdatosusuariorep) {
        this.vexdatosusuariorep = vexdatosusuariorep;
    }

    /**
     * @return the repLegal
     */
    public RepLegalProyecto getRepLegal() {
        return repLegal;
    }

    /**
     * @param repLegal the repLegal to set
     */
    public void setRepLegal(RepLegalProyecto repLegal) {
        this.repLegal = repLegal;
    }
}
