/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view.Util;

import java.util.Date;

/**
 *
 * @author mauricio
 */
public class ANPu {

    private Short id;
    private String tipo;
    private String APN;
    private String categoria;
    private Date fechaDecreto;
    private Date fechaPrograma;
    private String criterio;
    private String compatible;
    private String vinculacion;

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the APN
     */
    public String getAPN() {
        return APN;
    }

    /**
     * @param APN the APN to set
     */
    public void setAPN(String APN) {
        this.APN = APN;
    }

    /**
     * @return the categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the fechaDecreto
     */
    public Date getFechaDecreto() {
        return fechaDecreto;
    }

    /**
     * @param fechaDecreto the fechaDecreto to set
     */
    public void setFechaDecreto(Date fechaDecreto) {
        this.fechaDecreto = fechaDecreto;
    }

    /**
     * @return the fechaPrograma
     */
    public Date getFechaPrograma() {
        return fechaPrograma;
    }

    /**
     * @param fechaPrograma the fechaPrograma to set
     */
    public void setFechaPrograma(Date fechaPrograma) {
        this.fechaPrograma = fechaPrograma;
    }

    /**
     * @return the criterio
     */
    public String getCriterio() {
        return criterio;
    }

    /**
     * @param criterio the criterio to set
     */
    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    /**
     * @return the compatible
     */
    public String getCompatible() {
        return compatible;
    }

    /**
     * @param compatible the compatible to set
     */
    public void setCompatible(String compatible) {
        this.compatible = compatible;
    }

    /**
     * @return the vinculacion
     */
    public String getVinculacion() {
        return vinculacion;
    }

    /**
     * @param vinculacion the vinculacion to set
     */
    public void setVinculacion(String vinculacion) {
        this.vinculacion = vinculacion;
    }

    /**
     * @return the id
     */
    public Short getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Short id) {
        this.id = id;
    }
}
