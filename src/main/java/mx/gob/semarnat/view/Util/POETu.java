/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view.Util;

/**
 *
 * @author mauricio
 */
public class POETu {

    private Short id;
    private String tipo;
    private String nombreInstrumento;
    private String numNom;
    private String politicaAmbiental;
    private String uso;
    private String criterios;
    private String compatibles;
    private String vinculacion;
    private Boolean edit;
    
    
    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the nombreInstrumento
     */
    public String getNombreInstrumento() {
        return nombreInstrumento;
    }

    /**
     * @param nombreInstrumento the nombreInstrumento to set
     */
    public void setNombreInstrumento(String nombreInstrumento) {
        this.nombreInstrumento = nombreInstrumento;
    }

    /**
     * @return the numNom
     */
    public String getNumNom() {
        return numNom;
    }

    /**
     * @param numNom the numNom to set
     */
    public void setNumNom(String numNom) {
        this.numNom = numNom;
    }

    /**
     * @return the politicaAmbiental
     */
    public String getPoliticaAmbiental() {
        return politicaAmbiental;
    }

    /**
     * @param politicaAmbiental the politicaAmbiental to set
     */
    public void setPoliticaAmbiental(String politicaAmbiental) {
        this.politicaAmbiental = politicaAmbiental;
    }

    /**
     * @return the uso
     */
    public String getUso() {
        return uso;
    }

    /**
     * @param uso the uso to set
     */
    public void setUso(String uso) {
        this.uso = uso;
    }

    /**
     * @return the criterios
     */
    public String getCriterios() {
        return criterios;
    }

    /**
     * @param criterios the criterios to set
     */
    public void setCriterios(String criterios) {
        this.criterios = criterios;
    }

    /**
     * @return the compatibles
     */
    public String getCompatibles() {
        return compatibles;
    }

    /**
     * @param compatibles the compatibles to set
     */
    public void setCompatibles(String compatibles) {
        this.compatibles = compatibles;
    }

    /**
     * @return the vinculacion
     */
    public String getVinculacion() {
        return vinculacion;
    }

    /**
     * @param vinculacion the vinculacion to set
     */
    public void setVinculacion(String vinculacion) {
        this.vinculacion = vinculacion;
    }

    /**
     * @return the id
     */
    public Short getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Short id) {
        this.id = id;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
    if (tipo == null){
            return true;
        }
        if (tipo.equals("TIPO_OE")){
            return true;
        }
        if (tipo.equals("POEM_TIPO")){
            return false;
        }
        return true;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    
    
}
