/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.Util;

/**
 *
 * @author mauricio
 */
public class PDUu {

    private String nombrePlan;
    private String usos;
    private String claveUsos;
    private String cos;
    private String cus;
    private String vinculacion;
    private String tipo;
    private Short id;
    private Boolean edit;

    /**
     * @return the nombrePlan
     */
    public String getNombrePlan() {
        return nombrePlan;
    }

    /**
     * @param nombrePlan the nombrePlan to set
     */
    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    /**
     * @return the usos
     */
    public String getUsos() {
        return usos;
    }

    /**
     * @param usos the usos to set
     */
    public void setUsos(String usos) {
        this.usos = usos;
    }

    /**
     * @return the claveUsos
     */
    public String getClaveUsos() {
        return claveUsos;
    }

    /**
     * @param claveUsos the claveUsos to set
     */
    public void setClaveUsos(String claveUsos) {
        this.claveUsos = claveUsos;
    }

    /**
     * @return the cos
     */
    public String getCos() {
        return cos;
    }

    /**
     * @param cos the cos to set
     */
    public void setCos(String cos) {
        this.cos = cos;
    }

    /**
     * @return the cus
     */
    public String getCus() {
        return cus;
    }

    /**
     * @param cus the cus to set
     */
    public void setCus(String cus) {
        this.cus = cus;
    }

    /**
     * @return the vinculacion
     */
    public String getVinculacion() {
        return vinculacion;
    }

    /**
     * @param vinculacion the vinculacion to set
     */
    public void setVinculacion(String vinculacion) {
        this.vinculacion = vinculacion;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the id
     */
    public Short getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Short id) {
        this.id = id;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        if (tipo != null && tipo.equals("Pdum")) {
            return true;
        }

        return false;
//        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {

        this.edit = edit;
    }

}
