/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.CadenaFirmaDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.LoginDAO;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.dao.procedures.SegundasVias;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.CadenaFirma;
import mx.gob.semarnat.model.bitacora.CadenaFirmaLog;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento;
import mx.gob.semarnat.model.dgira_miae.CatTipoDocDgira;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbusuario;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.secure.AppSession;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import net.firel.BcAgregaCadenaRespuesta;
import net.firel.DetallesFirmado;
import net.firel.Firmas;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import com.itextpdf.text.DocumentException;



/**
 * Clase que contiene la funcionalidad de firma basicamente de Director General asi como corrección de los distintos oficios existentes
 * @author Rodrigo
 */
@ManagedBean(name = "segViasView")
@ViewScoped
public class SegundasViasView {
    private static final Logger logger = Logger.getLogger(SegundasViasView.class.getName());
    
    private String cveTipTram; 
    private Integer idTipTram;
    private String edoGestTram;
    private String idArea = ""; 
    private Integer idusuario = 0;
    private String bitaProyecto;
    private BitacoraDao dao = new BitacoraDao();
    private DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
    private SinatDgira sinatDgira;
    @SuppressWarnings("unused")
	private Bitacora bitacora1;
    private String folio;
    private String claveProyecto;
    @SuppressWarnings("unused")
	private short serialProyecto;
    private Integer idAux = 0;
    private short maxIdHisto = 0; 
    @SuppressWarnings("unused")
	private Historial bitacoraAT0022; 
    @SuppressWarnings("unused")
	private Historial bitacoraI00010;
    private Bitacora bitacora2;
    private List<Object[]> maxDocsDgiraFlujoHist  = new ArrayList<Object[]>();
    private DocumentoDgiraFlujoHist docsDgiraFlujoHist;
    private DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
    private DocumentoDgiraFlujo docsDgiraFlujo;
    private List<CatEstatusDocumento> subSectorCat; //
    private CatEstatusDocumento catEstatusDoc;
    private short idDocumento = 0; //variable que guarda la seleccion del combo de estaus del documento
    private String comentarioTurnado = "";
    private short tipoDoc = 0; 
    private String tipoDocId = "";
    private String nombreOfic; 
    private String urlOfic; 
    @SuppressWarnings("unused")
	private String statusProy;
    @SuppressWarnings("unused")
	private Tbarea tbArea;
    @SuppressWarnings("unused")
	private Tbarea tbAreaEnvio;
    private String perfilSig;
    private String URLgenOficPre;
    private String URLdb;
    private Integer statusDoc;
	private List<Object[]> areaResiveDocCon  = new ArrayList<Object[]>();
    private List<Object[]> DocRespDgira  = new ArrayList<Object[]>();
    private List<Object[]> statusDocRespDgira  = new ArrayList<Object[]>();
	private String jerarquia; 
    private HttpSession sesion = Util.getSession();
    private Boolean turnaDocsBtn = false;
    private Boolean turnaDocs = false;
    private String bitaProyOfic;
    private List<Object[]> documentoRespuestaDgira = new ArrayList<Object[]>();
    
    
    private Connection connection = null;
    private static final String driverDB = "oracle.jdbc.OracleDriver";
    // ================= POOL DE DESARROLLO ==========================
    private static final String urlDB = "jdbc:oracle:thin:@scan-db.semarnat.gob.mx:1525/SINDESA";
    private static final String userDB = "DGIRA_MIAE2";
    private static final String passDB = "DGIRA_MIAE2";
    // ================= POOL DE PRODUCCION ==========================
    /*private static final String urlDB = "jdbc:oracle:thin:@rac1-scan.semarnat.gob.mx:1525/SINAT";
    private static final String userDB = "DGIRA_MIAE2";
    private static final String passDB = "Dg1r4MiaE2";*/
    private SegundasVias segundasVias = new SegundasVias();
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private Boolean muestraSec = false;
    private int tipoSuspencion=0; 
    private int tipoApertura = 0;
 
    //variables resolutivo
    private short catTipoDoc=0;
    private int rolId=0; // rolId del rol con el que el usuario entro a la bitacora ( enel combo de opciones inicial )
    private List<CatTipoDocDgira> tipoDocCat; 
    private CatTipoDocDgira catTipoDoc2;
    private SelectItem[] catTipDoc;
    private Boolean muestTurnOrte= false; //bandera que indica si se vera seccion de turnado a RTE
    private String titulo ="";
    
    @SuppressWarnings("unused")
	private List<Historial> HistorialCIS303;
    private Boolean deInfoAdic = false; //bandera que indica si un tramite ya vino de una suspencion por info adicional
    @SuppressWarnings("unused")
	private List<Historial> NoHistorialAT0022; 
    @SuppressWarnings("unused")
	private List<Historial> NoHistorialCIS106;
    @SuppressWarnings("unused")
	private List<Historial> NoHistorial200501;
    @SuppressWarnings("unused")
	private List<Historial> NoHistorialIRA020;
    private List<Historial> NoHistorialAT0011;
    
    private String nombTipoOfic="";
    private List<Object[]> comenatriosTab = new ArrayList<>();
    private Boolean tramYaTurnadoDG = false;
        
   
    private StreamedContent file;
    @SuppressWarnings("unused")
	private String siFirmar;
    
    Tbusuario usuariorecibe = null;
    String usuariorecibetxt = "Nuevo destinatario";
    
    private Tbarea tbarea = new Tbarea();
    private String idEntidad = "";
    
     // valor 0 o 1 que indica si esta bitacora (pantalla de checklist) biene de una invocación del DG
     private int isUrlDG = 0;
     // valor 1 o 2 que indica si es la primer bandeja o segunda bandeja del DG ( si no es del dg ni si quiera se trae el valor en los parametros de la url 
     private int bandeja = 1; // por defecto es la bandeja 1
     // Generar Oficio, Turnar Oficio
     private int GO=0, TO=0;
    
    @SuppressWarnings({ "rawtypes", "unused" })
	public SegundasViasView() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        System.out.println("\nSegundasViasView..");
        
        FacesContext fContext = FacesContext.getCurrentInstance();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        Map params = ec.getRequestParameterMap();
        
        daoDg = new DgiraMiaeDaoGeneral();
        dao = new BitacoraDao();
       
        tbarea = (Tbarea) sesion.getAttribute("areaDelegacionUsuario");
        idEntidad = tbarea.getIdentidadfederativa();
        
        // esta informacion tiene valores en la sesion cuando lo invoca el dg, en otros roles no biene en la url por tanto en detalleproyectoview no se cachan los parametros de la url
        isUrlDG = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("isUrlDG").toString());
        bandeja = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("bandeja").toString());
        
        System.out.println("isUrlDG: " + isUrlDG);
        System.out.println("bandeja: " + bandeja);
        
        if(fContext.getExternalContext().getSessionMap().get("bitacoraProy") != null)
        {
            bitaProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
            idTipTram = daoBitacora.idTipTram(bitaProyecto);
            
            
            if(tipoDoc == 0) 
            {tipoDoc = 7; }    
            System.out.println("tipo de Oficio: " + params.get("tipoOfic"));
        }
        else
        {
            bitaProyecto = params.get("bitaNum").toString();
            tipoDoc = Short.parseShort(params.get("tipoOfic").toString());
            tipoDocId = params.get("tipoOficId").toString();
            System.out.println("tipo de Oficio1:  " + tipoDoc + "  id oficio: " + tipoDocId); 
            //
            try
            {
                if (params.get("tipoOfic").toString().equals("7"))
                {
                    titulo= "Oficio de información adicional";
                }
                if (params.get("tipoOfic").toString().equals("4"))
                {
                    titulo= "Oficio de prevención";
                }
                if (params.get("tipoOfic").toString().equals("5"))
                {
                    titulo= "Prevención por CheckList y Pago de derechos";
                }
                if (params.get("tipoOfic").toString().equals("1"))
                {
                    titulo= "Oficio de opinión técnica";
                }
                if (params.get("tipoOfic").toString().equals("2"))
                {
                    titulo= "Oficio de notificación a gobiernos";
                }
                if (params.get("tipoOfic").toString().equals("3"))
                {
                    titulo= "Oficio de pago de derechos";
                }
                if (params.get("tipoOfic").toString().equals("8"))
                {
                    titulo= "Oficio de consulta pública-Delegación";
                }
                if (params.get("tipoOfic").toString().equals("9"))
                {
                    titulo= "Oficio de consulta pública-Promovente";
                }
                if (params.get("tipoOfic").toString().equals("10"))
                {
                    titulo= "Oficio de consulta pública-Positiva";
                }
                if (params.get("tipoOfic").toString().equals("11"))
                {
                    titulo= "Oficio de consulta pública-Ciudadanos";
                }
                
                if(params.get("tramYaTurnadoDG") != null && params.get("tramYaTurnadoDG").toString().length()>0)
                {System.out.println("tramYaTurnadoDG11: " + tramYaTurnadoDG);
                    if(params.get("tramYaTurnadoDG").toString().equals("1"))
                    { tramYaTurnadoDG = true; }
                    else { tramYaTurnadoDG = false; }            
                }
                else { tramYaTurnadoDG = false; }
                
                //
                if (params.get("tipoOfic").toString().equals("12") || params.get("tipoOfic").toString().equals("13") || params.get("tipoOfic").toString().equals("14"))
                {
                    muestTurnOrte = true;
                }
                else
                {
                    muestTurnOrte = false;
                }
            }
            catch(Exception mt)
            {}
        }
        if(tipoDoc == 7 || tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5)
        {tipoDocId="1";}
        
        System.out.println("tipo de Oficio2:  " + tipoDoc + "  id oficio: " + tipoDocId); 
        
                
            idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu");
            idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            
            UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
    		System.out.println("rolId     :" + ur.getRolId().getIdRol());
    		
            rolId = (Integer) ur.getRolId().getIdRol();
            
            Proyecto proy = null;
            VisorDao daoV = new VisorDao();
            FacesContext ctxt = FacesContext.getCurrentInstance();
            
            LoginDAO loginDao = new LoginDAO();
            usuariorecibe = loginDao.getUsuario(idusuario);
            usuariorecibetxt = "Nuevo destinatario";
    	    if(usuariorecibe != null){
    	        	usuariorecibetxt = usuariorecibe.getIdempleado().getNombre() + " " + usuariorecibe.getIdempleado().getApellidopaterno() + " " + usuariorecibe.getIdempleado().getApellidomaterno();
    	        System.out.println("Usuario recibe: " + usuariorecibetxt);
    	    }
    	    System.out.println("Usuario recibe: " + usuariorecibetxt);
    	    

            if (bitaProyecto != null) 
            {
                comenatriosTab = daoBitacora.tablabOsevOfic(bitaProyecto, tipoDoc, tipoDocId, idEntidad);
                System.out.println("comenatriosTab SegVíasGuradado 1 size: " + comenatriosTab.size());
                EvaluacionProyecto ev1 = daoBitacora.datosEvaProy(bitaProyecto);
                if(ev1 != null)
                {
                    if(ev1.getEvaTipoDocId() != null)
                    {catTipoDoc = ev1.getEvaTipoDocId();}
                }
                
                idTipTram = daoBitacora.idTipTram(bitaProyecto);
                edoGestTram = daoBitacora.entGestTram(bitaProyecto);
                cveTipTram = daoBitacora.cveTipTram(bitaProyecto);
                System.out.println("cveTipTram: " + cveTipTram + "  idTipTram: " + idTipTram + "  edoGestTram: " + edoGestTram);
                if (!cveTipTram.isEmpty() && !edoGestTram.isEmpty()) {
                    fContext.getExternalContext().getSessionMap().put("cveTramite", cveTipTram);
                    fContext.getExternalContext().getSessionMap().put("idTramite", idTipTram);
                    fContext.getExternalContext().getSessionMap().put("edoGestTramite", edoGestTram);
                }
                try
                {
                    proy = daoV.verificaERA2(bitaProyecto);
                    if (proy != null) 
                    {
                        if(proy.getProyectoPK() != null)
                        {
                            ctxt.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
                            ctxt.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
                            folio = proy.getProyectoPK().getFolioProyecto();
                        }                        
                        
                        claveProyecto = proy.getClaveProyecto();
                        
                        //cargar lista con estatus de documentos
                        if (sesion != null) {   //JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
                            if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
                            {
                                subSectorCat = daoDg.getCatStatusDocs();
                            }
                            if (sesion.getAttribute("rol").toString().equals("da")) //Director de Area
                            {
                                subSectorCat = daoDg.getCatStatusDocs2();
                            }
                            if (sesion.getAttribute("rol").toString().equals("sd")) //Sub Director
                            {
                                subSectorCat = daoDg.getCatStatusDocs2();
                            }
                            if (sesion.getAttribute("rol").toString().equals("eva")) //Evaluador
                            {
                                subSectorCat = daoDg.getCatStatusDocs3();
                            }
                        }

                        //boton de Turnar Oficio
                        //tipoDoc = 7;
                        try {                        
                            DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
                            //combo = todoOficDoc(); //llenar combo con todos lo aficios
                            //nombreOfic  estadoOfic

                        } catch (Exception er33) {
                            //JOptionPane.showMessageDialog(null, "idDocumento:  " + er33.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }
                        if (DocRespDgira.size() > 0) 
                        {
                            if(tipoDoc == 1)
                            { nombTipoOfic="Opinión técnica"; }
                            if(tipoDoc == 2)
                            { nombTipoOfic="Notificaciones a gobiernos"; }
                            if(tipoDoc == 7)
                            { nombTipoOfic="Información adicional"; }
                            if(tipoDoc == 4)
                            { nombTipoOfic="Prevención por CheckList"; }
                            if(tipoDoc == 3)
                            { nombTipoOfic="Prevención por Pago de derechos"; }
                            if(tipoDoc == 5)
                            { nombTipoOfic="Prevención por CheckList y Pago de derechos"; }
                            
                        // pool url
                        if(docsDgiraFlujo == null){
                        	// en este caso obtenemos el documento dgira porque no se ha consultado
                        	docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
                        }
                            
                        List<DocumentoRespuestaDgira> lista;
                        lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
                        for (DocumentoRespuestaDgira x : lista) {
                            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
                            
                            try{
	                            if(docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO){
	                            	urlOfic = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoDoc+"&entidadfederativa="+edoGestTram+"&documentoid="+tipoDocId;
	                            }else{
	                            	urlOfic = x.getDocumentoUrl();
	                            }
                            }catch(Exception exx){
                            	System.out.println("\nno asignó el urlOfic de docsDgiraFlujo ya que este ultimo es null");
                            	//exx.printStackTrace();
                            	if(x != null){
                            		urlOfic = x.getDocumentoUrl();
                            	}
                            }
                        }
                        for (Object o : DocRespDgira) {
                            URLdb = o.toString();
                        }
                    } else {
                        URLdb = "";
                    }
                    //JOptionPane.showMessageDialog(null, "turnaDocsBtnP :  " + turnaDocsBtnP , "Error", JOptionPane.INFORMATION_MESSAGE); 
                    //----------------------verificar si el boton para turnar los  oficios, el que se encinetra dentro del apartado Turnado de Oficio    
                    idAux = 0;
                    maxIdHisto = 0;
                    //se verifica el id maximo de oficio a tratar (en este caso el tipo de prevencion=4)
                    try {
                        maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);

                        for (Object o : maxDocsDgiraFlujoHist) {
                            idAux = Integer.parseInt(o.toString());
                            maxIdHisto = idAux.shortValue();
                        }
                    } catch (Exception xxx) {
                        //JOptionPane.showMessageDialog(null, "idDocumento:  " + xxx.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                    }
                    if (maxIdHisto > 0) {   //trae todo lo relaionado al id maximo de tabla DOCUMENTO_DGIRA_FLUJO_HIST, relativo a una bitacora
                        docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, maxIdHisto);
                        statusProy = daoBitacora.estatusTramBita(bitaProyecto);
                        try {
                            //estatus del oficio en cuestion 
                            statusDocRespDgira = daoDg.statusDocsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, maxIdHisto);
                        } catch (Exception er) {
                            //JOptionPane.showMessageDialog(null, "idDocumento:  " + er.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }

                        for (Object o : statusDocRespDgira) {
                            statusDoc = Integer.parseInt(o.toString());
                        }
                    }
                    //JOptionPane.showMessageDialog(null,  "area envio: " + docsDgiraFlujoHistEdit.getIdAreaEnvioHist() + "   maxIdHisto: " + maxIdHisto , "Error", JOptionPane.INFORMATION_MESSAGE);
                    if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                        
                    	// pool delegaciones
                    	String perfilUsr = "";
                    	try {
                            if(statusDoc == 4) //solo mostrara opciones de catalogos y descripcion de turnado de oficio para tramites ya firmados
                            {
                                if(docsDgiraFlujoHistEdit.getEstatusDocumentoIdHist() != null)
                                { idDocumento = docsDgiraFlujoHistEdit.getEstatusDocumentoIdHist().getEstatusDocumentoId();}
                                if(docsDgiraFlujoHistEdit.getDocumentoDgiraObservHist() != null)
                                { comentarioTurnado = docsDgiraFlujoHistEdit.getDocumentoDgiraObservHist();}                            
                            }
                            areaResiveDocCon = daoBitacora.jerarquiaTbArea(docsDgiraFlujoHistEdit.getIdAreaEnvioHist(), idEntidad);
                            //JOptionPane.showMessageDialog(null, "idDocumento:  " +areaResiveDocCon.size(), "Error", JOptionPane.INFORMATION_MESSAGE);
                        } catch (Exception er2) {
                            //JOptionPane.showMessageDialog(null, "err:  " + er2.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }
                        for (Object o : areaResiveDocCon) {
                            jerarquia = o.toString();
                        }
                        
                     // pool delegaciones
                        if(docsDgiraFlujo == null){
                        	// en este caso obtenemos el documento dgira porque no se ha consultado
                        	docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
                        }

                        try{
                        	perfilUsr = docsDgiraFlujo.getRolId().getNombreCorto().trim();
                        }catch(NullPointerException exnp){
                        	perfilUsr = jerarquia.toLowerCase();
                        }
                        
                        // el statusDoc y estatus del documento_dgira_flujo debe ser igual ya que al turnar setea el estatus en ambos registros ( padre y detalle )
                    	perfilSig = AppSession.siguienteRol(perfilUsr, docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                    	//perfilSig = AppSession.siguienteRol(perfilUsr, statusDoc);
                        System.out.println("perfilSig :" + perfilSig);
                        

                        GO = 0; // para que habilite el boton "Generar oficio...."
                        TO = 0; // para que habilite en este caso el boton de "Turnar oficio"
                        
                        //si el perfil en session es igual al perfil al que se le envio el oficio, perimite la edicion del oficio
                        if (sesion.getAttribute("rol").toString().equals(perfilSig)) {
                        	
                        	
                        	if(sesion.getAttribute("rol").toString().equals("eva")) //solo el evaluador puede genera y modificar oficio de info adicional
                            {
                                URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ","+tipoDocId; //solo visualiza PDF y no lo modifica
                            }
                            else
                            {
                                URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                            }
                        	
                        	if (statusDoc == GenericConstants.ESTATUS_DOCTO_FIRMADO) //si viene firmado
                            {
                                turnaDocsBtn = true; //ocultar bootn de turnado
                                GO = 1; // para que inhabilite el boton "Generar oficio...."
                                TO = 1; // para que inhabilite en este caso el boton de "Turnar oficio"
                                
                                // si es la segunda bandeja de un ofirio ya firmado, se calcula su url download....
                                if(bandeja == 2){
                                
                                	GO = 0;         
                                	URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                                	 
                                    if(docsDgiraFlujo == null){
                                    	// en este caso obtenemos el documento dgira porque no se ha consultado
                                    	docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
                                    }                                        
                                    List<DocumentoRespuestaDgira> lista;
                                    lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
                                    for (DocumentoRespuestaDgira x : lista) {
                                        try{
            	                            if(docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO){
            	                            	URLgenOficPre = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoDoc+"&entidadfederativa="+edoGestTram+"&documentoid="+tipoDocId;
            	                            }
                                        }catch(Exception exx){                                        	
                                        }
                                    }
                                }
                                                                
                            }else{                        	
                            	turnaDocsBtn = false;//visualizar boton de turnado
                            	
                            	// cuando sea la url del director no debe habilitar ningun boton cuando la bitacora no este firmada para la segunda bandeja
                            	if(isUrlDG == 1 && bandeja == 2){
                            		GO = 1;
                            		TO = 1;
                            	}
                            }
                            
                        } else {
                            turnaDocsBtn = true; //ocultar bootn de turnado
                            URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                        }
                    } else {

                        turnaDocsBtn = false;//visualizar boton de turnado
                        if(sesion.getAttribute("rol").toString().equals("eva")) //solo el evaluador puede genera y modificar oficio de info adicional
                        {
                        URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ","+tipoDocId; //solo visualiza PDF y no lo modifica  bitaProyOfic
                        }
                        else
                        {
                            URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                        }
                            
                    }
                    
                    System.out.println("statusDoc : " + statusDoc);
                    System.out.println("turnaDocsBtn : " + turnaDocsBtn);
                    System.out.println("URLgenOficPre : " + URLgenOficPre);
                    System.out.println("Generar Oficio : " + GO);
                    System.out.println("Turnar Oficio : " + TO);
                    System.out.println("Terminó constructor de info adicional");
                    }
                }
                catch(Exception ar)
                {
                	System.out.println("Problema: " + ar.getMessage());
                	ar.printStackTrace();
                }
            }
        logger.debug(Utils.obtenerLogConstructor("Fin"));
        
    }
    
    public SegundasViasView(String val) {
    	
    	daoDg = new DgiraMiaeDaoGeneral();
    	dao = new BitacoraDao();
    	
    }
    
    public SelectItem[] getCatTipoDocSelectOne() {
        
        NoHistorialAT0011 = daoBitacora.numRegHistStatus(bitaProyecto, "AT0011");
        List<Object[]> lista;
        if(NoHistorialAT0011.isEmpty())//si no viene de tramite No integrado, se muestran toda las opciones
        {
            lista = daoDg.getCatTipoDoc(idTipTram);
        }
        else//si si viene de No inegracion de espediente, solo mostrara la opcion de Resolutivo Negado
        {
            lista = daoDg.getCatTipoDocNeg(idTipTram);
        }
        
        int size = lista.size() + 1;
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        items[0] = new SelectItem("0", "-- Seleccione --");
        i++;
        for (Object[] x : lista) {
            
            items[i++] = new SelectItem(Short.parseShort(x[0].toString())  , x[1].toString());
            
            System.out.println("Problema: " + x);
            
        }
        return items;

    }
    
    
    
    
    @SuppressWarnings("unused")
	public void suspPorInfoAdic(short tipoOfic)
    {

        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        idAux = 0;
        maxIdHisto = 0;
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial remitente;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        Historial situacATOO22 = null;
        Historial situacI00010 = null;
        Integer resultado = 0;
        Integer resultVersion=-1;
        tipoDoc = tipoOfic;
        Boolean exitoCambSituac = false;
        //tipoDoc = 4 Oficio de prevencion
        //tipoDoc = 7 Oficio de Información adicional
        Integer soloTurnOfic=-1;
        
                
        FacesContext ctxt = FacesContext.getCurrentInstance();
        
            if(bitaProyecto != null)
            {
                
                    //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

                    maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    for (Object o : maxDocsDgiraFlujoHist) {
                        idAux =  Integer.parseInt(o.toString());
                        maxIdHisto = idAux.shortValue();
                    }

                    docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                    
                    if(docsDgiraFlujoHistEdit != null && maxIdHisto > 0)
                    {            
                        docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                        docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                        docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                        daoDg.modifica(docsDgiraFlujoHistEdit);
                    }


                    //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
                    docsDgiraFlujo = daoDg.docsFlujoDgira(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);

                    if (docsDgiraFlujo != null) {
                        if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                            banderaNew = 0;
                        }// registro nuevo
                        else {
                            banderaNew = 1;
                        }

                    } else {
                        banderaNew = 1;
                    }

                    if (banderaNew == 1) {
                        docsDgiraFlujo = new DocumentoDgiraFlujo(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    }

                    catEstatusDoc = daoDg.idCatEstatusDoc(idDocumento);
                    docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
                    docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
                    docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
                    docsDgiraFlujo.setIdAreaRecibe(null);
                    docsDgiraFlujo.setIdUsuarioRecibe(null);
                    docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
                    docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
                    docsDgiraFlujo.setDocumentoDgiraObservacion(comentarioTurnado);
                    
                 // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
                    UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
//            		System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
//            		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
//            		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
//            		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
//            		System.out.println("rolId     :" + ur.getRolId().getIdRol());
            		
            		// Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
                    CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());
                    
                    // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
                   	docsDgiraFlujo.setRolId(rol);

                    try {
                        if (banderaNew == 1) {
                            daoDg.agrega(docsDgiraFlujo);
                        }
                        if (banderaNew == 0) {
                            daoDg.modifica(docsDgiraFlujo);
                        }

                        maxIdHisto = (short) (idAux + 1);
                        docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                        docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                        docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                        docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                        docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                        docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                        docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                        docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                        docsDgiraFlujoHist.setDocumentoDgiraObservHist(comentarioTurnado);
                        try {
                            daoDg.agrega(docsDgiraFlujoHist);
                        } catch (Exception ex) {
                            error = error + 1;
                        }
                    } catch (Exception e) {
                        error = error + 1;
                    }

                    if (error == 0) 
                    {
                        System.out.println("Terminó turnado de oficio" );

                        if(idDocumento == 4) //si el estatus del documento es 4=frimado
                        {
                            if(tipoDoc == 7)//si es oficio de Información adicional
                            {
                                tipoSuspencion = 1;
                                tipoApertura=2;
                                System.out.println("Comienza turnado de Oficio de Info. Adic. idDocumento: " + idDocumento);
                                
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situación");
                                    //Situaciones para colocar tramite en situación de suspencion por Información adicional                                    
                                    if(cambioSituacion("AT0031","I00010", getBitaProyecto()) == 1)//eva-sd  
                                    {
                                        resultado = resultado + 1;
                                        if(cambioSituacion("AT0032","AT0027", getBitaProyecto()) == 1)//sd-da
                                        {
//                                            resultado = resultado + 1;
//                                            if(cambioSituacion("AT0004","dadg") == 1) //da-dg 
//                                            {
                                                resultado = resultado + 1;
                                                if(cambioSituacion("AT0008","dg", getBitaProyecto()) == 1)//dg-dg 
                                                {  
                                                    resultado = resultado + 1;
                                                    if(cambioSituacion("ATIF01","dgecc", getBitaProyecto()) == 1)//dg-ecc 
                                                    {
                                                        resultado = resultado + 1;                                                                
                                                    }                                                    
                                                }
//                                            }
                                        }
                                    }
                                    if(resultado == 4)
                                    { 
                                        try
                                        {
                                            if(folio!=null)
                                            {
                                                
                                                //CREACION DE VERSION 3 DE TRAMITE, solo aplica para oficio de Información adicional
                                                //si el resultVersion es 0= bien 1=mal
                                                resultVersion = procVersiones(folio, (short)3);

                                                ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                                if(resultVersion == 0)
                                                { 
                                                    exitoCambSituac = true; 
                                                    System.out.println("Termino ingreso de version 3 de la MIA");
                                                }
                                                else {
                                                    exitoCambSituac = false; 
                                                    System.out.println("No Termino ingreso de version 3 de la MIA");
                                                }
                                                
                                            }
                                            else
                                            {
                                                exitoCambSituac = true; 
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            System.out.println("Problema con inserción de situaciones: " + ex.getMessage());
                                            ctxt.addMessage("growl", new FacesMessage("Trámite no turnado, intentelo mas tarde."));
                                            exitoCambSituac = false; 
                                        }
                                        
                                        
                                    }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5)//si es oficio de prevencion (3=pago de derechos  4=requisitos cofemer)
                            {
                                tipoSuspencion = 2;
                                tipoApertura=1;
                                System.out.println("Comienza turnado de Oficio de prevención idDocumento: " + idDocumento);
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situación");
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("W")) // tramites electronicos
                                    {
                                        //es -- por que se turna asi mismo
                                        if(cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)//if(cambioSituacion("AT0022","I00008") == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("-")) //tramites fisicos
                                    {
                                        //es -- por que se turna del SD al ECC
                                        if(cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    
                                    
                                    if(resultado == 1)
                                    { exitoCambSituac = true; }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 1 || tipoDoc == 2 ||tipoDoc == 8 || tipoDoc == 9 || tipoDoc == 10 || tipoDoc == 11)// estos oficios no van a segundas vias ni afectan el flujo del PEIA
                            {
                                exitoCambSituac = true;
                            }
                            
                            soloTurnOfic = 0;
                        }
                        else // para cuando no se trata del firmado del documento
                        {
                            soloTurnOfic = 1;
                        }
                        
                        
                        if(exitoCambSituac)
                        {
                            if(folio!=null)
                            {
                                if(tipoDoc == 7 || tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5) //segundas vias solo aplica a oficio de info. adicional y de prevencion
                                {
                                    //comienza tramite de segundas vias  
                                    try { //1=iNFORMACION ADICIONAL 2=PREEVENCION
                                        segundasVias.proc(bitaProyecto, tipoSuspencion,folio,tipoApertura,tipoDoc);
                                        ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                        System.out.println("Termino de segundas vías");                                

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, inténtelo mas tarde."));
                                        System.out.println("Problema con segundas vías: " + e.getMessage());
                                    }
                                }
                                else
                                {ctxt.addMessage("growl", new FacesMessage("oficio firmado exitosamente"));}
                            }
                            else
                            {
                                ctxt.addMessage("growl", new FacesMessage("Trámite turnado exitosamente."));
                            }
                        }
                        else
                        {
                            if(soloTurnOfic==0) //si se trata de turnado de tramite en esatdo de firma de DG
                            {
                                System.out.println("Problema con cambio de situación de trámite, no se corrio procedimeinto de segundas vías");
                                ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, intentelo mas tarde.Trámite no notificado a promovente."));
                            }
                            else//si se trata de turnado de tramite en esatdo de NO firma de DG
                            {
                                System.out.println("Se completo turnado de oficio (No es situación de firmado de DG)");
                                ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));
                            }
                        }
                        //FacesContext.getCurrentInstance().addMessage("messages", message);
                        turnaDocsBtn = true; 
                    } else {
                        ctxt.addMessage("growl", new FacesMessage("El turnado del oficio falló, intente nuevamente."));
                    }
            }
        
    }
    
    /***
     * Metodo que realiza un guardado del documento, cambiandole la situacion correspondiente a la firma o corrección
     * @param situacion
     * @param situacAnterior
     * @return
     */
	@SuppressWarnings("unused")
	public Integer cambioSituacion(String situacion, String situacAnterior, String bitacora) 
    {
        //situacion: Situacion a la que se desea cambiar el tramite
        //situacAnterior: Situacion en donde el tramite fue turnado, de aqui se obtiene el idAreaEnvia y idUsuarioEnvio(a quien se le habilitara el tramite cunado SINATEC libere el tramite de prenencion o algun paro de reloj )
        Historial remitente; ///idArea idusuario
        Historial remitente2;
        Integer bandera=0;
        String idAreaRecibe="";
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitacora);
        DocumentoDgiraFlujoHist docsDgiraFlujoHist2;
        
        if (bitacora != null && bitacora2!= null) 
        {
            try 
            {       
                if(!situacAnterior.equals("--"))
                { //02000000
                    if(situacAnterior.equals("dg") || situacAnterior.equals("dadg") || situacAnterior.equals("dgecc") || situacAnterior.equals("sdecc"))
                    {
                        if(situacAnterior.equals("dg"))
                        {
                            if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                            {
                                idArea= idArea.trim();
                                //idusuario=idusuario;
                                idAreaRecibe =  idArea.trim();
                            }
                            else{
                                idArea= "02000000";
                                idusuario=3943;
                                idAreaRecibe =  "02000000";
                            }
                        }
                        if(situacAnterior.equals("dadg"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"AT0027");//
                            if(remitente != null)
                            {
                                idArea=remitente.getHistoAreaRecibe();
                                idusuario=remitente.getHistoIdUsuarioRecibe();   
                                                                
                                if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                { idAreaRecibe =  idArea.trim(); }
                                else
                                { idAreaRecibe = "02000000";  }
                            }
                        }
                        if(situacAnterior.equals("dgecc"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"CIS104");
                            if(remitente != null)
                            {
                                if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                {
                                    idArea= idArea.trim();
                                    //idusuario=idusuario;
                                }
                                else
                                {
                                    idArea= "02000000";
                                    idusuario=3943;
                                }
                                idAreaRecibe = remitente.getHistoAreaEnvio(); 
                            }
                        }
                        if(situacAnterior.equals("sdecc")) //SD a ECC
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"CIS104");
                            //traer el Id de usuario del subdirector
                            docsDgiraFlujoHist2 = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, (short)1 );
         
                            if(remitente != null)
                            {
                                idArea= docsDgiraFlujoHist2.getIdAreaEnvioHist() ;
                                idusuario = docsDgiraFlujoHist2.getIdUsuarioEnvioHist();                            
                                idAreaRecibe = remitente.getHistoAreaEnvio(); 
                            }
                        }
                            
                    }
                    else
                    {
                        remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,situacAnterior);
                        if(remitente != null)
                        {
                            idArea=remitente.getHistoAreaRecibe();
                            if(remitente.getHistoIdUsuarioRecibe() != null)
                            idusuario=remitente.getHistoIdUsuarioRecibe(); 
                            idAreaRecibe = remitente.getHistoAreaEnvio(); 
                        }
                    }
                }
                else
                {idAreaRecibe = idArea;}//para situaciones AT0002, AT0003 y AT0011
                //FacesMessage message=null;                   
                sinatDgira = new SinatDgira();
                sinatDgira.setId(0);
                sinatDgira.setSituacion(situacion);
                sinatDgira.setIdEntidadFederativa(bitacora2.getBitaEntidadGestion());
                sinatDgira.setIdClaveTramite(bitacora2.getBitaTipotram()); //STRING BITA_TIPOTRAM
                sinatDgira.setIdTramite(bitacora2.getBitaIdTramite());
                sinatDgira.setIdAreaEnvio(idArea.trim());
                sinatDgira.setIdAreaRecibe(idAreaRecibe.trim());
                sinatDgira.setIdUsuarioEnvio(idusuario);
                //sinatDgira.setIdUsuarioRecibe(1340);
                sinatDgira.setGrupoTrabajo(new Short("0"));
                sinatDgira.setBitacora(bitacora);
                sinatDgira.setFecha(new Date());

                dao.agrega(sinatDgira);
                
                
                //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite turnado correctamente.", null);                    
                System.out.println("Terminó turnado exitosamente, situación: " + situacion);               
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                bandera = 1;                
            } catch (Exception er) {
                bandera = 0;
                er.printStackTrace();
                //FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido turnar el tramite, intente mas tarde.", null);
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                System.out.println("Error en turnado a situación " + situacion + " de trámite :  " + er.getMessage() );
            }
        }
        else
        {
            bandera = 0;
        }
        return bandera;
    }
    
    public Integer procVersiones(String folio, short version) throws Exception {
        Connection con = getConexion();
        Integer respuesta = -1;
        try
        {
            CallableStatement csLote = con.prepareCall("{call DGIRA_MIAE2.VERSION_PROYTEMATICA(?,?,?,?)}");
            csLote.setString(1, folio);
            csLote.setShort(2, version);
            csLote.registerOutParameter(3, Types.INTEGER);
            csLote.registerOutParameter(4, Types.VARCHAR);
            csLote.execute();
            respuesta = csLote.getInt(3);
            if (csLote.getInt(3) != 0) {
                throw new Exception("Error al ejecutar DGIRA_MIAE2.VERSION_PROYTEMATICA: " + csLote.getString(5));
            } else {
                System.out.println("Exito en procedimeinto de versiones ");
            }
        } catch (Exception ex) {
            System.out.println("Problema con creación de versión 3. " + ex.getMessage());
        }
         
        con.close();
        return respuesta;
    }
    
    private Connection getConexion() throws ClassNotFoundException, SQLException {
        Class.forName(driverDB);
        connection = DriverManager.getConnection(urlDB, userDB, passDB);
        return connection;
    }

    @SuppressWarnings("unused")
	private void closeConnection() throws SQLException {
        connection.close();
    }
    public void guardaResolutivo()
    {   
        System.out.println("\ninicia guardaResolutivo");
//        System.out.println("bitaProyecto : "+ bitaProyecto);
//        System.out.println("cveTipTram : "+ cveTipTram);
//        System.out.println("catTipoDoc : "+ catTipoDoc);
        
        
        if(catTipoDoc != 0)
        {
            EvaluacionProyecto ev = daoBitacora.datosEvaProy(bitaProyecto);
            ev.setEvaClaveTramite2(cveTipTram);
            ev.setEvaIdTramite2(idTipTram);
            ev.setEvaTipoDocId(catTipoDoc);
            try
            {daoDg.modifica(ev);}
            catch(Exception ex)
            {System.out.println("Problema guardaResolutivo: " + ex);}
        
        }
    }

   

	/**
     * @return the nombreOfic
     */
    public String getNombreOfic() {
        return nombreOfic;
    }

    /**
     * @param nombreOfic the nombreOfic to set
     */
    public void setNombreOfic(String nombreOfic) {
        this.nombreOfic = nombreOfic;
    }

    /**
     * @return the idDocumento
     */
    public short getIdDocumento() {
        return idDocumento;
    }

    /**
     * @param idDocumento the idDocumento to set
     */
    public void setIdDocumento(short idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     * @return the subSectorCat
     */
    public List<CatEstatusDocumento> getSubSectorCat() {
        return subSectorCat;
    }

    /**
     * @param subSectorCat the subSectorCat to set
     */
    public void setSubSectorCat(List<CatEstatusDocumento> subSectorCat) {
        this.subSectorCat = subSectorCat;
    }

    /**
     * @return the turnaDocsBtn
     */
    public Boolean getTurnaDocsBtn() {
        return turnaDocsBtn;
    }

    /**
     * @param turnaDocsBtn the turnaDocsBtn to set
     */
    public void setTurnaDocsBtn(Boolean turnaDocsBtn) {
        this.turnaDocsBtn = turnaDocsBtn;
    }

    /**
     * @return the cveTipTram
     */
    public String getCveTipTram() {
        return cveTipTram;
    }

    /**
     * @param cveTipTram the cveTipTram to set
     */
    public void setCveTipTram(String cveTipTram) {
        this.cveTipTram = cveTipTram;
    }

    /**
     * @return the idTipTram
     */
    public Integer getIdTipTram() {
        return idTipTram;
    }

    /**
     * @param idTipTram the idTipTram to set
     */
    public void setIdTipTram(Integer idTipTram) {
        this.idTipTram = idTipTram;
    }

    /**
     * @return the edoGestTram
     */
    public String getEdoGestTram() {
        return edoGestTram;
    }

    /**
     * @param edoGestTram the edoGestTram to set
     */
    public void setEdoGestTram(String edoGestTram) {
        this.edoGestTram = edoGestTram;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }
    
    public String getBitaProyOfic() {
        this.bitaProyOfic = getBitaProyecto().substring(0, 2) + "%2F" + getBitaProyecto().substring(3, 10) + "%2F" + getBitaProyecto().substring(11, 13) + "%2F" + getBitaProyecto().substring(14, 16);
        return bitaProyOfic;
    }

    public void setBitaProyOfic(String bitaProyOfic) {
        this.bitaProyOfic = bitaProyOfic;
    }
    
    @SuppressWarnings("rawtypes")
	public void selTurnadoDocsCheck() {
        turnaDocs = false; ////cveTipTram idTipTram edoGestTram, idTipoDoc, idDoc
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        Map params = ec.getRequestParameterMap();
        System.out.println("tipoOficInfoAdic: " + params.get("tipoOfic"));
        if(params.get("tipoOfic")!=null && tipoDoc == 0) 
        {tipoDoc = Short.parseShort(params.get("tipoOfic").toString());}
        System.out.println("XXXtipo de Oficio: " + tipoDoc);
        //si el tramite ya ha generado y guradado un oficio de preevencion por validacion de checkList, este ya puede ser turnado
        documentoRespuestaDgira = daoDg.docsRespDgira(getBitaProyecto(), cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
        if (documentoRespuestaDgira.size() > 0) {
            List<DocumentoRespuestaDgira> lista;
            lista = daoDg.docsRespDgiraBusq2(getBitaProyecto(), cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
            for (DocumentoRespuestaDgira x : lista) {
                nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
            }
            turnaDocs = true;
        } else {
            turnaDocs = false;

        }
        System.out.println("turnaDocs :" + turnaDocs);
        //JOptionPane.showMessageDialog(null, "Debe:  " + turnaDocs, "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * @return the turnaDocs
     */
    public Boolean getTurnaDocs() {
        return turnaDocs;
    }

    /**
     * @param turnaDocs the turnaDocs to set
     */
    public void setTurnaDocs(Boolean turnaDocs) {
        this.turnaDocs = turnaDocs;
    }

    /**
     * @return the urlOfic
     */
    public String getUrlOfic() {
        return urlOfic;
    }

    /**
     * @param urlOfic the urlOfic to set
     */
    public void setUrlOfic(String urlOfic) {
        this.urlOfic = urlOfic;
    }

    /**
     * @return the URLgenOficPre
     */
    public String getURLgenOficPre() {
        return URLgenOficPre;
    }

    /**
     * @param URLgenOficPre the URLgenOficPre to set
     */
    public void setURLgenOficPre(String URLgenOficPre) {
        this.URLgenOficPre = URLgenOficPre;
    }

    /**
     * @return the muestraSec
     */
    public Boolean getMuestraSec() {
        return muestraSec;
    }

    /**
     * @param muestraSec the muestraSec to set
     */
    public void setMuestraSec(Boolean muestraSec) {
        this.muestraSec = muestraSec;
    }

    /**
     * @return the bitaProyecto
     */
    public String getBitaProyecto() {
        return bitaProyecto;
    }

    /**
     * @param bitaProyecto the bitaProyecto to set
     */
    public void setBitaProyecto(String bitaProyecto) {
        this.bitaProyecto = bitaProyecto;
    }

    /**
     * @return the catTipoDoc
     */
    public short getCatTipoDoc() {
        return catTipoDoc;
    }

    /**
     * @param catTipoDoc the catTipoDoc to set
     */
    public void setCatTipoDoc(short catTipoDoc) {
        this.catTipoDoc = catTipoDoc;
    }
    
    

    public int getRolId() {
		return rolId;
	}

	public void setRolId(int rolId) {
		this.rolId = rolId;
	}

	/**
     * @return the tipoDocCat
     */
    public List<CatTipoDocDgira> getTipoDocCat() {
        return tipoDocCat;
    }

    /**
     * @param tipoDocCat the tipoDocCat to set
     */
    public void setTipoDocCat(List<CatTipoDocDgira> tipoDocCat) {
        this.tipoDocCat = tipoDocCat;
    }

    /**
     * @return the catTipoDoc2
     */
    public CatTipoDocDgira getCatTipoDoc2() {
        return catTipoDoc2;
    }

    /**
     * @param catTipoDoc2 the catTipoDoc2 to set
     */
    public void setCatTipoDoc2(CatTipoDocDgira catTipoDoc2) {
        this.catTipoDoc2 = catTipoDoc2;
    }

    /**
     * @return the catTipDoc
     */
    public SelectItem[] getCatTipDoc() {
        return catTipDoc;
    }

    /**
     * @param catTipDoc the catTipDoc to set
     */
    public void setCatTipDoc(SelectItem[] catTipDoc) {
        this.catTipDoc = catTipDoc;
    }

    /**
     * @return the muestTurnOrte
     */
    public Boolean getMuestTurnOrte() {
        return muestTurnOrte;
    }

    /**
     * @param muestTurnOrte the muestTurnOrte to set
     */
    public void setMuestTurnOrte(Boolean muestTurnOrte) {
        this.muestTurnOrte = muestTurnOrte;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the deInfoAdic
     */
    public Boolean getDeInfoAdic() {
        return deInfoAdic;
    }

    /**
     * @param deInfoAdic the deInfoAdic to set
     */
    public void setDeInfoAdic(Boolean deInfoAdic) {
        this.deInfoAdic = deInfoAdic;
    }

    /**
     * @return the comentarioTurnado
     */
    public String getComentarioTurnado() {
        return comentarioTurnado;
    }

    /**
     * @param comentarioTurnado the comentarioTurnado to set
     */
    public void setComentarioTurnado(String comentarioTurnado) {
        this.comentarioTurnado = comentarioTurnado;
    }

    /**
     * @return the nombTipoOfic
     */
    public String getNombTipoOfic() {
        return nombTipoOfic;
    }

    /**
     * @param nombTipoOfic the nombTipoOfic to set
     */
    public void setNombTipoOfic(String nombTipoOfic) {
        this.nombTipoOfic = nombTipoOfic;
    }

    /**
     * @return the comenatriosTab
     */
    public List<Object[]> getComenatriosTab() {
        return comenatriosTab;
    }

    /**
     * @param comenatriosTab the comenatriosTab to set
     */
    public void setComenatriosTab(List<Object[]> comenatriosTab) {
        this.comenatriosTab = comenatriosTab;
    }

    /**
     * @return the tramYaTurnadoDG
     */
    public Boolean getTramYaTurnadoDG() {
        return tramYaTurnadoDG;
    }

    /**
     * @param tramYaTurnadoDG the tramYaTurnadoDG to set
     */
    public void setTramYaTurnadoDG(Boolean tramYaTurnadoDG) {
        this.tramYaTurnadoDG = tramYaTurnadoDG;
    }

   

    public StreamedContent getFile() {
    	System.out.println("getFile...");
        return file;
    }
    
    
    /***
     * Descarga el jar para firma para información adicional y registra la cadena firma si no existe o en su caso muestra el token al usuario
     */
    public void siFirmarInfoAdicional(){
    	
    	System.out.println("\n\nsi firmar info adicional");
    	InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/resources/SignerClientAll.jar");
		file = new DefaultStreamedContent(stream, "application/java-archive", "SignerClientAll.jar");
		
		FacesContext fContext = FacesContext.getCurrentInstance();
		String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = bitaProyecto; // fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
		
		if(claveProyecto == null)
			claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();
		
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		//String documentoId = "1";  // para los oficios preventivos
		String documentoId = tipoDocId;
		
		if(titulo.equals("")){
			getTituloOficio(tipoDoc);
		}
		
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + documentoId);		
		System.out.println("edoGestTramite : " + edoGestTram);		
		System.out.println("rol usuario : " + fContext.getExternalContext().getSessionMap().get("rol")); //sesion.getAttribute("rol").toString());
		System.out.println("idAreaUsu : " + fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
		System.out.println("idUsu : " + fContext.getExternalContext().getSessionMap().get("idUsu"));
		System.out.println("Usuario recibe: " + usuariorecibetxt);
		System.out.println("titulo oficio : " + titulo);
		System.out.println("-------------------------------------------------------------------------");
		
		
		// busca si ya existe el registro de cadena firma...
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);		
		
		String msg_numero_token = Utils.obtenerConstante(GenericConstants.FIRMAR_NUMERO_TOKEN);
		String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.FIRMAR_NO_REGISTRO_CADENAFIRMA);
		
		if(firma != null){
		
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));			
		}else{
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
	        Date thisDate = new Date();
	        String fecha = dateFormat.format(thisDate);
	        	        
	        String descripcion_documento = titulo;	        
	        // String destinatario = usuariorecibetxt;
	        // String iniciales = "LDSM/ASR/AVA/GBA/JCTT";
	        // String infoaFirmar = "||"+fecha+"|"+bitacora+"|"+descripcion_documento+"|"+destinatario+"|"+iniciales+"||";
	        // String infoaFirmar = "||"+fecha+"|"+bitacora+"|"+descripcion_documento+"|"+destinatario+"||";
	        String infoaFirmar = "||"+bitacora+"|"+descripcion_documento+"||";
	        String infoaFirmarB64 = Base64.encodeBase64String(infoaFirmar.getBytes());
	        
	        System.out.println("fecha :" + fecha);
	        System.out.println("descripcion :" + descripcion_documento);
	        //System.out.println("destinatario :" + destinatario);
	        //System.out.println("iniciales :" + iniciales);
	        System.out.println("encodeB64 :" + infoaFirmarB64);
	        
			System.out.println("\n\nagregaCadena..");
			CadenaFirmaDao cadenaDao = new CadenaFirmaDao();
			BcAgregaCadenaRespuesta agregacadena = null;
			
			try {
				agregacadena = cadenaDao.agregaCadena(folio, infoaFirmarB64);
				
				if(agregacadena != null){
					 
					 firma = new CadenaFirma();

					 // firma.setClave(clave);
					 firma.setClaveProyecto(claveProyecto);
					 firma.setBitacora(bitacora);
					 firma.setClaveTramite2(cveTipTram);
					 firma.setIdTramite2(idTipTram);
					 firma.setTipoDocId(tipoDoc);
					 firma.setDocumentoId(documentoId);
					 
					 firma.setFolioProyecto(folio);
					 firma.setB64(infoaFirmarB64);
					 firma.setIdentificador(agregacadena.getIdentificador());
					 firma.setOperacion(String.valueOf(agregacadena.getTransferenciaOperacion()));
				 
					 new CadenaFirmaDao().agrega(firma);
					 
					 // despues de agregar la cadena de firma, se consulta para mostrar el identificador al usuario..
					 firma = null;
					 firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);
					 
					 if(firma != null){
					   FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
					 }else{
						 FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
					 }
					 					 
				}else{
						 
					FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
				}	
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
			}
		}
    	
    }
    
    /***
     * Metodo que realiza la corrección del oficio de información adicional
     */
    public void corrigeOficioInfoAdicional(){
    	
    	System.out.println("\n\ncorrigeOficioInfoAdicional..");
    	System.out.println("comentarios : " + comentarioTurnado);
    	idDocumento = 2; // a corrección
    	
    	try {
    		cambioSituacion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    /***
     * Método que realiza el turnado del oficio recientemente firmado de información adicional
     * @throws Exception
     */
    public void turnaOficioInfoAdicional(){  // informacion adicional ( turnado por el eva, sd, da )
    	
    	System.out.println("\n\nturnaOficioInfoAdicional..");
    	 
    	FacesContext fContext = FacesContext.getCurrentInstance();
		String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = bitaProyecto; // fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
		
		if(claveProyecto == null)
			claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();
		
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();
		//String documentoId = "1";  // para los oficios preventivos, informacion adicional		
		comentarioTurnado = "";
		
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		// System.out.println(fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
		// System.out.println(fContext.getExternalContext().getSessionMap().get("idUsu"));
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + tipoDocId);		
		System.out.println("edoGestTramite : " + edoGestTram);
		System.out.println("-------------------------------------------------------------------------");
    	
    	
    	if(isUrlDG == 1)
			idDocumento = GenericConstants.ESTATUS_DOCTO_FIRMADO; // 4 firmado
		else
			idDocumento = GenericConstants.ESTATUS_DOCTO_AUTORIZADO; // 3 autorizado ( turnar )
        
    	
    	// url que apunta al oficio dentro del folio( carpeta ). para este caso sera para ponerle esta url al codigo qr
		String ligaQR = Utils.ruta_liga_codigoqr + bitaProyecto;
		
		List<DocumentoRespuestaDgira> lista = daoDg.docsRespDgiraBusq2(bitacora, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
        for (DocumentoRespuestaDgira x : lista) {
            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
            urlOfic = x.getDocumentoUrl();
        }
		
		System.out.println("idDocumento : " + idDocumento);
		System.out.println("urlOfic : " + urlOfic);
		System.out.println("nombreOfic : " + nombreOfic);
		System.out.println("ligaQR : " + ligaQR);
		System.out.println("-------------------------------------------------------------------------");
    			
    	// busca si ya existe el registro de cadena firma...		
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma cadenafirma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);
		
		String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
		String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
		String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
		String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);
		
		if(cadenafirma == null){
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_hay_token_registrado));
			return;
		}    	
    	
		// obtenemos la firmalog del usuario logueado ( por su rol y nivel lo sabemos y no necesariamente existe este registro en este momento )
		CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenafirma, rol);
			
		// detallefirmado es objeto que tiene un estatus y descripcion y una lista de firmas del ws
		DetallesFirmado detallefirmado = null;
		try{
			detallefirmado = firmaDao.getFirmas(cadenafirma);
			// el ws no responde
			if(detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() != 0)){
				System.out.println("\nno responde el servicio web o no hay firmas...");
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));		
				return;
			// como el ws si esta respondiendo, analizar las firmas a ver si alguna de ellas corresponde con la de firmaLog actual...
			}else if (detallefirmado != null && detallefirmado.getEstado() == 0 && ( detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)){
				
				// cadenafirmaLog puede ser null
				// detallefirmado si trae firmas del ws
				Firmas firmaws = firmaDao.GetFirmaWS(cadenafirma, detallefirmado);
							
				// para este caso si no hay firma del ws decimos que no ha firmado el usuario...
				if(firmaws == null){
					System.out.println("ultima firma válida : null");
					System.out.println("\nel usuario no ha firmado...");
					FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
					return;
				}else{
					System.out.println("ultima firma válida : " + firmaws.getRfc() + " - " + firmaws.getFecha());
					System.out.println("el usuario ya firmó y se guardará en cadenafirmalog...");
					// SE ASUME QUE SI FIRMÓ YA por tanto primero agregamos el registro de cadenafirmalog correspondiente...
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
					Date fcha = formatter.parse(firmaws.getFecha());
					boolean resp = false;
					
					if(cadenafirmaLog == null){
						
						CadenaFirmaLog firmaLog = new CadenaFirmaLog();
						firmaLog.setCadenaFirma(cadenafirma);
						firmaLog.setEstatus((short)1);
						firmaLog.setRol(rol);
						firmaLog.setFecha(fcha);
						firmaLog.setFirma(firmaws.getFirma());
						firmaLog.setRfc(firmaws.getRfc());
					
						resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
						System.out.println("firmalog agregada: " + resp);
						
					}else{
						// se actualiza cadenafirmalog del rol en sesion
						cadenafirmaLog.setRfc(firmaws.getRfc());
						cadenafirmaLog.setFecha(fcha);
						cadenafirmaLog.setFirma(firmaws.getFirma());
						resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
						System.out.println("firmalog modificada: " + resp);
						
					}
					
					if(resp == true){
						
						// y mandamos turnar...
						if(idDocumento == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
							
							cambioSituacion();
							
						}else{
							DocumentoRespuestaDgira documento = new DocumentoRespuestaDgira();
                                                        documento.setDocumentoUrl(urlOfic);
                                                        documento.setDocumentoNombre(nombreOfic);
							firmaDao.editarPdf(documento, cadenafirma, ligaQR);
							firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(),  cadenafirma.getFolioProyecto());
							cambioSituacionInfoAdicional();
						}
						
					}else{
						throw new Exception("Error: al guardar el log de la firma para el rol actual");
					}
				}
				
			}else{

				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_se_realizo_turnado));
			}   
		}catch(Exception ex){
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
		}
    	
    }
    
    
    /***
     * Descarga el jar para firma y registra la cadena firma si no existe o en su caso muestra el token al usuario
     */
    public void siFirmar() // para la firma del preventivo
    {
    	System.out.println("\n\nsi firmar");
    	InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/resources/SignerClientAll.jar");
		file = new DefaultStreamedContent(stream, "application/java-archive", "SignerClientAll.jar");
		
		FacesContext fContext = FacesContext.getCurrentInstance();
		String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = bitaProyecto; // fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
		
		if(claveProyecto == null)
			claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
		
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		//String documentoId = "1";  // para los oficios preventivos
		String documentoId = tipoDocId;
		
		if(titulo.equals("")){
			getTituloOficio(tipoDoc);
		}
		
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + documentoId);		
		System.out.println("edoGestTramite : " + edoGestTram);		
		System.out.println("rol usuario : " + fContext.getExternalContext().getSessionMap().get("rol")); //sesion.getAttribute("rol").toString());
		System.out.println("idAreaUsu : " + fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
		System.out.println("idUsu : " + fContext.getExternalContext().getSessionMap().get("idUsu"));
		System.out.println("Usuario recibe: " + usuariorecibetxt);
		System.out.println("titulo oficio : " + titulo);
		System.out.println("-------------------------------------------------------------------------");
		
		
		// busca si ya existe el registro de cadena firma...
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);		
		
		String msg_numero_token = Utils.obtenerConstante(GenericConstants.FIRMAR_NUMERO_TOKEN);
		String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.FIRMAR_NO_REGISTRO_CADENAFIRMA);
		
		if(firma != null){
		
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));			
		}else{
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
	        Date thisDate = new Date();
	        String fecha = dateFormat.format(thisDate);
	        	        
	        String descripcion_documento = titulo;	        
	        // String destinatario = usuariorecibetxt;
	        // String iniciales = "LDSM/ASR/AVA/GBA/JCTT";
	        // String infoaFirmar = "||"+fecha+"|"+bitacora+"|"+descripcion_documento+"|"+destinatario+"|"+iniciales+"||";
	        // String infoaFirmar = "||"+fecha+"|"+bitacora+"|"+descripcion_documento+"|"+destinatario+"||";
	        String infoaFirmar = "||"+bitacora+"|"+descripcion_documento+"||";
	        String infoaFirmarB64 = Base64.encodeBase64String(infoaFirmar.getBytes());
	        
	        System.out.println("fecha :" + fecha);
	        System.out.println("descripcion :" + descripcion_documento);
	        //System.out.println("destinatario :" + destinatario);
	        //System.out.println("iniciales :" + iniciales);
	        System.out.println("encodeB64 :" + infoaFirmarB64);
	        
			System.out.println("\n\nagregaCadena..");
			CadenaFirmaDao cadenaDao = new CadenaFirmaDao();
			BcAgregaCadenaRespuesta agregacadena = null;
			
			try {
				agregacadena = cadenaDao.agregaCadena(folio, infoaFirmarB64);
				
				if(agregacadena != null){
					 
					 firma = new CadenaFirma();

					 // firma.setClave(clave);
					 firma.setClaveProyecto(claveProyecto);
					 firma.setBitacora(bitacora);
					 firma.setClaveTramite2(cveTipTram);
					 firma.setIdTramite2(idTipTram);
					 firma.setTipoDocId(tipoDoc);
					 firma.setDocumentoId(documentoId);
					 
					 firma.setFolioProyecto(folio);
					 firma.setB64(infoaFirmarB64);
					 firma.setIdentificador(agregacadena.getIdentificador());
					 firma.setOperacion(String.valueOf(agregacadena.getTransferenciaOperacion()));
				 
					 new CadenaFirmaDao().agrega(firma);
					 
					 // despues de agregar la cadena de firma, se consulta para mostrar el identificador al usuario..
					 firma = null;
					 firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);
					 
					 if(firma != null){
					   FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
					 }else{
						 FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
					 }
					 					 
				}else{
						 
					FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
				}	
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
			}
		}
    }
    
    
    /***
     * Limpia el panel de comentarios de la pantalla
     */
    public void limpiaComentarios(){
    	System.out.println("limpiaComentarios");
    	comentarioTurnado = "";
    	
    	// RequestContext.getCurrentInstance().reset("dlgcomentarioscorreccion:pnlComentarios");
    	// RequestContext.getCurrentInstance().update("dlgcomentarioscorreccion:pnlComentarios");
    }
    
    /***
     * Metodo que realiza la corrección del oficio, guardando los comentarios
     */
    public void corrigeOficio(){
    	
    	System.out.println("\n\ncorrigeOficio..");
    	System.out.println("comentarios : " + comentarioTurnado);
    	idDocumento = GenericConstants.ESTATUS_DOCTO_CORRECCION;; // a corrección
    	
    	try {
    		cambioSituacion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
        
        
    
    /***
     * Método que realiza el turnado del oficio recientemente firmado por el director general (dg)
     * @throws Exception
     */
    public void turnaOficio(){  // para : oficios preventivo ( que turna el dg )
    	
    	System.out.println("\n\nturnaOficio..");
    
    	FacesContext fContext = FacesContext.getCurrentInstance();
		String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = bitaProyecto; // fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 

		if(claveProyecto == null)
			claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();
		
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();
		//String documentoId = "1";  // para los oficios preventivos, informacion adicional
		String documentoId = tipoDocId; // se actualiza para todos los tipos de archivos
		comentarioTurnado = "";
		
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		// System.out.println(fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
		// System.out.println(fContext.getExternalContext().getSessionMap().get("idUsu"));
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + documentoId);		
		System.out.println("edoGestTramite : " + edoGestTram);
		System.out.println("-------------------------------------------------------------------------");
    	
       	idDocumento = GenericConstants.ESTATUS_DOCTO_FIRMADO; // FIRMADO ( ojo no es el mismo que 'a firma', porque este es del director general )
    	
    	// busca si ya existe el registro de cadena firma...		
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma cadenafirma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);

		String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
		String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
		String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
		String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);
		
		// url que apunta al oficio dentro del folio( carpeta ). para este caso sera para ponerle esta url al codigo qr
		String ligaQR = Utils.ruta_liga_codigoqr + bitaProyecto;
		    	
				
		if(cadenafirma == null){
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_hay_token_registrado));
			return;
		}    	
		
		// obtenemos la firmalog del usuario logueado ( por su rol y nivel lo sabemos y no necesariamente existe este registro en este momento )
		// short nivel = Short.parseShort(rol.equalsIgnoreCase("sd")?"1":rol.equalsIgnoreCase("da")?"2":"0");			
		CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenafirma, /*nivel,*/  rol);
			
		// detallefirmado es objeto que tiene un estatus y descripcion y una lista de firmas del ws
		DetallesFirmado detallefirmado = null;
		try{
			detallefirmado = firmaDao.getFirmas(cadenafirma);
			// el ws no responde
			if(detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() != 0)){
				System.out.println("\nno responde el servicio web o no hay firmas...");
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));		
				return;
			// como el ws si esta respondiendo, analizar las firmas a ver si alguna de ellas corresponde con la de firmaLog actual...
			}else if (detallefirmado != null && detallefirmado.getEstado() == 0 && ( detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)){
				
				System.out.println("\nel web service si trae firmas...");
				// si no existe una firmalog de ese rol (se asume que es la primera vez que turna el oficio aunque aun no sabemos si ya firmo electronicamente, es lo que vamos a comprobar)
				if(cadenafirmaLog == null){
					
					System.out.println("\ncaso donde NO HAY cadenafirmalog para el rol " + rol);
					// primero necesitamos la lista de firmas del ws, ya la tenemos porque es 'detallefirmado'
					// luego sacamos la cadenafirmaloglist ( aqui pudiera haber otras firmas pero no la del rol actual )
					// obtener de la lista de firmas del ws, la ultima firma de un rfc que no este en cadenafirmaloglist					
					Firmas firmaws = firmaDao.GetUltimaFirmaExcluidadeCadenaFirmaLogList(detallefirmado, cadenafirma);
					
					// para este caso si no hay firma del ws decimos que no ha firmado el usuario...
					if(firmaws == null){
						System.out.println("\nen el ws no existe firma que supla el registro de cadenafirmalog...");
						FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
						return;
					}else{
						
						System.out.println("\nen el ws existe una firma que suple el registro de cadenafirmalog...");
						System.out.println("por tanto se insertará en cadenafirmalog...");
						// SE ASUME QUE SI FIRMÓ YA (como SI trajo una firma que no esta en la lista de firmas de cadenafirmalog..)
						// por tanto primero agregamos el registro de cadenafirmalog correspondiente...
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
						Date fcha = formatter.parse(firmaws.getFecha());
						
						CadenaFirmaLog firmaLog = new CadenaFirmaLog();
						firmaLog.setCadenaFirma(cadenafirma);
						firmaLog.setEstatus((short)1);
						//firmaLog.setNivel(nivel);		
						firmaLog.setRol(rol);
						firmaLog.setFecha(fcha);
						firmaLog.setFirma(firmaws.getFirma());
						firmaLog.setRfc(firmaws.getRfc());
						
						boolean resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
						System.out.println("firmalog agregada: " + resp);
						
						if(resp == true){
                                                        DocumentoRespuestaDgira documento = new DocumentoRespuestaDgira();
                                                        documento.setDocumentoUrl(urlOfic);
                                                        documento.setDocumentoNombre(nombreOfic);
							firmaDao.editarPdf(documento, cadenafirma, ligaQR);
							firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(),  cadenafirma.getFolioProyecto());
							
							// y mandamos turnar...
							cambioSituacion();
						}else{
							throw new Exception("Error: al guardar el log de la firma para el rol actual");
						}
					}
				}else{
					
					System.out.println("\ncaso donde SI HAY ya una cadenafirmalog para el rol " + rol);
					
					// Regresa una Firmas si es que hay unas mas actual para el rfc que traemos en cadenafirmalog
					Firmas firmaws = firmaDao.GetUltimaFirmaSuperiorDelRFCActual(detallefirmado, cadenafirmaLog);
					
					// para este caso si no hay firma del ws decimos que no ha firmado el usuario...
					if(firmaws == null){
						System.out.println("\nen el ws no existe firma que supla el registro de cadenafirmalog...");
						FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
						return;
					}else{
						
						System.out.println("\nen el ws existe una firma que suple el registro de cadenafirmalog...");
						System.out.println("por tanto se insertará en cadenafirmalog...");
						// SE ASUME QUE SI FIRMÓ YA (como SI trajo una firma que no esta en la lista de firmas de cadenafirmalog..)
						// por tanto primero EDITAMOS el registro de cadenafirmalog correspondiente...
						// para este caso solo actualizamos la fecha y ponemos activo el estatus
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
						Date fcha = formatter.parse(firmaws.getFecha());
						cadenafirmaLog.setFecha(fcha);
						cadenafirmaLog.setEstatus((short)1);
						cadenafirmaLog.setFirma(firmaws.getFirma());
						boolean resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
						System.out.println("firmalog modificada: " + resp);
						
						if(resp == true){
                                                        DocumentoRespuestaDgira documento = new DocumentoRespuestaDgira();
                                                        documento.setDocumentoUrl(urlOfic);
                                                        documento.setDocumentoNombre(nombreOfic);
							firmaDao.editarPdf(documento, cadenafirma, ligaQR);
							firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(),  cadenafirma.getFolioProyecto());
							
							// y mandamos turnar...
							cambioSituacion();
						}else{
							throw new Exception("Error: al actualizar el log de la firma para el rol actual");
						}
					}
				}
				
			}else{

				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_se_realizo_turnado));
			}   
		}catch(Exception ex){
			System.out.println(ex.getMessage());			
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
		}
		
	    	    	
//		try {
//			firmaDao.editarPdf(urlOfic, nombreOfic, cadenafirma, ligaQR);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	
    }
    
    
    /***
     * Metodo que realiza un guardado del documento, cambiandole la situacion correspondiente a la firma o corrección
     * @throws IOException
     * @throws DocumentException
     */
    public void cambioSituacion() throws IOException, DocumentException
    {
    	
    	System.out.println("\n\ncambioSituacion..");  	   	
    	
        //tipoDoc=7;
          suspPorInfoAdic(tipoDoc);
          comenatriosTab = daoBitacora.tablabOsevOfic(bitaProyecto, tipoDoc,tipoDocId, idEntidad);
          System.out.println("comenatriosTab SegVíasGuradado 2 size: " + comenatriosTab.size());
        //boton de Turnar Oficio
        try {                        
            DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
            //combo = todoOficDoc(); //llenar combo con todos lo aficios
            //nombreOfic  estadoOfic

        } catch (Exception er33) {
            //JOptionPane.showMessageDialog(null, "idDocumento:  " + er33.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
        }
        if (DocRespDgira.size() > 0) 
        {
            List<DocumentoRespuestaDgira> lista;
            lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
            for (DocumentoRespuestaDgira x : lista) {
                nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
                urlOfic = x.getDocumentoUbicacion();
            }
            for (Object o : DocRespDgira) {
                URLdb = o.toString();
            }
        } else {
            URLdb = "";
        }
        //JOptionPane.showMessageDialog(null, "turnaDocsBtnP :  " + turnaDocsBtnP , "Error", JOptionPane.INFORMATION_MESSAGE); 
        //----------------------verificar si el boton para turnar los  oficios, el que se encinetra dentro del apartado Turnado de Oficio    
        idAux = 0;
        maxIdHisto = 0;
        //se verifica el id maximo de oficio a tratar (en este caso el tipo de prevencion=4)
        try {
            maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);

            for (Object o : maxDocsDgiraFlujoHist) {
                idAux = Integer.parseInt(o.toString());
                maxIdHisto = idAux.shortValue();
            }
        } catch (Exception xxx) {
            //JOptionPane.showMessageDialog(null, "idDocumento:  " + xxx.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
        }
        if (maxIdHisto > 0) {   //trae todo lo relaionado al id maximo de tabla DOCUMENTO_DGIRA_FLUJO_HIST, relativo a una bitacora
            docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, maxIdHisto);
            statusProy = daoBitacora.estatusTramBita(bitaProyecto);
            try {
                //estatus del oficio en cuestion 
                statusDocRespDgira = daoDg.statusDocsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, maxIdHisto);
            } catch (Exception er) {
                //JOptionPane.showMessageDialog(null, "idDocumento:  " + er.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
            }

            for (Object o : statusDocRespDgira) {
                statusDoc = Integer.parseInt(o.toString());
            }
        }
        //JOptionPane.showMessageDialog(null,  "area envio: " + docsDgiraFlujoHistEdit.getIdAreaEnvioHist() + "   maxIdHisto: " + maxIdHisto , "Error", JOptionPane.INFORMATION_MESSAGE);
        if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
            
        	// pool delegaciones
        	String perfilUsr = "";
        	try { 
                areaResiveDocCon = daoBitacora.jerarquiaTbArea(docsDgiraFlujoHistEdit.getIdAreaEnvioHist(), idEntidad);
                //JOptionPane.showMessageDialog(null, "idDocumento:  " +areaResiveDocCon.size(), "Error", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception er2) {
                //JOptionPane.showMessageDialog(null, "err:  " + er2.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
            }
            for (Object o : areaResiveDocCon) {
                jerarquia = o.toString();
            }
         
            // pool delegaciones
            if(docsDgiraFlujo == null){
            	// en este caso obtenemos el documento dgira porque no se ha consultado
            	docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
            }

            try{
            	perfilUsr = docsDgiraFlujo.getRolId().getNombreCorto().trim();
            }catch(NullPointerException exnp){
            	perfilUsr = jerarquia.toLowerCase();
            }
            
            // el statusDoc y estatus del documento_dgira_flujo debe ser igual ya que al turnar setea el estatus en ambos registros ( padre y detalle )
        	perfilSig = AppSession.siguienteRol(perfilUsr, docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
        	//perfilSig = AppSession.siguienteRol(perfilUsr, statusDoc);
            System.out.println("perfilSig :" + perfilSig);
            

            //si el perfil en session es igual al perfil al que se le envio el oficio, perimite la edicion del oficio
            if (sesion.getAttribute("rol").toString().equals(perfilSig)) {
                if (statusDoc == 4) //si viene firmado
                {
                    turnaDocsBtn = true; //ocultar bootn de turnado
                    URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                }
                else  //si no viene fiermado
                {
                    turnaDocsBtn = false;//visualizar boton de turnado
                    if (sesion.getAttribute("rol").toString().equals("eva"))
                    {
                        URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + ",7," + bitaProyecto + "," + tipoDocId; //solo visualiza PDF y no lo modifica
                    }
                    else
                    {
                        URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                    }
                }
            } else {
                turnaDocsBtn = true; //ocultar bootn de turnado
                URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
            }
        } else {

            turnaDocsBtn = false;//visualizar boton de turnado
            if (sesion.getAttribute("rol").toString().equals("eva"))
            {
                URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + ",7," + bitaProyecto + "," + tipoDocId; //solo visualiza PDF y no lo modifica  bitaProyOfic
            }
            else
            {
                URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
            }
        }

    }

    
    
    /***
     * Metodo que realiza un guardado del documento, cambiandole la situacion correspondiente a la firma o corrección, esto para el oficio de informacion adicional
     * @throws IOException
     * @throws DocumentException
     */
    public void cambioSituacionInfoAdicional() throws IOException, DocumentException
    {
    	
    	System.out.println("\n\ncambioSituacionInfoAdicional..");  	   	
    	
    	 	
        System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		System.out.println("bitacora :" + bitaProyecto);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + tipoDocId);		
		System.out.println("idEntidad : " + idEntidad);
		System.out.println("cveTipTram : " + cveTipTram);
		System.out.println("idTipTram : " + idTipTram);
		System.out.println("edoGestTram : " + edoGestTram);
		System.out.println("-------------------------------------------------------------------------");
        
        //tipoDoc=7;
          suspPorInfoAdic(tipoDoc);
          comenatriosTab = daoBitacora.tablabOsevOfic(bitaProyecto, tipoDoc,tipoDocId, idEntidad);
          System.out.println("comenatriosTab SegVíasGuradado 2 size: " + comenatriosTab.size());

          
        //boton de Turnar Oficio
        try {                        
            DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, idEntidad, tipoDocId);
            //combo = todoOficDoc(); //llenar combo con todos lo aficios
            //nombreOfic  estadoOfic

        } catch (Exception er33) {
            //JOptionPane.showMessageDialog(null, "idDocumento:  " + er33.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
        }
        
        
        if(docsDgiraFlujo == null){
        	// en este caso obtenemos el documento dgira porque no se ha consultado
        	docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, idEntidad, tipoDocId);
        }
        
        if (DocRespDgira.size() > 0) 
        {
            List<DocumentoRespuestaDgira> lista;
            lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, idEntidad, tipoDocId);
            for (DocumentoRespuestaDgira x : lista) {
            	 try{
                     if(docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO){
                     	urlOfic = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoDoc+"&entidadfederativa="+edoGestTram+"&documentoid="+tipoDocId;
                     }else{
                     	urlOfic = x.getDocumentoUrl();
                     }
                 }catch(Exception exx){
                 	System.out.println("\nno asignó el urlOfic de docsDgiraFlujo ya que este ultimo es null");
                 	//exx.printStackTrace();
                 	if(x != null){
                 		urlOfic = x.getDocumentoUrl();
                 	}
                 }
            }
           
        } 

        //----------------------verificar si el boton para turnar los  oficios, el que se encinetra dentro del apartado Turnado de Oficio    
        idAux = 0;
        maxIdHisto = 0;
        //se verifica el id maximo de oficio a tratar (en este caso el tipo de prevencion=4)
        try {
            maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, idEntidad, tipoDocId);

            for (Object o : maxDocsDgiraFlujoHist) {
                idAux = Integer.parseInt(o.toString());
                maxIdHisto = idAux.shortValue();
            }
        } catch (Exception xxx) {
            //JOptionPane.showMessageDialog(null, "idDocumento:  " + xxx.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
        }
        if (maxIdHisto > 0) {   //trae todo lo relaionado al id maximo de tabla DOCUMENTO_DGIRA_FLUJO_HIST, relativo a una bitacora
            docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, idEntidad, tipoDocId, maxIdHisto);
            statusProy = daoBitacora.estatusTramBita(bitaProyecto);
            try {
                //estatus del oficio en cuestion 
                statusDocRespDgira = daoDg.statusDocsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, idEntidad, tipoDocId, maxIdHisto);
            } catch (Exception er) {
                //JOptionPane.showMessageDialog(null, "idDocumento:  " + er.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
            }

            for (Object o : statusDocRespDgira) {
                statusDoc = Integer.parseInt(o.toString());
            }
        }
          
         turnaDocsBtn = true; //ocultar bootn de turnado
         URLgenOficPre = urlOfic; //solo visualiza PDF y no lo modifica
               
    }
    
	public Tbusuario getUsuariorecibe() {
		return usuariorecibe;
	}

	public void setUsuariorecibe(Tbusuario usuariorecibe) {
		this.usuariorecibe = usuariorecibe;
	}

	public String getUsuariorecibetxt() {
		return usuariorecibetxt;
	}

	public void setUsuariorecibetxt(String usuariorecibetxt) {
		this.usuariorecibetxt = usuariorecibetxt;
	}        

 
	private void getTituloOficio(short tipoDoc){

		if (tipoDoc == 7)
	    {
	        titulo= "Oficio de información adicional";
	    }
		else if (tipoDoc == 4)
	    {
	        titulo= "Oficio de prevención";
	    }
		else if (tipoDoc == 5)
	    {
	        titulo= "Prevención por CheckList y Pago de derechos";
	    }
		else if (tipoDoc == 1)
	    {
	        titulo= "Oficio de opinión técnica";
	    }
		else if (tipoDoc == 2)
	    {
	        titulo= "Oficio de notificación a gobiernos";
	    }
		else if (tipoDoc == 3)
	    {
	        titulo= "Oficio de pago de derechos";
	    }
		else if (tipoDoc == 8)
	    {
	        titulo= "Oficio de consulta pública-Delegación";
	    }
		else if (tipoDoc == 9)
	    {
	        titulo= "Oficio de consulta pública-Promovente";
	    }
		else if (tipoDoc == 10)
	    {
	        titulo= "Oficio de consulta pública-Positiva";
	    }
		else if (tipoDoc == 11)
	    {
	        titulo= "Oficio de consulta pública-Ciudadanos";
	        
	    }else{
	    	titulo= "Nombre de oficio";
	    }
	}

	public int getIsUrlDG() {
		return isUrlDG;
	}

	public void setIsUrlDG(int isUrlDG) {
		this.isUrlDG = isUrlDG;
	}

	public int getBandeja() {
		return bandeja;
	}

	public void setBandeja(int bandeja) {
		this.bandeja = bandeja;
	}


	public Integer getStatusDoc() {
		return statusDoc;
	}

	public void setStatusDoc(Integer statusDoc) {
		this.statusDoc = statusDoc;
	}

	public int getGO() {
		return GO;
	}

	public void setGO(int gO) {
		GO = gO;
	}

	public int getTO() {
		return TO;
	}

	public void setTO(int tO) {
		TO = tO;
	}
	
	
	
	
    
}
