package mx.gob.semarnat.view;

public class RevisionSolicitud {
	private final String DEFAULT_IMG = "/resources/images/crossmark.png";
	private final String CHECK_IMG = "/resources/images/checkmark.png";
	private String img;	
	private boolean validado;
	private String observaciones;
	private final String descripcionRequisito;
	private int pos;
	private short idSec;
	private short idReq;
	private boolean existe;
	private long idExencion;
	
	public RevisionSolicitud(String descripcionRequisito, boolean validado) {
		this.descripcionRequisito = descripcionRequisito;
		this.validado = validado;
		if(this.validado){
			this.img = CHECK_IMG;
		}else{
			this.img = DEFAULT_IMG;
		}		
		this.existe = false;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getDescripcionRequisito() {
		return descripcionRequisito;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public short getIdSec() {
		return idSec;
	}

	public void setIdSec(short idSec) {
		this.idSec = idSec;
	}

	public short getIdReq() {
		return idReq;
	}

	public void setIdReq(short idReq) {
		this.idReq = idReq;
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}

	public boolean isExiste() {
		return existe;
	}

	public void setExiste(boolean existe) {
		this.existe = existe;
	}

	/**
	 * @return the idExencion
	 */
	public long getIdExencion() {
		return idExencion;
	}

	/**
	 * @param idExencion the idExencion to set
	 */
	public void setIdExencion(long idExencion) {
		this.idExencion = idExencion;
	}
}
