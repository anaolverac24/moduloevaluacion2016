/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.go.semarnat.services.ServicioDocument;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.DirectorDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.mia.servicios.ServicioTokenGO;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.CriterioValoresProyecto;
import mx.gob.semarnat.model.dgira_miae.Document;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.dgira_miae.OpinionTecnicaProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.TokenGO;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.model.sinat.SinatSinatec;
import mx.gob.semarnat.secure.AppSession;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.primefaces.context.RequestContext;


/**
 *
 * @author Rodrigo
 */
@SuppressWarnings("serial")
@ManagedBean(name = "detalleExencionView")
@ViewScoped
public class DetalleProyectoExencionView implements Serializable {
	private static final Logger logger = Logger.getLogger(DetalleProyectoExencionView.class.getName());

	private final BitacoraDao dao = new BitacoraDao();
	private final VisorDao daoVisor = new VisorDao();
	private final DirectorDao daoDirector = new DirectorDao();
	private final HttpSession sesion = Util.getSession();
	private int activeTab = 0;
	private String bitacoraNom;
	private int isUrlDG = 0;
	private int bandeja = 1;
	private String liga;
	private String TunaSubDirec;
	private static String idArea;
	private String TunaEvaluador;
	private Boolean MuestraOpcTurnado = false;
	private Boolean MuestraOpcTurnadoNo = false;
	private Boolean dePrevenc = false; // bandera que indica si un tramite ya vino de una reactivacion de tramite por prevencion
	private Integer idusuario;
	private int banderaOpT = 0, banderaNot = 0;
	private List<SinatDgira> sinatDgiraAT0027;
	// private List<Historial> HistorialAT0001;
	private List<Historial> HistorialAT0027;
	private List<Historial> HistorialI00010;
	// private Historial HistorialAT0022;
	// private Historial HistorialCIS106;
	// private Historial Historial200501;
	private List<Historial> NoHistorialAT0022;
	private List<Historial> NoHistorialCIS106;
	private List<Historial> NoHistorial200501;
	private List<Historial> HistorialCIS303;
	private List<Historial> HistorialATIF01;
	private List<Historial> HistorialCIS302;
	private List<Historial> HistorialIRA020;
	private List<Historial> Historial040201;
	private List<Historial> HistorialCIS503;
	private List<Historial> HistorialCIS104;
	// comentarios del turnado
	private String comentarioDirectorTurnado = "";
	private String comentarioSubdirectorTurnado = "";
	private Proyecto proyDetalle;
	private SinatSinatec sinatSinatec;
	private Bitacora bitacoraDetalle;
	private Bitacora bitacora;
	private SinatDgira sinatDgira;
	// validacion ¿Esta completo?
	private String sinteGace = "dataEmpty.jpg", anaPre = "dataEmpty.jpg", valiPag = "dataEmpty.jpg", chklst = "dataEmpty.jpg", opTec = "dataEmpty.jpg";
	private Object[] detalleProy;
	private Object[] detalleProyTurn;
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> ProyCapitulos = new ArrayList();
	private SelectItem[] combo;
	private List<Bitacora> listBitacora;
	// bandera indicadora si el tramite se puede encontrar en situacion AT0027, I00010, para ser tunado a evaluador
	private boolean porTurnar;
	private boolean porTurnarSDPrev = false; // bandera indicadora si el tramite se encuentra en el ecc en las ig, situaciones: AT0022, CIS106, para
												// ser turnado a evaluacion
	private boolean yaResuelto = false; // bandera que indica que un tramite ya esta concluido
	private boolean enEvaluacion = false; // bandera que indica si el tramite en sesion esta actualmente ne evaluacion
	// bandera que indica si un tramite ya fue turnado, hablando del listado de proyectos, seran los proyectos de la segunda tabla (proyectos ya
	// turnados)
	private Boolean tramiteYaTurnado = false;
	// para boton de GO
	private Proyecto proyecto;
	private EvaluacionProyecto evaluacionProy;
	private boolean tramSinatec = true;
	private String cveTipTram;
	private Integer idTipTram;
	private String edoGestTram;
	private boolean estudioRiesgo = false;
	private String tipoOfic;
	

	// <editor-fold desc="Constructor" defaultstate="collapsed">
	public DetalleProyectoExencionView() {
		logger.debug(Utils.obtenerLogConstructor("Ini"));
		System.out.println("\n\nDetalleProyectoView..");

		listBitacora = dao.enECC(); // TODOS LOS TRAMITE CON SITUACION CIS104
		Historial bitacoraAT0027;
		FacesContext fContext = FacesContext.getCurrentInstance();

		idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
		idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");

		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		bitacoraNom = (String) fContext.getExternalContext().getSessionMap().get("bitacoraProy");

		if (req.getParameter("tramYaTurnado") != null && req.getParameter("tramYaTurnado").toString().length() > 0) {
			if (req.getParameter("tramYaTurnado").toString().equals("1")) {
				tramiteYaTurnado = true;
			} else {
				tramiteYaTurnado = false;
			}
		} else {
			tramiteYaTurnado = false;
		}

		isUrlDG = Integer.parseInt(req.getParameter("isUrlDG") == null ? "0" : req.getParameter("isUrlDG").toString());
		bandeja = Integer.parseInt(req.getParameter("bandeja") == null ? "1" : req.getParameter("bandeja").toString());

		System.out.println("bitaNum: " + bitacoraNom);
		System.out.println("tramiteYaTurnado: " + tramiteYaTurnado);
		System.out.println("isUrlDG: " + isUrlDG);
		System.out.println("bandeja: " + bandeja);

		fContext.getExternalContext().getSessionMap().put("tramiteYaTurnado", tramiteYaTurnado);
		fContext.getExternalContext().getSessionMap().put("isUrlDG", isUrlDG);
		fContext.getExternalContext().getSessionMap().put("bandeja", bandeja);

		if (bitacoraNom != null) {
			this.proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacoraNom);
			try {
				comentarioDirectorTurnado = dao.datosProySinatDgiraSituacionAT0027(bitacoraNom);
				comentarioSubdirectorTurnado = dao.datosProySinatDgiraSituacionI00010(bitacoraNom);
			} catch (Exception e) {
			}
			// colocalar datos de la bitacora en sesion (clave de tramite, id de tramite y estado de gestion del tramite)
			cveTipTram = dao.cveTipTram(bitacoraNom);
			idTipTram = dao.idTipTram(bitacoraNom);
			edoGestTram = dao.entGestTram(bitacoraNom);
			if (!cveTipTram.isEmpty() && !edoGestTram.isEmpty()) {
				fContext.getExternalContext().getSessionMap().put("cveTramite", cveTipTram);
				fContext.getExternalContext().getSessionMap().put("idTramite", idTipTram);
				fContext.getExternalContext().getSessionMap().put("edoGestTramite", edoGestTram);
			}
			bitacora = dao.datosBitaInd(bitacoraNom);

			if (bitacora != null) { // si el tramite se encuentra actualmente en evaluacion, se puede general suna resolucion o documento de respuesta
				if (bitacora.getBitaSituacion().trim().equals("AT0003")) {
					enEvaluacion = true;
				} else {
					enEvaluacion = false;
				}
			}

			if (sesion != null) {
				if (sesion.getAttribute("rol").toString().equals("da")) {

					HistorialIRA020 = dao.numRegHistStatus(bitacoraNom, "IRA020");
					HistorialCIS302 = dao.numRegHistStatus(bitacoraNom, "CIS302");
					// verificar si en la tabla puente existe la situacion AT0027, para tramites ingresados por sinatec
					sinatDgiraAT0027 = dao.datosProySinatDgira2(bitacoraNom);
					HistorialCIS104 = dao.numRegHistStatus(bitacoraNom, "CIS104");
					// verificar si en la tabla Historial de esquema bitacora existe la situacion AT0027, para aquellos tramites que no hayan sido
					// ingresados por sinatec sino por SINAT
					HistorialAT0027 = dao.numRegHistStatus(bitacoraNom, "AT0027");
					bitacoraAT0027 = (Historial) dao.datosProyHistStatus2(bitacoraNom, "AT0027");

					// este seria el sis104
					if (HistorialCIS104.size() == 2) // si el tamanio es igual a dos, indica que el tramite ya esta activo despues de haber sido
														// prevenido
					{
						dePrevenc = true;
					} else {
						dePrevenc = false;
					}

					// pool
					// si SI esta lleno la tabla punte SINAT_DGIRA con las situaciones NO se mostrara opcion a turnado, por que ya estan turnados
					if (sinatDgiraAT0027.size() == 1 && HistorialAT0027.size() == 1) { // ------------------ ya turnado
						MuestraOpcTurnadoNo = true;
						MuestraOpcTurnado = false;
						detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // TODOS LO TRAMITES CON SITUACION AT0027
					} else { // ------------------------------por turnar
								// si la tabla de punte SINAT_DGIRA viene vacia, se verificara en la tabla Historial
								// si SI encuentra la situacion, no mostrara opcion de turnado
						if (HistorialAT0027.size() == 1) {
							MuestraOpcTurnadoNo = true;
							MuestraOpcTurnado = false;
							detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // TODOS LO TRAMITES CON SITUACION AT0027
						} else {
							// si NO encuentra la situacion en la tabla Historial, no mostrara opcion de turnado (para tramites sin prevencion)
							// si NO esta lleno SI se mostrara opcion a turnado (para tramites de SINATEC)
							if ((HistorialAT0027.isEmpty() && HistorialCIS104.size() == 1) || (sinatDgiraAT0027.size() == 1)
									|| (sinatDgiraAT0027.size() == 2 && !enEvaluacion && HistorialIRA020.size() == 1)
									|| (sinatDgiraAT0027.size() == 2 && !enEvaluacion && HistorialCIS302.size() == 1)) {
								MuestraOpcTurnadoNo = false;
								MuestraOpcTurnado = true;
								if (bitacoraAT0027 != null) {
									if (bitacoraAT0027.getHistoAreaRecibe() != null) {
										TunaSubDirec = bitacoraAT0027.getHistoAreaRecibe();
									}
								}
								//detalleProy = dao.detalleProyDirector(bitacoraNom).get(0); // si NO esta lleno SI se mostrara opcion a turnado

							} else {
								MuestraOpcTurnadoNo = true;
								MuestraOpcTurnado = false;
								detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // si NO esta lleno SI se mostrara opcion a turnado
							}
						}
					}
				} else { // para subdirector
//					detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // TODOS LO TRAMITES CON SITUACION AT0027
				}
			}
			// se mapea bitacora en tabla SINAT_SINATEC para insertar Clave de proyecto y bitacora en tabla PROYECTO de DGIRA_MIAE
			sinatSinatec = dao.sinatSintec(bitacoraNom);
			if (sinatSinatec != null) {
				proyDetalle = daoVisor.detalleFolProy(sinatSinatec.getFolio().toString()); // tabla proyecto de DGIRA_MIAE (folio)
				bitacoraDetalle = dao.datosBitaInd(sinatSinatec.getBitacora()); // Tabla BITACORA de esquema BITACORA (numBitacora)
				evaluacionProy = dao.datosEvaProy(sinatSinatec.getBitacora());
				if (proyDetalle != null && bitacoraDetalle != null) {
					if (proyDetalle.getBitacoraProyecto() == null) {
						proyDetalle.setBitacoraProyecto(bitacoraDetalle.getBitaNumero());
						proyDetalle.setClaveProyecto(sinatSinatec.getProyecto());
						daoVisor.merge(proyDetalle);
					}
					if (evaluacionProy == null) {
						evaluacionProy = new EvaluacionProyecto(bitacoraNom);
						daoVisor.agrega(evaluacionProy);
					}
				}

			} else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite ingresado por SINAT, no exiten datos para el expediente electrónico.", null);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		combo = todoDireccion();
		liga = "#";
		try {
			if (bitacoraNom != null) {
				Proyecto proy = daoVisor.verificaERA2(bitacoraNom);
				fContext.getExternalContext().getSessionMap().put("bitacoraProy", bitacoraNom);
				if (proy != null) {
					if (proy.getProyectoPK() != null) {
						fContext.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
						fContext.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
						tramSinatec = false;

						if (proy.getEstudioRiesgoId() != null) {
							if (proy.getEstudioRiesgoId() == 1) {
								estudioRiesgo = true;
							} else {
								estudioRiesgo = false;
							}
						} else {
							estudioRiesgo = false;
						}
					}
				}

				if (sesion != null) {
					if (sesion.getAttribute("rol").toString().equals("sd")) {
						HistorialI00010 = dao.numRegHistStatus(bitacoraNom, "I00010");
						HistorialAT0027 = dao.numRegHistStatus(bitacoraNom, "AT0027");

						NoHistorialAT0022 = dao.numRegHistStatus(bitacoraNom, "AT0022");
						NoHistorialCIS106 = dao.numRegHistStatus(bitacoraNom, "CIS106");
						NoHistorial200501 = dao.numRegHistStatus(bitacoraNom, "200501");
						HistorialCIS303 = dao.numRegHistStatus(bitacoraNom, "CIS303");
						HistorialIRA020 = dao.numRegHistStatus(bitacoraNom, "IRA020");
						HistorialATIF01 = dao.numRegHistStatus(bitacoraNom, "ATIF01");
						HistorialCIS302 = dao.numRegHistStatus(bitacoraNom, "CIS302");
						Historial040201 = dao.numRegHistStatus(bitacoraNom, "040201");
						HistorialCIS503 = dao.numRegHistStatus(bitacoraNom, "CIS503");

						if (bitacora != null) { // si el tramite se encuentra actualmente en evaluacion, se puede general suna resolucion o documento
												// de respuesta
							if (bitacora.getBitaSituacion().trim().equals("AT0003")) {
								enEvaluacion = true;
							} else {
								enEvaluacion = false;
							}
						}

						if (Historial040201.isEmpty() && HistorialCIS503.isEmpty())// si el tramite no esta resuelto, puede pasar a verificar la
																					// situacion del tramite
						{
							yaResuelto = false;
							// situaciones que indican que el tramite esta en ECC por prevencion o pendiente en segundas vias &&
							// NoHistorial200501.isEmpty()
							if (NoHistorialAT0022.isEmpty() && NoHistorialCIS106.isEmpty()) {
								porTurnarSDPrev = true;
							} // No esta en ECC o segundas vias de prevencion
							else {
								porTurnarSDPrev = false;
							} // SI esta en ECC o segundas vias de prevencion

							if (porTurnarSDPrev) {
								if (bitacora.getBitaSituacion().equals("AT0027"))// tramite enviado al responsable del sector
								{
									porTurnar = true; // se puede turnar a evaluador

								} else {
									if (HistorialI00010.isEmpty() || HistorialI00010.size() == 1)// tramite enviado al evaluador
									{
										if ((HistorialATIF01.size() >= 0 || HistorialCIS302.size() >= 0) && HistorialCIS303.isEmpty() && HistorialIRA020.isEmpty()) {
											porTurnar = false;
										} else {
											porTurnar = true;// SI se puede turnar a evaluador
										}
									} else {
										porTurnar = false;
									} // ya no se puede turnar a evaluador
								}

								// verificar si ya a sido enviado a la situacion CIS303
							} else {
								if (HistorialCIS303.isEmpty() && HistorialIRA020.isEmpty()) // los que estuvieran en ECC por prevencion SIN info
																							// Adicional
								{
									if ((HistorialAT0027.isEmpty() || HistorialAT0027.size() == 1) && NoHistorial200501.size() == 1) // tramites en
																																		// ecc sin
																																		// prevencion
									{
										porTurnarSDPrev = false;
									} else // tramites en ecc y con reactivacion desaes de prevencion
									{
										porTurnarSDPrev = true;

										// HistorialIRA020 HistorialCIS302
										// tramite enviado al evaluador por que vienen de una reactivacion por info adic o por reactivacion de
										// prevencion (HistorialAT0027.size() == 1 && NoHistorial200501.size() > 0 && HistorialI00010.isEmpty())
										if ((HistorialAT0027.size() == 2 && HistorialI00010.isEmpty() && !enEvaluacion)
												|| (HistorialAT0027.size() == 2 && HistorialI00010.size() == 1 && !enEvaluacion && HistorialIRA020.size() == 1)
												|| (HistorialAT0027.size() == 2 && HistorialI00010.size() == 1 && !enEvaluacion && HistorialCIS302.size() == 1)) {
											porTurnar = true;// SI se puede turnar a evaluador

										} else {
											porTurnar = false;
										} // ya no se puede turnar a evaluador
									}
								} else {
									porTurnarSDPrev = true; // Historial040201 HistorialCIS503
									if ((HistorialI00010.size() == 1 || HistorialCIS303.size() == 1) && Historial040201.isEmpty() && HistorialCIS503.isEmpty())// tramite
																																								// enviado
																																								// al
																																								// evaluador
									{
										porTurnar = true;// SI se puede turnar a evaluador
									} else {
										porTurnar = false;
									} // ya no se puede turnar a evaluador

								}
							}
						} else// para cuando el tramite esta resulto
						{
							yaResuelto = true;
						}

					}
				}

				// porTurnar = !bitacora.getBitaSituacion().equals("I00010");
				if (bitacora != null) {
					if (bitacora.getBitaTipotram().equals("MP") || bitacora.getBitaTipotram().equals("DM") || bitacora.getBitaTipotram().equals("MG") || bitacora.getBitaTipotram().equals("DL")) {
						liga = "pantallaInsideCapMIA.xhtml?bitaNum=" + bitacoraNom;
						// liga = "pantallaDaInsideCapMIA.xhtml?bitaNum="+bitacoraNom;
					}
					if (bitacora.getBitaTipotram().equals("IP")) {
						liga = "pantallaInsideCapIP.xhtml?bitaNum=" + bitacoraNom;
						// liga = "pantallaDaInsideCapIP.xhtml";
					}
				}
			}
		} catch (Exception e) {
			System.out.println("bitaProyecto: " + e.getMessage() + " <-------------");
			// JOptionPane.showMessageDialog(null, "aaaa: " + e.getMessage() , " Exito", JOptionPane.INFORMATION_MESSAGE);
		}
		System.out.println("\nROLES DEL USUARIO :");
		System.out.println("Director general :" + AppSession.contieneDG());
		System.out.println("Director area :" + AppSession.contieneDA());
		System.out.println("Subdirector :" + AppSession.contieneSD());
		System.out.println("Evaluador :" + AppSession.contieneEVA());
		System.out.println("MuestraOpcTurnado :" + MuestraOpcTurnado);

		logger.debug(Utils.obtenerLogConstructor("Fin"));
	}// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Llena SelectItem">
	/**
	 *
	 * @return Select item cargado
	 */
	public SelectItem[] todoDireccion() {

		Tbarea areausuario = (Tbarea) sesion.getAttribute("areaDelegacionUsuario");

		List<Tbarea> lista;
		if (sesion.getAttribute("rol").toString().equals("sd")) {
			// lista = dao.evaluadoresTodos(idArea);
			lista = dao.evaluadoresTodos(areausuario, idArea);
		} else {
			// lista = dao.direccionTodos();
			lista = dao.direccionTodos(areausuario);
		}
		int size = lista.size() + 1;
		SelectItem[] items = new SelectItem[size];
		int i = 0;
		items[0] = new SelectItem("0", "-- Seleccione --");
		i++;
		for (Tbarea x : lista) {
			items[i++] = new SelectItem(x.getTbareaPK().getIdarea(), x.getArea());
		}
		return items;
	}// </editor-fold>

	// <editor-fold desc="Muestra los cambios hechos al guardar" defaultstate="collapsed">
	/**
	 * Muestra los cambios hechos al guardar
	 */
	public void guardar() {
		ProyCapitulos = dao.detalleProyCap(TunaSubDirec); // TODOS LOS TRAMITES CON ID DE SITUACION AT0001
	}// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="TURNADO DE DIRECTOR A SUBDIRECTOR">
	/**
	 * cambio de situacion de AT0001 A AT0027, TURNADO DE DIRECTOR A SUBDIRECTOR
	 */
	public void seleccionaReferencia() {

		System.out.println("\nseleccionaReferencia..");

		if (TunaSubDirec != null && bitacoraNom != null) {
			try {
				bitacora = (Bitacora) dao.busca(Bitacora.class, bitacoraNom);
				if (bitacora != null) {
					sinatDgira = new SinatDgira();
					sinatDgira.setId(0);
					sinatDgira.setSituacion("AT0027"); // I00008
					sinatDgira.setIdEntidadFederativa(bitacora.getBitaEntidadGestion());
					sinatDgira.setIdClaveTramite(bitacora.getBitaTipotram()); // STRING BITA_TIPOTRAM
					sinatDgira.setIdTramite(bitacora.getBitaIdTramite());
					sinatDgira.setIdAreaEnvio(idArea.trim());
					sinatDgira.setIdAreaRecibe(TunaSubDirec.trim());
					sinatDgira.setIdUsuarioEnvio(idusuario);
					sinatDgira.setObservaciones(comentarioDirectorTurnado);
					// sinatDgira.setIdUsuarioRecibe(1340);
					sinatDgira.setGrupoTrabajo(new Short("0"));
					sinatDgira.setBitacora(bitacoraNom);
					sinatDgira.setFecha(new Date());
					dao.agrega(sinatDgira);

					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Su información ha sido guardada con éxito.", null);
					FacesContext.getCurrentInstance().addMessage("messages", message);
					porTurnar = false;
				}
			} catch (Exception er) {
				System.out.println("ERR: " + er.getLocalizedMessage());

				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido guardar la información, Intente más tarde.", null);
				FacesContext.getCurrentInstance().addMessage("messages", message);
			}
		}

		if (sesion != null) {
			Historial bitacoraAT0027;
			if (sesion.getAttribute("rol").toString().equals("da")) {
				HistorialIRA020 = dao.numRegHistStatus(bitacoraNom, "IRA020");
				HistorialCIS302 = dao.numRegHistStatus(bitacoraNom, "CIS302");
				// verificar si en la tabla puente existe la situacion AT0027, para tramites ingresados por sinatec
				sinatDgiraAT0027 = dao.datosProySinatDgira2(bitacoraNom);
				HistorialCIS104 = dao.numRegHistStatus(bitacoraNom, "CIS104");
				// verificar si en la tabla Historial de esquema bitacora existe la situacion AT0027, para aquellos tramites que no hayan sido
				// ingresados por sinatec sino por SINAT
				HistorialAT0027 = dao.numRegHistStatus(bitacoraNom, "AT0027");
				bitacoraAT0027 = (Historial) dao.datosProyHistStatus2(bitacoraNom, "AT0027");

				if (HistorialCIS104.size() == 2 || HistorialAT0027.size() == 2) // si el tamaÃ±o es igual a dos, indica que el tramite ya esta activo
																				// despues de haber sido prevenido
				{
					dePrevenc = true;
				} else {
					dePrevenc = false;
				}

				// si SI esta lleno la tabla punte SINAT_DGIRA con las situaciones NO se mostrara opcion a turnado, por que ya estan turnados
				if (sinatDgiraAT0027.size() == 1 && (HistorialCIS104.size() == 1 || HistorialAT0027.size() == 1)) { // ------------------ ya turnado
					MuestraOpcTurnadoNo = true;
					MuestraOpcTurnado = false;
					//detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // TODOS LO TRAMITES CON SITUACION AT0027
				} else { // ------------------------------por turnar
							// si la tabla de punte SINAT_DGIRA viene vacia, se verificara en la tabla Historial
							// si SI encuentra la situacion, no mostrara opcion de turnado
					if (HistorialAT0027.size() == 1 && (HistorialCIS104.size() == 1 || HistorialAT0027.size() == 1)) {
						MuestraOpcTurnadoNo = true;
						MuestraOpcTurnado = false;
						detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // TODOS LO TRAMITES CON SITUACION AT0027
					} else {
						// si NO encuentra la situacion en la tabla Historil, no mostrara opcion de turnado (para tramites sin prevencion)
						// si NO esta lleno SI se mostrara opcion a turnado (para tramites de SINATEC)
						if ((HistorialAT0027.isEmpty() && (HistorialCIS104.size() == 1 || HistorialAT0027.size() == 1))
								|| (sinatDgiraAT0027.size() == 1 && (HistorialCIS104.size() == 2 || HistorialAT0027.size() == 2))
								|| (sinatDgiraAT0027.size() == 2 && (HistorialCIS104.size() == 2 || HistorialAT0027.size() == 2) && !enEvaluacion && HistorialIRA020.size() == 1)
								|| (sinatDgiraAT0027.size() == 2 && (HistorialCIS104.size() == 2 || HistorialAT0027.size() == 2) && !enEvaluacion && HistorialCIS302.size() == 1)) {
							MuestraOpcTurnadoNo = false;
							MuestraOpcTurnado = true;
							if (bitacoraAT0027 != null) {
								if (bitacoraAT0027.getHistoAreaRecibe() != null) {
									TunaSubDirec = bitacoraAT0027.getHistoAreaRecibe();
								}
							}
							detalleProy = dao.detalleProyDirector(bitacoraNom).get(0); // si NO esta lleno SI se mostrara opcion a turnado

						} else {
							MuestraOpcTurnadoNo = true;
							MuestraOpcTurnado = false;
							detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // si NO esta lleno SI se mostrara opcion a turnado
						}
					}
				}
			} else { // para subdirector
				detalleProy = dao.detalleProySubDirector(bitacoraNom).get(0); // TODOS LO TRAMITES CON SITUACION AT0027
			}
		}

		System.out.println("MuestraOpcTurnado :" + MuestraOpcTurnado);

	}// </editor-fold>

	public void generarOficio(String  docId) throws Exception{
		String template_name = null;
		boolean chk = this.chklst.equals("dataInComplete.jpg");
		boolean pag = this.valiPag.equals("dataInComplete.jpg");
		ServicioDocument document = new ServicioDocument();
		Document expedient = null;
		
		if(docId.equals("prevencion")) {
			System.out.println("---PREVENCION");
			if (pag && chk) {
				template_name = "Prevención por pago de derechos y requisítos";
				docId = "124";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if (pag && !chk) {
				template_name = "Prevención por pago de derechos";
				docId = "118";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if (chk && !pag) {
				template_name = "Prevención por requisítos";
				docId = "94";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			}
			generaConexion(template_name, expedient, docId);
		} else if(docId.equals("consultar oficios")) {
			generaConexion(" ", new Document(), "-1");
		} else if(docId.equals("consulta publica")) {
			expedient = document.getDocByIdByType("983", proyecto.getBitacoraProyecto());
			if ( expedient.getId() == 0) {
				template_name = "Consulta Publica Ciudadano Principal";
				generaConexion(template_name, expedient, "983");
				template_name = "Consulta Publica Promovente";
				generaConexion(template_name, expedient, "1077");
				template_name = "Consulta Publica Unidad Administrativa";
				generaConexion(template_name, expedient, "1078");
			} else {
				template_name = "Consulta Publica Ciudadano Secundario";
				generaConexion(template_name, expedient, "1079");
			}
		} else {
			if (docId.equals("684")){
				template_name = "Información Adicional";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if(docId.equals("380")) {
				template_name = "Notificación a Gobierno";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if(docId.equals("992")){
				template_name = "Opinión de Experto"; 
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if(docId.equals("513")){
				template_name = "Resolutivo Desechado";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if(docId.equals("1080")){
				template_name = "Resolutivo Desestimado";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if(docId.equals("1143")){
				template_name = "Resolutivo Procedente IP";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			} else if(docId.equals("1149")){
				template_name = "Resolutivo No Procedente IP";
				expedient = document.getDocByIdByType(docId, proyecto.getBitacoraProyecto());
			}
			generaConexion(template_name, expedient, docId);
		}

	}
	
	public void generaConexion(String template_name, Document expedient, String  docId) {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		RequestContext context = RequestContext.getCurrentInstance();
		ServicioTokenGO go = new ServicioTokenGO();
		TokenGO token = new TokenGO();
		
		try {
			token = go.getToken(getIdusuario());
			String goApiURL = servletContext.getInitParameter("goApi_url");
			String goURL = servletContext.getInitParameter("go_url");
			String valueUtf8TN = new String(template_name.getBytes("UTF-8"));
			String nombre = this.proyecto.getProyNombre();
                        nombre = nombre.replaceAll("\"", "");
                        nombre = nombre.replaceAll("”", ""); 
                        nombre = nombre.replaceAll("“", "");
                        System.out.println(nombre + "------------------");
			String valueUtf8NP = new String(nombre.getBytes("UTF-8"));
			HttpURLConnection conn;
			OutputStream os;
			
			if(docId.equals("-1")){
				URL url = new URL(goURL + "/expedients?token=" + token.getToken()+"&scope=expedients&project_id="+this.proyecto.getBitacoraProyecto()); 
				context.execute("window.open('" + url.toString() + "','_blank')");	
			} else if(expedient == null || expedient.id == 0){
				URL url = new URL(goApiURL + "/expedients"); 
                                System.out.println("url ::: " + url.toString());
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setRequestProperty("x-user-token", token.getToken());
				String input = "{\"expedient\":{\"project_id\":\"" + this.proyecto.getBitacoraProyecto() 
						+ "\",\"name\":\"" + valueUtf8TN + " - " + valueUtf8NP 
						+ "\",\"body\":null,\"header\":null,\"footer\":null,\"document_id\":\"" 
						+ docId + "\",\"document_web\":1,\"assignee_id\":null,\"discussions_attributes\":[]}}";
				os = conn.getOutputStream();
				os.write(input.getBytes());
				os.flush();
				System.out.println("os" + os.toString() + " conn msg:>_ " + conn.getResponseMessage() + " to string:>_ " + conn.toString());
				os.close();
				System.out.println("POST Response Code :: " + conn.getResponseCode());
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String id = "";
				String output;
				while ((output = br.readLine()) != null) {
					JSONParser parser = new JSONParser();
					JSONObject json = (JSONObject) parser.parse(output);
					json = (JSONObject) parser.parse(json.get("expedient").toString());
					System.out.println(json.get("id"));
					id = json.get("id").toString();
				}
				conn.disconnect();				
				URL urlE = new URL(goURL + "/editor/expedient/" + id + "?token=" + token.getToken()+"&scope=expedients&project_id="+this.proyecto.getBitacoraProyecto());
				System.out.println("url :: " + urlE.toString());
				context.execute("window.open('" + urlE.toString() + "','"+ template_name + "')");
				
			} else {
				URL urlE = new URL(goURL + "/editor/expedient/" + expedient.getId().toString() +"?token=" + token.getToken()+"&scope=expedients&project_id="+this.proyecto.getBitacoraProyecto()); 
				System.out.println("url :: " + urlE.toString());
				context.execute("window.open('" + urlE.toString() + "','_blank')");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	public void checkListCompleto() {
		List<Object[]> requisitosNull;
		List<Object[]> requisitos;
		List<Object[]> requisitosAprovados;
		if (bitacoraNom != null) {
                    boolean digital = (bitacoraNom.charAt(5) == 'W');
                    // ----------------verificacion de llenado de checkList---------------
                    requisitosNull = digital ? daoDirector.getReqTramiteNullDigital(bitacoraNom) : daoDirector.getReqTramiteNull(bitacoraNom);
                    requisitos = digital ? daoDirector.getReqTramiteDigital(bitacoraNom) : daoDirector.getReqTramite(bitacoraNom);
                    requisitosAprovados = daoDirector.getTramitesAprovados(bitacoraNom);

                    if (requisitosAprovados.size() == requisitos.size()) { // para cuando aun no se ha guardado nada en el checkList
                            activeTab = 2;
                            chklst = "dataComplete.jpg";
                    } else if (requisitosNull.size() == requisitos.size()) {
                            activeTab = 1;
                            chklst = "dataEmpty.jpg";
                    } else {
                            activeTab = 1;
                            chklst = "dataInComplete.jpg";
                    }
		}
	}

	public void pagoValidadoCompleto() {
		FacesContext context = FacesContext.getCurrentInstance();
		String folioProyecto = "";
		short serialProyecto;
		List<CriterioValoresProyecto> valoresCritEva = null;
		folioProyecto = (String) context.getExternalContext().getSessionMap().get("userFolioProy");
		if (context.getExternalContext().getSessionMap().get("userSerialProy")!=null)
			serialProyecto = (Short) context.getExternalContext().getSessionMap().get("userSerialProy");
		else
			serialProyecto = 1;
		try {
			valoresCritEva = daoDirector.getValorCritEvaluador2(folioProyecto, serialProyecto);
		} catch (Exception exp) {
			System.out.println("error: pago derechos: " + exp.getMessage());
		}

		if (valoresCritEva.isEmpty() || valoresCritEva == null) {
			valiPag = "dataEmpty.jpg";
			activeTab = 2;
		} else if (valiPag.equals("dataEmpty.jpg")) {
			int valorEva = 0;
			int valorUser = 0;
			CriterioValoresProyecto c1;
			int correct = 0;
			for (int i = 0; i < valoresCritEva.size(); i++) {
				c1 = (CriterioValoresProyecto) valoresCritEva.get(i);
				valorEva += Integer.parseInt(c1.getCriterioValorEvaluacion().toString());
				valorUser += Integer.parseInt(c1.getCriterioValorUsuario().toString());
				if (valorEva == valorUser)
					correct++;
			}
			if (correct < 3) {
				valiPag = "dataInComplete.jpg";
				activeTab = 2;
			} else if (correct == 3) {
				valiPag = "dataComplete.jpg";
				activeTab = 3;
			}
		}
		System.out.println("VALOR DE LA IMAGEN ::: " + valiPag);
	}

	public void pagoValidadoCompleto(double valEv, double valUs) {
		FacesContext context = FacesContext.getCurrentInstance();
		String folioProyecto = "";
		short serialProyecto;
		List<CriterioValoresProyecto> valoresCritEva = null;
		folioProyecto = (String) context.getExternalContext().getSessionMap().get("userFolioProy");
		serialProyecto = (Short) context.getExternalContext().getSessionMap().get("userSerialProy");
		try {
			valoresCritEva = daoDirector.getValorCritEvaluador2(folioProyecto, serialProyecto);
		} catch (Exception exp) {
			System.out.println("error: pago derechos: " + exp.getMessage());
		}

		if (valoresCritEva.isEmpty() || valoresCritEva == null) {
			valiPag = "dataEmpty.jpg";
			activeTab = 2;
		} else {
			if (valEv == valUs) {
				valiPag = "dataComplete.jpg";
				activeTab = 3;
			} else {
				valiPag = "dataInComplete.jpg";
				activeTab = 2;
			}
		}
	}

	public void analisisPreCompleto() {
		List<Object[]> analisisPreeList;
		analisisPreeList = daoDirector.getValorAnalisisPree(bitacoraNom);
		if (analisisPreeList.isEmpty()) {
			activeTab = 3;
			anaPre = "dataEmpty.jpg";
		} else {
			if (analisisPreeList.size() == 33) {
				anaPre = "dataComplete.jpg";
				activeTab = 4;
			} else if (analisisPreeList.size() == 0) {
				anaPre = "dataEmpty.jpg";
				activeTab = 3;
			} else {
				anaPre = "dataInComplete.jpg";
				activeTab = 3;
			}
		}

	}

	public void sintesisGacetaCompleto() {
		String eval2;
		eval2 = null;
		eval2 = daoVisor.evaProyecto(bitacoraNom);
		if (eval2.length() > 10) {
			sinteGace = "dataComplete.jpg";
			activeTab = 5;
		} else {
			sinteGace = "dataEmpty.jpg";
			activeTab = 4;
		}
	}

	public void opinionTecnicaCompleta() {
		if (activeTab != 8) {
			opTec = "dataComplete.jpg";
			if (allComplete()) {
				activeTab = 8;
			} else {
				activeTab = 0;
			}
		} else if (this.banderaOpT == 1 || this.banderaOpT == 1) {
			opTec = "dataComplete.jpg";
		} else {
			opTec = "dataEmpty.jpg";
		}
	}

	public void regresar() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> params = context.getExternalContext().getRequestParameterMap();
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		activeTab = Integer.parseInt(params.get("index"));
		if (getActiveTab() > 0)
			reqcontEnv.execute("PF('myTabView').select(" + getActiveTab() + ");");
	}

	public boolean allComplete() {
            boolean res = false;
            try {
                if (getChklst().equals("dataComplete.jpg")) {
                
                    if (getValiPag().equals("dataComplete.jpg")) {
                        if (getAnaPre().equals("dataComplete.jpg")) {
                            if (getSinteGace().equals("dataComplete.jpg")) {
                                res = true;
                            }
                        }
                    }
                }                                     
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(DetalleProyectoExencionView.class.getName()).log(Level.SEVERE, null, ex);
            }            
            return res;
	}

        public boolean existExpedient() {
            boolean resp = false;
            ServicioDocument document = new ServicioDocument();
            List<Document> expedientPrevencion;
            try {
                expedientPrevencion = document.getDocPrevencion(proyecto.getBitacoraProyecto());
                if (expedientPrevencion.size() > 0) {
                    resp = true;
                    System.out.println("::: ----->>>>> " + expedientPrevencion);
                }
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(DetalleProyectoExencionView.class.getName()).log(Level.SEVERE, null, ex);
            }            
            return resp;
        }
        
	// <editor-fold desc="Referencias para el subdirector" defaultstate="collapsed">
	@SuppressWarnings("unused")
	public void seleccionaReferenciaSub() {
		List<Object[]> requisitosNull;
		List<Object[]> requisitosNo;
		List<Object[]> requisitos;
		List<Object[]> valoresCritEva = null;// trae los valores que eleguio el SD en el apartado de Pago de derechos
		// EvaluacionProyecto eval2;
		String eval2;
		List<Object[]> analisisPreeList;
		boolean digital = false;
		boolean checkList = false;
		boolean pagoDerechos = false;
		boolean analisisPree = false;
		boolean sintesisGac = false;
		boolean opinionTecn = false;
		boolean esIP = false;
		boolean fisico;
		String folioProyecto = "";
		short serialProyecto;
		List<OpinionTecnicaProyecto> opinionTecnica;

		if (bitacoraNom != null) {
			fisico = !(bitacoraNom.charAt(5) == 'W'); // SI ES TRUE= TRAMITE FISICO, SE MANEJA NE BASE A BITACORA FALSE=TRAMITE ELECTRONICO SE MANEJA
														// EN BASE A FOLIO
			digital = (bitacoraNom.charAt(5) == 'W');
			esIP = (bitacoraNom.charAt(3) == 'I' && bitacoraNom.charAt(4) == 'P');

			// ----------------verificacion de llenado de checkList---------------
			requisitosNull = digital ? daoDirector.getReqTramiteNullDigital(bitacoraNom) : daoDirector.getReqTramiteNull(bitacoraNom);
			requisitos = digital ? daoDirector.getReqTramiteDigital(bitacoraNom) : daoDirector.getReqTramite(bitacoraNom);
			if (!(requisitosNull.size() == requisitos.size())) // para cuando aun no se ha guardado nada en el checkList
			{
				checkList = true;
			} else {
				checkList = false;
			}

			if (!esIP)// si no es IP no podra verificar pado de derechos ni analisis preeliminar, solo es para MIA
			{
				// ----------------verificacion de llenado de pago de derechos---------------
				if (fisico)// es tramite fisico
				{
					try {
						valoresCritEva = daoDirector.getValorCritEvaluador(bitacoraNom, (short) 1);
					} catch (Exception exp) {
						System.out.println("error: pago derechos: " + exp.getMessage());
					}
				} else// es tramite electronico)
				{
					FacesContext context = FacesContext.getCurrentInstance();

					folioProyecto = (String) context.getExternalContext().getSessionMap().get("userFolioProy");
					serialProyecto = Short.valueOf("1"); //context.getExternalContext().getSessionMap().get("userSerialProy");
					try {
						valoresCritEva = daoDirector.getValorCritEvaluador(folioProyecto, serialProyecto);
					} catch (Exception exp) {
						System.out.println("error: pago derechos: " + exp.getMessage());
					}
				}
				if (valoresCritEva.isEmpty()) {
					pagoDerechos = false;
				} else {
					pagoDerechos = true;
				}

				// ----------------verificacion de llenado de analisis preeliminar---------------
				try {
					analisisPreeList = daoDirector.getValorAnalisisPree(bitacoraNom);
					if (analisisPreeList.isEmpty()) {
						analisisPree = false;
					} else {
						if (analisisPreeList.size() == 33) {
							analisisPree = true;
						} else {
							analisisPree = false;
						}
					}
				} catch (Exception exp1) {
					System.out.println("error: analisis preeliminar: " + exp1.getMessage());
				}

				// ------------------verificar llenado de opiniones tecnicas------------------------------------

				opinionTecnica = daoDirector.getOpinionTecnica(bitacoraNom);

				if (opinionTecnica.isEmpty()) {
					opinionTecn = false;
				} else {
					opinionTecn = true;
				}

			}

			// ----------------verificacion de llenado de sintesis de la gaceta---------------
			eval2 = null;
			// eval = (EvaluacionProyecto) daoDirector.busca(EvaluacionProyecto.class, bitacoraNom);
			eval2 = daoVisor.evaProyecto(bitacoraNom);
			if (eval2.length() > 10) {
				sintesisGac = true;
				// if(eval2.getEvaSistesisProyecto() != null)
				// {
				// if(eval2.getEvaSistesisProyecto().length()> 10)
				// { sintesisGac = true; }
				// else
				// { sintesisGac = false; }
				// }
				// else
				// { sintesisGac = false; }
			} else {
				sintesisGac = false;
			}

			System.out.println("Secciones obligatorias para SD antes de turnar trámite a evaluador. checkList: " + checkList + "  pagoDerechos: " + pagoDerechos + "  analisisPree: " + analisisPree
					+ "  sintesisGac: " + sintesisGac);
		}

		// cambio de situacion de AT0008 A I00010, TURNADO DE SUBDIRECTOR A EVALUADOR
		if (TunaEvaluador.length() > 0 && bitacoraNom.length() > 0 && !TunaEvaluador.equals("0"))
		// JOptionPane.showMessageDialog(null, " Entro turnaEva: " + TunaEvaluador + " bita:" + bitacoraNom , "Error",
		// JOptionPane.INFORMATION_MESSAGE);
		{
		
                    try {
                            bitacora = (Bitacora) dao.busca(Bitacora.class, bitacoraNom);
                            if (bitacora != null) {

                                    sinatDgira = new SinatDgira();
                                    sinatDgira.setId(0);
                                    sinatDgira.setSituacion("I00010");
                                    sinatDgira.setIdEntidadFederativa(bitacora.getBitaEntidadGestion());
                                    sinatDgira.setIdClaveTramite(bitacora.getBitaTipotram()); // STRING BITA_TIPOTRAM
                                    sinatDgira.setIdTramite(bitacora.getBitaIdTramite());
                                    sinatDgira.setIdAreaEnvio(idArea.trim());
                                    sinatDgira.setIdAreaRecibe(TunaEvaluador.trim());
                                    sinatDgira.setIdUsuarioEnvio(idusuario);
                                    sinatDgira.setObservaciones(comentarioSubdirectorTurnado);
                                    // sinatDgira.setIdUsuarioRecibe(1340);
                                    sinatDgira.setGrupoTrabajo(new Short("0"));
                                    sinatDgira.setBitacora(bitacoraNom);
                                    sinatDgira.setFecha(new Date());
                                    dao.agrega(sinatDgira);

                                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Su información ha sido guardada con éxito.", null);
                                    FacesContext.getCurrentInstance().addMessage("messages", message);
                                    porTurnar = false;
                            }
                    } catch (Exception er) {
                            System.out.println("ERR: " + er.getLocalizedMessage());

                            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido guardar la información, Intente más tarde.", null);
                            FacesContext.getCurrentInstance().addMessage("messages", message);
                    }			
		} else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Seleccione al evaluador.", null);
			FacesContext.getCurrentInstance().addMessage("messages", message);
		}

	}// </editor-fold>

	// <editor-fold desc="Getters & setters" defaultstate="collapsed">
	public String getBitacoraNom() {
		return bitacoraNom;
	}

	public void setBitacoraNom(String bitacoraNom) {
		System.out.println("setter bitacora.");
		this.bitacoraNom = bitacoraNom;
	}

	public boolean isPorTurnar() {
		return porTurnar;
	}

	public void setPorTurnar(boolean porTurnar) {
		this.porTurnar = porTurnar;
	}

	public String getTunaSubDirec() {
		return TunaSubDirec;
	}

	public void setTunaSubDirec(String TunaSubDirec) {
		this.TunaSubDirec = TunaSubDirec;
	}

	public String getIdArea() {
		return idArea;
	}

	public Integer getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}

	public SinatDgira getSinatDgira() {
		return sinatDgira;
	}

	public void setSinatDgira(SinatDgira sinatDgira) {
		this.sinatDgira = sinatDgira;
	}

	public List<Object[]> getProyCapitulos() {
		return ProyCapitulos;
	}

	public void setProyCapitulos(List<Object[]> ProyCapitulos) {
		this.ProyCapitulos = ProyCapitulos;
	}

	public List<Bitacora> getListBitacora() {
		return listBitacora;
	}

	public void setListBitacora(List<Bitacora> listBitacora) {
		this.listBitacora = listBitacora;
	}

	public Boolean getMuestraOpcTurnado() {
		return MuestraOpcTurnado;
	}

	public void setMuestraOpcTurnado(Boolean MuestraOpcTurnado) {
		this.MuestraOpcTurnado = MuestraOpcTurnado;
	}

	public Object[] getDetalleProy() {
		return detalleProy;
	}

	public void setDetalleProy(Object[] detalleProy) {
		this.detalleProy = detalleProy;
	}

	public SinatSinatec getSinatSinatec() {
		return sinatSinatec;
	}

	public void setSinatSinatec(SinatSinatec sinatSinatec) {
		this.sinatSinatec = sinatSinatec;
	}

	public Proyecto getProyDetalle() {
		return proyDetalle;
	}

	public void setProyDetalle(Proyecto proyDetalle) {
		this.proyDetalle = proyDetalle;
	}

	public Bitacora getBitacoraDetalle() {
		return bitacoraDetalle;
	}

	public void setBitacoraDetalle(Bitacora bitacoraDetalle) {
		this.bitacoraDetalle = bitacoraDetalle;
	}

	public SelectItem[] getCmbDirec() {
		return combo;
	}

	public void setCmbDirec(SelectItem[] cmbDirec) {
		this.combo = cmbDirec;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	public Bitacora getBitacora() {
		return bitacora;
	}

	public void setBitacora(Bitacora bitacora) {
		this.bitacora = bitacora;
	}

	public void setTunaEvaluador(String TunaEvaluador) {
		this.TunaEvaluador = TunaEvaluador;
	}

	public String getTunaEvaluador() {
		return TunaEvaluador;
	}

	@SuppressWarnings("unused")
	public String ligaDetalle() {
		FacesContext context = FacesContext.getCurrentInstance();
		bitacora = (Bitacora) dao.busca(Bitacora.class, bitacoraNom);
		String tipoOficio;
		if (bitacora != null) {
			if (sesion != null) {
				if (sesion.getAttribute("rol").toString().equals("dg")) // Director General
				{// tipoOfic

					liga = "Commons/turnadoOficDG.xhtml?bitaNum=" + bitacoraNom + "&?faces-redirect=true";
					// liga = "pantallaInsideCapMIA.xhtml?bitaNum=" + bitacoraNom + "&tramYaTurnado=0&?faces-redirect=true";
					System.out.println("tipoOfic: " + tipoOfic);
				} else {
					if (bitacora.getBitaTipotram().equals("MP") || bitacora.getBitaTipotram().equals("DM") || bitacora.getBitaTipotram().equals("MG") || bitacora.getBitaTipotram().equals("DL")) {
						liga = "pantallaInsideCapMIA.xhtml?bitaNum=" + bitacoraNom + "&tramYaTurnado=0&?faces-redirect=true";
					}
					if (bitacora.getBitaTipotram().equals("IP")) {
						liga = "pantallaInsideCapIP.xhtml?bitaNum=" + bitacoraNom + "&tramYaTurnado=0&?faces-redirect=true";
					}
				}
			}
		}
		System.out.println("Liga: " + liga);
		porTurnar = true;
		return liga;
	}

	// </editor-fold>

	/**
	 * @return the detalleProyTurn
	 */
	public Object[] getDetalleProyTurn() {
		return detalleProyTurn;
	}

	/**
	 * @param detalleProyTurn
	 *            the detalleProyTurn to set
	 */
	public void setDetalleProyTurn(Object[] detalleProyTurn) {
		this.detalleProyTurn = detalleProyTurn;
	}

	/**
	 * @return the tramSinatec
	 */
	public boolean isTramSinatec() {
		return tramSinatec;
	}

	/**
	 * @param tramSinatec
	 *            the tramSinatec to set
	 */
	public void setTramSinatec(boolean tramSinatec) {
		this.tramSinatec = tramSinatec;
	}

	/**
	 * @return the estudioRiesgo
	 */
	public boolean isEstudioRiesgo() {
		return estudioRiesgo;
	}

	/**
	 * @param estudioRiesgo
	 *            the estudioRiesgo to set
	 */
	public void setEstudioRiesgo(boolean estudioRiesgo) {
		this.estudioRiesgo = estudioRiesgo;
	}

	/**
	 * @return the tipoOfic
	 */
	public String getTipoOfic() {
		return tipoOfic;
	}

	/**
	 * @param tipoOfic
	 *            the tipoOfic to set
	 */
	public void setTipoOfic(String tipoOfic) {
		this.tipoOfic = tipoOfic;
	}

	/**
	 * @return the porTurnarSDPrev
	 */
	public boolean isPorTurnarSDPrev() {
		return porTurnarSDPrev;
	}

	/**
	 * @param porTurnarSDPrev
	 *            the porTurnarSDPrev to set
	 */
	public void setPorTurnarSDPrev(boolean porTurnarSDPrev) {
		this.porTurnarSDPrev = porTurnarSDPrev;
	}

	/**
	 * @return the MuestraOpcTurnadoNo
	 */
	public Boolean getMuestraOpcTurnadoNo() {
		return MuestraOpcTurnadoNo;
	}

	/**
	 * @param MuestraOpcTurnadoNo
	 *            the MuestraOpcTurnadoNo to set
	 */
	public void setMuestraOpcTurnadoNo(Boolean MuestraOpcTurnadoNo) {
		this.MuestraOpcTurnadoNo = MuestraOpcTurnadoNo;
	}

	/**
	 * @return the dePrevenc
	 */
	public Boolean getDePrevenc() {
		return dePrevenc;
	}

	/**
	 * @param dePrevenc
	 *            the dePrevenc to set
	 */
	public void setDePrevenc(Boolean dePrevenc) {
		this.dePrevenc = dePrevenc;
	}

	/**
	 * @return the yaResuelto
	 */
	public boolean isYaResuelto() {
		return yaResuelto;
	}

	/**
	 * @param yaResuelto
	 *            the yaResuelto to set
	 */
	public void setYaResuelto(boolean yaResuelto) {
		this.yaResuelto = yaResuelto;
	}

	/**
	 * @return the tramiteYaTurnado
	 */
	public Boolean getTramiteYaTurnado() {
		return tramiteYaTurnado;
	}

	/**
	 * @param tramiteYaTurnado
	 *            the tramiteYaTurnado to set
	 */
	public void setTramiteYaTurnado(Boolean tramiteYaTurnado) {
		this.tramiteYaTurnado = tramiteYaTurnado;
	}

	public String getComentarioDirectorTurnado() {
		return comentarioDirectorTurnado;
	}

	public void setComentarioDirectorTurnado(String comentarioDirectorTurnado) {
		this.comentarioDirectorTurnado = comentarioDirectorTurnado;
	}

	public String getComentarioSubdirectorTurnado() {
		return comentarioSubdirectorTurnado;
	}

	public void setComentarioSubdirectorTurnado(String comentarioSubdirectorTurnado) {
		this.comentarioSubdirectorTurnado = comentarioSubdirectorTurnado;
	}

	public int getActiveTab() {
		System.out.println(activeTab + "<------------------------ ActiveTab");
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public int getBanderaOpT() {
		return banderaOpT;
	}

	public void setBanderaOpT(int banderaOpT) {
		this.banderaOpT = banderaOpT;
	}

	public int getBanderaNot() {
		return banderaNot;
	}

	public void setBanderaNot(int banderaNot) {
		this.banderaNot = banderaNot;
	}

	public String getSinteGace() {
		sintesisGacetaCompleto();
		return sinteGace;
	}

	public void setSinteGace(String sinteGace) {
		this.sinteGace = sinteGace;
	}

	public String getAnaPre() {
		analisisPreCompleto();
		return anaPre;
	}

	public void setAnaPre(String anaPre) {
		this.anaPre = anaPre;
	}

	public String getValiPag() {
		if (valiPag.equals("dataEmpty.jpg"))
			pagoValidadoCompleto();
		return valiPag;
	}

	public void setValiPag(String valiPag) {
		this.valiPag = valiPag;
	}

	public String getChklst() {
		checkListCompleto();
		return chklst;
	}

	public void setChklst(String chklst) {
		this.chklst = chklst;
	}

	public String getOpTec() {
		return opTec;
	}

	public void setOpTec(String opTec) {
		this.opTec = opTec;
	}
}