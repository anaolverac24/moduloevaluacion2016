/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorIp;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.MenuDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.catalogos.RamaProyecto;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.catalogos.TipoProyecto;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.RepLegalProyecto;
import mx.gob.semarnat.model.dgira_miae.RespTecProyecto;
import mx.gob.semarnat.model.dgira_miae.RespTecProyectoPK;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.view.Constantes;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "ipCap2")
@ViewScoped
public class IpCapitulo2 {
    private Bitacora bitacora;
    private final VisorDao dao = new VisorDao();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private List<Object[]> detalleProy = new ArrayList();
    private Proyecto folioDetalle;
    private Proyecto proyecto;
    private String idArea;
    private Integer idusuario;
    private HttpSession sesion = Util.getSession();
    private String cveProyecto; 
    private final MenuDao MenuDao= new MenuDao();
    
    //---------------capitulo 2
    private SectorProyecto sectorProy;  
    private SubsectorProyecto subsectorProy;
    private RamaProyecto ramaProy;
    private TipoProyecto tipoProy;
    private List<Object[]> edoAfectado = new ArrayList();
    private List<Object[]> todosEdos = new ArrayList();
    // define si se ve el tipo de domicilion exacto, para su llenado
    private Boolean localiz = null;
    // define si se ve el tipo de domicilion NO exacto, para su llenado
    private Boolean localizNo = null;
    // para la seleccion de referencia de tipo de domicilio
    private String tipoDomic;
    private CatVialidad catVialidad;  
    private CatTipoAsen catTipoAsen;
    private List<ArchivosProyecto> anexosProyecto = new ArrayList<ArchivosProyecto>();
    private Boolean vacio;
     private Vexdatosusuario promovente;
     private CatVialidad catVialidadProm;  
    private CatTipoAsen catTipoAsenProm;
    private List<Vexdatosusuariorep> representantesSelect;
    private RepresentanteDataModel representantes;
    private RespTecProyecto respTecProyecto = new RespTecProyecto();
    private String curp;
    private Boolean flag = null;
    private String rtRl;

    public IpCapitulo2() {
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        try {
            pramBita = params.get("bitaNum").toString();

        
            if (pramBita != null) 
            {
                cveProyecto = MenuDao.cveTramite(pramBita);
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().getSessionMap().put("bitacoraProy", pramBita);
                folioDetalle = dao.verificaERA(pramBita);
                if (folioDetalle.getProyectoPK() != null) 
                {
                    folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                    serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
//                    JOptionPane.showMessageDialog(null, "folio:  " + folioNum + "   serial: " + serialNum, "Error", JOptionPane.INFORMATION_MESSAGE);
                    //----proyecto en sesion
                    proyecto = folioDetalle;
                    
                    context.getExternalContext().getSessionMap().put("userFolioProy", folioDetalle.getProyectoPK().getFolioProyecto());
                    context.getExternalContext().getSessionMap().put("userSerialProy", folioDetalle.getProyectoPK().getSerialProyecto());
                    
                    
            //-------------------capitulo 2----------------------------------------------------
                    if (proyecto != null && folioDetalle.getProySupuesto() != null) {

                        sectorProy = dao.sectorProy(proyecto.getNsec());
                        subsectorProy = dao.subSecProy(proyecto.getNsec(), proyecto.getNsub());                            
                        tipoProy = dao.tipoProy(proyecto.getNrama(), proyecto.getNtipo());
                        ramaProy = dao.ramaProy(proyecto.getNsub(), proyecto.getNrama());

                        todosEdos = estadosAfetados(todosEdos, serialNum);
                        tipoDomic = seleccionaDomicilio();
                        catVialidad = dao.tipoVialidad(proyecto.getCveTipoVial());
                        catTipoAsen = dao.tipoAsenta(proyecto.getTipoAsentamientoId());
                            
                        //representantes legales desde BD
                        representantesSelect = new ArrayList();
                        representantes = new RepresentanteDataModel(dao.datosPrepLeg(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())));
                        //carga representantes legales previamente seleccionados
                        for (RepLegalProyecto repl: (List<RepLegalProyecto>) dao.listado_where_comp(RepLegalProyecto.class,
                                        "repLegalProyectoPK", "folioProyecto",
                                        proyecto.getProyectoPK().getFolioProyecto())) {

                            representantesSelect.add(representantes.getRowData((repl.getRfc()
                                    + "/"
                                    + repl.getRepLegalProyectoPK().getFolioProyecto())));
                            if (repl.getMismoEstudio().equals("S")) {
                                curp = repl.getRfc();
                                flag = true;
                            }
                        }
                        //carga responsable tecnico
                        respTecProyecto = (RespTecProyecto) dao.busca(RespTecProyecto.class, new RespTecProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
                        if (respTecProyecto != null) {
                            flag = false;
                            rtRl = "No";
                        } else {
                            respTecProyecto = new RespTecProyecto();
                            rtRl = "Si";
                        }
                        
                        try {
                            promovente = (Vexdatosusuario) dao.
                                    datosPromovente(Integer.
                                            parseInt(proyecto.getProyectoPK().
                                                    getFolioProyecto())).get(0);
                            vacio = false;
                        } catch (IndexOutOfBoundsException e) {
                            promovente = new Vexdatosusuario();
                            vacio = true;
                        }
                        
                        short capituloId = (short) 0;
                        short subcapituloId = (short) 0;
                        short seccionId = (short) 0;
                        short apartadoId = (short) 0; 
                        capituloId = 2;
                        subcapituloId = 1;
                        seccionId = 1;
                        apartadoId = 1;
                        anexosProyecto = dao.getArchivosProyecto(capituloId, subcapituloId, seccionId, apartadoId, folioNum, serialNum);
                    }
                } 

            }
        } catch (Exception e) {
         
           // JOptionPane.showMessageDialog(null,  "tramSinatec:  " + e.getMessage() , "Error", JOptionPane.INFORMATION_MESSAGE);
        }
                    
    }

    
    /**
     * @return the bitacora
     */
    public Bitacora getBitacora() {
        return bitacora;
    }

    /**
     * @param bitacora the bitacora to set
     */
    public void setBitacora(Bitacora bitacora) {
        this.bitacora = bitacora;
    }

    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * @return the detalleProy
     */
    public List<Object[]> getDetalleProy() {
        return detalleProy;
    }

    /**
     * @param detalleProy the detalleProy to set
     */
    public void setDetalleProy(List<Object[]> detalleProy) {
        this.detalleProy = detalleProy;
    }

    /**
     * @return the folioDetalle
     */
    public Proyecto getFolioDetalle() {
        return folioDetalle;
    }

    /**
     * @param folioDetalle the folioDetalle to set
     */
    public void setFolioDetalle(Proyecto folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sectorProy
     */
    public SectorProyecto getSectorProy() {
        return sectorProy;
    }

    /**
     * @param sectorProy the sectorProy to set
     */
    public void setSectorProy(SectorProyecto sectorProy) {
        this.sectorProy = sectorProy;
    }

    /**
     * @return the subsectorProy
     */
    public SubsectorProyecto getSubsectorProy() {
        return subsectorProy;
    }

    /**
     * @param subsectorProy the subsectorProy to set
     */
    public void setSubsectorProy(SubsectorProyecto subsectorProy) {
        this.subsectorProy = subsectorProy;
    }

    /**
     * @return the ramaProy
     */
    public RamaProyecto getRamaProy() {
        return ramaProy;
    }

    /**
     * @param ramaProy the ramaProy to set
     */
    public void setRamaProy(RamaProyecto ramaProy) {
        this.ramaProy = ramaProy;
    }

    /**
     * @return the tipoProy
     */
    public TipoProyecto getTipoProy() {
        return tipoProy;
    }

    /**
     * @param tipoProy the tipoProy to set
     */
    public void setTipoProy(TipoProyecto tipoProy) {
        this.tipoProy = tipoProy;
    }

    /**
     * @return the edoAfectado
     */
    public List<Object[]> getEdoAfectado() {
        return edoAfectado;
    }

    /**
     * @param edoAfectado the edoAfectado to set
     */
    public void setEdoAfectado(List<Object[]> edoAfectado) {
        this.edoAfectado = edoAfectado;
    }

    /**
     * @return the localiz
     */
    public Boolean getLocaliz() {
        return localiz;
    }

    /**
     * @param localiz the localiz to set
     */
    public void setLocaliz(Boolean localiz) {
        this.localiz = localiz;
    }

    /**
     * @return the localizNo
     */
    public Boolean getLocalizNo() {
        return localizNo;
    }

    /**
     * @param localizNo the localizNo to set
     */
    public void setLocalizNo(Boolean localizNo) {
        this.localizNo = localizNo;
    }

    /**
     * @return the tipoDomic
     */
    public String getTipoDomic() {
        return tipoDomic;
    }

    /**
     * @param tipoDomic the tipoDomic to set
     */
    public void setTipoDomic(String tipoDomic) {
        this.tipoDomic = tipoDomic;
    }

    /**
     * @return the catVialidad
     */
    public CatVialidad getCatVialidad() {
        return catVialidad;
    }

    /**
     * @param catVialidad the catVialidad to set
     */
    public void setCatVialidad(CatVialidad catVialidad) {
        this.catVialidad = catVialidad;
    }

    /**
     * @return the catTipoAsen
     */
    public CatTipoAsen getCatTipoAsen() {
        return catTipoAsen;
    }

    /**
     * @param catTipoAsen the catTipoAsen to set
     */
    public void setCatTipoAsen(CatTipoAsen catTipoAsen) {
        this.catTipoAsen = catTipoAsen;
    }

    /**
     * @return the anexosProyecto
     */
    public List<ArchivosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<ArchivosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    /**
     * @return the vacio
     */
    public Boolean getVacio() {
        return vacio;
    }

    /**
     * @param vacio the vacio to set
     */
    public void setVacio(Boolean vacio) {
        this.vacio = vacio;
    }

    /**
     * @return the promovente
     */
    public Vexdatosusuario getPromovente() {
        return promovente;
    }

    /**
     * @param promovente the promovente to set
     */
    public void setPromovente(Vexdatosusuario promovente) {
        this.promovente = promovente;
    }

    /**
     * @return the catVialidadProm
     */
    public CatVialidad getCatVialidadProm() {
        return catVialidadProm;
    }

    /**
     * @param catVialidadProm the catVialidadProm to set
     */
    public void setCatVialidadProm(CatVialidad catVialidadProm) {
        this.catVialidadProm = catVialidadProm;
    }

    /**
     * @return the catTipoAsenProm
     */
    public CatTipoAsen getCatTipoAsenProm() {
        return catTipoAsenProm;
    }

    /**
     * @param catTipoAsenProm the catTipoAsenProm to set
     */
    public void setCatTipoAsenProm(CatTipoAsen catTipoAsenProm) {
        this.catTipoAsenProm = catTipoAsenProm;
    }

    /**
     * @return the representantesSelect
     */
    public List<Vexdatosusuariorep> getRepresentantesSelect() {
        return representantesSelect;
    }

    /**
     * @param representantesSelect the representantesSelect to set
     */
    public void setRepresentantesSelect(List<Vexdatosusuariorep> representantesSelect) {
        this.representantesSelect = representantesSelect;
    }

    /**
     * @return the representantes
     */
    public RepresentanteDataModel getRepresentantes() {
        return representantes;
    }

    /**
     * @param representantes the representantes to set
     */
    public void setRepresentantes(RepresentanteDataModel representantes) {
        this.representantes = representantes;
    }

    /**
     * @return the respTecProyecto
     */
    public RespTecProyecto getRespTecProyecto() {
        return respTecProyecto;
    }

    /**
     * @param respTecProyecto the respTecProyecto to set
     */
    public void setRespTecProyecto(RespTecProyecto respTecProyecto) {
        this.respTecProyecto = respTecProyecto;
    }

    /**
     * @return the curp
     */
    public String getCurp() {
        return curp;
    }

    /**
     * @param curp the curp to set
     */
    public void setCurp(String curp) {
        this.curp = curp;
    }

    /**
     * @return the flag
     */
    public Boolean getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    /**
     * @return the rtRl
     */
    public String getRtRl() {
        return rtRl;
    }

    /**
     * @param rtRl the rtRl to set
     */
    public void setRtRl(String rtRl) {
        this.rtRl = rtRl;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }
    
    public final List<Object[]> estadosAfetados(List<Object[]> lista , Short version) {
//        List<Object[]> p = dao.proySigSum(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        List<Object[]> pObra = dao.mpiosSumObra(getProyecto().getProyectoPK().getFolioProyecto(), version);
        List<Object[]> pPredio = dao.mpiosSumPred(getProyecto().getProyectoPK().getFolioProyecto(), version);

        setEdoAfectado(null);
        //todosEdos = null;
        lista = null;
        String mensaje = "";

        Integer id;
//
////        String aux2;
//
        //---------------si trae obras o predios
        if (!pObra.isEmpty() || !pPredio.isEmpty()) {
            //----------si trae obras Y predios
            if (!pObra.isEmpty() && !pPredio.isEmpty()) {
                id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
                if (id != 0) {
                    //edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);                   
                    edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                }
            }
            //----------si trae obras Y NO predios
            if (!pObra.isEmpty() && pPredio.isEmpty()) {
                id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
                if (id != 0) {
                    //edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);                   
                    edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                    
                }
            }
            //----------si NO trae obras Y SI predios
            if (pObra.isEmpty() && !pPredio.isEmpty()) {
                id = Integer.parseInt(pPredio.get(pPredio.size() - 1)[0].toString());
                if (id != 0) {
                    //edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);                   
                    edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                    
                    //setEdoAfectado(edoAfectado);
                }
            }
        }



        //todosEdos = dao.edoTodosAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        lista = dao.edoTodosAfectado(getProyecto().getProyectoPK().getFolioProyecto(), version);
        
        
        if (pObra.isEmpty() || pPredio.isEmpty()) {
            mensaje = "Debe georrefenciar correctamente su proyecto";
            lista = null;
        }
       
        return lista;
    }
    
    
    
    // Selecciona referencia
    public String seleccionaDomicilio() {
        localiz = false;
        localizNo = false;
        String tipoDom = "";
        if (proyecto.getProyDomEstablecido() != null) {
            //JOptionPane.showMessageDialog(null, "seleccionaDomicilio:  " + proyecto.getProyDomEstablecido(), "Error", JOptionPane.INFORMATION_MESSAGE);

            //Carga Normas Oficiales
            if (proyecto.getProyDomEstablecido().toString().equals("S")) {  //SI tiene domicilio establecido
                localiz = true;
                localizNo = false;
                tipoDom = "Si";
                //JOptionPane.showMessageDialog(null, "localiza:  " + localiz + "  No localiz:  "+ localizNo, "Error", JOptionPane.INFORMATION_MESSAGE);
            }
            if (proyecto.getProyDomEstablecido().toString().equals("N")) {  //NO tiene domicilio establecido
                localizNo = true;
                localiz = false;
                tipoDom = "No";
                //JOptionPane.showMessageDialog(null, "localiza:  " + localiz + "  No localiz:  "+ localizNo, "Error", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        return tipoDom;
    }

    /**
     * @return the todosEdos
     */
    public List<Object[]> getTodosEdos() {
        return todosEdos;
    }

    /**
     * @param todosEdos the todosEdos to set
     */
    public void setTodosEdos(List<Object[]> todosEdos) {
        this.todosEdos = todosEdos;
    }
    
    public class RepresentanteDataModel extends ListDataModel<Vexdatosusuariorep> implements SelectableDataModel<Vexdatosusuariorep> {

        public RepresentanteDataModel() {
        }

        public RepresentanteDataModel(List<Vexdatosusuariorep> list) {
            super(list);
        }

        public Object getRowKey(Vexdatosusuariorep t) {
            String s = t.getVccurp() + "/" + t.getBgtramiteid();
            return s;
        }

        public Vexdatosusuariorep getRowData(String string) {
            String s[];
            s = string.split("/");
            try {
                return (Vexdatosusuariorep) dao.repLegPorCurp(s[0], Integer.parseInt(s[1]));
//            return (Vexdatosusuariorep) dao.datosPrepLeg(Integer.parseInt(string)).get(0);
            } catch (NoResultException e) {
                return new Vexdatosusuariorep();
            }
        }

    }
    
    public String getUrl() throws NoSuchAlgorithmException { 
        String perfil=null;
        String url = Constantes.URL_SIGEIA;
        HttpSession sesion = Util.getSession();
        
        
        if(sesion != null)
        {            
            perfil=sesion.getAttribute("rol").toString();           
        }
        String secureParam = "usuario=" + Constantes.USUARIO_SIGEIA
                + "&password=" + Constantes.PASSWORD_SIGEIA
                + "&noFolio=" + proyecto.getProyectoPK().getFolioProyecto()
                + "&claveProyecto=" + cveProyecto
                + "&version=" + (serialNum + 1)
                + "&tipoTramite=" + Constantes.TRAMITE
                + "&lote=" + proyecto.getProyLote()
                + "&perfil=" + perfil
                + "&estatus=A";
        return url.concat(secureParam);
    }
    
    
    
    public List<Object[]> getProySigSupProy() {

        //Carga preguntas segun el id de la nom
        List<Object[]> proySigSupProy = dao.proySigSupProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        return proySigSupProy;
    }
    
    public List<Object[]> getProySigSupProyPredios() {
        List<Object[]> proySigSupProyPredios = dao.proySigSumPredios(folioNum, serialNum);
        
        Object[] tmp = proySigSupProyPredios.get(0);
        System.out.println("value of superficie predio ::: " + tmp[0]);
        
        return proySigSupProyPredios;
    }
    
    public List<Object[]> getProySigSupProyObras() {
        List<Object[]> proySigSupProyObras = dao.proySigSumObras(folioNum, serialNum);
        return proySigSupProyObras;
    }
    public Double getTotalInv() {
        try{
        return proyecto.getProyInversionRequerida() != null
                && proyecto.getProyMedidasPrevencion() != null
                        ? proyecto.getProyInversionRequerida().doubleValue()
                        + proyecto.getProyMedidasPrevencion().doubleValue() : null;
        }catch(Exception e){
            return new Double("0");
        }
    }

    public Integer getTotalEmpleos() {
        return proyecto.getProyEmpleosPermanentes() != null
                && proyecto.getProyEmpleosTemporales() != null
                        ? proyecto.getProyEmpleosPermanentes()
                        + proyecto.getProyEmpleosTemporales() : null;

    }
    
    public String getVialidad() {
        return ((CatVialidad) dao.buscaCat(CatVialidad.class,
                new Short(promovente.getBgcveTipoVialLk().
                        toString()))).getDescripcion();
    }

    public String getAsentamiento() {
        return ((CatTipoAsen) dao.buscaCat(CatTipoAsen.class,
                new Short(promovente.getBgcveTipoAsenLk().
                        toString()))).getNombre();
    }

}
