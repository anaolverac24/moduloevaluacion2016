/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorIp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.catalogos.RamaProyecto;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.catalogos.TipoProyecto;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.CatEtapa;
import mx.gob.semarnat.model.dgira_miae.CatNorma;
import mx.gob.semarnat.model.dgira_miae.CatParqueIndustrial;
import mx.gob.semarnat.model.dgira_miae.CatPdu;
import mx.gob.semarnat.model.dgira_miae.CatPoet;
import mx.gob.semarnat.model.dgira_miae.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.dgira_miae.EtapaProyecto;
import mx.gob.semarnat.model.dgira_miae.ImpactoProyecto;
import mx.gob.semarnat.model.dgira_miae.ParqueIndustrialProyecto;
import mx.gob.semarnat.model.dgira_miae.PduProyecto;
import mx.gob.semarnat.model.dgira_miae.PoetProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.RespTecProyecto;
import mx.gob.semarnat.model.dgira_miae.SustanciaProyecto;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Paty
 */
@ManagedBean(name = "visor")
@ViewScoped

public class VisorView implements Serializable {
    private static final Logger logger = Logger.getLogger(VisorView.class.getName());

    private Bitacora bitacora;
    private final VisorDao dao = new VisorDao();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private List<Object[]> detalleProy = new ArrayList();

    private Proyecto folioDetalle;
    private Boolean MuestraERA = false;

    private String descSupuesto;
    private String descNorma;
    private CatNorma catNorma;
    private List<Object[]> pregNorma = new ArrayList();
    private Boolean MuestraNormas = false; //mostrar panel de normas
    private Boolean MuestraPaqInd = false; //mostrar panel de parques industriales
    private Boolean MuestraDesUrb = false; //mostrar panel de Desarrollo Urbano

    private Proyecto proyecto;

    private String urlCap1IP;
    private String urlCap2IP;
    private String urlCap3IP;
    private String urlCap4IP;

    //-------supuesto 2
    private CatParqueIndustrial catParqueIndustrial;
    private ParqueIndustrialProyecto parqueIndustrialProyecto;

    //------supuesto 3
    private PduProyecto pduProyecto;
    private PoetProyecto PoetProyecto;
    private CatPdu catPdu;
    private CatPoet catPoet;
    private String OpcPduPoet;
    private String OpcPduPoet2;
    private String justPduPoet;
    private Boolean mustraPoet=false;  //mustraPoet  mustraPdu
    private Boolean mustraPdu=false;

    //-------------otros
    private String capActivo;

    //---------------capitulo 2
    private SectorProyecto sectorProy;  
    private SubsectorProyecto subsectorProy;
    private RamaProyecto ramaProy;
    private TipoProyecto tipoProy;
    private List<Object[]> edoAfectado = new ArrayList();
    private List<Object[]> todosEdos = new ArrayList();
    // define si se ve el tipo de domicilion exacto, para su llenado
    private Boolean localiz = null;
    // define si se ve el tipo de domicilion NO exacto, para su llenado
    private Boolean localizNo = null;
    // para la seleccion de referencia de tipo de domicilio
    private String tipoDomic;
    private CatVialidad catVialidad;  
    private CatTipoAsen catTipoAsen;
    private List<AnexosProyecto> anexosProyecto = new ArrayList<AnexosProyecto>();
    private Boolean vacio;
     private Vexdatosusuario promovente;
     private CatVialidad catVialidadProm;  
    private CatTipoAsen catTipoAsenProm;
    private List<Vexdatosusuariorep> representantesSelect;
    private RepresentanteDataModel representantes;
    private RespTecProyecto respTecProyecto = new RespTecProyecto();
    private String curp;
    private Boolean flag = null;
    private String rtRl;
    //-----------------capitulo 3
    private List<IdenImpAmbHelper> impactoProyectos;
    private List<ImpactoProyecto> impactosBD;
    private IdenImpAmbHelper impactoProyectoSelect;
    private List<AnexosProyecto> anexosProyecto2 = new ArrayList();
    private String anios[];
    private String meses[];
    private Boolean banderas[];
    private Boolean nuevo[];
    private EtapaProyecto preparacionSitio = new EtapaProyecto();
    private EtapaProyecto construccion = new EtapaProyecto();
    private EtapaProyecto operaMantenim = new EtapaProyecto();
    private EtapaProyecto abandono = new EtapaProyecto();
    private List<AnexosProyecto> anexosProyectoEtapa1 = new ArrayList();
    private List<AnexosProyecto> anexosProyectoEtapa2 = new ArrayList();
    private List<AnexosProyecto> anexosProyectoEtapa3 = new ArrayList();
    private List<AnexosProyecto> anexosProyectoEtapa4 = new ArrayList();

    private List<SustanciaHelper> sustanciaProyectos;
    private SustanciaHelper sustanciaSeleccionada;
    private List<CatEtapa> catEtapa;
    private boolean nuevoSust = false;
    private boolean supera = false;
////    private List<CatUnidadMedida> catUnidades;
    private CatUnidadMedida catUnidadMedida;
    private List<AnexosProyecto> anexosProyectoSust = new ArrayList<AnexosProyecto>();

    private List<Object[]> proyectoSustancia = new ArrayList<Object[]>();
    private List<Object[]> proyectoEmiResDes = new ArrayList<Object[]>();

    //-------------------------nuevo
    private List<Object[]> proyImpactos = new ArrayList<Object[]>();
    private List<AnexosProyecto> anexosProyectoImpAmb = new ArrayList<AnexosProyecto>();

    //------------------------------------------------
    private EstudioRiesgoProyecto estudio = new EstudioRiesgoProyecto();
    private List<AnexosProyecto> descSistTransp = new ArrayList();
    private List<AnexosProyecto> baseDiseno = new ArrayList();
    private List<AnexosProyecto> condOper = new ArrayList();
    private List<AnexosProyecto> pruebVerif = new ArrayList();
    private List<AnexosProyecto> procMedCont = new ArrayList();
    private List<AnexosProyecto> analEvalRiesg = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> metodIdentJerq = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> radPotAfect = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> intRiesgo = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> efectAreaInfl = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> recomTecOper = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> sistSeg = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> medPrevent = new ArrayList<AnexosProyecto>();

    private List<AnexosProyecto> conclucERA = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> resumenRA = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> infoTec = new ArrayList<AnexosProyecto>();
    
    private List<AnexosProyecto> planosLocFot = new ArrayList<AnexosProyecto>(); 
    private List<AnexosProyecto> otrosAnex = new ArrayList<AnexosProyecto>();
    
    private boolean tramSinatec = true;
    
    private String idArea;
    private Integer idusuario;
    private HttpSession sesion = Util.getSession();
    private Historial bitaSiNoIntegexp;
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private Boolean muestraPegTurnado = false;
    private Historial bitaNoIntegexp;
    private Boolean muestraPegNoInteg = false;
    private List<Historial> HistorialI00010;
    private Bitacora bitacora2;
    private SinatDgira sinatDgira;
    private List<Historial> HistorialIRA020;
    private List<Historial> HistorialCIS303;
    private List<Historial> HistorialAT0003;

    public VisorView() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
        Integer resp1 = -1;
        Integer resp2 = -1;
            
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        try {
            pramBita = params.get("bitaNum").toString();

            //-------------------capitulo 2
            if (pramBita != null) {
                urlCap1IP = "capitulosIP/capitulo1IP.xhtml?bitaNum=" + pramBita;
                urlCap2IP = "capitulosIP/capitulo2IP.xhtml?bitaNum=" + pramBita;
                urlCap3IP = "capitulosIP/capitulo3IP.xhtml?bitaNum=" + pramBita;
                urlCap4IP = "capitulosIP/capitulo4IP.xhtml?bitaNum=" + pramBita;               
                
                if (sesion.getAttribute("rol").toString().equals("eva")) 
                {
                    bitaSiNoIntegexp = daoBitacora.datosProyHistStatus(pramBita, "AT0002");
                    HistorialI00010 = daoBitacora.numRegHistStatus(pramBita, "I00010");                    
                    HistorialAT0003 = daoBitacora.numRegHistStatus(pramBita, "AT0003");
                    HistorialIRA020 = daoBitacora.numRegHistStatus(pramBita, "IRA020");  
                    HistorialCIS303 = daoBitacora.numRegHistStatus(pramBita, "CIS303");
                    
                    //Si no ha sido integrado el trámite, solo se verifica integración por tramites nuevos sin prevención y con reactivación de prevención
                    if (bitaSiNoIntegexp == null) 
                    { 
                        if(HistorialI00010.size() == 1) // si el tramite viene de envio a evaluador por primera vez
                        {
                            //turnado a Integración del expediente
                            resp1 = cambioSituacion("AT0002", "--",1);                                
                            //turnado a evaluación
                            resp2 = cambioSituacion("AT0003", "--",2);
                            if (resp1 == 1 && resp2 == 1) {
                                System.out.println("tramite turnado a evaluación, con integración de expediente");
                            } else {

                            }
                        }
                    } 
                    //SI el trámite ya ha sido integrado
                    else 
                    {                      
                        if(HistorialI00010.size() == 2 && HistorialAT0003.size() == 1 && (HistorialIRA020.size()==1 ||  HistorialCIS303.size()==1)) // si el tramite viene de reactivación de info adicional solo se turna SIn integración de expediente, por que ya por su segundo turnado a evaluación
                        {  
                            //si va a evaluación sin integración de expediente
                            //turnado a evaluación
                            resp2 = cambioSituacion("AT0003", "--",1);
                            if (resp2 == 1) {
                                System.out.println("tramite turnado a evaluación, SIN integración de expediente, que viene de Reactivación de Info adic");
                            } else {

                            }
                        }
                        
                    }

                    System.out.println("muestraPegTurnado: " + muestraPegTurnado);
                }
                
                
                
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().getSessionMap().put("bitacoraProy", pramBita);
                folioDetalle = dao.verificaERA2(pramBita);
                if (folioDetalle.getProyectoPK() != null) 
                {                                      
                    tramSinatec = true;
                    folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                    serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
//                    JOptionPane.showMessageDialog(null, "folio:  " + folioNum + "   serial: " + serialNum, "Error", JOptionPane.INFORMATION_MESSAGE);
                    //----proyecto en sesion
                    proyecto = dao.cargaProyecto(folioNum, serialNum);
                    
                    context.getExternalContext().getSessionMap().put("userFolioProy", folioDetalle.getProyectoPK().getFolioProyecto());
                    context.getExternalContext().getSessionMap().put("userSerialProy", folioDetalle.getProyectoPK().getSerialProyecto());

            //---------------capitulo 1-------------------------------------------------
                    //JOptionPane.showMessageDialog(null,  "supuesto: " + folioDetalle.getProySupuesto() + "   normaId: " + folioDetalle.getProyNormaId(), "Error", JOptionPane.INFORMATION_MESSAGE);
                    if(folioDetalle.getProySupuesto() != null && folioDetalle.getProySupuesto().equals('1') && folioDetalle.getProyNormaId() == 129)
                    { 

                        if(folioDetalle.getProyNormaId() == 129)
                        {
                            MuestraERA = true; 
                        }
                        else
                        { MuestraERA = false; }
                    }


                } else {
                    tramSinatec = false;
                }

            }
        } catch (Exception e) {
            tramSinatec = false;
           // JOptionPane.showMessageDialog(null,  "tramSinatec:  " + e.getMessage() , "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        
        //JOptionPane.showMessageDialog(null,  "tramSinatec:  " + tramSinatec , "Error", JOptionPane.INFORMATION_MESSAGE);
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }
    
    
    /**
     * @return the bitacora
     */
    public Bitacora getBitacora() {
        return bitacora;
    }

    /**
     * @param bitacora the bitacora to set
     */
    public void setBitacora(Bitacora bitacora) {
        this.bitacora = bitacora;
    }
    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the detalleProy
     */
    public List<Object[]> getDetalleProy() {
        return detalleProy;
    }

    /**
     * @param detalleProy the detalleProy to set
     */
    public void setDetalleProy(List<Object[]> detalleProy) {
        this.detalleProy = detalleProy;
    }

    /**
     * @return the folioDetalle
     */
    public Proyecto getFolioDetalle() {
        return folioDetalle;
    }

    /**
     * @param folioDetalle the folioDetalle to set
     */
    public void setFolioDetalle(Proyecto folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    /**
     * @return the MuestraERA
     */
    public Boolean getMuestraERA() {
        return MuestraERA;
    }

    /**
     * @param MuestraERA the MuestraERA to set
     */
    public void setMuestraERA(Boolean MuestraERA) {
        this.MuestraERA = MuestraERA;
    }

    /**
     * @return the descSupuesto
     */
    public String getDescSupuesto() {
        return descSupuesto;
    }

    /**
     * @param descSupuesto the descSupuesto to set
     */
    public void setDescSupuesto(String descSupuesto) {
        this.descSupuesto = descSupuesto;
    }

    /**
     * @return the descNorma
     */
    public String getDescNorma() {
        return descNorma;
    }

    /**
     * @param descNorma the descNorma to set
     */
    public void setDescNorma(String descNorma) {
        this.descNorma = descNorma;
    }

    /**
     * @return the pregNorma
     */
    public List<Object[]> getPregNorma() {
        return pregNorma;
    }

    /**
     * @param pregNorma the pregNorma to set
     */
    public void setPregNorma(List<Object[]> pregNorma) {
        this.pregNorma = pregNorma;
    }

    /**
     * @return the catNorma
     */
    public CatNorma getCatNorma() {
        return catNorma;
    }

    /**
     * @param catNorma the catNorma to set
     */
    public void setCatNorma(CatNorma catNorma) {
        this.catNorma = catNorma;
    }

    /**
     * @return the MuestraNormas
     */
    public Boolean getMuestraNormas() {
        return MuestraNormas;
    }

    /**
     * @param MuestraNormas the MuestraNormas to set
     */
    public void setMuestraNormas(Boolean MuestraNormas) {
        this.MuestraNormas = MuestraNormas;
    }

    /**
     * @return the MuestraPaqInd
     */
    public Boolean getMuestraPaqInd() {
        return MuestraPaqInd;
    }

    /**
     * @param MuestraPaqInd the MuestraPaqInd to set
     */
    public void setMuestraPaqInd(Boolean MuestraPaqInd) {
        this.MuestraPaqInd = MuestraPaqInd;
    }

    /**
     * @return the MuestraDesUrb
     */
    public Boolean getMuestraDesUrb() {
        return MuestraDesUrb;
    }

    /**
     * @param MuestraDesUrb the MuestraDesUrb to set
     */
    public void setMuestraDesUrb(Boolean MuestraDesUrb) {
        this.MuestraDesUrb = MuestraDesUrb;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the catParqueIndustrial
     */
    public CatParqueIndustrial getCatParqueIndustrial() {
        return catParqueIndustrial;
    }

    /**
     * @param catParqueIndustrial the catParqueIndustrial to set
     */
    public void setCatParqueIndustrial(CatParqueIndustrial catParqueIndustrial) {
        this.catParqueIndustrial = catParqueIndustrial;
    }

    /**
     * @return the parqueIndustrialProyecto
     */
    public ParqueIndustrialProyecto getParqueIndustrialProyecto() {
        return parqueIndustrialProyecto;
    }

    /**
     * @param parqueIndustrialProyecto the parqueIndustrialProyecto to set
     */
    public void setParqueIndustrialProyecto(ParqueIndustrialProyecto parqueIndustrialProyecto) {
        this.parqueIndustrialProyecto = parqueIndustrialProyecto;
    }

    /**
     * @return the pduProyecto
     */
    public PduProyecto getPduProyecto() {
        return pduProyecto;
    }

    /**
     * @param pduProyecto the pduProyecto to set
     */
    public void setPduProyecto(PduProyecto pduProyecto) {
        this.pduProyecto = pduProyecto;
    }

    /**
     * @return the PoetProyecto
     */
    public PoetProyecto getPoetProyecto() {
        return PoetProyecto;
    }

    /**
     * @param PoetProyecto the PoetProyecto to set
     */
    public void setPoetProyecto(PoetProyecto PoetProyecto) {
        this.PoetProyecto = PoetProyecto;
    }

    /**
     * @return the catPdu
     */
    public CatPdu getCatPdu() {
        return catPdu;
    }

    /**
     * @param catPdu the catPdu to set
     */
    public void setCatPdu(CatPdu catPdu) {
        this.catPdu = catPdu;
    }

    /**
     * @return the catPoet
     */
    public CatPoet getCatPoet() {
        return catPoet;
    }

    /**
     * @param catPoet the catPoet to set
     */
    public void setCatPoet(CatPoet catPoet) {
        this.catPoet = catPoet;
    }

    /**
     * @return the OpcPduPoet
     */
    public String getOpcPduPoet() {
        return OpcPduPoet;
    }

    /**
     * @param OpcPduPoet the OpcPduPoet to set
     */
    public void setOpcPduPoet(String OpcPduPoet) {
        this.OpcPduPoet = OpcPduPoet;
    }

    /**
     * @return the OpcPduPoet2
     */
    public String getOpcPduPoet2() {
        return OpcPduPoet2;
    }

    /**
     * @param OpcPduPoet2 the OpcPduPoet2 to set
     */
    public void setOpcPduPoet2(String OpcPduPoet2) {
        this.OpcPduPoet2 = OpcPduPoet2;
    }

    /**
     * @return the justPduPoet
     */
    public String getJustPduPoet() {
        return justPduPoet;
    }

    /**
     * @param justPduPoet the justPduPoet to set
     */
    public void setJustPduPoet(String justPduPoet) {
        this.justPduPoet = justPduPoet;
    }

    /**
     * @return the urlCap1IP
     */
    public String getUrlCap1IP() {
        return urlCap1IP;
    }

    /**
     * @param urlCap1IP the urlCap1IP to set
     */
    public void setUrlCap1IP(String urlCap1IP) {
        this.urlCap1IP = urlCap1IP;
    }

    /**
     * @return the urlCap2IP
     */
    public String getUrlCap2IP() {
        return urlCap2IP;
    }

    /**
     * @param urlCap2IP the urlCap2IP to set
     */
    public void setUrlCap2IP(String urlCap2IP) {
        this.urlCap2IP = urlCap2IP;
    }

    /**
     * @return the urlCap3IP
     */
    public String getUrlCap3IP() {
        return urlCap3IP;
    }

    /**
     * @param urlCap3IP the urlCap3IP to set
     */
    public void setUrlCap3IP(String urlCap3IP) {
        this.urlCap3IP = urlCap3IP;
    }

    /**
     * @return the urlCap4IP
     */
    public String getUrlCap4IP() {
        return urlCap4IP;
    }

    /**
     * @param urlCap4IP the urlCap4IP to set
     */
    public void setUrlCap4IP(String urlCap4IP) {
        this.urlCap4IP = urlCap4IP;
    }

    /**
     * @return the capActivo
     */
    public String getCapActivo() {
        return capActivo;
    }

    /**
     * @param capActivo the capActivo to set
     */
    public void setCapActivo(String capActivo) {
        this.capActivo = capActivo;
    }

    public void cap1() {
        capActivo = "cap1";
//        JOptionPane.showMessageDialog(null,  "cap 222 activo:  " + capActivo , "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    public void cap2() {
        capActivo = "cap2";
//        JOptionPane.showMessageDialog(null,  "cap 222 activo:  " + capActivo , "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    public void cap3() {
        capActivo = "cap3";
//        JOptionPane.showMessageDialog(null,  "cap 222 activo:  " + capActivo , "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    public void cap4() {
        capActivo = "cap4";
//        JOptionPane.showMessageDialog(null,  "cap 222 activo:  " + capActivo , "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * @return the sectorProy
     */
    public SectorProyecto getSectorProy() {
        return sectorProy;
    }

    /**
     * @param sectorProy the sectorProy to set
     */
    public void setSectorProy(SectorProyecto sectorProy) {
        this.sectorProy = sectorProy;
    }

    /**
     * @return the subsectorProy
     */
    public SubsectorProyecto getSubsectorProy() {
        return subsectorProy;
    }

    /**
     * @param subsectorProy the subsectorProy to set
     */
    public void setSubsectorProy(SubsectorProyecto subsectorProy) {
        this.subsectorProy = subsectorProy;
    }

    /**
     * @return the ramaProy
     */
    public RamaProyecto getRamaProy() {
        return ramaProy;
    }

    /**
     * @param ramaProy the ramaProy to set
     */
    public void setRamaProy(RamaProyecto ramaProy) {
        this.ramaProy = ramaProy;
    }

    /**
     * @return the tipoProy
     */
    public TipoProyecto getTipoProy() {
        return tipoProy;
    }

    /**
     * @param tipoProy the tipoProy to set
     */
    public void setTipoProy(TipoProyecto tipoProy) {
        this.tipoProy = tipoProy;
    }
    /**
     * @return the edoAfectado
     */
    public List<Object[]> getEdoAfectado() {
        return edoAfectado;
    }

    /**
     * @param edoAfectado the edoAfectado to set
     */
    public void setEdoAfectado(List<Object[]> edoAfectado) {
        this.edoAfectado = edoAfectado;
    }

    /**
     * @return the todosEdos
     */
    public List<Object[]> getTodosEdos() {
        return todosEdos;
    }

    /**
     * @param todosEdos the todosEdos to set
     */
    public void setTodosEdos(List<Object[]> todosEdos) {
        this.todosEdos = todosEdos;
    }


    /**
     * @return the localiz
     */
    public Boolean getLocaliz() {
        return localiz;
    }

    /**
     * @param localiz the localiz to set
     */
    public void setLocaliz(Boolean localiz) {
        this.localiz = localiz;
    }

    /**
     * @return the localizNo
     */
    public Boolean getLocalizNo() {
        return localizNo;
    }

    /**
     * @param localizNo the localizNo to set
     */
    public void setLocalizNo(Boolean localizNo) {
        this.localizNo = localizNo;
    }

    

    /**
     * @return the tipoDomic
     */
    public String getTipoDomic() {
        return tipoDomic;
    }

    /**
     * @param tipoDomic the tipoDomic to set
     */
    public void setTipoDomic(String tipoDomic) {
        this.tipoDomic = tipoDomic;
    }

    /**
     * @return the catVialidad
     */
    public CatVialidad getCatVialidad() {
        return catVialidad;
    }

    /**
     * @param catVialidad the catVialidad to set
     */
    public void setCatVialidad(CatVialidad catVialidad) {
        this.catVialidad = catVialidad;
    }

    /**
     * @return the catTipoAsen
     */
    public CatTipoAsen getCatTipoAsen() {
        return catTipoAsen;
    }

    /**
     * @param catTipoAsen the catTipoAsen to set
     */
    public void setCatTipoAsen(CatTipoAsen catTipoAsen) {
        this.catTipoAsen = catTipoAsen;
    }
    /**
     * @return the anexosProyecto
     */
    public List<AnexosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<AnexosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    

    /**
     * @return the vacio
     */
    public Boolean getVacio() {
        return vacio;
    }

    /**
     * @param vacio the vacio to set
     */
    public void setVacio(Boolean vacio) {
        this.vacio = vacio;
    }

    /**
     * @return the promovente
     */
    public Vexdatosusuario getPromovente() {
        return promovente;
    }

    /**
     * @param promovente the promovente to set
     */
    public void setPromovente(Vexdatosusuario promovente) {
        this.promovente = promovente;
    }

   

    

    /**
     * @return the representantesSelect
     */
    public List<Vexdatosusuariorep> getRepresentantesSelect() {
        return representantesSelect;
    }

    /**
     * @param representantesSelect the representantesSelect to set
     */
    public void setRepresentantesSelect(List<Vexdatosusuariorep> representantesSelect) {
        this.representantesSelect = representantesSelect;
    }
    /**
     * @return the curp
     */
    public String getCurp() {
        return curp;
    }

    /**
     * @param curp the curp to set
     */
    public void setCurp(String curp) {
        this.curp = curp;
    }

    /**
     * @return the flag
     */
    public Boolean getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    /**
     * @return the representantes
     */
////    public RepresentanteDataModel getRepresentantes() {
////        return representantes;
////    }
////
////    /**
////     * @param representantes the representantes to set
////     */
////    public void setRepresentantes(RepresentanteDataModel representantes) {
////        this.representantes = representantes;
////    }
    /**
     * @return the rtRl
     */
    public String getRtRl() {
        return rtRl;
    }

    /**
     * @param rtRl the rtRl to set
     */
    public void setRtRl(String rtRl) {
        this.rtRl = rtRl;
    }

    /**
     * @return the respTecProyecto
     */
    public RespTecProyecto getRespTecProyecto() {
        return respTecProyecto;
    }

    /**
     * @param respTecProyecto the respTecProyecto to set
     */
    public void setRespTecProyecto(RespTecProyecto respTecProyecto) {
        this.respTecProyecto = respTecProyecto;
    }

    /**
     * @return the anexosProyecto2
     */
    public List<AnexosProyecto> getAnexosProyecto2() {
        return anexosProyecto2;
    }

    /**
     * @param anexosProyecto2 the anexosProyecto2 to set
     */
    public void setAnexosProyecto2(List<AnexosProyecto> anexosProyecto2) {
        this.anexosProyecto2 = anexosProyecto2;
    }

    /**
     * @return the anios
     */
    public String[] getAnios() {
        return anios;
    }

    /**
     * @param anios the anios to set
     */
    public void setAnios(String[] anios) {
        this.anios = anios;
    }

    /**
     * @return the meses
     */
    public String[] getMeses() {
        return meses;
    }

    /**
     * @param meses the meses to set
     */
    public void setMeses(String[] meses) {
        this.meses = meses;
    }

    /**
     * @return the banderas
     */
    public Boolean[] getBanderas() {
        return banderas;
    }

    /**
     * @param banderas the banderas to set
     */
    public void setBanderas(Boolean[] banderas) {
        this.banderas = banderas;
    }

    /**
     * @return the nuevo
     */
    public Boolean[] getNuevo() {
        return nuevo;
    }

    /**
     * @param nuevo the nuevo to set
     */
    public void setNuevo(Boolean[] nuevo) {
        this.nuevo = nuevo;
    }

    /**
     * @return the preparacionSitio
     */
    public EtapaProyecto getPreparacionSitio() {
        return preparacionSitio;
    }

    /**
     * @param preparacionSitio the preparacionSitio to set
     */
    public void setPreparacionSitio(EtapaProyecto preparacionSitio) {
        this.preparacionSitio = preparacionSitio;
    }

    /**
     * @return the construccion
     */
    public EtapaProyecto getConstruccion() {
        return construccion;
    }

    /**
     * @param construccion the construccion to set
     */
    public void setConstruccion(EtapaProyecto construccion) {
        this.construccion = construccion;
    }

    /**
     * @return the operaMantenim
     */
    public EtapaProyecto getOperaMantenim() {
        return operaMantenim;
    }

    /**
     * @param operaMantenim the operaMantenim to set
     */
    public void setOperaMantenim(EtapaProyecto operaMantenim) {
        this.operaMantenim = operaMantenim;
    }

    /**
     * @return the abandono
     */
    public EtapaProyecto getAbandono() {
        return abandono;
    }

    /**
     * @param abandono the abandono to set
     */
    public void setAbandono(EtapaProyecto abandono) {
        this.abandono = abandono;
    }

    /**
     * @return the anexosProyectoEtapa1
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa1() {
        return anexosProyectoEtapa1;
    }

    /**
     * @param anexosProyectoEtapa1 the anexosProyectoEtapa1 to set
     */
    public void setAnexosProyectoEtapa1(List<AnexosProyecto> anexosProyectoEtapa1) {
        this.anexosProyectoEtapa1 = anexosProyectoEtapa1;
    }

    /**
     * @return the anexosProyectoEtapa2
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa2() {
        return anexosProyectoEtapa2;
    }

    /**
     * @param anexosProyectoEtapa2 the anexosProyectoEtapa2 to set
     */
    public void setAnexosProyectoEtapa2(List<AnexosProyecto> anexosProyectoEtapa2) {
        this.anexosProyectoEtapa2 = anexosProyectoEtapa2;
    }

    /**
     * @return the anexosProyectoEtapa3
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa3() {
        return anexosProyectoEtapa3;
    }

    /**
     * @param anexosProyectoEtapa3 the anexosProyectoEtapa3 to set
     */
    public void setAnexosProyectoEtapa3(List<AnexosProyecto> anexosProyectoEtapa3) {
        this.anexosProyectoEtapa3 = anexosProyectoEtapa3;
    }

    /**
     * @return the anexosProyectoEtapa4
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa4() {
        return anexosProyectoEtapa4;
    }

    /**
     * @param anexosProyectoEtapa4 the anexosProyectoEtapa4 to set
     */
    public void setAnexosProyectoEtapa4(List<AnexosProyecto> anexosProyectoEtapa4) {
        this.anexosProyectoEtapa4 = anexosProyectoEtapa4;
    }

    /**
     * @return the sustanciaProyectos
     */
    public List<SustanciaHelper> getSustanciaProyectos() {
        return sustanciaProyectos;
    }

    /**
     * @param sustanciaProyectos the sustanciaProyectos to set
     */
    public void setSustanciaProyectos(List<SustanciaHelper> sustanciaProyectos) {
        this.sustanciaProyectos = sustanciaProyectos;
    }

    /**
     * @return the sustanciaSeleccionada
     */
    public SustanciaHelper getSustanciaSeleccionada() {
        return sustanciaSeleccionada;
    }

    /**
     * @param sustanciaSeleccionada the sustanciaSeleccionada to set
     */
    public void setSustanciaSeleccionada(SustanciaHelper sustanciaSeleccionada) {
        this.sustanciaSeleccionada = sustanciaSeleccionada;
    }

    /**
     * @return the catEtapa
     */
    public List<CatEtapa> getCatEtapa() {
        return catEtapa;
    }

    /**
     * @param catEtapa the catEtapa to set
     */
    public void setCatEtapa(List<CatEtapa> catEtapa) {
        this.catEtapa = catEtapa;
    }

    /**
     * @return the nuevoSust
     */
    public boolean isNuevoSust() {
        return nuevoSust;
    }

    /**
     * @param nuevoSust the nuevoSust to set
     */
    public void setNuevoSust(boolean nuevoSust) {
        this.nuevoSust = nuevoSust;
    }

    /**
     * @return the supera
     */
    public boolean isSupera() {
        return supera;
    }

    /**
     * @param supera the supera to set
     */
    public void setSupera(boolean supera) {
        this.supera = supera;
    }

    /**
     * @return the catUnidades
     */
////    public List<CatUnidadMedida> getCatUnidades() {
////        return catUnidades;
////    }
////
////    /**
////     * @param catUnidades the catUnidades to set
////     */
////    public void setCatUnidades(List<CatUnidadMedida> catUnidades) {
////        this.catUnidades = catUnidades;
////    }
    /**
     * @return the anexosProyectoSust
     */
    public List<AnexosProyecto> getAnexosProyectoSust() {
        return anexosProyectoSust;
    }

    /**
     * @param anexosProyectoSust the anexosProyectoSust to set
     */
    public void setAnexosProyectoSust(List<AnexosProyecto> anexosProyectoSust) {
        this.anexosProyectoSust = anexosProyectoSust;
    }

    /**
     * @return the catUnidadMedida
     */
    public CatUnidadMedida getCatUnidadMedida() {
        return catUnidadMedida;
    }

    /**
     * @param catUnidadMedida the catUnidadMedida to set
     */
    public void setCatUnidadMedida(CatUnidadMedida catUnidadMedida) {
        this.catUnidadMedida = catUnidadMedida;
    }
    /**
     * @return the proyectoSustancia
     */
    public List<Object[]> getProyectoSustancia() {
        return proyectoSustancia;
    }

    /**
     * @param proyectoSustancia the proyectoSustancia to set
     */
    public void setProyectoSustancia(List<Object[]> proyectoSustancia) {
        this.proyectoSustancia = proyectoSustancia;
    }

    /**
     * @return the proyectoEmiResDes
     */
    public List<Object[]> getProyectoEmiResDes() {
        return proyectoEmiResDes;
    }

    /**
     * @param proyectoEmiResDes the proyectoEmiResDes to set
     */
    public void setProyectoEmiResDes(List<Object[]> proyectoEmiResDes) {
        this.proyectoEmiResDes = proyectoEmiResDes;
    }

    /**
     * @return the proyImpactos
     */
    public List<Object[]> getProyImpactos() {
        return proyImpactos;
    }

    /**
     * @param proyImpactos the proyImpactos to set
     */
    public void setProyImpactos(List<Object[]> proyImpactos) {
        this.proyImpactos = proyImpactos;
    }

    /**
     * @return the anexosProyectoImpAmb
     */
    public List<AnexosProyecto> getAnexosProyectoImpAmb() {
        return anexosProyectoImpAmb;
    }

    /**
     * @param anexosProyectoImpAmb the anexosProyectoImpAmb to set
     */
    public void setAnexosProyectoImpAmb(List<AnexosProyecto> anexosProyectoImpAmb) {
        this.anexosProyectoImpAmb = anexosProyectoImpAmb;
    }

    /**
     * @return the estudio
     */
    public EstudioRiesgoProyecto getEstudio() {
        return estudio;
    }

    /**
     * @param estudio the estudio to set
     */
    public void setEstudio(EstudioRiesgoProyecto estudio) {
        this.estudio = estudio;
    }

    /**
     * @return the descSistTransp
     */
    public List<AnexosProyecto> getDescSistTransp() {
        return descSistTransp;
    }

    /**
     * @param descSistTransp the descSistTransp to set
     */
    public void setDescSistTransp(List<AnexosProyecto> descSistTransp) {
        this.descSistTransp = descSistTransp;
    }

    /**
     * @return the baseDiseno
     */
    public List<AnexosProyecto> getBaseDiseno() {
        return baseDiseno;
    }

    /**
     * @param baseDiseno the baseDiseno to set
     */
    public void setBaseDiseno(List<AnexosProyecto> baseDiseno) {
        this.baseDiseno = baseDiseno;
    }

    /**
     * @return the condOper
     */
    public List<AnexosProyecto> getCondOper() {
        return condOper;
    }

    /**
     * @param condOper the condOper to set
     */
    public void setCondOper(List<AnexosProyecto> condOper) {
        this.condOper = condOper;
    }

    /**
     * @return the pruebVerif
     */
    public List<AnexosProyecto> getPruebVerif() {
        return pruebVerif;
    }

    /**
     * @param pruebVerif the pruebVerif to set
     */
    public void setPruebVerif(List<AnexosProyecto> pruebVerif) {
        this.pruebVerif = pruebVerif;
    }

    /**
     * @return the procMedCont
     */
    public List<AnexosProyecto> getProcMedCont() {
        return procMedCont;
    }

    /**
     * @param procMedCont the procMedCont to set
     */
    public void setProcMedCont(List<AnexosProyecto> procMedCont) {
        this.procMedCont = procMedCont;
    }

    /**
     * @return the analEvalRiesg
     */
    public List<AnexosProyecto> getAnalEvalRiesg() {
        return analEvalRiesg;
    }

    /**
     * @param analEvalRiesg the analEvalRiesg to set
     */
    public void setAnalEvalRiesg(List<AnexosProyecto> analEvalRiesg) {
        this.analEvalRiesg = analEvalRiesg;
    }

    /**
     * @return the metodIdentJerq
     */
    public List<AnexosProyecto> getMetodIdentJerq() {
        return metodIdentJerq;
    }

    /**
     * @param metodIdentJerq the metodIdentJerq to set
     */
    public void setMetodIdentJerq(List<AnexosProyecto> metodIdentJerq) {
        this.metodIdentJerq = metodIdentJerq;
    }

    /**
     * @return the radPotAfect
     */
    public List<AnexosProyecto> getRadPotAfect() {
        return radPotAfect;
    }

    /**
     * @param radPotAfect the radPotAfect to set
     */
    public void setRadPotAfect(List<AnexosProyecto> radPotAfect) {
        this.radPotAfect = radPotAfect;
    }

    /**
     * @return the intRiesgo
     */
    public List<AnexosProyecto> getIntRiesgo() {
        return intRiesgo;
    }

    /**
     * @param intRiesgo the intRiesgo to set
     */
    public void setIntRiesgo(List<AnexosProyecto> intRiesgo) {
        this.intRiesgo = intRiesgo;
    }

    /**
     * @return the efectAreaInfl
     */
    public List<AnexosProyecto> getEfectAreaInfl() {
        return efectAreaInfl;
    }

    /**
     * @param efectAreaInfl the efectAreaInfl to set
     */
    public void setEfectAreaInfl(List<AnexosProyecto> efectAreaInfl) {
        this.efectAreaInfl = efectAreaInfl;
    }

    /**
     * @return the recomTecOper
     */
    public List<AnexosProyecto> getRecomTecOper() {
        return recomTecOper;
    }

    /**
     * @param recomTecOper the recomTecOper to set
     */
    public void setRecomTecOper(List<AnexosProyecto> recomTecOper) {
        this.recomTecOper = recomTecOper;
    }

    /**
     * @return the sistSeg
     */
    public List<AnexosProyecto> getSistSeg() {
        return sistSeg;
    }

    /**
     * @param sistSeg the sistSeg to set
     */
    public void setSistSeg(List<AnexosProyecto> sistSeg) {
        this.sistSeg = sistSeg;
    }

    /**
     * @return the medPrevent
     */
    public List<AnexosProyecto> getMedPrevent() {
        return medPrevent;
    }

    /**
     * @param medPrevent the medPrevent to set
     */
    public void setMedPrevent(List<AnexosProyecto> medPrevent) {
        this.medPrevent = medPrevent;
    }

    /**
     * @return the conclucERA
     */
    public List<AnexosProyecto> getConclucERA() {
        return conclucERA;
    }

    /**
     * @param conclucERA the conclucERA to set
     */
    public void setConclucERA(List<AnexosProyecto> conclucERA) {
        this.conclucERA = conclucERA;
    }

    /**
     * @return the resumenRA
     */
    public List<AnexosProyecto> getResumenRA() {
        return resumenRA;
    }

    /**
     * @param resumenRA the resumenRA to set
     */
    public void setResumenRA(List<AnexosProyecto> resumenRA) {
        this.resumenRA = resumenRA;
    }

    /**
     * @return the infoTec
     */
    public List<AnexosProyecto> getInfoTec() {
        return infoTec;
    }

    /**
     * @param infoTec the infoTec to set
     */
    public void setInfoTec(List<AnexosProyecto> infoTec) {
        this.infoTec = infoTec;
    }

////    public class RepresentanteDataModel extends ListDataModel<Vexdatosusuariorep> implements SelectableDataModel<Vexdatosusuariorep> {
////
////        public RepresentanteDataModel() {
////        }
////
////        public RepresentanteDataModel(List<Vexdatosusuariorep> list) {
////            super(list);
////        }
////
////        public Object getRowKey(Vexdatosusuariorep t) {
////            String s = t.getVccurp() + "/" + t.getBgtramiteid();
////            return s;
////        }
////
////        public Vexdatosusuariorep getRowData(String string) {
////            String s[];
////            s = string.split("/");
////            try {
////                return (Vexdatosusuariorep) dao.repLegPorCurp(s[0], Integer.parseInt(s[1]));
//////            return (Vexdatosusuariorep) dao.datosPrepLeg(Integer.parseInt(string)).get(0);
////            } catch (NoResultException e) {
////                return new Vexdatosusuariorep();
////            }
////        }
////
////    }
    private void cargaLista() {
        setNuevo(new Boolean[]{true, true, true, true});
        List<EtapaProyecto> etapa;
        if (proyecto.getProyTiempoVidaAnios() != null && proyecto.getProyTiempoVidaMeses() != null) {
            anios[0] = proyecto.getProyTiempoVidaAnios().toString();
            meses[0] = proyecto.getProyTiempoVidaMeses().toString();
        }

        try {
            etapa = (List<EtapaProyecto>) dao.listado_where_comp(EtapaProyecto.class, "etapaProyectoPK", "folioProyecto", proyecto.getProyectoPK().getFolioProyecto());

            for (EtapaProyecto e : etapa) {
                getNuevo()[(int) e.getCatEtapa().getEtapaId() - 1] = false;
                anios[(int) e.getCatEtapa().getEtapaId()] = e.getAnios() + "";
                meses[(int) e.getCatEtapa().getEtapaId()] = e.getMeses() + "";
                banderas[(int) e.getCatEtapa().getEtapaId() - 1] = e.getAnios() > 0 || e.getMeses() > 0;
                switch ((int) e.getCatEtapa().getEtapaId()) {
                    case 1:
                        setPreparacionSitio(e);
                        break;
                    case 2:
                        setConstruccion(e);
                        break;
                    case 3:
                        setOperaMantenim(e);
                        break;
                    case 4:
                        setAbandono(e);
                        break;
                }
            }
        } catch (NullPointerException e) {
        } catch (Exception e) {
        }
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the planosLocFot
     */
    public List<AnexosProyecto> getPlanosLocFot() {
        return planosLocFot;
    }

    /**
     * @param planosLocFot the planosLocFot to set
     */
    public void setPlanosLocFot(List<AnexosProyecto> planosLocFot) {
        this.planosLocFot = planosLocFot;
    }

    /**
     * @return the otrosAnex
     */
    public List<AnexosProyecto> getOtrosAnex() {
        return otrosAnex;
    }

    /**
     * @param otrosAnex the otrosAnex to set
     */
    public void setOtrosAnex(List<AnexosProyecto> otrosAnex) {
        this.otrosAnex = otrosAnex;
    }

    /**
     * @return the representantes
     */
    public RepresentanteDataModel getRepresentantes() {
        return representantes;
    }

    /**
     * @param representantes the representantes to set
     */
    public void setRepresentantes(RepresentanteDataModel representantes) {
        this.representantes = representantes;
    }

    /**
     * @return the impactoProyectos
     */
    public List<IdenImpAmbHelper> getImpactoProyectos() {
        return impactoProyectos;
    }

    /**
     * @param impactoProyectos the impactoProyectos to set
     */
    public void setImpactoProyectos(List<IdenImpAmbHelper> impactoProyectos) {
        this.impactoProyectos = impactoProyectos;
    }

    /**
     * @return the impactosBD
     */
    public List<ImpactoProyecto> getImpactosBD() {
        return impactosBD;
    }

    /**
     * @param impactosBD the impactosBD to set
     */
    public void setImpactosBD(List<ImpactoProyecto> impactosBD) {
        this.impactosBD = impactosBD;
    }

    /**
     * @return the impactoProyectoSelect
     */
    public IdenImpAmbHelper getImpactoProyectoSelect() {
        return impactoProyectoSelect;
    }

    /**
     * @param impactoProyectoSelect the impactoProyectoSelect to set
     */
    public void setImpactoProyectoSelect(IdenImpAmbHelper impactoProyectoSelect) {
        this.impactoProyectoSelect = impactoProyectoSelect;
    }

    /**
     * @return the mustraPoet
     */
    public Boolean getMustraPoet() {
        return mustraPoet;
    }

    /**
     * @param mustraPoet the mustraPoet to set
     */
    public void setMustraPoet(Boolean mustraPoet) {
        this.mustraPoet = mustraPoet;
    }

    /**
     * @return the mustraPdu
     */
    public Boolean getMustraPdu() {
        return mustraPdu;
    }

    /**
     * @param mustraPdu the mustraPdu to set
     */
    public void setMustraPdu(Boolean mustraPdu) {
        this.mustraPdu = mustraPdu;
    }

    /**
     * @return the tramSinatec
     */
    public boolean isTramSinatec() {
        return tramSinatec;
    }

    /**
     * @param tramSinatec the tramSinatec to set
     */
    public void setTramSinatec(boolean tramSinatec) {
        this.tramSinatec = tramSinatec;
    }

    /**
     * @return the muestraPegTurnado
     */
    public Boolean getMuestraPegTurnado() {
        return muestraPegTurnado;
    }

    /**
     * @param muestraPegTurnado the muestraPegTurnado to set
     */
    public void setMuestraPegTurnado(Boolean muestraPegTurnado) {
        this.muestraPegTurnado = muestraPegTurnado;
    }

    /**
     * @return the muestraPegNoInteg
     */
    public Boolean getMuestraPegNoInteg() {
        return muestraPegNoInteg;
    }

    /**
     * @param muestraPegNoInteg the muestraPegNoInteg to set
     */
    public void setMuestraPegNoInteg(Boolean muestraPegNoInteg) {
        this.muestraPegNoInteg = muestraPegNoInteg;
    }
    public class SustanciaHelper {

        private SustanciaProyecto model;
        private Boolean otra;
        private Integer idAnexo;

        public SustanciaHelper(SustanciaProyecto model, Boolean otra, Integer idAnexo) {
            this.model = model;
            this.otra = otra;
            this.idAnexo = idAnexo;
        }

        public SustanciaProyecto getModel() {
            return model;
        }

        public void setModel(SustanciaProyecto model) {
            this.model = model;
        }

        public Boolean getOtra() {
            if (getModel().getSustanciaId() != null) {
                otra = getModel().getSustanciaId().getSustanciaId().equals(new Short("9999"));
            }
            return otra;
        }

        public void setOtra(Boolean otra) {
            this.otra = otra;
        }

        public Integer getIdAnexo() {
            return idAnexo;
        }

        public void setIdAnexo(Integer idAnexo) {
            this.idAnexo = idAnexo;
        }

    }

    private void cargaListaSust() {
        List<EtapaProyecto> etapa;
        try {
            etapa = (List<EtapaProyecto>) dao.listado_where_comp(EtapaProyecto.class, "etapaProyectoPK", "folioProyecto", proyecto.getProyectoPK().getFolioProyecto());
            System.out.println("cantidad de etapas: " + etapa.size());
            for (EtapaProyecto e : etapa) {
                catEtapa.add(e.getCatEtapa());
            }
        } catch (NullPointerException e) {
        } catch (Exception e) {
        }
    }
    public String getDescUnid(Short clve)
    {
        String resul = "";
        catUnidadMedida = dao.buscaUnid(clve);
        if(catUnidadMedida!=null)
        { resul = catUnidadMedida.getCtunDesc();  }
        return resul;            
    }
    
    public class RepresentanteDataModel extends ListDataModel<Vexdatosusuariorep> implements SelectableDataModel<Vexdatosusuariorep> {

        public RepresentanteDataModel() {
        }

        public RepresentanteDataModel(List<Vexdatosusuariorep> list) {
            super(list);
        }

        public Object getRowKey(Vexdatosusuariorep t) {
            String s = t.getVccurp() + "/" + t.getBgtramiteid();
            return s;
        }

        public Vexdatosusuariorep getRowData(String string) {
            String s[];
            s = string.split("/");
            try {
                return (Vexdatosusuariorep) dao.repLegPorCurp(s[0], Integer.parseInt(s[1]));
//            return (Vexdatosusuariorep) dao.datosPrepLeg(Integer.parseInt(string)).get(0);
            } catch (NoResultException e) {
                return new Vexdatosusuariorep();
            }
        }

    }
    public String getVialidad() {
        return ((CatVialidad) dao.buscaCat(CatVialidad.class,
                new Short(promovente.getBgcveTipoVialLk().
                        toString()))).getDescripcion();
    }

    public String getAsentamiento() {
        return ((CatTipoAsen) dao.buscaCat(CatTipoAsen.class,
                new Short(promovente.getBgcveTipoAsenLk().
                        toString()))).getNombre();
    }
    
    //clase de ayuda para impactos
    public class IdenImpAmbHelper {

        private ImpactoProyecto impacto;
        private Integer id;

        public IdenImpAmbHelper(ImpactoProyecto impacto, Integer id) {
            this.impacto = impacto;
            this.id = id;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public ImpactoProyecto getImpacto() {
            return impacto;
        }

        public void setImpacto(ImpactoProyecto impacto) {
            this.impacto = impacto;
        }
    }
    
    public Integer cambioSituacion(String situacion, String situacAnterior, Integer id) 
    {
        //situacion: Situación a la que se desea cambiar el trámite
        //situacAnterior: Situación en donde el trámite fue turnado, de aqui se obtiene el idAreaEnvia y idUsuarioEnvio(a quien se le habilitará el trámite cunado SINATEC libere el trámite de prenención o algun paro de reloj )
        Historial remitente; ///idArea idusuario
        Historial remitente2; ///idArea idusuario
        Integer bandera=0;
        String idAreaRecibe="";
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, pramBita);
        if (pramBita != null && bitacora2!= null) 
        {
            try 
            {       
                if(!situacAnterior.equals("--"))
                { //02000000
                    if(situacAnterior.equals("dg") || situacAnterior.equals("dadg") || situacAnterior.equals("dgecc"))
                    {
                        if(situacAnterior.equals("dg"))
                        {
                            idArea= "02000000";
                            idusuario=3943;
                            idAreaRecibe =  "02000000";
                        }
                        if(situacAnterior.equals("dadg"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(pramBita,"AT0001");//
                            remitente2 = (Historial) daoBitacora.datosProyHistStatus(pramBita,"AT0027");//
                            if(remitente != null || remitente2 != null)
                            {
                                idArea=remitente.getHistoAreaRecibe();
                                idusuario=remitente.getHistoIdUsuarioRecibe();                            
                                idAreaRecibe = "02000000"; 
                            }
                        }
                        if(situacAnterior.equals("dgecc"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(pramBita,"CIS104");
                            if(remitente != null)
                            {
                                idArea= "02000000";
                                idusuario=3943;                            
                                idAreaRecibe = remitente.getHistoAreaEnvio(); 
                            }
                        }
                            
                    }
                    else
                    {
                        remitente = (Historial) daoBitacora.datosProyHistStatus(pramBita,situacAnterior);
                        if(remitente != null)
                        {
                            idArea=remitente.getHistoAreaRecibe();
                            idusuario=remitente.getHistoIdUsuarioRecibe();                            
                            idAreaRecibe = remitente.getHistoAreaEnvio(); 
                        }
                    }
                }
                else
                {idAreaRecibe = idArea;}//para situaciones AT0002, AT0003 y AT0011
                //FacesMessage message=null;   
                sinatDgira = null;
                sinatDgira = new SinatDgira();
                sinatDgira.setId(id);
                sinatDgira.setSituacion(situacion);
                sinatDgira.setIdEntidadFederativa(bitacora2.getBitaEntidadGestion());
                sinatDgira.setIdClaveTramite(bitacora2.getBitaTipotram()); //STRING BITA_TIPOTRAM
                sinatDgira.setIdTramite(bitacora2.getBitaIdTramite());
                sinatDgira.setIdAreaEnvio(idArea.trim());
                sinatDgira.setIdAreaRecibe(idAreaRecibe.trim());
                sinatDgira.setIdUsuarioEnvio(idusuario);
                //sinatDgira.setIdUsuarioRecibe(1340);
                sinatDgira.setGrupoTrabajo(new Short("0"));
                sinatDgira.setBitacora(pramBita);
                sinatDgira.setFecha(new Date());

                dao.agrega(sinatDgira);
                
                
                //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite turnado correctamente.", null);                    
                System.out.println("Terminó turnado exitosamente, situación: " + situacion);               
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                bandera = 1;                
            } catch (Exception er) {
                bandera = 0;
                er.printStackTrace();
                //FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido turnar el trámite, intente más tarde.", null);
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                System.out.println("Error en turnado a situación " + situacion + " de trámite :  " + er.getMessage() );
            }
        }
        else
        {
            bandera = 0;
        }
        return bandera;
    }
    
}
