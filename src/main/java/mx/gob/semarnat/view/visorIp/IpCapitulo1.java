/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorIp;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.model.dgira_miae.CatClasificacionMineral;
import mx.gob.semarnat.model.dgira_miae.CatMineralesReservados;
import mx.gob.semarnat.model.dgira_miae.CatNorma;
import mx.gob.semarnat.model.dgira_miae.CatObra;
import mx.gob.semarnat.model.dgira_miae.CatParqueIndustrial;
import mx.gob.semarnat.model.dgira_miae.CatPdu;
import mx.gob.semarnat.model.dgira_miae.CatPoet;
import mx.gob.semarnat.model.dgira_miae.CatTipoClima;
import mx.gob.semarnat.model.dgira_miae.CatTipoVegetacion;
import mx.gob.semarnat.model.dgira_miae.EspecificacionProyecto;
import mx.gob.semarnat.model.dgira_miae.ParqueIndustrialProyecto;
import mx.gob.semarnat.model.dgira_miae.PduProyecto;
import mx.gob.semarnat.model.dgira_miae.PoetProyecto;
import mx.gob.semarnat.model.dgira_miae.PreguntaClima;
import mx.gob.semarnat.model.dgira_miae.PreguntaClimaHelper;
import mx.gob.semarnat.model.dgira_miae.PreguntaClimaPK;
import mx.gob.semarnat.model.dgira_miae.PreguntaMineral;
import mx.gob.semarnat.model.dgira_miae.PreguntaObra;
import mx.gob.semarnat.model.dgira_miae.PreguntaObraHelper;
import mx.gob.semarnat.model.dgira_miae.PreguntaObraPK;
import mx.gob.semarnat.model.dgira_miae.PreguntaProyecto;
import mx.gob.semarnat.model.dgira_miae.PreguntaVegetacion;
import mx.gob.semarnat.model.dgira_miae.PreguntaVegetacionHelper;
import mx.gob.semarnat.model.dgira_miae.PreguntaVegetacionPK;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.secure.Util;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "ipCap1")
@ViewScoped
public class IpCapitulo1 {

    /**
     * @return the listaUnidades
     */
    public static List<CatUnidadMedida> getListaUnidades() {
        return listaUnidades;
    }

    /**
     * @param aListaUnidades the listaUnidades to set
     */
    public static void setListaUnidades(List<CatUnidadMedida> aListaUnidades) {
        listaUnidades = aListaUnidades;
    }
    private Bitacora bitacora;
    private final VisorDao dao = new VisorDao();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private List<Object[]> detalleProy = new ArrayList();
    private Proyecto folioDetalle;
    
     //-------supuesto 1
    private String descSupuesto;
    private String descNorma;
    private CatNorma catNorma;
    private List<Object[]> pregNorma = new ArrayList();
    private Boolean MuestraNormas = false; //mostrar panel de normas
    private Boolean MuestraPaqInd = false; //mostrar panel de parques industriales
    private Boolean MuestraDesUrb = false; //mostrar panel de Desarrollo Urbano
    private Proyecto proyecto;
    private String nombreCortoNomb=null;
    private String respPeg4=null;
    private String respPeg5=null;
    private Character respPeg6=null;
    private Character respPeg13=null; 
    private Character respPeg14=null;
    private String respPeg4SN=null;
    private String respPeg5SN=null;
    private String respPeg6SN=null;
    private String respPeg13SN=null; 
    private String respPeg14SN=null;
    private List<Object[]> obrasSup1 = new ArrayList();
  
    
    private Boolean panelPreg = false; // define si se puede ver el panel de las normas
    private Boolean panelEspe = false; // define si se puede ver el panel de especificaciones
    private Boolean verMinSel = false;
    private String msgMinSel = "";
    private List<PreguntaObra> obras = new ArrayList<PreguntaObra>();
    private Boolean disa = true;
    
    
    //normas estaticas 120
    private PreguntaProyecto preguntaProyecto;
    private List<SelectItem> catTipoClima = new LinkedList<SelectItem>(); // catalogo de parques
    private List<SelectItem> catTipoVegetacion = new LinkedList<SelectItem>(); // catalogo de parques
    private PreguntaVegetacion pregVegetacion = new PreguntaVegetacion();
    private PreguntaClima preClima = new PreguntaClima();
    private CatTipoClima tipoClimaSel = new CatTipoClima();
    private CatTipoVegetacion tipoVegetaSel = new CatTipoVegetacion();
    
    private List<EspecificacionProyecto> especificacionProyectoList;

    private List<SelectItem> catTipoMineral = new LinkedList<SelectItem>(); // catalogo de parques
    private List<SelectItem> catTipoObra = new LinkedList<SelectItem>(); // catalogo de parques
    private List<CatClasificacionMineral> clasificacionMineral;
    private List<CatTipoClima> tipoClimaList;
    private List<CatTipoVegetacion> tipoVegetacionList;
    private List<CatObra> tipObraList;
    private String[] estadoMineralSelect;
    private boolean mineralReservado = false;
    private Boolean flagNormCientoV = false;
    private List<PreguntaObraHelper> listaObras = new ArrayList();
    private Character respuestas[] = new Character[2];
    private PreguntaObraHelper obra;
    private PreguntaClimaHelper clima = new PreguntaClimaHelper(0, new PreguntaClima(new PreguntaClimaPK()));
    private List<PreguntaVegetacionHelper> listaVege;
    private PreguntaVegetacionHelper vege;
    private static List<CatUnidadMedida> listaUnidades;
    //norma 141
    private Boolean flagNorm141 = false;
    private List<MineralHelper> lista;
    private List<CatClasificacionMineral> listaClasif;
    private ArrayList<String> catalogoMinerales;
    
     //-------supuesto 2
    private CatParqueIndustrial catParqueIndustrial;
    private ParqueIndustrialProyecto parqueIndustrialProyecto;

    //------supuesto 3
    private PduProyecto pduProyecto;
    private PoetProyecto PoetProyecto;
    private CatPdu catPdu;
    private CatPoet catPoet;
    private String OpcPduPoet;
    private String OpcPduPoet2;
    private String justPduPoet;
    private Boolean mustraPoet=false;  //mustraPoet  mustraPdu
    private Boolean mustraPdu=false;
    
    private String idArea;
    private Integer idusuario;
    private HttpSession sesion = Util.getSession();

    public IpCapitulo1() {
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map<String, String> params = ec.getRequestParameterMap();
        try {
            pramBita = params.get("bitaNum").toString();

        
            if (pramBita != null) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().getSessionMap().put("bitacoraProy", pramBita);
                folioDetalle = dao.verificaERA(pramBita);
                if (folioDetalle.getProyectoPK() != null) 
                {
                    folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                    serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
//                    JOptionPane.showMessageDialog(null, "folio:  " + folioNum + "   serial: " + serialNum, "Error", JOptionPane.INFORMATION_MESSAGE);
                    //----proyecto en sesion
                    proyecto = folioDetalle;
                    
                    context.getExternalContext().getSessionMap().put("userFolioProy", folioDetalle.getProyectoPK().getFolioProyecto());
                    context.getExternalContext().getSessionMap().put("userSerialProy", folioDetalle.getProyectoPK().getSerialProyecto());
                    
                     //---------------capitulo 1-------------------------------------------------
                    if(folioDetalle.getProySupuesto() != null && folioDetalle.getProySupuesto().equals('1'))
                    { 
                        
                        listaObras = new ArrayList();
                        listaVege = new ArrayList();
                        
                        tipoClimaList = dao.tipoClima();
                        tipoVegetacionList = dao.tipoVegetacion();
                        tipObraList = dao.tipoObra();
                        listaUnidades = dao.unidadMedida("volúmen");
        
                        descSupuesto = "Existan normas oficiales mexicanas u otras disposiciones que regulen las emisiones, las descargas, el aprovechamiento de recursos naturales y en general, todos los impactos ambientales relevantes que pueden producir las obras o actividades."; 
                        MuestraNormas = true;
                        MuestraPaqInd = false;      
                        MuestraDesUrb = false;

                        catNorma = dao.descNorma(folioDetalle.getProyNormaId());
                        if(catNorma!=null)
                        { 
                            nombreCortoNomb = catNorma.getNormaNombre();
                            descNorma = catNorma.getNormaDescripcion();
                            pregNorma = dao.pregNorma(folioNum, serialNum);
                            
                            if (catNorma.getNormaId() == 120) {
                                flagNormCientoV = true;
                                panelEspe = true;

                            } else if (catNorma.getNormaId() == 141) {
                                //  
                                preguntaProyecto = dao.pregNorma2(folioNum, serialNum, (short)13);                                
                                if(preguntaProyecto!=null)
                                { respPeg13 = preguntaProyecto.getPreguntaRespuesta(); }
                                else { respPeg13 = null; }
                                
                                preguntaProyecto = dao.pregNorma2(folioNum, serialNum, (short)14);
                                if(preguntaProyecto!=null)
                                { respPeg14 = preguntaProyecto.getPreguntaRespuesta(); }
                                else { respPeg14 = null; }
                                
                                flagNorm141 = true;
                                panelEspe = true;

                            } else {
                                MuestraNormas = true;
                                panelPreg = true;
                                panelEspe = true;

                            }
                            if (proyecto.getProyNormaId() == 120) {

                                flagNormCientoV = true;
                                mineralReservado = false;
                                
                                preguntaProyecto = dao.pregNorma2(folioNum, serialNum, (short)4);                                
                                if(preguntaProyecto!=null)
                                { respPeg4 = preguntaProyecto.getPreguntaRespuesta().toString().equals("S") ? "SI" : "NO";                                   
                                }
                                else { respPeg4 = null; }
                                
                                
                                
                                preguntaProyecto = dao.pregNorma2(folioNum, serialNum, (short)5);
                                if(preguntaProyecto!=null)
                                { respPeg5 = preguntaProyecto.getPreguntaRespuesta().toString().equals("S") ? "SI" : "NO"; 
                                    
                                }
                                else { respPeg5 = null; }
                                
                                preguntaProyecto = dao.pregNorma2(folioNum, serialNum, (short)6);
                                if(preguntaProyecto!=null)
                                { respPeg6 = preguntaProyecto.getPreguntaRespuesta();
                                   
                                }
                                else { respPeg6 = null; }
                                
                                flagNormCientoV = true;
                                panelEspe = true;
                                
                                preClima = dao.getPreguntaClima(folioNum, serialNum);
                                pregVegetacion = dao.getPreguntaVegetacion(folioNum, serialNum);
                                tipoClimaSel = dao.getCatTpoclima(preClima.getPreguntaClimaPK().getClimaId());
                                tipoVegetaSel = dao.getCatTipoVegetacion(pregVegetacion.getPreguntaVegetacionPK().getPreguntaId());
                                
                                try{
                                obrasSup1 = dao.proyectoObras(folioNum, serialNum, (short)5);
                                }catch(Exception rr)
                                {System.out.println("error Obras : " + rr.getMessage());}
                                

                                String minsel = "";
                                List<PreguntaMineral> m = dao.getMinerales(folioNum, serialNum);
                                for (PreguntaMineral t1 : m) {
                                    CatMineralesReservados t3 = dao.getMineral(t1.getPreguntaMineralPK().getIdMinerales());
                                    minsel += t3.getDescripcionMinerales() + ",";
                                }

                                msgMinSel = minsel;
                                verMinSel = true;                                
                                MuestraNormas = true;//panelNorm = MuestraNormas;

                            }
                            if (proyecto.getProyNormaId() == 141) {
                                flagNorm141 = true;
                                mineralReservado = false;

                                String minsel = "";
                                List<PreguntaMineral> m = dao.getMinerales(folioNum, serialNum);
                                for (PreguntaMineral t1 : m) {
                                    CatMineralesReservados t3 = dao.getMineral(t1.getPreguntaMineralPK().getIdMinerales());
                                    minsel += t3.getDescripcionMinerales() + ",";
                                }

                                msgMinSel = minsel;
                                verMinSel = true;
                                MuestraNormas = true;
                            }
                            
                            
                            especificacionProyectoList = dao.getEspecificacionesProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), proyecto.getProyNormaId());
                            
                            //JOptionPane.showMessageDialog(null,  "pregNorma.size: " + pregNorma.size() , "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }
                    }
                    if(folioDetalle.getProySupuesto() != null && folioDetalle.getProySupuesto().equals('2'))                    
                    { 
                        descSupuesto = "Se trate de instalaciones ubicadas en parques industriales autorizados en los términos de la presente."; 
                        MuestraNormas = false;
                        MuestraPaqInd = true;      
                        MuestraDesUrb = false;

                        parqueIndustrialProyecto = dao.parqInd(folioNum, serialNum);
                        if(parqueIndustrialProyecto != null)
                        {
                            catParqueIndustrial = dao.descParqInd(parqueIndustrialProyecto.getParqueIndustrialProyectoPK().getParqueId());  
                        }

                    }
                    if(folioDetalle.getProySupuesto() != null && folioDetalle.getProySupuesto().equals('3'))
                    { 
                        descSupuesto = "Las obras o actividades de que se trate estén expresamente previstas por un plan parcial de desarrollo urbano o de ordenamiento ecológico que haya sido evaluado por la Secretaría en términos del artículo siguiente:"; 
                        MuestraNormas = false;
                        MuestraPaqInd = false;      
                        MuestraDesUrb = true;

                        // OpcPduPoet  OpcPduPoet2   justPduPoet
                        OpcPduPoet = "";  
                        OpcPduPoet2 = "";    
                        justPduPoet = ""; 

                        PoetProyecto = dao.POET(folioNum, serialNum);
                        pduProyecto = dao.PDU(folioNum, serialNum);    

                        //JOptionPane.showMessageDialog(null,  "pregNorma.size: " +  PoetProyecto.getPoetProyectoPK().getFolioProyecto() + "   pdu: " + pduProyecto.getPduJustificacion() , "Error", JOptionPane.INFORMATION_MESSAGE); 

                        if(PoetProyecto!=null)
                        {
                            mustraPoet = true; 
                            mustraPdu = false;
                            catPoet = dao.descPOET(PoetProyecto.getPoetProyectoPK().getPoetId());                        
                            OpcPduPoet = "Programa de Ordenamiento Ecológico de Territorio";                          
                            justPduPoet = PoetProyecto.getPoetJustificacion(); 
                            if(catPoet!=null)
                            {
                                 OpcPduPoet2 = catPoet.getPoetDescripcion();  
                            }
                        }
                        if(pduProyecto!=null)
                        {
                            mustraPoet = false; 
                            mustraPdu = true;
                            catPdu = dao.descPDU(pduProyecto.getPduProyectoPK().getPduId());
                            OpcPduPoet = "Programa de Desarrollo Urbano";
                            justPduPoet = pduProyecto.getPduJustificacion();
                            if(catPdu!=null)
                            {
                                 OpcPduPoet2 = catPdu.getPduDescripcion();  
                            }

                        }
                     }
                } 

            }
        } catch (Exception e) {
         
           // JOptionPane.showMessageDialog(null,  "tramSinatec:  " + e.getMessage() , "Error", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * @return the bitacora
     */
    public Bitacora getBitacora() {
        return bitacora;
    }

    /**
     * @param bitacora the bitacora to set
     */
    public void setBitacora(Bitacora bitacora) {
        this.bitacora = bitacora;
    }

    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * @return the detalleProy
     */
    public List<Object[]> getDetalleProy() {
        return detalleProy;
    }

    /**
     * @param detalleProy the detalleProy to set
     */
    public void setDetalleProy(List<Object[]> detalleProy) {
        this.detalleProy = detalleProy;
    }

    /**
     * @return the folioDetalle
     */
    public Proyecto getFolioDetalle() {
        return folioDetalle;
    }

    /**
     * @param folioDetalle the folioDetalle to set
     */
    public void setFolioDetalle(Proyecto folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    /**
     * @return the descSupuesto
     */
    public String getDescSupuesto() {
        return descSupuesto;
    }

    /**
     * @param descSupuesto the descSupuesto to set
     */
    public void setDescSupuesto(String descSupuesto) {
        this.descSupuesto = descSupuesto;
    }

    /**
     * @return the descNorma
     */
    public String getDescNorma() {
        return descNorma;
    }

    /**
     * @param descNorma the descNorma to set
     */
    public void setDescNorma(String descNorma) {
        this.descNorma = descNorma;
    }

    /**
     * @return the catNorma
     */
    public CatNorma getCatNorma() {
        return catNorma;
    }

    /**
     * @param catNorma the catNorma to set
     */
    public void setCatNorma(CatNorma catNorma) {
        this.catNorma = catNorma;
    }

    /**
     * @return the pregNorma
     */
    public List<Object[]> getPregNorma() {
        return pregNorma;
    }

    /**
     * @param pregNorma the pregNorma to set
     */
    public void setPregNorma(List<Object[]> pregNorma) {
        this.pregNorma = pregNorma;
    }

    /**
     * @return the MuestraNormas
     */
    public Boolean getMuestraNormas() {
        return MuestraNormas;
    }

    /**
     * @param MuestraNormas the MuestraNormas to set
     */
    public void setMuestraNormas(Boolean MuestraNormas) {
        this.MuestraNormas = MuestraNormas;
    }

    /**
     * @return the MuestraPaqInd
     */
    public Boolean getMuestraPaqInd() {
        return MuestraPaqInd;
    }

    /**
     * @param MuestraPaqInd the MuestraPaqInd to set
     */
    public void setMuestraPaqInd(Boolean MuestraPaqInd) {
        this.MuestraPaqInd = MuestraPaqInd;
    }

    /**
     * @return the MuestraDesUrb
     */
    public Boolean getMuestraDesUrb() {
        return MuestraDesUrb;
    }

    /**
     * @param MuestraDesUrb the MuestraDesUrb to set
     */
    public void setMuestraDesUrb(Boolean MuestraDesUrb) {
        this.MuestraDesUrb = MuestraDesUrb;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the catParqueIndustrial
     */
    public CatParqueIndustrial getCatParqueIndustrial() {
        return catParqueIndustrial;
    }

    /**
     * @param catParqueIndustrial the catParqueIndustrial to set
     */
    public void setCatParqueIndustrial(CatParqueIndustrial catParqueIndustrial) {
        this.catParqueIndustrial = catParqueIndustrial;
    }

    /**
     * @return the parqueIndustrialProyecto
     */
    public ParqueIndustrialProyecto getParqueIndustrialProyecto() {
        return parqueIndustrialProyecto;
    }

    /**
     * @param parqueIndustrialProyecto the parqueIndustrialProyecto to set
     */
    public void setParqueIndustrialProyecto(ParqueIndustrialProyecto parqueIndustrialProyecto) {
        this.parqueIndustrialProyecto = parqueIndustrialProyecto;
    }

    /**
     * @return the pduProyecto
     */
    public PduProyecto getPduProyecto() {
        return pduProyecto;
    }

    /**
     * @param pduProyecto the pduProyecto to set
     */
    public void setPduProyecto(PduProyecto pduProyecto) {
        this.pduProyecto = pduProyecto;
    }

    /**
     * @return the PoetProyecto
     */
    public PoetProyecto getPoetProyecto() {
        return PoetProyecto;
    }

    /**
     * @param PoetProyecto the PoetProyecto to set
     */
    public void setPoetProyecto(PoetProyecto PoetProyecto) {
        this.PoetProyecto = PoetProyecto;
    }

    /**
     * @return the catPdu
     */
    public CatPdu getCatPdu() {
        return catPdu;
    }

    /**
     * @param catPdu the catPdu to set
     */
    public void setCatPdu(CatPdu catPdu) {
        this.catPdu = catPdu;
    }

    /**
     * @return the catPoet
     */
    public CatPoet getCatPoet() {
        return catPoet;
    }

    /**
     * @param catPoet the catPoet to set
     */
    public void setCatPoet(CatPoet catPoet) {
        this.catPoet = catPoet;
    }

    /**
     * @return the OpcPduPoet
     */
    public String getOpcPduPoet() {
        return OpcPduPoet;
    }

    /**
     * @param OpcPduPoet the OpcPduPoet to set
     */
    public void setOpcPduPoet(String OpcPduPoet) {
        this.OpcPduPoet = OpcPduPoet;
    }

    /**
     * @return the OpcPduPoet2
     */
    public String getOpcPduPoet2() {
        return OpcPduPoet2;
    }

    /**
     * @param OpcPduPoet2 the OpcPduPoet2 to set
     */
    public void setOpcPduPoet2(String OpcPduPoet2) {
        this.OpcPduPoet2 = OpcPduPoet2;
    }

    /**
     * @return the justPduPoet
     */
    public String getJustPduPoet() {
        return justPduPoet;
    }

    /**
     * @param justPduPoet the justPduPoet to set
     */
    public void setJustPduPoet(String justPduPoet) {
        this.justPduPoet = justPduPoet;
    }

    /**
     * @return the mustraPoet
     */
    public Boolean getMustraPoet() {
        return mustraPoet;
    }

    /**
     * @param mustraPoet the mustraPoet to set
     */
    public void setMustraPoet(Boolean mustraPoet) {
        this.mustraPoet = mustraPoet;
    }

    /**
     * @return the mustraPdu
     */
    public Boolean getMustraPdu() {
        return mustraPdu;
    }

    /**
     * @param mustraPdu the mustraPdu to set
     */
    public void setMustraPdu(Boolean mustraPdu) {
        this.mustraPdu = mustraPdu;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the sesion
     */
    public HttpSession getSesion() {
        return sesion;
    }

    /**
     * @param sesion the sesion to set
     */
    public void setSesion(HttpSession sesion) {
        this.sesion = sesion;
    }

    /**
     * @return the nombreCortoNomb
     */
    public String getNombreCortoNomb() {
        return nombreCortoNomb;
    }

    /**
     * @param nombreCortoNomb the nombreCortoNomb to set
     */
    public void setNombreCortoNomb(String nombreCortoNomb) {
        this.nombreCortoNomb = nombreCortoNomb;
    }

    /**
     * @return the tipoClimaSel
     */
    public CatTipoClima getTipoClimaSel() {
        return tipoClimaSel;
    }

    /**
     * @param tipoClimaSel the tipoClimaSel to set
     */
    public void setTipoClimaSel(CatTipoClima tipoClimaSel) {
        this.tipoClimaSel = tipoClimaSel;
    }

    /**
     * @return the tipoVegetaSel
     */
    public CatTipoVegetacion getTipoVegetaSel() {
        return tipoVegetaSel;
    }

    /**
     * @param tipoVegetaSel the tipoVegetaSel to set
     */
    public void setTipoVegetaSel(CatTipoVegetacion tipoVegetaSel) {
        this.tipoVegetaSel = tipoVegetaSel;
    }

    /**
     * @return the panelPreg
     */
    public Boolean getPanelPreg() {
        return panelPreg;
    }

    /**
     * @param panelPreg the panelPreg to set
     */
    public void setPanelPreg(Boolean panelPreg) {
        this.panelPreg = panelPreg;
    }

    /**
     * @return the flagNormCientoV
     */
    public Boolean getFlagNormCientoV() {
        return flagNormCientoV;
    }

    /**
     * @param flagNormCientoV the flagNormCientoV to set
     */
    public void setFlagNormCientoV(Boolean flagNormCientoV) {
        this.flagNormCientoV = flagNormCientoV;
    }

    /**
     * @return the flagNorm141
     */
    public Boolean getFlagNorm141() {
        return flagNorm141;
    }

    /**
     * @param flagNorm141 the flagNorm141 to set
     */
    public void setFlagNorm141(Boolean flagNorm141) {
        this.flagNorm141 = flagNorm141;
    }
    
    public void agregaObra() {
        if (!getListaObras().isEmpty()) {
            int id;
            PreguntaObra p = new PreguntaObra();
            PreguntaObraPK pk = new PreguntaObraPK();
            p.setPreguntaObraPK(pk);
            id = (getListaObras().get((getListaObras().size() - 1))).getId();
            id++;
            getListaObras().add(new PreguntaObraHelper(id, p));
        } else {
            getListaObras().add(new PreguntaObraHelper(0, new PreguntaObra(new PreguntaObraPK())));
        }
    }



    public void agregaVege() {
        if (!listaVege.isEmpty()) {
            int id;
            id = (getListaVege().get((getListaVege().size() - 1))).getId();
            id++;
            getListaVege().add(new PreguntaVegetacionHelper(id, new PreguntaVegetacion(new PreguntaVegetacionPK())));
        } else {
            getListaVege().add(new PreguntaVegetacionHelper(0, new PreguntaVegetacion(new PreguntaVegetacionPK())));
        }
    }

    /**
     * @return the panelEspe
     */
    public Boolean getPanelEspe() {
        return panelEspe;
    }

    /**
     * @param panelEspe the panelEspe to set
     */
    public void setPanelEspe(Boolean panelEspe) {
        this.panelEspe = panelEspe;
    }

    /**
     * @return the listaObras
     */
    public List<PreguntaObraHelper> getListaObras() {
        return listaObras;
    }

    /**
     * @param listaObras the listaObras to set
     */
    public void setListaObras(List<PreguntaObraHelper> listaObras) {
        this.listaObras = listaObras;
    }

    /**
     * @return the listaVege
     */
    public List<PreguntaVegetacionHelper> getListaVege() {
        return listaVege;
    }

    /**
     * @param listaVege the listaVege to set
     */
    public void setListaVege(List<PreguntaVegetacionHelper> listaVege) {
        this.listaVege = listaVege;
    }

    /**
     * @return the respuestas
     */
    public Character[] getRespuestas() {
        return respuestas;
    }

    /**
     * @param respuestas the respuestas to set
     */
    public void setRespuestas(Character[] respuestas) {
        this.respuestas = respuestas;
    }

    /**
     * @return the disa
     */
    public Boolean getDisa() {
        return disa;
    }

    /**
     * @param disa the disa to set
     */
    public void setDisa(Boolean disa) {
        this.disa = disa;
    }

    /**
     * @return the catTipoClima
     */
    public List<SelectItem> getCatTipoClima() {
        return catTipoClima;
    }

    /**
     * @param catTipoClima the catTipoClima to set
     */
    public void setCatTipoClima(List<SelectItem> catTipoClima) {
        this.catTipoClima = catTipoClima;
    }

    /**
     * @return the catTipoVegetacion
     */
    public List<SelectItem> getCatTipoVegetacion() {
        return catTipoVegetacion;
    }

    /**
     * @param catTipoVegetacion the catTipoVegetacion to set
     */
    public void setCatTipoVegetacion(List<SelectItem> catTipoVegetacion) {
        this.catTipoVegetacion = catTipoVegetacion;
    }

    /**
     * @return the verMinSel
     */
    public Boolean getVerMinSel() {
        return verMinSel;
    }

    /**
     * @param verMinSel the verMinSel to set
     */
    public void setVerMinSel(Boolean verMinSel) {
        this.verMinSel = verMinSel;
    }

    /**
     * @return the msgMinSel
     */
    public String getMsgMinSel() {
        return msgMinSel;
    }

    /**
     * @param msgMinSel the msgMinSel to set
     */
    public void setMsgMinSel(String msgMinSel) {
        this.msgMinSel = msgMinSel;
    }

    /**
     * @return the obras
     */
    public List<PreguntaObra> getObras() {
        return obras;
    }

    /**
     * @param obras the obras to set
     */
    public void setObras(List<PreguntaObra> obras) {
        this.obras = obras;
    }

    /**
     * @return the tipObraList
     */
    public List<CatObra> getTipObraList() {
        return tipObraList;
    }

    /**
     * @param tipObraList the tipObraList to set
     */
    public void setTipObraList(List<CatObra> tipObraList) {
        this.tipObraList = tipObraList;
    }

    /**
     * @return the respPeg4
     */
    public String getRespPeg4() {
        return respPeg4;
    }

    /**
     * @param respPeg4 the respPeg4 to set
     */
    public void setRespPeg4(String respPeg4) {
        this.respPeg4 = respPeg4;
    }

    /**
     * @return the respPeg5
     */
    public String getRespPeg5() {
        return respPeg5;
    }

    /**
     * @param respPeg5 the respPeg5 to set
     */
    public void setRespPeg5(String respPeg5) {
        this.respPeg5 = respPeg5;
    }

    /**
     * @return the respPeg6
     */
    public Character getRespPeg6() {
        return respPeg6;
    }

    /**
     * @param respPeg6 the respPeg6 to set
     */
    public void setRespPeg6(Character respPeg6) {
        this.respPeg6 = respPeg6;
    }

    /**
     * @return the mineralReservado
     */
    public boolean isMineralReservado() {
        return mineralReservado;
    }

    /**
     * @param mineralReservado the mineralReservado to set
     */
    public void setMineralReservado(boolean mineralReservado) {
        this.mineralReservado = mineralReservado;
    }

    /**
     * @return the estadoMineralSelect
     */
    public String[] getEstadoMineralSelect() {
        return estadoMineralSelect;
    }

    /**
     * @param estadoMineralSelect the estadoMineralSelect to set
     */
    public void setEstadoMineralSelect(String[] estadoMineralSelect) {
        this.estadoMineralSelect = estadoMineralSelect;
    }

    /**
     * @return the lista
     */
    public List<MineralHelper> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<MineralHelper> lista) {
        this.lista = lista;
    }

    /**
     * @return the clasificacionMineral
     */
    public List<CatClasificacionMineral> getClasificacionMineral() {
        return clasificacionMineral;
    }

    /**
     * @param clasificacionMineral the clasificacionMineral to set
     */
    public void setClasificacionMineral(List<CatClasificacionMineral> clasificacionMineral) {
        this.clasificacionMineral = clasificacionMineral;
    }

    /**
     * @return the obrasSup1
     */
    public List<Object[]> getObrasSup1() {
        return obrasSup1;
    }

    /**
     * @param obrasSup1 the obrasSup1 to set
     */
    public void setObrasSup1(List<Object[]> obrasSup1) {
        this.obrasSup1 = obrasSup1;
    }

    /**
     * @return the respPeg13
     */
    public Character getRespPeg13() {
        return respPeg13;
    }

    /**
     * @param respPeg13 the respPeg13 to set
     */
    public void setRespPeg13(Character respPeg13) {
        this.respPeg13 = respPeg13;
    }

    /**
     * @return the respPeg14
     */
    public Character getRespPeg14() {
        return respPeg14;
    }

    /**
     * @param respPeg14 the respPeg14 to set
     */
    public void setRespPeg14(Character respPeg14) {
        this.respPeg14 = respPeg14;
    }

    /**
     * @return the respPeg4SN
     */
    public String getRespPeg4SN() {
        return respPeg4SN;
    }

    /**
     * @param respPeg4SN the respPeg4SN to set
     */
    public void setRespPeg4SN(String respPeg4SN) {
        this.respPeg4SN = respPeg4SN;
    }

    /**
     * @return the respPeg5SN
     */
    public String getRespPeg5SN() {
        return respPeg5SN;
    }

    /**
     * @param respPeg5SN the respPeg5SN to set
     */
    public void setRespPeg5SN(String respPeg5SN) {
        this.respPeg5SN = respPeg5SN;
    }

    /**
     * @return the respPeg6SN
     */
    public String getRespPeg6SN() {
        return respPeg6SN;
    }

    /**
     * @param respPeg6SN the respPeg6SN to set
     */
    public void setRespPeg6SN(String respPeg6SN) {
        this.respPeg6SN = respPeg6SN;
    }

    /**
     * @return the respPeg13SN
     */
    public String getRespPeg13SN() {
        return respPeg13SN;
    }

    /**
     * @param respPeg13SN the respPeg13SN to set
     */
    public void setRespPeg13SN(String respPeg13SN) {
        this.respPeg13SN = respPeg13SN;
    }

    /**
     * @return the respPeg14SN
     */
    public String getRespPeg14SN() {
        return respPeg14SN;
    }

    /**
     * @param respPeg14SN the respPeg14SN to set
     */
    public void setRespPeg14SN(String respPeg14SN) {
        this.respPeg14SN = respPeg14SN;
    }

   
    
    public class MineralHelper {//clase de apoyo para mostrar los minerales

        private List<CatMineralesReservados> listaMinerales;
        private ArrayList<String> listaIds;
        private String catalogo;

        public MineralHelper() {
        }

        public MineralHelper(List<CatMineralesReservados> listaMinerales, ArrayList<String> listaIds, String catalogo) {
            this.listaMinerales = listaMinerales;
            this.listaIds = listaIds;
            this.catalogo = catalogo;
        }

        public ArrayList<String> getListaIds() {
            return listaIds;
        }

        public void setListaIds(ArrayList<String> listaIds) {
            this.listaIds = listaIds;
        }

        public List<CatMineralesReservados> getListaMinerales() {
            return listaMinerales;
        }

        public void setListaMinerales(List<CatMineralesReservados> listaMinerales) {
            this.listaMinerales = listaMinerales;
        }

        public String getCatalogo() {
            return catalogo;
        }

        public void setCatalogo(String catalogo) {
            this.catalogo = catalogo;
        }
    }
    
    //normas estáticas
    public void cargaMinerales() {
        String label;
        getLista().clear();
        for (String estadoMineralSelect1 : getEstadoMineralSelect()) {
            label = ((CatClasificacionMineral) dao.busca(CatClasificacionMineral.class, new Short(estadoMineralSelect1))).
                    getDescripcionClasificacion();
            getLista().
                    add(new MineralHelper((List<CatMineralesReservados>) dao.listado_where(CatMineralesReservados.class, "idClasificacion", new Short(estadoMineralSelect1)),
                                    new ArrayList(), label.length() > 50 ? label.substring(0, 50).concat("...") : label));
        }

    }

	/**
	 * @return the especificacionProyectoList
	 */
	public List<EspecificacionProyecto> getEspecificacionProyectoList() {
		return especificacionProyectoList;
	}

	/**
	 * @param especificacionProyectoList the especificacionProyectoList to set
	 */
	public void setEspecificacionProyectoList(List<EspecificacionProyecto> especificacionProyectoList) {
		this.especificacionProyectoList = especificacionProyectoList;
	}
    
}
