/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorIp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.MenuDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.CatEtapa;
import mx.gob.semarnat.model.dgira_miae.EtapaProyecto;
import mx.gob.semarnat.model.dgira_miae.ImpactoProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.secure.Util;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "ipCap3")
@ViewScoped
public class IpCapitulo3 {
    private Bitacora bitacora;
    private final VisorDao dao = new VisorDao();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private List<Object[]> detalleProy = new ArrayList();
    private Proyecto proyecto;
    private final MenuDao MenuDao= new MenuDao();
    private String idArea;
    private Integer idusuario;
    private HttpSession sesion = Util.getSession();
    private String cveProyecto; 

    private Proyecto folioDetalle;
    //-----------------capitulo 3
    private List<VisorView.IdenImpAmbHelper> impactoProyectos;
    private List<ImpactoProyecto> impactosBD;
    private VisorView.IdenImpAmbHelper impactoProyectoSelect;
    private List<ArchivosProyecto> anexosProyecto2 = new ArrayList<ArchivosProyecto>();
    private String anios[];
    private String meses[];
    private Boolean banderas[];
    private Boolean nuevo[];
    private EtapaProyecto preparacionSitio = new EtapaProyecto();
    private EtapaProyecto construccion = new EtapaProyecto();
    private EtapaProyecto operaMantenim = new EtapaProyecto();
    private EtapaProyecto abandono = new EtapaProyecto();
    private List<ArchivosProyecto> anexosProyectoEtapa1 = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> anexosProyectoEtapa2 = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> anexosProyectoEtapa3 = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> anexosProyectoEtapa4 = new ArrayList<ArchivosProyecto>();
    private List<VisorView.SustanciaHelper> sustanciaProyectos;
    private VisorView.SustanciaHelper sustanciaSeleccionada;
    private List<CatEtapa> catEtapa;
    private boolean nuevoSust = false;
    private boolean supera = false;
    private CatUnidadMedida catUnidadMedida;
    private List<ArchivosProyecto> anexosProyectoSust = new ArrayList<ArchivosProyecto>();
    private List<Object[]> proyectoSustancia = new ArrayList<Object[]>();
    private List<Object[]> proyectoEmiResDes = new ArrayList<Object[]>();
    private List<Object[]> proyImpactos = new ArrayList<Object[]>();
    private List<ArchivosProyecto> anexosProyectoImpAmb = new ArrayList<ArchivosProyecto>();
    

    public IpCapitulo3() {
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        try {
            pramBita = params.get("bitaNum").toString();

        
            if (pramBita != null) 
            {
                cveProyecto = MenuDao.cveTramite(pramBita);
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().getSessionMap().put("bitacoraProy", pramBita);
                folioDetalle = dao.verificaERA(pramBita);
                if (folioDetalle.getProyectoPK() != null) 
                {
                    folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                    serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                    //----proyecto en sesion
                    proyecto = folioDetalle;
                    
                    context.getExternalContext().getSessionMap().put("userFolioProy", folioDetalle.getProyectoPK().getFolioProyecto());
                    context.getExternalContext().getSessionMap().put("userSerialProy", folioDetalle.getProyectoPK().getSerialProyecto());
                    
                    //---------------capitulo 3 ---------------------------------   
                        //3,1,2,1,
                        short capituloId2 = (short) 0;
                        short subcapituloId2 = (short) 0;
                        short seccionId2 = (short) 0;
                        short apartadoId2 = (short) 0; 
                        capituloId2 = 3;
                        subcapituloId2 = 1;
                        seccionId2 = 2;
                        apartadoId2 = 1;
                        anexosProyecto2 = dao.getArchivosProyecto(capituloId2, subcapituloId2, seccionId2, apartadoId2, folioNum, serialNum);

                        banderas = new Boolean[]{false, false, false, false};
                        anios = new String[]{null, null, null, null, null};
                        meses = new String[]{null, null, null, null, null};
                        cargaLista();

                       // 3,1,3,1
                        short capituloId3 = (short) 0;
                        short subcapituloId3 = (short) 0;
                        short seccionId3 = (short) 0;
                        short apartadoId3 = (short) 0; 
                        capituloId3 = 3;
                        subcapituloId3 = 1;
                        seccionId3 = 3;
                        apartadoId3 = 1;
                        anexosProyectoEtapa1 = dao.getArchivosProyecto(capituloId3, subcapituloId3, seccionId3, apartadoId3, folioNum, serialNum); 

                        //3,1,4,1
                        short capituloId4 = (short) 0;
                        short subcapituloId4 = (short) 0;
                        short seccionId4 = (short) 0;
                        short apartadoId4 = (short) 0; 
                        capituloId4 = 3;
                        subcapituloId4 = 1;
                        seccionId4 = 4;
                        apartadoId4 = 1;
                        anexosProyectoEtapa2 = dao.getArchivosProyecto(capituloId4, subcapituloId4, seccionId4, apartadoId4, folioNum, serialNum); 

                        //3,1,5,1
                        short capituloId5 = (short) 0;
                        short subcapituloId5 = (short) 0;
                        short seccionId5 = (short) 0;
                        short apartadoId5 = (short) 0; 
                        capituloId5 = 3;
                        subcapituloId5 = 1;
                        seccionId5 = 5;
                        apartadoId5 = 1;
                        anexosProyectoEtapa3 = dao.getArchivosProyecto(capituloId5, subcapituloId5, seccionId5, apartadoId5, folioNum, serialNum); 

                        //3,1,6,1
                        short capituloId6 = (short) 0;
                        short subcapituloId6 = (short) 0;
                        short seccionId6 = (short) 0;
                        short apartadoId6 = (short) 0; 
                        capituloId6 = 3;
                        subcapituloId6 = 1;
                        seccionId6 = 6;
                        apartadoId6 = 1;
                        anexosProyectoEtapa4 = dao.getArchivosProyecto(capituloId6, subcapituloId6, seccionId6, apartadoId6, folioNum, serialNum); 
                        
                        System.out.println("Antes de obtener las sustancias y residuos ::::::::::::::::::::::::::::::: ");
                        proyectoSustancia = dao.sustanciaProy(folioNum, serialNum);
                        System.out.println("Size of sustancias  ::::::::::::::::::::::::::::::: " + (proyectoSustancia != null ? proyectoSustancia.size() : 0));
                        proyectoEmiResDes = dao.emiResDesProy(folioNum, serialNum);
                        System.out.println("Size of residuos  ::::::::::::::::::::::::::::::: " + (proyectoEmiResDes != null ? proyectoEmiResDes.size() : 0));
                        
                        proyImpactos = dao.impactoProy(folioNum, serialNum);
                        short capituloId7 = (short) 0;
                        short subcapituloId7 = (short) 0;
                        short seccionId7 = (short) 0;
                        short apartadoId7 = (short) 0;
                        capituloId7 = 3;
                        subcapituloId7 = 5;
                        seccionId7 = 1;
                        apartadoId7 = 1;
                        anexosProyectoImpAmb = dao.getArchivosProyecto(capituloId7, subcapituloId7, seccionId7, apartadoId7, folioNum, serialNum); 

    
                } 

            }
        } catch (Exception e) {
        	e.printStackTrace();
           // JOptionPane.showMessageDialog(null,  "tramSinatec:  " + e.getMessage() , "Error", JOptionPane.INFORMATION_MESSAGE);
        }    
                    
    }
    
    private void cargaLista() {
        setNuevo(new Boolean[]{true, true, true, true});
        List<EtapaProyecto> etapa;
        if (getProyecto().getProyTiempoVidaAnios() != null && getProyecto().getProyTiempoVidaMeses() != null) {
            getAnios()[0] = getProyecto().getProyTiempoVidaAnios().toString();
            getMeses()[0] = getProyecto().getProyTiempoVidaMeses().toString();
        }

        try {
            etapa = (List<EtapaProyecto>) dao.listado_where_comp(EtapaProyecto.class, "etapaProyectoPK", "folioProyecto", getProyecto().getProyectoPK().getFolioProyecto());

            for (EtapaProyecto e : etapa) {
                getNuevo()[(int) e.getCatEtapa().getEtapaId() - 1] = false;
                getAnios()[(int) e.getCatEtapa().getEtapaId()] = e.getAnios() + "";
                getMeses()[(int) e.getCatEtapa().getEtapaId()] = e.getMeses() + "";
                getBanderas()[(int) e.getCatEtapa().getEtapaId() - 1] = e.getAnios() > 0 || e.getMeses() > 0;
                switch ((int) e.getCatEtapa().getEtapaId()) {
                    case 1:
                        setPreparacionSitio(e);
                        break;
                    case 2:
                        setConstruccion(e);
                        break;
                    case 3:
                        setOperaMantenim(e);
                        break;
                    case 4:
                        setAbandono(e);
                        break;
                }
            }
        } catch (NullPointerException e) {
        } catch (Exception e) {
        }
    }

    /**
     * @return the nuevo
     */
    public Boolean[] getNuevo() {
        return nuevo;
    }

    /**
     * @param nuevo the nuevo to set
     */
    public void setNuevo(Boolean[] nuevo) {
        this.nuevo = nuevo;
    }

    /**
     * @return the preparacionSitio
     */
    public EtapaProyecto getPreparacionSitio() {
        return preparacionSitio;
    }

    /**
     * @param preparacionSitio the preparacionSitio to set
     */
    public void setPreparacionSitio(EtapaProyecto preparacionSitio) {
        this.preparacionSitio = preparacionSitio;
    }

    /**
     * @return the construccion
     */
    public EtapaProyecto getConstruccion() {
        return construccion;
    }

    /**
     * @param construccion the construccion to set
     */
    public void setConstruccion(EtapaProyecto construccion) {
        this.construccion = construccion;
    }

    /**
     * @return the operaMantenim
     */
    public EtapaProyecto getOperaMantenim() {
        return operaMantenim;
    }

    /**
     * @param operaMantenim the operaMantenim to set
     */
    public void setOperaMantenim(EtapaProyecto operaMantenim) {
        this.operaMantenim = operaMantenim;
    }

    /**
     * @return the abandono
     */
    public EtapaProyecto getAbandono() {
        return abandono;
    }

    /**
     * @param abandono the abandono to set
     */
    public void setAbandono(EtapaProyecto abandono) {
        this.abandono = abandono;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the impactoProyectos
     */
    public List<VisorView.IdenImpAmbHelper> getImpactoProyectos() {
        return impactoProyectos;
    }

    /**
     * @param impactoProyectos the impactoProyectos to set
     */
    public void setImpactoProyectos(List<VisorView.IdenImpAmbHelper> impactoProyectos) {
        this.impactoProyectos = impactoProyectos;
    }

    /**
     * @return the impactosBD
     */
    public List<ImpactoProyecto> getImpactosBD() {
        return impactosBD;
    }

    /**
     * @param impactosBD the impactosBD to set
     */
    public void setImpactosBD(List<ImpactoProyecto> impactosBD) {
        this.impactosBD = impactosBD;
    }

    /**
     * @return the impactoProyectoSelect
     */
    public VisorView.IdenImpAmbHelper getImpactoProyectoSelect() {
        return impactoProyectoSelect;
    }

    /**
     * @param impactoProyectoSelect the impactoProyectoSelect to set
     */
    public void setImpactoProyectoSelect(VisorView.IdenImpAmbHelper impactoProyectoSelect) {
        this.impactoProyectoSelect = impactoProyectoSelect;
    }

    /**
     * @return the anexosProyecto2
     */
    public List<ArchivosProyecto> getAnexosProyecto2() {
        return anexosProyecto2;
    }

    /**
     * @param anexosProyecto2 the anexosProyecto2 to set
     */
    public void setAnexosProyecto2(List<ArchivosProyecto> anexosProyecto2) {
        this.anexosProyecto2 = anexosProyecto2;
    }

    /**
     * @return the anios
     */
    public String[] getAnios() {
        return anios;
    }

    /**
     * @param anios the anios to set
     */
    public void setAnios(String[] anios) {
        this.anios = anios;
    }

    /**
     * @return the meses
     */
    public String[] getMeses() {
        return meses;
    }

    /**
     * @param meses the meses to set
     */
    public void setMeses(String[] meses) {
        this.meses = meses;
    }

    /**
     * @return the banderas
     */
    public Boolean[] getBanderas() {
        return banderas;
    }

    /**
     * @param banderas the banderas to set
     */
    public void setBanderas(Boolean[] banderas) {
        this.banderas = banderas;
    }

    /**
     * @return the anexosProyectoEtapa1
     */
    public List<ArchivosProyecto> getAnexosProyectoEtapa1() {
        return anexosProyectoEtapa1;
    }

    /**
     * @param anexosProyectoEtapa1 the anexosProyectoEtapa1 to set
     */
    public void setAnexosProyectoEtapa1(List<ArchivosProyecto> anexosProyectoEtapa1) {
        this.anexosProyectoEtapa1 = anexosProyectoEtapa1;
    }

    /**
     * @return the anexosProyectoEtapa2
     */
    public List<ArchivosProyecto> getAnexosProyectoEtapa2() {
        return anexosProyectoEtapa2;
    }

    /**
     * @param anexosProyectoEtapa2 the anexosProyectoEtapa2 to set
     */
    public void setAnexosProyectoEtapa2(List<ArchivosProyecto> anexosProyectoEtapa2) {
        this.anexosProyectoEtapa2 = anexosProyectoEtapa2;
    }

    /**
     * @return the anexosProyectoEtapa3
     */
    public List<ArchivosProyecto> getAnexosProyectoEtapa3() {
        return anexosProyectoEtapa3;
    }

    /**
     * @param anexosProyectoEtapa3 the anexosProyectoEtapa3 to set
     */
    public void setAnexosProyectoEtapa3(List<ArchivosProyecto> anexosProyectoEtapa3) {
        this.anexosProyectoEtapa3 = anexosProyectoEtapa3;
    }

    /**
     * @return the anexosProyectoEtapa4
     */
    public List<ArchivosProyecto> getAnexosProyectoEtapa4() {
        return anexosProyectoEtapa4;
    }

    /**
     * @param anexosProyectoEtapa4 the anexosProyectoEtapa4 to set
     */
    public void setAnexosProyectoEtapa4(List<ArchivosProyecto> anexosProyectoEtapa4) {
        this.anexosProyectoEtapa4 = anexosProyectoEtapa4;
    }

    /**
     * @return the sustanciaProyectos
     */
    public List<VisorView.SustanciaHelper> getSustanciaProyectos() {
        return sustanciaProyectos;
    }

    /**
     * @param sustanciaProyectos the sustanciaProyectos to set
     */
    public void setSustanciaProyectos(List<VisorView.SustanciaHelper> sustanciaProyectos) {
        this.sustanciaProyectos = sustanciaProyectos;
    }

    /**
     * @return the sustanciaSeleccionada
     */
    public VisorView.SustanciaHelper getSustanciaSeleccionada() {
        return sustanciaSeleccionada;
    }

    /**
     * @param sustanciaSeleccionada the sustanciaSeleccionada to set
     */
    public void setSustanciaSeleccionada(VisorView.SustanciaHelper sustanciaSeleccionada) {
        this.sustanciaSeleccionada = sustanciaSeleccionada;
    }

    /**
     * @return the catEtapa
     */
    public List<CatEtapa> getCatEtapa() {
        return catEtapa;
    }

    /**
     * @param catEtapa the catEtapa to set
     */
    public void setCatEtapa(List<CatEtapa> catEtapa) {
        this.catEtapa = catEtapa;
    }

    /**
     * @return the nuevoSust
     */
    public boolean isNuevoSust() {
        return nuevoSust;
    }

    /**
     * @param nuevoSust the nuevoSust to set
     */
    public void setNuevoSust(boolean nuevoSust) {
        this.nuevoSust = nuevoSust;
    }

    /**
     * @return the supera
     */
    public boolean isSupera() {
        return supera;
    }

    /**
     * @param supera the supera to set
     */
    public void setSupera(boolean supera) {
        this.supera = supera;
    }

    /**
     * @return the catUnidadMedida
     */
    public CatUnidadMedida getCatUnidadMedida() {
        return catUnidadMedida;
    }

    /**
     * @param catUnidadMedida the catUnidadMedida to set
     */
    public void setCatUnidadMedida(CatUnidadMedida catUnidadMedida) {
        this.catUnidadMedida = catUnidadMedida;
    }

    /**
     * @return the anexosProyectoSust
     */
    public List<ArchivosProyecto> getAnexosProyectoSust() {
        return anexosProyectoSust;
    }

    /**
     * @param anexosProyectoSust the anexosProyectoSust to set
     */
    public void setAnexosProyectoSust(List<ArchivosProyecto> anexosProyectoSust) {
        this.anexosProyectoSust = anexosProyectoSust;
    }

    /**
     * @return the proyectoSustancia
     */
    public List<Object[]> getProyectoSustancia() {
        return proyectoSustancia;
    }

    /**
     * @param proyectoSustancia the proyectoSustancia to set
     */
    public void setProyectoSustancia(List<Object[]> proyectoSustancia) {
        this.proyectoSustancia = proyectoSustancia;
    }

    /**
     * @return the proyectoEmiResDes
     */
    public List<Object[]> getProyectoEmiResDes() {
        return proyectoEmiResDes;
    }

    /**
     * @param proyectoEmiResDes the proyectoEmiResDes to set
     */
    public void setProyectoEmiResDes(List<Object[]> proyectoEmiResDes) {
        this.proyectoEmiResDes = proyectoEmiResDes;
    }

    /**
     * @return the proyImpactos
     */
    public List<Object[]> getProyImpactos() {
        return proyImpactos;
    }

    /**
     * @param proyImpactos the proyImpactos to set
     */
    public void setProyImpactos(List<Object[]> proyImpactos) {
        this.proyImpactos = proyImpactos;
    }

    /**
     * @return the anexosProyectoImpAmb
     */
    public List<ArchivosProyecto> getAnexosProyectoImpAmb() {
        return anexosProyectoImpAmb;
    }

    /**
     * @param anexosProyectoImpAmb the anexosProyectoImpAmb to set
     */
    public void setAnexosProyectoImpAmb(List<ArchivosProyecto> anexosProyectoImpAmb) {
        this.anexosProyectoImpAmb = anexosProyectoImpAmb;
    }
    
    
    
}
