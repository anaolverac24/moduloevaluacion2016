/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorIp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import mx.gob.semarnat.dao.MenuDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.secure.Util;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "ipCapERA")
@ViewScoped
public class IpCapituloERA {
    private Bitacora bitacora;
    private final VisorDao dao = new VisorDao();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private List<Object[]> detalleProy = new ArrayList();
    private Proyecto proyecto;
    private final MenuDao MenuDao= new MenuDao();
    private String idArea;
    private Integer idusuario;
    private HttpSession sesion = Util.getSession();
    private String cveProyecto; 
    private Proyecto folioDetalle;
    
    //------------------------------------------------
    private EstudioRiesgoProyecto estudio = new EstudioRiesgoProyecto();
    private List<ArchivosProyecto> descSistTransp = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> baseDiseno = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> condOper = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> pruebVerif = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> procMedCont = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> analEvalRiesg = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> metodIdentJerq = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> radPotAfect = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> intRiesgo = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> efectAreaInfl = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> recomTecOper = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> sistSeg = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> medPrevent = new ArrayList<ArchivosProyecto>();

    private List<ArchivosProyecto> conclucERA = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> resumenRA = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> infoTec = new ArrayList<ArchivosProyecto>();
    
    private List<ArchivosProyecto> planosLocFot = new ArrayList<ArchivosProyecto>(); 
    private List<ArchivosProyecto> otrosAnex = new ArrayList<ArchivosProyecto>();

    public IpCapituloERA() {
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        try {
            pramBita = params.get("bitaNum").toString();

        
            if (pramBita != null) 
            {
                cveProyecto = MenuDao.cveTramite(pramBita);
                FacesContext context = FacesContext.getCurrentInstance();
                context.getExternalContext().getSessionMap().put("bitacoraProy", pramBita);
                folioDetalle = dao.verificaERA(pramBita);
                if (folioDetalle.getProyectoPK() != null) 
                {
                    folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                    serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                    //----proyecto en sesion
                    proyecto = folioDetalle;
                    
                    context.getExternalContext().getSessionMap().put("userFolioProy", folioDetalle.getProyectoPK().getFolioProyecto());
                    context.getExternalContext().getSessionMap().put("userSerialProy", folioDetalle.getProyectoPK().getSerialProyecto());
                    
                    
                //---------------capituloERA ---------------------------------          
                        
                        estudio = dao.cargaEstudioRiesgo(proyecto);

                        //4,2,1,1
                        short capituloId8 = (short) 0;
                        short subcapituloId8 = (short) 0;
                        short seccionId8 = (short) 0;
                        short apartadoId8 = (short) 0;
                        capituloId8 = 4;
                        subcapituloId8 = 2;
                        seccionId8 = 1;
                        apartadoId8 = 1;
                        descSistTransp = dao.getArchivosProyecto(capituloId8, subcapituloId8, seccionId8, apartadoId8, folioNum, serialNum); 

                        //4,2,2,1
                        short capituloId9 = (short) 0;
                        short subcapituloId9 = (short) 0;
                        short seccionId9 = (short) 0;
                        short apartadoId9 = (short) 0;
                        capituloId9 = 4;
                        subcapituloId9 = 2;
                        seccionId9 = 2;
                        apartadoId9 = 1;
                        baseDiseno = dao.getArchivosProyecto(capituloId9, subcapituloId9, seccionId9, apartadoId9, folioNum, serialNum); 

                        //4,2,4,1
                        short capituloId10 = (short) 0;
                        short subcapituloId10 = (short) 0;
                        short seccionId10 = (short) 0;
                        short apartadoId10 = (short) 0;
                        capituloId10 = 4;
                        subcapituloId10 = 2;
                        seccionId10 = 4;
                        apartadoId10 = 1;
                        condOper = dao.getArchivosProyecto(capituloId10, subcapituloId10, seccionId10, apartadoId10, folioNum, serialNum); 
                        //4,2,5,1
                        short capituloId11 = (short) 0;
                        short subcapituloId11 = (short) 0;
                        short seccionId11 = (short) 0;
                        short apartadoId11 = (short) 0;
                        capituloId11 = 4;
                        subcapituloId11 = 2;
                        seccionId11 = 5;
                        apartadoId11 = 1;
                        pruebVerif = dao.getArchivosProyecto(capituloId11, subcapituloId11, seccionId11, apartadoId11, folioNum, serialNum); 

                        //4,2,6,1
                        short capituloId12 = (short) 0;
                        short subcapituloId12 = (short) 0;
                        short seccionId12 = (short) 0;
                        short apartadoId12 = (short) 0;
                        capituloId12 = 4;
                        subcapituloId12 = 2;
                        seccionId12 = 6;
                        apartadoId12 = 1;
                        procMedCont = dao.getArchivosProyecto(capituloId12, subcapituloId12, seccionId12, apartadoId12, folioNum, serialNum); 

                        //4,2,7,1
                        short capituloId13 = (short) 0;
                        short subcapituloId13 = (short) 0;
                        short seccionId13 = (short) 0;
                        short apartadoId13 = (short) 0;
                        capituloId13 = 4;
                        subcapituloId13 = 2;
                        seccionId13 = 7;
                        apartadoId13 = 1;
                        analEvalRiesg = dao.getArchivosProyecto(capituloId13, subcapituloId13, seccionId13, apartadoId13, folioNum, serialNum); 

                        //4,2,8,1
                        short capituloId14 = (short) 0;
                        short subcapituloId14 = (short) 0;
                        short seccionId14 = (short) 0;
                        short apartadoId14 = (short) 0;
                        capituloId14 = 4;
                        subcapituloId14 = 2;
                        seccionId14 = 8;
                        apartadoId14 = 1;
                        metodIdentJerq = dao.getArchivosProyecto(capituloId14 , subcapituloId14 , seccionId14 , apartadoId14 , folioNum, serialNum); 

                        //4,3,1,1
                        short capituloId15 = (short) 0;
                        short subcapituloId15 = (short) 0;
                        short seccionId15 = (short) 0;
                        short apartadoId15 = (short) 0;
                        capituloId15 = 4;
                        subcapituloId15 = 3;
                        seccionId15 = 1;
                        apartadoId15 = 1;
                        radPotAfect = dao.getArchivosProyecto(capituloId15 , subcapituloId15 , seccionId15 , apartadoId15 , folioNum, serialNum); 

                        //4,3,2,1
                        short capituloId16 = (short) 0;
                        short subcapituloId16 = (short) 0;
                        short seccionId16 = (short) 0;
                        short apartadoId16 = (short) 0;
                        capituloId16 = 4;
                        subcapituloId16 = 3;
                        seccionId16 = 2;
                        apartadoId16 = 1;
                        intRiesgo = dao.getArchivosProyecto(capituloId16 , subcapituloId16 , seccionId16 , apartadoId16 , folioNum, serialNum); 

                        //4,3,3,1
                        short capituloId17 = (short) 0;
                        short subcapituloId17 = (short) 0;
                        short seccionId17 = (short) 0;
                        short apartadoId17 = (short) 0;
                        capituloId17 = 4;
                        subcapituloId17 = 3;
                        seccionId17 = 3;
                        apartadoId17 = 1;
                        efectAreaInfl= dao.getArchivosProyecto(capituloId17 , subcapituloId17 , seccionId17 , apartadoId17 , folioNum, serialNum); 

                        //4,4,1,1
                        short capituloId18 = (short) 0;
                        short subcapituloId18 = (short) 0;
                        short seccionId18 = (short) 0;
                        short apartadoId18 = (short) 0;
                        capituloId18 = 4;
                        subcapituloId18 = 4;
                        seccionId18 = 1;
                        apartadoId18 = 1;
                        recomTecOper= dao.getArchivosProyecto(capituloId18 , subcapituloId18 , seccionId18 , apartadoId18 , folioNum, serialNum); 

                        //4,4,2,1
                        short capituloId19 = (short) 0;
                        short subcapituloId19 = (short) 0;
                        short seccionId19 = (short) 0;
                        short apartadoId19 = (short) 0;
                        capituloId19 = 4;
                        subcapituloId19 = 4;
                        seccionId19 = 2;
                        apartadoId19 = 1;
                        sistSeg= dao.getArchivosProyecto(capituloId19 , subcapituloId19 , seccionId19 , apartadoId19 , folioNum, serialNum); 

                        //4,4,3,1
                        short capituloId20 = (short) 0;
                        short subcapituloId20 = (short) 0;
                        short seccionId20 = (short) 0;
                        short apartadoId20 = (short) 0;
                        capituloId20 = 4;
                        subcapituloId20 = 4;
                        seccionId20 = 3;
                        apartadoId20 = 1;
                        medPrevent= dao.getArchivosProyecto(capituloId20 , subcapituloId20, seccionId20, apartadoId20, folioNum, serialNum); 

                        //4,5,1,1
                        short capituloId21 = (short) 0;
                        short subcapituloId21 = (short) 0;
                        short seccionId21 = (short) 0;
                        short apartadoId21 = (short) 0;
                        capituloId21 = 4;
                        subcapituloId21 = 5;
                        seccionId21 = 1;
                        apartadoId21 = 1;
                        conclucERA= dao.getArchivosProyecto(capituloId21, subcapituloId21, seccionId21, apartadoId21, folioNum, serialNum); 

                        //4,5,2,1	
                        short capituloId22 = (short) 0;
                        short subcapituloId22 = (short) 0;
                        short seccionId22 = (short) 0;
                        short apartadoId22 = (short) 0;
                        capituloId22 = 4;
                        subcapituloId22 = 5;
                        seccionId22 = 2;
                        apartadoId22 = 1;
                        resumenRA= dao.getArchivosProyecto(capituloId22 , subcapituloId22, seccionId22, apartadoId22, folioNum, serialNum); 

                        //4,5,3,1
                        short capituloId23 = (short) 0;
                        short subcapituloId23 = (short) 0;
                        short seccionId23 = (short) 0;
                        short apartadoId23 = (short) 0;
                        capituloId23 = 4;
                        subcapituloId23 = 5;
                        seccionId23 = 3;
                        apartadoId23 = 1;
                        infoTec= dao.getArchivosProyecto(capituloId23 , subcapituloId23, seccionId23, apartadoId23, folioNum, serialNum); 
                      
                        //4,6,1,1
                        short capituloId24 = (short) 0;
                        short subcapituloId24 = (short) 0;
                        short seccionId24 = (short) 0;
                        short apartadoId24 = (short) 0;
                        capituloId24 = 4;
                        subcapituloId24 = 6;
                        seccionId24 = 1;
                        apartadoId24 = 1;
                        planosLocFot= dao.getArchivosProyecto(capituloId24 , subcapituloId24, seccionId24, apartadoId24, folioNum, serialNum); 
                        //4,6,2,1
                        short capituloId25 = (short) 0;
                        short subcapituloId25 = (short) 0;
                        short seccionId25 = (short) 0;
                        short apartadoId25 = (short) 0;
                        capituloId25 = 4;
                        subcapituloId25 = 6;
                        seccionId25 = 2;
                        apartadoId25 = 1;
                        otrosAnex= dao.getArchivosProyecto(capituloId25 , subcapituloId25, seccionId25, apartadoId25, folioNum, serialNum); 
                    
                } 

            }
        } catch (Exception e) {
         
           // JOptionPane.showMessageDialog(null,  "tramSinatec:  " + e.getMessage() , "Error", JOptionPane.INFORMATION_MESSAGE);
        }      
    }
    
    

    /**
     * @return the estudio
     */
    public EstudioRiesgoProyecto getEstudio() {
        return estudio;
    }

    /**
     * @param estudio the estudio to set
     */
    public void setEstudio(EstudioRiesgoProyecto estudio) {
        this.estudio = estudio;
    }

    /**
     * @return the descSistTransp
     */
    public List<ArchivosProyecto> getDescSistTransp() {
        return descSistTransp;
    }

    /**
     * @param descSistTransp the descSistTransp to set
     */
    public void setDescSistTransp(List<ArchivosProyecto> descSistTransp) {
        this.descSistTransp = descSistTransp;
    }

    /**
     * @return the baseDiseno
     */
    public List<ArchivosProyecto> getBaseDiseno() {
        return baseDiseno;
    }

    /**
     * @param baseDiseno the baseDiseno to set
     */
    public void setBaseDiseno(List<ArchivosProyecto> baseDiseno) {
        this.baseDiseno = baseDiseno;
    }

    /**
     * @return the condOper
     */
    public List<ArchivosProyecto> getCondOper() {
        return condOper;
    }

    /**
     * @param condOper the condOper to set
     */
    public void setCondOper(List<ArchivosProyecto> condOper) {
        this.condOper = condOper;
    }

    /**
     * @return the pruebVerif
     */
    public List<ArchivosProyecto> getPruebVerif() {
        return pruebVerif;
    }

    /**
     * @param pruebVerif the pruebVerif to set
     */
    public void setPruebVerif(List<ArchivosProyecto> pruebVerif) {
        this.pruebVerif = pruebVerif;
    }

    /**
     * @return the procMedCont
     */
    public List<ArchivosProyecto> getProcMedCont() {
        return procMedCont;
    }

    /**
     * @param procMedCont the procMedCont to set
     */
    public void setProcMedCont(List<ArchivosProyecto> procMedCont) {
        this.procMedCont = procMedCont;
    }

    /**
     * @return the analEvalRiesg
     */
    public List<ArchivosProyecto> getAnalEvalRiesg() {
        return analEvalRiesg;
    }

    /**
     * @param analEvalRiesg the analEvalRiesg to set
     */
    public void setAnalEvalRiesg(List<ArchivosProyecto> analEvalRiesg) {
        this.analEvalRiesg = analEvalRiesg;
    }

    /**
     * @return the metodIdentJerq
     */
    public List<ArchivosProyecto> getMetodIdentJerq() {
        return metodIdentJerq;
    }

    /**
     * @param metodIdentJerq the metodIdentJerq to set
     */
    public void setMetodIdentJerq(List<ArchivosProyecto> metodIdentJerq) {
        this.metodIdentJerq = metodIdentJerq;
    }

    /**
     * @return the radPotAfect
     */
    public List<ArchivosProyecto> getRadPotAfect() {
        return radPotAfect;
    }

    /**
     * @param radPotAfect the radPotAfect to set
     */
    public void setRadPotAfect(List<ArchivosProyecto> radPotAfect) {
        this.radPotAfect = radPotAfect;
    }

    /**
     * @return the intRiesgo
     */
    public List<ArchivosProyecto> getIntRiesgo() {
        return intRiesgo;
    }

    /**
     * @param intRiesgo the intRiesgo to set
     */
    public void setIntRiesgo(List<ArchivosProyecto> intRiesgo) {
        this.intRiesgo = intRiesgo;
    }

    /**
     * @return the efectAreaInfl
     */
    public List<ArchivosProyecto> getEfectAreaInfl() {
        return efectAreaInfl;
    }

    /**
     * @param efectAreaInfl the efectAreaInfl to set
     */
    public void setEfectAreaInfl(List<ArchivosProyecto> efectAreaInfl) {
        this.efectAreaInfl = efectAreaInfl;
    }

    /**
     * @return the recomTecOper
     */
    public List<ArchivosProyecto> getRecomTecOper() {
        return recomTecOper;
    }

    /**
     * @param recomTecOper the recomTecOper to set
     */
    public void setRecomTecOper(List<ArchivosProyecto> recomTecOper) {
        this.recomTecOper = recomTecOper;
    }

    /**
     * @return the sistSeg
     */
    public List<ArchivosProyecto> getSistSeg() {
        return sistSeg;
    }

    /**
     * @param sistSeg the sistSeg to set
     */
    public void setSistSeg(List<ArchivosProyecto> sistSeg) {
        this.sistSeg = sistSeg;
    }

    /**
     * @return the medPrevent
     */
    public List<ArchivosProyecto> getMedPrevent() {
        return medPrevent;
    }

    /**
     * @param medPrevent the medPrevent to set
     */
    public void setMedPrevent(List<ArchivosProyecto> medPrevent) {
        this.medPrevent = medPrevent;
    }

    /**
     * @return the conclucERA
     */
    public List<ArchivosProyecto> getConclucERA() {
        return conclucERA;
    }

    /**
     * @param conclucERA the conclucERA to set
     */
    public void setConclucERA(List<ArchivosProyecto> conclucERA) {
        this.conclucERA = conclucERA;
    }

    /**
     * @return the resumenRA
     */
    public List<ArchivosProyecto> getResumenRA() {
        return resumenRA;
    }

    /**
     * @param resumenRA the resumenRA to set
     */
    public void setResumenRA(List<ArchivosProyecto> resumenRA) {
        this.resumenRA = resumenRA;
    }

    /**
     * @return the infoTec
     */
    public List<ArchivosProyecto> getInfoTec() {
        return infoTec;
    }

    /**
     * @param infoTec the infoTec to set
     */
    public void setInfoTec(List<ArchivosProyecto> infoTec) {
        this.infoTec = infoTec;
    }

    /**
     * @return the planosLocFot
     */
    public List<ArchivosProyecto> getPlanosLocFot() {
        return planosLocFot;
    }

    /**
     * @param planosLocFot the planosLocFot to set
     */
    public void setPlanosLocFot(List<ArchivosProyecto> planosLocFot) {
        this.planosLocFot = planosLocFot;
    }

    /**
     * @return the otrosAnex
     */
    public List<ArchivosProyecto> getOtrosAnex() {
        return otrosAnex;
    }

    /**
     * @param otrosAnex the otrosAnex to set
     */
    public void setOtrosAnex(List<ArchivosProyecto> otrosAnex) {
        this.otrosAnex = otrosAnex;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }
    
}
