/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.APDao;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.AnalisisPreliminarProyecto;
import mx.gob.semarnat.model.dgira_miae.CatDependenciaOficio;
import mx.gob.semarnat.model.dgira_miae.NotificacionGobiernoProyecto;
import mx.gob.semarnat.model.dgira_miae.NotificacionGobiernoProyectoPK;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;


/**
 *
 * @author Rengerden
 */
@ManagedBean(name = "APView")
@ViewScoped
public class APView implements Serializable {
    private static final Logger logger = Logger.getLogger(APView.class.getName());

    private final APDao dao = new APDao();
    private List<Object[]> analisisUno = new ArrayList();
    private List<Object[]> analisisDos = new ArrayList();
    private List<Object[]> analisisTres = new ArrayList();
    private List<Object[]> analisisCuatro = new ArrayList();
    private List<Object[]> analisisCinco = new ArrayList();
    private List<Object[]> analisisSeis = new ArrayList();
    private List<Object[]> analisisSiete = new ArrayList();
    private List<Object[]> analisisOcho = new ArrayList();
    private List<Object[]> analisisNueve = new ArrayList();
    private List<Object[]> analisisDiez = new ArrayList();
    private List<Object[]> analisisOnce = new ArrayList();
    private List<Object[]> analisisDoce = new ArrayList();
    private List<Object[]> analisisTrece = new ArrayList();
    private final String bitaProyecto;
    private List<WizardHelper> listaUno = new ArrayList();
    private List<WizardHelper> listaDos = new ArrayList();
    private List<WizardHelper> listaTres = new ArrayList();
    private List<WizardHelper> listaCuatro = new ArrayList();
    private List<WizardHelper> listaCinco = new ArrayList();
    private List<WizardHelper> listaSeis = new ArrayList();
    private List<WizardHelper> listaSiete = new ArrayList();
    private List<WizardHelper> listaOcho = new ArrayList();
    private List<WizardHelper> listaNueve = new ArrayList();
    private List<WizardHelper> listaDiez = new ArrayList();
    private List<WizardHelper> listaOnce = new ArrayList();
    private List<WizardHelper> listaDoce = new ArrayList();
    private List<WizardHelper> listaTrece = new ArrayList();
    @ManagedProperty ( value = "#{detalleView}")
    private DetalleProyectoView detalleView;
    //<editor-fold desc="Variables para las notificaciones" defaultstate="collapsed">
    private final DgiraMiaeDaoGeneral daoDGIRA = new DgiraMiaeDaoGeneral();
    private final List<CatDependenciaOficio> listaDependencias = null;
    //------------ 
    private NotificacionGobiernoProyecto notificacionSelect;
    private List<NotificacionGobiernoProyecto> notificaciones = new ArrayList();
    //</editor-fold>

    private List<Historial> HistorialCIS303;
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private Boolean deInfoAdic = false; //bandera que indica si un trámite ya vino de una suspención por info adicional
    private List<Historial> NoHistorialAT0022; 
    private List<Historial> NoHistorialCIS106;
    private List<Historial> NoHistorial200501;
    
    public APView() {
        
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        FacesContext fContext = FacesContext.getCurrentInstance();
        bitaProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        //JOptionPane.showMessageDialog(null,  "  bitaProyecto: " + bitaProyecto, "  Exito", JOptionPane.INFORMATION_MESSAGE);

        if (bitaProyecto != null) {
        //List<AnalisisPreliminarProyecto> criterios = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"});
            
            
            HistorialCIS303 = daoBitacora.numRegHistStatus(bitaProyecto,"CIS303");
            NoHistorialAT0022 = daoBitacora.numRegHistStatus(bitaProyecto,"AT0022");
            NoHistorialCIS106 = daoBitacora.numRegHistStatus(bitaProyecto,"CIS106"); 
            NoHistorial200501 = daoBitacora.numRegHistStatus(bitaProyecto,"200501");
            if ( !NoHistorialAT0022.isEmpty() || !NoHistorialCIS106.isEmpty() || !NoHistorial200501.isEmpty() || HistorialCIS303.size() == 1) //si el tamaño es igual a 1, indica que el trámite ya esta suspendido por info adicional
            { deInfoAdic = true; }
            else { deInfoAdic = false; }
            
            List<AnalisisPreliminarProyecto> criterios = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 1}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno1 = dao.analisisUno();
            llenado(uno1, 1, criterios);

            List<AnalisisPreliminarProyecto> criterios2 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 6}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno22 = dao.analisisDos();
            llenado(uno22, 2, criterios2);

            List<AnalisisPreliminarProyecto> criterios3 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 8}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno33 = dao.analisisTres();
            llenado(uno33, 3, criterios3);

            List<AnalisisPreliminarProyecto> criterios4 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 11}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno44 = dao.analisisCuatro();
            llenado(uno44, 4, criterios4);

            List<AnalisisPreliminarProyecto> criterios5 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 14}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno55 = dao.analisisCinco();
            llenado(uno55, 5, criterios5);

            List<AnalisisPreliminarProyecto> criterios6 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 16}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno66 = dao.analisisSeis();
            llenado(uno66, 6, criterios6);

            List<AnalisisPreliminarProyecto> criterios7 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 18}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno77 = dao.analisisSiete();
            llenado(uno77, 7, criterios7);

            List<AnalisisPreliminarProyecto> criterios8 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 19}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno88 = dao.analisisOcho();
            llenado(uno88, 8, criterios8);

            List<AnalisisPreliminarProyecto> criterios9 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 22}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno99 = dao.analisisNueve();
            llenado(uno99, 9, criterios9);

            List<AnalisisPreliminarProyecto> criterios10 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 25}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno10 = dao.analisisDiez();
            llenado(uno10, 10, criterios10);

            List<AnalisisPreliminarProyecto> criterios11 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 26}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno11 = dao.analisisOnce();
            llenado(uno11, 11, criterios11);
            
            List<AnalisisPreliminarProyecto> criterios13 = (List<AnalisisPreliminarProyecto>) dao.lista_namedQuery("AnalisisPreliminarProyecto.findByBitacoraProyectoId", new Object[]{bitaProyecto, (short) 37}, new String[]{"bitacoraProyecto", "catAnalisisId"});
            List<Object[]> uno13 = dao.analisisTrece();
            llenado(uno13, 13, criterios13);
        }
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }

    public void guarda1() {
        guardaAnalisis(1);
    }

    public void guarda2() {
        guardaAnalisis(2);
    }

    public void guarda3() {
        guardaAnalisis(3);
    }

    public void guarda4() {
        guardaAnalisis(4);
    }

    public void guarda5() {
        guardaAnalisis(5);
    }

    public void guarda6() {
        guardaAnalisis(6);
    }

    public void guarda7() {
        guardaAnalisis(7);
    }

    public void guarda8() {
        guardaAnalisis(8);
    }

    public void guarda9() {
        guardaAnalisis(9);
    }

    public void guarda10() {
        guardaAnalisis(10);
    }

    public void guarda11() {
        guardaAnalisis(11);
    }

    public void guarda12() {
        guardaAnalisis(12);
    }

    public void guarda13() {
        guardaAnalisis(13);
    }

    public void guardaAnalisis(Integer numLista) {
    	RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        FacesContext ctxt = FacesContext.getCurrentInstance();
        Integer banderaLLeno = 0;
        AnalisisPreliminarProyecto cvp;
        List<Object[]> c;
        Integer id;
        short id2 = 0;
        Integer ok = 0;
        Integer vacios = 0;
        List<WizardHelper> lista = new ArrayList();

        switch (numLista) {
            case 1:
                lista = listaUno;
                break;
            case 2:
                lista = listaDos;
                break;
            case 3:
                lista = listaTres;
                break;
            case 4:
                lista = listaCuatro;
                break;
            case 5:
                lista = listaCinco;
                break;
            case 6:
                lista = listaSeis;
                break;
            case 7:
                lista = listaSiete;
                break;
            case 8:
                lista = listaOcho;
                break;
            case 9:
                lista = listaNueve;
                break;
            case 10:
                lista = listaDiez;
                break;
            case 11:
                lista = listaOnce;
                break;
            case 12:
                lista = listaDoce;
                break;
            case 13:
                lista = listaTrece;
                break;
        }

        for (WizardHelper helper : lista) {
            id = new Integer(helper.id.toString());
            id2 = id.shortValue();
            cvp = (AnalisisPreliminarProyecto) dao.analisisConsulta(bitaProyecto, id2);

            if (cvp.getAnalisisPreliminarProyectoPK() != null) {
                banderaLLeno = 1;
            } //editar
            else {
                banderaLLeno = 0;
            } //nuevo

            if (banderaLLeno == 0) {
                cvp = new AnalisisPreliminarProyecto(bitaProyecto, id.shortValue());
            }
            if(numLista != 13)
            {
                if (helper.seleccion.equals("S")) {
                    cvp.setAnalisisRespuesta(helper.seleccion);
                } else {
                    cvp.setAnalisisRespuesta(helper.seleccion);
                }
            }
            cvp.setAnalisisObservacion(helper.observacion);
            
            if(numLista != 13)
            {
                if (helper.seleccion.isEmpty()) //si no realizado la seleccion de alguna respuesta, no le permitira guardar
                {
                    vacios = vacios + 1;
                } else {
                    try {
                        if (banderaLLeno == 0) {
                            dao.agrega(cvp);
                        }
                        if (banderaLLeno == 1) {
                            dao.modifica(cvp);
                        }

                    } catch (Exception e) {
                        ok = ok + 1;
                    }
                }
            }
            else
            {
               try {
                            if (banderaLLeno == 0) {
                                dao.agrega(cvp);
                            }
                            if (banderaLLeno == 1) {
                                dao.modifica(cvp);
                            }

                        } catch (Exception e) {
                            ok = ok + 1;
                        }     
            }
        }
        
        if (ok == 0 && vacios == 0) {
            ctxt.addMessage("growl", new FacesMessage("Datos Análisis preliminar guardados exitosamente."));
        } else {
            ctxt.addMessage("growl", new FacesMessage("No se ha guradado la información, debe seleccionar la respuesta para cada pregunta"));
        }
        detalleView.analisisPreCompleto();
        if(detalleView.getActiveTab() != 0)
        	reqcontEnv.execute("PF('myTabView').select("+ detalleView.getActiveTab() +");");
    }

    //<editor-fold desc="metodos para la tabla de las notificaciones" defaultstate="collpased">
    public void agregarNotificacion() {
        notificaciones.add(new NotificacionGobiernoProyecto());
    }

    public void quitarNotificacion() {
        notificaciones.remove(notificacionSelect);
    }

    public void guardarNotificacion() {
        DgiraMiaeDaoGeneral dao = new DgiraMiaeDaoGeneral();
        Short s = (short) (dao.MAX(NotificacionGobiernoProyecto.class, "notificacionGobiernoProyectoPK.notificaciongId") + 1);
        for (NotificacionGobiernoProyecto ngp : (List<NotificacionGobiernoProyecto>) daoDGIRA.lista_namedQuery("NotificacionGobiernoProyecto.findByBitacoraProyecto", new Object[]{bitaProyecto}, new String[]{"bitacoraProyecto"})) {
            daoDGIRA.elimina(NotificacionGobiernoProyecto.class, ngp.getNotificacionGobiernoProyectoPK());
        }
        for (NotificacionGobiernoProyecto ngp : notificaciones) {
            ngp.setNotificacionGobiernoProyectoPK(new NotificacionGobiernoProyectoPK(bitaProyecto, s.toString()));
            daoDGIRA.agrega(ngp);
        }
    }//</editor-fold>

    //<editor-fold desc="Getters & Setters" defaultstate="collapsed">
    public List<Object[]> getAnalisisUno() {
        return analisisUno;
    }

    public void setAnalisisUno(List<Object[]> analisisUno) {
        this.analisisUno = analisisUno;
    }

    public List<Object[]> getAnalisisDos() {
        return analisisDos;
    }

    public void setAnalisisDos(List<Object[]> analisisDos) {
        this.analisisDos = analisisDos;
    }

    public List<Object[]> getAnalisisTres() {
        return analisisTres;
    }

    public void setAnalisisTres(List<Object[]> analisisTres) {
        this.analisisTres = analisisTres;
    }

    public List<Object[]> getAnalisisCuatro() {
        return analisisCuatro;
    }

    public void setAnalisisCuatro(List<Object[]> analisisCuatro) {
        this.analisisCuatro = analisisCuatro;
    }

    public List<Object[]> getAnalisisCinco() {
        return analisisCinco;
    }

    public void setAnalisisCinco(List<Object[]> analisisCinco) {
        this.analisisCinco = analisisCinco;
    }

    public List<Object[]> getAnalisisSeis() {
        return analisisSeis;
    }

    public void setAnalisisSeis(List<Object[]> analisisSeis) {
        this.analisisSeis = analisisSeis;
    }

    public List<Object[]> getAnalisisSiete() {
        return analisisSiete;
    }

    public void setAnalisisSiete(List<Object[]> analisisSiete) {
        this.analisisSiete = analisisSiete;
    }

    public List<Object[]> getAnalisisOcho() {
        return analisisOcho;
    }

    public void setAnalisisOcho(List<Object[]> analisisOcho) {
        this.analisisOcho = analisisOcho;
    }

    public List<Object[]> getAnalisisNueve() {
        return analisisNueve;
    }

    public void setAnalisisNueve(List<Object[]> analisisNueve) {
        this.analisisNueve = analisisNueve;
    }

    public List<Object[]> getAnalisisDiez() {
        return analisisDiez;
    }

    public void setAnalisisDiez(List<Object[]> analisisDiez) {
        this.analisisDiez = analisisDiez;
    }

    public List<Object[]> getAnalisisOnce() {
        return analisisOnce;
    }

    public void setAnalisisOnce(List<Object[]> analisisOnce) {
        this.analisisOnce = analisisOnce;
    }

    public List<Object[]> getAnalisisDoce() {
        return analisisDoce;
    }

    public void setAnalisisDoce(List<Object[]> analisisDoce) {
        this.analisisDoce = analisisDoce;
    }

    public List<Object[]> getAnalisisTrece() {
        return analisisTrece;
    }

    public void setAnalisisTrece(List<Object[]> analisisTrece) {
        this.analisisTrece = analisisTrece;
    }

    //---------cambios
    public List<CatDependenciaOficio> getListaDependencias() {
        return listaDependencias;
    }

    public NotificacionGobiernoProyecto getNotificacionSelect() {
        return notificacionSelect;
    }

    public void setNotificacionSelect(NotificacionGobiernoProyecto notificacionSelect) {
        this.notificacionSelect = notificacionSelect;
    }

    public List<NotificacionGobiernoProyecto> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(List<NotificacionGobiernoProyecto> notificaciones) {
        this.notificaciones = notificaciones;
    }//</editor-fold>

    /**
     * @return the listaUno
     */
    public List<WizardHelper> getListaUno() {
        return listaUno;
    }

    /**
     * @param listaUno the listaUno to set
     */
    public void setListaUno(List<WizardHelper> listaUno) {
        this.listaUno = listaUno;
    }

    /**
     * @return the listaDos
     */
    public List<WizardHelper> getListaDos() {
        return listaDos;
    }

    /**
     * @param listaDos the listaDos to set
     */
    public void setListaDos(List<WizardHelper> listaDos) {
        this.listaDos = listaDos;
    }

    /**
     * @return the listaTres
     */
    public List<WizardHelper> getListaTres() {
        return listaTres;
    }

    /**
     * @param listaTres the listaTres to set
     */
    public void setListaTres(List<WizardHelper> listaTres) {
        this.listaTres = listaTres;
    }

    /**
     * @return the listaCuatro
     */
    public List<WizardHelper> getListaCuatro() {
        return listaCuatro;
    }

    /**
     * @param listaCuatro the listaCuatro to set
     */
    public void setListaCuatro(List<WizardHelper> listaCuatro) {
        this.listaCuatro = listaCuatro;
    }

    /**
     * @return the listaCinco
     */
    public List<WizardHelper> getListaCinco() {
        return listaCinco;
    }

    /**
     * @param listaCinco the listaCinco to set
     */
    public void setListaCinco(List<WizardHelper> listaCinco) {
        this.listaCinco = listaCinco;
    }

    /**
     * @return the listaSeis
     */
    public List<WizardHelper> getListaSeis() {
        return listaSeis;
    }

    /**
     * @param listaSeis the listaSeis to set
     */
    public void setListaSeis(List<WizardHelper> listaSeis) {
        this.listaSeis = listaSeis;
    }

    /**
     * @return the listaSiete
     */
    public List<WizardHelper> getListaSiete() {
        return listaSiete;
    }

    /**
     * @param listaSiete the listaSiete to set
     */
    public void setListaSiete(List<WizardHelper> listaSiete) {
        this.listaSiete = listaSiete;
    }

    /**
     * @return the listaOcho
     */
    public List<WizardHelper> getListaOcho() {
        return listaOcho;
    }

    /**
     * @param listaOcho the listaOcho to set
     */
    public void setListaOcho(List<WizardHelper> listaOcho) {
        this.listaOcho = listaOcho;
    }

    /**
     * @return the listaNueve
     */
    public List<WizardHelper> getListaNueve() {
        return listaNueve;
    }

    /**
     * @param listaNueve the listaNueve to set
     */
    public void setListaNueve(List<WizardHelper> listaNueve) {
        this.listaNueve = listaNueve;
    }

    /**
     * @return the listaDiez
     */
    public List<WizardHelper> getListaDiez() {
        return listaDiez;
    }

    /**
     * @param listaDiez the listaDiez to set
     */
    public void setListaDiez(List<WizardHelper> listaDiez) {
        this.listaDiez = listaDiez;
    }

    /**
     * @return the listaOnce
     */
    public List<WizardHelper> getListaOnce() {
        return listaOnce;
    }

    /**
     * @param listaOnce the listaOnce to set
     */
    public void setListaOnce(List<WizardHelper> listaOnce) {
        this.listaOnce = listaOnce;
    }

    /**
     * @return the listaDoce
     */
    public List<WizardHelper> getListaDoce() {
        return listaDoce;
    }

    /**
     * @param listaDoce the listaDoce to set
     */
    public void setListaDoce(List<WizardHelper> listaDoce) {
        this.listaDoce = listaDoce;
    }

    /**
     * @return the listaTrece
     */
    public List<WizardHelper> getListaTrece() {
        return listaTrece;
    }

    /**
     * @param listaTrece the listaTrece to set
     */
    public void setListaTrece(List<WizardHelper> listaTrece) {
        this.listaTrece = listaTrece;
    }

    /**
     * @return the deInfoAdic
     */
    public Boolean getDeInfoAdic() {
        return deInfoAdic;
    }

    /**
     * @param deInfoAdic the deInfoAdic to set
     */
    public void setDeInfoAdic(Boolean deInfoAdic) {
        this.deInfoAdic = deInfoAdic;
    }


    public class WizardHelper implements Serializable {

        private Integer id;
        private String descripcion;
        private String seleccion;
        private String observacion;

        public WizardHelper(Integer id, String descripcion) {
            this.id = id;
            this.descripcion = descripcion;
        }

        public WizardHelper(Integer id, String descripcion, String seleccion, String observ) { //, Integer valorEval
            this.id = id;
            this.descripcion = descripcion;
            this.observacion = observ;
            this.seleccion = seleccion;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getSeleccion() {
            return seleccion;
        }

        public void setSeleccion(String seleccion) {
            this.seleccion = seleccion;
        }

        /**
         * @return the observacion
         */
        public String getObservacion() {
            return observacion;
        }

        /**
         * @param observacion the observacion to set
         */
        public void setObservacion(String observacion) {
            this.observacion = observacion;
        }
    }

    private void llenado(List<Object[]> uno22, Integer cap, List<AnalisisPreliminarProyecto> criterios) {
        String desc = "";
        int indice = 1;
        String selec = "";
        String obsv = "";
        List<Object[]> c;

        for (int i2 = 0; i2 <= uno22.size() - 1; i2++) {
            System.out.println(uno22.get(i2)[0]);
            if (criterios.isEmpty()) {

                indice = new Integer(uno22.get(i2)[0].toString());
                desc = uno22.get(i2)[1].toString();
                selec = "";
                obsv = "";
                switch (cap) {
                    case 1:
                        listaUno.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 2:
                        listaDos.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 3:
                        listaTres.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 4:
                        listaCuatro.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 5:
                        listaCinco.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 6:
                        listaSeis.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 7:
                        listaSiete.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 8:
                        listaOcho.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 9:
                        listaNueve.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 10:
                        listaDiez.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 11:
                        listaOnce.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 12:
                        listaDoce.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                    case 13:
                        listaTrece.add(new WizardHelper(indice, desc, selec, obsv));
                        break;
                }

            } else {
                indice = new Integer(uno22.get(i2)[0].toString());
                desc = uno22.get(i2)[1].toString();
                selec = "";
                obsv = "";

                c = dao.critVal(bitaProyecto, (short) indice);
                if (c.size() > 0) {
                    for (Object[] x : c) {
                        if (x[3] == null) {
                            obsv = "";
                        } else {
                            obsv = x[3].toString();
                        }
                        if(cap != 13)
                        {
                            if (x[2].toString() == null) {
                                selec = "";
                            } else {
                                selec = x[2].toString();
                            }
                        }
                    }
                    switch (cap) {
                        case 1:
                            listaUno.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 2:
                            listaDos.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 3:
                            listaTres.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 4:
                            listaCuatro.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 5:
                            listaCinco.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 6:
                            listaSeis.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 7:
                            listaSiete.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 8:
                            listaOcho.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 9:
                            listaNueve.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 10:
                            listaDiez.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 11:
                            listaOnce.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 12:
                            listaDoce.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                        case 13:
                            listaTrece.add(new WizardHelper(indice, desc, selec, obsv));
                            break;
                    }
                }
            }
        }
    }

	public DetalleProyectoView getDetalleView() {
		return detalleView;
	}

	public void setDetalleView(DetalleProyectoView detalleView) {
		this.detalleView = detalleView;
	}

}
