package mx.gob.semarnat.view.visorMia;

import mx.gob.semarnat.model.dgira_miae.ReiaObras;

public class UsoReiaNodo {
	
	 private static final long serialVersionUID = 1L;
	    
	    private String folio;	        
	    private ReiaObras obra;
	    private Long clave;	        	    
	    private String principal;	    
	    private String exceptuada;
	    private String tipoRow;
		
	    public UsoReiaNodo() {
			super();
			// TODO Auto-generated constructor stub
		}

		public UsoReiaNodo(String folio, ReiaObras obra, Long clave, String principal, String exceptuada, String tipoRow) {
			this.folio = folio;
			this.obra = obra;
			this.clave = clave;
			this.principal = principal;
			this.exceptuada = exceptuada;
			this.tipoRow = tipoRow;
		}

		/**
		 * @return the folio
		 */
		public String getFolio() {
			return folio;
		}

		/**
		 * @param folio the folio to set
		 */
		public void setFolio(String folio) {
			this.folio = folio;
		}

		/**
		 * @return the obra
		 */
		public ReiaObras getObra() {
			return obra;
		}

		/**
		 * @param obra the obra to set
		 */
		public void setObra(ReiaObras obra) {
			this.obra = obra;
		}

		/**
		 * @return the clave
		 */
		public Long getClave() {
			return clave;
		}

		/**
		 * @param clave the clave to set
		 */
		public void setClave(Long clave) {
			this.clave = clave;
		}

		/**
		 * @return the principal
		 */
		public String getPrincipal() {
			return principal;
		}

		/**
		 * @param principal the principal to set
		 */
		public void setPrincipal(String principal) {
			this.principal = principal;
		}

		/**
		 * @return the exceptuada
		 */
		public String getExceptuada() {
			return exceptuada;
		}

		/**
		 * @param exceptuada the exceptuada to set
		 */
		public void setExceptuada(String exceptuada) {
			this.exceptuada = exceptuada;
		}

		/**
		 * @return the tipoRow
		 */
		public String getTipoRow() {
			return tipoRow;
		}

		/**
		 * @param tipoRow the tipoRow to set
		 */
		public void setTipoRow(String tipoRow) {
			this.tipoRow = tipoRow;
		}

}
