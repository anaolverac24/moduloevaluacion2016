/**
 * 
 */
package mx.gob.semarnat.view.visorMia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Cesar
 *
 */
@Entity
@Table(name="VEG_TIPO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VegTipo.findByName", query = "SELECT v FROM VegTipo v WHERE v.vegNombre = :vegName")})
public class VegTipo implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 5147510983455309084L;

	@Id
	@GeneratedValue(generator="InvSeq") 
    @SequenceGenerator(name="InvSeq",sequenceName="SEQ_ID_VEG_TIPO")  
	@Basic(optional = false)
	@Column(name="ID_VEG_TIPO")
	private Integer idVegTipo;
	
	@Column(name="VEG_NOMBRE")
	private String vegNombre;
	
	@JoinColumn(name="ID_VEG_GRUPO", referencedColumnName="ID_VEG_GRUPO")
	@ManyToOne
	private VegGrupo vegGrupo;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "VEG_TIPO_FASE", joinColumns = { 
	@JoinColumn(name = "ID_VEG_TIPO", nullable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "ID_VEG_FASE", 
			nullable = false, updatable = false) })
	private List<VegFase> listaVegFases = new ArrayList<VegFase>();

	/**
	 * @return the idVegTipo
	 */
	public Integer getIdVegTipo() {
		return idVegTipo;
	}

	/**
	 * @param idVegTipo the idVegTipo to set
	 */
	public void setIdVegTipo(Integer idVegTipo) {
		this.idVegTipo = idVegTipo;
	}

	/**
	 * @return the vegNombre
	 */
	public String getVegNombre() {
		return vegNombre;
	}

	/**
	 * @param vegNombre the vegNombre to set
	 */
	public void setVegNombre(String vegNombre) {
		this.vegNombre = vegNombre;
	}

	/**
	 * @return the vegGrupo
	 */
	public VegGrupo getVegGrupo() {
		return vegGrupo;
	}

	/**
	 * @param vegGrupo the vegGrupo to set
	 */
	public void setVegGrupo(VegGrupo vegGrupo) {
		this.vegGrupo = vegGrupo;
	}

	/**
	 * @return the listaVegFases
	 */
	public List<VegFase> getListaVegFases() {
		return listaVegFases;
	}

	/**
	 * @param listaVegFases the listaVegFases to set
	 */
	public void setListaVegFases(List<VegFase> listaVegFases) {
		this.listaVegFases = listaVegFases;
	}
	
}
