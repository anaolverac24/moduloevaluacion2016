package mx.gob.semarnat.view.visorMia;

import mx.gob.semarnat.model.dgira_miae.ReiaCondiciones;
import mx.gob.semarnat.model.dgira_miae.ReiaObras;

public class NodeReiaObrasCondiciones {

	private static final long serialVersionUID = 1L;
	
    private Long idObCon;
    private String compuerta;
    private String idCondicion;
    private Long idCondicion2;
    private ReiaObras idObra;
    private String tipoRow;
    
    private String folio;	        
    private ReiaObras obra;
    private Long clave;	        	    
    private String principal;	    
    private String exceptuada;
	
    public NodeReiaObrasCondiciones() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NodeReiaObrasCondiciones(String idCondicion, Long idCondicion2, String tipoRow) {
		this.idCondicion = idCondicion;
		this.idCondicion2 = idCondicion2;
		this.tipoRow = tipoRow;
	}

	/**
	 * @return the idObCon
	 */
	public Long getIdObCon() {
		return idObCon;
	}

	/**
	 * @param idObCon the idObCon to set
	 */
	public void setIdObCon(Long idObCon) {
		this.idObCon = idObCon;
	}

	/**
	 * @return the compuerta
	 */
	public String getCompuerta() {
		return compuerta;
	}

	/**
	 * @param compuerta the compuerta to set
	 */
	public void setCompuerta(String compuerta) {
		this.compuerta = compuerta;
	}

	/**
	 * @return the idCondicion
	 */
	public String getIdCondicion() {
		return idCondicion;
	}

	/**
	 * @param idCondicion the idCondicion to set
	 */
	public void setIdCondicion(String idCondicion) {
		this.idCondicion = idCondicion;
	}

	/**
	 * @return the idObra
	 */
	public ReiaObras getIdObra() {
		return idObra;
	}

	/**
	 * @param idObra the idObra to set
	 */
	public void setIdObra(ReiaObras idObra) {
		this.idObra = idObra;
	}

	/**
	 * @return the tipoRow
	 */
	public String getTipoRow() {
		return tipoRow;
	}

	/**
	 * @param tipoRow the tipoRow to set
	 */
	public void setTipoRow(String tipoRow) {
		this.tipoRow = tipoRow;
	}

	/**
	 * @return the idCondicion2
	 */
	public Long getIdCondicion2() {
		return idCondicion2;
	}

	/**
	 * @param idCondicion2 the idCondicion2 to set
	 */
	public void setIdCondicion2(Long idCondicion2) {
		this.idCondicion2 = idCondicion2;
	}

	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * @return the obra
	 */
	public ReiaObras getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public void setObra(ReiaObras obra) {
		this.obra = obra;
	}

	/**
	 * @return the clave
	 */
	public Long getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(Long clave) {
		this.clave = clave;
	}

	/**
	 * @return the principal
	 */
	public String getPrincipal() {
		return principal;
	}

	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	/**
	 * @return the exceptuada
	 */
	public String getExceptuada() {
		return exceptuada;
	}

	/**
	 * @param exceptuada the exceptuada to set
	 */
	public void setExceptuada(String exceptuada) {
		this.exceptuada = exceptuada;
	}

}
