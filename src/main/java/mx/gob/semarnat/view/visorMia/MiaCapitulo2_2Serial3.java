/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorMia;

import java.util.ArrayList;
import java.util.List;


import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.AccidentesProyecto;
import mx.gob.semarnat.model.dgira_miae.ActividadEtapa;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.ContaminanteProyecto;
import mx.gob.semarnat.model.dgira_miae.EtapaProyecto;
import mx.gob.semarnat.model.dgira_miae.InfobioProyecto;
import mx.gob.semarnat.model.dgira_miae.InsumosProyecto;
import mx.gob.semarnat.model.dgira_miae.PrediocolinProy;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.SueloVegetacionProyecto;
import mx.gob.semarnat.model.dgira_miae.UsoSueloVeget;
import mx.gob.semarnat.utils.GenericConstants;

import org.primefaces.model.TreeNode;


/**
 *
 */
@ManagedBean(name = "miaCap2_2Serial3")
@ViewScoped
public class MiaCapitulo2_2Serial3 {

    private final DgiraMiaeDaoGeneral dao = new DgiraMiaeDaoGeneral();
    private final VisorDao daoVisor = new VisorDao();

    private Proyecto proyecto = new Proyecto();
    private String bitacora;
    private List<UsoSueloVeget> dimensionesUso = new ArrayList();
    private List<PrediocolinProy> predCol = new ArrayList();
    private List<Object> predCol1 = new ArrayList<>();
    //Duracion
    private EtapaProyecto etapaPreparacion;
    private EtapaProyecto etapaConstruccion;
    private EtapaProyecto etapaOperacion;
    private EtapaProyecto etapaAbandono;
     // Etapas
    private List<ActividadEtapa> actividadesPrepara = new ArrayList<ActividadEtapa>();
    private List<ActividadEtapa> actividadesConstru = new ArrayList<ActividadEtapa>();
    private List<ActividadEtapa> actividadesOperaci = new ArrayList<ActividadEtapa>();
    private List<ActividadEtapa> actividadesAbandon = new ArrayList<ActividadEtapa>();
    
    //ETAPAS NUEVAS LISTAS
    private List<Object> preparaList = new ArrayList<Object>();
    private List<Object> construList = new ArrayList<Object>();
    private List<Object> operaciList = new ArrayList<Object>();
    private List<Object> abandonList = new ArrayList<Object>();
    
    private Boolean verPreparacion = false;
    private Boolean verConstruccion = false;
    private Boolean verOperacion = false;
    private Boolean verAbandono = false;
    private Boolean verMsgEtapa = false;
    
    private List<AnexosProyecto> anexosProyectoEtapa1 = new ArrayList();
    private List<AnexosProyecto> anexosProyectoEtapa2 = new ArrayList();
    private List<AnexosProyecto> anexosProyectoEtapa3 = new ArrayList();
    private List<AnexosProyecto> anexosProyectoEtapa4 = new ArrayList();
    private String vseccion = "";
    
    //Listas para extraer los archivos anexos de etapa
    private List<ArchivosProyecto> archivos1 = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivos2 = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivos3 = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivos4 = new ArrayList<ArchivosProyecto>();
    
    //Listas de archivos de Duracion del Proyecto - Programa General de Trabajo
    private List<ArchivosProyecto> archivosDuracionProyecto = new ArrayList<ArchivosProyecto>();
    //Listas de archivos de Descripcion de Obras Principales del Proyecto
    private List<ArchivosProyecto> archivosDescObrasPrinProy = new ArrayList<ArchivosProyecto>();
    
    //insumos
    private boolean isInsumos;
    private List<InsumosProyecto> insumos = new ArrayList();
    //sustancias
    private List<Object[]> proyectoSustancia = new ArrayList<Object[]>();
    //explosivos
    private boolean explosivos;
    private List<ActividadEtapa> explosivosList = new ArrayList();
    private List<Object> listExplo = new ArrayList<Object>();
    
    private boolean descObras;
    private boolean infoBioRender;
    private boolean accidentesAmb;
    
    //VALIABLES PARA LA TREETABLE
    
    private TreeNode rootSuelos;
    private TablaNodo tablaNodo = new TablaNodo();
    private List<SueloVegetacionProyecto> sueloVegetacion = new ArrayList<SueloVegetacionProyecto>();
    
    
    //contaminantes
    private List<ContaminanteProyecto> contaminanteList = new ArrayList();
    //Caracteristicas particulares
    // private List<CaracPartProy> caracteristicas = new ArrayList<CaracPartProy>();
    private int vistaVersion;
    
    // Informacion biotecnologica de las especies a cultivar
    private List<InfobioProyecto> infoBio = new ArrayList();
    
    // Accidentes
    private List<AccidentesProyecto> accidenteList = new ArrayList<AccidentesProyecto>();

    @PostConstruct
    public void init() {
        
        try {
            sueloVegetacion = dao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
            rootSuelos = tablaNodo.CargaUsos(sueloVegetacion);
            System.out.println("System para tree");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        infoBio = dao.getInfoBios(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        accidenteList = dao.getAccidentes(proyecto.getProyectoPK().getFolioProyecto(), (short)3);   
    }
    
    public MiaCapitulo2_2Serial3() {
        //Obtiene numero de bitacora desde url
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        bitacora = ec.getRequestParameterMap().get("bitaNum");
        vistaVersion = (ec.getRequestParameterMap().get("vistaVersion") != null) ? 
                Integer.parseInt((String)ec.getRequestParameterMap().get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;        
        if (bitacora != null) {
            if (!dao.lista_namedQuery("ProyectoD.findByBitacoraProyecto", new Object[]{bitacora}, new String[]{"bitacoraProyecto"}).isEmpty()) {
                //proyecto = (Proyecto) dao.lista_namedQuery("ProyectoD.findByBitacoraProyecto", new Object[]{bitacora}, new String[]{"bitacoraProyecto"}).get(0);
            
//            	switch (vistaVersion) {
//                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
//                    proyecto = daoVisor.obtieneProyectoVersionAnterior(bitacora);
//                    break;
//                    
//                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
//                    proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);
//                    break;
//                    
//                case GenericConstants.VISTA_EVALUADOR_1ER_INGRESO:
//                case GenericConstants.VISTA_EVALUADOR_INFO_ADICIONAL:
//                    //Se obtiene la información del proyecto de la versión anterior del promovente (1, 3, ...)
//                    if (vistaVersion == GenericConstants.VISTA_EVALUADOR_1ER_INGRESO) {//Si versión 2
//                        proyecto = daoVisor.obtieneProyectoVersionAnterior(bitacora); //versión 1
//                    } else {//Si versión 4
//                        proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);//versión 3
//                    }
//
//                    proyecto.getProyectoPK().setSerialProyecto((short)vistaVersion);
//
//                    break;
//                    
//                    
//            } 
            	proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);
            	//proyecto.getProyectoPK().setSerialProyecto((short)vistaVersion);
            	
                if (proyecto != null) {
                    
                    //carga predios colindantes
                    predCol = (List<PrediocolinProy>) dao.lista_namedQuery("PrediocolinProy.findByFolioSerial",
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                            new String[]{"folio", "serial"});
                   
                    
                    //Lista de consulta para los datos
                    
                    predCol1 = dao.Predios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    
                    //</editor-fold>
                    etapaPreparacion = cargaEtapaProyecto((short) 1);
                    etapaConstruccion = cargaEtapaProyecto((short) 2);
                    etapaOperacion = cargaEtapaProyecto((short) 3);
                    etapaAbandono = cargaEtapaProyecto((short) 4);
                    
                    if(proyecto.getNsub() == 4 || proyecto.getNsub() == 6)
                    { vseccion = "5"; }
                    else
                    { vseccion = "4"; }
                    //2,2,#{capitulo2View.vseccion},1,
                    short capituloId3 = (short) 0;
                    short subcapituloId3 = (short) 0;
                    short seccionId3 = (short) 0;
                    short apartadoId3 = (short) 0; 
                    capituloId3 = 2;
                    subcapituloId3 = 2;
                    seccionId3 = Short.parseShort(vseccion);
                    apartadoId3 = 1;
                    anexosProyectoEtapa1 = dao.getAnexos(capituloId3, subcapituloId3, seccionId3, apartadoId3, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 

                    //2,2,#{capitulo2View.vseccion},2,
                    short capituloId4 = (short) 0;
                    short subcapituloId4 = (short) 0;
                    short seccionId4 = (short) 0;
                    short apartadoId4 = (short) 0; 
                    capituloId4 = 2;
                    subcapituloId4 = 2;
                    seccionId4 = Short.parseShort(vseccion);
                    apartadoId4 = 2;
                    anexosProyectoEtapa2 = dao.getAnexos(capituloId4, subcapituloId4, seccionId4, apartadoId4, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 

                    //2,2,#{capitulo2View.vseccion},3,
                    short capituloId5 = (short) 0;
                    short subcapituloId5 = (short) 0;
                    short seccionId5 = (short) 0;
                    short apartadoId5 = (short) 0; 
                    capituloId5 = 2;
                    subcapituloId5 = 2;
                    seccionId5 = Short.parseShort(vseccion);
                    apartadoId5 = 3;
                    anexosProyectoEtapa3 = dao.getAnexos(capituloId5, subcapituloId5, seccionId5, apartadoId5, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 

                    //2,2,#{capitulo2View.vseccion},4,
                    short capituloId6 = (short) 0;
                    short subcapituloId6 = (short) 0;
                    short seccionId6 = (short) 0;
                    short apartadoId6 = (short) 0; 
                    capituloId6 = 2;
                    subcapituloId6 = 2;
                    seccionId6 = Short.parseShort(vseccion);
                    apartadoId6 = 4;
                    anexosProyectoEtapa4 = dao.getAnexos(capituloId6, subcapituloId6, seccionId6, apartadoId6, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 
                    
                    actividadesPrepara = dao.getActividadesEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
                    actividadesConstru = dao.getActividadesEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 2);
                    actividadesOperaci = dao.getActividadesEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 3);
                    actividadesAbandon = dao.getActividadesEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 4);                    
                    //Listas modificadas de consulta
                    
                    preparaList = dao.actividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
                    construList = dao.actividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 2);
                    operaciList = dao.actividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 3);
                    abandonList = dao.actividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 4);                    
                    //Listas modificadas para consultar los archivos anexos
                    short subsector = proyecto.getNsub(), seccionEtapa = 0;

                    seccionEtapa = (subsector == 4 || subsector == 6) ? (short)5 : (short)4;                    
                    archivos1 = dao.archivosProyecto((short)2, (short)2, seccionEtapa, (short) 1, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    archivos2 = dao.archivosProyecto((short)2, (short)2, seccionEtapa, (short) 2, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    archivos3 = dao.archivosProyecto((short)2, (short)2, seccionEtapa, (short) 3, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    archivos4 = dao.archivosProyecto((short)2, (short)2, seccionEtapa, (short) 4, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    
                    verEtapas();
                    
                    System.out.println("Subsector: " + proyecto.getNsub().toString());
                    System.out.println("Carga de constructor de pantalla para tree table ");
                    
                    short subsector2 = proyecto.getNsub(), seccionId = 0;
                    
                    if(subsector2 == 6){
                    	descObras = true;
                    }
                    
                    if(subsector2 == 4){
                    	infoBioRender = true;
                    }
                    
                    if(subsector2 == 3){
                    	accidentesAmb = true;
                    }
                    
                    if(subsector2 == 6){
                    	seccionId = 4;
                    } else{
                    	seccionId = 3;
                    }
                    		
                    archivosDuracionProyecto = dao.archivosProyecto((short) 2, (short) 2, seccionId, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    
                    archivosDescObrasPrinProy = dao.archivosProyecto((short) 2, (short) 2, (short) 3, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Otros Insumos">
                    isInsumos = proyecto.getNsub().equals(6) || proyecto.getNsub().equals(7);
                    if (isInsumos) {
                        insumos = (List<InsumosProyecto>) dao.lista_namedQuery("InsumosProyecto.findByFolioSerial", new Object[]{proyecto.getProyectoPK().getFolioProyecto(), (short)3},
                                new String[]{"folio", "serial"});//.getInsumos(folioProyecto, serialProyecto);
                    }//</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Sustancias">
                    
                    proyectoSustancia = dao.sustanciaProy(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    //JOptionPane.showMessageDialog(null,  "tramSinatec:  " + proyectoSustancia.size() , "Error", JOptionPane.INFORMATION_MESSAGE);
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Explosivos">
                    explosivos = proyecto.getNsub() != 2 && proyecto.getNsub() != 4 && proyecto.getNsub() != 6;
                    if (explosivos) {
                        explosivosList = (List<ActividadEtapa>) dao.lista_namedQuery("ActividadEtapa.findByFolioSerial",
                                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                                new String[]{"folio", "serial"});
                        
                        listExplo = dao.explosivos(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                        
                        
                    }
                    //</editor-fold>
                    //<editor-fold defaultstate="collapsed" desc="Contaminantes">
//                    contaminanteList = dao.getContaminanteProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    contaminanteList = (List<ContaminanteProyecto>) dao.lista_namedQuery("ContaminanteProyecto.findByFolioSerial",
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), (short)3},
                            new String[]{"folio", "serial"});
                    //</editor-fold>
                }
            }
        }
    	
    }
   
    //<editor-fold defaultstate="collapsed" desc="cargaEtapaProyecto">
    private EtapaProyecto cargaEtapaProyecto(short etapaId) {
        EtapaProyecto ep = null;
        try {
            ep = (EtapaProyecto) dao.lista_namedQuery("EtapaProyecto.findByFolSerEtapa", new Object[]{
                proyecto.getProyectoPK().getFolioProyecto(),
                proyecto.getProyectoPK().getSerialProyecto(), etapaId}, new String[]{"folio", "serial", "etapaId"}).get(0);
        } catch (Exception e) {
            System.out.println("No se cargo/encontro la etapa con ID: " + etapaId);
        }
        return ep;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public List<UsoSueloVeget> getDimensionesUso() {
        return dimensionesUso;
    }

    public void setDimensionesUso(List<UsoSueloVeget> dimensionesUso) {
        this.dimensionesUso = dimensionesUso;
    }

    public List<PrediocolinProy> getPredCol() {
        return predCol;
    }

    public void setPredCol(List<PrediocolinProy> predCol) {
        this.predCol = predCol;
    }

    public EtapaProyecto getEtapaPreparacion() {
        return etapaPreparacion;
    }

    public void setEtapaPreparacion(EtapaProyecto etapaPreparacion) {
        this.etapaPreparacion = etapaPreparacion;
    }

    public boolean isIsInsumos() {
        return isInsumos;
    }

    public void setIsInsumos(boolean isInsumos) {
        this.isInsumos = isInsumos;
    }

    public List<ContaminanteProyecto> getContaminanteList() {
        return contaminanteList;
    }

    public void setContaminanteList(List<ContaminanteProyecto> contaminanteList) {
        this.contaminanteList = contaminanteList;
    }
    
    public boolean isExplosivos() {
        return explosivos;
    }

    public void setExplosivos(boolean explosivos) {
        this.explosivos = explosivos;
    }

    public List<ActividadEtapa> getExplosivosList() {
        return explosivosList;
    }

    public void setExplosivosList(List<ActividadEtapa> explosivosList) {
        this.explosivosList = explosivosList;
    }

    public EtapaProyecto getEtapaConstruccion() {
        return etapaConstruccion;
    }

    public void setEtapaConstruccion(EtapaProyecto etapaConstruccion) {
        this.etapaConstruccion = etapaConstruccion;
    }

    public EtapaProyecto getEtapaOperacion() {
        return etapaOperacion;
    }

    public void setEtapaOperacion(EtapaProyecto etapaOperacion) {
        this.etapaOperacion = etapaOperacion;
    }

    public EtapaProyecto getEtapaAbandono() {
        return etapaAbandono;
    }

    public void setEtapaAbandono(EtapaProyecto etapaAbandono) {
        this.etapaAbandono = etapaAbandono;
    }

    public List<InsumosProyecto> getInsumos() {
        return insumos;
    }

    public void setInsumos(List<InsumosProyecto> insumos) {
        this.insumos = insumos;
    }
    //</editor-fold>

    public void verEtapas() {
        try {
            int tp = etapaPreparacion.getAnios() + etapaPreparacion.getMeses() + etapaPreparacion.getSemanas();
            int tc = etapaConstruccion.getAnios() + etapaConstruccion.getMeses() + etapaConstruccion.getSemanas();
            int to = etapaOperacion.getAnios() + etapaOperacion.getMeses() + etapaOperacion.getSemanas();
            int ta = etapaAbandono.getAnios() + etapaAbandono.getMeses() + etapaAbandono.getSemanas();

            System.out.println("tp " + tp);
            System.out.println("tc " + tc);
            System.out.println("to " + to);
            System.out.println("ta " + ta);

            verPreparacion = (tp > 0);
            verConstruccion = (tc > 0);
            verOperacion = (to > 0);
            verAbandono = (ta > 0);
            verMsgEtapa = (!verPreparacion && !verConstruccion && !verOperacion && !verAbandono);
        } catch (Exception e) {
        }
    }

    /**
     * @return the actividadesPrepara
     */
    public List<ActividadEtapa> getActividadesPrepara() {
        return actividadesPrepara;
    }

    /**
     * @param actividadesPrepara the actividadesPrepara to set
     */
    public void setActividadesPrepara(List<ActividadEtapa> actividadesPrepara) {
        this.actividadesPrepara = actividadesPrepara;
    }

    /**
     * @return the actividadesConstru
     */
    public List<ActividadEtapa> getActividadesConstru() {
        return actividadesConstru;
    }

    /**
     * @param actividadesConstru the actividadesConstru to set
     */
    public void setActividadesConstru(List<ActividadEtapa> actividadesConstru) {
        this.actividadesConstru = actividadesConstru;
    }

    /**
     * @return the actividadesOperaci
     */
    public List<ActividadEtapa> getActividadesOperaci() {
        return actividadesOperaci;
    }

    /**
     * @param actividadesOperaci the actividadesOperaci to set
     */
    public void setActividadesOperaci(List<ActividadEtapa> actividadesOperaci) {
        this.actividadesOperaci = actividadesOperaci;
    }

    /**
     * @return the actividadesAbandon
     */
    public List<ActividadEtapa> getActividadesAbandon() {
        return actividadesAbandon;
    }

    /**
     * @param actividadesAbandon the actividadesAbandon to set
     */
    public void setActividadesAbandon(List<ActividadEtapa> actividadesAbandon) {
        this.actividadesAbandon = actividadesAbandon;
    }

    /**
     * @return the verPreparacion
     */
    public Boolean getVerPreparacion() {
        return verPreparacion;
    }

    /**
     * @param verPreparacion the verPreparacion to set
     */
    public void setVerPreparacion(Boolean verPreparacion) {
        this.verPreparacion = verPreparacion;
    }

    /**
     * @return the verConstruccion
     */
    public Boolean getVerConstruccion() {
        return verConstruccion;
    }

    /**
     * @param verConstruccion the verConstruccion to set
     */
    public void setVerConstruccion(Boolean verConstruccion) {
        this.verConstruccion = verConstruccion;
    }

    /**
     * @return the verOperacion
     */
    public Boolean getVerOperacion() {
        return verOperacion;
    }

    /**
     * @param verOperacion the verOperacion to set
     */
    public void setVerOperacion(Boolean verOperacion) {
        this.verOperacion = verOperacion;
    }

    /**
     * @return the verAbandono
     */
    public Boolean getVerAbandono() {
        return verAbandono;
    }

    /**
     * @param verAbandono the verAbandono to set
     */
    public void setVerAbandono(Boolean verAbandono) {
        this.verAbandono = verAbandono;
    }

    /**
     * @return the verMsgEtapa
     */
    public Boolean getVerMsgEtapa() {
        return verMsgEtapa;
    }

    /**
     * @param verMsgEtapa the verMsgEtapa to set
     */
    public void setVerMsgEtapa(Boolean verMsgEtapa) {
        this.verMsgEtapa = verMsgEtapa;
    }

    /**
     * @return the proyectoSustancia
     */
    public List<Object[]> getProyectoSustancia() {
        return proyectoSustancia;
    }

    /**
     * @param proyectoSustancia the proyectoSustancia to set
     */
    public void setProyectoSustancia(List<Object[]> proyectoSustancia) {
        this.proyectoSustancia = proyectoSustancia;
    }

    /**
     * @return the anexosProyectoEtapa1
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa1() {
        return anexosProyectoEtapa1;
    }

    /**
     * @param anexosProyectoEtapa1 the anexosProyectoEtapa1 to set
     */
    public void setAnexosProyectoEtapa1(List<AnexosProyecto> anexosProyectoEtapa1) {
        this.anexosProyectoEtapa1 = anexosProyectoEtapa1;
    }

    /**
     * @return the anexosProyectoEtapa2
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa2() {
        return anexosProyectoEtapa2;
    }

    /**
     * @param anexosProyectoEtapa2 the anexosProyectoEtapa2 to set
     */
    public void setAnexosProyectoEtapa2(List<AnexosProyecto> anexosProyectoEtapa2) {
        this.anexosProyectoEtapa2 = anexosProyectoEtapa2;
    }

    /**
     * @return the anexosProyectoEtapa3
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa3() {
        return anexosProyectoEtapa3;
    }

    /**
     * @param anexosProyectoEtapa3 the anexosProyectoEtapa3 to set
     */
    public void setAnexosProyectoEtapa3(List<AnexosProyecto> anexosProyectoEtapa3) {
        this.anexosProyectoEtapa3 = anexosProyectoEtapa3;
    }

    /**
     * @return the anexosProyectoEtapa4
     */
    public List<AnexosProyecto> getAnexosProyectoEtapa4() {
        return anexosProyectoEtapa4;
    }

    /**
     * @param anexosProyectoEtapa4 the anexosProyectoEtapa4 to set
     */
    public void setAnexosProyectoEtapa4(List<AnexosProyecto> anexosProyectoEtapa4) {
        this.anexosProyectoEtapa4 = anexosProyectoEtapa4;
    }

    /**
     * @return the vseccion
     */
    public String getVseccion() {
        return vseccion;
    }

    /**
     * @param vseccion the vseccion to set
     */
    public void setVseccion(String vseccion) {
        this.vseccion = vseccion;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public String getBitacora() {
        return bitacora;
    }

	public List<Object> getPredCol1() {
		return predCol1;
	}

	public void setPredCol1(List<Object> predCol1) {
		this.predCol1 = predCol1;
	}

	//Getter y Setters de listas de consulta de archivos 
		
	public List<Object> getPreparaList() {
		return preparaList;
	}

	/**
	 * @return the archivos1
	 */
	public List<ArchivosProyecto> getArchivos1() {
		return archivos1;
	}

	/**
	 * @param archivos1 the archivos1 to set
	 */
	public void setArchivos1(List<ArchivosProyecto> archivos1) {
		this.archivos1 = archivos1;
	}

	/**
	 * @return the archivos2
	 */
	public List<ArchivosProyecto> getArchivos2() {
		return archivos2;
	}

	/**
	 * @param archivos2 the archivos2 to set
	 */
	public void setArchivos2(List<ArchivosProyecto> archivos2) {
		this.archivos2 = archivos2;
	}

	/**
	 * @return the archivos3
	 */
	public List<ArchivosProyecto> getArchivos3() {
		return archivos3;
	}

	/**
	 * @param archivos3 the archivos3 to set
	 */
	public void setArchivos3(List<ArchivosProyecto> archivos3) {
		this.archivos3 = archivos3;
	}

	/**
	 * @return the archivos4
	 */
	public List<ArchivosProyecto> getArchivos4() {
		return archivos4;
	}

	/**
	 * @param archivos4 the archivos4 to set
	 */
	public void setArchivos4(List<ArchivosProyecto> archivos4) {
		this.archivos4 = archivos4;
	}
	
	//Listas de consulta de informacion
	
	public void setPreparaList(List<Object> preparaList) {
		this.preparaList = preparaList;
	}

	public List<Object> getConstruList() {
		return construList;
	}

	public void setConstruList(List<Object> construList) {
		this.construList = construList;
	}

	public List<Object> getOperaciList() {
		return operaciList;
	}

	public void setOperaciList(List<Object> operaciList) {
		this.operaciList = operaciList;
	}

	public List<Object> getAbandonList() {
		return abandonList;
	}

	public void setAbandonList(List<Object> abandonList) {
		this.abandonList = abandonList;
	}

	public List<Object> getListExplo() {
		return listExplo;
	}

	public void setListExplo(List<Object> listExplo) {
		this.listExplo = listExplo;
	}

	/**
	 * @return the archivosDuracionProyecto
	 */
	public List<ArchivosProyecto> getArchivosDuracionProyecto() {
		return archivosDuracionProyecto;
	}

	/**
	 * @param archivosDuracionProyecto the archivosDuracionProyecto to set
	 */
	public void setArchivosDuracionProyecto(List<ArchivosProyecto> archivosDuracionProyecto) {
		this.archivosDuracionProyecto = archivosDuracionProyecto;
	}

	/**
	 * @return the archivosDescObrasPrinProy
	 */
	public List<ArchivosProyecto> getArchivosDescObrasPrinProy() {
		return archivosDescObrasPrinProy;
	}

	/**
	 * @param archivosDescObrasPrinProy the archivosDescObrasPrinProy to set
	 */
	public void setArchivosDescObrasPrinProy(List<ArchivosProyecto> archivosDescObrasPrinProy) {
		this.archivosDescObrasPrinProy = archivosDescObrasPrinProy;
	}

	/**
	 * @return the descObras
	 */
	public boolean isDescObras() {
		return descObras;
	}

	/**
	 * @param descObras the descObras to set
	 */
	public void setDescObras(boolean descObras) {
		this.descObras = descObras;
	}

	public TreeNode getRootSuelos() {
		return rootSuelos;
	}

	public void setRootSuelos(TreeNode rootSuelos) {
		this.rootSuelos = rootSuelos;
	}

	public TablaNodo getTablaNodo() {
		return tablaNodo;
	}

	public void setTablaNodo(TablaNodo tablaNodo) {
		this.tablaNodo = tablaNodo;
	}

	public List<SueloVegetacionProyecto> getSueloVegetacion() {
		return sueloVegetacion;
	}

	public void setSueloVegetacion(List<SueloVegetacionProyecto> sueloVegetacion) {
		this.sueloVegetacion = sueloVegetacion;
	}

	/**
	 * @return the infoBio
	 */
	public List<InfobioProyecto> getInfoBio() {
		return infoBio;
	}

	/**
	 * @param infoBio the infoBio to set
	 */
	public void setInfoBio(List<InfobioProyecto> infoBio) {
		this.infoBio = infoBio;
	}

	/**
	 * @return the infoBioRender
	 */
	public boolean isInfoBioRender() {
		return infoBioRender;
	}

	/**
	 * @param infoBioRender the infoBioRender to set
	 */
	public void setInfoBioRender(boolean infoBioRender) {
		this.infoBioRender = infoBioRender;
	}

	/**
	 * @return the accidenteList
	 */
	public List<AccidentesProyecto> getAccidenteList() {
		return accidenteList;
	}

	/**
	 * @param accidenteList the accidenteList to set
	 */
	public void setAccidenteList(List<AccidentesProyecto> accidenteList) {
		this.accidenteList = accidenteList;
	}

	/**
	 * @return the accidentesAmb
	 */
	public boolean isAccidentesAmb() {
		return accidentesAmb;
	}

	/**
	 * @param accidentesAmb the accidentesAmb to set
	 */
	public void setAccidentesAmb(boolean accidentesAmb) {
		this.accidentesAmb = accidentesAmb;
	}
	
}
