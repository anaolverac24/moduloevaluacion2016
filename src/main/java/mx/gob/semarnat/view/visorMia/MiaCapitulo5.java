package mx.gob.semarnat.view.visorMia;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.ssl.ProbablyBadPasswordException;
import org.omg.CORBA.IMP_LIMIT;

import com.itextpdf.text.Image;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoTO;
import mx.gob.semarnat.model.dgira_miae.ImpacAmbProyecto;
import mx.gob.semarnat.utils.GenericConstants;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paty
 */
@ManagedBean(name = "miaCap5")
@ViewScoped
public class MiaCapitulo5 {
    private final VisorDao dao = new VisorDao();
    private final DgiraMiaeDaoGeneral daoGeneral = new DgiraMiaeDaoGeneral();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private Proyecto folioDetalle;
    private Proyecto proyecto;
    private String urlCap5MIA;
    private List<AnexosProyecto> anexosProyecto = new ArrayList<AnexosProyecto>();
    private List<ImpacAmbProyecto> evaluacion = new ArrayList();
    private List<AnexosProyecto> anexosProyecto2 = new ArrayList<AnexosProyecto>();
    
    private int vistaVersion;
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    
    // Archivos de Metodologia para identificar y evaluar los impactos ambientales - Justificacion de la metodologia utilizada
 	private List<ArchivosProyecto> archivosJustiMetodo = new ArrayList<ArchivosProyecto>();
 	// Archivos de Resultados de evaluacion de los impactos ambientales
 	private List<ArchivosProyecto> archivosResEvaImpAmb = new ArrayList<ArchivosProyecto>();
 	
 	private ProyectoTO proyectoTO;
 	
 	// Datos de los cuadros de texto enriquecido
 	private String metIdenEvaImpAmb;
	private String justificacion;

    @SuppressWarnings("static-access")
	public MiaCapitulo5() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        pramBita = params.get("bitaNum").toString();
        vistaVersion = (params.get("vistaVersion") != null) ? 
                Integer.parseInt((String)params.get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
        
        if (pramBita != null) 
        { 
            urlCap5MIA = "../capitulosMIA/capitulo5MIA.xhtml?bitaNum=" + pramBita;
            switch (vistaVersion) {
                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
                    folioDetalle = dao.obtieneProyectoUltimaVersion(pramBita);
                    break;
                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
                    folioDetalle = dao.obtieneProyectoVersionAnterior(pramBita);
                    break;
            }
            
            if (folioDetalle != null)  
            {
                folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                //JOptionPane.showMessageDialog(null, "folio:  " + folioNum + "   serial: " + serialNum, "Error", JOptionPane.INFORMATION_MESSAGE);
                //----proyecto en sesion
                proyecto = folioDetalle;//dao.cargaProyecto(folioNum, serialNum);

                //4,3,1,1
                short capituloId = (short) 0;
                short subcapituloId = (short) 0;
                short seccionId = (short) 0;
                short apartadoId = (short) 0; 
                capituloId = 4;
                subcapituloId = 3;
                seccionId = 1;
                apartadoId = 1;
////                    anexosProyecto = dao.getAnexos(capituloId, subcapituloId, seccionId, apartadoId, folioNum, serialNum);

                //4,3,1,1
                short capituloId2 = (short) 0;
                short subcapituloId2 = (short) 0;
                short seccionId2 = (short) 0;
                short apartadoId2 = (short) 0; 
                capituloId2 = 4;
                subcapituloId2 = 3;
                seccionId2 = 1;
                apartadoId2 = 1;
////                    anexosProyecto2 = dao.getAnexos(capituloId2, subcapituloId2, seccionId2, apartadoId2, folioNum, serialNum);
            }
            
            mapInfoAdicional = daoGeneral.generaMapaInfoAdicionalCapitulo(pramBita, "5");
        }
        
        archivosJustiMetodo = daoGeneral.archivosProyecto((short)5, (short)1, (short)0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosResEvaImpAmb = daoGeneral.archivosProyecto((short)5, (short)2, (short)0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
     //  evaluacion = (List<ImpacAmbProyecto>) dao.impactoAmbiente(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        try {
            this.proyectoTO = daoGeneral.proyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        
        metIdenEvaImpAmb = daoGeneral.parseHtmlParagraph(proyectoTO.getDescMetutil()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        justificacion = daoGeneral.parseHtmlParagraph(proyectoTO.getProyJustmetUtil()).toString().replace("[", "").replace("], ", "").replace("]", "");        
        
        System.out.println("Fin de MiaCap5");
    }  
    
    
    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * @return the folioDetalle
     */
    public Proyecto getFolioDetalle() {
        return folioDetalle;
    }

    /**
     * @param folioDetalle the folioDetalle to set
     */
    public void setFolioDetalle(Proyecto folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the urlCap5MIA
     */
    public String getUrlCap5MIA() {
        return urlCap5MIA;
    }

    /**
     * @param urlCap5MIA the urlCap5MIA to set
     */
    public void setUrlCap5MIA(String urlCap5MIA) {
        this.urlCap5MIA = urlCap5MIA;
    }

    /**
     * @return the anexosProyecto
     */
    public List<AnexosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<AnexosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    /**
     * @return the anexosProyecto2
     */
    public List<AnexosProyecto> getAnexosProyecto2() {
        return anexosProyecto2;
    }

    /**
     * @param anexosProyecto2 the anexosProyecto2 to set
     */
    public void setAnexosProyecto2(List<AnexosProyecto> anexosProyecto2) {
        this.anexosProyecto2 = anexosProyecto2;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

	/**
	 * @return the archivosJustiMetodo
	 */
	public List<ArchivosProyecto> getArchivosJustiMetodo() {
		return archivosJustiMetodo;
	}

	/**
	 * @param archivosJustiMetodo the archivosJustiMetodo to set
	 */
	public void setArchivosJustiMetodo(List<ArchivosProyecto> archivosJustiMetodo) {
		this.archivosJustiMetodo = archivosJustiMetodo;
	}

	/**
	 * @return the archivosResEvaImpAmb
	 */
	public List<ArchivosProyecto> getArchivosResEvaImpAmb() {
		return archivosResEvaImpAmb;
	}

	/**
	 * @param archivosResEvaImpAmb the archivosResEvaImpAmb to set
	 */
	public void setArchivosResEvaImpAmb(List<ArchivosProyecto> archivosResEvaImpAmb) {
		this.archivosResEvaImpAmb = archivosResEvaImpAmb;
	}


	/**
	 * @return the metIdenEvaImpAmb
	 */
	public String getMetIdenEvaImpAmb() {
		return metIdenEvaImpAmb;
	}


	/**
	 * @param metIdenEvaImpAmb the metIdenEvaImpAmb to set
	 */
	public void setMetIdenEvaImpAmb(String metIdenEvaImpAmb) {
		this.metIdenEvaImpAmb = metIdenEvaImpAmb;
	}


	/**
	 * @return the justificacion
	 */
	public String getJustificacion() {
		return justificacion;
	}


	/**
	 * @param justificacion the justificacion to set
	 */
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}
         
}



