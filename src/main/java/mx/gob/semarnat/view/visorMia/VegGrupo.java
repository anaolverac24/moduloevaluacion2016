/**
 * 
 */
package mx.gob.semarnat.view.visorMia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Cesar
 *
 */
@Entity
@Table(name= "VEG_GRUPO")
@XmlRootElement
public class VegGrupo implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 7295890183935367086L;

	@Id
	@GeneratedValue(generator="InvSeq") 
    @SequenceGenerator(name="InvSeq",sequenceName="SEQ_ID_VEG_GRUPO")  
	@Basic(optional = false)
	@Column(name="ID_VEG_GRUPO")
	private Integer idVegGrupo;
	
	@Column(name="VEG_NOMBRE")
	private String vegNombre;
	
	@OneToMany(mappedBy="vegGrupo")
	private List<VegTipo> listaVegTipos = new ArrayList<VegTipo>();

	/**
	 * @return the idVegGrupo
	 */
	public Integer getIdVegGrupo() {
		return idVegGrupo;
	}

	/**
	 * @param idVegGrupo the idVegGrupo to set
	 */
	public void setIdVegGrupo(Integer idVegGrupo) {
		this.idVegGrupo = idVegGrupo;
	}

	/**
	 * @return the vegNombre
	 */
	public String getVegNombre() {
		return vegNombre;
	}

	/**
	 * @param vegNombre the vegNombre to set
	 */
	public void setVegNombre(String vegNombre) {
		this.vegNombre = vegNombre;
	}

	/**
	 * @return the listaVegTipos
	 */
	public List<VegTipo> getListaVegTipos() {
		return listaVegTipos;
	}

	/**
	 * @param listaVegTipos the listaVegTipos to set
	 */
	public void setListaVegTipos(List<VegTipo> listaVegTipos) {
		this.listaVegTipos = listaVegTipos;
	}
	
	
}
