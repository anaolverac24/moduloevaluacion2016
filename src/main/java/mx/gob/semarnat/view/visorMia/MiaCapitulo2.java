/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorMia;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.CaracPartProyDAO;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.MenuDao;
import mx.gob.semarnat.dao.SigeiaDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.CaracPartProy;
import mx.gob.semarnat.model.dgira_miae.CaracPartProyPK;
import mx.gob.semarnat.model.dgira_miae.CaracPartProyTO;
import mx.gob.semarnat.model.dgira_miae.CatalogoTO;
import mx.gob.semarnat.model.dgira_miae.ImpacAmbProyecto;
import mx.gob.semarnat.model.dgira_miae.InversionEtapas;
import mx.gob.semarnat.model.dgira_miae.MedPrevImpactProy;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoTO;
import mx.gob.semarnat.model.dgira_miae.ServicioProyecto;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.view.Constantes;

import org.primefaces.context.RequestContext;

/**
 *
 * @author Paty
 */
@ManagedBean(name = "miaCap2")
@ViewScoped
public class MiaCapitulo2 implements Serializable {

    private final VisorDao dao = new VisorDao();
    private final MenuDao MenuDao= new MenuDao();
    private final DgiraMiaeDaoGeneral daoGral = new DgiraMiaeDaoGeneral();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private Proyecto folioDetalle;
    private Proyecto proyecto;
    private Integer idusuario;
    private String urlCap2MIA;
    private List<Object[]> todosEdos = new ArrayList();
    private List<Object[]> edoAfectado = new ArrayList();
    private List<Object[]> criterios = new ArrayList();
    private List<MedPrevImpactProy> medPreventivas = new ArrayList();
    // para la seleccion de referencia de tipo de domicilio
    private String tipoDomic;
    private CatVialidad catVialidad;
    private CatTipoAsen catTipoAsen;
    // define si se ve el tipo de domicilion exacto, para su llenado
    private Boolean localiz = null;
    // define si se ve el tipo de domicilion NO exacto, para su llenado
    private Boolean localizNo = null;
    private List<AnexosProyecto> anexosProyecto = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> anexosProyecto2 = new ArrayList();
    private DecimalFormat df = new DecimalFormat("##.##");
    
    private List<ImpacAmbProyecto> evaluacion = new ArrayList<ImpacAmbProyecto>();
    private String cveProyecto; 
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private String idArea;
    
    private int vistaVersion;
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    
    // Archivos de Ubicacion Fisica del Proyecto
    private List<ArchivosProyecto> archivosUbicacion = new ArrayList<ArchivosProyecto>();
    
    // Inversion y Empleos
    private List<InversionEtapas> inversionE = new ArrayList<InversionEtapas>();
    private InversionEtapas inversionEtapas;
    private boolean proyInversionPE;
    private ProyectoTO proyectoTO;
    
    // Servicios
    private List<ServicioProyecto> serviciosReqPrep = new ArrayList<ServicioProyecto>();
    private List<ServicioProyecto> serviciosReqCons = new ArrayList<ServicioProyecto>();
    private List<ServicioProyecto> serviciosReqOper = new ArrayList<ServicioProyecto>();
    private List<ServicioProyecto> serviciosReqAban = new ArrayList<ServicioProyecto>();
    
    // CarParProy
    private List<CaracPartProyTO> listaCaractTO;
    /**
     * Objeto para las transacciones de la tabla caracteristica DAO.
     */
	private CaracPartProyDAO caracPartProyDAO = new CaracPartProyDAO();
	
	private SigeiaDao sigeiaDao = new SigeiaDao();
	
	// Texto de Naturaleza del proyecto
	private String naturalezaProy;
    
    @SuppressWarnings({ "static-access", "unchecked" })
	public MiaCapitulo2() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        idusuario = (Integer) ec.getSessionMap().get("idUsu");
        pramBita = params.get("bitaNum").toString();        
        vistaVersion = (params.get("vistaVersion") != null) ? 
                Integer.parseInt((String)params.get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
        
        if (pramBita != null) {
            FacesContext fContext = FacesContext.getCurrentInstance();
            idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu");
            
            
            
            cveProyecto = MenuDao.cveTramite(pramBita);
            urlCap2MIA = "capitulosMIA/capitulo2MIA.xhtml?bitaNum=" + pramBita;
            
//            switch (vistaVersion) {
//                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
//                    folioDetalle = dao.obtieneProyectoVersionAnterior(pramBita);
//                    break;
//                    
//                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
//                    folioDetalle = dao.obtieneProyectoUltimaVersion(pramBita);
//                    break;
//                    
//                case GenericConstants.VISTA_EVALUADOR_1ER_INGRESO:
//                case GenericConstants.VISTA_EVALUADOR_INFO_ADICIONAL:
//                    //Se obtiene la información del proyecto de la versión anterior del promovente (1, 3, ...)
//                    if (vistaVersion == GenericConstants.VISTA_EVALUADOR_1ER_INGRESO) {//Si versión 2
//                        folioDetalle = dao.obtieneProyectoVersionAnterior(pramBita); //versión 1
//                    } else {//Si versión 4
//                        folioDetalle = dao.obtieneProyectoUltimaVersion(pramBita);//versión 3
//                    }
//
//                    folioDetalle.getProyectoPK().setSerialProyecto((short)vistaVersion);
//
//                    break;
//            }  
            
            folioDetalle = dao.obtieneProyectoVersionAnterior(pramBita);
            
            if (folioDetalle != null) {  // JOptionPane.showMessageDialog(null, "folio:  " + folioDetalle, "Error", JOptionPane.INFORMATION_MESSAGE);

                folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                //JOptionPane.showMessageDialog(null, "folio:  " + folioNum + "   serial: " + serialNum, "Error", JOptionPane.INFORMATION_MESSAGE);
                //----proyecto en sesion
                proyecto = folioDetalle;//dao.cargaProyecto(folioNum, serialNum);

                criterios = dao.getCriterios(folioNum, serialNum);                
                
//                medPreventivas = dao.getMedPreventivas(folioNum, serialNum);
                medPreventivas = (List<MedPrevImpactProy>) daoGral.lista_namedQuery("MedPrevImpactProy.findBySerialProyecto",
                        new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                        new String[]{"folioProyecto", "serialProyecto"});
                
                evaluacion = (List<ImpacAmbProyecto>) daoGral.lista_namedQuery("ImpacAmbProyecto.findByFolioSerial",
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                            new String[]{"folioProyecto", "serialProyecto"});
                
             //   evaluacion = daoGral.impactoAmbiente(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                
                //todo impactos

                todosEdos = estadosAfetados();
                
                tipoDomic = seleccionaDomicilio();
                catVialidad = dao.tipoVialidad(proyecto.getCveTipoVial());
                catTipoAsen = dao.tipoAsenta(proyecto.getTipoAsentamientoId());
                //2,1,3,1,
                short capituloId = (short) 0;
                short subcapituloId = (short) 0;
                short seccionId = (short) 0;
                short apartadoId = (short) 0;
                capituloId = 2;
                subcapituloId = 1;
                seccionId = 3;
                apartadoId = 1;
                anexosProyecto = dao.getAnexos(capituloId, subcapituloId, seccionId, apartadoId, folioNum, serialNum);
                
                //2,2,3,1
                short capituloId2 = (short) 0;
                short subcapituloId2 = (short) 0;
                short seccionId2 = (short) 0;
                short apartadoId2 = (short) 0; 
                capituloId2 = 2;
                subcapituloId2 = 2;
                seccionId2 = 3;
                apartadoId2 = 1;
                anexosProyecto2 = dao.getAnexos(capituloId2, subcapituloId2, seccionId2, apartadoId2, folioNum, serialNum);

            }
        }
        
        mapInfoAdicional = daoGeneral.generaMapaInfoAdicionalCapitulo(pramBita, "2");
        mapInfoAdicional.putAll(daoGeneral.generaMapaInfoAdicionalCapitulo(pramBita, "5"));
        mapInfoAdicional.putAll(daoGeneral.generaMapaInfoAdicionalCapitulo(pramBita, "6"));
        
        archivosUbicacion = daoGral.archivosProyecto((short)2, (short)1, (short)3, (short) 2, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        try {
            this.proyectoTO = daoGral.proyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        
		try {
			inversionEtapas = daoGral.getInversion(proyectoTO.getProyectoPK().getFolioProyecto(),
					proyectoTO.getProyectoPK().getSerialProyecto(), (short) 1);
		} catch (Exception e) {
			System.out.println("No encontrado");
		}

		if (inversionEtapas == null) {
			this.inversionEtapas = new InversionEtapas();
		}
		
		/**
         * Se determina que tabla se mostrara de acuerdo a la variable guardada en MIA
         */
		if (proyectoTO.getProyMontoEtapasAplica() != null && proyectoTO.getProyMontoEtapasAplica().equals("N")) {
			this.proyInversionPE = false;
			System.out.println("Aplica Monto por Etapas: " + proyInversionPE);
		}
		else if (proyectoTO.getProyMontoEtapasAplica() != null && proyectoTO.getProyMontoEtapasAplica().equals("S")) {
			this.proyInversionPE = true;
			System.out.println("Aplica Monto por Etapas: " + proyInversionPE);
		}
		

		/**
         * Se llena lista de inversion y empleos por Etapas
         */
		try {
			inversionE = daoGral.getInversiones(proyecto.getProyectoPK().getFolioProyecto(),
					proyecto.getProyectoPK().getSerialProyecto());
		} catch (Exception e) {
			System.out.println("Inversiones no encontradas");
		}
		
		/**
         * Se llenan las listas de Servicios requeridos por etapas
         */
		try {
            serviciosReqPrep = daoGral.getServicios(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),(short) 1);
            if (serviciosReqPrep == null) {
            	serviciosReqPrep = new ArrayList<ServicioProyecto>();
            }
            System.out.println("============== existen" + serviciosReqPrep.size() + " servicios en preparacion ===============");
        } catch (Exception e) {
            e.printStackTrace();
        }
		try {
            serviciosReqCons = daoGral.getServicios(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),(short) 2);
            if (serviciosReqCons == null) {
            	serviciosReqCons = new ArrayList<ServicioProyecto>();
            }
            System.out.println("============== existen" + serviciosReqCons.size() + " servicios en preparacion ===============");
        } catch (Exception e) {
            e.printStackTrace();
        }
		try {
            serviciosReqOper = daoGral.getServicios(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),(short) 3);
            if (serviciosReqOper == null) {
            	serviciosReqOper = new ArrayList<ServicioProyecto>();
            }
            System.out.println("============== existen" + serviciosReqOper.size() + " servicios en preparacion ===============");
        } catch (Exception e) {
            e.printStackTrace();
        }
		try {
            serviciosReqAban = daoGral.getServicios(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),(short) 1);
            if (serviciosReqAban == null) {
            	serviciosReqAban = new ArrayList<ServicioProyecto>();
            }
            System.out.println("============== existen" + serviciosReqAban.size() + " servicios en preparacion ===============");
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		/**
         * Se llena la lista para caracteristicas particulares del proyecto
         */	
		listaCaractTO = new ArrayList<CaracPartProyTO>();
		
		consultarCaracteristicasProyecto();
		
		/**
         * Se obtiene el contenido en naturaleza del proyecto para quitar las etiquetas de HTML
         */
		naturalezaProy = daoGral.parseHtmlParagraph(proyectoTO.getProyDescNat()).toString().replace("[", "").replace("],", "").replace("]", "");
		
		System.out.println("Fin MiaCap2");
    }
    
    /**
     * Permite consultar las caracteristicas del proyecto.
     */
    public void consultarCaracteristicasProyecto() {
        try {
            List<CaracPartProy> caracteristicasPartProy = new ArrayList<CaracPartProy>();
            
            listaCaractTO = new ArrayList<CaracPartProyTO>();
			//consulta en la tabla del promovente, si existe un registro de SIGEIA.
            caracteristicasPartProy = caracPartProyDAO.getCaracPartSIGEIAPROM(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            //Si no contiene registros de SIGEIA.
            if (caracteristicasPartProy.isEmpty()){

				short contadorConsecutivoSIGEIA = 1;
				short contadorConsecutivoPROMOVENTE = 1;

    			//Se consulta la informacion de SIGEIA.
    			List<Object[]> listaSueloVegSIGEIA = sigeiaDao.consultarCaracPartProySIGEIA(proyecto.getProyectoPK().getFolioProyecto());
    			//Se itera la lista de registros de Suelo Vegetacion de SIGEIA.
    			for (Object[] objects : listaSueloVegSIGEIA) {
    				CaracPartProy caracPartProy = new CaracPartProy();
        			CaracPartProyTO caracPartProyTO= new CaracPartProyTO();
        			caracPartProyTO.setNombreObra(objects[0].toString());
        			
        			caracPartProyTO.setSuperficie(objects[2].toString());

        			caracPartProyTO.setNaturaleza(new CatalogoTO());
        			caracPartProyTO.setTemporalidad(new CatalogoTO());
        			
        			
        			CatUnidadMedida catUnidadMedida = new CatUnidadMedida();            			
        			catUnidadMedida.setCtunDesc("m2");
        			
        			CatalogoTO catalogoTO = new CatalogoTO();
        			catalogoTO.setDescripcion(catUnidadMedida.getCtunDesc());
        			caracPartProyTO.setUnidad(catalogoTO);
        			
        			caracPartProyTO.setEstatusSigProm("0");        			
        			
    				Short maxCaracteristica = caracPartProyDAO.getMaxCaracteristica(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    				
    				if (maxCaracteristica == null) {
    					maxCaracteristica = 0;
    				} else {
    					maxCaracteristica++;
    				}			
    				
    				CaracPartProyPK caracPartProyPK = new CaracPartProyPK();
    				caracPartProyPK.setCaracteristicaId(maxCaracteristica);
      				
    				caracPartProyTO.setCaracteristicaID(contadorConsecutivoSIGEIA);
    				
    				contadorConsecutivoSIGEIA++;
    				
    				caracPartProyPK.setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
    				caracPartProyPK.setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
    				
    				caracPartProy.setCaracPartProyPK(caracPartProyPK);
    				
    				Short maxValueSequence = caracPartProyDAO.consultarMAXCaracSequence(); 
    				//Se obtiene el maximo valor de la secuencia.
    				if (maxValueSequence == null) {
    					maxValueSequence = 1;
            			caracPartProyTO.setId(maxValueSequence);
    					caracPartProy.setIdCaracSeq(maxValueSequence);
    				} else {
            			caracPartProyTO.setId((short) (maxValueSequence + 1));
    					caracPartProy.setIdCaracSeq((short) (maxValueSequence + 1));
    				}
    				caracPartProy.setNombreObra(caracPartProyTO.getNombreObra());
    				caracPartProy.setEstatusSigProm(caracPartProyTO.getEstatusSigProm());
    				
    				String superficie = caracPartProyTO.getSuperficie().replaceAll(",", "");
    				
    				caracPartProy.setSuperficie(Double.parseDouble(superficie));
    				caracPartProy.setNaturalezaId(null);
    				caracPartProy.setTemporalidadId(null);
    				caracPartProyDAO.guardarCaracteristica(caracPartProy);

        			listaCaractTO.add(caracPartProyTO);            		

    			}    			

            	//Se consulta la informacion del promovente.
                caracteristicasPartProy = caracPartProyDAO.getCaracPart(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

            	//Se itera la lista de caracteristicas.
            	for(CaracPartProy caracPartProy:caracteristicasPartProy){
            			
            			CaracPartProyTO caracPartProyTO= new CaracPartProyTO();
            			caracPartProyTO.setCaracteristicaID(contadorConsecutivoPROMOVENTE);
            			
            			contadorConsecutivoPROMOVENTE++;
            			
            			caracPartProyTO.setObraActividad(caracPartProy.getObraActividad());
            			caracPartProyTO.setNombreObra(caracPartProy.getNombreObra());
            			caracPartProyTO.setDescripcion(caracPartProy.getDescripcion());
            			caracPartProyTO.setDescTrunked(caracPartProyTO.getDescripcion()!=null && caracPartProyTO.getDescripcion().length()>200 ? caracPartProyTO.getDescripcion().substring(0,200):caracPartProyTO.getDescripcion());
            			caracPartProyTO.setId(caracPartProy.getIdCaracSeq());
            		
            			String superficie = caracPartProy.getSuperficie().toString();
            			
            			caracPartProyTO.setSuperficie(superficie);
            			
            			if (caracPartProy.getNaturalezaId() == null && caracPartProy.getTemporalidadId() == null) {
            				caracPartProyTO.setNaturaleza(new CatalogoTO());
            				caracPartProyTO.setTemporalidad(new CatalogoTO());
            			} else {
                			caracPartProyTO.setNaturaleza(new CatalogoTO(caracPartProy.getNaturalezaId().getNaturalezaId(),caracPartProy.getNaturalezaId().getNaturalezaDescripcion() != null 
                					? caracPartProy.getNaturalezaId().getNaturalezaDescripcion()
                							: caracPartProyDAO.consultarNaturaleza(caracPartProy.getNaturalezaId().getNaturalezaId()).getNaturalezaDescripcion()));
                			
                			caracPartProyTO.setTemporalidad(new CatalogoTO(caracPartProy.getTemporalidadId().getTemporalidadId(),caracPartProy.getTemporalidadId().getTemporalidadDescripcion() != null 
                					? caracPartProy.getTemporalidadId().getTemporalidadDescripcion() 
                							:caracPartProyDAO.consultarTemporalidad(caracPartProy.getTemporalidadId().getTemporalidadId()).getTemporalidadDescripcion()));
						}           			
            			
            			CatUnidadMedida catUnidadMedida = caracPartProyDAO.getCatUnidadMedida(caracPartProy.getCtunClve());            			
            			caracPartProyTO.setUnidad(catUnidadMedida!=null ?new CatalogoTO(catUnidadMedida.getCtunClve(),catUnidadMedida.getCtunAbre()):null);
            			caracPartProyTO.setEstatusSigProm(caracPartProy.getEstatusSigProm());        			

            			listaCaractTO.add(caracPartProyTO);            			
            			
            	}
            } else {
            	
				short contadorConsecutivoSIGEIA = 1;
				short contadorConsecutivoPROMOVENTE = 1;
				
            	//Se consulta la informacion del promovente.
            	caracteristicasPartProy = caracPartProyDAO.getCaracPartOrderBySIGEIA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            	//Se itera la lista de caracteristicas del proyecto de SIGEIA.
            	for(CaracPartProy caracPartProy:caracteristicasPartProy){
            			
            			CaracPartProyTO caracPartProyTO= new CaracPartProyTO();
            			
            			if (caracPartProy.getEstatusSigProm().equals("0")) {
                			caracPartProyTO.setCaracteristicaID(contadorConsecutivoSIGEIA);
                			contadorConsecutivoSIGEIA++;
						} else {
							caracPartProyTO.setCaracteristicaID(contadorConsecutivoPROMOVENTE);
							contadorConsecutivoPROMOVENTE++;
						}
            			
            			caracPartProyTO.setObraActividad(caracPartProy.getObraActividad());
            			caracPartProyTO.setNombreObra(caracPartProy.getNombreObra());
            			caracPartProyTO.setDescripcion(caracPartProy.getDescripcion());
            			caracPartProyTO.setDescTrunked(caracPartProyTO.getDescripcion()!=null && caracPartProyTO.getDescripcion().length()>200 ? caracPartProyTO.getDescripcion().substring(0,200):caracPartProyTO.getDescripcion());
            			caracPartProyTO.setId(caracPartProy.getIdCaracSeq());
            			caracPartProyTO.setSuperficie(caracPartProy.getSuperficie().toString());
            			
            			if (caracPartProy.getNaturalezaId() != null && caracPartProy.getTemporalidadId() != null) {
                			caracPartProyTO.setNaturaleza(new CatalogoTO(caracPartProy.getNaturalezaId().getNaturalezaId(),caracPartProy.getNaturalezaId().getNaturalezaDescripcion() != null 
                					? caracPartProy.getNaturalezaId().getNaturalezaDescripcion()
                							: caracPartProyDAO.consultarNaturaleza(caracPartProy.getNaturalezaId().getNaturalezaId()).getNaturalezaDescripcion()));
                			
                			caracPartProyTO.setTemporalidad(new CatalogoTO(caracPartProy.getTemporalidadId().getTemporalidadId(),caracPartProy.getTemporalidadId().getTemporalidadDescripcion() != null 
                					? caracPartProy.getTemporalidadId().getTemporalidadDescripcion() 
                							:caracPartProyDAO.consultarTemporalidad(caracPartProy.getTemporalidadId().getTemporalidadId()).getTemporalidadDescripcion()));							
						} else {
                			caracPartProyTO.setNaturaleza(new CatalogoTO());
                			caracPartProyTO.setTemporalidad(new CatalogoTO());
						}            			
            			
            			if (caracPartProy.getCtunClve() != null) {
                			CatUnidadMedida catUnidadMedida = caracPartProyDAO.getCatUnidadMedida(caracPartProy.getCtunClve());
                			caracPartProyTO.setUnidad(catUnidadMedida!=null ?new CatalogoTO(catUnidadMedida.getCtunClve(),catUnidadMedida.getCtunAbre()):null);							
						} else {
		        			CatUnidadMedida catUnidadMedida = new CatUnidadMedida();            			
		        			catUnidadMedida.setCtunDesc("m2");
		        			
		        			CatalogoTO catalogoTO = new CatalogoTO();
		        			catalogoTO.setDescripcion(catUnidadMedida.getCtunDesc());
		        			caracPartProyTO.setUnidad(catalogoTO);		        			
						}

            			caracPartProyTO.setEstatusSigProm(caracPartProy.getEstatusSigProm());        			

            			listaCaractTO.add(caracPartProyTO);            			            			
            		}
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

    public String getUrl() throws NoSuchAlgorithmException {
        String perfilSeg=null; 
        String perfil=null;
        String url = Constantes.URL_SIGEIA;
        HttpSession sesion = Util.getSession();
        
//        List<Object[]> jerarquia  = daoBitacora.jerarquiaTbArea(idArea);
        if(sesion != null)
        {
            
                perfil=sesion.getAttribute("rol").toString();
           
        }
        String secureParam = "usuario=" + Constantes.USUARIO_SIGEIA
                + "&password=" + Constantes.PASSWORD_SIGEIA
                + "&noFolio=" + proyecto.getProyectoPK().getFolioProyecto()
                + "&claveProyecto=" + cveProyecto
                + "&version=" + (serialNum + 1)
                + "&tipoTramite=" + Constantes.TRAMITE
                + "&lote=" + proyecto.getProyLote()
                + "&perfil=" + perfil
                + "&estatus=A";


        return url.concat(secureParam);
    }

    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * @return the folioDetalle
     */
    public Proyecto getFolioDetalle() {
        return folioDetalle;
    }

    /**
     * @param folioDetalle the folioDetalle to set
     */
    public void setFolioDetalle(Proyecto folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the criterios
     */
    public List<Object[]> getCriterios() {
        return criterios;
    }

    /**
     * @param criterios the criterios to set
     */
    public void setCriterios(List<Object[]> criterios) {
        this.criterios = criterios;
    }

    /**
     * @return the urlCap2MIA
     */
    public String getUrlCap2MIA() {
        return urlCap2MIA;
    }

    /**
     * @param urlCap2MIA the urlCap2MIA to set
     */
    public void setUrlCap2MIA(String urlCap2MIA) {
        this.urlCap2MIA = urlCap2MIA;
    }

    

    public List<ImpacAmbProyecto> getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(List<ImpacAmbProyecto> evaluacion) {
        this.evaluacion = evaluacion;
    }

    /**
     * @return the edoAfectado
     */
    public List<Object[]> getEdoAfectado() {
        return edoAfectado;
    }

    /**
     * @param edoAfectado the edoAfectado to set
     */
    public void setEdoAfectado(List<Object[]> edoAfectado) {
        this.edoAfectado = edoAfectado;
    }

    
    
     public final List<Object[]> estadosAfetados() {
//      List<Object[]> p = dao.proySigSum(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

      List<Object[]> pObra = dao.mpiosSumObra(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
      List<Object[]> pPredio = dao.mpiosSumPred(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

      setEdoAfectado(null);
      todosEdos = null;
      String mensaje = "";

      Integer id;
//
////      String aux2;
//
      //---------------si trae obras o predios
      if (!pObra.isEmpty() || !pPredio.isEmpty()) {
          //----------si trae obras Y predios
          if (!pObra.isEmpty() && !pPredio.isEmpty()) {
              id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
              System.out.print("Obra afectada --->>>  "+id);
              if (id != 0) {
                  edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
              }
          }
          //----------si trae obras Y NO predios
          if (!pObra.isEmpty() && pPredio.isEmpty()) {
              id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
              System.out.print("Predios afectada --->>>  "+id);
              if (id != 0) {
                  edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
              }
          }
          //----------si NO trae obras Y SI predios
          if (pObra.isEmpty() && !pPredio.isEmpty()) {
              id = Integer.parseInt(pPredio.get(pPredio.size() - 1)[0].toString());
              if (id != 0) {
                  edoAfectado = dao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                  setEdoAfectado(edoAfectado);
              }
          }
      }

      todosEdos = dao.edoTodosAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

      if (pObra.isEmpty() || pPredio.isEmpty()) {
          mensaje = "Debe georrefenciar correctamente su proyecto";
          todosEdos = null;
      }
      if (mensaje.length() != 0) {
          FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Debe georrefenciar correctamente su proyecto", null);
          FacesContext.getCurrentInstance().addMessage(null, message);
      }

      try{
          FacesContext context = FacesContext.getCurrentInstance();
          context.getExternalContext().getSessionMap().put("clvMunici", todosEdos.get(0)[1]);
      }catch(Exception e3){
          
      }
      
      return todosEdos;
  }

    
    
    /** 
     * @return the tipoDomic
     */
    public String getTipoDomic() {
        return tipoDomic;
    }

    /**
     * @param tipoDomic the tipoDomic to set
     */
    public void setTipoDomic(String tipoDomic) {
        this.tipoDomic = tipoDomic;
    }

    /**
     * @return the catVialidad
     */
    public CatVialidad getCatVialidad() {
        return catVialidad;
    }

    /**
     * @param catVialidad the catVialidad to set
     */
    public void setCatVialidad(CatVialidad catVialidad) {
        this.catVialidad = catVialidad;
    }

    /**
     * @return the catTipoAsen
     */
    public CatTipoAsen getCatTipoAsen() {
        return catTipoAsen;
    }

    /**
     * @param catTipoAsen the catTipoAsen to set
     */
    public void setCatTipoAsen(CatTipoAsen catTipoAsen) {
        this.catTipoAsen = catTipoAsen;
    }

    // Selecciona referencia
    public String seleccionaDomicilio() {
        setLocaliz((Boolean) false);
        setLocalizNo((Boolean) false);
        String tipoDom = "";
        if (proyecto.getProyDomEstablecido() != null) {
            //JOptionPane.showMessageDialog(null, "seleccionaDomicilio:  " + proyecto.getProyDomEstablecido(), "Error", JOptionPane.INFORMATION_MESSAGE);

            //Carga Normas Oficiales
            if (proyecto.getProyDomEstablecido().toString().equals("S")) {  //SI tiene domicilio establecido
                setLocaliz((Boolean) true);
                setLocalizNo((Boolean) false);
                tipoDom = "Si";
                //JOptionPane.showMessageDialog(null, "localiza:  " + localiz + "  No localiz:  "+ localizNo, "Error", JOptionPane.INFORMATION_MESSAGE);
            }
            if (proyecto.getProyDomEstablecido().toString().equals("N")) {  //NO tiene domicilio establecido
                setLocalizNo((Boolean) true);
                setLocaliz((Boolean) false);
                tipoDom = "No";
                //JOptionPane.showMessageDialog(null, "localiza:  " + localiz + "  No localiz:  "+ localizNo, "Error", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        return tipoDom;
    }

    /**
     * @return the localiz
     */
    public Boolean getLocaliz() {
        return localiz;
    }

    /**
     * @param localiz the localiz to set
     */
    public void setLocaliz(Boolean localiz) {
        this.localiz = localiz;
    }

    /**
     * @return the localizNo
     */
    public Boolean getLocalizNo() {
        return localizNo;
    }

    /**
     * @param localizNo the localizNo to set
     */
    public void setLocalizNo(Boolean localizNo) {
        this.localizNo = localizNo;
    }

    /**
     * @return the anexosProyecto
     */
    public List<AnexosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<AnexosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    public List<Object[]> getProySigSupProy() {

        //Carga preguntas segun el id de la nom
        List<Object[]> proySigSupProy = dao.proySig2(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        return proySigSupProy;
    }
    
    public List<Object[]> getProySigSupProyEva() {

        //Carga preguntas segun el id de la nom
        List<Object[]> proySigSupProyEva = dao.proySigSupProyecto(getProyecto().getProyectoPK().getFolioProyecto(), (short)2);

        return proySigSupProyEva;
    }

    public List<Object[]> getProySigSupProyPredios() {
        List<Object[]> proySigSupProyPredios = dao.proySigSumPredios(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        return proySigSupProyPredios;
    }

    public List<Object[]> getProySigSupProyObras() {
        List<Object[]> proySigSupProyObras = dao.proySigSumObras(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        return proySigSupProyObras;
    }
    
    public List<Object[]> getProySigSupProyPrediosEva() {
        List<Object[]> proySigSupProyPrediosEva = dao.proySigSumPredios(getProyecto().getProyectoPK().getFolioProyecto(), (short)2);
        return proySigSupProyPrediosEva;
    }

    public List<Object[]> getProySigSupProyObrasEva() {
        List<Object[]> proySigSupProyObrasEva = dao.proySigSumObras(getProyecto().getProyectoPK().getFolioProyecto(), (short)2);
        return proySigSupProyObrasEva;
    }

    public String getPorFederal() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;

        if (f == 0) {
            return "0";
        }

        double out = (f / t) * 100;
        return ("" + df.format(out));
    }

    public String getPorEstatal() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;

        if (e == 0) {
            return "0";
        }
        double out = ((e / t) * 100);
        return ("" + df.format(out));
    }

    public String getPorMunicipal() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;

        if (m == 0) {
            return "0";
        }
        double out = (m / t) * 100;
        return ("" + df.format(out));
    }

    public String getPorPrivada() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;
        if (p == 0) {
            return "0";
        }
        double out = (p / t) * 100;
        return ("" + df.format(out));
    }

    public String getPorTot() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;
        if (t == 0) {
            return ("0");

        }
        return ("100");
    }

    /**
     * @return the cveProyecto
     */
    public String getCveProyecto() {
        return cveProyecto;
    }

    /**
     * @param cveProyecto the cveProyecto to set
     */
    public void setCveProyecto(String cveProyecto) {
        this.cveProyecto = cveProyecto;
    }

    /**
     * @return the anexosProyecto2
     */
    public List<AnexosProyecto> getAnexosProyecto2() {
        return anexosProyecto2;
    }

    /**
     * @param anexosProyecto2 the anexosProyecto2 to set
     */
    public void setAnexosProyecto2(List<AnexosProyecto> anexosProyecto2) {
        this.anexosProyecto2 = anexosProyecto2;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }
    /**
     * @return the todosEdos
     */
    public List<Object[]> getTodosEdos() {
        return todosEdos;
    }

    /**
     * @param todosEdos the todosEdos to set
     */
    public void setTodosEdos(List<Object[]> todosEdos) {
        this.todosEdos = todosEdos;
    }
    
    public void informacionAdicional(){
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String info = params.get("info");
        String[] tmp = info.split(","); //[5]        
        String res = daoGeneral.getInformacionAdicional(Short.parseShort(tmp[0]),
                Short.parseShort(tmp[1]),
                Short.parseShort(tmp[2]),
                Short.parseShort(tmp[3]),
                tmp[4]);
        
        this.resInfoAdicional = res;
    }
    
    private final DgiraMiaeDaoGeneral daoGeneral = new DgiraMiaeDaoGeneral();
    private String resInfoAdicional;

    public String getResInfoAdicional() {
        return resInfoAdicional;
    }

    public void setResInfoAdicional(String resInfoAdicional) {
        this.resInfoAdicional = resInfoAdicional;
    }
    private String ubicActual;

    public String getUbicActual() {
        return ubicActual;
    }

    public void setUbicActual(String ubicActual) {
        this.ubicActual = ubicActual;
    }
    
    public void remoteInformacionAdicional() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        String ubicInfoAdic = ctxt.getExternalContext().getRequestParameterMap().get("ubicInfoAdic");
        
        System.out.println("ubicInfoAdic --> " + ubicInfoAdic);
        
        RequestContext reqContEnv = RequestContext.getCurrentInstance();
        reqContEnv.execute("infoAdicionalResultado('1')");
        
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public List<MedPrevImpactProy> getMedPreventivas() {
        return medPreventivas;
    }

	/**
	 * @return the archivosUbicacion
	 */
	public List<ArchivosProyecto> getArchivosUbicacion() {
		return archivosUbicacion;
	}

	/**
	 * @param archivosUbicacion the archivosUbicacion to set
	 */
	public void setArchivosUbicacion(List<ArchivosProyecto> archivosUbicacion) {
		this.archivosUbicacion = archivosUbicacion;
	}

	/**
	 * @return the inversionE
	 */
	public List<InversionEtapas> getInversionE() {
		return inversionE;
	}

	/**
	 * @param inversionE the inversionE to set
	 */
	public void setInversionE(List<InversionEtapas> inversionE) {
		this.inversionE = inversionE;
	}

	/**
	 * @return the inversionEtapas
	 */
	public InversionEtapas getInversionEtapas() {
		return inversionEtapas;
	}

	/**
	 * @param inversionEtapas the inversionEtapas to set
	 */
	public void setInversionEtapas(InversionEtapas inversionEtapas) {
		this.inversionEtapas = inversionEtapas;
	}

	/**
	 * @return the proyectoTO
	 */
	public ProyectoTO getProyectoTO() {
		return proyectoTO;
	}

	/**
	 * @param proyectoTO the proyectoTO to set
	 */
	public void setProyectoTO(ProyectoTO proyectoTO) {
		this.proyectoTO = proyectoTO;
	}

	/**
	 * @return the proyInversionPE
	 */
	public boolean isProyInversionPE() {
		return proyInversionPE;
	}

	/**
	 * @param proyInversionPE the proyInversionPE to set
	 */
	public void setProyInversionPE(boolean proyInversionPE) {
		this.proyInversionPE = proyInversionPE;
	}
	
	/**
	 * @return the serviciosReqPrep
	 */
	public List<ServicioProyecto> getServiciosReqPrep() {
		return serviciosReqPrep;
	}

	/**
	 * @param serviciosReqPrep the serviciosReqPrep to set
	 */
	public void setServiciosReqPrep(List<ServicioProyecto> serviciosReqPrep) {
		this.serviciosReqPrep = serviciosReqPrep;
	}

	/**
	 * @return the serviciosReqCons
	 */
	public List<ServicioProyecto> getServiciosReqCons() {
		return serviciosReqCons;
	}

	/**
	 * @param serviciosReqCons the serviciosReqCons to set
	 */
	public void setServiciosReqCons(List<ServicioProyecto> serviciosReqCons) {
		this.serviciosReqCons = serviciosReqCons;
	}

	/**
	 * @return the serviciosReqOper
	 */
	public List<ServicioProyecto> getServiciosReqOper() {
		return serviciosReqOper;
	}

	/**
	 * @param serviciosReqOper the serviciosReqOper to set
	 */
	public void setServiciosReqOper(List<ServicioProyecto> serviciosReqOper) {
		this.serviciosReqOper = serviciosReqOper;
	}

	/**
	 * @return the serviciosReqAban
	 */
	public List<ServicioProyecto> getServiciosReqAban() {
		return serviciosReqAban;
	}

	/**
	 * @param serviciosReqAban the serviciosReqAban to set
	 */
	public void setServiciosReqAban(List<ServicioProyecto> serviciosReqAban) {
		this.serviciosReqAban = serviciosReqAban;
	}

	/**
	 * @return the listaCaractTO
	 */
	public List<CaracPartProyTO> getListaCaractTO() {
		return listaCaractTO;
	}

	/**
	 * @param listaCaractTO the listaCaractTO to set
	 */
	public void setListaCaractTO(List<CaracPartProyTO> listaCaractTO) {
		this.listaCaractTO = listaCaractTO;
	}

	/**
	 * @return the caracPartProyDAO
	 */
	public CaracPartProyDAO getCaracPartProyDAO() {
		return caracPartProyDAO;
	}

	/**
	 * @param caracPartProyDAO the caracPartProyDAO to set
	 */
	public void setCaracPartProyDAO(CaracPartProyDAO caracPartProyDAO) {
		this.caracPartProyDAO = caracPartProyDAO;
	}

	/**
	 * @return the sigeiaDao
	 */
	public SigeiaDao getSigeiaDao() {
		return sigeiaDao;
	}

	/**
	 * @param sigeiaDao the sigeiaDao to set
	 */
	public void setSigeiaDao(SigeiaDao sigeiaDao) {
		this.sigeiaDao = sigeiaDao;
	}

	/**
	 * @return the naturalezaProy
	 */
	public String getNaturalezaProy() {
		return naturalezaProy;
	}

	/**
	 * @param naturalezaProy the naturalezaProy to set
	 */
	public void setNaturalezaProy(String naturalezaProy) {
		this.naturalezaProy = naturalezaProy;
	}
	
}
