/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view.visorMia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.utils.GenericConstants;

/**
 *
 * @author Paty
 */
@ManagedBean(name = "miaCap6")
@ViewScoped
public class MiaCapitulo6 {
    private final VisorDao dao = new VisorDao();
    private final DgiraMiaeDaoGeneral daoGeneral = new DgiraMiaeDaoGeneral();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private Proyecto folioDetalle;
    private Proyecto proyecto;
    private String urlCap6MIA;
    
    private int vistaVersion;
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    
	// Archivos de Medidas preventivas y de mitigacion de los impactos ambientales
	private List<ArchivosProyecto> archivosMedPrevMitiImpAmb = new ArrayList<ArchivosProyecto>();

    public MiaCapitulo6() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        pramBita = params.get("bitaNum").toString();
        vistaVersion = (params.get("vistaVersion") != null) ? 
                Integer.parseInt((String)params.get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
        if (pramBita != null) 
        { 
            urlCap6MIA = "../capitulosMIA/capitulo5MIA.xhtml?bitaNum=" + pramBita;
            switch (vistaVersion) {
                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
                    folioDetalle = dao.obtieneProyectoUltimaVersion(pramBita);
                    break;
                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
                    folioDetalle = dao.obtieneProyectoVersionAnterior(pramBita);
                    break;
            }    
            
                if (folioDetalle != null)  
                {
                    folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                    serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                    //JOptionPane.showMessageDialog(null, "folio:  " + folioNum + "   serial: " + serialNum, "Error", JOptionPane.INFORMATION_MESSAGE);
                    //----proyecto en sesion
                    proyecto = folioDetalle;//dao.cargaProyecto(folioNum, serialNum);
                }
                
            mapInfoAdicional = daoGeneral.generaMapaInfoAdicionalCapitulo(pramBita, "6");
        }
        
        archivosMedPrevMitiImpAmb = daoGeneral.archivosProyecto((short)6, (short)1, (short)0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
    }

    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * @return the folioDetalle
     */
    public Proyecto getFolioDetalle() {
        return folioDetalle;
    }

    /**
     * @param folioDetalle the folioDetalle to set
     */
    public void setFolioDetalle(Proyecto folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the urlCap6MIA
     */
    public String getUrlCap6MIA() {
        return urlCap6MIA;
    }

    /**
     * @param urlCap6MIA the urlCap6MIA to set
     */
    public void setUrlCap6MIA(String urlCap6MIA) {
        this.urlCap6MIA = urlCap6MIA;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

	/**
	 * @return the archivosMedPrevMitiImpAmb
	 */
	public List<ArchivosProyecto> getArchivosMedPrevMitiImpAmb() {
		return archivosMedPrevMitiImpAmb;
	}

	/**
	 * @param archivosMedPrevMitiImpAmb the archivosMedPrevMitiImpAmb to set
	 */
	public void setArchivosMedPrevMitiImpAmb(List<ArchivosProyecto> archivosMedPrevMitiImpAmb) {
		this.archivosMedPrevMitiImpAmb = archivosMedPrevMitiImpAmb;
	}

}
