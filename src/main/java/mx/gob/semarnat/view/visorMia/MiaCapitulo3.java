/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorMia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.AnpEstatal;
import mx.gob.semarnat.model.dgira_miae.AnpFederal;
import mx.gob.semarnat.model.dgira_miae.AnpMuicipal;
import mx.gob.semarnat.model.dgira_miae.AnpProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArticuloReglaAnp;
import mx.gob.semarnat.model.dgira_miae.ConvenioProy;
import mx.gob.semarnat.model.dgira_miae.DisposicionProyecto;
import mx.gob.semarnat.model.dgira_miae.InstrUrbanos;
import mx.gob.semarnat.model.dgira_miae.LeyFedEstProyecto;
import mx.gob.semarnat.model.dgira_miae.NormaProyecto;
import mx.gob.semarnat.model.dgira_miae.Numeral;
import mx.gob.semarnat.model.dgira_miae.OeGralTerrit;
import mx.gob.semarnat.model.dgira_miae.OeMarinos;
import mx.gob.semarnat.model.dgira_miae.OePoligEnvol;
import mx.gob.semarnat.model.dgira_miae.OeRegionales1;
import mx.gob.semarnat.model.dgira_miae.OeRegionales2;
import mx.gob.semarnat.model.dgira_miae.OeRegionales3;
import mx.gob.semarnat.model.dgira_miae.PdumProyecto;
import mx.gob.semarnat.model.dgira_miae.PoetmProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ReglamentoProyecto;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.view.Util.ANPu;
import mx.gob.semarnat.view.Util.PDUu;
import mx.gob.semarnat.view.Util.POETu;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "miaCap3")
@SuppressWarnings("serial")
@ViewScoped
public class MiaCapitulo3 {

    private final DgiraMiaeDaoGeneral dao = new DgiraMiaeDaoGeneral();
    private final VisorDao daoVisor = new VisorDao();

    private Proyecto proyecto = new Proyecto();
    private final String bitacora;
    //leyes
    private List<LeyFedEstProyecto> leyesFederal = new ArrayList();
    private List<LeyFedEstProyecto> leyesEstatal = new ArrayList();
    
    private List<Object> leyeslist= new ArrayList();
    
    //reglamentos
    private List<ReglamentoProyecto> reglamentos = new ArrayList();
    private List<Object> reglaList = new ArrayList();
    //normas y numerales
    private List<NormaProyecto> normas = new ArrayList();
    private List<Numeral> numerales = new ArrayList();
    
    private List<Object> normalist = new ArrayList();
    private List<Object> numeralist = new ArrayList();
    
    //poet
    private List<POETu> poet = new ArrayList();
    private List<PoetmProyecto> poetProyecto = new ArrayList<PoetmProyecto>();
    
    private List<Object>listPOETList = new ArrayList<Object>();
    
    //ANP
    private List<ANPu> anpu = new ArrayList();
    private List<ArticuloReglaAnp> anpArticulo = new ArrayList();
    private List<ArticuloReglaAnp> anpRegla = new ArrayList();
    
    private List<AnpProyecto> lista = new ArrayList<>();
    
    private List<Object> listANP = new ArrayList<Object>();
    
    //PDU
    private List<PDUu> pdu1 = new ArrayList();
    private List<PdumProyecto> pdus = new ArrayList<PdumProyecto>();
    
    private List<Object> listPDU = new ArrayList<Object>();
    
    //acuerdos
    private List<ConvenioProy> convenios = new ArrayList();

    private List<ConvenioProy> convenioslist = new ArrayList();
    
    //disposiciones
    private List<DisposicionProyecto> disposiciones = new ArrayList();
    
    private List<DisposicionProyecto> dispolist = new ArrayList();
    
    private int vistaVersion;	
    
    private String vinculacion;
    
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    
  //Archivos de Otras Disposiciones
    private List<ArchivosProyecto> archivosDisposiciones = new ArrayList<ArchivosProyecto>();

    public MiaCapitulo3() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        bitacora = ec.getRequestParameterMap().get("bitaNum");
        
        vistaVersion = ( ec.getRequestParameterMap().get("vistaVersion") != null) ? 
                Integer.parseInt((String)ec.getRequestParameterMap().get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;        
        
        if (bitacora != null) {
            if (!dao.lista_namedQuery("ProyectoD.findByBitacoraProyecto", new Object[]{bitacora}, new String[]{"bitacoraProyecto"}).isEmpty()) {
            switch (vistaVersion) {
                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
                    proyecto = daoVisor.obtieneProyectoVersionAnterior(bitacora);
                    break;
                    
                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
                    proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);
                    break;
                    
                case GenericConstants.VISTA_EVALUADOR_1ER_INGRESO:
                case GenericConstants.VISTA_EVALUADOR_INFO_ADICIONAL:
                    //Se obtiene la información del proyecto de la versión anterior del promovente (1, 3, ...)
                    if (vistaVersion == GenericConstants.VISTA_EVALUADOR_1ER_INGRESO) {//Si versión 2
                        proyecto = daoVisor.obtieneProyectoVersionAnterior(bitacora); //versión 1
                    } else {//Si versión 4
                        proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);//versión 3
                    }

                    proyecto.getProyectoPK().setSerialProyecto((short)vistaVersion);

                    break;
            }                
            
                if (proyecto != null) {
                    //<editor-fold defaultstate="collapsed" desc="Leyes">
                    leyesFederal = (List<LeyFedEstProyecto>) dao.lista_namedQuery("LeyFedEstProyecto.findByFolioSerial",
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                            new String[]{"folio", "serial"});
                    leyesEstatal = (List<LeyFedEstProyecto>) dao.lista_namedQuery("LeyFedEstProyecto.findByFolioSerial2",
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                            new String[]{"folio", "serial"});
                    
                  leyeslist=dao.leyess(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());  
                  
                    
                    //</editor-fold>
                    //<editor-fold desc="Reglamentos" defaultstate="collapsed">
                  //  reglamentos = (List<ReglamentoProyecto>) dao.lista_namedQuery("ReglamentoProyecto.findByFolioProyecto",
                    //        new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                      //      new String[]{"folio", "serial"});
                  
                  reglamentos = dao.reglamentos(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                		  
                		  
                    reglaList=dao.reglam(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    //</editor-fold>
                    //<editor-fold desc="Normas y numerales" defaultstate="collapsed">
                    //normas = (List<NormaProyecto>) dao.lista_namedQuery("NormaProyecto.findByFolioSerial", new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},new String[]{"folio", "serial"});
                    
                    normas = dao.consultarNormas(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
                    
                    normalist=dao.normass(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    numeralist=dao.nums(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    
//                    numerales =  (List<Numeral>) dao.lista_namedQuery("Numeral.findByFolioSerial",
//                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
//                            new String[]{"folio", "serial"});//miaDao.getNumerales(folioProyecto, serialProyecto);
                    
                    
                    //</editor-fold>
                    //<editor-fold desc="POET" defaultstate="collapsed">
                    cargaPoets();
                    //</editor-fold>
                    //<editor-fold desc="ANP" defaultstate="collapsed"> 
                    cargaANPu();
                    //</editor-fold>
                    //<editor-fold desc="PDU" defaultstate="collapsed">
                    cargaPDUS();
                    //</editor-fold>
                    //<editor-fold desc="Acuerdos" defaultstate="collapsed">
                    convenios = (List<ConvenioProy>) dao.lista_namedQuery("ConvenioProy.findByFolioSerial",
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                            new String[]{"folio", "serial"});
                    //</editor-fold>
                    
                    
                    convenioslist = dao.conv(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    
                    //<editor-fold desc="Disposiciones" defaultstate="collapsed">
                    disposiciones = (List<DisposicionProyecto>) dao.lista_namedQuery("DisposicionProyecto.findByFolioSerial",
                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                            new String[]{"folio", "serial"});
                    //</editor-fold>
                    
                    dispolist = dao.dispo(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                }
            }
            
            mapInfoAdicional = dao.generaMapaInfoAdicionalCapitulo(bitacora, "3");
        
        }
        
        archivosDisposiciones = dao.archivosProyecto((short)3, (short)8, (short)0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
    }

    //<editor-fold desc="Carga POET's" defaultstate="collapsed">
//    private void cargaPoets() {
//        poet.clear();  
//        List<OeRegionales1> gral = (List<OeRegionales1>) dao.lista_namedQuery("OeRegionales1.findByFolioCveVer",
//                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
//                new String[]{
//                    "folio", "version"
//                });
//        for (OeRegionales1 o : gral) {
//            POETu p = new POETu();
//            p.setTipo("TIPO_OE");
//            p.setNombreInstrumento(o.getNomOe());
//            p.setNumNom(o.getUga());
//            p.setPoliticaAmbiental(o.getPolitica());
//            p.setUso(o.getUsoPred());
//            p.setCriterios(o.getCriterios());
//            p.setCompatibles(o.getComp());
//            p.setVinculacion("");
//            poet.add(p);
//        }
//        List<PoetmProyecto> pmp = (List<PoetmProyecto>) dao.lista_namedQuery("PoetmProyecto.findByFolioSerial",
//                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
//                new String[]{"folio", "serial"});
//        for (PoetmProyecto o : pmp) {
//            POETu p = new POETu();
//            p.setTipo("POEM_TIPO");
//            p.setNombreInstrumento(o.getPoetmNombreInstrumento());
//            p.setNumNom(o.getPoetmNombreUga());
//            p.setPoliticaAmbiental(o.getPoetmPoliticaAmbiental());
//            p.setUso(o.getPoetmUsoPredominante());
//            p.setCriterios(o.getPoetmCriterio());
//            p.setCompatibles(o.getPoetmCompatible());
//            p.setVinculacion(o.getPoetmVinculacion());
//            p.setId(o.getPoetmProyectoPK().getPoetmId());
//            poet.add(p);
//        }
//        //----versión evaluador
//      
//        List<OeRegionales1> gralEva = (List<OeRegionales1>) dao.lista_namedQuery("OeRegionales1.findByFolioCveVer",
//                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), (short)2},
//                new String[]{
//                    "folio", "version"
//                });
//        for (OeRegionales1 o : gralEva) {
//            POETu p = new POETu();
//            p.setTipo("TIPO_OE");
//            p.setNombreInstrumento(o.getNomOe());
//            p.setNumNom(o.getUga());
//            p.setPoliticaAmbiental(o.getPolitica());
//            p.setUso(o.getUsoPred());
//            p.setCriterios(o.getCriterios());
//            p.setCompatibles(o.getComp());
//            p.setVinculacion("");
//            
//        }
//    }//</editor-fold>

    
    private void cargaPoets() {
    	
    	listPOETList = dao.poet(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    	
        poetProyecto = dao.getPoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        if (poetProyecto.isEmpty()) {
            poet.clear();
            List<OeRegionales1> gral = dao.getOeReg1(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (OeRegionales1 o : gral) {
                POETu p = new POETu();
                p.setTipo("Regional 1");
                p.setNombreInstrumento(o.getNomOe());
                p.setNumNom(o.getUga());
                p.setPoliticaAmbiental(o.getPolitica());
                p.setUso(o.getUsoPred());
                p.setCriterios(o.getCriterios());
                p.setCompatibles(o.getComp());
                p.setVinculacion("");
                poet.add(p);
            }
            List<OeRegionales2> gral2 = dao.getOeReg2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (OeRegionales2 o : gral2) {
                POETu p = new POETu();
                p.setTipo("Regional 2");
                p.setNombreInstrumento(o.getNomOe());
                p.setNumNom(o.getUga());
                p.setPoliticaAmbiental(o.getPolitica());
                p.setUso(o.getUsoPred());
                p.setCriterios(o.getCriterios());
                p.setCompatibles(o.getComp());
                p.setVinculacion("");
                poet.add(p);
            }
            List<OeRegionales3> gral3 = dao.getOeReg3(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (OeRegionales3 o : gral3) {
                POETu p = new POETu();
                p.setTipo("Regional 3");
                p.setNombreInstrumento(o.getNomOe());
                p.setNumNom(o.getUga());
                p.setPoliticaAmbiental(o.getPolitica());
                p.setUso(o.getUsoPred());
                p.setCriterios(o.getCriterios());
                p.setCompatibles(o.getComp());
                p.setVinculacion("");
                poet.add(p);
            }
            List<OeGralTerrit> gral4 = dao.getOeGral(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (OeGralTerrit o : gral4) {
                POETu p = new POETu();
                p.setTipo("General");
                p.setNombreInstrumento(o.getNom());
                p.setNumNom("" + o.getUab());
                p.setPoliticaAmbiental(o.getPolitica());
                p.setUso(o.getDescrip());
                p.setCriterios("");
                p.setCompatibles(o.getComp());
                p.setVinculacion("");
                poet.add(p);
            }
            List<OeMarinos> gral5 = dao.getOeMarinos(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (OeMarinos o : gral5) {
                POETu p = new POETu();
                p.setTipo("Marino");
                p.setNombreInstrumento(o.getNomOe());
                p.setNumNom("" + o.getUga());
                p.setPoliticaAmbiental(o.getPolitica());
                p.setUso(o.getUsoPred());
                p.setCriterios(o.getCriterios());
                p.setCompatibles(o.getComp());
                p.setVinculacion("");
                poet.add(p);
            }
            List<OePoligEnvol> gral6 = dao.getOePoligEnvol(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (OePoligEnvol o : gral6) {
                POETu p = new POETu();
                p.setTipo("Local");
                p.setNombreInstrumento(o.getNomOe());
                p.setNumNom("" + o.getUga());
                p.setPoliticaAmbiental(o.getPolitica());
                p.setUso(o.getUsoPred());
                p.setCriterios(o.getCriterios());
                p.setCompatibles(o.getComp());
                p.setVinculacion("");
                poet.add(p);
            }


            for (POETu u : poet) {
                Short id = dao.getMaxPoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                PoetmProyecto m = new PoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
//                m.setPoetmCompatible(u.getCompatibles());
                m.setPoetmCriterio(u.getCriterios());
                m.setPoetmNombreInstrumento(u.getNombreInstrumento());
                m.setPoetmNombreUga(u.getNumNom());
                m.setPoetmPoliticaAmbiental(u.getPoliticaAmbiental());
                m.setPoetmTipo(u.getTipo());
                m.setPoetmUsoPredominante(u.getUso());
                m.setPoetmVinculacion(u.getVinculacion());
                try {
                   // dao.merge(m);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                poetProyecto.add(m);
            }
        }
    }
    
    //<editor-fold desc="Carga ANP's" defaultstate="collapsed">
    private void cargaANPu() {
        
    	 
        listANP =dao.anp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        lista = dao.consultaANP(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    	
    	List<AnpEstatal> e = (List<AnpEstatal>) dao.lista_namedQuery("AnpEstatal.findByFolioCveSerial",
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                new String[]{"folio", "version"});
        
        for (AnpEstatal t : e) {
            ANPu u = new ANPu();
            u.setAPN(t.getNombre());
            u.setCategoria(t.getCategoria());
            u.setCompatible(t.getComp());
            u.setCriterio("");
            u.setFechaDecreto(t.getFDec());
            u.setFechaPrograma(t.getFechaHora());
            u.setVinculacion(t.getDescrip());
            u.setId(t.getAnpEstatalPK().getIdr());
            u.setTipo("");
            anpu.add(u);
        }
        List<AnpMuicipal> e2 = (List<AnpMuicipal>) dao.lista_namedQuery("AnpMuicipal.findByFolioCveVersion",
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                new String[]{"folio", "version"});
        for (AnpMuicipal t : e2) {
            ANPu u = new ANPu();
            u.setAPN(t.getNombre());
            u.setCategoria(t.getCategoria());
            u.setCompatible(t.getComp());
            u.setCriterio("");
            u.setFechaDecreto(t.getFDec());
            u.setFechaPrograma(t.getFechaHora());
            u.setVinculacion(t.getDescrip());
            u.setId(t.getAnpMuicipalPK().getIdr());
            u.setTipo("");
            anpu.add(u);
        }
        List<AnpFederal> e3 = (List<AnpFederal>) dao.lista_namedQuery("AnpFederal.findByFolioCveVersion",
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                new String[]{"folio", "version"});
        for (AnpFederal t : e3) {
            ANPu u = new ANPu();
            u.setAPN(t.getNombre());
            u.setCategoria(t.getCatDecret());
            u.setCompatible(t.getComp());
            u.setCriterio("");
            u.setFechaDecreto(t.getUltDecret());
            u.setFechaPrograma(t.getFechaHora());
            u.setVinculacion(t.getDescrip());
            u.setId(t.getAnpFederalPK().getIdr());
            u.setTipo("");
            anpu.add(u);
        }
        //----versión evaluador
        
        /*anpArticulo = (List<ArticuloReglaAnp>) dao.lista_namedQuery("ArticuloReglaAnp.findBySerialSerialProyecto",
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                new String[]{"folio", "serial"});*/
        
        anpArticulo = dao.getArticuloReglaAnp(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), "A");
        
        /*anpRegla = (List<ArticuloReglaAnp>) dao.lista_namedQuery("ArticuloReglaAnp.findBySerialSerialProyecto",
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                new String[]{"folio", "serial"});*/
        
        anpRegla=dao.getArticuloReglaAnp(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), "R");

    }
    //</editor-fold>

    //<editor-fold desc="Carga PDU's" defaultstate="collapsed">
    private void cargaPDUS() {
//        List<InstrUrbanos> u = (List<InstrUrbanos>) dao.lista_namedQuery("InstrUrbanos.findByFolVerCve",
//                new Object[]{proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto() },
//                new String[]{"fol", "clveProy"});
//        for (InstrUrbanos t : u) {
//            PDUu u2 = new PDUu();
//            u2.setClaveUsos(t.getClvUsocp());
//            u2.setNombrePlan(t.getInstUrb());
//            u2.setUsos(t.getUsoclaspol());
//            u2.setTipo("InsUrb");
//            pdu1.add(u2);
//        }
        
    	listPDU=dao.pdu(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    	
        pdus = dao.getPdumProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        if (pdus == null || pdus.isEmpty()) {
            List<InstrUrbanos> u = dao.getInstrUbr(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (InstrUrbanos t : u) {
                Short id = dao.getMaxPdumProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
                PdumProyecto p = new PdumProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                p.setPdumClaveUsos(t.getClvUsocp());
                p.setPdumNombre(t.getInstUrb());
                p.setPdumUsos(t.getUsoclaspol());
//                try {
//                    getMiaDao().merge(p);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                pdus.add(p);
            }
        }
        
        
        //----------versión del evaluador
        
        List<InstrUrbanos> uEva = (List<InstrUrbanos>) dao.lista_namedQuery("InstrUrbanos.findByFolVerCve",
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(),(short)2 },
                new String[]{"fol", "clveProy"});
        for (InstrUrbanos t : uEva) {
            PDUu u2 = new PDUu();
            u2.setClaveUsos(t.getClvUsocp());
            u2.setNombrePlan(t.getInstUrb());
            u2.setUsos(t.getUsoclaspol());
            u2.setTipo("InsUrb");
            
        }
        

        List<PdumProyecto> u2 = (List<PdumProyecto>) dao.lista_namedQuery("PdumProyecto.findByFolSer",
                new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                new String[]{"folio", "serial"});
        for (PdumProyecto t : u2) {
            PDUu u3 = new PDUu();
            u3.setClaveUsos(t.getPdumClaveUsos());
            u3.setCos(t.getPdumCos());
            u3.setCus(t.getPdumCus());
            u3.setNombrePlan(t.getPdumNombre());
            u3.setTipo("Pdum");
            u3.setUsos(t.getPdumUsos());
            u3.setVinculacion(t.getPdumVinculacion());
        }
    }//</editor-fold>
    
    
    //======================================================================================
    public void MostrarVinculacion() {
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    	
    	vinculacion = params.get("idItem").toString();
		
		 
    }
    //======================================================================================
    
    
    

    //<editor-fold defaultstate="collapsed" desc="Getters & Setters">
    public List<LeyFedEstProyecto> getLeyesEstatal() {
        return leyesEstatal;
    }

    public void setLeyesEstatal(List<LeyFedEstProyecto> leyesEstatal) {
        this.leyesEstatal = leyesEstatal;
    }

    public List<PDUu> getPdu1() {
        return pdu1;
    }

    public void setPdu1(List<PDUu> pdu1) {
        this.pdu1 = pdu1;
    }

    public List<DisposicionProyecto> getDisposiciones() {
        return disposiciones;
    }

    public void setDisposiciones(List<DisposicionProyecto> disposiciones) {
        this.disposiciones = disposiciones;
    }

    public List<ConvenioProy> getConvenios() {
        return convenios;
    }

    public void setConvenios(List<ConvenioProy> convenios) {
        this.convenios = convenios;
    }

    public List<ArticuloReglaAnp> getAnpArticulo() {
        return anpArticulo;
    }

    public void setAnpArticulo(List<ArticuloReglaAnp> anpArticulo) {
        this.anpArticulo = anpArticulo;
    }

    public List<ANPu> getAnpu() {
        return anpu;
    }

    public void setAnpu(List<ANPu> anpu) {
        this.anpu = anpu;
    }

    public List<ArticuloReglaAnp> getAnpRegla() {
        return anpRegla;
    }

    public void setAnpRegla(List<ArticuloReglaAnp> anpRegla) {
        this.anpRegla = anpRegla;
    }

    public List<POETu> getPoet() {
        return poet;
    }

    public void setPoet(List<POETu> poet) {
        this.poet = poet;
    }

    public List<NormaProyecto> getNormas() {
        return normas;
    }

    public List<Numeral> getNumerales() {
        return numerales;
    }

    public List<ReglamentoProyecto> getReglamentos() {
        return reglamentos;
    }

    public List<LeyFedEstProyecto> getLeyesFederal() {
        return leyesFederal;
    }

    public void setLeyesFederal(List<LeyFedEstProyecto> leyesFederal) {
        this.leyesFederal = leyesFederal;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the pdus
     */
    public List<PdumProyecto> getPdus() {
        return pdus;
    }

    /**
     * @param pdus the pdus to set
     */
    public void setPdus(List<PdumProyecto> pdus) {
        this.pdus = pdus;
    }

    /**
     * @return the poetProyecto
     */
    public List<PoetmProyecto> getPoetProyecto() {
        return poetProyecto;
    }

    /**
     * @param poetProyecto the poetProyecto to set
     */
    public void setPoetProyecto(List<PoetmProyecto> poetProyecto) {
        this.poetProyecto = poetProyecto;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

    public String getBitacora() {
        return bitacora;
    }

    //Listas para las consultas
    
	public List<Object> getListPOETList() {
		return listPOETList;
	}

	public void setListPOETList(List<Object> listPOETList) {
		this.listPOETList = listPOETList;
	}

	public List<Object> getListANP() {
		return listANP;
	}

	public void setListANP(List<Object> listANP) {
		this.listANP = listANP;
	}

	public List<Object> getListPDU() {
		return listPDU;
	}

	public void setListPDU(List<Object> listPDU) {
		this.listPDU = listPDU;
	}

	/**
	 * @return the convenioslist
	 */
	public List<ConvenioProy> getConvenioslist() {
		return convenioslist;
	}

	/**
	 * @param convenioslist the convenioslist to set
	 */
	public void setConvenioslist(List<ConvenioProy> convenioslist) {
		this.convenioslist = convenioslist;
	}

	/**
	 * @return the dispolist
	 */
	public List<DisposicionProyecto> getDispolist() {
		return dispolist;
	}

	/**
	 * @param dispolist the dispolist to set
	 */
	public void setDispolist(List<DisposicionProyecto> dispolist) {
		this.dispolist = dispolist;
	}

	public List<Object> getLeyeslist() {
		return leyeslist;
	}

	public void setLeyeslist(List<Object> leyeslist) {
		this.leyeslist = leyeslist;
	}

	public List<Object> getReglaList() {
		return reglaList;
	}

	public void setReglaList(List<Object> reglaList) {
		this.reglaList = reglaList;
	}

	public List<Object> getNormalist() {
		return normalist;
	}

	public void setNormalist(List<Object> normalist) {
		this.normalist = normalist;
	}

	public List<Object> getNumeralist() {
		return numeralist;
	}

	public void setNumeralist(List<Object> numeralist) {
		this.numeralist = numeralist;
	}

	public String getVinculacion() {
		return vinculacion;
	}

	public void setVinculacion(String vinculacion) {
		this.vinculacion = vinculacion;
	}

	/**
	 * @return the archivosDisposiciones
	 */
	public List<ArchivosProyecto> getArchivosDisposiciones() {
		return archivosDisposiciones;
	}

	/**
	 * @param archivosDisposiciones the archivosDisposiciones to set
	 */
	public void setArchivosDisposiciones(List<ArchivosProyecto> archivosDisposiciones) {
		this.archivosDisposiciones = archivosDisposiciones;
	}

	public List<AnpProyecto> getLista() {
		return lista;
	}

	public void setLista(List<AnpProyecto> lista) {
		this.lista = lista;
	}
    
	
}

    //</editor-fold>
