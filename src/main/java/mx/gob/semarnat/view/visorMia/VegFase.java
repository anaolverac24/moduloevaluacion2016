/**
 * 
 */
package mx.gob.semarnat.view.visorMia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Cesar
 *
 */
@Entity
@Table(name="VEG_FASE")
@XmlRootElement
public class VegFase implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 7055928826677813877L;

	@Id
	@GeneratedValue(generator="InvSeq") 
    @SequenceGenerator(name="InvSeq",sequenceName="SEQ_ID_VEG_FASE")  
	@Basic(optional = false)
	@Column(name="ID_VEG_FASE")
	private Integer idVegFase;
	
	@Column(name="VEG_NOMBRE")
	private String vegNombre;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "listaVegFases")
	private List<VegTipo> listaVegTipos = new ArrayList<VegTipo>();

	/**
	 * @return the idVegFase
	 */
	public Integer getIdVegFase() {
		return idVegFase;
	}

	/**
	 * @param idVegFase the idVegFase to set
	 */
	public void setIdVegFase(Integer idVegFase) {
		this.idVegFase = idVegFase;
	}

	/**
	 * @return the vegNombre
	 */
	public String getVegNombre() {
		return vegNombre;
	}

	/**
	 * @param vegNombre the vegNombre to set
	 */
	public void setVegNombre(String vegNombre) {
		this.vegNombre = vegNombre;
	}

	/**
	 * @return the listaVegTipos
	 */
	public List<VegTipo> getListaVegTipos() {
		return listaVegTipos;
	}

	/**
	 * @param listaVegTipos the listaVegTipos to set
	 */
	public void setListaVegTipos(List<VegTipo> listaVegTipos) {
		this.listaVegTipos = listaVegTipos;
	}
}
