/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view.visorMia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoTO;
import mx.gob.semarnat.utils.GenericConstants;


/**
 *
 * @author Paty
 */
@ManagedBean(name = "miaCap8Serial3")
@ViewScoped
public class MiaCapitulo8Serial3 {
	 private final DgiraMiaeDaoGeneral dao1 = new DgiraMiaeDaoGeneral();
    private final VisorDao dao = new VisorDao();
    private final DgiraMiaeDaoGeneral daoGeneral = new DgiraMiaeDaoGeneral();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private Proyecto proyecto;
    private String urlCap8MIA;
    
    private ProyectoTO proyectoTO;
    
    private int vistaVersion;
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    private List<AnexosProyecto> anexosProyOtrAnex = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> anexosProyFotog = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> anexosProyVideos = new ArrayList<AnexosProyecto>();
    private String glosario ;
    
    
    
    private Object glosarioProyecto = new Object();
    
	// Archivos de Otros Anexos
	private List<ArchivosProyecto> archivosOtrosAnexos = new ArrayList<ArchivosProyecto>();
	// Archivos de Fotografias
	private List<ArchivosProyecto> archivosFotografias = new ArrayList<ArchivosProyecto>();
	// Archivos de Videos
	private List<ArchivosProyecto> archivosVideos = new ArrayList<ArchivosProyecto>();
	
	// Cuadros de texto de Identificacion de los Instrumentos Metodologicos y
	// Elementos Tecnicos que Sustentan la Informacion senalada en las
	// Fracciones Anteriores
	private String glosProyecto;
	private String bibliografia;
	private String resManImpAmb;

    @SuppressWarnings("static-access")
	public MiaCapitulo8Serial3() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        pramBita = params.get("bitaNum").toString();

        vistaVersion = GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
        
        if (pramBita != null) 
        { 
            urlCap8MIA = "../capitulosMIA/capitulo5MIA.xhtml?bitaNum=" + pramBita;
            
//            switch (vistaVersion) {
//                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
//                    proyecto = dao.obtieneProyectoUltimaVersion(pramBita);
//                    break;
//                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
//                    proyecto = dao.obtieneProyectoVersionAnterior(pramBita);
//                    break;
//            }  
            
            proyecto = dao.obtieneProyectoUltimaVersion(pramBita);

            if (proyecto != null)  
            {
                folioNum = proyecto.getProyectoPK().getFolioProyecto();
                serialNum = (short)3;
                
                
                //anexosProyOtrAnex anexosProyFotog anexosProyVideos
            
                //8,1,0,0
                short capituloId = (short) 0;
                short subcapituloId = (short) 0;
                short seccionId = (short) 0;
                short apartadoId = (short) 0;
                capituloId = 8;
                subcapituloId = 1;
                seccionId = 0;
                apartadoId = 0;
                anexosProyOtrAnex = dao.getAnexos(capituloId, subcapituloId, seccionId, apartadoId, folioNum, serialNum);
                
                //8,2,0,0
                short capituloId2 = (short) 0;
                short subcapituloId2 = (short) 0;
                short seccionId2 = (short) 0;
                short apartadoId2 = (short) 0;
                capituloId2 = 8;
                subcapituloId2 = 2;
                seccionId2 = 0;
                apartadoId2 = 0;
                anexosProyFotog = dao.getAnexos(capituloId2, subcapituloId2, seccionId2, apartadoId2, folioNum, serialNum);
                
                //8,3,0,0
                short capituloId3 = (short) 0;
                short subcapituloId3 = (short) 0;
                short seccionId3 = (short) 0;
                short apartadoId3 = (short) 0;
                capituloId3 = 8;
                subcapituloId3 = 3;
                seccionId3 = 0;
                apartadoId3 = 0;
                anexosProyVideos = dao.getAnexos(capituloId3, subcapituloId3, seccionId3, apartadoId3, folioNum, serialNum);
                
               glosario = daoGeneral.getGlosario(folioNum, serialNum);
                
                glosarioProyecto= daoGeneral.glosarioPro(folioNum, serialNum);
                
                
            }
            
            mapInfoAdicional = daoGeneral.generaMapaInfoAdicionalCapitulo(pramBita, "8");
                        
        }
        
        archivosOtrosAnexos = daoGeneral.archivosProyecto((short) 8, (short) 1, (short) 0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);

        archivosFotografias = daoGeneral.archivosProyecto((short) 8, (short) 2, (short) 0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);

        archivosVideos = daoGeneral.archivosProyecto((short) 8, (short) 3, (short) 0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
     
        try {
            this.proyectoTO = daoGeneral.proyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        
        /**
         * Datos de los cuadros de texto exriquecido sin las etiquetas y sin el cuadro de texto en el cap 8
         */
        glosProyecto = dao1.parseHtmlParagraph(proyectoTO.getGlosario()).toString().replace("[", "").replace("],", "").replace("]", "");
        
        bibliografia = dao1.parseHtmlParagraph(proyectoTO.getBiblioProy()).toString().replace("[", "").replace("],", "").replace("]", "");
        
        resManImpAmb = dao1.parseHtmlParagraph(proyectoTO.getManifestacionRes()).toString().replace("[", "").replace("],", "").replace("]", "");
        
    }


    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the urlCap8MIA
     */
    public String getUrlCap8MIA() {
        return urlCap8MIA;
    }

    /**
     * @param urlCap8MIA the urlCap8MIA to set
     */
    public void setUrlCap8MIA(String urlCap8MIA) {
        this.urlCap8MIA = urlCap8MIA;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

    /**
     * @return the anexosProyOtrAnex
     */
    public List<AnexosProyecto> getAnexosProyOtrAnex() {
        return anexosProyOtrAnex;
    }

    /**
     * @param anexosProyOtrAnex the anexosProyOtrAnex to set
     */
    public void setAnexosProyOtrAnex(List<AnexosProyecto> anexosProyOtrAnex) {
        this.anexosProyOtrAnex = anexosProyOtrAnex;
    }

    /**
     * @return the anexosProyFotog
     */
    public List<AnexosProyecto> getAnexosProyFotog() {
        return anexosProyFotog;
    }

    /**
     * @param anexosProyFotog the anexosProyFotog to set
     */
    public void setAnexosProyFotog(List<AnexosProyecto> anexosProyFotog) {
        this.anexosProyFotog = anexosProyFotog;
    }

    /**
     * @return the anexosProyVideos
     */
    public List<AnexosProyecto> getAnexosProyVideos() {
        return anexosProyVideos;
    }

    /**
     * @param anexosProyVideos the anexosProyVideos to set
     */
    public void setAnexosProyVideos(List<AnexosProyecto> anexosProyVideos) {
        this.anexosProyVideos = anexosProyVideos;
    }

    /**
     * @return the glosario
     */
    public String getGlosario() {
        return glosario;
    }

    /**
     * @param glosario the glosario to set
     */
    public void setGlosario(String glosario) {
        this.glosario = glosario;
    }

	public Object getGlosarioProyecto() {
		return glosarioProyecto;
	}

	public void setGlosarioProyecto(Object glosarioProyecto) {
		this.glosarioProyecto = glosarioProyecto;
	}

	/**
	 * @return the archivosOtrosAnexos
	 */
	public List<ArchivosProyecto> getArchivosOtrosAnexos() {
		return archivosOtrosAnexos;
	}

	/**
	 * @param archivosOtrosAnexos the archivosOtrosAnexos to set
	 */
	public void setArchivosOtrosAnexos(List<ArchivosProyecto> archivosOtrosAnexos) {
		this.archivosOtrosAnexos = archivosOtrosAnexos;
	}

	/**
	 * @return the archivosFotografias
	 */
	public List<ArchivosProyecto> getArchivosFotografias() {
		return archivosFotografias;
	}

	/**
	 * @param archivosFotografias the archivosFotografias to set
	 */
	public void setArchivosFotografias(List<ArchivosProyecto> archivosFotografias) {
		this.archivosFotografias = archivosFotografias;
	}

	/**
	 * @return the archivosVideos
	 */
	public List<ArchivosProyecto> getArchivosVideos() {
		return archivosVideos;
	}

	/**
	 * @param archivosVideos the archivosVideos to set
	 */
	public void setArchivosVideos(List<ArchivosProyecto> archivosVideos) {
		this.archivosVideos = archivosVideos;
	}


	/**
	 * @return the glosProyecto
	 */
	public String getGlosProyecto() {
		return glosProyecto;
	}


	/**
	 * @param glosProyecto the glosProyecto to set
	 */
	public void setGlosProyecto(String glosProyecto) {
		this.glosProyecto = glosProyecto;
	}


	/**
	 * @return the bibliografia
	 */
	public String getBibliografia() {
		return bibliografia;
	}


	/**
	 * @param bibliografia the bibliografia to set
	 */
	public void setBibliografia(String bibliografia) {
		this.bibliografia = bibliografia;
	}


	/**
	 * @return the resManImpAmb
	 */
	public String getResManImpAmb() {
		return resManImpAmb;
	}


	/**
	 * @param resManImpAmb the resManImpAmb to set
	 */
	public void setResManImpAmb(String resManImpAmb) {
		this.resManImpAmb = resManImpAmb;
	}
	
}
