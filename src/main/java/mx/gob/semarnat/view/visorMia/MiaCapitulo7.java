/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view.visorMia;

import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoTO;
import mx.gob.semarnat.utils.GenericConstants;

/**
 *
 * @author Paty
 */
@ManagedBean(name = "miaCap7")
@ViewScoped
public class MiaCapitulo7 {
    private final VisorDao dao = new VisorDao();
    private final DgiraMiaeDaoGeneral daoGeneral = new DgiraMiaeDaoGeneral();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private Proyecto folioDetalle;
    private Proyecto proyecto;
    private String urlCap7MIA;
    
    private int vistaVersion;
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    
    private ProyectoTO proyectoTO;
    
	// Cuadros de texto de Pronosticos ambientales y en su caso evaluacion de alternativas
	private String escSinProy;
	private String escConProy;
	private String escProyMed;
	private String pronAmb;
	private String escActFut;
	private String conclusionProy;
	private String evaAlternativas;

    @SuppressWarnings("static-access")
	public MiaCapitulo7() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        pramBita = params.get("bitaNum").toString();
        vistaVersion = (params.get("vistaVersion") != null) ? 
                Integer.parseInt((String)params.get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
        
        if (pramBita != null) 
        { 
            urlCap7MIA = "../capitulosMIA/capitulo5MIA.xhtml?bitaNum=" + pramBita;
            switch (vistaVersion) {
                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
                    proyecto = dao.obtieneProyectoUltimaVersion(pramBita);
                    break;
                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
                    proyecto = dao.obtieneProyectoVersionAnterior(pramBita);
                    break;
            }
            
            if (folioDetalle != null)  
            {
                folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                //JOptionPane.showMessageDialog(null, "folio:  " + folioNum + "   serial: " + serialNum, "Error", JOptionPane.INFORMATION_MESSAGE);
                //----proyecto en sesion
                proyecto = folioDetalle;//dao.cargaProyecto(folioNum, serialNum);
            }
            
            mapInfoAdicional = daoGeneral.generaMapaInfoAdicionalCapitulo(pramBita, "7");
        }
        
        try {
            this.proyectoTO = daoGeneral.proyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        
        escSinProy = daoGeneral.parseHtmlParagraph(proyectoTO.getEsceSinProy()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        escConProy = daoGeneral.parseHtmlParagraph(proyectoTO.getEsceConProy()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        escProyMed = daoGeneral.parseHtmlParagraph(proyectoTO.getEsceConProyMed()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        pronAmb = daoGeneral.parseHtmlParagraph(proyectoTO.getPronosAmb()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        escActFut = daoGeneral.parseHtmlParagraph(proyectoTO.getEscActFut()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        conclusionProy = daoGeneral.parseHtmlParagraph(proyectoTO.getConclusionProy()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        evaAlternativas = daoGeneral.parseHtmlParagraph(proyectoTO.getEvaAlternativas()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        System.out.println("Fin del MiaCap7");
        
    }

    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * @return the folioDetalle
     */
    public Proyecto getFolioDetalle() {
        return folioDetalle;
    }

    /**
     * @param folioDetalle the folioDetalle to set
     */
    public void setFolioDetalle(Proyecto folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the urlCap7MIA
     */
    public String getUrlCap7MIA() {
        return urlCap7MIA;
    }

    /**
     * @param urlCap7MIA the urlCap7MIA to set
     */
    public void setUrlCap7MIA(String urlCap7MIA) {
        this.urlCap7MIA = urlCap7MIA;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

	/**
	 * @return the proyectoTO
	 */
	public ProyectoTO getProyectoTO() {
		return proyectoTO;
	}

	/**
	 * @param proyectoTO the proyectoTO to set
	 */
	public void setProyectoTO(ProyectoTO proyectoTO) {
		this.proyectoTO = proyectoTO;
	}

	/**
	 * @return the escSinProy
	 */
	public String getEscSinProy() {
		return escSinProy;
	}

	/**
	 * @param escSinProy the escSinProy to set
	 */
	public void setEscSinProy(String escSinProy) {
		this.escSinProy = escSinProy;
	}

	/**
	 * @return the escConProy
	 */
	public String getEscConProy() {
		return escConProy;
	}

	/**
	 * @param escConProy the escConProy to set
	 */
	public void setEscConProy(String escConProy) {
		this.escConProy = escConProy;
	}

	/**
	 * @return the escProyMed
	 */
	public String getEscProyMed() {
		return escProyMed;
	}

	/**
	 * @param escProyMed the escProyMed to set
	 */
	public void setEscProyMed(String escProyMed) {
		this.escProyMed = escProyMed;
	}

	/**
	 * @return the pronAmb
	 */
	public String getPronAmb() {
		return pronAmb;
	}

	/**
	 * @param pronAmb the pronAmb to set
	 */
	public void setPronAmb(String pronAmb) {
		this.pronAmb = pronAmb;
	}

	/**
	 * @return the escActFut
	 */
	public String getEscActFut() {
		return escActFut;
	}

	/**
	 * @param escActFut the escActFut to set
	 */
	public void setEscActFut(String escActFut) {
		this.escActFut = escActFut;
	}

	/**
	 * @return the conclusionProy
	 */
	public String getConclusionProy() {
		return conclusionProy;
	}

	/**
	 * @param conclusionProy the conclusionProy to set
	 */
	public void setConclusionProy(String conclusionProy) {
		this.conclusionProy = conclusionProy;
	}

	/**
	 * @return the evaAlternativas
	 */
	public String getEvaAlternativas() {
		return evaAlternativas;
	}

	/**
	 * @param evaAlternativas the evaAlternativas to set
	 */
	public void setEvaAlternativas(String evaAlternativas) {
		this.evaAlternativas = evaAlternativas;
	}

}
