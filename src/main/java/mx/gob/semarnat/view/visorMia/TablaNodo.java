package mx.gob.semarnat.view.visorMia;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import mx.gob.semarnat.model.dgira_miae.ReiaObras;
import mx.gob.semarnat.model.dgira_miae.ReiaObrasCondiciones;
import mx.gob.semarnat.model.dgira_miae.ReiaProyObras;
import mx.gob.semarnat.model.dgira_miae.SueloVegetacionProyecto;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;


/**
 * @author efrainc
 *
 */
@ApplicationScoped
public class TablaNodo 
{
	public TablaNodo()
	{
		
	}
	
	/**
	 * 
	 * @param lista tipo suelo vegetacion que trae los datos para llenar la treetable
	 * @return lista para pintar en tabla de capitulo 2 y 4 
	 */
	
	public TreeNode CargaUsos(List<SueloVegetacionProyecto> lista)
	{
		TreeNode root = new DefaultTreeNode(new UsoSueloNodo("-", "-", "-", "-", 0.0, "-"), null);
		
		/**
		 * For para pintar los padres de la tabla  
		 */
		for (SueloVegetacionProyecto s:lista)
		{
			TreeNode uso = new DefaultTreeNode(new UsoSueloNodo(s.getSueloComponente(), s.getSueloDescripcion(), s.getSueloTipoEcov(), s.getSueloTipoGen(), s.getSueloFaseVs(), s.getSueloAreaSigeia(), s.getSueloDiagnostico(), "Padre"),root);
			
			/**
			 * for para pintar los hijos de a acuedo a cada  nodo que contenga datos
			 */
			for (SueloVegetacionDetalle sueloDetalle : s.getListaDetalle()) {
				
			TreeNode nodeDetalle = new DefaultTreeNode(new NodeSueloVegetacion(
							sueloDetalle.getIdSueloVegetacionDetalle(),
							"->",
							sueloDetalle.getVegGrupo().getVegNombre(),
							sueloDetalle.getVegTipo().getVegNombre(),
							sueloDetalle.getVegFase().getVegNombre(),
							"-",
							sueloDetalle.getDiagnosticoCorto(),
							sueloDetalle.getSuperficie(),
							"Hijo"), uso);
			}
			
			
		}
		
		
		
		
		return root;
	}
	
	/**
	 * 
	 * @param lista lista de tipo ReiaProyObras para estructurar la treetable
	 * @return Lista para pintar tabla en interface
	 */
	public TreeNode CargaReia(List<ReiaProyObras> lista) {
		TreeNode root = new DefaultTreeNode(new UsoReiaNodo("-", new ReiaObras(),(long) 0, "-", "-", "-"), null);

		// Iteracion para los nodos padres
		for (ReiaProyObras s : lista) {
			TreeNode uso = new DefaultTreeNode(new UsoReiaNodo(s.getFolio(), s.getObra(), s.getClave(),
					s.getPrincipal(), s.getExceptuada(), "Padre"), root);

			for (ReiaObrasCondiciones reiaCondiciones : s.getObra().getReiaObrasCondicionesCollection()) {

				TreeNode nodeDetalle = new DefaultTreeNode(
						new NodeReiaObrasCondiciones(reiaCondiciones.getIdCondicion().getCondicion(),
								reiaCondiciones.getIdCondicion().getRespuesta(), "Hijo"), uso);
			}
		}
		return root;
	}
	
}
