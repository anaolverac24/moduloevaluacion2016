/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorMia;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.model.dgira_miae.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;

/**
 * 
 * @author efrainc
 *
 */

@SuppressWarnings("serial")
@ManagedBean (name="estuRiesgo")
@ViewScoped
public class EstudioRiesgoBean  implements Serializable {

   private final DgiraMiaeDaoGeneral dao = new DgiraMiaeDaoGeneral();
   private EstudioRiesgoProyecto estudioR = new EstudioRiesgoProyecto();
   private Proyecto proyecto = new Proyecto();
   private String estudioDescBasesDise;
   private String estudioProyectoCivil;
   private String estudioProyectoMecanico;
   private String estudioProyectoIncendio;
   private String estudioDetalladaProceso;
   private String estudioAlmacenamiento;
   private String estudioProcesoAuxiliar;
   private String estudioPruebasVerificacion;
   private String estudioOperacion;
   private String estudioCuartoControl;
   private String estudioSistemaAislamiento;
   private String estudioAntecedentesAcci;
   private String estudioJerarRiesgo;
   private String estudioRadiosPotenciales;
   private String estudioEfectosAreaInflu;
   private String estudioRecomendacionesTecOp;
   private String estudioSistemasSeguridad;
   private String estudioMedidasPreventivas;
   private String estudioSenalaConclusion;
   private String estudioResumenSituacion;
   private String estudioInformeTecnico;
   private String folioNum;
   private short serialNum;
   
    @SuppressWarnings("static-access")
	public EstudioRiesgoBean() {
    	
 
    	if (proyecto != null)  
        {
            folioNum = proyecto.getProyectoPK().getFolioProyecto();
            serialNum = proyecto.getProyectoPK().getSerialProyecto();
            
            estudioR = dao.cargaEstudioRiesgo(proyecto);
            
    	 estudioDescBasesDise=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioProyectoCivil=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioProyectoMecanico=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioProyectoIncendio=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioDetalladaProceso=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioAlmacenamiento=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioProcesoAuxiliar=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioPruebasVerificacion=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioOperacion=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioCuartoControl=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioSistemaAislamiento=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioAntecedentesAcci=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioJerarRiesgo=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioRadiosPotenciales=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioEfectosAreaInflu=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioRecomendacionesTecOp=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioSistemasSeguridad=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioMedidasPreventivas=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioSenalaConclusion=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioResumenSituacion=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
    	 estudioInformeTecnico=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
        }
    }


    
    /**
     * Getter and setter  
     */
    
	/**
	 * @return the estudioR
	 */
	public EstudioRiesgoProyecto getEstudioR() {
		return estudioR;
	}


	/**
	 * @param estudioR the estudioR to set
	 */
	public void setEstudioR(EstudioRiesgoProyecto estudioR) {
		this.estudioR = estudioR;
	}



	/**
	 * @return the estudioDescBasesDise
	 */
	public String getEstudioDescBasesDise() {
		return estudioDescBasesDise;
	}



	/**
	 * @param estudioDescBasesDise the estudioDescBasesDise to set
	 */
	public void setEstudioDescBasesDise(String estudioDescBasesDise) {
		this.estudioDescBasesDise = estudioDescBasesDise;
	}



	/**
	 * @return the estudioProyectoCivil
	 */
	public String getEstudioProyectoCivil() {
		return estudioProyectoCivil;
	}



	/**
	 * @param estudioProyectoCivil the estudioProyectoCivil to set
	 */
	public void setEstudioProyectoCivil(String estudioProyectoCivil) {
		this.estudioProyectoCivil = estudioProyectoCivil;
	}



	/**
	 * @return the estudioProyectoMecanico
	 */
	public String getEstudioProyectoMecanico() {
		return estudioProyectoMecanico;
	}



	/**
	 * @param estudioProyectoMecanico the estudioProyectoMecanico to set
	 */
	public void setEstudioProyectoMecanico(String estudioProyectoMecanico) {
		this.estudioProyectoMecanico = estudioProyectoMecanico;
	}



	/**
	 * @return the estudioProyectoIncendio
	 */
	public String getEstudioProyectoIncendio() {
		return estudioProyectoIncendio;
	}



	/**
	 * @param estudioProyectoIncendio the estudioProyectoIncendio to set
	 */
	public void setEstudioProyectoIncendio(String estudioProyectoIncendio) {
		this.estudioProyectoIncendio = estudioProyectoIncendio;
	}



	/**
	 * @return the estudioDetalladaProceso
	 */
	public String getEstudioDetalladaProceso() {
		return estudioDetalladaProceso;
	}



	/**
	 * @param estudioDetalladaProceso the estudioDetalladaProceso to set
	 */
	public void setEstudioDetalladaProceso(String estudioDetalladaProceso) {
		this.estudioDetalladaProceso = estudioDetalladaProceso;
	}



	/**
	 * @return the estudioAlmacenamiento
	 */
	public String getEstudioAlmacenamiento() {
		return estudioAlmacenamiento;
	}



	/**
	 * @param estudioAlmacenamiento the estudioAlmacenamiento to set
	 */
	public void setEstudioAlmacenamiento(String estudioAlmacenamiento) {
		this.estudioAlmacenamiento = estudioAlmacenamiento;
	}



	/**
	 * @return the estudioProcesoAuxiliar
	 */
	public String getEstudioProcesoAuxiliar() {
		return estudioProcesoAuxiliar;
	}



	/**
	 * @param estudioProcesoAuxiliar the estudioProcesoAuxiliar to set
	 */
	public void setEstudioProcesoAuxiliar(String estudioProcesoAuxiliar) {
		this.estudioProcesoAuxiliar = estudioProcesoAuxiliar;
	}



	/**
	 * @return the estudioPruebasVerificacion
	 */
	public String getEstudioPruebasVerificacion() {
		return estudioPruebasVerificacion;
	}



	/**
	 * @param estudioPruebasVerificacion the estudioPruebasVerificacion to set
	 */
	public void setEstudioPruebasVerificacion(String estudioPruebasVerificacion) {
		this.estudioPruebasVerificacion = estudioPruebasVerificacion;
	}



	/**
	 * @return the estudioOperacion
	 */
	public String getEstudioOperacion() {
		return estudioOperacion;
	}



	/**
	 * @param estudioOperacion the estudioOperacion to set
	 */
	public void setEstudioOperacion(String estudioOperacion) {
		this.estudioOperacion = estudioOperacion;
	}



	/**
	 * @return the estudioCuartoControl
	 */
	public String getEstudioCuartoControl() {
		return estudioCuartoControl;
	}



	/**
	 * @param estudioCuartoControl the estudioCuartoControl to set
	 */
	public void setEstudioCuartoControl(String estudioCuartoControl) {
		this.estudioCuartoControl = estudioCuartoControl;
	}



	/**
	 * @return the estudioSistemaAislamiento
	 */
	public String getEstudioSistemaAislamiento() {
		return estudioSistemaAislamiento;
	}



	/**
	 * @param estudioSistemaAislamiento the estudioSistemaAislamiento to set
	 */
	public void setEstudioSistemaAislamiento(String estudioSistemaAislamiento) {
		this.estudioSistemaAislamiento = estudioSistemaAislamiento;
	}



	/**
	 * @return the estudioAntecedentesAcci
	 */
	public String getEstudioAntecedentesAcci() {
		return estudioAntecedentesAcci;
	}



	/**
	 * @param estudioAntecedentesAcci the estudioAntecedentesAcci to set
	 */
	public void setEstudioAntecedentesAcci(String estudioAntecedentesAcci) {
		this.estudioAntecedentesAcci = estudioAntecedentesAcci;
	}



	/**
	 * @return the estudioJerarRiesgo
	 */
	public String getEstudioJerarRiesgo() {
		return estudioJerarRiesgo;
	}



	/**
	 * @param estudioJerarRiesgo the estudioJerarRiesgo to set
	 */
	public void setEstudioJerarRiesgo(String estudioJerarRiesgo) {
		this.estudioJerarRiesgo = estudioJerarRiesgo;
	}



	/**
	 * @return the estudioRadiosPotenciales
	 */
	public String getEstudioRadiosPotenciales() {
		return estudioRadiosPotenciales;
	}



	/**
	 * @param estudioRadiosPotenciales the estudioRadiosPotenciales to set
	 */
	public void setEstudioRadiosPotenciales(String estudioRadiosPotenciales) {
		this.estudioRadiosPotenciales = estudioRadiosPotenciales;
	}



	/**
	 * @return the estudioEfectosAreaInflu
	 */
	public String getEstudioEfectosAreaInflu() {
		return estudioEfectosAreaInflu;
	}



	/**
	 * @param estudioEfectosAreaInflu the estudioEfectosAreaInflu to set
	 */
	public void setEstudioEfectosAreaInflu(String estudioEfectosAreaInflu) {
		this.estudioEfectosAreaInflu = estudioEfectosAreaInflu;
	}



	/**
	 * @return the estudioRecomendacionesTecOp
	 */
	public String getEstudioRecomendacionesTecOp() {
		return estudioRecomendacionesTecOp;
	}



	/**
	 * @param estudioRecomendacionesTecOp the estudioRecomendacionesTecOp to set
	 */
	public void setEstudioRecomendacionesTecOp(String estudioRecomendacionesTecOp) {
		this.estudioRecomendacionesTecOp = estudioRecomendacionesTecOp;
	}



	/**
	 * @return the estudioSistemasSeguridad
	 */
	public String getEstudioSistemasSeguridad() {
		return estudioSistemasSeguridad;
	}



	/**
	 * @param estudioSistemasSeguridad the estudioSistemasSeguridad to set
	 */
	public void setEstudioSistemasSeguridad(String estudioSistemasSeguridad) {
		this.estudioSistemasSeguridad = estudioSistemasSeguridad;
	}



	/**
	 * @return the estudioMedidasPreventivas
	 */
	public String getEstudioMedidasPreventivas() {
		return estudioMedidasPreventivas;
	}



	/**
	 * @param estudioMedidasPreventivas the estudioMedidasPreventivas to set
	 */
	public void setEstudioMedidasPreventivas(String estudioMedidasPreventivas) {
		this.estudioMedidasPreventivas = estudioMedidasPreventivas;
	}



	/**
	 * @return the estudioSenalaConclusion
	 */
	public String getEstudioSenalaConclusion() {
		return estudioSenalaConclusion;
	}



	/**
	 * @param estudioSenalaConclusion the estudioSenalaConclusion to set
	 */
	public void setEstudioSenalaConclusion(String estudioSenalaConclusion) {
		this.estudioSenalaConclusion = estudioSenalaConclusion;
	}



	/**
	 * @return the estudioResumenSituacion
	 */
	public String getEstudioResumenSituacion() {
		return estudioResumenSituacion;
	}



	/**
	 * @param estudioResumenSituacion the estudioResumenSituacion to set
	 */
	public void setEstudioResumenSituacion(String estudioResumenSituacion) {
		this.estudioResumenSituacion = estudioResumenSituacion;
	}



	/**
	 * @return the estudioInformeTecnico
	 */
	public String getEstudioInformeTecnico() {
		return estudioInformeTecnico;
	}



	/**
	 * @param estudioInformeTecnico the estudioInformeTecnico to set
	 */
	public void setEstudioInformeTecnico(String estudioInformeTecnico) {
		this.estudioInformeTecnico = estudioInformeTecnico;
	}



	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}



	/**
	 * @param proyecto the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}



	/**
	 * @return the folioNum
	 */
	public String getFolioNum() {
		return folioNum;
	}



	/**
	 * @param folioNum the folioNum to set
	 */
	public void setFolioNum(String folioNum) {
		this.folioNum = folioNum;
	}



	/**
	 * @return the serialNum
	 */
	public short getSerialNum() {
		return serialNum;
	}



	/**
	 * @param serialNum the serialNum to set
	 */
	public void setSerialNum(short serialNum) {
		this.serialNum = serialNum;
	}
	
	
	
	

}
