/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorMia;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.model.TreeNode;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.catalogos.RamaProyecto;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.catalogos.TipoProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ReiaCategorias;
import mx.gob.semarnat.model.dgira_miae.ReiaCondiciones;
import mx.gob.semarnat.model.dgira_miae.ReiaObras;
import mx.gob.semarnat.model.dgira_miae.ReiaObrasCondiciones;
import mx.gob.semarnat.model.dgira_miae.ReiaProyCondiciones;
import mx.gob.semarnat.model.dgira_miae.ReiaProyObras;
import mx.gob.semarnat.model.dgira_miae.RepLegalProyecto;
import mx.gob.semarnat.model.dgira_miae.RespTecProyecto;
import mx.gob.semarnat.model.dgira_miae.RespTecProyectoPK;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.view.Util.RepLegalu;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "miaCap1")
@ViewScoped
public class MiaCapitulo1 {

    private final DgiraMiaeDaoGeneral dao = new DgiraMiaeDaoGeneral();
    private final VisorDao daoVisor = new VisorDao();

    private String pramBita;
    private Integer idusuario;
    private String vialidad = "";
    private String asentamiento = "";

    private Proyecto proyecto = new Proyecto();
    private Vexdatosusuario promovente = new Vexdatosusuario();
    private RespTecProyecto respTecProyecto = new RespTecProyecto();
    private List<RepLegalProyecto> legalProyecto = new ArrayList();
    private List<Vexdatosusuariorep> repLegal = new ArrayList();
    private List<Vexdatosusuariorep> legalRespTec = new ArrayList();
    private List<RepLegalu> repLegalu = new ArrayList<RepLegalu>();
    private List<Vexdatosusuariorep> representanes = new ArrayList<Vexdatosusuariorep>();
    private String pregREEIA;

    private SectorProyecto sector;
    private SubsectorProyecto subSector;
    private RamaProyecto rama;
    private TipoProyecto tipo;
    
    private int vistaVersion;
    
    // VALIABLES PARA LA TREETABLE
    private TreeNode rootReia;
    private TablaNodo tablaNodo = new TablaNodo();
    // REIA
    private List<ReiaObras> reiaObras = new ArrayList<ReiaObras>();
    private List<ReiaCategorias> reiaCategorias = new ArrayList<ReiaCategorias>();
    private List<ReiaCondiciones> reiaCondiciones = new ArrayList<ReiaCondiciones>();
    private List<ReiaObrasCondiciones> reiaObrasCondiciones = new ArrayList<ReiaObrasCondiciones>();
    private List<ReiaProyCondiciones> reiaProyCondiciones = new ArrayList<ReiaProyCondiciones>();
    private List<ReiaProyObras> reiaProyObras = new ArrayList<ReiaProyObras>();

    public MiaCapitulo1() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        idusuario = (Integer) ec.getSessionMap().get("idUsu");
        pramBita = params.get("bitaNum").toString();
        
        vistaVersion = (params.get("vistaVersion") != null) ? 
                Integer.parseInt((String)params.get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
        
        //recupera el proyecto
        if (pramBita != null) {
            if (!dao.lista_namedQuery("ProyectoD.findByBitacoraProyecto", new Object[]{pramBita}, new String[]{"bitacoraProyecto"}).isEmpty()) {
                
                switch (vistaVersion) {
                    case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
                        proyecto = daoVisor.obtieneProyectoUltimaVersion(pramBita);
                        break;
                    case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
                        proyecto = daoVisor.obtieneProyectoVersionAnterior(pramBita);
                        break;
                    default:
                        proyecto = daoVisor.obtieneProyectoUltimaVersion(pramBita);
                        break;
                }
                
                //<editor-fold desc="llenado de catalogos" defaultstate="collapsed">
                sector = (SectorProyecto) dao.busca(SectorProyecto.class, proyecto.getNsec());
                subSector = (SubsectorProyecto) dao.busca(SubsectorProyecto.class, proyecto.getNsub());
                rama = (RamaProyecto) dao.busca(RamaProyecto.class, proyecto.getNrama());
                tipo = (TipoProyecto) dao.busca(TipoProyecto.class, proyecto.getNtipo());
                //</editor-fold>
                //<editor-fold desc="datos del promovente" defaultstate="collapsed">
                if (!((List<Vexdatosusuario>) dao.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()))).isEmpty()) {
                    promovente = (Vexdatosusuario) dao.datosPromovente(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())).get(0);
                    vialidad = ((CatVialidad) dao.busca(CatVialidad.class, new Short(promovente.getBgcveTipoVialLk().toString()))).getDescripcion();
                    asentamiento = ((CatTipoAsen) dao.busca(CatTipoAsen.class, new Short(promovente.getBgcveTipoAsenLk().toString()))).getNombre();
                }//</editor-fold>
                //<editor-fold desc="representante legal" defaultstate="collapsed">
                legalProyecto = (List<RepLegalProyecto>) dao.
                        lista_namedQuery("RepLegalProyecto.findByFolioProyecto",
                                new Object[]{proyecto.getProyectoPK().getFolioProyecto()},
                                new String[]{"folioProyecto"});
                repLegal = new ArrayList();
//                for (RepLegalProyecto rlp : legalProyecto) {
//                    repLegal.add(dao.repLegPorCurp(rlp.getRepLegalProyectoPK().getRfc(),
//                            Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())));
//                }//</editor-fold>
                
                representanes = dao.datosPrepLeg(Integer.valueOf(proyecto.getProyectoPK().getFolioProyecto()));
                for (Vexdatosusuariorep r : representanes) {
                    RepLegalu r2 = new RepLegalu();
                    r2.setVexdatosusuariorep(r);
                    RepLegalProyecto r3 = null;
                    try {
                        r3 = dao.getRepLegalCurp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), r.getVccurp());
                    } catch (Exception e3) {

                    }
                    if (r3 == null) {
                        r3 = new RepLegalProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),r.getVccurp() );
    //                    r3.setRfc(sector);
    //                    r3.setRepresentanteCurp(r.getVccurp());
                    }
                    r2.setRepLegal(r3);
                    repLegalu.add(r2);
                }
                
                
                //<editor-fold desc="responsable tecnico" defaultstate="collapsed">
                legalRespTec = new ArrayList();
                for (RepLegalProyecto rlp : legalProyecto) {
                    try {
                        if (rlp.getMismoEstudio().contains("S")) {
                            legalRespTec.add(dao.repLegPorCurp(rlp.getRfc(),
                                    Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto())));
                        }
                    } catch (Exception e) {
                    }
                }
                respTecProyecto = (RespTecProyecto) dao.busca(RespTecProyecto.class, new RespTecProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));//</editor-fold>
                
                
                
                
                if(proyecto.getProyectoPK() != null)
                {
                    if(proyecto.getProyRespTecIgualLegal().equals('N'))
                    {
                        pregREEIA = "No";
                    }
                    if(proyecto.getProyRespTecIgualLegal().equals('Y'))
                    {
                        pregREEIA = "Si";
                    }
                }
                
                //JOptionPane.showMessageDialog(null,  "error RRRR:  " + proyecto.getProyRespTecIgualLegal(), "Error", JOptionPane.INFORMATION_MESSAGE);                                
            }
        }
        
        reiaProyObras = dao.getReiaProyObras(proyecto.getProyectoPK().getFolioProyecto());
        
        //rootReia = tablaNodo.CargaReia(reiaProyObras);
        
        System.out.println("Fin del constructor de MiaCap1");
        
    }

//<editor-fold defaultstate="collapsed" desc="Getters & setters"> 
    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public String getAsentamiento() {
        return asentamiento;
    }

    public String getVialidad() {
        return vialidad;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public SectorProyecto getSector() {
        return sector;
    }

    public SubsectorProyecto getSubSector() {
        return subSector;
    }

    public RamaProyecto getRama() {
        return rama;
    }

    public TipoProyecto getTipo() {
        return tipo;
    }

    public Vexdatosusuario getPromovente() {
        return promovente;
    }

    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    public String getPramBita() {
        return pramBita;
    }

    public RespTecProyecto getRespTecProyecto() {
        return respTecProyecto;
    }

    public List<RepLegalProyecto> getLegalProyecto() {
        return legalProyecto;
    }

    public List<RepLegalu> getLegalRespTec() {
        return repLegalu;
    }

    public List<Vexdatosusuariorep> getRepLegal() {
        return repLegal;
    }

    public void setLegalProyecto(List<RepLegalProyecto> legalProyecto) {
        this.legalProyecto = legalProyecto;
    }

    public void setLegalRespTec(List<Vexdatosusuariorep> legalRespTec) {
        this.legalRespTec = legalRespTec;
    }

    public void setPromovente(Vexdatosusuario promovente) {
        this.promovente = promovente;
    }

    public void setRepLegal(List<Vexdatosusuariorep> repLegal) {
        this.repLegal = repLegal;
    }

    public void setRespTecProyecto(RespTecProyecto respTecProyecto) {
        this.respTecProyecto = respTecProyecto;
    }

    public boolean isRespTec() 
    {
        boolean resp = true;
//        if (repLegalu != null) {
//            return repLegalu.isEmpty();
//        } else {
//            return false;
//        }
        //
        if(proyecto.getProyectoPK() != null)
        {
            if(proyecto.getProyRespTecIgualLegal().equals("N"))
            {
                resp = false;
                pregREEIA = "No";
            }
            else
            {
                resp = true;
                pregREEIA = "Si";
            }
        }
        return resp;
    }
//</editor-fold>
    
    public Boolean getTipoPerMor() {
        Boolean resp = false;
        if(promovente.getVctipopersona()!= null)
        {
            resp = promovente.getVctipopersona().equals("MOR");
        }
        return resp;
    }

    public Boolean getTipoPerFis() {
        Boolean resp = false;
        
        if(promovente.getVctipopersona()!= null)
        {
            resp = promovente.getVctipopersona().equals("FIS");
        }
        return resp;

    }

    /**
     * @return the repLegalu
     */
    public List<RepLegalu> getRepLegalu() {
        return repLegalu;
    }

    /**
     * @param repLegalu the repLegalu to set
     */
    public void setRepLegalu(List<RepLegalu> repLegalu) {
        this.repLegalu = repLegalu;
    }

    /**
     * @return the pregREEIA
     */
    public String getPregREEIA() {
        return pregREEIA;
    }

    /**
     * @param pregREEIA the pregREEIA to set
     */
    public void setPregREEIA(String pregREEIA) {
        this.pregREEIA = pregREEIA;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

	/**
	 * @return the reiaObras
	 */
	public List<ReiaObras> getReiaObras() {
		return reiaObras;
	}

	/**
	 * @param reiaObras the reiaObras to set
	 */
	public void setReiaObras(List<ReiaObras> reiaObras) {
		this.reiaObras = reiaObras;
	}

	/**
	 * @return the reiaCategorias
	 */
	public List<ReiaCategorias> getReiaCategorias() {
		return reiaCategorias;
	}

	/**
	 * @param reiaCategorias the reiaCategorias to set
	 */
	public void setReiaCategorias(List<ReiaCategorias> reiaCategorias) {
		this.reiaCategorias = reiaCategorias;
	}

	/**
	 * @return the reiaCondiciones
	 */
	public List<ReiaCondiciones> getReiaCondiciones() {
		return reiaCondiciones;
	}

	/**
	 * @param reiaCondiciones the reiaCondiciones to set
	 */
	public void setReiaCondiciones(List<ReiaCondiciones> reiaCondiciones) {
		this.reiaCondiciones = reiaCondiciones;
	}

	/**
	 * @return the reiaObrasCondiciones
	 */
	public List<ReiaObrasCondiciones> getReiaObrasCondiciones() {
		return reiaObrasCondiciones;
	}

	/**
	 * @param reiaObrasCondiciones the reiaObrasCondiciones to set
	 */
	public void setReiaObrasCondiciones(List<ReiaObrasCondiciones> reiaObrasCondiciones) {
		this.reiaObrasCondiciones = reiaObrasCondiciones;
	}

	/**
	 * @return the reiaProyCondiciones
	 */
	public List<ReiaProyCondiciones> getReiaProyCondiciones() {
		return reiaProyCondiciones;
	}

	/**
	 * @param reiaProyCondiciones the reiaProyCondiciones to set
	 */
	public void setReiaProyCondiciones(List<ReiaProyCondiciones> reiaProyCondiciones) {
		this.reiaProyCondiciones = reiaProyCondiciones;
	}

	/**
	 * @return the reiaProyObras
	 */
	public List<ReiaProyObras> getReiaProyObras() {
		return reiaProyObras;
	}

	/**
	 * @param reiaProyObras the reiaProyObras to set
	 */
	public void setReiaProyObras(List<ReiaProyObras> reiaProyObras) {
		this.reiaProyObras = reiaProyObras;
	}

	/**
	 * @return the rootReia
	 */
	public TreeNode getRootReia() {
		return rootReia;
	}

	/**
	 * @param rootReia the rootReia to set
	 */
	public void setRootReia(TreeNode rootReia) {
		this.rootReia = rootReia;
	}

	/**
	 * @return the tablaNodo
	 */
	public TablaNodo getTablaNodo() {
		return tablaNodo;
	}

	/**
	 * @param tablaNodo the tablaNodo to set
	 */
	public void setTablaNodo(TablaNodo tablaNodo) {
		this.tablaNodo = tablaNodo;
	}
    
}