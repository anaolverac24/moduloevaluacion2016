/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorMia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.CatEstEsp;
import mx.gob.semarnat.model.dgira_miae.ClimaProyecto;
import mx.gob.semarnat.model.dgira_miae.CuerposAguaProyecto;
import mx.gob.semarnat.model.dgira_miae.FaunaProyecto;
import mx.gob.semarnat.model.dgira_miae.FloraProyecto;
import mx.gob.semarnat.model.dgira_miae.LocalidIndigenas;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoTO;
import mx.gob.semarnat.model.dgira_miae.SueloVegetacionProyecto;
import mx.gob.semarnat.utils.GenericConstants;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "miaCap4Serial3")
@ViewScoped
public class MiaCapitulo4Serial3 {

    private final DgiraMiaeDaoGeneral dao = new DgiraMiaeDaoGeneral();
    private final VisorDao daoVisor = new VisorDao();

    private Proyecto proyecto = new Proyecto();
    private final String bitacora;

    private List<ClimaProyecto> climas = new ArrayList();
    private List<LocalidIndigenas> locInd = new ArrayList();
    
    //Declaracion de lista para cuerpos de agua
    private List<CuerposAguaProyecto> cuerposAgua = new ArrayList<CuerposAguaProyecto>();
    
    private List<Object[]> microCuenca2 = new ArrayList<Object[]>();
    private List<Object[]> acuiferos2 = new ArrayList<Object[]>();
    private List <SueloVegetacionProyecto> sueloVegetacion= new ArrayList<SueloVegetacionProyecto>();
    private List<FloraProyecto> flora = new ArrayList<FloraProyecto>();
    private List<FaunaProyecto> fauna = new ArrayList<FaunaProyecto>();
    private List<Object> estudiosEsp = new ArrayList<Object>();
    private List<CatEstEsp> catEstEsp = new ArrayList<CatEstEsp>();
    
    private List<AnexosProyecto> anexosSA = new ArrayList();
    private List<AnexosProyecto> anexosAF = new ArrayList(); 
    private List<AnexosProyecto> anexosSP = new ArrayList();
    private List<AnexosProyecto> anexosClimasFM = new ArrayList(); 
    private List<AnexosProyecto> anexosSuelos = new ArrayList();
    private List<AnexosProyecto> anexosPaisaje= new ArrayList();
    private List<AnexosProyecto> anexosDiagAmb = new ArrayList();
    private List<AnexosProyecto> anexosestEsp = new ArrayList();
    
    private int vistaVersion;
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    
	// Archivos de Delimitacion del Sistema Ambiental
	private List<ArchivosProyecto> archivosDelSisAmb = new ArrayList<ArchivosProyecto>();
	// Archivos de Delimitacion del area de Influencia
	private List<ArchivosProyecto> archivosDelAreaInf = new ArrayList<ArchivosProyecto>();
	// Archivos de Delimitacion del Sitio del Proyecto
	private List<ArchivosProyecto> archivosDelSitProy = new ArrayList<ArchivosProyecto>();
	// Archivos de Clima y Fenomenos Meteorologicos
	private List<ArchivosProyecto> archivosClimaFenMet = new ArrayList<ArchivosProyecto>();
	// Archivos de Suelos
	private List<ArchivosProyecto> archivosSuelos = new ArrayList<ArchivosProyecto>();
	// Archivos de Paisajes
	private List<ArchivosProyecto> archivosPaisajes = new ArrayList<ArchivosProyecto>();
	// Archivos de Diagnostico Ambiental
	private List<ArchivosProyecto> archivosDiagAmb = new ArrayList<ArchivosProyecto>();
	
	private ProyectoTO proyectoTO = new ProyectoTO();
	
	// Cuadros de texto de Climas
	private String observacionesClima;	
	private String tempFenMetDescrip;

	// Cuadros de texto de Delimitaciones
	private String delimitacionSisAmb;
	private String delimitacionAreaInf;
	private String delimitacionSitioProy;
	
	// Cuadros de texto de Suelos
	private String geologiaGeom;
	private String suelos;
	
	// Cuadros de texto de Hidrologia
	private String hidroSub;
	private String hidroSup;
	
	// Cuadros de texto de Analisis Biologico
	private String analisisBio;
	
	// Cuadros de texto de Paisajes
	private String paisajes;
	
	// Cuadros de texto de Dinamica poblacion
	private String dinamicaPob;
	
	// Cuadros de texto de Diagnostico ambiental
	private String diagnosticoAmb;
	
    @SuppressWarnings("static-access")
	public MiaCapitulo4Serial3() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        bitacora = ec.getRequestParameterMap().get("bitaNum");
        vistaVersion = GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;        
        
        if (bitacora != null) {
            if (!dao.lista_namedQuery("ProyectoD.findByBitacoraProyecto", new Object[]{bitacora}, new String[]{"bitacoraProyecto"}).isEmpty()) {
//            switch (vistaVersion) {
//                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
//                    proyecto = daoVisor.obtieneProyectoVersionAnterior(bitacora);
//                    break;
//                    
//                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
//                    proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);
//                    break;
//                    
//                case GenericConstants.VISTA_EVALUADOR_1ER_INGRESO:
//                case GenericConstants.VISTA_EVALUADOR_INFO_ADICIONAL:
//                    //Se obtiene la información del proyecto de la versión anterior del promovente (1, 3, ...)
//                    if (vistaVersion == GenericConstants.VISTA_EVALUADOR_1ER_INGRESO) {//Si versión 2
//                        proyecto = daoVisor.obtieneProyectoVersionAnterior(bitacora); //versión 1
//                    } else {//Si versión 4
//                        proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);//versión 3
//                    }
//
//                    proyecto.getProyectoPK().setSerialProyecto((short)vistaVersion);
//
//                    break;
//            }                
            	
            	proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacora);
            	
                if (proyecto != null) {
                    //<editor-fold desc="Climas" defaultstate="collapsed">
//                    climas = (List<ClimaProyecto>) dao.lista_namedQuery("ClimaProyecto.findByFolioSerial",
//                            new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
//                            new String[]{"folio", "serial"});
                    System.out.println("climas <::::::::::::::::");
                    climas = dao.getClimaProyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    if (climas.isEmpty()) {
                        List<Object[]> cli = dao.getClimas2(proyecto.getProyectoPK().getFolioProyecto(), (short)3, proyecto.getClaveProyecto());
                        for (Object[] c : cli) {
                            ClimaProyecto cp = new ClimaProyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                            cp.setProyecto(proyecto);
                            cp.setClaveClimatologica(c[0].toString());
                            cp.setDesTem(c[1].toString());
                            cp.setDescPrec(c[2].toString());
                            cp.setGrupoMapa2(c[3].toString());
                            climas.add(cp);
                        }
                    }
                    
                    //</editor-fold>
                    
                    catEstEsp = dao.getCatEstEsp();
                    
                    microCuenca2 = dao.getMicrocuenca2(proyecto.getProyectoPK().getFolioProyecto(), (short)3, proyecto.getClaveProyecto());
                    acuiferos2 = dao.getAcuiferos2(proyecto.getProyectoPK().getFolioProyecto(), (short)3, proyecto.getClaveProyecto());
                    sueloVegetacion = dao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    flora = dao.getFloraProyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    fauna = dao.getFaunaProyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    
                    try {

                    	estudiosEsp = dao.EstudiosEsp(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                    	
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("ERROR DE ESTUDIOS ESPECIALES: "+ e.getMessage());
					}
                    
                    
                    
                    
                    //4,2,1,6
                    short capituloId10 = (short) 0;
                    short subcapituloId10 = (short) 0;
                    short seccionId10 = (short) 0;
                    short apartadoId10 = (short) 0; 
                    capituloId10 = 4;
                    subcapituloId10 = 2;
                    seccionId10 = 1;
                    apartadoId10 = 6;
                    anexosestEsp = dao.getAnexos(capituloId10, subcapituloId10, seccionId10, apartadoId10, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 
                    
                    
                    //4,1,1,0,
                    short capituloId3 = (short) 0;
                    short subcapituloId3 = (short) 0;
                    short seccionId3 = (short) 0;
                    short apartadoId3 = (short) 0; 
                    capituloId3 = 4;
                    subcapituloId3 = 1;
                    seccionId3 = 1;
                    apartadoId3 = 0;
                    anexosSA = dao.getAnexos(capituloId3, subcapituloId3, seccionId3, apartadoId3, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 
                   
                    
                    //4,1,2,0
                    short capituloId4 = (short) 0;
                    short subcapituloId4 = (short) 0;
                    short seccionId4 = (short) 0;
                    short apartadoId4 = (short) 0; 
                    capituloId4 = 4;
                    subcapituloId4 = 1;
                    seccionId4 = 2;
                    apartadoId4 = 0;
                    anexosAF = dao.getAnexos(capituloId4, subcapituloId4, seccionId4, apartadoId4, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 

                    //4,1,3,0,
                    short capituloId5 = (short) 0;
                    short subcapituloId5 = (short) 0;
                    short seccionId5 = (short) 0;
                    short apartadoId5 = (short) 0; 
                    capituloId5 = 4;
                    subcapituloId5 = 1;
                    seccionId5 = 3;
                    apartadoId5 = 0;
                    anexosSP = dao.getAnexos(capituloId5, subcapituloId5, seccionId5, apartadoId5, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 
                   
                    //4,2,1,1,
                    short capituloId6 = (short) 0;
                    short subcapituloId6 = (short) 0;
                    short seccionId6 = (short) 0;
                    short apartadoId6 = (short) 0; 
                    capituloId6 = 4;
                    subcapituloId6 = 2;
                    seccionId6 = 1;
                    apartadoId6 = 1;
                    anexosClimasFM = dao.getAnexos(capituloId6, subcapituloId6, seccionId6, apartadoId6, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 
                   
                    
                    //4,2,1,3
                    short capituloId7 = (short) 0;
                    short subcapituloId7 = (short) 0;
                    short seccionId7 = (short) 0;
                    short apartadoId7 = (short) 0; 
                    capituloId7 = 4;
                    subcapituloId7 = 2;
                    seccionId7 = 1;
                    apartadoId7 = 3;
                    anexosSuelos = dao.getAnexos(capituloId7, subcapituloId7, seccionId7, apartadoId7, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 

                    //4,3,0,0,
                    short capituloId8 = (short) 0;
                    short subcapituloId8 = (short) 0;
                    short seccionId8 = (short) 0;
                    short apartadoId8 = (short) 0; 
                    capituloId8 = 4;
                    subcapituloId8 = 3;
                    seccionId8 = 0;
                    apartadoId8 = 0;
                    anexosPaisaje  = dao.getAnexos(capituloId8, subcapituloId8, seccionId8, apartadoId8, proyecto.getProyectoPK().getFolioProyecto(), (short)3); 
                    
                    //4,5,0,0,
                    short capituloId9 = (short) 0;
                    short subcapituloId9 = (short) 0;
                    short seccionId9 = (short) 0;
                    short apartadoId9 = (short) 0; 
                    capituloId9 = 4;
                    subcapituloId9 = 5;
                    seccionId9 = 0;
                    apartadoId9 = 0;
                    anexosDiagAmb = dao.getAnexos(capituloId9, subcapituloId9, seccionId9, apartadoId9, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
                }
            }
            
            mapInfoAdicional = dao.generaMapaInfoAdicionalCapitulo(bitacora, "4");
        }
        
        archivosDelSisAmb = dao.archivosProyecto((short)4, (short)1, (short)0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        archivosDelAreaInf = dao.archivosProyecto((short)4, (short)1, (short)2, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        archivosDelSitProy = dao.archivosProyecto((short)4, (short)1, (short)3, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        archivosClimaFenMet = dao.archivosProyecto((short)4, (short)2, (short)1, (short) 1, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        archivosSuelos = dao.archivosProyecto((short)4, (short)2, (short)1, (short) 4, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        archivosPaisajes = dao.archivosProyecto((short)4, (short)3, (short)0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        archivosDiagAmb = dao.archivosProyecto((short)4, (short)5, (short)0, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        try {
            this.proyectoTO = dao.proyecto(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        
        /**
         * Lista para tabla de cuerpos de agua de capitulo 4
         */
        cuerposAgua = dao.getCuerposAgua(proyecto.getProyectoPK().getFolioProyecto(), (short)3);
        
        /**
         * Datos de los cuadros de texto exriquecido sin las etiquetas y sin el cuadro de texto en el cap 4
         */
        observacionesClima = dao.parseHtmlParagraph(proyectoTO.getClimaFenMetObserv()).toString().replace("[", "").replace("],", "").replace("]", "");
        
        tempFenMetDescrip = dao.parseHtmlParagraph(proyectoTO.getTempFenMetDescrip()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        delimitacionSisAmb = dao.parseHtmlParagraph(proyectoTO.getDescDelimSistemAmb()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        delimitacionAreaInf = dao.parseHtmlParagraph(proyectoTO.getDescDelimAreaInflu()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        delimitacionSitioProy = dao.parseHtmlParagraph(proyectoTO.getDescDelimSitProy()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        geologiaGeom = dao.parseHtmlParagraph(proyectoTO.getDescGeos()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        suelos = dao.parseHtmlParagraph(proyectoTO.getDescSuelo()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        hidroSub = dao.parseHtmlParagraph(proyectoTO.getHidroSubtObserva()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        hidroSup = dao.parseHtmlParagraph(proyectoTO.getHidroSuPObserva()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        analisisBio = proyectoTO.getAnalisisbioVeg();
        
        paisajes = proyectoTO.getDescPaisajeProy();
        
        dinamicaPob = dao.parseHtmlParagraph(proyectoTO.getMedioSocioproy()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
        diagnosticoAmb = dao.parseHtmlParagraph(proyectoTO.getDescDiagamb()).toString().replace("[", "").replace("], ", "").replace("]", "");
        
    }
    
    private String notNullFilter(String cadena) {
        if (cadena == null || cadena.compareToIgnoreCase("null") == 0) {
            return "";
        } else {
            return cadena;
        }
    }
    
  //=================================================================================================================================================================
  //=================================================================================================================================================================
    //<editor-fold desc="Getters & Setters" defaultstate="collapsed">
    public Proyecto getProyecto() {
        return proyecto;
    }

    public List<ClimaProyecto> getClimas() {
        return climas;
    }

    public List<LocalidIndigenas> getLocInd() {
        return locInd;
    }

    //</editor-fold>

   

    /**
     * @return the acuiferos2
     */
    public List<Object[]> getAcuiferos2() {
        return acuiferos2;
    }

    /**
     * @param acuiferos2 the acuiferos2 to set
     */
    public void setAcuiferos2(List<Object[]> acuiferos2) {
        this.acuiferos2 = acuiferos2;
    }

    /**
     * @return the sueloVegetacion
     */
    public List<SueloVegetacionProyecto> getSueloVegetacion() {
        return sueloVegetacion;
    }

    /**
     * @param sueloVegetacion the sueloVegetacion to set
     */
    public void setSueloVegetacion(List <SueloVegetacionProyecto> sueloVegetacion) {
        this.sueloVegetacion = sueloVegetacion;
    }

    /**
	 * @return the flora
	 */
	public List<FloraProyecto> getFlora() {
		return flora;
	}

	/**
	 * @param flora the flora to set
	 */
	public void setFlora(List<FloraProyecto> flora) {
		this.flora = flora;
	}

	/**
	 * @return the fauna
	 */
	public List<FaunaProyecto> getFauna() {
		return fauna;
	}

	/**
	 * @param fauna the fauna to set
	 */
	public void setFauna(List<FaunaProyecto> fauna) {
		this.fauna = fauna;
	}

	/**
     * @return the anexosSA
     */
    public List<AnexosProyecto> getAnexosSA() {
        return anexosSA;
    }

    /**
     * @param anexosSA the anexosSA to set
     */
    public void setAnexosSA(List<AnexosProyecto> anexosSA) {
        this.anexosSA = anexosSA;
    }

    /**
     * @return the anexosAF
     */
    public List<AnexosProyecto> getAnexosAF() {
        return anexosAF;
    }

    /**
     * @param anexosAF the anexosAF to set
     */
    public void setAnexosAF(List<AnexosProyecto> anexosAF) {
        this.anexosAF = anexosAF;
    }

    /**
     * @return the anexosSP
     */
    public List<AnexosProyecto> getAnexosSP() {
        return anexosSP;
    }

    /**
     * @param anexosSP the anexosSP to set
     */
    public void setAnexosSP(List<AnexosProyecto> anexosSP) {
        this.anexosSP = anexosSP;
    }

    /**
     * @return the anexosClimasFM
     */
    public List<AnexosProyecto> getAnexosClimasFM() {
        return anexosClimasFM;
    }

    /**
     * @param anexosClimasFM the anexosClimasFM to set
     */
    public void setAnexosClimasFM(List<AnexosProyecto> anexosClimasFM) {
        this.anexosClimasFM = anexosClimasFM;
    }

    /**
     * @return the anexosSuelos
     */
    public List<AnexosProyecto> getAnexosSuelos() {
        return anexosSuelos;
    }

    /**
     * @param anexosSuelos the anexosSuelos to set
     */
    public void setAnexosSuelos(List<AnexosProyecto> anexosSuelos) {
        this.anexosSuelos = anexosSuelos;
    }

    /**
     * @return the anexosPaisaje
     */
    public List<AnexosProyecto> getAnexosPaisaje() {
        return anexosPaisaje;
    }

    /**
     * @param anexosPaisaje the anexosPaisaje to set
     */
    public void setAnexosPaisaje(List<AnexosProyecto> anexosPaisaje) {
        this.anexosPaisaje = anexosPaisaje;
    }

    /**
     * @return the anexosDiagAmb
     */
    public List<AnexosProyecto> getAnexosDiagAmb() {
        return anexosDiagAmb;
    }

    /**
     * @param anexosDiagAmb the anexosDiagAmb to set
     */
    public void setAnexosDiagAmb(List<AnexosProyecto> anexosDiagAmb) {
        this.anexosDiagAmb = anexosDiagAmb;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

    /**
     * @return the anexosestEsp
     */
    public List<AnexosProyecto> getAnexosestEsp() {
        return anexosestEsp;
    }

    /**
     * @param anexosestEsp the anexosestEsp to set
     */
    public void setAnexosestEsp(List<AnexosProyecto> anexosestEsp) {
        this.anexosestEsp = anexosestEsp;
    }

    /**
     * @return the microCuenca2
     */
    public List<Object[]> getMicroCuenca2() {
        return microCuenca2;
    }

    /**
     * @param microCuenca2 the microCuenca2 to set
     */
    public void setMicroCuenca2(List<Object[]> microCuenca2) {
        this.microCuenca2 = microCuenca2;
    }

    /**
     * @return the catEstEsp
     */
    public List<CatEstEsp> getCatEstEsp() {
        return catEstEsp;
    }

    /**
     * @param catEstEsp the catEstEsp to set
     */
    public void setCatEstEsp(List<CatEstEsp> catEstEsp) {
        this.catEstEsp = catEstEsp;
    }

    /**
     * @return the estudiosEsp
     */
    public List<Object> getEstudiosEsp() {
        return estudiosEsp;
    }

    /**
     * @param estudiosEsp the estudiosEsp to set
     */
    public void setEstudiosEsp(List<Object> estudiosEsp) {
        this.estudiosEsp = estudiosEsp;
    }

    public String getBitacora() {
        return bitacora;
    }

	/**
	 * @return the archivosDelSisAmb
	 */
	public List<ArchivosProyecto> getArchivosDelSisAmb() {
		return archivosDelSisAmb;
	}

	/**
	 * @param archivosDelSisAmb the archivosDelSisAmb to set
	 */
	public void setArchivosDelSisAmb(List<ArchivosProyecto> archivosDelSisAmb) {
		this.archivosDelSisAmb = archivosDelSisAmb;
	}

	/**
	 * @return the archivosDelAreaInf
	 */
	public List<ArchivosProyecto> getArchivosDelAreaInf() {
		return archivosDelAreaInf;
	}

	/**
	 * @param archivosDelAreaInf the archivosDelAreaInf to set
	 */
	public void setArchivosDelAreaInf(List<ArchivosProyecto> archivosDelAreaInf) {
		this.archivosDelAreaInf = archivosDelAreaInf;
	}

	/**
	 * @return the archivosDelSitProy
	 */
	public List<ArchivosProyecto> getArchivosDelSitProy() {
		return archivosDelSitProy;
	}

	/**
	 * @param archivosDelSitProy the archivosDelSitProy to set
	 */
	public void setArchivosDelSitProy(List<ArchivosProyecto> archivosDelSitProy) {
		this.archivosDelSitProy = archivosDelSitProy;
	}

	/**
	 * @return the archivosClimaFenMet
	 */
	public List<ArchivosProyecto> getArchivosClimaFenMet() {
		return archivosClimaFenMet;
	}

	/**
	 * @param archivosClimaFenMet the archivosClimaFenMet to set
	 */
	public void setArchivosClimaFenMet(List<ArchivosProyecto> archivosClimaFenMet) {
		this.archivosClimaFenMet = archivosClimaFenMet;
	}

	/**
	 * @return the archivosSuelos
	 */
	public List<ArchivosProyecto> getArchivosSuelos() {
		return archivosSuelos;
	}

	/**
	 * @param archivosSuelos the archivosSuelos to set
	 */
	public void setArchivosSuelos(List<ArchivosProyecto> archivosSuelos) {
		this.archivosSuelos = archivosSuelos;
	}

	/**
	 * @return the archivosPaisajes
	 */
	public List<ArchivosProyecto> getArchivosPaisajes() {
		return archivosPaisajes;
	}

	/**
	 * @param archivosPaisajes the archivosPaisajes to set
	 */
	public void setArchivosPaisajes(List<ArchivosProyecto> archivosPaisajes) {
		this.archivosPaisajes = archivosPaisajes;
	}

	/**
	 * @return the archivosDiagAmb
	 */
	public List<ArchivosProyecto> getArchivosDiagAmb() {
		return archivosDiagAmb;
	}

	/**
	 * @param archivosDiagAmb the archivosDiagAmb to set
	 */
	public void setArchivosDiagAmb(List<ArchivosProyecto> archivosDiagAmb) {
		this.archivosDiagAmb = archivosDiagAmb;
	}

	/**
	 * @return the observacionesClima
	 */
	public String getObservacionesClima() {
		return observacionesClima;
	}

	/**
	 * @param observacionesClima the observacionesClima to set
	 */
	public void setObservacionesClima(String observacionesClima) {
		this.observacionesClima = observacionesClima;
	}

	/**
	 * @return the tempFenMetDescrip
	 */
	public String getTempFenMetDescrip() {
		return tempFenMetDescrip;
	}

	/**
	 * @param tempFenMetDescrip the tempFenMetDescrip to set
	 */
	public void setTempFenMetDescrip(String tempFenMetDescrip) {
		this.tempFenMetDescrip = tempFenMetDescrip;
	}
	/**
	 * @return the proyectoTO
	 */
	public ProyectoTO getProyectoTO() {
		return proyectoTO;
	}
	/**
	 * @param proyectoTO the proyectoTO to set
	 */
	public void setProyectoTO(ProyectoTO proyectoTO) {
		this.proyectoTO = proyectoTO;
	}

	/**
	 * @return the cuerposAgua
	 */
	public List<CuerposAguaProyecto> getCuerposAgua() {
		return cuerposAgua;
	}

	/**
	 * @param cuerposAgua the cuerposAgua to set
	 */
	public void setCuerposAgua(List<CuerposAguaProyecto> cuerposAgua) {
		this.cuerposAgua = cuerposAgua;
	}

	/**
	 * @return the delimitacionSisAmb
	 */
	public String getDelimitacionSisAmb() {
		return delimitacionSisAmb;
	}

	/**
	 * @param delimitacionSisAmb the delimitacionSisAmb to set
	 */
	public void setDelimitacionSisAmb(String delimitacionSisAmb) {
		this.delimitacionSisAmb = delimitacionSisAmb;
	}

	/**
	 * @return the delimitacionAreaInf
	 */
	public String getDelimitacionAreaInf() {
		return delimitacionAreaInf;
	}

	/**
	 * @param delimitacionAreaInf the delimitacionAreaInf to set
	 */
	public void setDelimitacionAreaInf(String delimitacionAreaInf) {
		this.delimitacionAreaInf = delimitacionAreaInf;
	}

	/**
	 * @return the delimitacionSitioProy
	 */
	public String getDelimitacionSitioProy() {
		return delimitacionSitioProy;
	}

	/**
	 * @param delimitacionSitioProy the delimitacionSitioProy to set
	 */
	public void setDelimitacionSitioProy(String delimitacionSitioProy) {
		this.delimitacionSitioProy = delimitacionSitioProy;
	}

	/**
	 * @return the geologiaGeom
	 */
	public String getGeologiaGeom() {
		return geologiaGeom;
	}

	/**
	 * @param geologiaGeom the geologiaGeom to set
	 */
	public void setGeologiaGeom(String geologiaGeom) {
		this.geologiaGeom = geologiaGeom;
	}

	/**
	 * @return the suelos
	 */
	public String getSuelos() {
		return suelos;
	}

	/**
	 * @param suelos the suelos to set
	 */
	public void setSuelos(String suelos) {
		this.suelos = suelos;
	}

	/**
	 * @return the analisisBio
	 */
	public String getAnalisisBio() {
		return analisisBio;
	}

	/**
	 * @param analisisBio the analisisBio to set
	 */
	public void setAnalisisBio(String analisisBio) {
		this.analisisBio = analisisBio;
	}

	/**
	 * @return the paisajes
	 */
	public String getPaisajes() {
		return paisajes;
	}

	/**
	 * @param paisajes the paisajes to set
	 */
	public void setPaisajes(String paisajes) {
		this.paisajes = paisajes;
	}

	/**
	 * @return the dinamicaPob
	 */
	public String getDinamicaPob() {
		return dinamicaPob;
	}

	/**
	 * @param dinamicaPob the dinamicaPob to set
	 */
	public void setDinamicaPob(String dinamicaPob) {
		this.dinamicaPob = dinamicaPob;
	}

	/**
	 * @return the diagnosticoAmb
	 */
	public String getDiagnosticoAmb() {
		return diagnosticoAmb;
	}

	/**
	 * @param diagnosticoAmb the diagnosticoAmb to set
	 */
	public void setDiagnosticoAmb(String diagnosticoAmb) {
		this.diagnosticoAmb = diagnosticoAmb;
	}

	/**
	 * @param climas the climas to set
	 */
	public void setClimas(List<ClimaProyecto> climas) {
		this.climas = climas;
	}

	/**
	 * @return the hidroSub
	 */
	public String getHidroSub() {
		return hidroSub;
	}

	/**
	 * @param hidroSub the hidroSub to set
	 */
	public void setHidroSub(String hidroSub) {
		this.hidroSub = hidroSub;
	}

	/**
	 * @return the hidroSup
	 */
	public String getHidroSup() {
		return hidroSup;
	}

	/**
	 * @param hidroSup the hidroSup to set
	 */
	public void setHidroSup(String hidroSup) {
		this.hidroSup = hidroSup;
	}
    
}