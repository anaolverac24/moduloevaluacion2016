/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.visorMia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.utils.GenericConstants;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "estudioRiesgoView")
@ViewScoped
public class ERAView {

    private final VisorDao daoV = new VisorDao();
    private String pramBita;
    private String folioNum;
    private short serialNum;
    private Proyecto proyecto = null;
    private Proyecto folioDetalle;
    
    private final DgiraMiaeDaoGeneral dao = new DgiraMiaeDaoGeneral();
    private String sector = "";
    private EstudioRiesgoProyecto estudio;
    
    private EstudioRiesgoProyecto estudioR= new EstudioRiesgoProyecto();
    
    private Integer claveTramite = 0;
    private short subsec = 0;
    private Integer idusuario;
    
    private List<AnexosProyecto> anexosList1;
    private List<AnexosProyecto> anexosList2;
    private List<AnexosProyecto> anexosList3;
    private List<AnexosProyecto> anexosList4;
    private List<AnexosProyecto> anexosList5;
    private List<AnexosProyecto> anexosList6;
    private List<AnexosProyecto> anexosList7;
    private List<AnexosProyecto> anexosList8;
    private List<AnexosProyecto> anexosList9;
    private List<AnexosProyecto> anexosList10;
    private List<AnexosProyecto> anexosList11;
    private List<AnexosProyecto> anexosList12;
    private List<AnexosProyecto> anexosList13; 
    private List<AnexosProyecto> anexosList14;
    private List<AnexosProyecto> anexosList15;
    private List<AnexosProyecto> anexosList16;
    private List<AnexosProyecto> anexosList17;
    private List<AnexosProyecto> anexosList18;
    private List<AnexosProyecto> anexosList19;
    private List<AnexosProyecto> anexosList20;
    private List<AnexosProyecto> anexosList21;
    private List<AnexosProyecto> anexosList22;
    private List<AnexosProyecto> anexosList23;
    private List<AnexosProyecto> anexosList24;
    
    //Inicio de campos para 
    private String estudioDescBasesDise;
    private String estudioProyectoCivil;
    private String estudioProyectoMecanico;
    private String estudioProyectoIncendio;
    private String estudioDetalladaProceso;
    private String estudioAlmacenamiento;
    private String estudioProcesoAuxiliar;
    private String estudioPruebasVerificacion;
    private String estudioOperacion;
    private String estudioCuartoControl;
    private String estudioSistemaAislamiento;
    private String estudioAntecedentesAcci;
    private String estudioJerarRiesgo;
    private String estudioRadiosPotenciales;
    private String estudioEfectosAreaInflu;
    private String estudioRecomendacionesTecOp;
    private String estudioSistemasSeguridad;
    private String estudioMedidasPreventivas;
    private String estudioSenalaConclusion;
    private String estudioResumenSituacion;
    private String estudioInformeTecnico;    
    //=========================
   
    private List<Object[]> proyectoSustancia;
    
    private int vistaVersion;
    
    private HashMap<String, boolean[]> mapInfoAdicional;
    
	// Archivos de Bases de Diseno
	private List<ArchivosProyecto> archivosBasesDis = new ArrayList<ArchivosProyecto>();
	// Archivos de Bases de Diseno - Proyecto Civil
	private List<ArchivosProyecto> archivosProyCivil = new ArrayList<ArchivosProyecto>();
	// Archivos de Bases de Diseno - Proyecto Mecanico
	private List<ArchivosProyecto> archivosProyMecanico = new ArrayList<ArchivosProyecto>();
	// Archivos de Bases de Diseno - Proyecto Sistema Contra Incendio
	private List<ArchivosProyecto> archivosProySisConInc = new ArrayList<ArchivosProyecto>();
	// Archivos de Descripcion Detallada del Proceso
	private List<ArchivosProyecto> archivosDescDetPro = new ArrayList<ArchivosProyecto>();
	// Archivos de Descripcion Detallada del Proceso - Hoja de Seguridad
	private List<ArchivosProyecto> archivosHojaSeg = new ArrayList<ArchivosProyecto>();
	// Archivos de Descripcion Detallada del Proceso - Almacenamiento
	private List<ArchivosProyecto> archivosAlmacenamiento = new ArrayList<ArchivosProyecto>();
	// Archivos de Descripcion Detallada del Proceso - Equipos de Proceso y Auxiliares
	private List<ArchivosProyecto> archivosEquiposProAux = new ArrayList<ArchivosProyecto>();
	// Archivos de Descripcion Detallada del Proceso - Pruebas de Verificacion
	private List<ArchivosProyecto> archivosPruebasVer = new ArrayList<ArchivosProyecto>();
	// Archivos de Condiciones de Operacion
	private List<ArchivosProyecto> archivosCondOper = new ArrayList<ArchivosProyecto>();
	// Archivos de Condiciones de Operacion - Especificacion del Cuarto de Control
	private List<ArchivosProyecto> archivosEspeCuarCon = new ArrayList<ArchivosProyecto>();
	// Archivos de Condiciones de Operacion - Sistema de Aislamiento
	private List<ArchivosProyecto> archivosSisAisla = new ArrayList<ArchivosProyecto>();
	// Archivos de Analisis y Evaluacion de Riesgos - Antecedentes de Accidentes e Incidentes
	private List<ArchivosProyecto> archivosAnteAccideInc = new ArrayList<ArchivosProyecto>();
	// Archivos de Analisis y Evaluacion de Riesgos - Metodologias de Identificacion y Jerarquizacion de los Riesgos
	private List<ArchivosProyecto> archivosMetIdeJerarRies = new ArrayList<ArchivosProyecto>();
	// Archivos de Radios Ponteciales de Afectacion
	private List<ArchivosProyecto> archivosRadiosPotenAfec = new ArrayList<ArchivosProyecto>();
	// Archivos de Interacciones de Riesgo
	private List<ArchivosProyecto> archivosInteraccionesRiesgo = new ArrayList<ArchivosProyecto>();
	// Archivos de Efectos Sobre el Area de Influencia
	private List<ArchivosProyecto> archivosEfectosSAreaInflu = new ArrayList<ArchivosProyecto>();
	// Archivos de Recomendaciones Tecnico/Operativas
	private List<ArchivosProyecto> archivosRecoTecOper = new ArrayList<ArchivosProyecto>();
	// Archivos de Recomendaciones Tecnico/Operativas - Sistemas de Seguridad
	private List<ArchivosProyecto> archivosSisSeg = new ArrayList<ArchivosProyecto>();
	// Archivos de Recomendaciones Tecnico/Operativas - Medidas Preventivas
	private List<ArchivosProyecto> archivosMedidasPreve = new ArrayList<ArchivosProyecto>();
	// Archivos de Senalar las Conclusiones del Estudio de Riesgo Ambiental
	private List<ArchivosProyecto> archivosSeConcluEstRieAmb = new ArrayList<ArchivosProyecto>();
	// Archivos de Situacion General que Presenta el Proyecto en Materia de Riesgo Ambiental
	private List<ArchivosProyecto> archivosSitGralPreProyMatRieAmb = new ArrayList<ArchivosProyecto>();
	// Archivos de Presentar el Informe Tecnico Debidamente Llenado
	private List<ArchivosProyecto> archivosPreInfTec = new ArrayList<ArchivosProyecto>();
	// Archivos de Planos de localizacion y Fotografias
	private List<ArchivosProyecto> archivosPlanosLocaFoto = new ArrayList<ArchivosProyecto>();
	// Archivos de Otros Anexos
	private List<ArchivosProyecto> archivosOtrosAnexos = new ArrayList<ArchivosProyecto>();

    @SuppressWarnings({ "static-access", "unused" })
	public ERAView() {
    
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        @SuppressWarnings("rawtypes")
		Map params = ec.getRequestParameterMap();
        pramBita = params.get("bitaNum").toString();
        idusuario = (Integer) ec.getSessionMap().get("idUsu");
        
        vistaVersion = (params.get("vistaVersion") != null) ? 
                Integer.parseInt((String)params.get("vistaVersion")) : 
                GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
        
        if (pramBita != null) 
        { 
            switch (vistaVersion) {
                case GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL:
                    folioDetalle = daoV.obtieneProyectoUltimaVersion(pramBita);
                    break;
                case GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO:
                    folioDetalle = daoV.obtieneProyectoVersionAnterior(pramBita);
                    break;
            }            
            
            if (folioDetalle != null)  
            {
                folioNum = folioDetalle.getProyectoPK().getFolioProyecto();
                serialNum = folioDetalle.getProyectoPK().getSerialProyecto();
                
                proyecto = folioDetalle;
                
                if(proyecto != null)
                {
                    System.out.println("Folio nuevo " + pramBita + " idususrio: " + idusuario);  
                    if (proyecto.getNsub() != null) {
                        SectorProyecto sp = (SectorProyecto) dao.busca(SectorProyecto.class, proyecto.getNsec());
                        sector = sp.getSector();
                        SubsectorProyecto  su = (SubsectorProyecto) dao.busca(SubsectorProyecto.class, proyecto.getNsub());
                    }
                    estudio = dao.cargaEstudioRiesgo(proyecto);
                    
                    
                    
                    if(folioNum != null && serialNum > 0)
                    {
                        //10,2,1,0
                        short capituloId = (short) 0;
                        short subcapituloId = (short) 0;
                        short seccionId = (short) 0;
                        short apartadoId = (short) 0;
                        capituloId = 10;
                        subcapituloId = 2;
                        seccionId = 1;
                        apartadoId = 0;
                        anexosList1 = dao.getAnexos(capituloId, subcapituloId, seccionId, apartadoId, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                        //10,2,1,1  
                        short capituloId2 = (short) 0;
                        short subcapituloId2 = (short) 0;
                        short seccionId2 = (short) 0;
                        short apartadoId2 = (short) 0;
                        capituloId2 = 10;
                        subcapituloId2 = 2;
                        seccionId2 = 1;
                        apartadoId2 = 1;
                        anexosList2 = dao.getAnexos(capituloId2, subcapituloId2, seccionId2, apartadoId2, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,1,2
                        short capituloId3 = (short) 0;
                        short subcapituloId3 = (short) 0;
                        short seccionId3 = (short) 0;
                        short apartadoId3 = (short) 0;
                        capituloId3 = 10;
                        subcapituloId3 = 2;
                        seccionId3 = 1;
                        apartadoId3 = 2;
                        anexosList3 = dao.getAnexos(capituloId3, subcapituloId3, seccionId3, apartadoId3, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                        //10,2,1,3
                        short capituloId4 = (short) 0;
                        short subcapituloId4 = (short) 0;
                        short seccionId4 = (short) 0;
                        short apartadoId4 = (short) 0;
                        capituloId4 = 10;
                        subcapituloId4 = 2;
                        seccionId4 = 1;
                        apartadoId4 = 3;
                        anexosList4 = dao.getAnexos(capituloId4, subcapituloId4, seccionId4, apartadoId4, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,2,0,
                        short capituloId5 = (short) 0;
                        short subcapituloId5 = (short) 0;
                        short seccionId5 = (short) 0;
                        short apartadoId5 = (short) 0;
                        capituloId5 = 10;
                        subcapituloId5 = 2;
                        seccionId5 = 2;
                        apartadoId5 = 0;
                        anexosList5 = dao.getAnexos(capituloId5, subcapituloId5, seccionId5, apartadoId5, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                        // Sustancias
                        proyectoSustancia = dao.sustanciaProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                        //10,2,2,2
                        short capituloId6 = (short) 0;
                        short subcapituloId6 = (short) 0;
                        short seccionId6 = (short) 0;
                        short apartadoId6 = (short) 0;
                        capituloId6 = 10;
                        subcapituloId6 = 2;
                        seccionId6 = 2;
                        apartadoId6 = 2;
                        anexosList6 = dao.getAnexos(capituloId6, subcapituloId6, seccionId6, apartadoId6, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                        //10,2,2,3
                        short capituloId7 = (short) 0;
                        short subcapituloId7 = (short) 0;
                        short seccionId7 = (short) 0;
                        short apartadoId7 = (short) 0;
                        capituloId7 = 10;
                        subcapituloId7 = 2;
                        seccionId7 = 2;
                        apartadoId7 = 3;
                        anexosList7 = dao.getAnexos(capituloId7, subcapituloId7, seccionId7, apartadoId7, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,2,4
                        short capituloId8 = (short) 0;
                        short subcapituloId8 = (short) 0;
                        short seccionId8 = (short) 0;
                        short apartadoId8 = (short) 0;
                        capituloId8 = 10;
                        subcapituloId8 = 2;
                        seccionId8 = 2;
                        apartadoId8 = 4;
                        anexosList8 = dao.getAnexos(capituloId8, subcapituloId8, seccionId8, apartadoId8, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,3,0
                        short capituloId9 = (short) 0;
                        short subcapituloId9 = (short) 0;
                        short seccionId9 = (short) 0;
                        short apartadoId9 = (short) 0;
                        capituloId9 = 10;
                        subcapituloId9 = 2;
                        seccionId9 = 3;
                        apartadoId9 = 0;
                        anexosList9 = dao.getAnexos(capituloId9, subcapituloId9, seccionId9, apartadoId9, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,3,1
                        short capituloId10 = (short) 0;
                        short subcapituloId10 = (short) 0;
                        short seccionId10 = (short) 0;
                        short apartadoId10 = (short) 0;
                        capituloId10 = 10;
                        subcapituloId10 = 2;
                        seccionId10 = 3;
                        apartadoId10 = 1;
                        anexosList10 = dao.getAnexos(capituloId10, subcapituloId10, seccionId10, apartadoId10, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,3,2
                        short capituloId11 = (short) 0;
                        short subcapituloId11 = (short) 0;
                        short seccionId11 = (short) 0;
                        short apartadoId11 = (short) 0;
                        capituloId11 = 10;
                        subcapituloId11 = 2;
                        seccionId11 = 3;
                        apartadoId11 = 2;
                        anexosList11 = dao.getAnexos(capituloId11, subcapituloId11, seccionId11, apartadoId11, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,4,1
                        short capituloId12 = (short) 0;
                        short subcapituloId12 = (short) 0;
                        short seccionId12 = (short) 0;
                        short apartadoId12 = (short) 0;
                        capituloId12 = 10;
                        subcapituloId12 = 2;
                        seccionId12 = 4;
                        apartadoId12 = 1;
                        anexosList12 = dao.getAnexos(capituloId12, subcapituloId12, seccionId12, apartadoId12, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,2,4,2
                        short capituloId13 = (short) 0;
                        short subcapituloId13 = (short) 0;
                        short seccionId13 = (short) 0;
                        short apartadoId13 = (short) 0;
                        capituloId13 = 10;
                        subcapituloId13 = 2;
                        seccionId13 = 4;
                        apartadoId13 = 2;
                        anexosList13 = dao.getAnexos(capituloId13, subcapituloId13, seccionId13, apartadoId13, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,3,1,0
                        short capituloId14 = (short) 0;
                        short subcapituloId14 = (short) 0;
                        short seccionId14 = (short) 0;
                        short apartadoId14 = (short) 0;
                        capituloId14 = 10;
                        subcapituloId14 = 3;
                        seccionId14 = 1;
                        apartadoId14 = 0;
                        anexosList14 = dao.getAnexos(capituloId14, subcapituloId14, seccionId14, apartadoId14, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,3,2,0,
                        short capituloId15 = (short) 0;
                        short subcapituloId15 = (short) 0;
                        short seccionId15 = (short) 0;
                        short apartadoId15 = (short) 0;
                        capituloId15 = 10;
                        subcapituloId15 = 3;
                        seccionId15 = 2;
                        apartadoId15 = 0;
                        anexosList15 = dao.getAnexos(capituloId15, subcapituloId15, seccionId15, apartadoId15, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,3,3,0
                        short capituloId16 = (short) 0;
                        short subcapituloId16 = (short) 0;
                        short seccionId16 = (short) 0;
                        short apartadoId16 = (short) 0;
                        capituloId16 = 10;
                        subcapituloId16 = 3;
                        seccionId16 = 3;
                        apartadoId16 = 0;
                        anexosList16 = dao.getAnexos(capituloId16, subcapituloId16, seccionId16, apartadoId16, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,4,1,0
                        short capituloId17 = (short) 0;
                        short subcapituloId17 = (short) 0;
                        short seccionId17 = (short) 0;
                        short apartadoId17 = (short) 0;
                        capituloId17 = 10;
                        subcapituloId17 = 4;
                        seccionId17 = 1;
                        apartadoId17 = 0;
                        anexosList17 = dao.getAnexos(capituloId17, subcapituloId17, seccionId17, apartadoId17, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,4,1,1
                        short capituloId18 = (short) 0;
                        short subcapituloId18 = (short) 0;
                        short seccionId18 = (short) 0;
                        short apartadoId18 = (short) 0;
                        capituloId18 = 10;
                        subcapituloId18 = 4;
                        seccionId18 = 1;
                        apartadoId18 = 1;
                        anexosList18 = dao.getAnexos(capituloId18, subcapituloId18, seccionId18, apartadoId18, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,4,1,2
                        short capituloId19 = (short) 0;
                        short subcapituloId19 = (short) 0;
                        short seccionId19 = (short) 0;
                        short apartadoId19 = (short) 0;
                        capituloId19 = 10;
                        subcapituloId19 = 4;
                        seccionId19 = 1;
                        apartadoId19 = 2;
                        anexosList19 = dao.getAnexos(capituloId19, subcapituloId19, seccionId19, apartadoId19, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,5,1,0
                        short capituloId20 = (short) 0;
                        short subcapituloId20 = (short) 0;
                        short seccionId20 = (short) 0;
                        short apartadoId20 = (short) 0;
                        capituloId20 = 10;
                        subcapituloId20 = 5;
                        seccionId20 = 1;
                        apartadoId20 = 0;
                        anexosList20 = dao.getAnexos(capituloId20, subcapituloId20, seccionId20, apartadoId20, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,5,2,0
                        short capituloId21 = (short) 0;
                        short subcapituloId21 = (short) 0;
                        short seccionId21 = (short) 0;
                        short apartadoId21 = (short) 0;
                        capituloId21 = 10;
                        subcapituloId21 = 5;
                        seccionId21 = 2;
                        apartadoId21 = 0;
                        anexosList21 = dao.getAnexos(capituloId21, subcapituloId21, seccionId21, apartadoId21, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,5,3,0
                        short capituloId22 = (short) 0;
                        short subcapituloId22 = (short) 0;
                        short seccionId22 = (short) 0;
                        short apartadoId22 = (short) 0;
                        capituloId22 = 10;
                        subcapituloId22 = 5;
                        seccionId22 = 3;
                        apartadoId22 = 0;
                        anexosList22 = dao.getAnexos(capituloId22, subcapituloId22, seccionId22, apartadoId22, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                        //10,6,1,0
                        short capituloId23 = (short) 0;
                        short subcapituloId23 = (short) 0;
                        short seccionId23 = (short) 0;
                        short apartadoId23 = (short) 0;
                        capituloId23 = 10;
                        subcapituloId23 = 6;
                        seccionId23 = 1;
                        apartadoId23 = 0;
                        anexosList23 = dao.getAnexos(capituloId23, subcapituloId23, seccionId23, apartadoId23, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());


                        //10,6,2,0
                        short capituloId24 = (short) 0;
                        short subcapituloId24 = (short) 0;
                        short seccionId24 = (short) 0;
                        short apartadoId24 = (short) 0;
                        capituloId24 = 10;
                        subcapituloId24 = 6;
                        seccionId24 = 2;
                        apartadoId24 = 0;
                        anexosList24 = dao.getAnexos(capituloId24, subcapituloId24, seccionId24, apartadoId24, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    }
                }
                
                //===================================================================================================================================================
                estudioR = dao.estudioRiesgo(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
             estudioDescBasesDise=dao.parseHtmlParagraph(estudioR.getEstudioDescBasesDise()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioProyectoCivil=dao.parseHtmlParagraph(estudioR.getEstudioProyectoCivil()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioProyectoMecanico=dao.parseHtmlParagraph(estudioR.getEstudioProyectoMecanico()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioProyectoIncendio=dao.parseHtmlParagraph(estudioR.getEstudioProyectoIncendio()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioDetalladaProceso=dao.parseHtmlParagraph(estudioR.getEstudioDetalldaProceso()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioAlmacenamiento=dao.parseHtmlParagraph(estudioR.getEstudioAlmacenamiento()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioProcesoAuxiliar=dao.parseHtmlParagraph(estudioR.getEstudioProcesoAuxiliar()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioPruebasVerificacion=dao.parseHtmlParagraph(estudioR.getEstudioPruebasVerificacion()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioOperacion=dao.parseHtmlParagraph(estudioR.getEstudioOperacion()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioCuartoControl=dao.parseHtmlParagraph(estudioR.getEstudioCuartoControl()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioSistemaAislamiento=dao.parseHtmlParagraph(estudioR.getEstudioSistemaAislamiento()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioAntecedentesAcci=dao.parseHtmlParagraph(estudioR.getEstudioAntecedentesAcciInci()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioJerarRiesgo=dao.parseHtmlParagraph(estudioR.getEstudioIntJerarRiesgo()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioRadiosPotenciales=dao.parseHtmlParagraph(estudioR.getEstudioRadiosPotenciales()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioEfectosAreaInflu=dao.parseHtmlParagraph(estudioR.getEstudioEfectosAreaInflu()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioRecomendacionesTecOp=dao.parseHtmlParagraph(estudioR.getEstudioRecomendacionesTecOp()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioSistemasSeguridad=dao.parseHtmlParagraph(estudioR.getEstudioSistemasSeguridad()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioMedidasPreventivas=dao.parseHtmlParagraph(estudioR.getEstudioMedidasPreventivas()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioSenalaConclusion=dao.parseHtmlParagraph(estudioR.getEstudioSenalaConclusion()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioResumenSituacion=dao.parseHtmlParagraph(estudioR.getEstudioResumenSituacion()).toString().replace("[", "").replace("],", "").replace("]", "");
           	 estudioInformeTecnico=dao.parseHtmlParagraph(estudioR.getEstudioInformeTecnico()).toString().replace("[", "").replace("],", "").replace("]", "");
           	    //===================================================================================================================================================            	
           	 
            }
            
            mapInfoAdicional = dao.generaMapaInfoAdicionalCapitulo(pramBita, "10");
            
        }
        
        archivosBasesDis = dao.archivosProyecto((short) 10, (short) 2, (short) 1, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosProyCivil = dao.archivosProyecto((short) 10, (short) 2, (short) 1, (short) 1, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosProyMecanico = dao.archivosProyecto((short) 10, (short) 2, (short) 1, (short) 2, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosProySisConInc = dao.archivosProyecto((short) 10, (short) 2, (short) 1, (short) 3, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosDescDetPro = dao.archivosProyecto((short) 10, (short) 2, (short) 2, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosHojaSeg = dao.archivosProyecto((short) 10, (short) 2, (short) 2, (short) 1, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosAlmacenamiento = dao.archivosProyecto((short) 10, (short) 2, (short) 2, (short) 2, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosEquiposProAux = dao.archivosProyecto((short) 10, (short) 2, (short) 2, (short) 3, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosPruebasVer = dao.archivosProyecto((short) 10, (short) 2, (short) 2, (short) 4, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosCondOper = dao.archivosProyecto((short) 10, (short) 2, (short) 3, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosEspeCuarCon = dao.archivosProyecto((short) 10, (short) 2, (short) 3, (short) 1, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosSisAisla = dao.archivosProyecto((short) 10, (short) 2, (short) 3, (short) 2, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosAnteAccideInc = dao.archivosProyecto((short) 10, (short) 2, (short) 4, (short) 1, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosMetIdeJerarRies = dao.archivosProyecto((short) 10, (short) 2, (short) 4, (short) 2, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());                
        
        archivosRadiosPotenAfec = dao.archivosProyecto((short) 10, (short) 3, (short) 1, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosInteraccionesRiesgo = dao.archivosProyecto((short) 10, (short) 3, (short) 2, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosEfectosSAreaInflu = dao.archivosProyecto((short) 10, (short) 3, (short) 3, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosRecoTecOper = dao.archivosProyecto((short) 10, (short) 4, (short) 1, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosSisSeg = dao.archivosProyecto((short) 10, (short) 4, (short) 1, (short) 1, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosMedidasPreve = dao.archivosProyecto((short) 10, (short) 4, (short) 1, (short) 2, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosSeConcluEstRieAmb = dao.archivosProyecto((short) 10, (short) 5, (short) 1, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosSitGralPreProyMatRieAmb = dao.archivosProyecto((short) 10, (short) 5, (short) 2, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosPreInfTec = dao.archivosProyecto((short) 10, (short) 5, (short) 3, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosPlanosLocaFoto = dao.archivosProyecto((short) 10, (short) 6, (short) 1, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        archivosOtrosAnexos = dao.archivosProyecto((short) 10, (short) 6, (short) 2, (short) 0, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
    }

    /**
     * @return the pramBita
     */
    public String getPramBita() {
        return pramBita;
    }

    /**
     * @param pramBita the pramBita to set
     */
    public void setPramBita(String pramBita) {
        this.pramBita = pramBita;
    }

    /**
     * @return the folioNum
     */
    public String getFolioNum() {
        return folioNum;
    }

    /**
     * @param folioNum the folioNum to set
     */
    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    /**
     * @return the serialNum
     */
    public short getSerialNum() {
        return serialNum;
    }

    /**
     * @param serialNum the serialNum to set
     */
    public void setSerialNum(short serialNum) {
        this.serialNum = serialNum;
    }


    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the estudio
     */
    public EstudioRiesgoProyecto getEstudio() {
        return estudio;
    }

    /**
     * @param estudio the estudio to set
     */
    public void setEstudio(EstudioRiesgoProyecto estudio) {
        this.estudio = estudio;
    }

    /**
     * @return the claveTramite
     */
    public Integer getClaveTramite() {
        return claveTramite;
    }

    /**
     * @param claveTramite the claveTramite to set
     */
    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    /**
     * @return the subsec
     */
    public short getSubsec() {
        return subsec;
    }

    /**
     * @param subsec the subsec to set
     */
    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the anexosList1
     */
    public List<AnexosProyecto> getAnexosList1() {
        return anexosList1;
    }

    /**
     * @param anexosList1 the anexosList1 to set
     */
    public void setAnexosList1(List<AnexosProyecto> anexosList1) {
        this.anexosList1 = anexosList1;
    }

    /**
     * @return the anexosList2
     */
    public List<AnexosProyecto> getAnexosList2() {
        return anexosList2;
    }

    /**
     * @param anexosList2 the anexosList2 to set
     */
    public void setAnexosList2(List<AnexosProyecto> anexosList2) {
        this.anexosList2 = anexosList2;
    }

    /**
     * @return the anexosList3
     */
    public List<AnexosProyecto> getAnexosList3() {
        return anexosList3;
    }

    /**
     * @param anexosList3 the anexosList3 to set
     */
    public void setAnexosList3(List<AnexosProyecto> anexosList3) {
        this.anexosList3 = anexosList3;
    }

    /**
     * @return the anexosList4
     */
    public List<AnexosProyecto> getAnexosList4() {
        return anexosList4;
    }

    /**
     * @param anexosList4 the anexosList4 to set
     */
    public void setAnexosList4(List<AnexosProyecto> anexosList4) {
        this.anexosList4 = anexosList4;
    }

    /**
     * @return the anexosList5
     */
    public List<AnexosProyecto> getAnexosList5() {
        return anexosList5;
    }

    /**
     * @param anexosList5 the anexosList5 to set
     */
    public void setAnexosList5(List<AnexosProyecto> anexosList5) {
        this.anexosList5 = anexosList5;
    }

    /**
     * @return the anexosList6
     */
    public List<AnexosProyecto> getAnexosList6() {
        return anexosList6;
    }

    /**
     * @param anexosList6 the anexosList6 to set
     */
    public void setAnexosList6(List<AnexosProyecto> anexosList6) {
        this.anexosList6 = anexosList6;
    }

    /**
     * @return the anexosList7
     */
    public List<AnexosProyecto> getAnexosList7() {
        return anexosList7;
    }

    /**
     * @param anexosList7 the anexosList7 to set
     */
    public void setAnexosList7(List<AnexosProyecto> anexosList7) {
        this.anexosList7 = anexosList7;
    }

    /**
     * @return the anexosList8
     */
    public List<AnexosProyecto> getAnexosList8() {
        return anexosList8;
    }

    /**
     * @param anexosList8 the anexosList8 to set
     */
    public void setAnexosList8(List<AnexosProyecto> anexosList8) {
        this.anexosList8 = anexosList8;
    }

    /**
     * @return the anexosList9
     */
    public List<AnexosProyecto> getAnexosList9() {
        return anexosList9;
    }

    /**
     * @param anexosList9 the anexosList9 to set
     */
    public void setAnexosList9(List<AnexosProyecto> anexosList9) {
        this.anexosList9 = anexosList9;
    }

    /**
     * @return the anexosList10
     */
    public List<AnexosProyecto> getAnexosList10() {
        return anexosList10;
    }

    /**
     * @param anexosList10 the anexosList10 to set
     */
    public void setAnexosList10(List<AnexosProyecto> anexosList10) {
        this.anexosList10 = anexosList10;
    }

    /**
     * @return the anexosList11
     */
    public List<AnexosProyecto> getAnexosList11() {
        return anexosList11;
    }

    /**
     * @param anexosList11 the anexosList11 to set
     */
    public void setAnexosList11(List<AnexosProyecto> anexosList11) {
        this.anexosList11 = anexosList11;
    }

    /**
     * @return the anexosList12
     */
    public List<AnexosProyecto> getAnexosList12() {
        return anexosList12;
    }

    /**
     * @param anexosList12 the anexosList12 to set
     */
    public void setAnexosList12(List<AnexosProyecto> anexosList12) {
        this.anexosList12 = anexosList12;
    }

    /**
     * @return the anexosList13
     */
    public List<AnexosProyecto> getAnexosList13() {
        return anexosList13;
    }

    /**
     * @param anexosList13 the anexosList13 to set
     */
    public void setAnexosList13(List<AnexosProyecto> anexosList13) {
        this.anexosList13 = anexosList13;
    }

    /**
     * @return the anexosList14
     */
    public List<AnexosProyecto> getAnexosList14() {
        return anexosList14;
    }

    /**
     * @param anexosList14 the anexosList14 to set
     */
    public void setAnexosList14(List<AnexosProyecto> anexosList14) {
        this.anexosList14 = anexosList14;
    }

    /**
     * @return the anexosList15
     */
    public List<AnexosProyecto> getAnexosList15() {
        return anexosList15;
    }

    /**
     * @param anexosList15 the anexosList15 to set
     */
    public void setAnexosList15(List<AnexosProyecto> anexosList15) {
        this.anexosList15 = anexosList15;
    }

    /**
     * @return the anexosList16
     */
    public List<AnexosProyecto> getAnexosList16() {
        return anexosList16;
    }

    /**
     * @param anexosList16 the anexosList16 to set
     */
    public void setAnexosList16(List<AnexosProyecto> anexosList16) {
        this.anexosList16 = anexosList16;
    }

    /**
     * @return the anexosList17
     */
    public List<AnexosProyecto> getAnexosList17() {
        return anexosList17;
    }

    /**
     * @param anexosList17 the anexosList17 to set
     */
    public void setAnexosList17(List<AnexosProyecto> anexosList17) {
        this.anexosList17 = anexosList17;
    }

    /**
     * @return the anexosList18
     */
    public List<AnexosProyecto> getAnexosList18() {
        return anexosList18;
    }

    /**
     * @param anexosList18 the anexosList18 to set
     */
    public void setAnexosList18(List<AnexosProyecto> anexosList18) {
        this.anexosList18 = anexosList18;
    }

    /**
     * @return the anexosList19
     */
    public List<AnexosProyecto> getAnexosList19() {
        return anexosList19;
    }

    /**
     * @param anexosList19 the anexosList19 to set
     */
    public void setAnexosList19(List<AnexosProyecto> anexosList19) {
        this.anexosList19 = anexosList19;
    }

    /**
     * @return the anexosList20
     */
    public List<AnexosProyecto> getAnexosList20() {
        return anexosList20;
    }

    /**
     * @param anexosList20 the anexosList20 to set
     */
    public void setAnexosList20(List<AnexosProyecto> anexosList20) {
        this.anexosList20 = anexosList20;
    }

    /**
     * @return the anexosList21
     */
    public List<AnexosProyecto> getAnexosList21() {
        return anexosList21;
    }

    /**
     * @param anexosList21 the anexosList21 to set
     */
    public void setAnexosList21(List<AnexosProyecto> anexosList21) {
        this.anexosList21 = anexosList21;
    }

    /**
     * @return the anexosList22
     */
    public List<AnexosProyecto> getAnexosList22() {
        return anexosList22;
    }

    /**
     * @param anexosList22 the anexosList22 to set
     */
    public void setAnexosList22(List<AnexosProyecto> anexosList22) {
        this.anexosList22 = anexosList22;
    }

    /**
     * @return the anexosList23
     */
    public List<AnexosProyecto> getAnexosList23() {
        return anexosList23;
    }

    /**
     * @param anexosList23 the anexosList23 to set
     */
    public void setAnexosList23(List<AnexosProyecto> anexosList23) {
        this.anexosList23 = anexosList23;
    }

    /**
     * @return the anexosList24
     */
    public List<AnexosProyecto> getAnexosList24() {
        return anexosList24;
    }

    /**
     * @param anexosList24 the anexosList24 to set
     */
    public void setAnexosList24(List<AnexosProyecto> anexosList24) {
        this.anexosList24 = anexosList24;
    }

    /**
     * @return the proyectoSustancia
     */
    public List<Object[]> getProyectoSustancia() {
        return proyectoSustancia;
    }

    /**
     * @param proyectoSustancia the proyectoSustancia to set
     */
    public void setProyectoSustancia(List<Object[]> proyectoSustancia) {
        this.proyectoSustancia = proyectoSustancia;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

	/**
	 * @return the archivosBasesDis
	 */
	public List<ArchivosProyecto> getArchivosBasesDis() {
		return archivosBasesDis;
	}

	/**
	 * @param archivosBasesDis the archivosBasesDis to set
	 */
	public void setArchivosBasesDis(List<ArchivosProyecto> archivosBasesDis) {
		this.archivosBasesDis = archivosBasesDis;
	}

	/**
	 * @return the archivosProyCivil
	 */
	public List<ArchivosProyecto> getArchivosProyCivil() {
		return archivosProyCivil;
	}

	/**
	 * @param archivosProyCivil the archivosProyCivil to set
	 */
	public void setArchivosProyCivil(List<ArchivosProyecto> archivosProyCivil) {
		this.archivosProyCivil = archivosProyCivil;
	}

	/**
	 * @return the archivosProyMecanico
	 */
	public List<ArchivosProyecto> getArchivosProyMecanico() {
		return archivosProyMecanico;
	}

	/**
	 * @param archivosProyMecanico the archivosProyMecanico to set
	 */
	public void setArchivosProyMecanico(List<ArchivosProyecto> archivosProyMecanico) {
		this.archivosProyMecanico = archivosProyMecanico;
	}

	/**
	 * @return the archivosProySisConInc
	 */
	public List<ArchivosProyecto> getArchivosProySisConInc() {
		return archivosProySisConInc;
	}

	/**
	 * @param archivosProySisConInc the archivosProySisConInc to set
	 */
	public void setArchivosProySisConInc(List<ArchivosProyecto> archivosProySisConInc) {
		this.archivosProySisConInc = archivosProySisConInc;
	}

	/**
	 * @return the archivosDescDetPro
	 */
	public List<ArchivosProyecto> getArchivosDescDetPro() {
		return archivosDescDetPro;
	}

	/**
	 * @param archivosDescDetPro the archivosDescDetPro to set
	 */
	public void setArchivosDescDetPro(List<ArchivosProyecto> archivosDescDetPro) {
		this.archivosDescDetPro = archivosDescDetPro;
	}

	/**
	 * @return the archivosHojaSeg
	 */
	public List<ArchivosProyecto> getArchivosHojaSeg() {
		return archivosHojaSeg;
	}

	/**
	 * @param archivosHojaSeg the archivosHojaSeg to set
	 */
	public void setArchivosHojaSeg(List<ArchivosProyecto> archivosHojaSeg) {
		this.archivosHojaSeg = archivosHojaSeg;
	}

	/**
	 * @return the archivosAlmacenamiento
	 */
	public List<ArchivosProyecto> getArchivosAlmacenamiento() {
		return archivosAlmacenamiento;
	}

	/**
	 * @param archivosAlmacenamiento the archivosAlmacenamiento to set
	 */
	public void setArchivosAlmacenamiento(List<ArchivosProyecto> archivosAlmacenamiento) {
		this.archivosAlmacenamiento = archivosAlmacenamiento;
	}

	/**
	 * @return the archivosEquiposProAux
	 */
	public List<ArchivosProyecto> getArchivosEquiposProAux() {
		return archivosEquiposProAux;
	}

	/**
	 * @param archivosEquiposProAux the archivosEquiposProAux to set
	 */
	public void setArchivosEquiposProAux(List<ArchivosProyecto> archivosEquiposProAux) {
		this.archivosEquiposProAux = archivosEquiposProAux;
	}

	/**
	 * @return the archivosPruebasVer
	 */
	public List<ArchivosProyecto> getArchivosPruebasVer() {
		return archivosPruebasVer;
	}

	/**
	 * @param archivosPruebasVer the archivosPruebasVer to set
	 */
	public void setArchivosPruebasVer(List<ArchivosProyecto> archivosPruebasVer) {
		this.archivosPruebasVer = archivosPruebasVer;
	}

	/**
	 * @return the archivosCondOper
	 */
	public List<ArchivosProyecto> getArchivosCondOper() {
		return archivosCondOper;
	}

	/**
	 * @param archivosCondOper the archivosCondOper to set
	 */
	public void setArchivosCondOper(List<ArchivosProyecto> archivosCondOper) {
		this.archivosCondOper = archivosCondOper;
	}

	/**
	 * @return the archivosEspeCuarCon
	 */
	public List<ArchivosProyecto> getArchivosEspeCuarCon() {
		return archivosEspeCuarCon;
	}

	/**
	 * @param archivosEspeCuarCon the archivosEspeCuarCon to set
	 */
	public void setArchivosEspeCuarCon(List<ArchivosProyecto> archivosEspeCuarCon) {
		this.archivosEspeCuarCon = archivosEspeCuarCon;
	}

	/**
	 * @return the archivosSisAisla
	 */
	public List<ArchivosProyecto> getArchivosSisAisla() {
		return archivosSisAisla;
	}

	/**
	 * @param archivosSisAisla the archivosSisAisla to set
	 */
	public void setArchivosSisAisla(List<ArchivosProyecto> archivosSisAisla) {
		this.archivosSisAisla = archivosSisAisla;
	}

	/**
	 * @return the archivosAnteAccideInc
	 */
	public List<ArchivosProyecto> getArchivosAnteAccideInc() {
		return archivosAnteAccideInc;
	}

	/**
	 * @param archivosAnteAccideInc the archivosAnteAccideInc to set
	 */
	public void setArchivosAnteAccideInc(List<ArchivosProyecto> archivosAnteAccideInc) {
		this.archivosAnteAccideInc = archivosAnteAccideInc;
	}

	/**
	 * @return the archivosMetIdeJerarRies
	 */
	public List<ArchivosProyecto> getArchivosMetIdeJerarRies() {
		return archivosMetIdeJerarRies;
	}

	/**
	 * @param archivosMetIdeJerarRies the archivosMetIdeJerarRies to set
	 */
	public void setArchivosMetIdeJerarRies(List<ArchivosProyecto> archivosMetIdeJerarRies) {
		this.archivosMetIdeJerarRies = archivosMetIdeJerarRies;
	}

	/**
	 * @return the archivosRadiosPotenAfec
	 */
	public List<ArchivosProyecto> getArchivosRadiosPotenAfec() {
		return archivosRadiosPotenAfec;
	}

	/**
	 * @param archivosRadiosPotenAfec the archivosRadiosPotenAfec to set
	 */
	public void setArchivosRadiosPotenAfec(List<ArchivosProyecto> archivosRadiosPotenAfec) {
		this.archivosRadiosPotenAfec = archivosRadiosPotenAfec;
	}

	/**
	 * @return the archivosInteraccionesRiesgo
	 */
	public List<ArchivosProyecto> getArchivosInteraccionesRiesgo() {
		return archivosInteraccionesRiesgo;
	}

	/**
	 * @param archivosInteraccionesRiesgo the archivosInteraccionesRiesgo to set
	 */
	public void setArchivosInteraccionesRiesgo(List<ArchivosProyecto> archivosInteraccionesRiesgo) {
		this.archivosInteraccionesRiesgo = archivosInteraccionesRiesgo;
	}

	/**
	 * @return the archivosEfectosSAreaInflu
	 */
	public List<ArchivosProyecto> getArchivosEfectosSAreaInflu() {
		return archivosEfectosSAreaInflu;
	}

	/**
	 * @param archivosEfectosSAreaInflu the archivosEfectosSAreaInflu to set
	 */
	public void setArchivosEfectosSAreaInflu(List<ArchivosProyecto> archivosEfectosSAreaInflu) {
		this.archivosEfectosSAreaInflu = archivosEfectosSAreaInflu;
	}

	/**
	 * @return the archivosRecoTecOper
	 */
	public List<ArchivosProyecto> getArchivosRecoTecOper() {
		return archivosRecoTecOper;
	}

	/**
	 * @param archivosRecoTecOper the archivosRecoTecOper to set
	 */
	public void setArchivosRecoTecOper(List<ArchivosProyecto> archivosRecoTecOper) {
		this.archivosRecoTecOper = archivosRecoTecOper;
	}

	/**
	 * @return the archivosSisSeg
	 */
	public List<ArchivosProyecto> getArchivosSisSeg() {
		return archivosSisSeg;
	}

	/**
	 * @param archivosSisSeg the archivosSisSeg to set
	 */
	public void setArchivosSisSeg(List<ArchivosProyecto> archivosSisSeg) {
		this.archivosSisSeg = archivosSisSeg;
	}

	/**
	 * @return the archivosMedidasPreve
	 */
	public List<ArchivosProyecto> getArchivosMedidasPreve() {
		return archivosMedidasPreve;
	}

	/**
	 * @param archivosMedidasPreve the archivosMedidasPreve to set
	 */
	public void setArchivosMedidasPreve(List<ArchivosProyecto> archivosMedidasPreve) {
		this.archivosMedidasPreve = archivosMedidasPreve;
	}

	/**
	 * @return the archivosSeConcluEstRieAmb
	 */
	public List<ArchivosProyecto> getArchivosSeConcluEstRieAmb() {
		return archivosSeConcluEstRieAmb;
	}

	/**
	 * @param archivosSeConcluEstRieAmb the archivosSeConcluEstRieAmb to set
	 */
	public void setArchivosSeConcluEstRieAmb(List<ArchivosProyecto> archivosSeConcluEstRieAmb) {
		this.archivosSeConcluEstRieAmb = archivosSeConcluEstRieAmb;
	}

	/**
	 * @return the archivosSitGralPreProyMatRieAmb
	 */
	public List<ArchivosProyecto> getArchivosSitGralPreProyMatRieAmb() {
		return archivosSitGralPreProyMatRieAmb;
	}

	/**
	 * @param archivosSitGralPreProyMatRieAmb the archivosSitGralPreProyMatRieAmb to set
	 */
	public void setArchivosSitGralPreProyMatRieAmb(List<ArchivosProyecto> archivosSitGralPreProyMatRieAmb) {
		this.archivosSitGralPreProyMatRieAmb = archivosSitGralPreProyMatRieAmb;
	}

	/**
	 * @return the archivosPreInfTec
	 */
	public List<ArchivosProyecto> getArchivosPreInfTec() {
		return archivosPreInfTec;
	}

	/**
	 * @param archivosPreInfTec the archivosPreInfTec to set
	 */
	public void setArchivosPreInfTec(List<ArchivosProyecto> archivosPreInfTec) {
		this.archivosPreInfTec = archivosPreInfTec;
	}

	/**
	 * @return the archivosPlanosLocaFoto
	 */
	public List<ArchivosProyecto> getArchivosPlanosLocaFoto() {
		return archivosPlanosLocaFoto;
	}

	/**
	 * @param archivosPlanosLocaFoto the archivosPlanosLocaFoto to set
	 */
	public void setArchivosPlanosLocaFoto(List<ArchivosProyecto> archivosPlanosLocaFoto) {
		this.archivosPlanosLocaFoto = archivosPlanosLocaFoto;
	}

	/**
	 * @return the archivosOtrosAnexos
	 */
	public List<ArchivosProyecto> getArchivosOtrosAnexos() {
		return archivosOtrosAnexos;
	}

	/**
	 * @param archivosOtrosAnexos the archivosOtrosAnexos to set
	 */
	public void setArchivosOtrosAnexos(List<ArchivosProyecto> archivosOtrosAnexos) {
		this.archivosOtrosAnexos = archivosOtrosAnexos;
	}

	/**
	 * @return the estudioDescBasesDise
	 */
	public String getEstudioDescBasesDise() {
		return estudioDescBasesDise;
	}

	/**
	 * @param estudioDescBasesDise the estudioDescBasesDise to set
	 */
	public void setEstudioDescBasesDise(String estudioDescBasesDise) {
		this.estudioDescBasesDise = estudioDescBasesDise;
	}

	/**
	 * @return the estudioProyectoCivil
	 */
	public String getEstudioProyectoCivil() {
		return estudioProyectoCivil;
	}

	/**
	 * @param estudioProyectoCivil the estudioProyectoCivil to set
	 */
	public void setEstudioProyectoCivil(String estudioProyectoCivil) {
		this.estudioProyectoCivil = estudioProyectoCivil;
	}

	/**
	 * @return the estudioProyectoMecanico
	 */
	public String getEstudioProyectoMecanico() {
		return estudioProyectoMecanico;
	}

	/**
	 * @param estudioProyectoMecanico the estudioProyectoMecanico to set
	 */
	public void setEstudioProyectoMecanico(String estudioProyectoMecanico) {
		this.estudioProyectoMecanico = estudioProyectoMecanico;
	}

	/**
	 * @return the estudioProyectoIncendio
	 */
	public String getEstudioProyectoIncendio() {
		return estudioProyectoIncendio;
	}

	/**
	 * @param estudioProyectoIncendio the estudioProyectoIncendio to set
	 */
	public void setEstudioProyectoIncendio(String estudioProyectoIncendio) {
		this.estudioProyectoIncendio = estudioProyectoIncendio;
	}

	/**
	 * @return the estudioDetalladaProceso
	 */
	public String getEstudioDetalladaProceso() {
		return estudioDetalladaProceso;
	}

	/**
	 * @param estudioDetalladaProceso the estudioDetalladaProceso to set
	 */
	public void setEstudioDetalladaProceso(String estudioDetalladaProceso) {
		this.estudioDetalladaProceso = estudioDetalladaProceso;
	}

	/**
	 * @return the estudioAlmacenamiento
	 */
	public String getEstudioAlmacenamiento() {
		return estudioAlmacenamiento;
	}

	/**
	 * @param estudioAlmacenamiento the estudioAlmacenamiento to set
	 */
	public void setEstudioAlmacenamiento(String estudioAlmacenamiento) {
		this.estudioAlmacenamiento = estudioAlmacenamiento;
	}

	/**
	 * @return the estudioProcesoAuxiliar
	 */
	public String getEstudioProcesoAuxiliar() {
		return estudioProcesoAuxiliar;
	}

	/**
	 * @param estudioProcesoAuxiliar the estudioProcesoAuxiliar to set
	 */
	public void setEstudioProcesoAuxiliar(String estudioProcesoAuxiliar) {
		this.estudioProcesoAuxiliar = estudioProcesoAuxiliar;
	}

	/**
	 * @return the estudioPruebasVerificacion
	 */
	public String getEstudioPruebasVerificacion() {
		return estudioPruebasVerificacion;
	}

	/**
	 * @param estudioPruebasVerificacion the estudioPruebasVerificacion to set
	 */
	public void setEstudioPruebasVerificacion(String estudioPruebasVerificacion) {
		this.estudioPruebasVerificacion = estudioPruebasVerificacion;
	}

	/**
	 * @return the estudioOperacion
	 */
	public String getEstudioOperacion() {
		return estudioOperacion;
	}

	/**
	 * @param estudioOperacion the estudioOperacion to set
	 */
	public void setEstudioOperacion(String estudioOperacion) {
		this.estudioOperacion = estudioOperacion;
	}

	/**
	 * @return the estudioCuartoControl
	 */
	public String getEstudioCuartoControl() {
		return estudioCuartoControl;
	}

	/**
	 * @param estudioCuartoControl the estudioCuartoControl to set
	 */
	public void setEstudioCuartoControl(String estudioCuartoControl) {
		this.estudioCuartoControl = estudioCuartoControl;
	}

	/**
	 * @return the estudioSistemaAislamiento
	 */
	public String getEstudioSistemaAislamiento() {
		return estudioSistemaAislamiento;
	}

	/**
	 * @param estudioSistemaAislamiento the estudioSistemaAislamiento to set
	 */
	public void setEstudioSistemaAislamiento(String estudioSistemaAislamiento) {
		this.estudioSistemaAislamiento = estudioSistemaAislamiento;
	}

	/**
	 * @return the estudioAntecedentesAcci
	 */
	public String getEstudioAntecedentesAcci() {
		return estudioAntecedentesAcci;
	}

	/**
	 * @param estudioAntecedentesAcci the estudioAntecedentesAcci to set
	 */
	public void setEstudioAntecedentesAcci(String estudioAntecedentesAcci) {
		this.estudioAntecedentesAcci = estudioAntecedentesAcci;
	}

	/**
	 * @return the estudioJerarRiesgo
	 */
	public String getEstudioJerarRiesgo() {
		return estudioJerarRiesgo;
	}

	/**
	 * @param estudioJerarRiesgo the estudioJerarRiesgo to set
	 */
	public void setEstudioJerarRiesgo(String estudioJerarRiesgo) {
		this.estudioJerarRiesgo = estudioJerarRiesgo;
	}

	/**
	 * @return the estudioRadiosPotenciales
	 */
	public String getEstudioRadiosPotenciales() {
		return estudioRadiosPotenciales;
	}

	/**
	 * @param estudioRadiosPotenciales the estudioRadiosPotenciales to set
	 */
	public void setEstudioRadiosPotenciales(String estudioRadiosPotenciales) {
		this.estudioRadiosPotenciales = estudioRadiosPotenciales;
	}

	/**
	 * @return the estudioEfectosAreaInflu
	 */
	public String getEstudioEfectosAreaInflu() {
		return estudioEfectosAreaInflu;
	}

	/**
	 * @param estudioEfectosAreaInflu the estudioEfectosAreaInflu to set
	 */
	public void setEstudioEfectosAreaInflu(String estudioEfectosAreaInflu) {
		this.estudioEfectosAreaInflu = estudioEfectosAreaInflu;
	}

	/**
	 * @return the estudioRecomendacionesTecOp
	 */
	public String getEstudioRecomendacionesTecOp() {
		return estudioRecomendacionesTecOp;
	}

	/**
	 * @param estudioRecomendacionesTecOp the estudioRecomendacionesTecOp to set
	 */
	public void setEstudioRecomendacionesTecOp(String estudioRecomendacionesTecOp) {
		this.estudioRecomendacionesTecOp = estudioRecomendacionesTecOp;
	}

	/**
	 * @return the estudioSistemasSeguridad
	 */
	public String getEstudioSistemasSeguridad() {
		return estudioSistemasSeguridad;
	}

	/**
	 * @param estudioSistemasSeguridad the estudioSistemasSeguridad to set
	 */
	public void setEstudioSistemasSeguridad(String estudioSistemasSeguridad) {
		this.estudioSistemasSeguridad = estudioSistemasSeguridad;
	}

	/**
	 * @return the estudioMedidasPreventivas
	 */
	public String getEstudioMedidasPreventivas() {
		return estudioMedidasPreventivas;
	}

	/**
	 * @param estudioMedidasPreventivas the estudioMedidasPreventivas to set
	 */
	public void setEstudioMedidasPreventivas(String estudioMedidasPreventivas) {
		this.estudioMedidasPreventivas = estudioMedidasPreventivas;
	}

	/**
	 * @return the estudioSenalaConclusion
	 */
	public String getEstudioSenalaConclusion() {
		return estudioSenalaConclusion;
	}

	/**
	 * @param estudioSenalaConclusion the estudioSenalaConclusion to set
	 */
	public void setEstudioSenalaConclusion(String estudioSenalaConclusion) {
		this.estudioSenalaConclusion = estudioSenalaConclusion;
	}

	/**
	 * @return the estudioResumenSituacion
	 */
	public String getEstudioResumenSituacion() {
		return estudioResumenSituacion;
	}

	/**
	 * @param estudioResumenSituacion the estudioResumenSituacion to set
	 */
	public void setEstudioResumenSituacion(String estudioResumenSituacion) {
		this.estudioResumenSituacion = estudioResumenSituacion;
	}

	/**
	 * @return the estudioInformeTecnico
	 */
	public String getEstudioInformeTecnico() {
		return estudioInformeTecnico;
	}

	/**
	 * @param estudioInformeTecnico the estudioInformeTecnico to set
	 */
	public void setEstudioInformeTecnico(String estudioInformeTecnico) {
		this.estudioInformeTecnico = estudioInformeTecnico;
	}

	
	//====================================================
	
	
	
	//====================================================
	
	
}

