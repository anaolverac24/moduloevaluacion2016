/**
 * 
 */
package mx.gob.semarnat.view.visorMia;

import java.io.Serializable;

/**
 * @author Cesar
 *
 */
public class NodeSueloVegetacion implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -1041848930696605557L;
	
	private int id;
	private String nombreObra;
	private String grupoVegetacion;
	private String tipoVegetacion;
	private String faseVegetacion;
	private String descripcion;
	private String diagnostico;
	private double superficie;
	private String tipoRow;
	
	/**
	 * Constructor default
	 */
	public NodeSueloVegetacion() {
		super();
	}
	
	/**
	 * Constructor alterno
	 * @param id
	 * @param nombreObra
	 * @param grupoVegetacion
	 * @param tipoVegetacion
	 * @param faseVegetacion
	 * @param descripcion
	 * @param tipoRow
	 */
	public NodeSueloVegetacion(int id, String nombreObra, String grupoVegetacion, String tipoVegetacion,
			String faseVegetacion, String descripcion, Double superficie, String tipoRow) {
		this.id = id;
		this.nombreObra = nombreObra;
		this.grupoVegetacion = grupoVegetacion;
		this.tipoVegetacion = tipoVegetacion;
		this.faseVegetacion = faseVegetacion;
		this.descripcion = descripcion;
		this.superficie = superficie;
		this.tipoRow = tipoRow;
	}
	
	/**
	 * Constructor alterno
	 * @param id
	 * @param nombreObra
	 * @param grupoVegetacion
	 * @param tipoVegetacion
	 * @param faseVegetacion
	 * @param descripcion
	 * @param diagnostico
	 * @param tipoRow
	 */
	public NodeSueloVegetacion(int id, String nombreObra, String grupoVegetacion, String tipoVegetacion,
			String faseVegetacion, String descripcion, String diagnostico, Double superficie, String tipoRow) {
		this.id = id;
		this.nombreObra = nombreObra;
		this.grupoVegetacion = grupoVegetacion;
		this.tipoVegetacion = tipoVegetacion;
		this.faseVegetacion = faseVegetacion;
		this.descripcion = descripcion;
		this.diagnostico = diagnostico;
		this.superficie = superficie;
		this.tipoRow = tipoRow;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nombreObra
	 */
	public String getNombreObra() {
		return nombreObra;
	}
	/**
	 * @param nombreObra the nombreObra to set
	 */
	public void setNombreObra(String nombreObra) {
		this.nombreObra = nombreObra;
	}
	/**
	 * @return the grupoVegetacion
	 */
	public String getGrupoVegetacion() {
		return grupoVegetacion;
	}
	/**
	 * @param grupoVegetacion the grupoVegetacion to set
	 */
	public void setGrupoVegetacion(String grupoVegetacion) {
		this.grupoVegetacion = grupoVegetacion;
	}
	/**
	 * @return the tipoVegetacion
	 */
	public String getTipoVegetacion() {
		return tipoVegetacion;
	}
	/**
	 * @param tipoVegetacion the tipoVegetacion to set
	 */
	public void setTipoVegetacion(String tipoVegetacion) {
		this.tipoVegetacion = tipoVegetacion;
	}
	/**
	 * @return the faseVegetacion
	 */
	public String getFaseVegetacion() {
		return faseVegetacion;
	}
	/**
	 * @param faseVegetacion the faseVegetacion to set
	 */
	public void setFaseVegetacion(String faseVegetacion) {
		this.faseVegetacion = faseVegetacion;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the tipoRow
	 */
	public String getTipoRow() {
		return tipoRow;
	}
	/**
	 * @param tipoRow the tipoRow to set
	 */
	public void setTipoRow(String tipoRow) {
		this.tipoRow = tipoRow;
	}

	/**
	 * @return the superficie
	 */
	public double getSuperficie() {
		return superficie;
	}

	/**
	 * @param superficie the superficie to set
	 */
	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
}
