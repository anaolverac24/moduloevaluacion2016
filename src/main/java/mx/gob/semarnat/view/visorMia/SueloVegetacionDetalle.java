/**
 * 
 */
package mx.gob.semarnat.view.visorMia;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.semarnat.model.dgira_miae.SueloVegetacionProyecto;

/**
 * @author 
 *
 */
@Entity
@Table(name="SUELO_VEGETACION_DETALLE", schema="DGIRA_MIAE2")
@XmlRootElement
public class SueloVegetacionDetalle implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 8744950213855773348L;
	
	@Id
	@GeneratedValue(generator="InvSeq") 
    @SequenceGenerator(name="InvSeq",sequenceName="SEQ_ID_SUE_VEG_DETALLE")  
	@Basic(optional = false)
	@Column(name="ID_SEQ_SUE_VEG_DETALLE")
	private Integer idSueloVegetacionDetalle;
	
	@JoinColumn(name="ID_GRUPO_VEGETACION", referencedColumnName="ID_VEG_GRUPO")
	@ManyToOne
	private VegGrupo vegGrupo;
	
	@JoinColumn(name="ID_TIPO_VEGETACION", referencedColumnName="ID_VEG_TIPO")
	@ManyToOne
	private VegTipo vegTipo;
	
	@JoinColumn(name="ID_FASE_VEGETACION", referencedColumnName="ID_VEG_FASE")
	@ManyToOne
	private VegFase vegFase;
	
	@Column(name="SUPERFICIE")
	private Double superficie;
	
	@Column(name="DIAGNOSTICO")
	private String diagnostico;
	
	@Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "SUELO_VEG_PROY_ID")
    private short sueloVegProyId;
	
	@JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SUELO_VEG_PROY_ID", referencedColumnName = "SUELO_VEG_PROY_ID", insertable = false, updatable = false)})
	@ManyToOne
	private SueloVegetacionProyecto sueloVegetacionProyecto;

	/**
	 * Constructor default
	 */
	public SueloVegetacionDetalle() {
	}
	
	public SueloVegetacionDetalle(String folioProyecto, short serialProyecto) {
		this.folioProyecto = folioProyecto;
		this.serialProyecto = serialProyecto;
	}
	
	/**
	 * Constructor alterno
	 * @param sueloVegProyecto
	 */
	public SueloVegetacionDetalle(String folioProyecto, short serialProyecto, short sueloVegProyId) {
		this.folioProyecto = folioProyecto;
		this.serialProyecto = serialProyecto;
		this.sueloVegProyId = sueloVegProyId;
	}
	
	/**
	 * @return the idSueloVegetacionDetalle
	 */
	public Integer getIdSueloVegetacionDetalle() {
		return idSueloVegetacionDetalle;
	}

	/**
	 * @param idSueloVegetacionDetalle the idSueloVegetacionDetalle to set
	 */
	public void setIdSueloVegetacionDetalle(Integer idSueloVegetacionDetalle) {
		this.idSueloVegetacionDetalle = idSueloVegetacionDetalle;
	}

	/**
	 * @return the vegGrupo
	 */
	public VegGrupo getVegGrupo() {
		return vegGrupo;
	}

	/**
	 * @param vegGrupo the vegGrupo to set
	 */
	public void setVegGrupo(VegGrupo vegGrupo) {
		this.vegGrupo = vegGrupo;
	}

	/**
	 * @return the vegTipo
	 */
	public VegTipo getVegTipo() {
		return vegTipo;
	}

	/**
	 * @param vegTipo the vegTipo to set
	 */
	public void setVegTipo(VegTipo vegTipo) {
		this.vegTipo = vegTipo;
	}

	/**
	 * @return the vegFase
	 */
	public VegFase getVegFase() {
		return vegFase;
	}

	/**
	 * @param vegFase the vegFase to set
	 */
	public void setVegFase(VegFase vegFase) {
		this.vegFase = vegFase;
	}

	/**
	 * @return the superficie
	 */
	public Double getSuperficie() {
		return superficie;
	}

	/**
	 * @param superficie the superficie to set
	 */
	public void setSuperficie(Double superficie) {
		this.superficie = superficie;
	}
	
    public String getDiagnosticoCorto() {
    	if (diagnostico != null) {
            if(diagnostico.length()<199){
                return diagnostico;
            }
            return diagnostico.substring(0, 199);			
		} else {
			return diagnostico;
		}
    }

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	/**
	 * @return the sueloVegetacionProyecto
	 */
	public SueloVegetacionProyecto getSueloVegetacionProyecto() {
		return sueloVegetacionProyecto;
	}

	/**
	 * @param sueloVegetacionProyecto the sueloVegetacionProyecto to set
	 */
	public void setSueloVegetacionProyecto(
			SueloVegetacionProyecto sueloVegetacionProyecto) {
		this.sueloVegetacionProyecto = sueloVegetacionProyecto;
	}
}
