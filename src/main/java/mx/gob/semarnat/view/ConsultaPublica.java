/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.view;

import com.itextpdf.text.DocumentException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import mx.gob.semarnat.dao.APDao;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.CadenaFirmaDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.LoginDAO;
import mx.gob.semarnat.dao.MenuDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.dao.procedures.SegundasVias;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.CadenaFirma;
import mx.gob.semarnat.model.bitacora.CadenaFirmaLog;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento;
import mx.gob.semarnat.model.dgira_miae.ConsultaPublicaProyecto;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbusuario;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.secure.AppSession;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import net.firel.BcAgregaCadenaRespuesta;
import net.firel.DetallesFirmado;
import net.firel.Firmas;


/**
 *
 * @author Paty
 */
@SuppressWarnings("serial")
@ManagedBean(name = "ConsultaP")
@ViewScoped
public class ConsultaPublica implements Serializable{

    private static final Logger logger = Logger.getLogger(ConsultaPublica.class.getName());
    
    
    /**
     * @return the dao
     */
    public BitacoraDao getDao() {
        return new BitacoraDao();
    }

//    /**
//     * @param aDao the dao to set
//     */
//    public static void setDao(BitacoraDao aDao) {
//        dao = aDao;
//    }

    private String bitaProyecto;   
    private String bitaProyecto2;
    private BitacoraDao dao = new BitacoraDao();
    private final MenuDao MenuDao= new MenuDao();
    private final DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
    private final APDao daoAP = new APDao();
    private List<Object[]> docsDonsPub = new ArrayList<Object[]>();
    private List<Object[]> datosBitacora = new ArrayList<Object[]>();
    private List<Object[]> promoTram = new ArrayList<Object[]>();
    private String nombProm;
    private String cveProyecto;
    private String resp = ""; //default value 
    private boolean bandera = false;
    private boolean ofCiudad = false;
    private boolean ofCiProm = false;
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<WizardHelper> listaUno = new ArrayList();
    private HttpSession sesion = Util.getSession();
    private String cveTipTram; 
    private Integer idTipTram;
    private String edoGestTram;
    private Bitacora bitacora1;
    private SinatDgira sinatDgira;
    private String idArea;
    private Integer idusuario;
    private List<CatEstatusDocumento> subSectorCat;
    private List<Historial> HistorialCIS303;
    private Boolean deInfoAdic = false; //bandera que indica si un trámite ya vino de una suspención por info adicional
    private List<Historial> NoHistorialAT0022; 
    private List<Historial> NoHistorialCIS106;
    private List<Historial> NoHistorial200501;

    private boolean tieneCiudPrinc;
    private boolean conModificacion;
    private Short estatusDoctoTurnado;
    private String comentarioTurnado = "";
    private WizardHelper doctoPromovente;
    private WizardHelper doctoDelegaciones;
    
    private List<Object[]> comentariosTab;
    private HashMap<String,List<Object[]>> mapComentarios = new HashMap<String, List<Object[]>>(); 

    private StreamedContent file;
    
    private Tbarea tbarea = new Tbarea();
    
    private String idEntidad;
    
    Tbusuario usuariorecibe = null;
    String usuariorecibetxt = "";
    
    
    // VARIABLES que se utilizan para el proceso de turnado del DG exclusivamente
    // -----------------------------------------------------------------------------------
    /***
     * setea la accion a realizar con el turnado
     */
    private short idDocumento = 0; //variable ( se usa solo para cuando turna el DG ) setea la accion a realizar con el turnado
    private String folio;
    private Integer idAux = 0;
    private short maxIdHisto = 0;
    private Boolean turnaDocsBtn = false;
    private Bitacora bitacora2;
    private final BitacoraDao daoBitacora = new BitacoraDao();
    private String tipoDocId = "";
    private short tipoDoc = 0;
    private List<Object[]> maxDocsDgiraFlujoHist  = new ArrayList<Object[]>();
    private DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
    private DocumentoDgiraFlujo docsDgiraFlujo;
    private CatEstatusDocumento catEstatusDoc;
    private DocumentoDgiraFlujoHist docsDgiraFlujoHist;
    private int tipoSuspencion=0; 
    private int tipoApertura = 0;
    
    DocumentoRespuestaDgira documento = new DocumentoRespuestaDgira();
    
    private String nombreOfic=""; 
    private String urlOfic="";
 // valor 0 o 1 que indica si esta bitacora (pantalla de checklist) biene de una invocación del DG
    private int isUrlDG = 0;
    // valor 1 o 2 que indica si es la primer bandeja o segunda bandeja del DG ( si no es del dg ni si quiera se trae el valor en los parametros de la url 
    private int bandeja = 1; // por defecto es la bandeja 1
    
    
    
    // -----------------------------------------------------------------------------------
    
    public ConsultaPublica() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        System.out.println("\n\nConsultaPublica..");

        @SuppressWarnings("unused")
		String idProm=null;
        nombProm = null;
        FacesContext fContext = FacesContext.getCurrentInstance();
        bitaProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        bitaProyecto2 =  fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();   
        //cveTipTram = (String) fContext.getExternalContext().getSessionMap().get("cveTramite");
        //idTipTram = (Integer) fContext.getExternalContext().getSessionMap().get("idTramite");
        //edoGestTram = (String) fContext.getExternalContext().getSessionMap().get("edoGestTramite");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu");
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
        
     // esta informacion tiene valores en la sesion cuando lo invoca el dg, en otros roles no biene en la url por tanto en detalleproyectoview no se cachan los parametros de la url
        isUrlDG = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("isUrlDG").toString());
        bandeja = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("bandeja").toString());
        
        System.out.println("isUrlDG: " + isUrlDG);
        System.out.println("bandeja: " + bandeja);
        
        cveTipTram = dao.cveTipTram(bitaProyecto);
        idTipTram = dao.idTipTram(bitaProyecto);        
        edoGestTram = dao.entGestTram(bitaProyecto);
         
        HttpSession session = Util.getSession();
        tbarea = (Tbarea) session.getAttribute("areaDelegacionUsuario");
        idEntidad = tbarea.getIdentidadfederativa();
        System.out.println(tbarea.getArea()+" - "+idEntidad);
        
        LoginDAO loginDao = new LoginDAO();
        usuariorecibe = loginDao.getUsuario(idusuario);
	    if(usuariorecibe != null){
	        	usuariorecibetxt = usuariorecibe.getIdempleado().getNombre() + " " + usuariorecibe.getIdempleado().getApellidopaterno() + " " + usuariorecibe.getIdempleado().getApellidomaterno();
	        System.out.println("Usuario recibe: " + usuariorecibetxt);
	    }else{
	        	usuariorecibetxt = "";
	        	System.out.println("Usuario recibe: NULL");
	    }        
        

        bitaProyecto = bitaProyecto2;
        carga();
        if(bitaProyecto != null) {
            HistorialCIS303 = dao.numRegHistStatus(bitaProyecto,"CIS303");
            NoHistorialAT0022 = dao.numRegHistStatus(bitaProyecto,"AT0022");
            NoHistorialCIS106 = dao.numRegHistStatus(bitaProyecto,"CIS106"); 
            NoHistorial200501 = dao.numRegHistStatus(bitaProyecto,"200501");
            if ( !NoHistorialAT0022.isEmpty() || !NoHistorialCIS106.isEmpty() || !NoHistorial200501.isEmpty() || HistorialCIS303.size() == 1) //si el tamaño es igual a 1, indica que el trámite ya esta suspendido por info adicional
            { deInfoAdic = true; }
            else { deInfoAdic = false; }
            
            if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
            {
                subSectorCat = daoDg.getCatStatusDocs();
            }
            if (sesion.getAttribute("rol").toString().equals("da")) //Director de Area
            {
                subSectorCat = daoDg.getCatStatusDocs2();
            }
            if (sesion.getAttribute("rol").toString().equals("sd")) //Sub Director
            {
                subSectorCat = daoDg.getCatStatusDocs2();
            }
            if (sesion.getAttribute("rol").toString().equals("eva")) //Evaluador
            {
                subSectorCat = daoDg.getCatStatusDocs3();
            }
        }
        
        short[] lstTipos ={GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS, GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL};
        for(short sTipo : lstTipos){
            List<Object[]> lstComentarios;
            String idDoc, idLst;
//            comentariosTab = dao.obtenerObservacionesOficiosTodos(bitaProyecto, sTipo);
            
//            System.out.println("ID_ENTIDAD: "+idEntidad);
            //Se pasa el parametro idEntidad para realizar consultas deacuerdo a la entidad del usuario en Sesion
            comentariosTab = dao.obtenerObservacionesOficiosTodos(bitaProyecto, sTipo, idEntidad);
            
            for (Object[] obj : comentariosTab) {
                idDoc = (String)obj[6];
                idLst = sTipo + "_" + idDoc;

                lstComentarios = mapComentarios.get(idLst);
                if ( lstComentarios == null ) {
                    lstComentarios = new ArrayList<>();
                }
                lstComentarios.add(obj);

                mapComentarios.put(idLst, lstComentarios);
            }
        }
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }

    /**
     * @return the bitaProyecto
     */
    public String getBitaProyecto() {
        return bitaProyecto;
    }

    /**
     * @param bitaProyecto the bitaProyecto to set
     */
    public void setBitaProyecto(String bitaProyecto) {
        this.bitaProyecto = bitaProyecto;
    }

    /**
     * @return the docsDonsPub
     */
    public List<Object[]> getDocsDonsPub() {
        return docsDonsPub;
    }

    /**
     * @param docsDonsPub the docsDonsPub to set
     */
    public void setDocsDonsPub(List<Object[]> docsDonsPub) {
        this.docsDonsPub = docsDonsPub;
    }

    /**
     * @return the datosBitacora
     */
    public List<Object[]> getDatosBitacora() {
        return datosBitacora;
    }

    /**
     * @param datosBitacora the datosBitacora to set
     */
    public void setDatosBitacora(List<Object[]> datosBitacora) {
        this.datosBitacora = datosBitacora;
    }

    /**
     * @return the promoTram
     */
    public List<Object[]> getPromoTram() {
        return promoTram;
    }

    /**
     * @param promoTram the promoTram to set
     */
    public void setPromoTram(List<Object[]> promoTram) {
        this.promoTram = promoTram;
    }

    /**
     * @return the nombProm
     */
    public String getNombProm() {
        return nombProm;
    }

    /**
     * @param nombProm the nombProm to set
     */
    public void setNombProm(String nombProm) {
        this.nombProm = nombProm;
    }

    /**
     * @return the cveProyecto
     */
    public String getCveProyecto() {
        return cveProyecto;
    }

    /**
     * @param cveProyecto the cveProyecto to set
     */
    public void setCveProyecto(String cveProyecto) {
        this.cveProyecto = cveProyecto;
    }

    /**
     * @return the MenuDao
     */
    public MenuDao getMenuDao() {
        return MenuDao;
    }


    /**
     * @return the resp
     */
    public String getResp() {
        return resp;
    }

    /**
     * @param resp the resp to set
     */
    public void setResp(String resp) {
        this.resp = resp;
    }
    
    public void repuestaChanged(){
        //assign new value to localeCode
        //resp = e.getNewValue().toString();
        System.out.print(resp);
        if(resp.equals("S") )
        {
            bandera = true;
            ofCiudad = true;
        }
        //JOptionPane.showMessageDialog(null,  "  resp: " + resp , "Error", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * @return the bandera
     */
    public boolean isBandera() {
        return bandera;
    }

    /**
     * @param bandera the bandera to set
     */
    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

    /**
     * @return the ofCiudad
     */
    public boolean isOfCiudad() {
        return ofCiudad;
    }

    /**
     * @param ofCiudad the ofCiudad to set
     */
    public void setOfCiudad(boolean ofCiudad) {
        this.ofCiudad = ofCiudad;
    }

    /**
     * @return the daoAP
     */
    public APDao getDaoAP() {
        return daoAP;
    }

    /**
     * @return the listaUno
     */
    public List<WizardHelper> getListaUno() {
        return listaUno;
    }

    /**
     * @param listaUno the listaUno to set
     */
    public void setListaUno(List<WizardHelper> listaUno) {
        this.listaUno = listaUno;
    }

    /**
     * @return the ofCiProm
     */
    public boolean isOfCiProm() {
        return ofCiProm;
    }

    /**
     * @param ofCiProm the ofCiProm to set
     */
    public void setOfCiProm(boolean ofCiProm) {
        this.ofCiProm = ofCiProm;
    }

    /**
     * @return the cveTipTram
     */
    public String getCveTipTram() {
        return cveTipTram;
    }

    /**
     * @param cveTipTram the cveTipTram to set
     */
    public void setCveTipTram(String cveTipTram) {
        this.cveTipTram = cveTipTram;
    }

    /**
     * @return the idTipTram
     */
    public Integer getIdTipTram() {
        return idTipTram;
    }

    /**
     * @param idTipTram the idTipTram to set
     */
    public void setIdTipTram(Integer idTipTram) {
        this.idTipTram = idTipTram;
    }

    /**
     * @return the edoGestTram
     */
    public String getEdoGestTram() {
        return edoGestTram;
    }

    /**
     * @param edoGestTram the edoGestTram to set
     */
    public void setEdoGestTram(String edoGestTram) {
        this.edoGestTram = edoGestTram;
    }

    /**
     * @return the deInfoAdic
     */
    public Boolean getDeInfoAdic() {
        return deInfoAdic;
    }

    /**
     * @param deInfoAdic the deInfoAdic to set
     */
    public void setDeInfoAdic(Boolean deInfoAdic) {
        this.deInfoAdic = deInfoAdic;
    }

    public List<CatEstatusDocumento> getSubSectorCat() {
        return subSectorCat;
    }

    public void setSubSectorCat(List<CatEstatusDocumento> subSectorCat) {
        this.subSectorCat = subSectorCat;
    }

    public boolean isTieneCiudPrinc() {
        return tieneCiudPrinc;
    }

    public void setTieneCiudPrinc(boolean tieneCiudPrinc) {
        this.tieneCiudPrinc = tieneCiudPrinc;
    }

    public boolean isConModificacion() {
        return conModificacion;
    }

    public void setConModificacion(boolean conModificacion) {
        this.conModificacion = conModificacion;
    }

    public Short getEstatusDoctoTurnado() {
        return estatusDoctoTurnado;
    }

    public void setEstatusDoctoTurnado(Short estatusDoctoTurnado) {
        this.estatusDoctoTurnado = estatusDoctoTurnado;
    }

    public void setEstatusDoctoTurnado() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");

        this.estatusDoctoTurnado = Short.parseShort(strIdEstadoDocto);
    }

    /**
     * @return the comentarioTurnado
     */
    public String getComentarioTurnado() {
        return comentarioTurnado;
    }

    /**
     * @param comentarioTurnado the comentarioTurnado to set
     */
    public void setComentarioTurnado(String comentarioTurnado) {
    	
        this.comentarioTurnado = comentarioTurnado;
        // this.doctoDelegaciones.setObservacion(comentarioTurnado);
        System.out.println("setComentarioTurnado.." + this.comentarioTurnado);
    }
    
    /**
     * @param comentarioTurnado the comentarioTurnado to set
     */
    public void setComentarioTurnado() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        String strComentarios = ctxt.getExternalContext().getRequestParameterMap().get("comentTurnado");

        this.comentarioTurnado = strComentarios;
        System.out.println("setComentarioTurnado..." + this.comentarioTurnado);
    }

    /**
     * @return the doctoPromovente
     */
    public WizardHelper getDoctoPromovente() {
        return doctoPromovente;
    }

    /**
     * @return the doctoDelegaciones
     */
    public WizardHelper getDoctoDelegaciones() {
        return doctoDelegaciones;
    }

    public List<Object[]> getComentariosTab() {
        return comentariosTab;
    }
    
    public class WizardHelper implements Serializable {

        private String bitacora; 
        private String clveProy;
        private String oficio;
        private String remitente;
        private String dependencia;
        private String selec;
        private Boolean boton;
        private boolean lastSelected;
        private String estatusDocto;
        private String ligaDocto;
        private boolean editable; //Indica si el documento se puede editar, de acuerdo al perfil del usuario y estatus del docto
        private String observacion;
        

        public WizardHelper(String bitacora, String clveProy, String oficio, String remitente, String dependencia, String seleccion, Boolean boton) { //, Integer valorEval
            this.bitacora = bitacora;
            this.clveProy = clveProy;
            this.oficio = oficio;
            this.remitente = remitente;
            this.dependencia = dependencia;
            this.selec = seleccion;
            this.boton = boton;
            
            this.lastSelected = false;
        }

        public WizardHelper(String bitacora, String clveProy, String oficio, String remitente, String dependencia, 
                String seleccion, Boolean boton, String ligaDocto, Boolean editable, String observacion) { //, Integer valorEval
        	
            this.bitacora = bitacora;
            this.clveProy = clveProy;
            this.oficio = oficio;
            this.remitente = remitente;
            this.dependencia = dependencia;
            this.selec = seleccion;
            this.boton = boton;
            
            this.lastSelected = false;
            this.ligaDocto = ligaDocto;
            this.editable = editable;
            this.observacion = observacion;
        }

        /**
         * @return the bitacora
         */
        public String getBitacora() {
            return bitacora;
        }

        /**
         * @param bitacora the bitacora to set
         */
        public void setBitacora(String bitacora) {
            this.bitacora = bitacora;
        }

        /**
         * @return the clveProy
         */
        public String getClveProy() {
            return clveProy;
        }

        /**
         * @param clveProy the clveProy to set
         */
        public void setClveProy(String clveProy) {
            this.clveProy = clveProy;
        }

        /**
         * @return the oficio
         */
        public String getOficio() {
            return oficio;
        }

        /**
         * @param oficio the oficio to set
         */
        public void setOficio(String oficio) {
            this.oficio = oficio;
        }

        /**
         * @return the remitente
         */
        public String getRemitente() {
            return remitente;
        }

        /**
         * @param remitente the remitente to set
         */
        public void setRemitente(String remitente) {
            this.remitente = remitente;
        }

        /**
         * @return the dependencia
         */
        public String getDependencia() {
            return dependencia;
        }

        /**
         * @param dependencia the dependencia to set
         */
        public void setDependencia(String dependencia) {
            this.dependencia = dependencia;
        }

        /**
         * @return the selec
         */
        public String getSelec() {
            return selec;
        }

        /**
         * @param selec the selec to set
         */
        public void setSelec(String selec) {
            this.selec = selec;
        }

        /**
         * @return the boton
         */
        public Boolean getBoton() {
            return boton;
        }

        /**
         * @param boton the boton to set
         */
        public void setBoton(Boolean boton) {
            this.boton = boton;
        }
        
        public boolean isLastSelected() {
            boolean tmpSel = this.lastSelected;
            this.lastSelected = false;
            return tmpSel;
        }
    
        
        public void onValueChangeCmb() {
            this.lastSelected = true;
            daoDg.actualizaConsultaPublica(bitaProyecto, getOficio(), getSelec());
            //System.out.println("listener onChangeCmb");
        }    
        
//        public void valueChangeCmbInner(ValueChangeEvent e) {
//            System.out.println("listener valueChangeCmb");
//        }

        public String getEstatusDocto() {
            return estatusDocto;
        }
        
        public void setEstatusDocto() {
            FacesContext ctxt = FacesContext.getCurrentInstance();
            String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");
            
            this.estatusDocto = strIdEstadoDocto;
        }

        public void setEstatusDocto(String estatusDocto) {
            this.estatusDocto = estatusDocto;
        }

        public String getLigaDocto() {
            return ligaDocto;
        }

        public void setLigaDocto(String ligaDocto) {
            this.ligaDocto = ligaDocto;
        }

        public boolean isEditable() {
            return editable;
        }

        public void setEditable(boolean editable) {
            this.editable = editable;
        }

        /**
         * @return the observacion
         */
        public String getObservacion() {
            return observacion;
        }

        /**
         * @param observacion the observacion to set
         */
        public void setObservacion(String observacion) {
            this.observacion = observacion;
        }
        public void setObservacion() {
            FacesContext ctxt = FacesContext.getCurrentInstance();
            String observacion = ctxt.getExternalContext().getRequestParameterMap().get("observacion");

            this.observacion = observacion;
        }
    }
    
    @SuppressWarnings("unused")
	public void guardaCP()
    {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        Integer banderaLLeno=0;
        ConsultaPublicaProyecto cvp;
        List<Object[]> c;
        String id;        
        Integer ok=0;
        Integer countSI = 0;
        Integer countSI2 = 0;
        
        
        //guardado de respuestas de manera normal, despues de haberlas asignados a respuesta NO
        for (WizardHelper helper : listaUno) 
        {
            id = helper.oficio;
            cvp = (ConsultaPublicaProyecto) daoAP.cpConsulta(bitaProyecto2,id);
            if(cvp.getConsultaPublicaProyectoPK() != null)
            { banderaLLeno = 1; } //editar
            else
            { banderaLLeno = 0; } //nuevo
            
            if(banderaLLeno == 0)
            { 
                cvp = new ConsultaPublicaProyecto(bitaProyecto2, id);
            }
            
            if(helper.selec.equals("S") || helper.selec.equals("N"))
            { 
                if(helper.selec.equals("S"))
                { countSI2 = countSI2 + 1; }
                if(countSI2 == 1 && helper.selec.equals("S")) //solo el primer SI se guarda y las siguientes son NO
                { cvp.setConsultaPublicaRespuesta(helper.selec);  ofCiProm = true; } 
                else
                {
                    cvp.setConsultaPublicaRespuesta("N");
                    ofCiProm = false;
                }
                
            }
            else
            { cvp.setConsultaPublicaRespuesta("N");  ofCiProm = false; }    
            
            try {
                if(countSI2<=1) // si solo existe un si se procede a guardar de lo contrario se enviara mensaje de "de que solo selecciones una vez el si y los demas a no"
                {
                    if(banderaLLeno == 0)
                    {  daoAP.agrega(cvp); }
                    if(banderaLLeno == 1)
                    {  daoAP.modifica(cvp);  }                   
                }
               
            } catch (Exception e) {
                ok = ok + 1;
            }
            
        }
        if(countSI2==1) // si solo existe un si se procede a guarda4r de lo contrario se enviara mensaje de "de que solo selecciones una vez el si y los demas a no"
        {
            if(ok==0)
            {
                ctxt.addMessage("growl", new FacesMessage("Datos de Consulta pública guardados exitosamente."));
                carga();
            }        
        }
        if(countSI2>1) //si escogio mas de 1 SI
        {ctxt.addMessage("growl", new FacesMessage("Debe seleccionar solo a un ciudadano, la información no fue guardada, intente nuevamente."));} 
        
        if(countSI2==0) //si no escogio ningun SI
        {
            ctxt.addMessage("growl", new FacesMessage("Debe seleccionar a un ciudadano para darle respuesta, la información no fue guardada, intente nuevamente."));
            carga();
        } 
    }
    
    private ArrayList<Object> verificaEstadoDocto(String idDocumento, short tipoOficio) {
        DocumentoRespuestaDgira doctoResp;
        DocumentoDgiraFlujo doctoFlujo;
        boolean isEditable;
        String perfilUsr;
        ArrayList<Object> aResp = null;
        String observacion = "";
        boolean isFirmado = false;
        //Turnado
        //Obtiene la liga de acceso al documento en caso de existir
        doctoResp = daoDg.getDocumentotoRespuestaDgira(bitaProyecto, 
                idDocumento, tipoOficio);

        doctoFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, 
                tipoOficio, edoGestTram, idDocumento);

        isEditable = false;
        String perfilEnSesion = sesion.getAttribute("rol").toString();
        String perfilSig;

        if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {


        	try{
            	perfilUsr = doctoFlujo.getRolId().getNombreCorto().trim();
            	}catch(NullPointerException exnp){
            		perfilUsr = dao.getPerfilUsuario(doctoFlujo.getIdUsuarioEnvio());
            }
        	if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() != GenericConstants.ESTATUS_DOCTO_FIRMADO ) {

            	perfilSig = AppSession.siguienteRol(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                isEditable = perfilSig.compareToIgnoreCase(perfilEnSesion) == 0;                
                observacion = doctoFlujo.getDocumentoDgiraObservacion() != null ? doctoFlujo.getDocumentoDgiraObservacion() : "";
                
            } else {
                isFirmado = true;
            }                       
        } else if (perfilEnSesion.compareToIgnoreCase("eva") == 0) {
            //Por defecto, cuando no tiene flujo comienza por el evaluador
            isEditable = true;
        }
        
        aResp = new ArrayList<>();
        aResp.add(doctoResp != null ? doctoResp.getDocumentoUrl() : "");//urlDocto
        aResp.add(isEditable);
        aResp.add(observacion);
        aResp.add(isFirmado);
        
        return aResp;
    }    
    
    
    public void cambio()
    {
        //WizardHelper.
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void carga()
    {
    	
    	System.out.println("\n\ncarga..");
            DocumentoRespuestaDgira doctoResp;
            DocumentoDgiraFlujo doctoFlujo;
            String perfilUsr, observacion = "";
            
            String idProm=null;
            listaUno = new ArrayList();
            if(bitaProyecto != null)
            {
                //JOptionPane.showMessageDialog(null,  "  bitaProyecto: " + bitaProyecto , "Error", JOptionPane.INFORMATION_MESSAGE);
                tieneCiudPrinc = daoDg.tieneCiudadanoPrincipal(bitaProyecto);

                List<ConsultaPublicaProyecto> listConsultaP = (List<ConsultaPublicaProyecto>) daoAP.lista_namedQuery("ConsultaPublicaProyecto.findByEvaBitacoraProyecto", new Object[]{bitaProyecto2}, new String[]{"evaBitacoraProyecto"});
                cveProyecto = MenuDao.cveTramite(bitaProyecto);
                if(cveProyecto != null)
                {  //docsDonsPub = dao.consultaPublica(bitaProyecto2);
//                	System.out.println("ID_ENTIDAD: "+idEntidad);
                   //Se pasa el parametro idEntidad para realizar consultas deacuerdo a la entidad del usuario en Sesion
                   docsDonsPub = dao.consultaPublica(bitaProyecto2, idEntidad); }  //documentos y oficios     
                //obtienen nombre de promovente
                datosBitacora = dao.detalleProyCap(bitaProyecto2);
                if(datosBitacora.size()>0)
                {   
                   for (Object o : datosBitacora) {               
                        idProm = o.toString();
                    }
                    if(idProm != null)
                    {
                        promoTram = dao.nombrePromovente(idProm);
                        if(promoTram.size()>0)
                        {
                            for (Object o : promoTram) {               
                                nombProm = o.toString();
                            }
                        }
                    }
                    else
                    { nombProm = "--------"; }                

                }
                else
                { nombProm = "--------"; }

                //creación d lista de oficios
                String oficio; 
                String remitente; 
                String dependencia; 
                String seleccion;
                Boolean boton = false;
                List<Object[]> d;
                String ligaDoctoDG = "";
                boolean editable = true;
                
                // ------------------------------------PROMOVENTE-----------------------------------------------------------
                
                //Documentos de promovente y delegaciones
                ArrayList<Object> aResp;
                String oficioPromovente = "0";
                aResp = verificaEstadoDocto( oficioPromovente, GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE);
                                                
                try { 
                    File oficioDoc = new File((String)aResp.get(0)); 
                    System.out.println(oficioDoc.exists()); 
                    if (!oficioDoc.exists()) { 
                        ligaDoctoDG = "../../../ModuloEvaluacion/preview?tipo=oficio&bitaProyecto=" + bitaProyecto  
                                +"&idDocumento=" + oficioPromovente 
                                + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE; 
                    }else{
                        ligaDoctoDG = (String)aResp.get(0);
                    }
                } catch (Exception e) { 
                    System.out.println("No se a cerrado el borrador de oficio"); 
                } 
                editable = (Boolean)aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir       
                
                doctoFlujo = daoDg.docsFlujoDgira(bitaProyecto2, cveTipTram, idTipTram, 
                		GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE, edoGestTram, oficioPromovente);
                
                if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
                    
                    if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
                    	
                    	ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto2+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE+"&entidadfederativa="+edoGestTram+"&documentoid="+oficioPromovente;
                    	editable = false; // para ocultar los botones de firmar, turnar y corregir
                    	
                    	// si es la segunda bandeja de un ofirio ya firmado, se calcula su url download....
//                    	if(isUrlDG == 1 && bandeja == 1){ // bandeja 1
                        	
//                            List<DocumentoRespuestaDgira> lista;
//                            lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
//                            for (DocumentoRespuestaDgira x : lista) {
//                                try{
//                                	// esta liga se la pone al boton "Consultar"
//                                	ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto2+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE+"&entidadfederativa="+edoGestTram+"&documentoid="+oficioPromovente;
//                                	editable = false; // para ocultar los botones de firmar, turnar y corregir
//                                }catch(Exception exx){                                        	
//                                }
//                            }
//                        }
                    }else{
                    	if(isUrlDG == 1 && bandeja == 2){ // bandeja 1
                    		editable = false; // para ocultar los botones de firmar, turnar y corregir
                    	}
                    	
                    }
                }
                
                System.out.println("\nOficio Promovente - ligaDocto: " + ligaDoctoDG );
                this.doctoPromovente = new WizardHelper(bitaProyecto2, cveProyecto, oficioPromovente, null, null, null, null,
                        ligaDoctoDG, editable, (String)aResp.get(2));
                
                
                
                // -----------------------------DELEGACIONES----------------------------------------------------                
                
                ligaDoctoDG = "";
                editable = true;
                doctoFlujo = null;
                String oficioDelegaciones = "0";
                aResp = verificaEstadoDocto( oficioDelegaciones, GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION);
                
                try { 
                    File oficioDoc = new File((String)aResp.get(0)); 
                    System.out.println(oficioDoc.exists()); 
                    if (!oficioDoc.exists()) { 
                        ligaDoctoDG = "../../../ModuloEvaluacion/preview?tipo=oficio&bitaProyecto=" + bitaProyecto  
                                +"&idDocumento=" + oficioPromovente 
                                + "&tipoOficio=" + GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION; 
                    }else{
                        ligaDoctoDG = (String)aResp.get(0);
                    }
                } catch (Exception e) { 
                    System.out.println("No se a cerrado el borrador de oficio"); 
                }
                
                editable = (Boolean)aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir   
                
                doctoFlujo = daoDg.docsFlujoDgira(bitaProyecto2, cveTipTram, idTipTram, 
                		GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION, edoGestTram, oficioDelegaciones);
                
                if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
                    
                    if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
                    	
                    	ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto2+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE+"&entidadfederativa="+edoGestTram+"&documentoid="+oficioPromovente;
                    	editable = false; // para ocultar los botones de firmar, turnar y corregir                    	

                    }else{
                    	if(isUrlDG == 1 && bandeja == 2){ // bandeja 1
                    		editable = false; // para ocultar los botones de firmar, turnar y corregir
                    	}
                    	
                    }
                }
                
                System.out.println("\nOficio Delegación - ligaDocto: " + ligaDoctoDG );
                this.doctoDelegaciones = new WizardHelper(bitaProyecto2, cveProyecto, oficioDelegaciones, null, null, null, null,
                                ligaDoctoDG, editable, (String)aResp.get(2));

                
                
                
                // --------------------------------------CONSULTA PUBLICA CIUDADANO 11------------------------------------------------------
                ligaDoctoDG = "";                
                doctoFlujo = null;
                //Determina si ya existe un oficio como ciudadano principal y no permitir su captura
                if(docsDonsPub.size()>0)
                {
                    conModificacion = false;
                    for(int i2=0; i2< docsDonsPub.size(); i2++) 
                    {
                        editable = false;
                        oficio = ""; 
                        remitente = ""; 
                        dependencia = ""; 
                        seleccion = "N";
                        observacion = "";
                        System.out.println(docsDonsPub.get(i2)[0]);
                        
                        //Obtiene la liga de acceso al documento en caso de existir
//                        doctoResp = daoDg.getDocumentotoRespuestaDgira(bitaProyecto2, 
//                                docsDonsPub.get(i2)[4] != null ? docsDonsPub.get(i2)[4].toString(): "");
                        
                        doctoResp = daoDg.getDocumentotoRespuestaDgira(bitaProyecto, String.valueOf(i2), GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL);
                        
                        //Se determina si el perfil puede ver el botÃ³n de generar oficio (isEditable)
                        //String idDocumento = docsDonsPub.get(i2)[4] != null ? docsDonsPub.get(i2)[4].toString(): "";
                        String idDocumento = String.valueOf(i2);
                        
                        short tipoOficio = doctoResp != null ? doctoResp.getDocumentoRespuestaDgiraPK().getTipoDocId() : 
                                GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS;
                        
                        doctoFlujo = daoDg.docsFlujoDgira(bitaProyecto2, cveTipTram, idTipTram, 
                                tipoOficio, edoGestTram, idDocumento);
                        
                        if(doctoResp != null) {
                            File oficioDoc = new File(doctoResp.getDocumentoUrl());
                            System.out.println(oficioDoc.exists());
                            if (oficioDoc.exists()) {
                                ligaDoctoDG = doctoResp.getDocumentoUrl();
                            } else {
                                ligaDoctoDG = "/preview?tipo=oficio&bitaProyecto=" + bitaProyecto 
                                    +"&idDocumento=" + doctoResp.getDocumentoRespuestaDgiraPK().getDocumentoId()
                                    + "&tipoOficio=" + doctoResp.getDocumentoRespuestaDgiraPK().getTipoDocId();
                            }
                            editable = true;
                        }
                        String perfilEnSesion = sesion.getAttribute("rol").toString();
                        String perfilSig;
                        
                        
                        if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
                        
                        	conModificacion = true;//Existe al menos un registro con documento, por lo que ya no se puede modificar el resto
                            
                            try{
                            	perfilUsr = doctoFlujo.getRolId().getNombreCorto().trim();
                            	}catch(NullPointerException exnp){
                            		perfilUsr = dao.getPerfilUsuario(doctoFlujo.getIdUsuarioEnvio());
                            }
                            if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() != GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
                                
                            	perfilSig = AppSession.siguienteRol(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                                observacion = doctoFlujo.getDocumentoDgiraObservacion() != null ? doctoFlujo.getDocumentoDgiraObservacion() : "";
                                editable = perfilSig.compareToIgnoreCase(perfilEnSesion) == 0;
                            }  
                            
                            if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
                            	
                            	ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto2+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoOficio+"&entidadfederativa="+edoGestTram+"&documentoid="+idDocumento;
                            	editable = false; // para ocultar los botones de firmar, turnar y corregir                    	

                            }else{
                            	if(isUrlDG == 1 && bandeja == 2){ // bandeja 1
                            		editable = false; // para ocultar los botones de firmar, turnar y corregir
                            	}
                            	
                            }
                        }
                        
                        
                        System.out.println("idDocumento :" + idDocumento);
                        System.out.println("ligaDoctoDG :" + ligaDoctoDG);
                        System.out.println("editable :" + editable);

                        if (listConsultaP.isEmpty()) {
                            if(docsDonsPub.get(i2)[4] != null)
                            { 
                                oficio = docsDonsPub.get(i2)[4].toString();  
                            }
                            if(docsDonsPub.get(i2)[3] != null)
                            { remitente = docsDonsPub.get(i2)[3].toString(); }
                            else {remitente = "";}
                            if(docsDonsPub.get(i2)[2] != null)
                            { dependencia = docsDonsPub.get(i2)[2].toString(); }
                            else {dependencia = "";}
                            //String figSig = obtenerFiguraSiguiente(TIPO_OFICIO_CONSULTA_PUBLICA, oficio);
                            listaUno.add(new WizardHelper(bitaProyecto2, cveProyecto, oficio, remitente, dependencia, seleccion, boton,
                            		ligaDoctoDG, editable, observacion));

                        }
                        else
                        {
                            bandera = true;
                            seleccion = "";
                            if(docsDonsPub.get(i2)[4] != null)
                            {
                                oficio = docsDonsPub.get(i2)[4].toString();  
                                if(docsDonsPub.get(i2)[3] != null)
                                { remitente = docsDonsPub.get(i2)[3].toString(); }
                                else {remitente = "";}
                                if(docsDonsPub.get(i2)[2] != null)
                                { dependencia = docsDonsPub.get(i2)[2].toString(); }
                                else {dependencia = "";}

                                d = daoAP.respConsultPub(bitaProyecto2, oficio);
                                if(d.size()>0)
                                {                                 
                                    for (Object[] x : d) 
                                    { 
                                        if(x[2].toString() == null)
                                        { seleccion = "N"; ofCiProm = false; }  
                                        else
                                        { 
                                            seleccion = x[2].toString(); 
                                            if(seleccion.equals("S")) // si encuentra un solo si
                                            { ofCiudad = true;  boton = true;}
                                            else
                                            { ofCiProm = false;  boton = false;}
                                        }
                                    } 
                                    //listaUno.add(new WizardHelper(bitaProyecto, cveProyecto, docsDonsPub.get(i2)[4].toString(), docsDonsPub.get(i2)[2].toString(),docsDonsPub.get(i2)[3].toString(),"S"));                         

                                    
                                    listaUno.add(new WizardHelper(bitaProyecto2, cveProyecto, oficio, remitente, dependencia, seleccion, boton, 
                                    		ligaDoctoDG, editable, observacion));                         
                                }
                            }                        
                        }
                    }
                }
            }
    }
    
    
    
    // cargaYaFirmadoDG variables para refrescar los enlaces de oficios ligaDocto
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void cargaYaFirmadoDG()
    {
		
			System.out.println("\n\ncargaYaFirmadoDG..");
			System.out.println("tipoDocId : " + tipoDocId);
            DocumentoRespuestaDgira doctoResp;
            DocumentoDgiraFlujo doctoFlujo;
            boolean isEditable;
            String perfilUsr, observacion = "";
            
            String idProm=null;
            if(bitaProyecto != null)
            {
                tieneCiudPrinc = daoDg.tieneCiudadanoPrincipal(bitaProyecto);                
                
                List<ConsultaPublicaProyecto> listConsultaP = (List<ConsultaPublicaProyecto>) daoAP.lista_namedQuery("ConsultaPublicaProyecto.findByEvaBitacoraProyecto", new Object[]{bitaProyecto2}, new String[]{"evaBitacoraProyecto"});
                cveProyecto = MenuDao.cveTramite(bitaProyecto);
                if(cveProyecto != null)
                {  //docsDonsPub = dao.consultaPublica(bitaProyecto2);
                 docsDonsPub = dao.consultaPublica(bitaProyecto2, idEntidad); }  //documentos y oficios     
                //obtienen nombre de promovente
                datosBitacora = dao.detalleProyCap(bitaProyecto2);
                if(datosBitacora.size()>0)
                {   
                   for (Object o : datosBitacora) {               
                        idProm = o.toString();
                    }
                    if(idProm != null)
                    {
                        promoTram = dao.nombrePromovente(idProm);
                        if(promoTram.size()>0)
                        {
                            for (Object o : promoTram) {               
                                nombProm = o.toString();
                            }
                        }
                    }
                    else
                    { nombProm = "--------"; }                

                }
                else
                { nombProm = "--------"; }

                //creaciÃ³n d lista de oficios
                String oficio; 
                String remitente; 
                String dependencia; 
                String seleccion;
                Boolean boton = false;
                List<Object[]> d;
                String ligaDoctoDG = "";
                
                ArrayList<Object> aResp;
                
                if(tipoDocId.equals("PROMOVENTE")){
                
	                //Documentos de promovente y delegaciones                
	                String oficioPromovente = "PROMOVENTE";
	                aResp = verificaEstadoDocto( oficioPromovente, GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE);
	                ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto2+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE+"&entidadfederativa="+edoGestTram+"&documentoid="+oficioPromovente;
	                System.out.println("\nOficio Promovente - ligaDocto: " + ligaDoctoDG );
	                this.doctoPromovente = new WizardHelper(bitaProyecto2, cveProyecto, oficioPromovente, null, null, null, null,
	                                ligaDoctoDG, (Boolean)aResp.get(1), (String)aResp.get(2));
	                
                }else if(tipoDocId.equals("DELEGACIONES")){
                
                	ligaDoctoDG = "";
                	String oficioDelegaciones = "DELEGACIONES";
	                aResp = verificaEstadoDocto( oficioDelegaciones, GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION);
	                ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto2+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION+"&entidadfederativa="+edoGestTram+"&documentoid="+oficioDelegaciones;
	                System.out.println("Oficio DelegaciÃ³n - ligaDocto: " + ligaDoctoDG );
	                this.doctoDelegaciones = new WizardHelper(bitaProyecto2, cveProyecto, oficioDelegaciones, null, null, null, null,
	                                ligaDoctoDG, (Boolean)aResp.get(1), (String)aResp.get(2));
                
                }else{
                
                listaUno = new ArrayList(); // lista de los oficios de consulta a ciudadanos ( multiples )
                ligaDoctoDG = "";
                //Determina si ya existe un oficio como ciudadano principal y no permitir su captura
                if(docsDonsPub.size()>0)
                {
                    conModificacion = false;
                    for(int i2=0; i2<= docsDonsPub.size() - 1; i2++) 
                    {
                        oficio = ""; 
                        remitente = ""; 
                        dependencia = ""; 
                        seleccion = "N";
                        observacion = "";
                        System.out.println(docsDonsPub.get(i2)[0]);
                        
                        //Obtiene la liga de acceso al documento en caso de existir
                        doctoResp = daoDg.getDocumentotoRespuestaDgira(bitaProyecto2, 
                                docsDonsPub.get(i2)[4] != null ? docsDonsPub.get(i2)[4].toString(): "");
                        
                       	//Se determina si el perfil puede ver el botÃ³n de generar oficio (isEditable)
                        String idDocumento = docsDonsPub.get(i2)[4] != null ? docsDonsPub.get(i2)[4].toString(): "";
                        short tipoOficio = doctoResp != null ? doctoResp.getDocumentoRespuestaDgiraPK().getTipoDocId() : 
                                GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS;
                        
                        doctoFlujo = daoDg.docsFlujoDgira(bitaProyecto2, cveTipTram, idTipTram, 
                                tipoOficio, edoGestTram, idDocumento);
                        
                        if(doctoResp != null) ligaDoctoDG = doctoResp.getDocumentoUrl();

                        isEditable = false;
                        String perfilEnSesion = sesion.getAttribute("rol").toString();
                        String perfilSig;
                        
                        if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
                            conModificacion = true;//Existe al menos un registro con documento, por lo que ya no se puede modificar el resto
                            
                            
                            try{
                            	perfilUsr = doctoFlujo.getRolId().getNombreCorto().trim();
                            	}catch(NullPointerException exnp){
                            		perfilUsr = dao.getPerfilUsuario(doctoFlujo.getIdUsuarioEnvio());
                            }
                            if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() != GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
                                
                             	perfilSig = AppSession.siguienteRol(perfilUsr, doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                                observacion = doctoFlujo.getDocumentoDgiraObservacion() != null ? doctoFlujo.getDocumentoDgiraObservacion() : "";
                                isEditable = perfilSig.compareToIgnoreCase(perfilEnSesion) == 0;
                            }                        
                        } else if (perfilEnSesion.compareToIgnoreCase("eva") == 0) {
                            //Por defecto, cuando no tiene flujo comienza por el evaluador
                            isEditable = true;
                        }

                        // si el id del oficio de consulta publica solicitante es el id que estamos turnando entonces a ese oficio cambiarle la url
                        if(tipoDocId.equals(idDocumento))
                        	ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto2+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoOficio+"&entidadfederativa="+edoGestTram+"&documentoid="+idDocumento;
                        
                        System.out.println("Oficio Ciudadanos - ligaDocto: " + ligaDoctoDG );
                        
                        if (listConsultaP.isEmpty()) {
                            if(docsDonsPub.get(i2)[4] != null)
                            { oficio = docsDonsPub.get(i2)[4].toString();  }
                            if(docsDonsPub.get(i2)[3] != null)
                            { remitente = docsDonsPub.get(i2)[3].toString(); }
                            else {remitente = "";}
                            if(docsDonsPub.get(i2)[2] != null)
                            { dependencia = docsDonsPub.get(i2)[2].toString(); }
                            else {dependencia = "";}
                            //String figSig = obtenerFiguraSiguiente(TIPO_OFICIO_CONSULTA_PUBLICA, oficio);
                            listaUno.add(new WizardHelper(bitaProyecto2, cveProyecto, oficio, remitente, dependencia, seleccion, boton,
                                ligaDoctoDG, isEditable, observacion));

                        }
                        else
                        {
                            bandera = true;
                            seleccion = "";
                            if(docsDonsPub.get(i2)[4] != null)
                            {
                                oficio = docsDonsPub.get(i2)[4].toString();  
                                if(docsDonsPub.get(i2)[3] != null)
                                { remitente = docsDonsPub.get(i2)[3].toString(); }
                                else {remitente = "";}
                                if(docsDonsPub.get(i2)[2] != null)
                                { dependencia = docsDonsPub.get(i2)[2].toString(); }
                                else {dependencia = "";}

                                d = daoAP.respConsultPub(bitaProyecto2, oficio);
                                if(d.size()>0)
                                {                                 
                                    for (Object[] x : d) 
                                    { 
                                        if(x[2].toString() == null)
                                        { seleccion = "N"; ofCiProm = false; }  
                                        else
                                        { 
                                            seleccion = x[2].toString(); 
                                            if(seleccion.equals("S")) // si encuentra un solo si
                                            { ofCiudad = true;  boton = true;}
                                            else
                                            { ofCiProm = false;  boton = false;}
                                        }
                                    } 
                                    //listaUno.add(new WizardHelper(bitaProyecto, cveProyecto, docsDonsPub.get(i2)[4].toString(), docsDonsPub.get(i2)[2].toString(),docsDonsPub.get(i2)[3].toString(),"S"));                         

                                    
                                    listaUno.add(new WizardHelper(bitaProyecto2, cveProyecto, oficio, remitente, dependencia, seleccion, boton, 
                                            ligaDoctoDG, isEditable, observacion));                         
                                }
                            }                        
                        }
                    }
                }
              } // else
            }
    }
	
	
    
    public void suspPorInfoAdic() {
        if (bitaProyecto != null) {
            try {
                bitacora1 = (Bitacora) dao.busca(Bitacora.class, bitaProyecto);
                if (bitacora1 != null) 
                {
                    FacesMessage message=null;
                    if(bitacora1.getBitaSituacion().equals("CIS302"))
                    {
                        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "El trÃ¡mite se encuentra suspendido por informaciÃ³n adicional.", null);
                    }
                    else
                    {
                        sinatDgira = new SinatDgira();
                        sinatDgira.setId(0);
                        sinatDgira.setSituacion("CIS302");
                        sinatDgira.setIdEntidadFederativa(bitacora1.getBitaEntidadGestion());
                        sinatDgira.setIdClaveTramite(bitacora1.getBitaTipotram()); //STRING BITA_TIPOTRAM
                        sinatDgira.setIdTramite(bitacora1.getBitaIdTramite());
                        sinatDgira.setIdAreaEnvio(idArea.trim());
                        sinatDgira.setIdAreaRecibe(idArea.trim());
                        sinatDgira.setIdUsuarioEnvio(idusuario);
                        //sinatDgira.setIdUsuarioRecibe(1340);
                        sinatDgira.setGrupoTrabajo(new Short("0"));
                        sinatDgira.setBitacora(bitaProyecto);
                        sinatDgira.setFecha(new Date());
                        dao.agrega(sinatDgira);

                        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "TrÃ¡mite suspendido por informaciÃ³n adicional.", null);                    
                    }
                    FacesContext.getCurrentInstance().addMessage("messages", message);
                }
            } catch (Exception er) {
                er.printStackTrace();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido turnar el trÃ¡mite, intente mÃ¡s tarde.", null);
                FacesContext.getCurrentInstance().addMessage("messages", message);
            }
        }
    }
    
    
    @SuppressWarnings("unused")
	public void suspPorInfoAdic(short tipoOfic)
    {

        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        idAux = 0;
        maxIdHisto = 0;
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial remitente;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        Historial situacATOO22 = null;
        Historial situacI00010 = null;
        Integer resultado = 0;
        Integer resultVersion=-1;
        short tipoDoc = tipoOfic;
        Boolean exitoCambSituac = false;
        //tipoDoc = 4 Oficio de prevencion
        //tipoDoc = 7 Oficio de InformaciÃ³n adicional
        Integer soloTurnOfic=-1;
        SegundasViasView segundasViasView = new SegundasViasView("");
                
        FacesContext ctxt = FacesContext.getCurrentInstance();
        
            if(bitaProyecto != null)
            {
                
                    //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

                    maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    for (Object o : maxDocsDgiraFlujoHist) {
                        idAux =  Integer.parseInt(o.toString());
                        maxIdHisto = idAux.shortValue();
                    }

                    docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                    
                    if(docsDgiraFlujoHistEdit != null && maxIdHisto > 0)
                    {            
                        docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                        docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                        docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                        daoDg.modifica(docsDgiraFlujoHistEdit);
                    }


                    //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
                    docsDgiraFlujo = daoDg.docsFlujoDgira(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);

                    if (docsDgiraFlujo != null) {
                        if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                            banderaNew = 0;
                        }// registro nuevo
                        else {
                            banderaNew = 1;
                        }

                    } else {
                        banderaNew = 1;
                    }

                    if (banderaNew == 1) {
                        docsDgiraFlujo = new DocumentoDgiraFlujo(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    }

                    catEstatusDoc = daoDg.idCatEstatusDoc(idDocumento);
                    docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
                    docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
                    docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
                    docsDgiraFlujo.setIdAreaRecibe(null);
                    docsDgiraFlujo.setIdUsuarioRecibe(null);
                    docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
                    docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
                    docsDgiraFlujo.setDocumentoDgiraObservacion(comentarioTurnado);
                    
                 // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
                    UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
//            		System.out.println("\ninformaciÃ³n de usuario y area con el que entrÃ³ a la bitacora:");
//            		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
//            		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
//            		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
//            		System.out.println("rolId     :" + ur.getRolId().getIdRol());
            		
            		// Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
                    CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());
                    
                    // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
                   	docsDgiraFlujo.setRolId(rol);

                    try {
                        if (banderaNew == 1) {
                            daoDg.agrega(docsDgiraFlujo);
                        }
                        if (banderaNew == 0) {
                            daoDg.modifica(docsDgiraFlujo);
                        }

                        maxIdHisto = (short) (idAux + 1);
                        docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                        docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                        docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                        docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                        docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                        docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                        docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                        docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                        docsDgiraFlujoHist.setDocumentoDgiraObservHist(comentarioTurnado);
                        try {
                            daoDg.agrega(docsDgiraFlujoHist);
                        } catch (Exception ex) {
                            error = error + 1;
                        }
                    } catch (Exception e) {
                        error = error + 1;
                    }

                    if (error == 0) 
                    {
                        System.out.println("TerminÃ³ turnado de oficio" );

                        if(idDocumento == 4) //si el estatus del documento es 4=frimado
                        {
                            if(tipoDoc == 7)//si es oficio de InformaciÃ³n adicional
                            {
                                tipoSuspencion = 1;
                                tipoApertura=2;
                                System.out.println("Comienza turnado de Oficio de Info. Adic. idDocumento: " + idDocumento);
                                
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situaciÃ³n");
                                    //Situaciones para colocar tramite en situaciÃ³n de suspencion por InformaciÃ³n adicional                                    
                                    if(segundasViasView.cambioSituacion("AT0031","I00010", getBitaProyecto()) == 1)//eva-sd  
                                    {
                                        resultado = resultado + 1;
                                        if(segundasViasView.cambioSituacion("AT0032","AT0027", getBitaProyecto()) == 1)//sd-da
                                        {
//                                            resultado = resultado + 1;
//                                            if(cambioSituacion("AT0004","dadg") == 1) //da-dg 
//                                            {
                                                resultado = resultado + 1;
                                                if(segundasViasView.cambioSituacion("AT0008","dg", getBitaProyecto()) == 1)//dg-dg 
                                                {  
                                                    resultado = resultado + 1;
                                                    if(segundasViasView.cambioSituacion("ATIF01","dgecc", getBitaProyecto()) == 1)//dg-ecc 
                                                    {
                                                        resultado = resultado + 1;                                                                
                                                    }                                                    
                                                }
//                                            }
                                        }
                                    }
                                    if(resultado == 4)
                                    { 
                                        try
                                        {
                                            if(folio!=null)
                                            {
                                                
                                                //CREACION DE VERSION 3 DE TRAMITE, solo aplica para oficio de InformaciÃ³n adicional
                                                //si el resultVersion es 0= bien 1=mal
                                            	resultVersion = segundasViasView.procVersiones(folio, (short)3);

                                                ctxt.addMessage("growl", new FacesMessage("TrÃ¡mite turnado. TrÃ¡mite notificado a promovente."));
                                                if(resultVersion == 0)
                                                { 
                                                    exitoCambSituac = true; 
                                                    System.out.println("Termino ingreso de version 3 de la MIA");
                                                }
                                                else {
                                                    exitoCambSituac = false; 
                                                    System.out.println("No Termino ingreso de version 3 de la MIA");
                                                }
                                                
                                            }
                                            else
                                            {
                                                exitoCambSituac = true; 
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            System.out.println("Problema con inserciÃ³n de situaciones: " + ex.getMessage());
                                            ctxt.addMessage("growl", new FacesMessage("TrÃ¡mite no turnado, intentelo mas tarde."));
                                            exitoCambSituac = false; 
                                        }
                                        
                                        
                                    }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5)//si es oficio de prevencion (3=pago de derechos  4=requisitos cofemer)
                            {
                                tipoSuspencion = 2;
                                tipoApertura=1;
                                System.out.println("Comienza turnado de Oficio de prevenciÃ³n idDocumento: " + idDocumento);
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situaciÃ³n");
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("W")) // tramites electronicos
                                    {
                                        //es -- por que se turna asi mismo
                                        if(segundasViasView.cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)//if(cambioSituacion("AT0022","I00008") == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("-")) //tramites fisicos
                                    {
                                        //es -- por que se turna del SD al ECC
                                        if(segundasViasView.cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    
                                    
                                    if(resultado == 1)
                                    { exitoCambSituac = true; }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 1 || tipoDoc == 2 ||tipoDoc == 8 || tipoDoc == 9 || tipoDoc == 10 || tipoDoc == 11)// estos oficios no van a segundas vias ni afectan el flujo del PEIA
                            {
                                exitoCambSituac = true;
                            }
                            
                            soloTurnOfic = 0;
                        }
                        else // para cuando no se trata del firmado del documento
                        {
                            soloTurnOfic = 1;
                        }
                        
                        
                        if(exitoCambSituac)
                        {
                            if(folio!=null)
                            {
                                if(tipoDoc == 7 || tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5) //segundas vias solo aplica a oficio de info. adicional y de prevencion
                                {
                                    //comienza tramite de segundas vias  
                                    try { //1=iNFORMACION ADICIONAL 2=PREEVENCION
                                    	
                                    	SegundasVias segundasVias = new SegundasVias();
                                    	segundasVias.proc(bitaProyecto, tipoSuspencion,folio,tipoApertura,tipoDoc);
                                        ctxt.addMessage("growl", new FacesMessage("TrÃ¡mite turnado. TrÃ¡mite notificado a promovente."));
                                        System.out.println("Termino de segundas vÃ­as");                                

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, intÃ©ntelo mas tarde."));
                                        System.out.println("Problema con segundas vÃ­as: " + e.getMessage());
                                    }
                                }
                                else
                                {ctxt.addMessage("growl", new FacesMessage("oficio firmado exitosamente"));}
                            }
                            else
                            {
                                ctxt.addMessage("growl", new FacesMessage("TrÃ¡mite turnado exitosamente."));
                            }
                        }
                        else
                        {
                            if(soloTurnOfic==0) //si se trata de turnado de tramite en esatdo de firma de DG
                            {
                                System.out.println("Problema con cambio de situaciÃ³n de trÃ¡mite, no se corrio procedimeinto de segundas vÃ­as");
                                ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, intentelo mas tarde.TrÃ¡mite no notificado a promovente."));
                            }
                            else//si se trata de turnado de tramite en esatdo de NO firma de DG
                            {
                                System.out.println("Se completo turnado de oficio (No es situaciÃ³n de firmado de DG)");
                                ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));
                            }
                        }
                        //FacesContext.getCurrentInstance().addMessage("messages", message);
                        turnaDocsBtn = true; 
                    } else {
                        ctxt.addMessage("growl", new FacesMessage("El turnado del oficio fallÃ³, intente nuevamente."));
                    }
            }
    }
    
    
    
    public void onChangeCmb() {
        //System.out.println("listener onChangeCmb");
        for (WizardHelper ele : this.listaUno) {
            if (!ele.isLastSelected()) {
                ele.selec = "N";
            }
        }
    }    
    
    public void muestraObservaciones() {
        String idDocumento, strTipoDocto;
        short sTipoDocto;
        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();
        
        try {
            idDocumento = ctxt.getExternalContext().getRequestParameterMap().get("idDocumento");
            strTipoDocto = ctxt.getExternalContext().getRequestParameterMap().get("tipoDocto");
            sTipoDocto =  strTipoDocto.compareTo("S") == 0 ? GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL : GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS;
            String idLst = sTipoDocto + "_" + idDocumento;
            
            //comentariosTab = dao.obtenerObservacionesOficios(bitaProyecto, sTipoDocto, idDocumento);
            comentariosTab = mapComentarios.get(idLst);
            
        } catch (Exception e) {
            reqContEnv.execute("alert('OcurriÃ³ un error durante la obtenciÃ³n de las observaciones, intente nuevamente')");
        }
    }
    
    
    /***
     * Turna el documento de consulta publica cuando NO es el DG quien estÃ¡ turnando
     * @param valcomentarios boleano que indica que ponga o no comentarios ( si es true los pone siempre que se hayan capturado... )
     */
    @SuppressWarnings("unused")
	public void turnaDoctoConsultaPublica_(boolean valcomentarios) {
    	
    	System.out.println("\nturnaDoctoConsultaPublica_...");
    	
        String idDocumento = "", tipoDoctoTurnado;
        short idEstadoDocto = 0, sTipoDoc = 0;
        
        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();
        
        try {
            tipoDoctoTurnado = ctxt.getExternalContext().getRequestParameterMap().get("tipoDoctoTurnado");
            idDocumento = ctxt.getExternalContext().getRequestParameterMap().get("idDocumento");
            

            if ( tipoDoctoTurnado != null ) {
                sTipoDoc = Short.parseShort(tipoDoctoTurnado);
                if (sTipoDoc == GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE) {
                    
                        //estatusDoctoTurnado = Short.parseShort(this.doctoPromovente.getEstatusDocto());  
                        if(!sesion.getAttribute("rol").toString().equals("eva"))
                        { 
                        	comentarioTurnado = this.doctoPromovente.getObservacion();
                        }
                        if ( estatusDoctoTurnado == 0 ) {
                            reqContEnv.execute("alert('Debe indicar el estado del documento para su turnado')");
                        } else {
                            idEstadoDocto = estatusDoctoTurnado;
                        }
                    
                } else { //sTipoDoc == GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION
                    estatusDoctoTurnado = Short.parseShort(this.doctoDelegaciones.getEstatusDocto());
                    if(!sesion.getAttribute("rol").toString().equals("eva"))
                    { 
                    	comentarioTurnado = this.doctoDelegaciones.getObservacion(); 
                    }  
                    if ( estatusDoctoTurnado == 0 ) {
                        reqContEnv.execute("alert('Debe indicar el estado del documento para su turnado')");
                    } else {
                        idEstadoDocto = estatusDoctoTurnado;
                    }
                }
            } else {
                //String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");
                if ( estatusDoctoTurnado == 0 ) {
                    reqContEnv.execute("alert('Debe indicar el estado del documento para su turnado')");
                } else {
                    idEstadoDocto = estatusDoctoTurnado;
                }

                String ciudadanoPrincipal = ctxt.getExternalContext().getRequestParameterMap().get("ciudadanoPrincipal");
                sTipoDoc = (ciudadanoPrincipal.compareToIgnoreCase("S") != 0) ? GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL :
                        GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS;
            }

            int banderaNew = 0;
            int error = 0;
            Integer valInt = 0;
//            Integer idAux = 0;
//            short maxIdHisto = 0;
            Bitacora bitacora2 = (Bitacora) dao.busca(Bitacora.class, bitaProyecto);
            Historial subdirector;
            VisorDao daoV = new VisorDao();
            Proyecto proy = null;
            Historial situacATOO22 = null;
            Historial situacI00010 = null;
            String folio = null;

            List<Object[]> maxDocsDgiraFlujoHist;
            DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
            DocumentoDgiraFlujo docsDgiraFlujo;
            CatEstatusDocumento catEstatusDoc;
            DocumentoDgiraFlujoHist docsDgiraFlujoHist;
            String observacionesDocDgira = valcomentarios  ? this.comentarioTurnado : "";
            System.out.println("comentarios :" + this.comentarioTurnado);

            if (bitaProyecto != null) {
                try {
                    proy = daoV.verificaERA2(bitaProyecto);
                    if (proy != null) {
                        if (proy.getProyectoPK() != null) {
                            ctxt.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
                            ctxt.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
                            folio = proy.getProyectoPK().getFolioProyecto();
                        }
                    }
                } catch (Exception ar) {
                    System.out.println("Problema: " + ar.getMessage());
                }

            }

            if (bitaProyecto != null && folio != null && idEstadoDocto != 0 && !idDocumento.isEmpty()) {
                situacATOO22 = dao.datosProyHistStatus(bitaProyecto, "AT0022");
                situacI00010 = dao.datosProyHistStatus(bitaProyecto, "I00010");
                situacATOO22 = null;
                situacI00010 = null;
                if (situacATOO22 != null || situacI00010 != null) {
                    ctxt.addMessage("growl", new FacesMessage("Oficio de prevenciÃ³n no puede ser turnado, el trÃ¡mite ya se encuentra prevenido o ha sido turnado al evaluador."));
                } else {
                    //-------------------actualizaciÃ³n en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

                    maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, idDocumento);
                    for (Object o : maxDocsDgiraFlujoHist) {
                        idAux = Integer.parseInt(o.toString());
                        maxIdHisto = idAux.shortValue();
                    }

                    docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, idDocumento, maxIdHisto);

                    if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                        docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                        docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                        docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                        daoDg.modifica(docsDgiraFlujoHistEdit);
                    }

                    //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
                    docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, idDocumento);

                    if (docsDgiraFlujo != null) {
                        if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                            banderaNew = 0;
                        }// registro nuevo
                        else {
                            banderaNew = 1;
                        }

                    } else {
                        banderaNew = 1;
                    }

                    if (banderaNew == 1) {
                        docsDgiraFlujo = new DocumentoDgiraFlujo(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, idDocumento);
                    }

                    catEstatusDoc = daoDg.idCatEstatusDoc(idEstadoDocto);
                    docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
                    docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
                    docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
                    docsDgiraFlujo.setIdAreaRecibe(null);
                    docsDgiraFlujo.setIdUsuarioRecibe(null);
                    docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
                    docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
                    docsDgiraFlujo.setDocumentoDgiraObservacion(observacionesDocDgira);

                 // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
                    UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
            		System.out.println("\ninformaciÃ³n de usuario y area con el que entrÃ³ a la bitacora:");
            		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
            		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
            		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
            		System.out.println("rolId     :" + ur.getRolId().getIdRol());
            		
            		// Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
                    CatRol rol = dao.getRolPorId(ur.getRolId().getIdRol());
                    
                    // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
                   	docsDgiraFlujo.setRolId(rol);

                    try {
                        if (banderaNew == 1) {
                            daoDg.agrega(docsDgiraFlujo);
                        }
                        if (banderaNew == 0) {
                            daoDg.modifica(docsDgiraFlujo);
                        }

                        maxIdHisto = (short) (idAux + 1);
                        docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, idDocumento, maxIdHisto);
                        docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                        docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                        docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                        docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                        docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                        docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                        docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                        docsDgiraFlujoHist.setDocumentoDgiraObservHist(observacionesDocDgira);
                        try {
                            daoDg.agrega(docsDgiraFlujoHist);
                        } catch (Exception ex) {
                            error = error + 1;
                        }
                    } catch (Exception e) {
                        error = error + 1;
                    }

                    if (error == 0) {
                        System.out.println("TerminÃ³ turnado de oficio");

                        //Comienza procedimiento de segundas vÃ­as en otros casos, aquÃ­ no aplica
                        ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));
                        carga();	
                        /*
                        //----------------------verificar si el boton para turnar los  oficios, el que se encinetra dentro del apartado Turnado de Oficio    
                        idAux = 0;
                        maxIdHisto = 0;
                        //se verifica el id mÃ¡ximo de oficio a tratar (en este caso el tipo de prevenciÃ³n=4)
                        try {
                            maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");

                            for (Object o : maxDocsDgiraFlujoHist) {
                                idAux = Integer.parseInt(o.toString());
                                maxIdHisto = idAux.shortValue();
                            }
                        } catch (Exception xxx) {
                            //JOptionPane.showMessageDialog(null, "idDocumento:  " + xxx.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }
                        if (maxIdHisto > 0) {   //trae todo lo relaionado al id mÃ¡ximo de tabla DOCUMENTO_DGIRA_FLUJO_HIST, relativo a una bitacora
                            docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);
                            statusProy = daoBitacora.estatusTramBita(bitaProyecto);
                            try {
                                //estatus del oficio en cuestiÃ³n 
                                statusDocRespDgira = daoDg.statusDocsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);
                            } catch (Exception er) {
                                //JOptionPane.showMessageDialog(null, "idDocumento:  " + er.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                            }

                            for (Object o : statusDocRespDgira) {
                                statusDoc = Integer.parseInt(o.toString());
                            }
                        }


                        HistorialAT0001 = daoBitacora.numRegHistStatus(bitaProyecto,"AT0001");
                        HistorialCIS303 = daoBitacora.numRegHistStatus(bitaProyecto,"CIS303");
                        if (HistorialAT0001.size() == 2 || HistorialCIS303.size() == 1) //si el tamaÃ±o es igual a dos, indica que el trÃ¡mite ya esta activo despues de haber sido prevenido
                        { dePrevenc = true; }
                        else { dePrevenc = false; }

                        if(dePrevenc) //si ya viene de una reactivaciÃ³n por prevenciÃ³n, ya no puede guardar check list y no puede generar oficio de prevenciÃ³n
                        { 
                            turnaDocsBtn = true; //ocultar bootn de turnado
                            URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                        }
                        else
                        {
                            //JOptionPane.showMessageDialog(null,  "area envio: " + docsDgiraFlujoHistEdit.getIdAreaEnvioHist() + "   maxIdHisto: " + maxIdHisto , "Error", JOptionPane.INFORMATION_MESSAGE);
                            if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                                try {
                                    areaResiveDocCon = daoBitacora.jerarquiaTbArea(docsDgiraFlujoHistEdit.getIdAreaEnvioHist());
                                    //JOptionPane.showMessageDialog(null, "idDocumento:  " +areaResiveDocCon.size(), "Error", JOptionPane.INFORMATION_MESSAGE);
                                } catch (Exception er2) {
                                    //JOptionPane.showMessageDialog(null, "err:  " + er2.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                                }
                                for (Object o : areaResiveDocCon) {
                                    jerarquia = o.toString();
                                }
                                if (jerarquia.equals("DG")) {
                                    //statusDoc
                                    if (statusDoc == 3) {
                                        perfilSig = "dg";
                                    }
                                    if (statusDoc == 2) {
                                        perfilSig = "da";
                                    }
                                }
                                if (jerarquia.equals("DA")) {
                                    //statusDoc
                                    if (statusDoc == 3) {
                                        perfilSig = "dg";
                                    }
                                    if (statusDoc == 2) {
                                        perfilSig = "sd";
                                    }
                                }
                                if (jerarquia.equals("SD")) {
                                    //statusDoc
                                    if (statusDoc == 3) {
                                        perfilSig = "da";
                                    }
                                    if (statusDoc == 2) {
                                        perfilSig = "eva";
                                    }
                                }
                                if (jerarquia.equals("JD")) {
                                    //statusDoc
                                    if (statusDoc == 3) {
                                        perfilSig = "sd";
                                    }
                                }
                                //si el perfil en session es igual al perfil al que se le envio el oficio, perimite la ediciÃ³n del oficio
                                if (sesion.getAttribute("rol").toString().equals(perfilSig)) {
                                    turnaDocsBtn = false;//visualizar boton de turnado
                                    URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + ",4," + bitaProyecto + ",1"; //solo visualiza PDF y no lo modifica
                                } else {
                                    turnaDocsBtn = true; //ocultar bootn de turnado
                                    URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                                    turnaDocs = true;
                                }
                            } else {
                                turnaDocsBtn = false;//visualizar boton de turnado
                                URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + ",4," + bitaProyecto + ",1"; //solo visualiza PDF y no lo modifica  bitaProyOfic
                            }
                        }*/
                        //------------------------------------------------------------
                    } else {
                        //ctxt.addMessage("growl", new FacesMessage("El turnado del oficio de fallÃ³, intente nuevamente."));
                        reqContEnv.execute("alert('El turnado del oficio de fallÃ³, intente nuevamente')");
                    }
                }

                ArrayList<Object> aResp;
                //Documentos de promovente y delegaciones
                if (sTipoDoc == GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE) {
                    String oficioPromovente = "PROMOVENTE";
                    aResp = verificaEstadoDocto( oficioPromovente, GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE);
                    this.doctoPromovente = new WizardHelper(bitaProyecto2, cveProyecto, oficioPromovente, null, null, null, null,
                                    (String)aResp.get(0), (Boolean)aResp.get(1), (String)aResp.get(2));
                } else if (sTipoDoc == GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION) {
                    String oficioDelegaciones = "DELEGACIONES";
                    aResp = verificaEstadoDocto( oficioDelegaciones, GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION);
                    this.doctoDelegaciones = new WizardHelper(bitaProyecto2, cveProyecto, oficioDelegaciones, null, null, null, null,
                                    (String)aResp.get(0), (Boolean)aResp.get(1), (String)aResp.get(2));            
                }

                this.estatusDoctoTurnado = 0;
                this.comentarioTurnado = "";
                reqContEnv.execute("alert('Oficio turnado exitosamente')");            
            }

            
        } catch (Exception e) {        	
            System.out.println("error de turnado de oficios de consulta pÃºblica: " + e.getMessage());
            System.out.println("error de turnado de oficios de consulta pÃºblica: " + e.getLocalizedMessage());
            reqContEnv.execute("alert('OcurriÃ³ un error durante el turnado, intente nuevamente')");
        }
        
    }
    
    /***
     * Turna el documento de consulta publica cuando NO es el DG quien estÃ¡ turnando
     * @param valcomentarios boleano que indica que ponga o no comentarios ( si es true los pone siempre que se hayan capturado... )
     */
	@SuppressWarnings("unused")
	public void turnaDoctoConsultaPublicaDG_() {
    	
		System.out.println("\n\nturnaDoctoConsultaPublicaDG_..");
    	
		String tipoDoctoTurnado;
        
        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();
        
            tipoDoctoTurnado = ctxt.getExternalContext().getRequestParameterMap().get("tipoDoctoTurnado");
            tipoDocId = ctxt.getExternalContext().getRequestParameterMap().get("idDocumento");
            

            if ( tipoDoctoTurnado != null ) {
                tipoDoc = Short.parseShort(tipoDoctoTurnado);
                if (tipoDoc == GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE) {
                    
                        //estatusDoctoTurnado = Short.parseShort(this.doctoPromovente.getEstatusDocto());  
                        if(!sesion.getAttribute("rol").toString().equals("eva"))
                        { 
                        	comentarioTurnado = this.doctoPromovente.getObservacion();
                        }
                        if ( estatusDoctoTurnado == 0 ) {
//                            reqContEnv.execute("alert('Debe indicar el estado del documento para su turnado')");
                        } else {
//                            idEstadoDocto = estatusDoctoTurnado;
                        }
                    
                } else { //sTipoDoc == GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION
                    estatusDoctoTurnado = Short.parseShort(this.doctoDelegaciones.getEstatusDocto());
                    if(!sesion.getAttribute("rol").toString().equals("eva"))
                    { 
                    	comentarioTurnado = this.doctoDelegaciones.getObservacion(); 
                    }  
                    if ( estatusDoctoTurnado == 0 ) {
//                        reqContEnv.execute("alert('Debe indicar el estado del documento para su turnado')");
                    } else {
//                        idEstadoDocto = estatusDoctoTurnado;
                    }
                }
            } else {
                //String strIdEstadoDocto = ctxt.getExternalContext().getRequestParameterMap().get("idEstadoDocto");
                if ( estatusDoctoTurnado == 0 ) {
//                    reqContEnv.execute("alert('Debe indicar el estado del documento para su turnado')");
                } else {
//                    idEstadoDocto = estatusDoctoTurnado;
                }

                String ciudadanoPrincipal = ctxt.getExternalContext().getRequestParameterMap().get("ciudadanoPrincipal");
                tipoDoc = (ciudadanoPrincipal.compareToIgnoreCase("S") == 0) ? GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL :
                        GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS;
            }

            Bitacora bitacora2 = (Bitacora) dao.busca(Bitacora.class, bitaProyecto);
            VisorDao daoV = new VisorDao();
            Proyecto proy = null;
            
            List<Object[]> maxDocsDgiraFlujoHist;
            DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
            DocumentoDgiraFlujo docsDgiraFlujo;
            CatEstatusDocumento catEstatusDoc;
            DocumentoDgiraFlujoHist docsDgiraFlujoHist;

            if (bitaProyecto != null) {
                try {
                    proy = daoV.verificaERA2(bitaProyecto);
                    if (proy != null) {
                        if (proy.getProyectoPK() != null) {
                            ctxt.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
                            ctxt.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
                            folio = proy.getProyectoPK().getFolioProyecto();
                        }
                    }
                } catch (Exception ar) {
                    System.out.println("Problema: " + ar.getMessage());
                }

            }

    		// folio = ctxt.getExternalContext().getSessionMap().get("userFolioProy").toString();
    		// String bitacora = ctxt.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
    		String claveProyecto = ctxt.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
    		// String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
    		// int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
    		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
    		// String idDocumento = fContext.getExternalContext().getRequestParameterMap().get("idDocumento");
            // String strTipoDocto = fContext.getExternalContext().getRequestParameterMap().get("tipoDoctoTurnado");
                
    		
		
        System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		System.out.println("bitacora :" + bitaProyecto);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + tipoDocId);		
		System.out.println("idEntidad : " + idEntidad);
		System.out.println("cveTipTram : " + cveTipTram);
		System.out.println("idTipTram : " + idTipTram);
		System.out.println("edoGestTram : " + edoGestTram);
		System.out.println("-------------------------------------------------------------------------");		
		
        //tipoDoc=7;
          suspPorInfoAdic(tipoDoc);
          
          // para que cargue de nuevo las listas de oficios y refresque la ligaDocto
          cargaYaFirmadoDG();

	}
    
    
    /***
     * Regresa el siguiente perfil segun el perfil actual y estatus del documento 
     * @param perfilActual
     * @param estatusDocto
     * @return
     */
    public String perfilSiguiente (String perfilActual, short estatusDocto) {
        String perfilSig = "";
        
        if (perfilActual.compareToIgnoreCase("DG") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "dg";
            }
            if (estatusDocto == 2) {
                perfilSig = "da";
            }
        } else if (perfilActual.compareToIgnoreCase("DA") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "dg";
            }
            if (estatusDocto == 2) {
                perfilSig = "sd";
            }
        } else if (perfilActual.compareToIgnoreCase("SD") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "da";
            }
            if (estatusDocto == 2) {
                perfilSig = "eva";
            }
        } else if (perfilActual.compareToIgnoreCase("JD") == 0 || perfilActual.compareToIgnoreCase("eva") == 0) {
            if (estatusDocto == 3) {
                perfilSig = "sd";
            }
        } else {
            perfilSig = perfilActual;
        }
        /*
        switch(perfilSig) {
            case "sd":
                leyenda = "Subdirector";
                break;
            case "eva":
                leyenda = "Evaluador";
                break;
            case "da":
                leyenda = "Director de Area";
                break;
            case "dg":
                leyenda = "Director General";
        }
        
        return leyenda;
        */
        return perfilSig;
        
    }
  

	/**
	 * @return the file
	 */
	public StreamedContent getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(StreamedContent file) {
		this.file = file;
	}
    
	
    public void corrige(){
    	
    	System.out.println("\n\n corrige..");
    	System.out.println("comentarios : " + this.comentarioTurnado);
    	
    	
    	estatusDoctoTurnado=GenericConstants.ESTATUS_DOCTO_CORRECCION;
    	try {
    		turnaDoctoConsultaPublica_(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    
    
    /***
     * Método que realiza el turnado del oficio
     * 
     */
    public void turnaDoctoConsultaPublicaN(){
        System.out.println("\n\nturna oficio Consulta Publica..");

        FacesContext fContext = FacesContext.getCurrentInstance();
        String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
        String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
        String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
        int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
        // String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
        
        String idDocumentoStr = fContext.getExternalContext().getRequestParameterMap().get("idDocumento");
        int value = -1;
        for (int i = 0; i < listaUno.size(); i++) {   
            System.out.println( idDocumentoStr + " ::: " + listaUno.get(i).oficio);
            if (idDocumentoStr.equals(listaUno.get(i).oficio)) {
                value = i;
            }
        }
        
        // Si value es =-1 es una consulta para delegaciones o promovente
        if (value == -1) {
            tipoDocId = idDocumentoStr;
            String strTipoDocto = fContext.getExternalContext().getRequestParameterMap().get("tipoDoctoTurnado");
            System.out.println("strTipoDocto ::: " + strTipoDocto);
            tipoDoc = Short.valueOf(strTipoDocto);
        } else {
            tipoDocId = String.valueOf(value);
            tipoDoc = GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL;
        }
        
        String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();

        System.out.println("-------------------------------------------------------------------------");
        System.out.println("folio :" + folio);
        System.out.println("claveProyecto :" + claveProyecto);
        // System.out.println(fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
        // System.out.println(fContext.getExternalContext().getSessionMap().get("idUsu"));
        System.out.println("bitacora :" + bitacora);
        System.out.println("claveTramite :" + claveTramite);
        System.out.println("idTramite :" + idTramite);
        System.out.println("tipoDocId :" + tipoDoc);
        System.out.println("documentoId : " + tipoDocId);
        System.out.println("edoGestTramite : " + edoGestTram);
        System.out.println("rol : " + rol);
        System.out.println("-------------------------------------------------------------------------");
       
        if (isUrlDG == 1) {
            idDocumento = GenericConstants.ESTATUS_DOCTO_FIRMADO; // 4 firmado
        } else {
            idDocumento = GenericConstants.ESTATUS_DOCTO_AUTORIZADO; // 3 autorizado ( turnar )
        }
        // url que apunta al oficio dentro del folio( carpeta ). para este caso sera para ponerle esta url al codigo qr
        String ligaQR = Utils.ruta_liga_codigoqr + bitaProyecto;

        List<DocumentoRespuestaDgira> lista = daoDg.docsRespDgiraBusq2(bitacora, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
        for (DocumentoRespuestaDgira x : lista) {
            documento = x;
//            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
//            urlOfic = x.getDocumentoUrl();
        }

        System.out.println("idDocumento : " + idDocumento);
        System.out.println("urlOfic : " + urlOfic);
        System.out.println("nombreOfic : " + nombreOfic);
        System.out.println("ligaQR : " + ligaQR);
        System.out.println("-------------------------------------------------------------------------");

        String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
        String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
        String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
        String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);

        // busca si ya existe el registro de cadena firma...		
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        CadenaFirma cadenafirma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);

        if (cadenafirma == null) {
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_hay_token_registrado));
            return;
        }

        CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenafirma, rol);

        DetallesFirmado detallefirmado = null;
        try {

            detallefirmado = firmaDao.getFirmas(cadenafirma);

            if (detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() == 98)) {
                System.out.println("\nno responde el servicio web o no hay firmas...");
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
                return;
            } else if (detallefirmado != null && detallefirmado.getEstado() == 0 && (detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)) {

                // cadenafirmaLog puede ser null
                // detallefirmado si trae firmas del ws
                Firmas firmaws = firmaDao.GetFirmaWS(cadenafirma, detallefirmado);

                // para este caso si no hay firma del ws decimos que no ha firmado el usuario...
                if (firmaws == null) {
                    System.out.println("ultima firma válida : null");
                    System.out.println("\nel usuario no ha firmado...");
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
                    return;
                } else {
                    System.out.println("ultima firma válida : " + firmaws.getRfc() + " - " + firmaws.getFecha());
                    System.out.println("el usuario ya firmó y se guardará en cadenafirmalog...");
                    // SE ASUME QUE SI FIRMÓ YA por tanto primero agregamos el registro de cadenafirmalog correspondiente...
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
                    Date fcha = formatter.parse(firmaws.getFecha());
                    boolean resp = false;

                    if (cadenafirmaLog == null) {

                        CadenaFirmaLog firmaLog = new CadenaFirmaLog();
                        firmaLog.setCadenaFirma(cadenafirma);
                        firmaLog.setEstatus((short) 1);
                        firmaLog.setRol(rol);
                        firmaLog.setFecha(fcha);
                        firmaLog.setFirma(firmaws.getFirma());
                        firmaLog.setRfc(firmaws.getRfc());

                        resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
                        System.out.println("firmalog agregada: " + resp);

                    } else {
                        // se actualiza cadenafirmalog del rol en sesion
                        cadenafirmaLog.setRfc(firmaws.getRfc());
                        cadenafirmaLog.setFecha(fcha);
                        cadenafirmaLog.setFirma(firmaws.getFirma());
                        resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
                        System.out.println("firmalog modificada: " + resp);

                    }

                    if (resp == true) {

                        // y mandamos turnar...
                        if (idDocumento == GenericConstants.ESTATUS_DOCTO_AUTORIZADO) {

                            turnarDocumento();

                        } else {
                            //documento.setDocumentoUbicacion(urlOfic);
                            //documento.setDocumentoNombre(nombreOfic);
                            firmaDao.editarPdf(documento, cadenafirma, ligaQR);
                            firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(), cadenafirma.getFolioProyecto());
                            turnarDocumentoDG();
                        }

                    } else {
                        throw new Exception("Error: al guardar el log de la firma para el rol actual");
                    }
                }

            } else {
                // pudiera ser un error -99 ( no hay conexion con el servicio buscriptográfico )
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_se_realizo_turnado));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
        }
    }
    
    public void turnarDocumento() {

        System.out.println("turnaDoctoConsultaPublica");

        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqContEnv = RequestContext.getCurrentInstance();

        
        String idDocumentoStr = ctxt.getExternalContext().getRequestParameterMap().get("idDocumento");
        int value = -1;
        for (int i = 0; i < listaUno.size(); i++) {   
            System.out.println( idDocumentoStr + " ::: " + listaUno.get(i).oficio);
            if (idDocumentoStr.equals(listaUno.get(i).oficio)) {
                value = i;
            }
        }
        
        short sTipoDoc;
        
        // Si value es =-1 es una consulta para delegaciones o promovente
        if (value == -1) {
            tipoDocId = idDocumentoStr;
            String strTipoDocto = ctxt.getExternalContext().getRequestParameterMap().get("tipoDoctoTurnado");
            System.out.println("strTipoDocto ::: " + strTipoDocto);
            sTipoDoc = Short.valueOf(strTipoDocto);
        } else {
            tipoDocId = String.valueOf(value);
            sTipoDoc = GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL;
        }
                        
        DgiraMiaeDaoGeneral daoDGIRA = new DgiraMiaeDaoGeneral();
        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        Integer idAux = 0;
        short maxIdHisto = 0;
        Bitacora bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial subdirector;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        String folio = null;

        List<Object[]> maxDocsDgiraFlujoHist;
        DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
        DocumentoDgiraFlujo docsDgiraFlujo;
        CatEstatusDocumento catEstatusDoc;
        DocumentoDgiraFlujoHist docsDgiraFlujoHist;
        String observacionesDocDgira = this.comentarioTurnado;

        //bitaProyecto = null;
        if (bitaProyecto != null) {
            try {
                proy = daoV.verificaERA2(bitaProyecto);
                if (proy != null) {
                    if (proy.getProyectoPK() != null) {
                        ctxt.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
                        ctxt.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
                        folio = proy.getProyectoPK().getFolioProyecto();
                    }
                }
            } catch (Exception ar) {
                System.out.println("Problema: " + ar.getMessage());
            }

        }

        if (bitaProyecto != null) {
            //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

            maxDocsDgiraFlujoHist = daoDGIRA.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, tipoDocId);
            for (Object o : maxDocsDgiraFlujoHist) {
                idAux = Integer.parseInt(o.toString());
                maxIdHisto = idAux.shortValue();
            }

            docsDgiraFlujoHistEdit = daoDGIRA.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, tipoDocId, maxIdHisto);

            if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                daoDGIRA.modifica(docsDgiraFlujoHistEdit);
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
            docsDgiraFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, tipoDocId);

            if (docsDgiraFlujo != null) {
                if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                    banderaNew = 0;
                }// registro nuevo
                else {
                    banderaNew = 1;
                }

            } else {
                banderaNew = 1;
            }

            if (banderaNew == 1) {
                docsDgiraFlujo = new DocumentoDgiraFlujo(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, tipoDocId);
            }
             // esta variable es como el idDocumento pero solo se usa para turnar cuando no es el DG
            short idEstadoDocto = GenericConstants.ESTATUS_DOCTO_AUTORIZADO;
            catEstatusDoc = daoDGIRA.idCatEstatusDoc(idEstadoDocto);
            docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
            docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
            docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
            docsDgiraFlujo.setIdAreaRecibe(null);
            docsDgiraFlujo.setIdUsuarioRecibe(null);
            docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
            docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
            docsDgiraFlujo.setDocumentoDgiraObservacion(observacionesDocDgira);

            // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
            UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
            System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
            System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
            System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
            System.out.println("rol     :" + ur.getRolId().getNombreCorto());
            System.out.println("rolId     :" + ur.getRolId().getIdRol());

            // Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
            CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());

            // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
            docsDgiraFlujo.setRolId(rol);

            try {
                if (banderaNew == 1) {
                    System.out.println("banderaNew = 1");
                    daoDGIRA.agrega(docsDgiraFlujo);
                }
                if (banderaNew == 0) {
                    System.out.println("banderaNew = 0");
                    daoDGIRA.modifica(docsDgiraFlujo);
                }

                maxIdHisto = (short) (idAux + 1);
                docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, sTipoDoc, edoGestTram, tipoDocId, maxIdHisto);
                docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                docsDgiraFlujoHist.setDocumentoDgiraObservHist(observacionesDocDgira);
                try {
                    System.out.println("Llamando al metodo agrega de daoDGIRA...");
                    daoDGIRA.agrega(docsDgiraFlujoHist);
                } catch (Exception ex) {
                    System.out.println("Error al llamar al metodo agrega: " + ex.toString());
                    error = error + 1;
                }
            } catch (Exception e) {
                System.out.println("Error al llamar al metodo agrega o modifica: " + e.toString());
                error = error + 1;
            }

            if (error == 0) {
                System.out.println("Errore : 0, continua...");
                ArrayList<Object> aResp = verificaEstadoDocto(tipoDocId, sTipoDoc);
//                for (NotificacionHelper otp : notificacion) {
//                    if (otp.getId().intValue() == Short.valueOf(sIdDocumento).intValue()) {
//                        otp.setEditable((Boolean) aResp.get(1));
//                        otp.setLigaDocto((String) aResp.get(0));
//                        otp.setFirmado((Boolean) aResp.get(3));
//                    }
//                }

                System.out.println("Termina turnado de oficio");

                //Comienza procedimiento de segundas vias en otros casos, aqui­ no aplica
                //reqContEnv.execute("alert('Oficio turnado exitosamente')");
                ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));

                //------------------------------------------------------------
            } else {
                System.out.println("Se registro un error!");
                //reqContEnv.execute("alert('El turnado del oficio de falla, intente nuevamente')");
                ctxt.addMessage("growl", new FacesMessage("El turnado del oficio de falló, intente nuevamente."));
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
//            this.estatusTurnadoDocto = "0";
            this.comentarioTurnado = "";
        }

    }
    
    
    public void turnarDocumentoDG() throws IOException, DocumentException {

        System.out.println("\n\ncambioSituacionDG..");
        System.out.println("tipoDoc : " + tipoDoc);
        System.out.println("tipoDocId : " + tipoDocId);
        System.out.println("idDocumento : " + idDocumento);

        DgiraMiaeDaoGeneral daoDGIRA = new DgiraMiaeDaoGeneral();
        
        
        
        //TODO cambiar por tipo de oficio
        //tipoDoc=GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS;
        suspPorInfoAdic(tipoDoc);

        ArrayList<Object> aResp;
        //notificacion = new ArrayList();
        String ligaDoctoDG = "";
        boolean editable = true;
          DocumentoDgiraFlujo doctoFlujo;
        String documentoId_ = "";
        int i = 0;
        
        aResp = verificaEstadoDocto(tipoDocId, tipoDoc);
        ligaDoctoDG = (String) aResp.get(0);
        editable = (Boolean) aResp.get(1);  // para mostrar los botones de firmar, turnar y corregir       

        documentoId_ = tipoDocId;

        doctoFlujo = daoDGIRA.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS, edoGestTram, documentoId_);
        if ( doctoFlujo != null && doctoFlujo.getDocumentoDgiraFlujoPK() != null ) {
                  
            if (doctoFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO ) {
             //al oficio que acabo de turnar le actualizo la liga al pdf y oculto los botones de turnado
                if (tipoDocId.equals(documentoId_)) {
                    ligaDoctoDG = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora=" + bitaProyecto + "&clavetramite=" + cveTipTram + "&idtramite=" + idTipTram + "&tipodoc=" + GenericConstants.TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS + "&entidadfederativa=" + edoGestTram + "&documentoid=" + documentoId_;
                    editable = false; // para ocultar los botones de firmar, turnar y corregir
                }
            }
        }

        System.out.println("\nOficio Notificación - documentoId: " + documentoId_);
        System.out.println("Oficio Notificación - ligaDocto: " + ligaDoctoDG);
        System.out.println("Oficio Notificación - editable: " + editable);

    }
     
    /***
     * MÃ©todo que realiza el turnado del oficio unicamente cuando es un DG
     * @throws Exception
     */
    public void turnaDoctoConsultaPublicaDG(){
    	
    	System.out.println("\n\nturnaDoctoConsultaPublicaDG..");
    	
    	FacesContext fContext = FacesContext.getCurrentInstance();
		folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
		String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();
		this.comentarioTurnado = "";
	
		try {
            
            String idDocumento_ = fContext.getExternalContext().getRequestParameterMap().get("idDocumento");
            String strTipoDocto = fContext.getExternalContext().getRequestParameterMap().get("tipoDoctoTurnado");
            String ciudadanoPrincipal = fContext.getExternalContext().getRequestParameterMap().get("ciudadanoPrincipal");
            if (idDocumento_!=null ){
            	tipoDocId=idDocumento_;
            	
            }
            if (strTipoDocto!=null){
            	tipoDoc = Short.parseShort(strTipoDocto);
            }else{
            	if (ciudadanoPrincipal!=null && ciudadanoPrincipal.equals("S")){
            		tipoDoc =GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL;
            	}else{
            		tipoDoc =GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS;
            	}
            }
            
        }catch(Exception ex){
        	ex.printStackTrace();
        }
			
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);		
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + tipoDocId);
		System.out.println("edoGestTramite : " + edoGestTram);		
		System.out.println("rol : " + rol);
		System.out.println("-------------------------------------------------------------------------");
    	
		
		idDocumento = GenericConstants.ESTATUS_DOCTO_FIRMADO; // FIRMADO ( ojo no es el mismo que 'a firma', porque este es del director general )
    	
		// url que apunta al oficio dentro del folio( carpeta ). para este caso sera para ponerle esta url al codigo qr
		String ligaQR = Utils.ruta_liga_codigoqr + bitacora;
		
		List<DocumentoRespuestaDgira> lista = daoDg.docsRespDgiraBusq2(bitacora, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId);
        for (DocumentoRespuestaDgira x : lista) {
            documento = x;
//            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
//            urlOfic = x.getDocumentoUrl();
        }
        
        System.out.println("idDocumento : " + idDocumento);
		System.out.println("urlOfic : " + urlOfic);
		System.out.println("nombreOfic : " + nombreOfic);
		System.out.println("ligaQR : " + ligaQR);
		System.out.println("-------------------------------------------------------------------------");
				
    	// busca si ya existe el registro de cadena firma...		
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma cadenafirma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);

		String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
		String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
		String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
		String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);
		

		if(cadenafirma == null){
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_hay_token_registrado));
			return;
		}    	
		
		// obtenemos la firmalog del usuario logueado ( por su rol y nivel lo sabemos y no necesariamente existe este registro en este momento )
		// short nivel = Short.parseShort(rol.equalsIgnoreCase("sd")?"1":rol.equalsIgnoreCase("da")?"2":"0");			
		CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenafirma, /*nivel,*/  rol);
				
		// detallefirmado es objeto que tiene un estatus y descripcion y una lista de firmas del ws
		DetallesFirmado detallefirmado = null;
		try{
			detallefirmado = firmaDao.getFirmas(cadenafirma);
			// el ws no responde
			if(detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() != 0)){
				System.out.println("\nno responde el servicio web o no hay firmas...");
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));		
				return;
			// como el ws si esta respondiendo, analizar las firmas a ver si alguna de ellas corresponde con la de firmaLog actual...
			}else if (detallefirmado != null && detallefirmado.getEstado() == 0 && ( detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)){
				
				// cadenafirmaLog puede ser null
				// detallefirmado si trae firmas del ws
				Firmas firmaws = firmaDao.GetFirmaWS(cadenafirma, detallefirmado);
							
				// para este caso si no hay firma del ws decimos que no ha firmado el usuario...
				if(firmaws == null){
					System.out.println("ultima firma vÃ¡lida : null");
					System.out.println("\nel usuario no ha firmado...");
					FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
					return;
				}else{
					System.out.println("ultima firma vÃ¡lida : " + firmaws.getRfc() + " - " + firmaws.getFecha());
					System.out.println("el usuario ya firmÃ³ y se guardarÃ¡ en cadenafirmalog...");
					// SE ASUME QUE SI FIRMÃ“ YA por tanto primero agregamos el registro de cadenafirmalog correspondiente...
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
					Date fcha = formatter.parse(firmaws.getFecha());
					boolean resp = false;
					
					if(cadenafirmaLog == null){
						
						CadenaFirmaLog firmaLog = new CadenaFirmaLog();
						firmaLog.setCadenaFirma(cadenafirma);
						firmaLog.setEstatus((short)1);
						firmaLog.setRol(rol);
						firmaLog.setFecha(fcha);
						firmaLog.setFirma(firmaws.getFirma());
						firmaLog.setRfc(firmaws.getRfc());
					
						resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
						System.out.println("firmalog agregada: " + resp);
						
					}else{
						// se actualiza cadenafirmalog del rol en sesion
						cadenafirmaLog.setRfc(firmaws.getRfc());
						cadenafirmaLog.setFecha(fcha);
						cadenafirmaLog.setFirma(firmaws.getFirma());
						resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
						System.out.println("firmalog modificada: " + resp);
						
					}
					
					if(resp == true){
						
						// y mandamos turnar...
                                                //documento.setDocumentoUrl(urlOfic);
                                                //documento.setDocumentoNombre(nombreOfic);
						firmaDao.editarPdf(documento, cadenafirma, ligaQR);
						firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(),  cadenafirma.getFolioProyecto());
						turnaDoctoConsultaPublicaDG_();
						
					}else{
						throw new Exception("Error: al guardar el log de la firma para el rol actual");
					}
				}
				
			}else{

				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_se_realizo_turnado));
			}   
		}catch(Exception ex){
			System.out.println(ex.getMessage());			
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
		}		
    }


    
    
    /***
     * Descarga el jar para firma y registra la cadena firma si no existe o en su caso muestra el token al usuario
     */
    public void siFirmar(){                  
        System.out.println("\n\nsi firmar: ");
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/resources/SignerClientAll.jar");
        file = new DefaultStreamedContent(stream, "application/java-archive", "SignerClientAll.jar");

        FacesContext fContext = FacesContext.getCurrentInstance();
        String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
        String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
        String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
        int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
       
        String idDocumentoStr = fContext.getExternalContext().getRequestParameterMap().get("idDocumento");
        int value = -1;
        for (int i = 0; i < listaUno.size(); i++) {   
            System.out.println( idDocumentoStr + " ::: " + listaUno.get(i).oficio);
            if (idDocumentoStr.equals(listaUno.get(i).oficio)) {
                value = i;
            }
        }
                
        // Si value es = -1 es una consulta para delegaciones o promovente
        if (value == -1) {
            tipoDocId = idDocumentoStr;
            String strTipoDocto = fContext.getExternalContext().getRequestParameterMap().get("tipoDoctoTurnado");
            System.out.println("strTipoDocto ::: " + strTipoDocto );
            tipoDoc = Short.valueOf(strTipoDocto);
        } else {
            tipoDocId = String.valueOf(value);
            tipoDoc = GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL;
        }
        
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("folio :" + folio);
        System.out.println("claveProyecto :" + claveProyecto);
        System.out.println("bitacora :" + bitacora);
        System.out.println("claveTramite :" + claveTramite);
        System.out.println("idTramite :" + idTramite);
        System.out.println("tipoDocId :" + tipoDoc);
        System.out.println("documentoId : " + tipoDocId);
        System.out.println("edoGestTramite : " + edoGestTram);
        System.out.println("rol usuario : " + fContext.getExternalContext().getSessionMap().get("rol")); //sesion.getAttribute("rol").toString());
        System.out.println("idAreaUsu : " + fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
        System.out.println("idUsu : " + fContext.getExternalContext().getSessionMap().get("idUsu"));
        System.out.println("-------------------------------------------------------------------------");

        // busca si ya existe el registro de cadena firma...
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);

        String msg_numero_token = Utils.obtenerConstante(GenericConstants.FIRMAR_NUMERO_TOKEN);
        String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.FIRMAR_NO_REGISTRO_CADENAFIRMA);

        if (firma != null) {

            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));

        } else {
            String decripcionDocumento;
            if (tipoDoc == 8) {
                decripcionDocumento = "Cunsulta Publica de Delegaciones";
            } else if(tipoDoc == 10){
                decripcionDocumento = "Consulta Publica de ciudadano principal";
            } else{
                decripcionDocumento = "Consulta Publica de Promovente";
            }
            System.out.println("descripcion :" + decripcionDocumento);
//	        String destinatario = "Destinatario nuevo";
//	        System.out.println("destinatario :" + destinatario);
//	        String iniciales = "LDSM/ASR/AVA/GBA/JCTT";
//	        System.out.println("iniciales :" + iniciales);

            String infoaFirmar = "||" + bitacora + "|" + decripcionDocumento + "||";
            String infoaFirmarB64 = Base64.encodeBase64String(infoaFirmar.getBytes());
            System.out.println("encodeB64 :" + infoaFirmarB64);

            System.out.println("\n\nagregaCadena..");
            CadenaFirmaDao cadenaDao = new CadenaFirmaDao();
            BcAgregaCadenaRespuesta agregacadena = null;

            try {
                agregacadena = cadenaDao.agregaCadena(folio, infoaFirmarB64);

                if (agregacadena != null) {

                    firma = new CadenaFirma();

                    // firma.setClave(clave);
                    firma.setClaveProyecto(claveProyecto);
                    firma.setBitacora(bitacora);
                    firma.setClaveTramite2(cveTipTram);
                    firma.setIdTramite2(idTipTram);
                    firma.setTipoDocId(tipoDoc);
                    firma.setDocumentoId(tipoDocId);

                    firma.setFolioProyecto(folio);
                    firma.setB64(infoaFirmarB64);
                    firma.setIdentificador(agregacadena.getIdentificador());
                    firma.setOperacion(String.valueOf(agregacadena.getTransferenciaOperacion()));

                    new CadenaFirmaDao().agrega(firma);

                    // despues de agregar la cadena de firma, se consulta para mostrar el identificador al usuario..
                    firma = null;
                    // firmaDao = new CadenaFirmaDao();
                    // firma = getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, documentoId);					 
                    firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);

                    if (firma != null) {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
                    } else {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
                    }

                } else {

                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
            }
        }
    }
    
    private String getNameTipoDoc(short idTipoDoc){
	String name=null;
	
	if (GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION         == idTipoDoc){
		name=GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION_S; 
	}else if (GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE  == idTipoDoc){
		name=GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE_S;
	}else if (GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL  == idTipoDoc){
		name=GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL_S;
	}else if (GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS  == idTipoDoc){
		name=GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS_S;
	}
    
    return name;
}

public Tbarea getTbarea() {
	return tbarea;
}

public void setTbarea(Tbarea tbarea) {
	this.tbarea = tbarea;
}

public String getIdEntidad() {
	return idEntidad;
}

public void setIdEntidad(String idEntidad) {
	this.idEntidad = idEntidad;
}

public short getIdDocumento() {
	return idDocumento;
}

public void setIdDocumento(short idDocumento) {
	this.idDocumento = idDocumento;
}

public Boolean getTurnaDocsBtn() {
	return turnaDocsBtn;
}

public void setTurnaDocsBtn(Boolean turnaDocsBtn) {
	this.turnaDocsBtn = turnaDocsBtn;
}

public String getNombreOfic() {
	return nombreOfic;
}

public void setNombreOfic(String nombreOfic) {
	this.nombreOfic = nombreOfic;
}

    
}


