package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.mia.servicios.evaluacion.ServicioBitacora_Historial;
import mx.gob.semarnat.model.bitacora.Historial;

@SuppressWarnings("serial")
@ManagedBean(name = "semaforoBean")
@ViewScoped
public class SemaforoBean extends BaseBean implements Serializable {
	private final String IMG_VERDE = "traffic-light-green.jpg";
	private final String IMG_AMARILLO = "traffic-light-amber.jpg";
	private final String IMG_ROJO = "traffic-light-red.jpg";
	private final String CSS_COLOR_VERDE = "color: green;";
	private final String CSS_COLOR_AMARILLO = "color: gold;";
	private final String CSS_COLOR_ROJO = "color: red;";
	private final int MAX_DIAS_SOL_INFO_AD = 40;
	private final int MAX_DIAS_CAP_INFO_AD = 60;
	private final String SITUACION_INTEGRO_EXPEDIENTE = "AT0002";
	private final String SITUACION_FIRMADO_DIR_GENERAL = "ATIF01";
	private final String SITUACION_PROMOVENTE_RECIBE_SOL_IA = "CIS302";
	private final String SOL_IA_LEYENDA = "Plazo para solicitar información adicional:";
	private final String CAP_IA_LEYENDA = "Días restantes para presentar información adicional:";
	private final String[] SOL_IA_SITUACIONES = { "'" + SITUACION_INTEGRO_EXPEDIENTE + "'", "'AT0003'", "'AT0031'", "'AT0032'", "'AT0008'" };
	private final String[] CAP_IA_SITUACIONES = { "'" + SITUACION_FIRMADO_DIR_GENERAL + "'", "'" + SITUACION_PROMOVENTE_RECIBE_SOL_IA + "'" };
	// Ana Olvera
	// comento el 20
	// de Octubre que
	// la situación ATIF01 debera
	// quitarse una vez en producción para las situaciones del semaforo de captura
	//
	private final int[] SOL_IA_RANGO_VERDE = { 40, 11 };
	private final int[] SOL_IA_RANGO_AMARILLO = { 10, 1 };
	private final int SOL_IA_RANGO_ROJO = 0;
	//
	private final int[] CAP_IA_RANGO_VERDE = { 60, 21 };
	private final int[] CAP_IA_RANGO_AMARILLO = { 20, 1 };
	private final int CAP_IA_RANGO_ROJO = 0;
	//
	private String estatus = IMG_ROJO;
	private String diaColor = CSS_COLOR_ROJO;
	private String dias;
	private boolean solicitudIA;// Saber si es solicitud de IA o captura de IA
	private boolean mostrarSemaforo;
	private String leyendaSemaforo;
	private boolean desHabilitarGenerarOficioIA;
	//
	private String bitacora;
	private ServicioBitacora_Historial sBitHis;

	public SemaforoBean() {
		inicializarBean();
	}

	private void inicializarBean() {
            bitacora = null;
            mostrarSemaforo = false;
            desHabilitarGenerarOficioIA = false;
            //
            try {
                HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                if (req.getParameter("numBitacora") != null || req.getParameter("bitaNum") != null) {
                        if (req.getParameter("numBitacora") != null) {
                                bitacora = req.getParameter("numBitacora");
                                System.out.println("numBitacora: " + bitacora);
                        }
                        if (req.getParameter("bitaNum") != null && bitacora == null) {
                                bitacora = req.getParameter("bitaNum");
                                System.out.println("bitaNum: " + bitacora);
                        }
                }

                sBitHis = new ServicioBitacora_Historial();
                //
                if (!bitacora.contains("IP")) {
                    validarSemaforo();
                    if (mostrarSemaforo) {
                        leyendaSemaforo = solicitudIA ? SOL_IA_LEYENDA : CAP_IA_LEYENDA;
                    }
                }			
            } catch (NumberFormatException | MiaExcepcion e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            }
            System.out.println("SemaforoBean creado");
            System.out.println("mostrarSemaforo: " + mostrarSemaforo);
            System.out.println("solicitudIA: " + solicitudIA);
	}

	private void validaDias() throws MiaExcepcion {
		int diasRestantes = 0;
		
		
		if (solicitudIA) {
			Historial h = sBitHis.encontrarPorBitacoraPorSituacion(bitacora, SITUACION_INTEGRO_EXPEDIENTE);
			diasRestantes = sBitHis.diasHabilesRestantesPorRangoFechas(h.getHistoFecharec(), new Date(), MAX_DIAS_SOL_INFO_AD);
			if(diasRestantes > 40)
				diasRestantes = 40;
			if (diasRestantes < SOL_IA_RANGO_ROJO) {
				mostrarSemaforo = false;
			} else {
				if (enRango(SOL_IA_RANGO_VERDE[0], SOL_IA_RANGO_VERDE[1], diasRestantes)) {
					estatus = IMG_VERDE;
					diaColor = CSS_COLOR_VERDE;
				}
				if (enRango(SOL_IA_RANGO_AMARILLO[0], SOL_IA_RANGO_AMARILLO[1], diasRestantes)) {
					estatus = IMG_AMARILLO;
					diaColor = CSS_COLOR_AMARILLO;
				}
				if (diasRestantes <= SOL_IA_RANGO_ROJO ) {
					estatus = IMG_ROJO;
					diaColor = CSS_COLOR_ROJO;
					desHabilitarGenerarOficioIA = true;
                                        diasRestantes = 0;
				}
				dias = diasRestantes + " días";
			}
		} else {
			Date fechaTurnado = sBitHis.obtenFechaTurnadoPorBitacoraPorSituacion(bitacora, SITUACION_PROMOVENTE_RECIBE_SOL_IA);
			if (fechaTurnado == null) {
				mostrarSemaforo = false;
			} else {
				diasRestantes = sBitHis.diasRestantesPresentarIA(bitacora);

				if (enRango(CAP_IA_RANGO_VERDE[0], CAP_IA_RANGO_VERDE[1], diasRestantes)) {
					estatus = IMG_VERDE;
					diaColor = CSS_COLOR_VERDE;
				}
				if (enRango(CAP_IA_RANGO_AMARILLO[0], CAP_IA_RANGO_AMARILLO[1], diasRestantes)) {
					estatus = IMG_AMARILLO;
					diaColor = CSS_COLOR_AMARILLO;
				}
				if (diasRestantes <= CAP_IA_RANGO_ROJO) {
					estatus = IMG_ROJO;
					diaColor = CSS_COLOR_ROJO;
                                        diasRestantes = 0;
				}
				dias = diasRestantes + " días";

			}

		}

	}

	private boolean enRango(int a, int b, int c) {
		return c >= a && c <= b ? true : c >= b && c <= a;
	}

	private void validarSemaforo() throws NumberFormatException, MiaExcepcion {
		if (bitacora != null) {
			boolean solIA = sBitHis.existeBitacoraPorSituaciones(bitacora, SOL_IA_SITUACIONES);
			Historial h = sBitHis.encontrarPorBitacoraPorSituacion(bitacora, SITUACION_FIRMADO_DIR_GENERAL);
			if (solIA && h == null) {
				mostrarSemaforo = true;
				solicitudIA = true;
			} else {
				desHabilitarGenerarOficioIA = true;
			}

			if (!mostrarSemaforo) {
				boolean capIA = sBitHis.existeBitacoraPorSituaciones(bitacora, CAP_IA_SITUACIONES);
				if (capIA) {
					mostrarSemaforo = true;
					solicitudIA = false;
				}
			}

			if (mostrarSemaforo) {
				validaDias();
			}
		}
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getDiaColor() {
		return diaColor;
	}

	public void setDiaColor(String diaColor) {
		this.diaColor = diaColor;
	}

	public String getDias() {
		return dias;
	}

	public void setDias(String dias) {
		this.dias = dias;
	}

	public boolean isSolicitudIA() {
		return solicitudIA;
	}

	public void setSolicitudIA(boolean solicitudIA) {
		this.solicitudIA = solicitudIA;
	}

	public ServicioBitacora_Historial getsBitHis() {
		return sBitHis;
	}

	public void setsBitHis(ServicioBitacora_Historial sBitHis) {
		this.sBitHis = sBitHis;
	}

	public boolean isMostrarSemaforo() {
		return mostrarSemaforo;
	}

	public void setMostrarSemaforo(boolean mostrarSemaforo) {
		this.mostrarSemaforo = mostrarSemaforo;
	}

	public String getBitacora() {
		return bitacora;
	}

	public void setBitacora(String bitacora) {
		this.bitacora = bitacora;
	}
        
	public String getLeyendaSemaforo() {
		return leyendaSemaforo;
	}

	public void setLeyendaSemaforo(String leyendaSemaforo) {
		this.leyendaSemaforo = leyendaSemaforo;
	}

	public boolean isDesHabilitarGenerarOficioIA() {
		//
		return desHabilitarGenerarOficioIA;
	}

	public void setDesHabilitarGenerarOficioIA(boolean desHabilitarGenerarOficioIA) {
		this.desHabilitarGenerarOficioIA = desHabilitarGenerarOficioIA;
	}
}
