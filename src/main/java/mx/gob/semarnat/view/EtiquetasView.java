package mx.gob.semarnat.view;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

import mx.gob.semarnat.dao.ReportesProyectosDAO;
import mx.gob.semarnat.dao.DelegacionMunicipioDAO;
import mx.gob.semarnat.dao.EntidadFederativaDAO;
import mx.gob.semarnat.dao.SectorProyectoDAO;
import mx.gob.semarnat.dao.SubsectorProyectoDAO;
import mx.gob.semarnat.dao.TBAreaDAO;
import mx.gob.semarnat.model.catalogos.DelegacionMunicipio;
import mx.gob.semarnat.model.catalogos.EntidadFederativa;
import mx.gob.semarnat.model.catalogos.Sector;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.secure.AppSession;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.Utils;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class EtiquetasView implements Serializable {

    private static final Logger logger = Logger.getLogger(EtiquetasView.class.getName());

    private String datos_generales1 = "<b>BITÁCORA:</b>09/IP-0101/11/12<br /><b>CLAVE:</b> 03BS2012M0008 <br />"
            + "<b>NOMBRE DEL PROYECTO:</b>Proyecto de exploración minera directa Texcalama, San Antonio, municipio de la paz, B.C.S.<br />"
            + "<b>PROMOVENTE:</b> COMPAÑIA MINERA PITALLA SA DE CV <br />";

    private String datos_generales = "<b>BITÁCORA:</b>09/MG-0132/12/13<br /><b>CLAVE:</b> 16MI2013E0014 <br />"
            + "<b>NOMBRE DEL PROYECTO:</b> LÍNEAS DE TRANSMISIÓN (LT'S) PURÉPECHA- VISTA HERMOSA, PURÉPECHA-JACONA, PURÉPECHA ENTRONQUE SAHUAYO-JIQUILPAN, "
            + "PURÉPECHA ENTRONQUE CARAPÍN-MAZAMITLA Y SUBESTACIÓN ELÉCTRICA (SE) PURÉPECHA<br />"
            + "<b>PROMOVENTE:</b> COMISIÓN FEDERAL DE ELECTRICIDAD <br />";

    private String detalles = "<b>SUBSECTOR:</b> GASERO<br />" + "<b>RAMA: </b><br />"
            + "<b>TIPO DE PROYECTO:</b><br />";
    private String detalles1 = "<b>SUBSECTOR:</b> GASERO<br />" + "<b>RAMA: </b><br />"
            + "<b>TIPO DE PROYECTO:</b><br />";
    private String evaluador = "<b> Nombre del usuario: </b> Evaluador 1";
    private String subdirector = "<b> Nombre del usuario: </b> Subdirector 1";
    private String da = "<b> Nombre del usuario: </b> Director 1";
    private String dg = "<b> Nombre del usuario: </b> Director General";
    private String ecc = "<b> Nombre del usuario: </b> Espacio de Contacto Ciudadano 1";
    private MenuModel oficios;
    private MenuModel docDeriv;
    private MenuModel leyes;
    private MenuModel consultas;
    /**
     * Permite indicar que pantalla del submenu seleccionado del Listado de
     * Proyectos mostrar.
     */
    private String opcionSeleccionada;

    /**
     * Permite mostrar el listado de menus del proyecto segun los roles que
     * contenga el usuario loggeado.
     */
    private MenuModel menusListadoProyectos;

    private MenuModel menusAdministracion;

    // Consultas
//    private int tipoProyectos = 0;
    private EntidadFederativaDAO entidadFederativaDAO;
    private SectorProyectoDAO sectorProyectoDAO;
    private SubsectorProyectoDAO subsectorProyectoDAO;
    private DelegacionMunicipioDAO delegacionMunicipioDAO;
    private ReportesProyectosDAO reportesProyectosDAO;
    private TBAreaDAO tbAreaDO;

    private List<EntidadFederativa> listEntidadFeder;
    private List<SectorProyecto> listSectorProy;
    private List<SubsectorProyecto> listSubsectorProy;
    private List<DelegacionMunicipio> listDelMuni;
    private List<Object[]> listRepProy;
    private List<Object[]> listResumenes;
    private List<Tbarea> listTBArea;
    private List<Object[]> listSubdirs;
    private List<Object[]> listEvals;

    private Date fechaInicio;
    private Date fechaFin;
    private String claveProyecto;
    private String entidad;
    private int riesgo;
    private int sector;
    private String entidadGestion;
    private String municipo;
    private String estudio;
    private int subsector;
    //resueltos
    private String direccion;
    private String subdir;
    private String eval;
    private int eficiencia;//Si/No
    private int sentidoResol;

    private int totalizarOpcion;
    private String tituloColTipoResumen;
    private boolean busquedaOk = false;
    private double[] totalesResumenesCalculados;//Subsector_tipoTram;//3 posiciones
//    private double[] totalesEficiencia;//5 posiciones
//    private double[] totalesInversionEmpl;//8 posiciones

    @SuppressWarnings("unchecked")
    @PostConstruct
    public void init() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));

        oficios = new DefaultMenuModel();
        docDeriv = new DefaultMenuModel();
        leyes = new DefaultMenuModel();
        consultas = new DefaultMenuModel();

        menusListadoProyectos = new DefaultMenuModel();

        menusAdministracion = new DefaultMenuModel();

        /*
		 * DefaultMenuItem item = new DefaultMenuItem("De prevencion");
		 * oficios.addElement(item); item = new DefaultMenuItem("Resolutivo");
		 * item.setCommand("#{etiquetasView.abrirArchivo}");
		 * oficios.addElement(item);
         */
        DefaultMenuItem oficPrev = new DefaultMenuItem("De prevención");
        DefaultMenuItem oficOpinTec = new DefaultMenuItem("De solicitud de opinión técnica");
        DefaultMenuItem oficConsPub = new DefaultMenuItem("De consulta pÃºblica");
        DefaultMenuItem oficReuPub = new DefaultMenuItem("De Reunión pÃºblica");
        DefaultMenuItem oficNotifMun = new DefaultMenuItem("De notificación a municipios");
        DefaultMenuItem oficInfoAdic = new DefaultMenuItem("De solicitud de información adicional");
        DefaultMenuItem oficAmpPlazo = new DefaultMenuItem("De ampliación de plazo");
        DefaultMenuItem oficDes = new DefaultMenuItem("De desechamiento");
        DefaultMenuItem oficResol = new DefaultMenuItem("Resolutivo");

        DefaultMenuItem docHojRut = new DefaultMenuItem("Hoja de ruta");
        DefaultMenuItem docRteSub = new DefaultMenuItem("Reporte técnico de evaluación subdirector");
        DefaultMenuItem docRteEval = new DefaultMenuItem("Reporte técnico de evaluación evaluador");
        DefaultMenuItem docRteAxo1 = new DefaultMenuItem("Reporte técnico de evaluación anexo I- ERA");
        DefaultMenuItem docRteAxo2 = new DefaultMenuItem("Reporte técnico de evaluación anexo II- Consulta PÃºblica");
        DefaultMenuItem docVisCamp = new DefaultMenuItem("Reporte de visita de campo");
        DefaultMenuItem docHojAcu = new DefaultMenuItem("Hoja de Acuerdo");

        DefaultMenuItem lfpa = new DefaultMenuItem("LFPA");
        DefaultMenuItem lgeepa = new DefaultMenuItem("LGEEPA");
        DefaultMenuItem reia = new DefaultMenuItem("REIA");

        // MENU CONSULTAS ADMINISTRADOR
        DefaultMenuItem repProyIngresados = new DefaultMenuItem("Reportes de proyectos ingresados");
        repProyIngresados.setCommand("#{etiquetasView.pantallaRolUsuario(201)}");
        DefaultMenuItem repProyResueltos = new DefaultMenuItem("Reportes de proyectos resueltos");
        repProyResueltos.setCommand("#{etiquetasView.pantallaRolUsuario(202)}");

        HttpSession session = Util.getSession();
        // String rol = session.getAttribute("rol").toString();

        this.opcionSeleccionada = (String) session.getAttribute("opcionSeleccionada");

        List<UsuarioRol> roles = (List<UsuarioRol>) session.getAttribute("roles");

        for (UsuarioRol rol : roles) {
            // SI ES DIRECTOR GENERAL
            if (rol.getRolId().getNombreCorto().equals("dg")) {
                DefaultMenuItem menuOficioPendienteFirma = new DefaultMenuItem(
                        "Proyectos con Oficio pendiente de firma (DG)");
                DefaultMenuItem menuOficioFirmado = new DefaultMenuItem("Proyectos con Oficio Firmado (DG)");

                RequestContext.getCurrentInstance()
                        .update(":formBandejaTodos:grdPanelProyOficio, :formProyectosFirmado:grdProyectoOficioFirma");

                menuOficioPendienteFirma.setParam("menuRolSeleccionado", "dg");
                menuOficioFirmado.setParam("menuRolSeleccionado", "dg");
                
                menuOficioPendienteFirma.setParam("vista", "uno");
                menuOficioFirmado.setParam("vista", "dos");

                menuOficioPendienteFirma.setUpdate(":formMenu:toolMenu");

                menuOficioPendienteFirma.setCommand("#{etiquetasView.pantallaRolUsuario(1)}");
                menuOficioFirmado.setCommand("#{etiquetasView.pantallaRolUsuario(2)}");

                this.menusListadoProyectos.addElement(menuOficioPendienteFirma);
                this.menusListadoProyectos.addElement(menuOficioFirmado);

            } else if (rol.getRolId().getNombreCorto().equals("da")) {
                DefaultMenuItem menuProyectosPendientesTurnarDA = new DefaultMenuItem("Proyectos Pendientes por Turnar (DA)");
                DefaultMenuItem menuProyectosProcesoDA = new DefaultMenuItem("Proyectos en Proceso (DA)");

                menuProyectosPendientesTurnarDA.setParam("menuRolSeleccionado", "da");
                menuProyectosProcesoDA.setParam("menuRolSeleccionado", "da");
                
                menuProyectosPendientesTurnarDA.setParam("vista", "uno");
                menuProyectosProcesoDA.setParam("vista", "dos");

                menuProyectosPendientesTurnarDA.setCommand("#{etiquetasView.pantallaRolUsuario(3)}");
                menuProyectosProcesoDA.setCommand("#{etiquetasView.pantallaRolUsuario(4)}");

                this.menusListadoProyectos.addElement(menuProyectosPendientesTurnarDA);
                this.menusListadoProyectos.addElement(menuProyectosProcesoDA);

            } else if (rol.getRolId().getNombreCorto().equals("sd")) {
                DefaultMenuItem menuProyectosPendientesTurnarSD = new DefaultMenuItem(
                        "Proyectos Pendientes por Turnar (SD)");
                DefaultMenuItem menuProyectosProcesoSD = new DefaultMenuItem("Proyectos en Proceso (SD)");

                menuProyectosPendientesTurnarSD.setParam("menuRolSeleccionado", "sd");
                menuProyectosProcesoSD.setParam("menuRolSeleccionado", "sd");
                
                menuProyectosPendientesTurnarSD.setParam("vista", "uno");
                menuProyectosProcesoSD.setParam("vista", "dos");

                menuProyectosPendientesTurnarSD.setCommand("#{etiquetasView.pantallaRolUsuario(5)}");
                menuProyectosProcesoSD.setCommand("#{etiquetasView.pantallaRolUsuario(6)}");

                this.menusListadoProyectos.addElement(menuProyectosPendientesTurnarSD);
                this.menusListadoProyectos.addElement(menuProyectosProcesoSD);

            } else if (rol.getRolId().getNombreCorto().equals("eva")) {
                DefaultMenuItem menuProyectosEvaluacion = new DefaultMenuItem("Proyectos en Evaluación (EVA)");
                DefaultMenuItem menuProyectosProcesoEVA = new DefaultMenuItem("Proyectos en Proceso (EVA)");

                menuProyectosEvaluacion.setParam("menuRolSeleccionado", "eva");
                menuProyectosProcesoEVA.setParam("menuRolSeleccionado", "eva");
                
                menuProyectosEvaluacion.setParam("vista", "uno");
                menuProyectosProcesoEVA.setParam("vista", "dos");

                menuProyectosEvaluacion.setCommand("#{etiquetasView.pantallaRolUsuario(7)}");
                menuProyectosProcesoEVA.setCommand("#{etiquetasView.pantallaRolUsuario(8)}");

                this.menusListadoProyectos.addElement(menuProyectosEvaluacion);
                this.menusListadoProyectos.addElement(menuProyectosProcesoEVA);

            } else if (rol.getRolId().getNombreCorto().equals("sad")) {
                DefaultMenuItem menuUsuarios = new DefaultMenuItem("Usuarios");
                menuUsuarios.setParam("menuRolSeleccionado", "sad");
                menuUsuarios.setCommand("#{etiquetasView.pantallaRolUsuario(101)}");

                DefaultMenuItem menuDirectorio = new DefaultMenuItem("Directorio");
                menuDirectorio.setParam("menuRolSeleccionado", "sad");
                menuDirectorio.setCommand("#{etiquetasView.pantallaRolUsuario(102)}");

                repProyIngresados.setParam("menuRolSeleccionado", "admin");
                repProyResueltos.setParam("menuRolSeleccionado", "admin");
                
                this.menusAdministracion.addElement(menuUsuarios);
                this.menusAdministracion.addElement(menuDirectorio);

            } else if (rol.getRolId().getNombreCorto().equals("admin")) {
                DefaultMenuItem menuUsuarios = new DefaultMenuItem("Usuarios");
                menuUsuarios.setParam("menuRolSeleccionado", "admin");
                menuUsuarios.setCommand("#{etiquetasView.pantallaRolUsuario(101)}");

                DefaultMenuItem menuDirectorio = new DefaultMenuItem("Directorio");
                menuDirectorio.setParam("menuRolSeleccionado", "admin");
                menuDirectorio.setCommand("#{etiquetasView.pantallaRolUsuario(102)}");

                this.menusAdministracion.addElement(menuUsuarios);
                this.menusAdministracion.addElement(menuDirectorio);

                repProyIngresados.setParam("menuRolSeleccionado", "admin");
                repProyResueltos.setParam("menuRolSeleccionado", "admin");
            }
        }

        this.oficios.addElement(oficPrev);
        this.oficios.addElement(oficOpinTec);
        this.oficios.addElement(oficConsPub);
        this.oficios.addElement(oficReuPub);
        this.oficios.addElement(oficNotifMun);
        this.oficios.addElement(oficInfoAdic);
        this.oficios.addElement(oficAmpPlazo);
        this.oficios.addElement(oficDes);
        this.oficios.addElement(oficResol);

        this.docDeriv.addElement(docHojRut);
        this.docDeriv.addElement(docRteSub);
        this.docDeriv.addElement(docRteEval);
        this.docDeriv.addElement(docRteAxo1);
        this.docDeriv.addElement(docRteAxo2);
        this.docDeriv.addElement(docVisCamp);
        this.docDeriv.addElement(docHojAcu);

        this.leyes.addElement(lfpa);
        this.leyes.addElement(lgeepa);
        this.leyes.addElement(reia);

//        if(rol.getRolId().getNombreCorto().equals("admin")){
//            
//        }
        this.consultas.addElement(repProyIngresados);
        this.consultas.addElement(repProyResueltos);

        // CONSULTAS//
        entidadFederativaDAO = new EntidadFederativaDAO();
        sectorProyectoDAO = new SectorProyectoDAO();
        subsectorProyectoDAO = new SubsectorProyectoDAO();
        delegacionMunicipioDAO = new DelegacionMunicipioDAO();
        reportesProyectosDAO = new ReportesProyectosDAO();
        tbAreaDO = new TBAreaDAO();

        listEntidadFeder = entidadFederativaDAO.getAllEntidadF();
        listSectorProy = sectorProyectoDAO.getAllSectorP();
        listTBArea = tbAreaDO.getAllTBArea();
        listSubdirs = reportesProyectosDAO.getPersonasByRol(5);//5: subdirectores
        listEvals = reportesProyectosDAO.getPersonasByRol(6);//6: evaluadores

        Calendar c = Calendar.getInstance();// fecha actual
        c.set(Calendar.DATE, 1);

        fechaInicio = c.getTime();
        fechaFin = new Date();
        tituloColTipoResumen = "Todos";

        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }

    /**
     * Permite cargar la informacion de la pantalla seleccionad segun el rol del
     * usuario
     */
    public String pantallaRolUsuario(int opcionSeleccionada) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        HttpSession session = Util.getSession();
        // Se obtiene el identificador para saber que pantalla del rol mostrar
        String menuSeleccionado = params.get("menuRolSeleccionado");
        String vista = params.get("vista");

        this.opcionSeleccionada = String.valueOf(opcionSeleccionada);
        session.setAttribute("opcionSeleccionada", this.opcionSeleccionada);
        session.setAttribute("vista", vista);

        // Si es un menu del director general.
        if (menuSeleccionado.equals("dg")) {
            session.setAttribute("rol", "dg");
        } else if (menuSeleccionado.equals("da")) {
            session.setAttribute("rol", "da");
        } else if (menuSeleccionado.equals("sd")) {
            session.setAttribute("rol", "sd");
        } else if (menuSeleccionado.equals("eva")) {
            session.setAttribute("rol", "eva");
        } else if (menuSeleccionado.equals("sad")) {
            session.setAttribute("rol", "sad");
        } else if (menuSeleccionado.equals("admin")) {
            session.setAttribute("rol", "admin");
        }

        // String contextPath =
        // FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            if (opcionSeleccionada < 100) {
                ec.redirect(ec.getRequestContextPath() + "/faces/Restringido/pantallaPrincipal.xhtml"); // Listado
                // de
                // proyectos
            } else if (opcionSeleccionada >= 100 && opcionSeleccionada <= 200) {
                ec.redirect(ec.getRequestContextPath() + "/faces/Restringido/pantallaPrincipalAdministracion.xhtml"); // Opciones
                // de
                // Administrador
            } else if (opcionSeleccionada > 200 && opcionSeleccionada <= 202) {
                ec.redirect(ec.getRequestContextPath() + "/faces/Restringido/pantallaPrincipalConsultas.xhtml"); // Consultas
                // Administrador
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());

        System.out.println("\ninformación de rol seleccionado :");
        if (ur != null) {
            System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
            System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
            System.out.println("rol     :" + ur.getRolId().getNombreCorto());
            System.out.println("rolId   :" + ur.getRolId().getIdRol());
        }

        return "";
    }

    /*
	 * CONSULTAS 1.- REPORTES DE PROYECTOS INGRESADOS 2.- REPORTES DE PROYECTOS
	 * RESUELTOS
     */
    public void asignarVistaTipoProyectos(int tipo) {
        System.out.println("Tipo vista: " + tipo);
        resetReportesProyectos();
        opcionSeleccionada = "" + tipo;
//        tipoProyectos = tipo;
    }

    public void asignarDelMun() {
        System.out.println("id Ent fed: " + entidad);
        listDelMuni = delegacionMunicipioDAO.getAllDelegacionMunicipioByEntidad(entidad);
    }

    public void asignarSubsectores() {
        System.out.println("id Sector: " + sector);
        listSubsectorProy = subsectorProyectoDAO.getSubsectorProyBySector(sector);
    }

    public void buscarReportes() {
        System.out.println("Buscando ...");
        System.out.println("Fecini: " + fechaInicio);
        System.out.println("Fecfin: " + fechaFin);
        System.out.println("clave pro: " + claveProyecto);
        System.out.println("Entidad: " + entidad);
        System.out.println("Riesgo: " + riesgo);
        System.out.println("Sector: " + sector);
        System.out.println("Entidad gestion: " + entidadGestion);
        System.out.println("Municipio: " + municipo);
        System.out.println("Estudio: " + estudio);
        System.out.println("Subsector: " + subsector);
        System.out.println("--SOLO PARA RESUELTOS...");
        System.out.println("Direccion: " + direccion);
        System.out.println("Subdirector: " + subdir);
        System.out.println("Evaluador: " + eval);
        System.out.println("Sentido de la resolucion: " + sentidoResol);
        System.out.println("Eficiencia: " + eficiencia);

//        if (tipoProyectos == 1) {
        if (opcionSeleccionada.equals("201")) {
            listRepProy = reportesProyectosDAO.busquedaRepProyIngreso(fechaInicio, fechaFin, claveProyecto, entidad, riesgo, sector,
                    entidadGestion, municipo, estudio, subsector);
        } else if (opcionSeleccionada.equals("202")) {
            listRepProy = reportesProyectosDAO.busquedaRepProyResueltos(fechaInicio, fechaFin, claveProyecto, entidad, riesgo, sector,
                    entidadGestion, municipo, estudio, subsector, direccion, subdir, eval, eficiencia, sentidoResol);
        }

        busquedaOk = !listRepProy.isEmpty();//si repIng no es vacia = true

    }

    public void totalizarCmd(int tipoTramite) {
        System.out.println("Tipo tramite > " + tipoTramite);
        System.out.println("Totalizando > " + totalizarOpcion);
        if (busquedaOk) {
            if (tipoTramite == 1) {//proyectos ingresados
                switch (totalizarOpcion) {
//                    case 1:
//                        listResumenes = reportesProyectosDAO.resumenPorSubsector(fechaInicio, fechaFin, subsector);
//                        listResumenes.addAll(reportesProyectosDAO.resumenPorTipoTramite(fechaInicio, fechaFin, estudio));
//                        listResumenes.addAll(reportesProyectosDAO.resumenPorEntidad(fechaInicio, fechaFin, entidad));
//                        tituloColTipoResumen = "Todos";
//                        break;
                    case 2:
                        listResumenes = reportesProyectosDAO.resumenPorSubsector(fechaInicio, fechaFin, subsector);
                        tituloColTipoResumen = "Por subsector";
                        break;
                    case 3:
                        listResumenes = reportesProyectosDAO.resumenPorTipoTramite(fechaInicio, fechaFin, estudio);
                        tituloColTipoResumen = "Por tipo de trámite";
                        break;
                    case 4:
                        listResumenes = reportesProyectosDAO.resumenPorEntidad(fechaInicio, fechaFin, entidad);
                        tituloColTipoResumen = "Por entidad";
                        break;
                    default:
                        listResumenes = new ArrayList<>();
                        break;
                }
                totalesResumenes(0);
            } else if (tipoTramite == 2) {//proyectos resueltos
                switch (totalizarOpcion) {
                    case 1:
                        listResumenes = reportesProyectosDAO.resumenPorSubsectorProyRes(fechaInicio, fechaFin, subsector);
                        tituloColTipoResumen = "Por subsector";
                        totalesResumenes(1);
                        break;
                    case 2:
                        listResumenes = reportesProyectosDAO.resumenPorTipoTramProyRes(fechaInicio, fechaFin, estudio);
                        tituloColTipoResumen = "Por tipo de tramite";
                        totalesResumenes(2);
                        break;
                    case 3:
                        listResumenes = reportesProyectosDAO.resumenPorEntidadProyRes(fechaInicio, fechaFin, entidad);
                        tituloColTipoResumen = "Por entidad";
                        totalesResumenes(3);
                        break;
                    case 4:
                        listResumenes = reportesProyectosDAO.resumenPorEficienciaProyRes(fechaInicio, fechaFin, eficiencia);
                        tituloColTipoResumen = "Por eficiencia";
                        totalesResumenes(4);
                        break;
                    case 5:
                        listResumenes = reportesProyectosDAO.resumenPorResumenInversionProyRes(fechaInicio, fechaFin);
                        tituloColTipoResumen = "Por Resumen de Inversión y Empleos de Proyectos Autorizados en oficinas centrales";
                        totalesResumenes(5);
                        break;
                    default:
                        listResumenes = new ArrayList<>();
                        break;
                }
            }

            System.out.println("Finaliza Totalizado > " + totalizarOpcion);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Realiza una busqueda de reportes"));
        }
    }

    public void totalesResumenes(int totalesTipo) {
        System.out.println("Tipo totaliazo resumenes");
        if (totalesTipo == 0) {
            totalesResumenesCalculados = new double[1];
            for (Object[] i : listResumenes) {
                totalesResumenesCalculados[0] += Double.parseDouble("" + i[1]);
//                totalesResumenesCalculados[1] += Double.parseDouble("" + i[2]);
            }
            for (int i = 0; i < totalesResumenesCalculados.length; i++) {
                System.out.println((i + 1) + " " + totalesResumenesCalculados[i]);
            }
        }
        if (totalesTipo >= 1 && totalesTipo <= 3) {
            totalesResumenesCalculados = new double[3];
            for (Object[] i : listResumenes) {
                totalesResumenesCalculados[0] += Double.parseDouble("" + i[1]);
                totalesResumenesCalculados[1] += Double.parseDouble("" + i[2]);
                totalesResumenesCalculados[2] += Double.parseDouble("" + i[3]);
            }
            for (int i = 0; i < totalesResumenesCalculados.length; i++) {
                System.out.println((i + 1) + " " + totalesResumenesCalculados[i]);
            }
        } else if (totalesTipo == 4) {
            totalesResumenesCalculados = new double[5];
            for (Object[] i : listResumenes) {
                totalesResumenesCalculados[0] += Double.parseDouble("" + i[1]);
                totalesResumenesCalculados[1] += Double.parseDouble("" + i[2]);
                totalesResumenesCalculados[2] += Double.parseDouble("" + i[3]);
                totalesResumenesCalculados[3] += Double.parseDouble("" + i[4]);
                totalesResumenesCalculados[4] += Double.parseDouble("" + i[5]);
            }
            for (int i = 0; i < totalesResumenesCalculados.length; i++) {
                System.out.println((i + 1) + " " + totalesResumenesCalculados[i]);
            }
        } else if (totalesTipo == 5) {
            totalesResumenesCalculados = new double[8];
            for (Object[] i : listResumenes) {
                totalesResumenesCalculados[0] += Double.parseDouble("" + i[1]);
                totalesResumenesCalculados[1] += Double.parseDouble("" + i[2]);
                totalesResumenesCalculados[2] += Double.parseDouble("" + i[3]);
                totalesResumenesCalculados[3] += Double.parseDouble("" + i[4]);
                totalesResumenesCalculados[4] += Double.parseDouble("" + i[5]);
                totalesResumenesCalculados[5] += Double.parseDouble("" + i[6]);
                totalesResumenesCalculados[6] += Double.parseDouble("" + i[7]);
                totalesResumenesCalculados[7] += Double.parseDouble("" + i[8]);
            }
            for (int i = 0; i < totalesResumenesCalculados.length; i++) {
                System.out.println((i + 1) + " " + totalesResumenesCalculados[i]);
            }
        } else {
            System.out.println("Tipo totaliazo resumenes: --- ninguno");
        }
    }

    public void resetReportesProyectos() {

//		Date fechaInicio;
//		Date fechaFin;
        this.claveProyecto = null;
        this.entidad = null;
        this.riesgo = 0;
        this.sector = 0;
        this.entidadGestion = null;
        this.municipo = null;
        this.estudio = null;
        this.subsector = 0;
        //resueltos
        this.direccion = null;
        this.subdir = null;
        this.eval = null;
        this.eficiencia = 0;//Si/No
        this.sentidoResol = 0;

        this.listRepProy = new ArrayList<>();
        this.listResumenes = new ArrayList<>();

        this.totalizarOpcion = 0;
        this.tituloColTipoResumen = null;
        this.busquedaOk = false;
    }

    /*
	 * fin
	 * cnsultas-----------------------------------------------------------------
	 * ---------
     */
    public String getDatos_generales() {
        return datos_generales;
    }

    public void setDatos_generales(String datos_generales) {
        this.datos_generales = datos_generales;
    }

    public String getEvaluador() {
        return evaluador;
    }

    public void setEvaluador(String evaluador) {
        this.evaluador = evaluador;
    }

    public String getSubdirector() {
        return subdirector;
    }

    public void setSubdirector(String subdirector) {
        this.subdirector = subdirector;
    }

    public String getDa() {
        return da;
    }

    public void setDa(String da) {
        this.da = da;
    }

    public String getDg() {
        return dg;
    }

    public void setDg(String dg) {
        this.dg = dg;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getEcc() {
        return ecc;
    }

    public void setEcc(String ecc) {
        this.ecc = ecc;
    }

    public MenuModel getOficios() {
        return oficios;
    }

    public void setOficios(MenuModel oficios) {
        this.oficios = oficios;
    }

    public MenuModel getDocDeriv() {
        return docDeriv;
    }

    public void setDocDeriv(MenuModel docDeriv) {
        this.docDeriv = docDeriv;
    }

    public MenuModel getLeyes() {
        return leyes;
    }

    public void setLeyes(MenuModel leyes) {
        this.leyes = leyes;
    }

    public MenuModel getConsultas() {
        return consultas;
    }

    public void setConsultas(MenuModel consultas) {
        this.consultas = consultas;
    }

    public String update() {
        return datos_generales;
    }

    public String getDatos_generales1() {
        return datos_generales1;
    }

    public void setDatos_generales1(String datos_generales1) {
        this.datos_generales1 = datos_generales1;
    }

    public String getDetalles1() {
        return detalles1;
    }

    public void setDetalles1(String detalles1) {
        this.detalles1 = detalles1;
    }

    public void abrirArchivo() {
        try {
            File path = new File("resources/media/03BS2012M0008Resolutivo.pdf");
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the menusListadoProyectos
     */
    public MenuModel getMenusListadoProyectos() {
        return menusListadoProyectos;
    }

    /**
     * @param menusListadoProyectos the menusListadoProyectos to set
     */
    public void setMenusListadoProyectos(MenuModel menusListadoProyectos) {
        this.menusListadoProyectos = menusListadoProyectos;
    }

    /**
     * @return the opcionSeleccionada
     */
    public String getOpcionSeleccionada() {
        return opcionSeleccionada;
    }

    /**
     * @param opcionSeleccionada the opcionSeleccionada to set
     */
    public void setOpcionSeleccionada(String opcionSeleccionada) {
        this.opcionSeleccionada = opcionSeleccionada;
    }

    public MenuModel getMenusAdministracion() {
        return menusAdministracion;
    }

    public List<EntidadFederativa> getListEntidadFeder() {
        return listEntidadFeder;
    }

    public List<SectorProyecto> getListSectorProy() {
        return listSectorProy;
    }

    public void setListSectorProy(List<SectorProyecto> listSectorProy) {
        this.listSectorProy = listSectorProy;
    }

    public List<SubsectorProyecto> getListSubsectorProy() {
        return listSubsectorProy;
    }

    public void setListSubsectorProy(List<SubsectorProyecto> listSubsectorProy) {
        this.listSubsectorProy = listSubsectorProy;
    }

    public void setListEntidadFeder(List<EntidadFederativa> listEntidadFeder) {
        this.listEntidadFeder = listEntidadFeder;
    }

    public List<DelegacionMunicipio> getListDelMuni() {
        return listDelMuni;
    }

    public void setListDelMuni(List<DelegacionMunicipio> listDelMuni) {
        this.listDelMuni = listDelMuni;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getClaveProyecto() {
        return claveProyecto;
    }

    public void setClaveProyecto(String claveProyecto) {
        this.claveProyecto = claveProyecto;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public int getRiesgo() {
        return riesgo;
    }

    public void setRiesgo(int riesgo) {
        this.riesgo = riesgo;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public String getEntidadGestion() {
        return entidadGestion;
    }

    public void setEntidadGestion(String entidadGestion) {
        this.entidadGestion = entidadGestion;
    }

    public String getMunicipo() {
        return municipo;
    }

    public void setMunicipo(String municipo) {
        this.municipo = municipo;
    }

    public String getEstudio() {
        return estudio;
    }

    public void setEstudio(String estudio) {
        this.estudio = estudio;
    }

    public int getSubsector() {
        return subsector;
    }

    public void setSubsector(int subsector) {
        this.subsector = subsector;
    }

    public List<Object[]> getListRepProy() {
        return listRepProy;
    }

    public void setListRepProy(List<Object[]> listRepProy) {
        this.listRepProy = listRepProy;
    }

    public List<Object[]> getListResumenes() {
        return listResumenes;
    }

    public void setListResumenes(List<Object[]> listResumenes) {
        this.listResumenes = listResumenes;
    }

    public int getTotalizarOpcion() {
        return totalizarOpcion;
    }

    public void setTotalizarOpcion(int totalizarOpcion) {
        this.totalizarOpcion = totalizarOpcion;
    }

    public String getTituloColTipoResumen() {
        return tituloColTipoResumen;
    }

    public void setTituloColTipoResumen(String tituloColTipoResumen) {
        this.tituloColTipoResumen = tituloColTipoResumen;
    }

    public List<Tbarea> getListTBArea() {
        return listTBArea;
    }

    public void setListTBArea(List<Tbarea> listTBArea) {
        this.listTBArea = listTBArea;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSubdir() {
        return subdir;
    }

    public void setSubdir(String subdir) {
        this.subdir = subdir;
    }

    public int getEficiencia() {
        return eficiencia;
    }

    public void setEficiencia(int eficiencia) {
        this.eficiencia = eficiencia;
    }

    public List<Object[]> getListSubdirs() {
        return listSubdirs;
    }

    public void setListSubdirs(List<Object[]> listSubdirs) {
        this.listSubdirs = listSubdirs;
    }

    public List<Object[]> getListEvals() {
        return listEvals;
    }

    public void setListEvals(List<Object[]> listEvals) {
        this.listEvals = listEvals;
    }

    public String getEval() {
        return eval;
    }

    public void setEval(String eval) {
        this.eval = eval;
    }

    public int getSentidoResol() {
        return sentidoResol;
    }

    public void setSentidoResol(int sentidoResol) {
        this.sentidoResol = sentidoResol;
    }

    public double[] getTotalesResumenesCalculados() {
        return totalesResumenesCalculados;
    }

}
