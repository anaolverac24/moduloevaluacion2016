package mx.gob.semarnat.view;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.gob.semarnat.dao.EntidadFederativaDAO;
import mx.gob.semarnat.dao.LoginDAO;
import mx.gob.semarnat.dao.UsuarioRolDAO;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.mia.servicios.ServicioTokenGO;
import mx.gob.semarnat.model.catalogos.EntidadFederativa;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbusuario;
import mx.gob.semarnat.model.seguridad.TokenGO;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 * @version 2
 * @author Paty
 */
@SuppressWarnings("serial")
@ManagedBean(name = "loginView")
@SessionScoped
public class LoginView implements Serializable {

    private static final Logger logger = Logger.getLogger(LoginView.class.getName());

    private String cadenaMD5;
    private String usuario;
    private String password;
    private String passDig;
    private Tbusuario tbUsuario;
    private final LoginDAO dao = new LoginDAO();

    @SuppressWarnings("unused")
    private EntidadFederativaDAO entidadFederativaDAO = new EntidadFederativaDAO();

    private EntidadFederativa entidadFederativa;
    private Tbarea tbArea;
    /**
     * Lista de roles de un usuario.
     */
    private List<UsuarioRol> rolesUsuario;
    /**
     * Roles a validar para el muestreo de los botones del menu.
     */
    private List<String> roles;

    public LoginView() {
        logger.debug(Utils.obtenerLogConstructor(""));
    }

    //<editor-fold defaultstate="collapsed" desc="toHexadecimal(byte[] digest)">
    /**
     * *
     * Convierte un arreglo de bytes a String usando valores hexadecimales
     *
     * @param digest arreglo de bytes a convertir
     * @return String creado a partir de <code>digest</code>
     */
    private static String toHexadecimal(byte[] digest) {
        String hash = "";
        for (byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }
            hash += Integer.toHexString(b);
        }
        return hash;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getStringMessageDigest(String password)">
    /**
     * *
     * Encripta un mensaje de texto mediante algoritmo de resumen de mensaje.
     *
     * @param password texto a encriptar
     * @return mensaje encriptado
     */
    public String getStringMessageDigest(String password) {
        String algorithm = "MD5";
        byte[] digest = null;
        byte[] buffer = password.getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Error creando Digest");
        }
        return toHexadecimal(digest);
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Obtener nombre del usuario en sesión">
    public String getNombreUsuario() {
        HttpSession session = Util.getSession();
        return session.getAttribute("usuario").toString();
    }//</editor-fold>

    /**
     * Identificar al usuario con nombre de usuario y contraseña
     */
    public void autenticacion() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String userFolioProy = req.getParameter("folioProy");
        System.out.println("FOLIOPROY:::" + userFolioProy);
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("userFolioProy", userFolioProy);

        if (usuario != null && passDig != null) {

            tbUsuario = dao.autenticacion(usuario, passDig);
            UsuarioRolDAO usuarioRolDAO = new UsuarioRolDAO();

            if (tbUsuario.getIdusuario() != null) {

                context.getExternalContext().getSessionMap().put("idAreaUsu", tbUsuario.getIdarea());
                context.getExternalContext().getSessionMap().put("idUsu", tbUsuario.getIdusuario());
                context.getExternalContext().getSessionMap().put("idEmpleado", tbUsuario.getIdempleado());
                context.getExternalContext().getSessionMap().put("nombUsu", tbUsuario.getUsuario());

                try {
                    tbArea = (Tbarea) dao.buscaPerf(tbUsuario); //buscar perfil del usuario
                    if (tbArea != null) {
                        HttpSession session = Util.getSession();
                        session.setAttribute("usuario", tbUsuario.getUsuario());
                        session.setAttribute("idUsuario", tbUsuario.getIdusuario());
                        //Se consulta el Area con su Entidad Federativa del usuario.
                        EntidadFederativaDAO entidadFederativaDAO = new EntidadFederativaDAO();
                        entidadFederativa = entidadFederativaDAO.consultarEntidadFederativaID(tbArea.getIdentidadfederativa());

                        //Se manda en sesion la delegacion del usuario.
                        session.setAttribute("areaDelegacionUsuario", tbArea);

                        try {
                            rolesUsuario = usuarioRolDAO.consultarRolesByIdUsuario(tbUsuario.getIdusuario());
                            // Si el usuario contiene roles
                            if (rolesUsuario != null) {
                                roles = new ArrayList<String>();
                                //Se asignan a la sesion.
                                session.setAttribute("roles", rolesUsuario);

                                //Se obtiene la fecha actual.
                                Date fechaSystem = Calendar.getInstance().getTime();
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                String fechaActualizacionConvert = simpleDateFormat.format(fechaSystem);

                                //Se convierte el formato de la fecha actual a dd-MM-yyyy
                                Date fechaActualConvert = simpleDateFormat.parse(fechaActualizacionConvert);

                                //Se itera la lista de los roles del usuario
                                for (UsuarioRol usuarioRol : rolesUsuario) {

                                    //Si el usuario tiene el rol de Administrador o Super Admin se mantienen los permisos.
                                    switch (usuarioRol.getRolId().getIdRol()) {
                                        case 1:
                                            //Si es SuperAdmin no borra el rol.
                                            //Se agrega el rol al usuario.
                                            roles.add(usuarioRol.getRolId().getNombreCorto());
                                            break;
                                        case 2:
                                            //Si es Administrador no borra el rol.
                                            //Se agrega el rol al usuario.
                                            roles.add(usuarioRol.getRolId().getNombreCorto());
                                            break;
                                        default:
                                            String fechaVencimiento = usuarioRol.getFechaVencimiento().replace("(DOF ", "").replace(")", "");
                                            //Se convierte el formato de la fecha actual a dd-MM-yyyy
                                            Date fechaVencimientoConvert = simpleDateFormat.parse(fechaVencimiento);

                                            //Si el rol de la fecha de vencimiento es menor a la fecha actual.
                                            if (fechaVencimientoConvert.compareTo(fechaActualConvert) < 0) {
                                                //Se elimina el rol del usuario.
                                                usuarioRolDAO.eliminarRolByIdUsuarioAndByIdRol(
                                                        usuarioRol.getUsuarioId().getIdusuario(), usuarioRol.getRolId().getIdRol());
                                            } else {
                                                //Se agrega el rol al usuario.
                                                roles.add(usuarioRol.getRolId().getNombreCorto());
                                            }
                                            break;
                                    }
                                }
                            } else {
                                session.setAttribute("roles", tbArea.getJerarquia());
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        if (tbArea.getJerarquia().equals("DA")) {
                            session.setAttribute("rol", "DA");
                            context.getExternalContext().getSessionMap().put("rol", "da");
                            FacesContext.getCurrentInstance().getExternalContext().redirect("./Restringido/pantallaPrincipal.xhtml");
                        }
                        if (tbArea.getJerarquia().equals("SD")) {
                            session.setAttribute("rol", "SD");
                            context.getExternalContext().getSessionMap().put("rol", "sd");
                            FacesContext.getCurrentInstance().getExternalContext().redirect("./Restringido/pantallaPrincipal.xhtml");
                        }
                        if (tbArea.getJerarquia().equals("JD") && (tbArea.getSiglas().contains("EVALUADOR"))) {
                            session.setAttribute("rol", "EVA");
                            context.getExternalContext().getSessionMap().put("rol", "eva");
                            FacesContext.getCurrentInstance().getExternalContext().redirect("./Restringido/pantallaPrincipal.xhtml");
                        }
                        if (tbArea.getJerarquia().equals("DG")) {
                            session.setAttribute("rol", "DG");
                            context.getExternalContext().getSessionMap().put("rol", "dg");
                            FacesContext.getCurrentInstance().getExternalContext().redirect("./Restringido/pantallaPrincipal.xhtml");
                        }
                    }
                    ////////TOKEN para conexion con el generador de oficios
                    String token = createToken();
                    ServicioTokenGO stg = new ServicioTokenGO();
                    Date date = new Date();

                    TokenGO tokenGO = new TokenGO(tbUsuario.getIdusuario(), token, date);

                    stg.save(tokenGO);

                } catch (Exception er) {
                    er.printStackTrace();
                }
            } else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario NO encontrado", null);
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
    }

    public String createToken() {
        String token = "";
        int longitud = 100;
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while (i < longitud) {
            char c = (char) r.nextInt(255);
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'z')) {
                token += c;
                i++;
            }
        }

        return token.replace("\\", "").replace("/", "").replace("%", "").replace("`", "");
    }

     public void muestraPlantillas() {
        try {
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            RequestContext context = RequestContext.getCurrentInstance();

            String goURL = servletContext.getInitParameter("go_url");
            ServicioTokenGO go = new ServicioTokenGO();
            TokenGO token = go.getToken(tbUsuario.getIdusuario());

            URL url = new URL(goURL + "/templates?token=" + token.getToken()+"&scope=templates"); 
            context.execute("window.open('" + url.toString() + "','_blank')");
        } catch (MiaExcepcion ex) {
            ex.printStackTrace();
        } catch (MalformedURLException ex) {
            java.util.logging.Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public String logout() {
        HttpSession session = Util.getSession();

        session.invalidate();
        /// token generador de oficios
        ServicioTokenGO stg = new ServicioTokenGO();
        TokenGO tokenGO = new TokenGO(tbUsuario.getIdusuario());

        try {
            stg.delete(tokenGO.getIdUsuario());
        } catch (MiaExcepcion e) {
            e.printStackTrace();
        }

        return "/login?faces-redirect=true";
    }

    /**
     * @return the cadenaMD5
     */
    public String getCadenaMD5() {
        return cadenaMD5;
    }

    /**
     * @param cadenaMD5 the cadenaMD5 to set
     */
    public void setCadenaMD5(String cadenaMD5) {
        this.cadenaMD5 = cadenaMD5;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the tbusUario
     */
    public Tbusuario getTbusUario() {
        return tbUsuario;
    }

    /**
     * @param tbusUario the tbusUario to set
     */
    public void setTbusUario(Tbusuario tbusUario) {
        this.tbUsuario = tbusUario;
    }//</editor-fold>

    /**
     * @return the passDig
     */
    public String getPassDig() {
        return passDig;
    }

    /**
     * @param passDig the passDig to set
     */
    public void setPassDig(String passDig) {
        this.passDig = passDig;
    }

    /**
     * @return the tbArea
     */
    public Tbarea getTbArea() {
        return tbArea;
    }

    /**
     * @param tbArea the tbArea to set
     */
    public void setTbArea(Tbarea tbArea) {
        this.tbArea = tbArea;
    }

    /**
     * @return the entidadFederativa
     */
    public EntidadFederativa getEntidadFederativa() {
        return entidadFederativa;
    }

    /**
     * @param entidadFederativa the entidadFederativa to set
     */
    public void setEntidadFederativa(EntidadFederativa entidadFederativa) {
        this.entidadFederativa = entidadFederativa;
    }

    /**
     * @return the rolesUsuario
     */
    public List<UsuarioRol> getRolesUsuario() {
        return rolesUsuario;
    }

    /**
     * @param rolesUsuario the rolesUsuario to set
     */
    public void setRolesUsuario(List<UsuarioRol> rolesUsuario) {
        this.rolesUsuario = rolesUsuario;
    }

    /**
     * @return the roles
     */
    public List<String> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

}
