package mx.gob.semarnat.view;

public class ArchivoAnexo {

    private final String documento;
    private String respuesta;
    private String observaciones;
    private int pos;
    private short idSec;
    private short idReq;
    private boolean existe;
    private String url;

    public ArchivoAnexo(String documento) {
        this.documento = documento;
        this.existe = false;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getDocumento() {
        return documento;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public short getIdSec() {
        return idSec;
    }

    public void setIdSec(short idSec) {
        this.idSec = idSec;
    }

    public short getIdReq() {
        return idReq;
    }

    public void setIdReq(short idReq) {
        this.idReq = idReq;
    }

    public boolean isExiste() {
        return existe;
    }

    public void setExiste(boolean existe) {
        this.existe = existe;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ArchivoAnexo{" + "documento=" + documento + ", respuesta=" + respuesta + ", observaciones=" + observaciones + ", pos=" + pos + ", idSec=" + idSec + ", idReq=" + idReq + ", existe=" + existe + ", url=" + url + '}';
    }

}
