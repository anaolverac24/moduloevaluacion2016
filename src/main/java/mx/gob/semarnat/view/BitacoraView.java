/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.model.sinat.SinatSinatec;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Paty
 */
@SuppressWarnings("serial")
public class BitacoraView implements Serializable {
    private static final Logger logger = Logger.getLogger(BitacoraView.class.getName());

    private final BitacoraDao dao = new BitacoraDao();
    private final VisorDao daoVisor = new VisorDao();

    private List<Historial> listHistorial;
    private List<Bitacora> listBitacora;
    private Bitacora bitacora;
    private SinatDgira sinatDgira;
    @SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	private List<Object[]> todosProy = new ArrayList();
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> detalleProy = new ArrayList();
    @SuppressWarnings({ "unused", "unchecked", "rawtypes" })
	private List<Object[]> todosProyTurnados = new ArrayList();
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> ProyCapitulos = new ArrayList();
    private String bitacoraNom;
    private String TunaSubDirec;
    private SelectItem[] cmbDirec;
    private String liga;
    private SinatSinatec sinatSinatec;
    private Proyecto proyDetalle;
    private Bitacora bitacoraDetalle;
    private EvaluacionProyecto evaluacionProy;
    private String idArea;
    private Integer idusuario;
    private SinatDgira sinatDgiraAT0027;
    private Boolean MuestraOpcTurnado = false;
    private Boolean MuestraOpcTurnadoNo = false;

    //<editor-fold defaultstate="expanded" desc="Constructor">
    public BitacoraView() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        listBitacora = dao.enECC(); //TODOS LOS TRAMITE CON SITUACION CIS104
        FacesContext fContext = FacesContext.getCurrentInstance();
        fContext.getExternalContext().getSessionMap().get("userFolioProy");

        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu"); //
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");

        //<editor-fold defaultstate="collapsed" desc="CAMBIO DE SITUACION DE CIS104 A AT0001">
        for (Bitacora e : listBitacora) {
            RequestContext reqcontEnv = RequestContext.getCurrentInstance();
            try {
                bitacora = (Bitacora) dao.busca(Bitacora.class, new String(e.getBitaNumero()));
                if (bitacora != null) {
                    sinatDgira = new SinatDgira();
                    sinatDgira.setId(0);
                    sinatDgira.setSituacion("AT0027");
                    sinatDgira.setIdEntidadFederativa(bitacora.getBitaEntidadGestion());
                    sinatDgira.setIdClaveTramite(bitacora.getBitaTipotram()); //STRING BITA_TIPOTRAM
                    sinatDgira.setIdTramite(bitacora.getBitaIdTramite());
                    sinatDgira.setIdAreaEnvio(idArea.trim());
                    sinatDgira.setIdAreaRecibe(idArea.trim());
                    sinatDgira.setIdUsuarioEnvio(idusuario);
                    sinatDgira.setIdUsuarioRecibe(idusuario);
                    sinatDgira.setGrupoTrabajo(new Short("0"));
                    sinatDgira.setBitacora(e.getBitaNumero());
                    sinatDgira.setFecha(new Date());
                    dao.agrega(sinatDgira);
                }
                reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            } catch (Exception er) {
                er.printStackTrace();
                reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
            }

        }//</editor-fold>

//        todosProy = dao.enRecepDirector2(); //TODOS LOS TRAMITES CON ID DE SITUACION AT0001
        //<editor-fold desc="MOSTRAR DETALLE DE TRAMITE" defaultstate="collapsed">
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String bitacoraNum = req.getParameter("numBitacora");
        if (bitacoraNum != null) {

//            bitacoraNom = bitacoraNum;
            //------verificar si el tramite ya esta turnado, para saber como mostrar el detalle del proyecto, con opcion a turnado o sin opcion a turnado
            sinatDgiraAT0027 = dao.datosProySinatDgira(bitacoraNum);
            try {
                if (sinatDgiraAT0027.getBitacora().length() > 0) //si SI esta lleno NO se mostrara opcion a turnado, por que ya estan turnados
                {
                    MuestraOpcTurnadoNo = true;
                    MuestraOpcTurnado = false;

                    detalleProy = dao.detalleProySubDirector(bitacoraNum); //TODOS LO TRAMITES CON SITUACION (AT0027)
                }
            } catch (Exception ex) {
                if (sinatDgiraAT0027 != null) //si NO esta lleno SI se mostrara opcion a turnado
                {
                    MuestraOpcTurnadoNo = false;
                    MuestraOpcTurnado = true;
                    detalleProy = dao.detalleProyDirector(bitacoraNum); //si NO esta lleno SI se mostrara opcion a turnado
                }
            }

            //-----------------se mapea bitacora en tabla SINAT_SINATEC para insertar Clave de proyecto y bitacora en tabla PROYECTO de DGIRA_MIAE
            sinatSinatec = dao.sinatSintec(bitacoraNom);
            if (sinatSinatec != null) {
                proyDetalle = daoVisor.detalleFolProy(sinatSinatec.getFolio().toString()); //tabla proyecto de DGIRA_MIAE (folio)
                bitacoraDetalle = dao.datosBitaInd(sinatSinatec.getBitacora()); //Tabla BITACORA de esquema BITACORA (numBitacora)
                evaluacionProy = dao.datosEvaProy(sinatSinatec.getBitacora());
                //-----si la bitacora en tabla SINAT_SINATEC se encuentra en tabla BITACORA de esquema BITACORA, se insertara numero de bitacora en tabla PROYECTO de DGIRA_MIAE 
                if (proyDetalle != null && bitacoraDetalle != null) {
                    if(proyDetalle.getBitacoraProyecto() == null)
                    {
                        proyDetalle.setBitacoraProyecto(bitacoraDetalle.getBitaNumero());
                        daoVisor.merge(proyDetalle);
                    }
                    if(evaluacionProy != null && evaluacionProy.getBitacoraProyecto() == null)
                    {
                        evaluacionProy.setBitacoraProyecto(sinatSinatec.getBitacora());
                        daoVisor.agrega(evaluacionProy);
                    }
                }
            } else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite ingresado por SINAT, no exiten datos para el expediente electrónico.", null);
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }//</editor-fold>

        cmbDirec = todoDireccion();
//        todosProyTurnados = dao.enRecepDirectorTurnados() ; //TODOS LOS TRAMITES CON ID DE SITUACION AT0001

        //<editor-fold desc="genracion de URL, para visor de Expediente Electronico" defaultstate="collapsed">
        liga = "#";
        try {
            if (bitacoraNom != null) {
                bitacora = dao.datosBitaInd(bitacoraNom);
                if (bitacora != null) {
                    if (bitacora.getBitaTipotram().equals("MP") || bitacora.getBitaTipotram().equals("DM") || bitacora.getBitaTipotram().equals("MG") 
                    		|| bitacora.getBitaTipotram().equals("DL")) {
                        liga = "pantallaDaInsideCapMIA.xhtml?bitaNum=" + bitacoraNom;
                        //liga = "pantallaDaInsideCapMIA.xhtml?bitaNum="+bitacoraNom;
                    }
                    if (bitacora.getBitaTipotram().equals("IP")) {
                        liga = "pantallaDaInsideCapIP.xhtml?bitaNum=" + bitacoraNom;
                        //liga = "pantallaDaInsideCapIP.xhtml";
                    }
                }
            }

        } catch (Exception e) {
        }//</editor-fold>
        
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }//</editor-fold>

    //<editor-fold desc="Listado de proyectos en direccion" defaultstate="collapsed">
    public SelectItem[] todoDireccion() {
        List<Tbarea> lista = new ArrayList<Tbarea>();
        lista = dao.direccionTodos();

        int size = lista.size() + 1;
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        //if (selectOne) {
        items[0] = new SelectItem("0", "-- Seleccione --");
        i++;
        //}
        for (Tbarea x : lista) {

            items[i++] = new SelectItem(x.getTbareaPK().getIdarea(), x.getArea());
        }
        return items;

    }//</editor-fold>

    //<editor-fold desc="Guardado del turnado" defaultstate="collapsed">
    public void guardar() {
        ProyCapitulos = dao.detalleProyCap(TunaSubDirec); //TODOS LOS TRAMITES CON ID DE SITUACION AT0001
    }//</editor-fold>

    //<editor-fold desc="Seleccion de la referencia" defaultstate="collapsed">
    public void seleccionaReferencia() {
        //cambio de situacion de AT0001 A I00008, TURNADO DE DIRECTOR A SUBDIRECTOR

        if (TunaSubDirec != null && bitacoraNom != null) {
            try {
                bitacora = (Bitacora) dao.busca(Bitacora.class, new String(bitacoraNom));
                if (bitacora != null) {

                    sinatDgira = new SinatDgira();
                    sinatDgira.setId(0);
                    sinatDgira.setSituacion("AT0027");
                    sinatDgira.setIdEntidadFederativa(bitacora.getBitaEntidadGestion());
                    sinatDgira.setIdClaveTramite(bitacora.getBitaTipotram()); //STRING BITA_TIPOTRAM
                    sinatDgira.setIdTramite(bitacora.getBitaIdTramite());
                    sinatDgira.setIdAreaEnvio(idArea.trim());
                    sinatDgira.setIdAreaRecibe(TunaSubDirec.trim());
                    sinatDgira.setIdUsuarioEnvio(idusuario);
                    //sinatDgira.setIdUsuarioRecibe(1340);
                    sinatDgira.setGrupoTrabajo(new Short("0"));
                    sinatDgira.setBitacora(bitacoraNom);
                    sinatDgira.setFecha(new Date());
                    dao.agrega(sinatDgira);

                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Su información ha sido guardada con éxito.", null);
                    FacesContext.getCurrentInstance().addMessage("messages", message);
                }
            } catch (Exception er) {
                er.printStackTrace();

                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido guardar la información, Intente más tarde.", null);
                FacesContext.getCurrentInstance().addMessage("messages", message);
            }
        }
    }//</editor-fold>

    //<editor-fold desc="Getters & setters" defaultstate="collapsed">
    /**
     * @return the listHistorial
     */
    public List<Historial> getListHistorial() {
        return listHistorial;
    }

    /**
     * @param listHistorial the listHistorial to set
     */
    public void setListHistorial(List<Historial> listHistorial) {
        this.listHistorial = listHistorial;
    }

    /**
     * @return the bitacora
     */
    public Bitacora getBitacora() {
        return bitacora;
    }

    /**
     * @param bitacora the bitacora to set
     */
    public void setBitacora(Bitacora bitacora) {
        this.bitacora = bitacora;
    }

    /**
     * @return the detalleProy
     */
    public List<Object[]> getDetalleProy() {
        return detalleProy;
    }

    /**
     * @param detalleProy the detalleProy to set
     */
    public void setDetalleProy(List<Object[]> detalleProy) {
        this.detalleProy = detalleProy;
    }

    /**
     * @return the bitacoraNom
     */
    public String getBitacoraNom() {
        return bitacoraNom;
    }

    /**
     * @param bitacoraNom the bitacoraNom to set
     */
    public void setBitacoraNom(String bitacoraNom) {
        this.bitacoraNom = bitacoraNom;
    }

    /**
     * @return the TunaSubDirec
     */
    public String getTunaSubDirec() {

        return TunaSubDirec;
    }

    /**
     * @param TunaSubDirec the TunaSubDirec to set
     */
    public void setTunaSubDirec(String TunaSubDirec) {

        this.TunaSubDirec = TunaSubDirec;
    }

    /**
     * @return the cmbDirec
     */
    public SelectItem[] getCmbDirec() {
        return cmbDirec;
    }

    /**
     * @param cmbDirec the cmbDirec to set
     */
    public void setCmbDirec(SelectItem[] cmbDirec) {
        this.cmbDirec = cmbDirec;
    }

    /**
     * @return the todosProyTurnados
     */
//    public List<Object[]> getTodosProyTurnados() {
//        return todosProyTurnados;
//    }
//
//    /**
//     * @param todosProyTurnados the todosProyTurnados to set
//     */
//    public void setTodosProyTurnados(List<Object[]> todosProyTurnados) {
//        this.todosProyTurnados = todosProyTurnados;
//    }
    /**
     * @return the ProyCapitulos
     */
    public List<Object[]> getProyCapitulos() {
        return ProyCapitulos;
    }

    /**
     * @param ProyCapitulos the ProyCapitulos to set
     */
    public void setProyCapitulos(List<Object[]> ProyCapitulos) {
        this.ProyCapitulos = ProyCapitulos;
    }

    /**
     * @return the liga
     */
    public String getLiga() {
        return liga;
    }

    /**
     * @param liga the liga to set
     */
    public void setLiga(String liga) {
        this.liga = liga;
    }

    /**
     * @return the sinatSinatec
     */
    public SinatSinatec getSinatSinatec() {
        return sinatSinatec;
    }

    /**
     * @param sinatSinatec the sinatSinatec to set
     */
    public void setSinatSinatec(SinatSinatec sinatSinatec) {
        this.sinatSinatec = sinatSinatec;
    }

    /**
     * @return the proyDetalle
     */
    public Proyecto getProyDetalle() {
        return proyDetalle;
    }

    /**
     * @param proyDetalle the proyDetalle to set
     */
    public void setProyDetalle(Proyecto proyDetalle) {
        this.proyDetalle = proyDetalle;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the sinatDgiraAT0027
     */
    public SinatDgira getSinatDgiraAT0027() {    	              
        return sinatDgiraAT0027;
    }

    /**
     * @param sinatDgiraAT0027 the sinatDgiraAT0027 to set
     */
    public void setSinatDgiraAT0027(SinatDgira sinatDgiraAT0027) {
        this.sinatDgiraAT0027 = sinatDgiraAT0027;
    }

    /**
     * @return the MuestraOpcTurnado
     */
    public Boolean getMuestraOpcTurnado() {
        return MuestraOpcTurnado;
    }

    /**
     * @param MuestraOpcTurnado the MuestraOpcTurnado to set
     */
    public void setMuestraOpcTurnado(Boolean MuestraOpcTurnado) {
        this.MuestraOpcTurnado = MuestraOpcTurnado;
    }

    /**
     * @return the MuestraOpcTurnadoNo
     */
    public Boolean getMuestraOpcTurnadoNo() {
        return MuestraOpcTurnadoNo;
    }

    /**
     * @param MuestraOpcTurnadoNo the MuestraOpcTurnadoNo to set
     */
    public void setMuestraOpcTurnadoNo(Boolean MuestraOpcTurnadoNo) {
        this.MuestraOpcTurnadoNo = MuestraOpcTurnadoNo;
    }//</editor-fold>

    /**
     * @return the evaluacionProy
     */
    public EvaluacionProyecto getEvaluacionProy() {
        return evaluacionProy;
    }

    /**
     * @param evaluacionProy the evaluacionProy to set
     */
    public void setEvaluacionProy(EvaluacionProyecto evaluacionProy) {
        this.evaluacionProy = evaluacionProy;
    }

}
