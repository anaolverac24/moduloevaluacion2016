package mx.gob.semarnat.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.primefaces.context.RequestContext;

import mx.go.semarnat.services.ServicioDocument;
import mx.go.semarnat.services.ServicioUnidadAdministrativa;
import mx.go.semarnat.services.UnidadAdministrativa;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.mia.servicios.ServicioTokenGO;
import mx.gob.semarnat.mia.servicios.dgiramiae.ServicioResutando;
import mx.gob.semarnat.mia.servicios.evaluacion.ServicioOficioNotiGobiernos;
import mx.gob.semarnat.mia.servicios.evaluacion.ServicioOpinionTecnicaProyecto;
import mx.gob.semarnat.model.catalogos.CatTratamientos;
import mx.gob.semarnat.model.catalogos.DelegacionMunicipio;
import mx.gob.semarnat.model.catalogos.EntidadFederativa;
import mx.gob.semarnat.model.dgira_miae.Document;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoOfiGobierno;
import mx.gob.semarnat.model.dgira_miae.Resultando;
import mx.gob.semarnat.model.seguridad.TokenGO;

@SuppressWarnings("serial")
@ManagedBean(name = "notiGobiernosBean")
@ViewScoped
public class NotiGobiernosBean extends BaseBean implements Serializable{

	private final String MOD_EDITAR = "Editar registro";
	private String bitacoraProyecto;
        private String bitacoraProyectoUrl;
        private String turnado;
	private String folioProyecto;
	private Integer idusuario;
	private String document_id;
	private String template;
	private List<UnidadAdministrativa> listaResultandos = new ArrayList<UnidadAdministrativa>();
	private List<UnidadAdministrativa> unidadesSelecionados = new ArrayList<UnidadAdministrativa>();
        
        private UnidadAdministrativa unidadSeleccionado = new UnidadAdministrativa();
        
        //PARA AGREGAR UNIDADES ADMINISTRATIVAS
        private List<EntidadFederativa> listEstados;
	private List<DelegacionMunicipio> listMunicipios;
	private List<CatTratamientos> listTratamientos;
        
        private List<SelectItem> itemsEstados;
	private List<SelectItem> itemsMunicipios;
	private List<SelectItem> itemsTratamientos;
	
        private List<SelectItem> itemsUnidadesAdministrativas;
	private int opinionTec;
        
	private String nombreDependencia;
	private String cargo;
	private String nombbreDestinatario;
	private long idSeleccionado;
        
        private String entidadSeleccionado;
        private String municipioSeleccionado;
	
	private String tituloModal;
	private boolean cargado = false;
	
	//servicios
	private ServicioUnidadAdministrativa sua;
	private ServicioOficioNotiGobiernos song;
	
	public NotiGobiernosBean() {
		
		inicializarBean();
	}
	
	private void inicializarBean() {
            FacesContext fContext = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("userFolioProy");
            idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
            document_id = req.getParameter("documentId");
            template = req.getParameter("template");
            bitacoraProyecto = req.getParameter("numBitacora");
            bitacoraProyectoUrl = req.getParameter("numBitacora");
            turnado = "true".equals(req.getParameter("turnado")) ? "1":"0";
            System.out.println("folioProyecto: " + folioProyecto);
            System.out.println("idusuario: " + idusuario);
            System.out.println("document_id: " + document_id);
            System.out.println("template: " + template);
            System.out.println("bitacoraProyecto: " + bitacoraProyecto);
            sua = new ServicioUnidadAdministrativa();

            listaResultandos = sua.buscarEntidadesNotiicacionGobierno(bitacoraProyecto);

            for (UnidadAdministrativa listaResultando : listaResultandos) {
                if (!(listaResultando.getNombreDependencia().length() > 0)) {
                    cargado = true;
                }                    
            }

            DgiraMiaeDaoGeneral miaeDao = new DgiraMiaeDaoGeneral();
            listEstados = miaeDao.getEstados();	
            itemsEstados = new ArrayList<>();
            for (EntidadFederativa entidadFederativa : listEstados) {
                SelectItem name = new SelectItem(entidadFederativa.getIdEntidadFederativa(), entidadFederativa.getNombreEntidadFederativa());
                itemsEstados.add(name);
            }
            listTratamientos = miaeDao.getTratamientos();
            itemsTratamientos = new ArrayList<>();
            for (CatTratamientos tratamientos : listTratamientos) {
                SelectItem name = new SelectItem(tratamientos.getId(), tratamientos.getDegree());
                getItemsTratamientos().add(name);
            }
        
            itemsUnidadesAdministrativas = new ArrayList<>();
            itemsUnidadesAdministrativas.add(new SelectItem(2, "Gobierno Estatal"));
            itemsUnidadesAdministrativas.add(new SelectItem(3, "Gobierno Municipal"));
	}

	public void abrirModalEditarRegistro(ActionEvent e) {
		System.out.println("abrirModalResultandoEditarRegistro");
		tituloModal = MOD_EDITAR;
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String idActividad = params.get("idSeleccion").toString();
		int position = Integer.parseInt(idActividad);
		System.out.println("Position: " + position);
		UnidadAdministrativa r = listaResultandos.get(position);
		limpiarFormulario();
		cargarValores(r);
		setCargado(true);
		RequestContext.getCurrentInstance().reset("formADUpdate:direcorioDetail");
		RequestContext.getCurrentInstance().execute("PF('dlogResultando').show();");
	}

        
        
	private void limpiarFormulario() {
		nombreDependencia = null;
		cargo = null;
		nombbreDestinatario = null;
		idSeleccionado = 0;
	}
	
	public void guardar(ActionEvent e) {
		System.out.println("Click en guardar");
		sua = new ServicioUnidadAdministrativa();
		
		try {
			UnidadAdministrativa item = sua.findById(idSeleccionado);
			System.out.println("----> " + item.getId());
			item.setNombbreDestinatario(nombbreDestinatario);
			System.out.println("----> " + item.getNombbreDestinatario());
			
			sua.saveUnidad(item);
			listaResultandos = sua.buscarEntidadesOpinionTecnica(bitacoraProyecto);
                        
                        for (UnidadAdministrativa listaResultando : listaResultandos) {
                            if (!(listaResultando.getNombreDependencia().length() > 0)) {
                                cargado = true;
                            }
                        }
                        
			RequestContext.getCurrentInstance().execute("PF('dlogResultando').hide();");
			addMessage("growl", FacesMessage.SEVERITY_INFO, "Registro editado exitosamente");
		} catch (MiaExcepcion e1) {
			e1.printStackTrace();
			// Fallo
			addMessage("growl", FacesMessage.SEVERITY_ERROR, "No se pudo guardar el registro");
		}
	}
	
	private void addMessage(String componentId, Severity severity, String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(componentId, new FacesMessage(severity, message, message));
	}
	
	private void cargarValores(UnidadAdministrativa r) {
		nombreDependencia = r.getNombreDependencia();
		cargo = r.getCargo();
		nombbreDestinatario = r.getNombbreDestinatario();
		idSeleccionado = r.getId();
                entidadSeleccionado = r.getEstadoId();
                municipioSeleccionado = r.getMunicipioId();
                
	}
	
	public void generaOficio(){
		sua = new ServicioUnidadAdministrativa();
		song = new ServicioOficioNotiGobiernos();
		////Marcar las dependencias seleccionadas
		for (UnidadAdministrativa unidadAdministrativa : unidadesSelecionados) {
			System.out.println(bitacoraProyecto);
			System.out.println(unidadAdministrativa.getId());
			try{
				ProyectoOfiGobierno pog = new ProyectoOfiGobierno(bitacoraProyecto, unidadAdministrativa.getId());
				System.out.println("--->" + pog.getBitacoraProyecto());
				song.guarda(pog);
				
				ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
				String goApiURL = servletContext.getInitParameter("goApi_url");
				String goURL = servletContext.getInitParameter("go_url");
				System.out.println("Generar Oficio: " + template);
				System.out.println("IdUsuario: " + idusuario);
				System.out.println("GoApi URL: " + goApiURL);
				RequestContext context = RequestContext.getCurrentInstance();
				context = RequestContext.getCurrentInstance();
				HttpURLConnection conn;
		
				OutputStream os;
				VisorDao daoVisor = new VisorDao();
				ServicioTokenGO servTkGo = new ServicioTokenGO();
				ServicioDocument servDoc = new ServicioDocument();
				Proyecto proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacoraProyecto);
				String nameProyect = proyecto != null ? proyecto.getProyNombre() : null;
				try {
					//valueUtf8NP = nameProyect != null ? new String(nameProyect.getBytes("UTF-8")) : null;
					String valueUtf8NP = bitacoraProyecto + "-" + new String(unidadAdministrativa.getNombreDependencia().getBytes("UTF-8"));
					Document doc = servDoc.getDocByIdByType(document_id, folioProyecto);
					TokenGO token = servTkGo.getToken(idusuario);
					if (token != null && (doc == null || doc.id == 0)) {
		
						URL url = new URL(goApiURL + "/expedients");
						conn = (HttpURLConnection) url.openConnection();
						conn.setDoOutput(true);
						conn.setRequestMethod("POST");
						conn.setRequestProperty("Content-Type", "application/json");
						conn.setRequestProperty("x-user-token", token.getToken());
						String input = "{\"expedient\":{\"project_id\":\"" + bitacoraProyecto + "\",\"name\":\"" + template + " - " + valueUtf8NP
								+ "\",\"body\":null,\"header\":null,\"footer\":null,\"document_id\":\"" + document_id + "\",\"assignee_id\":null,\"discussions_attributes\":[]}}";
		
						System.out.println("json ---->>> " + input);
						os = conn.getOutputStream();
						os.write(input.getBytes());
						os.flush();
						System.out.println("os" + os.toString() + " conn msg:>_ " + conn.getResponseMessage() + " to string:>_ " + conn.toString());
						os.close();
						System.out.println("POST Response Code :: " + conn.getResponseCode());
						if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
							throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
						}
						BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
						String id = "";
						String output;
						while ((output = br.readLine()) != null) {
							JSONParser parser = new JSONParser();
							JSONObject json = (JSONObject) parser.parse(output);
							json = (JSONObject) parser.parse(json.get("expedient").toString());
							System.out.println(json.get("id"));
							id = json.get("id").toString();
						}
						conn.disconnect();
		
						URL urlE = new URL(goURL + "/editor/expedient/" + id + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
						System.out.println("url :: " + urlE.toString());
						context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP + "')");

						song.elemina(pog);
					} else {
						if (doc != null) {
							URL urlE = new URL(goURL + "/editor/expedient/" + doc.getId().longValue() + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
							System.out.println("url :: " + urlE.toString());
							context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP + "')");
						}
					}
		
				} catch (MiaExcepcion e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}catch(MiaExcepcion ex){
				ex.printStackTrace();
			}
		}	
	}
	
	public void generaOficioUnico(){
		sua = new ServicioUnidadAdministrativa();
		song = new ServicioOficioNotiGobiernos();
		////Marcar las dependencias seleccionadas
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String idActividad = params.get("idSeleccion").toString();
		int position = Integer.parseInt(idActividad);
		System.out.println("Position: " + position);
		UnidadAdministrativa unidadAdministrativa = listaResultandos.get(position);
			System.out.println(bitacoraProyecto);
			System.out.println(unidadAdministrativa.getId());
			try{
				ProyectoOfiGobierno pog = new ProyectoOfiGobierno(bitacoraProyecto, unidadAdministrativa.getId());
				System.out.println("--->" + pog.getBitacoraProyecto());
				song.guarda(pog);
				
				ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
				String goApiURL = servletContext.getInitParameter("goApi_url");
				String goURL = servletContext.getInitParameter("go_url");
				System.out.println("Generar Oficio: " + template);
				System.out.println("IdUsuario: " + idusuario);
				System.out.println("GoApi URL: " + goApiURL);
				RequestContext context = RequestContext.getCurrentInstance();
				context = RequestContext.getCurrentInstance();
				HttpURLConnection conn;
		
				OutputStream os;
				VisorDao daoVisor = new VisorDao();
				ServicioTokenGO servTkGo = new ServicioTokenGO();
				ServicioDocument servDoc = new ServicioDocument();
				Proyecto proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacoraProyecto);
				String nameProyect = proyecto != null ? proyecto.getProyNombre() : null;
				try {
					//valueUtf8NP = nameProyect != null ? new String(nameProyect.getBytes("UTF-8")) : null;
					String valueUtf8NP = bitacoraProyecto + "-" + new String(unidadAdministrativa.getNombreDependencia().getBytes("UTF-8"));
					Document doc = servDoc.getDocByIdByType(document_id, folioProyecto);
					TokenGO token = servTkGo.getToken(idusuario);
					if (token != null && (doc == null || doc.id == 0)) {
						int idDoc = position + 1;
						URL url = new URL(goApiURL + "/expedients");
						conn = (HttpURLConnection) url.openConnection();
						conn.setDoOutput(true);
						conn.setRequestMethod("POST");
						conn.setRequestProperty("Content-Type", "application/json");
						conn.setRequestProperty("x-user-token", token.getToken());
						String input = "{\"expedient\":{\"project_id\":\"" + bitacoraProyecto + "\",\"name\":\"" + template + " - " + valueUtf8NP
								+ "\",\"body\":null,\"header\":null,\"footer\":null,\"document_id\":\"" + document_id + "\",\"document_web\":\"" + idDoc + "\",\"assignee_id\":null,\"discussions_attributes\":[]}}";
		
						System.out.println("json ---->>> " + input);
						os = conn.getOutputStream();
						os.write(input.getBytes());
						os.flush();
						System.out.println("os" + os.toString() + " conn msg:>_ " + conn.getResponseMessage() + " to string:>_ " + conn.toString());
						os.close();
						System.out.println("POST Response Code :: " + conn.getResponseCode());
						if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
							throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
						}
						BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
						String id = "";
						String output;
						while ((output = br.readLine()) != null) {
							JSONParser parser = new JSONParser();
							JSONObject json = (JSONObject) parser.parse(output);
							json = (JSONObject) parser.parse(json.get("expedient").toString());
							System.out.println(json.get("id"));
							id = json.get("id").toString();
						}
						conn.disconnect();
		
						URL urlE = new URL(goURL + "/editor/expedient/" + id + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
						System.out.println("url :: " + urlE.toString());
						context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP + "')");

						song.elemina(pog);
					} else {
						if (doc != null) {
							URL urlE = new URL(goURL + "/editor/expedient/" + doc.getId().longValue() + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
							System.out.println("url :: " + urlE.toString());
							context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP + "')");
						}
					}
		
				} catch (MiaExcepcion e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}catch(MiaExcepcion ex){
				ex.printStackTrace();
			}	
	}
	
        public void onEstadoChange() {
		DgiraMiaeDaoGeneral miaeDao = new DgiraMiaeDaoGeneral();
		System.out.println("estado seleccionado :::: " + unidadSeleccionado.getEstadoId());
		listMunicipios = miaeDao.getMunicipios(unidadSeleccionado.getEstadoId());
		itemsMunicipios = new ArrayList<>();
		for (DelegacionMunicipio municipio : listMunicipios) {
			SelectItem select = new SelectItem(municipio.getIdDelegacionMunicipio(), municipio.getNombreDelegacionMunicipio());
			itemsMunicipios.add(select);
		}
		System.out.println("Municipios :::::::::::::::::::::: " + itemsMunicipios);
	}
        
        public void validarDatos() {
            if (unidadSeleccionado.getNombreDependencia().equals(null) || unidadSeleccionado.getNombreCorto().equals(null) ||
                unidadSeleccionado.getDomicilio().equals(null) || unidadSeleccionado.getNombbreDestinatario().equals(null) ||
                unidadSeleccionado.getCargo().equals(null) || unidadSeleccionado.getTelefono().equals(null) ||
                unidadSeleccionado.getCorreo().equals(null)){
    //    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Llena todos los campos"));
                System.out.print("faltan datos");
            } else {
            System.out.print("guardado");
                agregarRegistro();   		   		
            }
        }
        
        public void agregarRegistro(){
            try {
                sua = new ServicioUnidadAdministrativa();
                System.out.println("unidadSeleccionado ----->>>" + unidadSeleccionado);			
                unidadSeleccionado.setEstado("activo");
                unidadSeleccionado.setOpinionTec(opinionTec);
                sua.saveUnidad(unidadSeleccionado);
                FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Exito",  "se guardo exitosamente el registro") );
	        RequestContext.getCurrentInstance().execute("PF('dlgAddUpdate').hide()");
	        unidadSeleccionado = new UnidadAdministrativa();
	        listaResultandos = sua.buscarEntidadesNotiicacionGobierno(bitacoraProyecto);	        
            } catch (MiaExcepcion e) {			
                e.printStackTrace();
            }
	}
        
	public List<UnidadAdministrativa> getListaResultandos() {
		return listaResultandos;
	}

	public void setListaResultandos(List<UnidadAdministrativa> listaResultandos) {
		this.listaResultandos = listaResultandos;
	}

	public String getNombreDependencia() {
		return nombreDependencia;
	}

	public void setNombreDependencia(String nombreDependencia) {
		this.nombreDependencia = nombreDependencia;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getNombbreDestinatario() {
		return nombbreDestinatario;
	}

	public void setNombbreDestinatario(String nombbreDestinatario) {
		this.nombbreDestinatario = nombbreDestinatario;
	}

	public String getTituloModal() {
		return tituloModal;
	}

	public void setTituloModal(String tituloModal) {
		this.tituloModal = tituloModal;
	}

	public List<UnidadAdministrativa> getUnidadesSelecionados() {
		return unidadesSelecionados;
	}

	public void setUnidadesSelecionados(List<UnidadAdministrativa> unidadesSelecionados) {
		this.unidadesSelecionados = unidadesSelecionados;
	}

    /**
     * @return the unidadSeleccionado
     */
    public UnidadAdministrativa getUnidadSeleccionado() {
        return unidadSeleccionado;
    }

    /**
     * @param unidadSeleccionado the unidadSeleccionado to set
     */
    public void setUnidadSeleccionado(UnidadAdministrativa unidadSeleccionado) {
        this.unidadSeleccionado = unidadSeleccionado;
    }

    /**
     * @return the itemsEstados
     */
    public List<SelectItem> getItemsEstados() {
        return itemsEstados;
    }

    /**
     * @param itemsEstados the itemsEstados to set
     */
    public void setItemsEstados(List<SelectItem> itemsEstados) {
        this.itemsEstados = itemsEstados;
    }

    /**
     * @return the itemsMunicipios
     */
    public List<SelectItem> getItemsMunicipios() {
        return itemsMunicipios;
    }

    /**
     * @param itemsMunicipios the itemsMunicipios to set
     */
    public void setItemsMunicipios(List<SelectItem> itemsMunicipios) {
        this.itemsMunicipios = itemsMunicipios;
    }

    /**
     * @return the itemsTratamientos
     */
    public List<SelectItem> getItemsTratamientos() {
        return itemsTratamientos;
    }

    /**
     * @param itemsTratamientos the itemsTratamientos to set
     */
    public void setItemsTratamientos(List<SelectItem> itemsTratamientos) {
        this.itemsTratamientos = itemsTratamientos;
    }

    /**
     * @return the entidadSeleccionado
     */
    public String getEntidadSeleccionado() {
        return entidadSeleccionado;
    }

    /**
     * @param entidadSeleccionado the entidadSeleccionado to set
     */
    public void setEntidadSeleccionado(String entidadSeleccionado) {
        this.entidadSeleccionado = entidadSeleccionado;
    }

    /**
     * @return the municipioSeleccionado
     */
    public String getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    /**
     * @param municipioSeleccionado the municipioSeleccionado to set
     */
    public void setMunicipioSeleccionado(String municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    /**
     * @return the cargado
     */
    public boolean isCargado() {
        return cargado;
    }

    /**
     * @param cargado the cargado to set
     */
    public void setCargado(boolean cargado) {
        this.cargado = cargado;
    }

    /**
     * @return the itemsUnidadesAdministrativas
     */
    public List<SelectItem> getItemsUnidadesAdministrativas() {
        return itemsUnidadesAdministrativas;
    }

    /**
     * @param itemsUnidadesAdministrativas the itemsUnidadesAdministrativas to set
     */
    public void setItemsUnidadesAdministrativas(List<SelectItem> itemsUnidadesAdministrativas) {
        this.itemsUnidadesAdministrativas = itemsUnidadesAdministrativas;
    }

    /**
     * @return the opinionTec
     */
    public int getOpinionTec() {
        return opinionTec;
    }

    /**
     * @param opinionTec the opinionTec to set
     */
    public void setOpinionTec(int opinionTec) {
        this.opinionTec = opinionTec;
    }

    public String getBitacoraProyectoUrl() {
        String temp = bitacoraProyectoUrl.replaceAll("/", "%2f");
        return temp;
    }

    public void setBitacoraProyectoUrl(String bitacoraProyectoUrl) {
        this.bitacoraProyectoUrl = bitacoraProyectoUrl;
    }

    public String getTurnado() {
        return turnado;
    }

    public void setTurnado(String turnado) {
        this.turnado = turnado;
    }
	
	
}
