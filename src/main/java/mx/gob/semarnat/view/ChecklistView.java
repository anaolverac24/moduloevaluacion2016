package mx.gob.semarnat.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.BitacoraRequisitos;
import mx.gob.semarnat.dao.CadenaFirmaDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.DirectorDao;
import mx.gob.semarnat.dao.LoginDAO;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.dao.procedures.RequisitosDataModel;
import mx.gob.semarnat.dao.procedures.SegundasVias;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.CadenaFirma;
import mx.gob.semarnat.model.bitacora.CadenaFirmaLog;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.RequisitosSubdirector;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbusuario;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.secure.AppSession;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import mx.gob.semarnat.ws.WSRecuperaArchivos;
import net.firel.BcAgregaCadenaRespuesta;
import net.firel.DetallesFirmado;
import net.firel.Firmas;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.itextpdf.text.DocumentException;

@SuppressWarnings("serial")
@ManagedBean(name = "chklstView")
@ViewScoped
public class ChecklistView implements Serializable {
    private static final Logger logger = Logger.getLogger(ChecklistView.class.getName());

    /**
     * @return the daoDg
     */
    public DgiraMiaeDaoGeneral getDaoDg() {
        return new DgiraMiaeDaoGeneral();
    }

//    /**
//     * @param aDaoDg the daoDg to set
//     */
//    public static void setDaoDg(DgiraMiaeDaoGeneral aDaoDg) {
//        
////        daoDg = aDaoDg;
//    }
    @ManagedProperty ( value = "#{detalleView}")
    private DetalleProyectoView detalleView;
    private final DirectorDao dao = new DirectorDao();
    private DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
    private final BitacoraRequisitos bq;

    private String bitaProyecto;
    private String bitaProyOfic;
    private String rutaImg;
    private Boolean oficioPrev = false;
    private String folio;
    private short serialProyecto;
    // valor 0 o 1 que indica si esta bitacora (pantalla de checklist) biene de una invocación del DG
    private int isUrlDG = 0;
    // valor 1 o 2 que indica si es la primer bandeja o segunda bandeja del DG ( si no es del dg ni si quiera se trae el valor en los parametros de la url 
    private int bandeja = 1; // por defecto es la bandeja 1
    

    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<ListaRequisitos> requisitos = new ArrayList();
	@SuppressWarnings({ "unchecked", "rawtypes" })
    private List<ListaRequisitos> requisitosImg = new ArrayList();
    private List<RequisitosSubdirector> tblReq;

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private List<ListaRequisitos> requisitosSi = new ArrayList();
    private String observaciones;
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private List<ListaRequisitos> valoresDT = new ArrayList();
    private Object[] objectArray;

    private RequisitosDataModel reqDM;
    private Integer bandera;

    private String idArea;
    private Integer idusuario;
    private Historial bitacoraAT0022;
    private Historial bitacoraI00010;
    private Bitacora bitacora2; 
    private Bitacora bitacora;
    private SinatDgira sinatDgira;
    private final BitacoraDao daoBitacora = new BitacoraDao();

    private Integer counReqSi = 0;
    private Integer counReqSiGuard = 0;
    private Boolean oficPrev = false;
    private Integer pagoDere;
    private Integer checkList;
    private Integer aux;

    private String cveTipTram;
    private Integer idTipTram;
    private String edoGestTram;

    //manejo de turnado de oficio de checkList  
    private List<Object[]> documentoRespuestaDgira = new ArrayList<Object[]>();
    private Boolean turnaDocs = false;
    private List<CatEstatusDocumento> subSectorCat; //
    private DocumentoDgiraFlujoHist docsDgiraFlujoHist;
    private DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
    private DocumentoDgiraFlujo docsDgiraFlujo;
    private HttpSession sesion = Util.getSession();
    private short idDocumento = 0;
    private String comentarioTurnado = "";
    private String areaEnvio = null;
    private short tipoDoc = 4;
    private String tipoDocId = "";
    private List<Object[]> maxDocsDgiraFlujoHist = new ArrayList<Object[]>();
    private CatEstatusDocumento catEstatusDoc;
    private Integer idAux = 0;
    private short maxIdHisto = 0;
    private Boolean turnaDocsBtn = false;
    private Boolean corregirDocsBtn = false;
    private List<Object[]> DocRespDgira = new ArrayList<Object[]>();
    private List<Object[]> statusDocRespDgira = new ArrayList<Object[]>();
    private Boolean turnaDocsBtnP = false;
    private Tbarea tbArea;
    @SuppressWarnings("unused")
	private Tbarea tbAreaEnvio;
    private String perfilSig;
    private String URLgenOficPre;
    private String URLdb;
	@SuppressWarnings("unused")
	private Integer statusDoc;
    private List<Object[]> areaResiveDocCon = new ArrayList<Object[]>();
	private String jerarquia;
        
    DocumentoRespuestaDgira documento = new DocumentoRespuestaDgira();
        
    private String nombreOfic;
    private String urlOfic;
    @SuppressWarnings("unused")
	private String statusProy;

    private SegundasVias segundasVias = new SegundasVias();

    private SelectItem[] combo;

    private String turnaEva;//guarda desición de si se tuena a evaluación o se previene el trámite
    @SuppressWarnings("unused")
	private Historial bitaSiNoIntegexp;
    private Boolean muestraPegTurnado = false;
    private Boolean muestraSec = false;
    
    private Boolean dePrevenc = false; //bandera que indica si un trámite ya vino de una reactivación de trámite por prevención o de una suspención por info adicional
    private Boolean desechamiento = false; //bandera que indica si un tramite ya viene en la segunda vuelta
    private List<Historial> HistorialCIS303;
    
    private List<Historial> NoHistorialAT0022; 
    private List<Historial> NoHistorialCIS106;
    private List<Historial> NoHistorial200501;
    
    private List<Historial> HistorialI00010;
    private List<Historial> NoHistorialIRA020;
    private List<Historial> NoHistorialAT0027;
    
    private Boolean tramiteYaTurnado= false; //bandera que indica si un tramite ya fue turnado, hablando del listado de proyectos, seran los proyectos de la segunda tabla (proyectos ya turnados)
    private Boolean crearResolutivo = false;  //bandera que indica si se puede general información adicional
    private Boolean crearResolutivo2 = false; //bandera que indica si se puede general un documento de respuesta de conclusión de trámite (Negado, desechado, aprovado)
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<ListaRequisitos> requisitosNull = new ArrayList();
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<ListaRequisitos> requisitosNo = new ArrayList();
    
    private boolean digital;
    
    private BigDecimal bitamonto;
    public EvaluacionProyecto eval;
    private Double montoCalc = 0.0;
    private Double bitamontoDouble = 0.0;
    private String nombTipoOfic="";
    private List<Object[]> comenatriosTab = new ArrayList<>();
    
    private StreamedContent file;
    @SuppressWarnings("unused")
	private String siFirmar;
    
    Tbusuario usuariorecibe = null;
    String usuariorecibetxt = "";
    
    private Tbarea tbarea = new Tbarea();
    private String idEntidad = "";
    
    
    // variables para firma del DG
    private int tipoSuspencion=0; 
    private int tipoApertura = 0;
    
    @SuppressWarnings("rawtypes")
	public ChecklistView() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        System.out.println("\n\nChecklistView...");
        
        this.bq = new BitacoraRequisitos();
        FacesContext fContext = FacesContext.getCurrentInstance();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
        Map params = ec.getRequestParameterMap();
        
        tbarea = (Tbarea) sesion.getAttribute("areaDelegacionUsuario");
        idEntidad = tbarea.getIdentidadfederativa();

        // esta informacion tiene valores en la sesion cuando lo invoca el dg, en otros roles no biene en la url por tanto en detalleproyectoview no se cachan los parametros de la url
        isUrlDG = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("isUrlDG").toString());
        bandeja = Integer.parseInt(fContext.getExternalContext().getSessionMap().get("bandeja").toString());
        
        System.out.println("isUrlDG: " + isUrlDG);
        System.out.println("bandeja: " + bandeja);
        
        if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
        {
        	try{
            bitaProyecto = params.get("bitaNum").toString();
        	}catch(NullPointerException exnull){
        		bitaProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        	}
            
            //muestraSec
            try
            {
                if (params.get("tipoOfic").toString().equals("7"))
                {
                    muestraSec = true;
                }
                else
                {
                    muestraSec = false;
                }
            }
            catch(Exception mt)
            {
            	System.out.println("\nError al obtener la bitacora....");
            }
        } else {
            bitaProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
            tramiteYaTurnado = (Boolean) fContext.getExternalContext().getSessionMap().get("tramiteYaTurnado");
            
            System.out.println("tramiteYaTurnado: " + tramiteYaTurnado);
        }
        System.out.println("bitaProyecto: " + bitaProyecto);

        if (bitaProyecto != null) {   //colocalar datos de la bitacora en sesión (clave de tramite, id de tramite y estado de gestion del tramite)
            cveTipTram = daoBitacora.cveTipTram(bitaProyecto);
            idTipTram = daoBitacora.idTipTram(bitaProyecto);
            edoGestTram = daoBitacora.entGestTram(bitaProyecto);
            bitacora = daoBitacora.datosBitaInd(bitaProyecto);
            //JOptionPane.showMessageDialog(null,  "cveTipTram: " + cveTipTram + "  idTipTram: " + idTipTram + "  edoGestTram: " + edoGestTram, "  Exito", JOptionPane.INFORMATION_MESSAGE);
            if (!cveTipTram.isEmpty() && !edoGestTram.isEmpty()) {
                fContext.getExternalContext().getSessionMap().put("cveTramite", cveTipTram);
                fContext.getExternalContext().getSessionMap().put("idTramite", idTipTram);
                fContext.getExternalContext().getSessionMap().put("edoGestTramite", edoGestTram);
            }
        }
        //fContext.getExternalContext().getSessionMap().get("userFolioProy");
        idArea = (String) fContext.getExternalContext().getSessionMap().get("idAreaUsu");
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");

        cveTipTram = (String) fContext.getExternalContext().getSessionMap().get("cveTramite");
        idTipTram = (Integer) fContext.getExternalContext().getSessionMap().get("idTramite");
        edoGestTram = (String) fContext.getExternalContext().getSessionMap().get("edoGestTramite");
        
        LoginDAO loginDao = new LoginDAO();
        usuariorecibe = loginDao.getUsuario(idusuario);
	    if(usuariorecibe != null){
	        	usuariorecibetxt = usuariorecibe.getIdempleado().getNombre() + " " + usuariorecibe.getIdempleado().getApellidopaterno() + " " + usuariorecibe.getIdempleado().getApellidomaterno();
	        System.out.println("Usuario recibe: " + usuariorecibetxt);
	    }else{
	        	usuariorecibetxt = "";
	        	System.out.println("Usuario recibe: NULL");
	    }
        
        
        if ((fContext.getExternalContext().getSessionMap().get("userFolioProy") != null) ) 
        {
            folio = (String) fContext.getExternalContext().getSessionMap().get("userFolioProy");
            serialProyecto = (Short) fContext.getExternalContext().getSessionMap().get("userSerialProy");
        }
        
        traerBitacora(bitaProyecto, folio, cveTipTram, idTipTram);
        this.digital = bq.isDigital();

        if (sesion != null) {   //JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
        	// pool DG
//            if (!sesion.getAttribute("rol").toString().equals("dg")) //Director General
//            {
//                if (!sesion.getAttribute("rol").toString().equals("dg")) 
//                {                   
                    //JOptionPane.showMessageDialog(null,  "chkList:  " + checkList + "  pago: " + pagoDere , "Error", JOptionPane.INFORMATION_MESSAGE);
                    List<Object[]> data;
                    data = this.digital ? dao.getReqTramiteDigital(bitaProyecto) : dao.getReqTramite(bitaProyecto);
                    for (Object[] req : data) {
                        requisitos.add(new ListaRequisitos(req));
                    }
                    data = this.digital ? dao.getReqTramiteSIDigital(bitaProyecto) : dao.getReqTramiteSI(bitaProyecto);
                    for (Object[] reqSi : data) {
                        requisitosSi.add(new ListaRequisitos(reqSi));
                    }
                    for (ListaRequisitos a : requisitos) {
                        if (Integer.parseInt(a.getO()[3].toString()) == 0) { //si entrego requisito en ECC
                            rutaImg = "/resources/images/crossmark.png";
                            if (a.getO()[5]!= null && a.getO()[5].toString().isEmpty()) {
                                observaciones = "";
                                requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg));
                                System.out.println(a.getO()[0] + "," + a.getO()[1] + "," + a.getO()[2] + "," + a.getO()[3] + "," + a.getO()[4] + "," + a.getO()[5] + "," + observaciones + rutaImg);
                            } else {
                                observaciones = a.getO()[5] == null?"":a.getO()[5].toString();
                                requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg));
                                System.out.println(a.getO()[0] + "," + a.getO()[1] + "," + a.getO()[2] + "," + a.getO()[3] + "," + a.getO()[4] + "," + a.getO()[5] + "," + observaciones + rutaImg);
                            }

                        } else { //para los requisitos que si se entegaron en ECC
                            rutaImg = "/resources/images/checkmark.png";
                            if (a.getO()[5] == null) { //cuando no se tiene observaciones en los requisitos
                                observaciones = "";
                                requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg)); //marca con una paloma las requisitos que si fueron entregados en el ECC
                                System.out.println(a.getO()[0] + "," + a.getO()[1] + "," + a.getO()[2] + "," + a.getO()[3] + "," + a.getO()[4] + "," + a.getO()[5] + "," + observaciones + rutaImg);
                            } else {
                                observaciones = a.getO()[5].toString();
                                requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg));
                                System.out.println(a.getO()[0] + "," + a.getO()[1] + "," + a.getO()[2] + "," + a.getO()[3] + "," + a.getO()[4] + "," + a.getO()[5] + "," + observaciones + rutaImg);
                            }
                        }
                    }
                    
                    //
                    data = this.digital ? dao.getReqTramiteNullDigital(bitaProyecto) : dao.getReqTramiteNull(bitaProyecto);
                    for (Object[] reqNull : data) {
                        requisitosNull.add(new ListaRequisitos(reqNull));
                    }
                    data = this.digital ? dao.getReqTramiteNoDigital(bitaProyecto) : dao.getReqTramiteNo(bitaProyecto);
                    for (Object[] reqNo : data) {
                        requisitosNo.add(new ListaRequisitos(reqNo));
                    }  
//                }
//            }
        }

        //cargar lista con estatus de documentos
        if (sesion != null) {   //JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
            if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
            {
                subSectorCat = daoDg.getCatStatusDocs();
            }
            if (sesion.getAttribute("rol").toString().equals("da")) //Director de Ã�rea
            {
                subSectorCat = daoDg.getCatStatusDocs2();
            }
            if (sesion.getAttribute("rol").toString().equals("sd")) //Sub Director
            {
                subSectorCat = daoDg.getCatStatusDocs3();
            }
            if (sesion.getAttribute("rol").toString().equals("eva")) //Evaluador
            {
                subSectorCat = daoDg.getCatStatusDocs2();
            }
        }
        
        //verificar que tipo de oficio se va a generar asi como saber si se muestra la opción de modificar el Oficio o Solo el PDF
        verificaTipoOficio();
        

        System.out.println("crearResolutivo: " + crearResolutivo);
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }
    
    /***
     * verificar que tipo de oficio se va a generar asi como saber si se muestra la opción de modificar el Oficio o Solo el PDF
     */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void verificaTipoOficio()
    {
        
        if (sesion != null) 
        {   
        	   // pool DG
//                if (!sesion.getAttribute("rol").toString().equals("dg")) //Director General
//                {
        
                    //counReqSi = guarada la cuenta de los requisitos que traen comentario e BD y no son registros nuevos
                    //counReqSiGuard = guarda los texboxs que estan vacios
                    //verificar si existen datos en el pago de derechos y checkList para levantar bandera y poder generar oficio de prevención
                    List<Object[]> criterios = (List<Object[]>) daoDg.critValProyEva(folio, serialProyecto);
                    
					List<RequisitosSubdirector> chkList = (List<RequisitosSubdirector>) daoDg.lista_namedQuery("RequisitosSubdirector.findByReqsdBitacora", new Object[]{bitaProyecto}, new String[]{"reqsdBitacora"});

                    bitamonto = daoBitacora.montoVEX(bitaProyecto); //monto promovente
                    if(bitamonto != null)
                    { bitamontoDouble = bitamonto.doubleValue();  }

                    String montoEva = "";
                    montoEva = daoDg.getCostoSDPagDerec(bitaProyecto);
                    if(montoEva.length()>0)
                    {
                        try{
                            montoEva = montoEva.replace("[", "").replace("]", "");
                            montoCalc = Double.parseDouble(montoEva);
                        }
                        catch(Exception pagDer)
                        {
                            System.out.println("monto: " + pagDer.getMessage());
                            System.out.println("monto: " + pagDer.getLocalizedMessage());
                        }
                    }
                    
                    if (!criterios.isEmpty()) {
                        for (Object o : criterios) {
                            if(o == null)
                            { aux = 0;}
                            else
                            { aux = Integer.parseInt(o.toString()); }
                        }
                        //JOptionPane.showMessageDialog(null,  "33xxx:  " + aux , "Error", JOptionPane.INFORMATION_MESSAGE);
                        if (aux == 0) {
                            pagoDere = 0; //pago de derechos vacios
                        } else {
                            //si el usuario lleno el formulario de pago de derechos se procede a comparar el monto del promovente y el monto calculado por el SD
                            if(montoCalc > bitamontoDouble)
                            {                                 
                                pagoDere = 1; //pago de derechos de promovente es menor o igual al calculado pr el evaluador
                            }
                            else
                            {
                                pagoDere = 0; //pago de derechos correcto
                            }                            
                        }
                    }
                    System.out.println("Monto evaluador: " + montoCalc + "  monto promovente: " + bitamontoDouble);
                    
                    List<Object[]> data;
                    requisitos = new ArrayList();
                    data = this.digital ? dao.getReqTramiteDigital(bitaProyecto) : dao.getReqTramite(bitaProyecto);
                    for (Object[] req : data) {
                        requisitos.add(new ListaRequisitos(req));
                    }

                    if (chkList.size() > 0 && (requisitos.size() == requisitosSi.size()) && (requisitos.size() > 0 && requisitosSi.size() > 0)) {
                        checkList = 0; //checkList lleno, sin ninguna observación
                    } else {
                        checkList = 1;
                    }

                    if(pagoDere == 1 && checkList == 1) //si el checklist y el pago de derechos no estan llenos, se generara un oficio de ambos
                    { tipoDoc = 5;  oficPrev = true; desechamiento = false;}
                    if(pagoDere == 1 && checkList == 0) //si el checklist esta bien y el pago de derechos esta mal, se generara un oficio de pago de derechos
                    { tipoDoc = 3;  oficPrev = true; desechamiento = false;}
                    if(pagoDere == 0 && checkList == 1) //si el checklist esta mal y el pago de derechos esta bien, se generara un oficio de pago de checkList
                    { tipoDoc = 4;  oficPrev = true; desechamiento = false;}
                    if(pagoDere == 0 && checkList == 0) //si el checklist esta bien y el pago de derechos esta bien, NO se generara un oficio
                    { tipoDoc = 0; dePrevenc = true; desechamiento = true;}  
                    
                    //else { oficPrev = false; }
                    System.out.println("pago de derechos: " + pagoDere + "  checkList: " +checkList);
                    System.out.println("tipo oficio checkList: " + tipoDoc);

                    // pool delegaciones
                    NoHistorialAT0027 = daoBitacora.numRegHistStatus(bitaProyecto,"AT0027");
                    if(tipoDoc == 0 && NoHistorialAT0027.size() == 2)//quiere decir que en algun moento se previno por checkList o por pago de derechos pero al regresar de la reactivaciÃ³n
                    //si subsano sus fallas y por ello no se detecta que tipo de oficio trae, por lo cual se perguntarra por oficio 5 o 4
                    {
                        DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, (short)4, edoGestTram, "1");
                        if(DocRespDgira.size()>0)
                        { tipoDoc = 4;}
                        DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, (short)5, edoGestTram, "1");
                        if(DocRespDgira.size()>0)
                        { tipoDoc = 5;}
                        DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, (short)3, edoGestTram, "1");
                        if(DocRespDgira.size()>0)
                        { tipoDoc = 3;}
                        DocRespDgira = null;
                        System.out.println("tipo oficio checkList despues de reactivación de prevención: " + tipoDoc);
                    }
                    
      

                    //boton de Turnar Oficio
                    try {
                        DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
                      

                    } catch (Exception er33) {
                        //JOptionPane.showMessageDialog(null, "idDocumento:  " + er33.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                    }

                    if (DocRespDgira.size() > 0) {
                        if(tipoDoc == 4)
                        { nombTipoOfic="Prevención por CheckList"; }
                        if(tipoDoc == 3)
                        { nombTipoOfic="Prevención por Pago de derechos"; }
                        if(tipoDoc == 5)
                        { nombTipoOfic="Prevención por CheckList y Pago de derechos"; }
                        
                        List<DocumentoRespuestaDgira> lista;
                        lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
                        for (DocumentoRespuestaDgira x : lista) {
                            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
                            urlOfic = x.getDocumentoUrl();
                            documento = x;
                        }
                        for (Object o : DocRespDgira) {
                            URLdb = o.toString();
                        }
                    } else {

                        URLdb = "";
                    }
                    
//                    if(sesion.getAttribute("rol").toString().equals("sd")){
//                    	corregirDocsBtn = false;
//                    }else if(sesion.getAttribute("rol").toString().equals("da")){
//                    	corregirDocsBtn = false;  // aqui va true pero para no mostrar por lo pronto pongo false
//                    }
                    
                    //JOptionPane.showMessageDialog(null, "turnaDocsBtnP :  " + turnaDocsBtnP , "Error", JOptionPane.INFORMATION_MESSAGE); 
                    //----------------------verificar si el boton para turnar los  oficios, el que se encinetra dentro del apartado Turnado de Oficio    
                    idAux = 0;
                    maxIdHisto = 0;
                    //se verifica el id mÃ¡ximo de oficio a tratar (en este caso el tipo de prevención=4)
                    try {
                        maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");

                        for (Object o : maxDocsDgiraFlujoHist) {
                            idAux = Integer.parseInt(o.toString());
                            maxIdHisto = idAux.shortValue();
                        }
                    } catch (Exception xxx) {
                        //JOptionPane.showMessageDialog(null, "idDocumento:  " + xxx.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                    }
                    if (maxIdHisto > 0) {   //trae todo lo relaionado al id mÃ¡ximo de tabla DOCUMENTO_DGIRA_FLUJO_HIST, relativo a una bitacora
                        docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);
                        statusProy = daoBitacora.estatusTramBita(bitaProyecto);
                        try {
                            //estatus del oficio en cuestión 
                            statusDocRespDgira = daoDg.statusDocsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);
                        } catch (Exception er) {
                            //JOptionPane.showMessageDialog(null, "idDocumento:  " + er.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }

                        for (Object o : statusDocRespDgira) {
                            statusDoc = Integer.parseInt(o.toString());
                        }
                    }
                    //JOptionPane.showMessageDialog(null,  "area envio: " + docsDgiraFlujoHistEdit.getIdAreaEnvioHist() + "   maxIdHisto: " + maxIdHisto , "Error", JOptionPane.INFORMATION_MESSAGE);

                    // pool delegaciones
                    // HistorialAT0001  dePrevenc
                    //HistorialAT0001 = daoBitacora.numRegHistStatus(bitaProyecto,"AT0001");
                    HistorialCIS303 = daoBitacora.numRegHistStatus(bitaProyecto,"CIS303");
                    NoHistorialAT0022 = daoBitacora.numRegHistStatus(bitaProyecto,"AT0022");
                    NoHistorialCIS106 = daoBitacora.numRegHistStatus(bitaProyecto,"CIS106"); 
                    NoHistorial200501 = daoBitacora.numRegHistStatus(bitaProyecto,"200501");
                    NoHistorialIRA020 = daoBitacora.numRegHistStatus(bitaProyecto,"IRA020");
                    

                    if(bitacora != null)
                    {   // si el trámite se encuentra actualmente en evaluación, se puede general info adicional, excepto cuando ya viene de una reactivación por info adicional
                        
                        comenatriosTab = daoBitacora.tablabOsevOfic(bitaProyecto, tipoDoc, "1", idEntidad);
                        if(bitacora.getBitaSituacion().trim().equals("AT0003") && NoHistorialIRA020.isEmpty() && HistorialCIS303.isEmpty())
                        { crearResolutivo = true; }
                        else
                        { crearResolutivo = false; }
                        //crearResolutivo2
                        // si el trámite se encuentra actualmente en evaluación, se puede general suna resolución o documento de respuesta
                        if(bitacora.getBitaSituacion().trim().equals("AT0003") && !tramiteYaTurnado) //  && !tramiteYaTurnado && (!NoHistorialIRA020.isEmpty() || !HistorialCIS303.isEmpty())
                        { crearResolutivo2 = true; }
                        else
                        { crearResolutivo2 = false; }
                        System.out.println("situación actual: " + bitacora.getBitaSituacion().trim().equals("AT0003") + "   tramiteYaTurnado: " + tramiteYaTurnado + "   crearResolutivo2: " + crearResolutivo2);
                    }
                    if(NoHistorialCIS106.size() == 1){
                    	desechamiento = true;
                    }else {
                    	desechamiento = false;
                    }

                    // pool delegaciones
                    if((NoHistorialAT0027.size() == 1 || NoHistorialAT0027.isEmpty()) && NoHistorialAT0022.isEmpty() && NoHistorialCIS106.isEmpty() && NoHistorial200501.isEmpty()  && HistorialCIS303.isEmpty() && NoHistorialIRA020.isEmpty())//si no vienen de ninguna activacion
                    { dePrevenc = false; desechamiento = false; }
                    else 
                    {  //HistorialAT0001.size() == 2 && NoHistorialAT0027.size() == 2 quiere decir que el trámite ya vino de una reactivación por prevención
                        if (!NoHistorialAT0022.isEmpty() || !NoHistorialCIS106.isEmpty() || !NoHistorial200501.isEmpty() || HistorialCIS303.size() == 1 || NoHistorialIRA020.size() == 1) //si el tamaÃ±o es igual a dos, indica que el trámite ya esta activo despues de haber sido prevenido
                        { 
                           if (NoHistorialAT0027.size() == 2)
                           {
                            dePrevenc = false; 
                           }
                           else { dePrevenc = true;  }
                        }
                        else 
                        { dePrevenc = false; }   
                    }
                    
                 // pool delegaciones
                    if(docsDgiraFlujo == null){
                    	docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
                    }
                    if(bitacora2 == null){
                    	bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
                    }
                    
                    boolean diasproceso = false;
                    long diasproceso_ = 0;
                    long diasiexpediente_ = 0;
                    try{		
                    	diasproceso_ = bitacora2.getBitaDiasProceso();
                    }catch(Exception ex){}
                    try{		
                        diasiexpediente_ = Long.parseLong(bitacora2.getBitaDiasIexpediente().toString());
                    }catch(Exception ex){}
                        
                    diasproceso = diasproceso_ <= diasiexpediente_;
                    
                    System.out.println("dePrevenc: " + dePrevenc);
                    System.out.println("diasproceso_: " + diasproceso_);
                    System.out.println("diasiexpediente_: " + diasiexpediente_);
                    System.out.println("diasproceso: " + diasproceso);
                    
                    if(dePrevenc) //si ya viene de una reactivación por prevención, ya no puede guardar check list y no puede generar oficio de prevención
                    {            // lo anterior cambia, para guardar de nuevo el checkList y pago de derechos para verificar si el promovente cumplio con lo requisitos del oficio de prevención,
                                 // de no ser asÃ­, se procede a generar oficio de desechamiento88
                        turnaDocsBtn = true; //ocultar bootn de turnado(firmado ahora)
                        //dePrevenc = false; //muestra bootn de generar oficio
                        URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                        
                        if(isUrlDG == 1 && bandeja == 2){
                    		try{
	                            if(docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO){
	                            	URLgenOficPre = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoDoc+"&entidadfederativa="+edoGestTram+"&documentoid=1";
	                            	dePrevenc = false;
	                            }
                    		}catch(Exception ex){
                    			URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                    		}
                    		
                    	}
                        
                    }
                    else
                    {

                        if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) 
                        { // pool delegaciones
                        	String perfilUsr = "";
                            try { // asegurarse que pueda tomar la jerarquia considerando tambien la entidad federativa...
                                areaResiveDocCon = daoBitacora.jerarquiaTbArea(docsDgiraFlujoHistEdit.getIdAreaEnvioHist(), idEntidad);
                                //JOptionPane.showMessageDialog(null, "idDocumento:  " +areaResiveDocCon.size(), "Error", JOptionPane.INFORMATION_MESSAGE);
                            } catch (Exception er2) {
                                //JOptionPane.showMessageDialog(null, "err:  " + er2.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                            }
                            for (Object o : areaResiveDocCon) {
                                jerarquia = o.toString();
                            }
                            
                            
                            
                            try{
                            	perfilUsr = docsDgiraFlujo.getRolId().getNombreCorto().trim();
                            }catch(NullPointerException exnp){
                            	perfilUsr = jerarquia.toLowerCase();
                            }
                            
                            // el statusDoc y estatus del documento_dgira_flujo debe ser igual ya que al turnar setea el estatus en ambos registros ( padre y detalle )
                        	perfilSig = AppSession.siguienteRol(perfilUsr, docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                        	//perfilSig = AppSession.siguienteRol(perfilUsr, statusDoc);
                            System.out.println("perfilSig :" + perfilSig);

                            
  
                            //si el perfil en session es igual al perfil al que se le envio el oficio, perimite la edición del oficio, esto era antes ahora recordar que solo el evaluador puede generar y modificar oficio de prevención
                            if (sesion.getAttribute("rol").toString().equals(perfilSig)) {
                                turnaDocsBtn = false;//visualizar boton de turnado(firmado ahora)
                                                                
                                if(sesion.getAttribute("rol").toString().equals("sd")) //solo el SD puede modificar y generar oficio de prevención
                                {
                                	// cuando es un sd y dias proceso NO es menor o igual que dias iexpediente
                                	if(!diasproceso)
                                		turnaDocsBtn = true; //ocultar boton de turnado(firmado) 
                                	
                                    URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; 
                                }
                                else
                                {
                                	if(isUrlDG == 1 && bandeja == 2){
                                		try{
            	                            if(docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO){
            	                            	URLgenOficPre = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoDoc+"&entidadfederativa="+edoGestTram+"&documentoid=1";
            	                            }
                                		}catch(Exception ex){
                                			URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                                		}
                                	}else{
                                		URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                                	}
                                }
                                
                            } else { 
                                turnaDocsBtn = true; //ocultar bootn de turnado
                                URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                            }
                        } else {
                            if(requisitosNull.size() == requisitos.size() || pagoDere == 0) //para cuando aun no se ha guardado nada en el checkList
                            {
                                URLgenOficPre = ""; //solo abre una hoja en blanco
                            }
                            if(requisitosNo.size() > 0 || pagoDere == 1) //para cuando si se ha guardado pero falatn requisitos en el checkList, recordar que solo el evaluador puede generar y modificar oficio de prevención
                            {
                                //URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; 
                                 if(sesion.getAttribute("rol").toString().equals("sd")) //solo el SD puede modificar y generar oficio de prevención
                                { 
                                    URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; 
                                }
                                else
                                {
                                    URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                                }
                            }
                            turnaDocsBtn = false;//visualizar boton de turnado
                            
                            // pero cuando es un sd...
                            if(sesion.getAttribute("rol").toString().equals("sd"))
                            {
                            	// y dias proceso NO es menor o igual que dias iexpediente
                                if(!diasproceso)
                                	turnaDocsBtn = true; //ocultar boton de turnado(firmado) 
                            }
                        }
                        System.out.println("oficio URL: " + URLgenOficPre);
                    }
//                }
            
        }
        System.out.println("URLgenOficPre : " + URLgenOficPre);
        
    }
    
    /***
     * Obtiene el StreamedContent de un archivo tipo "application/octet-stream" el cual es buscado por su idArchivo pasado en el request
     * @return el StreamedContent del file
     */
    public StreamedContent getArchivo(){
        try {
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String strIdArchivo = (String)req.getParameter("idArchivo");
            
            //strIdArchivo = "4192";
            File fArchivo = WSRecuperaArchivos.obtenerArchivo(Integer.parseInt(strIdArchivo));
            if (fArchivo != null) {
                InputStream stream = new FileInputStream(fArchivo);
                StreamedContent sFile = new DefaultStreamedContent(stream, "application/octet-stream", fArchivo.getName());
            
                return sFile;
            } else {
                logger.info("WS Archivo no encontrado, folio: " + strIdArchivo);
                return null;
            }

        } catch (Exception e) {
            logger.error("Ocurrió un error durante la descarga del archivo", e);
            return null;
        }
        
    }

    private void traerBitacora(String bitacora, String folio, String claveTipoTramite, Integer idTipoTramite) {
        System.out.println(bitacora);
        bq.generaRequisitos(bitacora, folio, claveTipoTramite, idTipoTramite);
        System.out.println("Procedimiento ejecutado");
    }

    /**
     * @return the oficioPrev
     */
    public Boolean getOficioPrev() {
        return oficioPrev;
    }

    /**
     * @param oficioPrev the oficioPrev to set
     */
    public void setOficioPrev(Boolean oficioPrev) {
        this.oficioPrev = oficioPrev;
    }

    /**
     * @return the bandera
     */
    public Integer getBandera() {
        return bandera;
    }

    /**
     * @param bandera the bandera to set
     */
    public void setBandera(Integer bandera) {
        this.bandera = bandera;
    }

    /**
     * @return the idArea
     */
    public String getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the idusuario
     */
    public Integer getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the sinatDgira
     */
    public SinatDgira getSinatDgira() {
        return sinatDgira;
    }

    /**
     * @param sinatDgira the sinatDgira to set
     */
    public void setSinatDgira(SinatDgira sinatDgira) {
        this.sinatDgira = sinatDgira;
    }

    /**
     * @return the folio
     */
    public String getFolio() {
        return folio;
    }

    /**
     * @param folio the folio to set
     */
    public void setFolio(String folio) {
        this.folio = folio;
    }

    /**
     * @return the oficPrev
     */
    public Boolean getOficPrev() {
        return oficPrev;
    }

    /**
     * @param oficPrev the oficPrev to set
     */
    public void setOficPrev(Boolean oficPrev) {
        this.oficPrev = oficPrev;
    }

    /**
     * @return the pagoDere
     */
    public Integer getPagoDere() {
        return pagoDere;
    }

    /**
     * @param pagoDere the pagoDere to set
     */
    public void setPagoDere(Integer pagoDere) {
        this.pagoDere = pagoDere;
    }

    /**
     * @return the checkList
     */
    public Integer getCheckList() {
        return checkList;
    }

    /**
     * @param checkList the checkList to set
     */
    public void setCheckList(Integer checkList) {
        this.checkList = checkList;
    }

    /**
     * @return the aux
     */
    public Integer getAux() {
        return aux;
    }

    /**
     * @param aux the aux to set
     */
    public void setAux(Integer aux) {
        this.aux = aux;
    }

    /**
     * @return the cveTipTram
     */
    public String getCveTipTram() {
        return cveTipTram;
    }

    /**
     * @param cveTipTram the cveTipTram to set
     */
    public void setCveTipTram(String cveTipTram) {
        this.cveTipTram = cveTipTram;
    }

    /**
     * @return the idTipTram
     */
    public Integer getIdTipTram() {
        return idTipTram;
    }

    /**
     * @param idTipTram the idTipTram to set
     */
    public void setIdTipTram(Integer idTipTram) {
        this.idTipTram = idTipTram;
    }

    /**
     * @return the edoGestTram
     */
    public String getEdoGestTram() {
        return edoGestTram;
    }

    /**
     * @param edoGestTram the edoGestTram to set
     */
    public void setEdoGestTram(String edoGestTram) {
        this.edoGestTram = edoGestTram;
    }

    /**
     * @return the documentoRespuestaDgira
     */
    public List<Object[]> getDocumentoRespuestaDgira() {
        return documentoRespuestaDgira;
    }

    /**
     * @param documentoRespuestaDgira the documentoRespuestaDgira to set
     */
    public void setDocumentoRespuestaDgira(List<Object[]> documentoRespuestaDgira) {
        this.documentoRespuestaDgira = documentoRespuestaDgira;
    }

    /**
     * @return the turnaDocs
     */
    public Boolean getTurnaDocs() {
        return turnaDocs;
    }

    /**
     * @param turnaDocs the turnaDocs to set
     */
    public void setTurnaDocs(Boolean turnaDocs) {
        this.turnaDocs = turnaDocs;
    }

    /**
     * @return the subSectorCat
     */
    public List<CatEstatusDocumento> getSubSectorCat() {
        return subSectorCat;
    }

    /**
     * @param subSectorCat the subSectorCat to set
     */
    public void setSubSectorCat(List<CatEstatusDocumento> subSectorCat) {
        this.subSectorCat = subSectorCat;
    }

    /**
     * @return the docsDgiraFlujoHist
     */
    public DocumentoDgiraFlujoHist getDocsDgiraFlujoHist() {
        return docsDgiraFlujoHist;
    }

    /**
     * @param docsDgiraFlujoHist the docsDgiraFlujoHist to set
     */
    public void setDocsDgiraFlujoHist(DocumentoDgiraFlujoHist docsDgiraFlujoHist) {
        this.docsDgiraFlujoHist = docsDgiraFlujoHist;
    }

    /**
     * @return the sesion
     */
    public HttpSession getSesion() {
        return sesion;
    }

    /**
     * @param sesion the sesion to set
     */
    public void setSesion(HttpSession sesion) {
        this.sesion = sesion;
    }

    /**
     * @return the idDocumento
     */
    public short getIdDocumento() {
        return idDocumento;
    }

    /**
     * @param idDocumento the idDocumento to set
     */
    public void setIdDocumento(short idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     * @return the areaEnvio
     */
    public String getAreaEnvio() {
        return areaEnvio;
    }

    /**
     * @param areaEnvio the areaEnvio to set
     */
    public void setAreaEnvio(String areaEnvio) {
        this.areaEnvio = areaEnvio;
    }

    /**
     * @return the tipoDoc
     */
    public short getTipoDoc() {
        return tipoDoc;
    }

    /**
     * @param tipoDoc the tipoDoc to set
     */
    public void setTipoDoc(short tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    /**
     * @return the docsDgiraFlujo
     */
    public DocumentoDgiraFlujo getDocsDgiraFlujo() {
        return docsDgiraFlujo;
    }

    /**
     * @param docsDgiraFlujo the docsDgiraFlujo to set
     */
    public void setDocsDgiraFlujo(DocumentoDgiraFlujo docsDgiraFlujo) {
        this.docsDgiraFlujo = docsDgiraFlujo;
    }

    /**
     * @return the docsDgiraFlujoHistEdit
     */
    public DocumentoDgiraFlujoHist getDocsDgiraFlujoHistEdit() {
        return docsDgiraFlujoHistEdit;
    }

    /**
     * @param docsDgiraFlujoHistEdit the docsDgiraFlujoHistEdit to set
     */
    public void setDocsDgiraFlujoHistEdit(DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit) {
        this.docsDgiraFlujoHistEdit = docsDgiraFlujoHistEdit;
    }

    /**
     * @return the maxDocsDgiraFlujoHist
     */
    public List<Object[]> getMaxDocsDgiraFlujoHist() {
        return maxDocsDgiraFlujoHist;
    }

    /**
     * @param maxDocsDgiraFlujoHist the maxDocsDgiraFlujoHist to set
     */
    public void setMaxDocsDgiraFlujoHist(List<Object[]> maxDocsDgiraFlujoHist) {
        this.maxDocsDgiraFlujoHist = maxDocsDgiraFlujoHist;
    }

    /**
     * @return the catEstatusDoc
     */
    public CatEstatusDocumento getCatEstatusDoc() {
        return catEstatusDoc;
    }

    /**
     * @param catEstatusDoc the catEstatusDoc to set
     */
    public void setCatEstatusDoc(CatEstatusDocumento catEstatusDoc) {
        this.catEstatusDoc = catEstatusDoc;
    }

    /**
     * @return the turnaDocsBtn
     */
    public Boolean getTurnaDocsBtn() {
        return turnaDocsBtn;
    }

    /**
     * @param turnaDocsBtn the turnaDocsBtn to set
     */
    public void setTurnaDocsBtn(Boolean turnaDocsBtn) {
        this.turnaDocsBtn = turnaDocsBtn;
    }
    
    

    public Boolean getCorregirDocsBtn() {
		return corregirDocsBtn;
	}

	public void setCorregirDocsBtn(Boolean corregirDocsBtn) {
		this.corregirDocsBtn = corregirDocsBtn;
	}

	/**
     * @return the turnaDocsBtnP
     */
    public Boolean getTurnaDocsBtnP() {
        return turnaDocsBtnP;
    }

    /**
     * @param turnaDocsBtnP the turnaDocsBtnP to set
     */
    public void setTurnaDocsBtnP(Boolean turnaDocsBtnP) {
        this.turnaDocsBtnP = turnaDocsBtnP;
    }

    /**
     * @return the DocRespDgira
     */
    public List<Object[]> getDocRespDgira() {
        return DocRespDgira;
    }

    /**
     * @param DocRespDgira the DocRespDgira to set
     */
    public void setDocRespDgira(List<Object[]> DocRespDgira) {
        this.DocRespDgira = DocRespDgira;
    }

    /**
     * @return the tbArea
     */
    public Tbarea getTbArea() {
        return tbArea;
    }

    /**
     * @param tbArea the tbArea to set
     */
    public void setTbArea(Tbarea tbArea) {
        this.tbArea = tbArea;
    }

    /**
     * @return the perfilSig
     */
    public String getPerfilSig() {
        return perfilSig;
    }

    /**
     * @param perfilSig the perfilSig to set
     */
    public void setPerfilSig(String perfilSig) {
        this.perfilSig = perfilSig;
    }

    /**
     * @return the URLgenOficPre
     */
    public String getURLgenOficPre() {
        return URLgenOficPre;
    }

    /**
     * @param URLgenOficPre the URLgenOficPre to set
     */
    public void setURLgenOficPre(String URLgenOficPre) {
        this.URLgenOficPre = URLgenOficPre;
    }

    /**
     * @return the nombreOfic
     */
    public String getNombreOfic() {
        return nombreOfic;
    }

    /**
     * @param nombreOfic the nombreOfic to set
     */
    public void setNombreOfic(String nombreOfic) {
        this.nombreOfic = nombreOfic;
    }

    /**
     * @return the urlOfic
     */
    public String getUrlOfic() {
        return urlOfic;
    }

    /**
     * @param urlOfic the urlOfic to set
     */
    public void setUrlOfic(String urlOfic) {
        this.urlOfic = urlOfic;
    }

    /**
     * @return the turnaEva
     */
    public String getTurnaEva() {
        return turnaEva;
    }

    /**
     * @param turnaEva the turnaEva to set
     */
    public void setTurnaEva(String turnaEva) {
        this.turnaEva = turnaEva;
    }

    /**
     * @return the muestraPegTurnado
     */
    public Boolean getMuestraPegTurnado() {
        return muestraPegTurnado;
    }

    /**
     * @param muestraPegTurnado the muestraPegTurnado to set
     */
    public void setMuestraPegTurnado(Boolean muestraPegTurnado) {
        this.muestraPegTurnado = muestraPegTurnado;
    }

    /**
     * @return the muestraSec
     */
    public Boolean getMuestraSec() {
        return muestraSec;
    }

    /**
     * @param muestraSec the muestraSec to set
     */
    public void setMuestraSec(Boolean muestraSec) {
        this.muestraSec = muestraSec;
    }

    /**
     * @return the dePrevenc
     */
    public Boolean getDePrevenc() {
        return dePrevenc;
    }

    /**
     * @param dePrevenc the dePrevenc to set
     */
    public void setDePrevenc(Boolean dePrevenc) {
        this.dePrevenc = dePrevenc;
    }

    /**
     * @return the tramiteYaTurnado
     */
    public Boolean getTramiteYaTurnado() {
        return tramiteYaTurnado;
    }

    /**
     * @param tramiteYaTurnado the tramiteYaTurnado to set
     */
    public void setTramiteYaTurnado(Boolean tramiteYaTurnado) {
        this.tramiteYaTurnado = tramiteYaTurnado;
    }

    /**
     * @return the crearResolutivo
     */
    public Boolean getCrearResolutivo() {
        return crearResolutivo;
    }

    /**
     * @param crearResolutivo the crearResolutivo to set
     */
    public void setCrearResolutivo(Boolean crearResolutivo) {
        this.crearResolutivo = crearResolutivo;
    }

    /**
     * @return the crearResolutivo2
     */
    public Boolean getCrearResolutivo2() {
        return crearResolutivo2;
    }

    /**
     * @param crearResolutivo2 the crearResolutivo2 to set
     */
    public void setCrearResolutivo2(Boolean crearResolutivo2) {
        this.crearResolutivo2 = crearResolutivo2;
    }
    
    /**
     * @return the comentarioTurnado
     */
    public String getComentarioTurnado() {
        return comentarioTurnado;
    }

    /**
     * @param comentarioTurnado the comentarioTurnado to set
     */
    public void setComentarioTurnado(String comentarioTurnado) {
        this.comentarioTurnado = comentarioTurnado;
    }

    public boolean isDigital() {
        return digital;
    }

    /**
     * @return the nombTipoOfic
     */
    public String getNombTipoOfic() {
        return nombTipoOfic;
    }

    /**
     * @param nombTipoOfic the nombTipoOfic to set
     */
    public void setNombTipoOfic(String nombTipoOfic) {
        this.nombTipoOfic = nombTipoOfic;
    }

    /**
     * @return the comenatriosTab
     */
    public List<Object[]> getComenatriosTab() {
        return comenatriosTab;
    }

    /**
     * @param comenatriosTab the comenatriosTab to set
     */
    public void setComenatriosTab(List<Object[]> comenatriosTab) {
        this.comenatriosTab = comenatriosTab;
    }


    /*public void traerBitacora(ActionEvent event){
     FacesContext fc = FacesContext.getCurrentInstance();
     this.bitacora = getBitacoraParam(fc);
     System.out.println(bitacora);
     bq.ejecutaSP(bitacora);
     System.out.println("Procedimiento ejecutado");
     }
    
     public String getBitacoraParam(FacesContext fc){
     Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
     return params.get("numBitacoraChk");
     }*/
    public class ListaRequisitos {

        private Object[] o;
        private String obsPre;
        private String obs;
        private String rutaChkImg;
        private boolean entregadoSD;

        public boolean isEntregadoSD() {
            return entregadoSD;
        }

        public void setEntregadoSD(boolean entregadoSD) {
            this.entregadoSD = entregadoSD;
        }

        public ListaRequisitos(Object[] o) {
            this.o = o;

            try {
                //Si es 1, bandera activa de entregado a subdirector
                BigDecimal bdEntSD = (BigDecimal) o[4];
                this.entregadoSD = (bdEntSD.doubleValue() == 1);
            } catch (Exception e) {
                this.entregadoSD = false;
            }
        }

        public ListaRequisitos(Object[] o, String obsPre, String rutaChkImg) {
            this.o = o;
            this.obsPre = obsPre;
            this.rutaChkImg = rutaChkImg;

            try {
                //Si es 1, bandera activa de entregado a subdirector
                BigDecimal bdEntSD = (BigDecimal) o[4];
                this.entregadoSD = (bdEntSD.doubleValue() == 1);
            } catch (Exception e) {
                this.entregadoSD = false;
            }
        }

        public Object[] getO() {
            return o;
        }

        public void setO(Object[] o) {
            this.o = o;
        }

        public String getObs() {
            return obs;
        }

        public void setObs(String obs) {
            this.obs = obs;
        }

        public String getRutaChkImg() {
            return rutaChkImg;
        }

        public void setRutaChkImg(String rutaChkImg) {
            this.rutaChkImg = rutaChkImg;
        }

        public String getObsPre() {
            return obsPre;
        }

        public void setObsPre(String obsPre) {
            this.obsPre = obsPre;
        }

    }

    @SuppressWarnings("unused")
	public void guardar() throws Exception {
        String msg = "";
        Integer banVac = 0;
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        HashMap<Integer, ListaRequisitos> listaTemp = new HashMap<>();
        bandera = 0;
        counReqSi = 0;
        counReqSiGuard = 0;
        EvaluacionProyecto eval;

        try {
            for (ListaRequisitos a : requisitosImg) {
                for (ListaRequisitos e : requisitosSi) { //todos los que ya valido el SubDirector
                    //compara si se trata del mismo clave de requisito y compara con las que ya estan aprovadas por el SD
                    if (Integer.parseInt(a.getO()[0].toString()) == Integer.parseInt(e.getO()[0].toString())) {

                        listaTemp.put(Integer.parseInt(a.getO()[0].toString()), e);
                        dao.guardarReq(bitaProyecto, Integer.parseInt(a.getO()[0].toString()), a.getObsPre(), 1);
                        bandera = 0;
                    } else {
                        if (!listaTemp.containsKey(Integer.parseInt(a.getO()[0].toString()))) {
                            bandera = 1;
                            if (a.obsPre.isEmpty()) //verifica comentarios de BD
                            {
                                counReqSi = counReqSi + 1;
                            }
                            if (a.getObsPre().isEmpty()) // si la observación esta vacÃ­a no deja guardar(del texbox)
                            {
                                counReqSiGuard = counReqSiGuard + 1;
                            } else {
                                dao.guardarReq(bitaProyecto, Integer.parseInt(a.getO()[0].toString()), a.getObsPre(), 0);
                            }

                        }
                    }
                }
                if (requisitosSi.size() == 0) {
                    bandera = 0;
                    //bandera = 1;
                    if (!a.obsPre.isEmpty()) //verifica comentarios de BD
                    {
                        counReqSi = counReqSi + 1;
                    }
                    if (a.getObsPre().isEmpty()) // si la observación esta vacÃ­a no deja guardar(del texbox)
                    {
                        counReqSiGuard = counReqSiGuard + 1;
                    } else {
                        dao.guardarReq(bitaProyecto, Integer.parseInt(a.getO()[0].toString()), a.getObsPre(), 0);
                    }
                }
            }
            
            
            if(dePrevenc) // sino viene de prevención
            {  msg = "Su información ha sido guardada con Éxito"; }
            else
            {
                NoHistorialAT0027 = daoBitacora.numRegHistStatus(bitaProyecto,"AT0027");
                if(!(requisitosSi.size() == requisitosImg.size()) && NoHistorialAT0027.size()==2)
                {
                    msg = "Su información ha sido guardada con Éxito. Debe turnar trámite a evaluador para generar oficio de desechamiento.";
                }
                else
                {
                    msg = "Su información ha sido guardada con Éxito";
                }
            }
            
           

            //JOptionPane.showMessageDialog(null,  "counReqSiGuard:  " + counReqSiGuard + "  counReqSi: " + counReqSi + "  bandera: " + bandera + "  requisitosImg.size(): " + requisitosImg.size() + "   requisitosSi.size(): " + requisitosSi.size(), "Error", JOptionPane.INFORMATION_MESSAGE);
            reqcontEnv.execute("alert('" + msg + "')");

            System.out.println(bitaProyecto);
            System.out.println("numBitacora: " + bitaProyecto);
            requisitos.clear();
            requisitosImg.clear();            
            
            List<Object[]> data;
            data = this.digital ? dao.getReqTramiteDigital(bitaProyecto) : dao.getReqTramite(bitaProyecto);
            for (Object[] req : data) {
                requisitos.add(new ListaRequisitos(req));
            }
                    
            
            for (ListaRequisitos a : requisitos) {
                if (Integer.parseInt(a.getO()[3].toString()) == 0) {
                    rutaImg = "/resources/images/crossmark.png";
                    if (a.getO()[5].toString().isEmpty()) {
                        observaciones = "";
                        requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg));
                    } else {
                        observaciones = a.getO()[5].toString();
                        requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg));
                    }

                } else {
                    rutaImg = "/resources/images/checkmark.png";
                    if (a.getO()[5] == null) {
                        observaciones = "";
                        requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg));
                    } else {
                        observaciones = a.getO()[5].toString();
                        requisitosImg.add(new ListaRequisitos(a.getO(), observaciones, rutaImg));
                    }
                }
            }
            

        
        data = this.digital ? dao.getReqTramiteNullDigital(bitaProyecto) : dao.getReqTramiteNull(bitaProyecto);
        for (Object[] reqNull : data) {
            requisitosNull.add(new ListaRequisitos(reqNull));
        }
        data = this.digital ? dao.getReqTramiteNoDigital(bitaProyecto) : dao.getReqTramiteNo(bitaProyecto);
        for (Object[] reqNo : data) {
            requisitosNo.add(new ListaRequisitos(reqNo));
        } 

        
        //verificar que tipo de oficio se va a generar asi como saber si se muestra la opción de modificar el Oficio o Solo el PDF
        verificaTipoOficio();


        } catch (Exception e) {

            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente mÃ¡s tarde.')");
        }
        detalleView.checkListCompleto();
        if(detalleView.getActiveTab() != 0){
        	System.out.println("hice----------->" + detalleView.getActiveTab());
        	reqcontEnv.execute("PF('myTabView').select("+ detalleView.getActiveTab() +");");
        }
    }

    @SuppressWarnings("unused")
	public void cambioPrev() {
        String mensaje = "";
        if (bitaProyecto != null) {
            try {////bitacoraAT0022  bitacoraI00010

                bitacoraAT0022 = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto, "AT0022");
                bitacoraI00010 = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto, "I00010");
                bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);

                FacesMessage message = null;
                if (bitacoraI00010 != null) {
                    message = new FacesMessage(FacesMessage.SEVERITY_INFO, "El trámite se encuentra en evaluación, la prevención no procede.", null);
                } else {
                    if (bitacoraAT0022 != null) {
                        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "El trámite se encuentra en prevención.", null);
                    } else {
                        if (bitacora2 != null) {
                            sinatDgira = new SinatDgira();
                            sinatDgira.setId(0);
                            sinatDgira.setSituacion("AT0022");
                            sinatDgira.setIdEntidadFederativa(bitacora2.getBitaEntidadGestion());
                            sinatDgira.setIdClaveTramite(bitacora2.getBitaTipotram()); //STRING BITA_TIPOTRAM
                            sinatDgira.setIdTramite(bitacora2.getBitaIdTramite());
                            sinatDgira.setIdAreaEnvio(idArea.trim());
                            sinatDgira.setIdAreaRecibe(idArea.trim());
                            sinatDgira.setIdUsuarioEnvio(idusuario);
                            //sinatDgira.setIdUsuarioRecibe(1340);
                            sinatDgira.setGrupoTrabajo(new Short("0"));
                            sinatDgira.setBitacora(bitaProyecto);
                            sinatDgira.setFecha(new Date());
                            try {
                                daoBitacora.agrega(sinatDgira);

//                                    mensaje = "Trámite turnado a prevención. ";
//                                    //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite turnado a prevención.", null);
//                                    //comienza trámite de segundas vÃ­as  
//                                    try { //1=iNFORMACIÃ“N ADICIONAL 2=PREEVENCIÃ“N
//                                        segundasVias.proc(bitaProyecto, 2,folio);
//                                        mensaje = mensaje + "Trámite notificado a promovente.";
//                                        System.out.println("Termino de segundas vÃ­as");
//                                         //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "termina scrip.", null);
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "NOOOOO termina scrip.", null);
//                                        System.out.println("Problema con segundas vÃ­as: " + e.getMessage());
//                                    }
                                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite turnado a prevención. Trámite notificado a promovente.", null);

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                    }
                }
                FacesContext.getCurrentInstance().addMessage("messages", message);

            } catch (Exception er) {
                er.printStackTrace();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido turnar el trámite, intente mÃ¡s tarde.", null);
                FacesContext.getCurrentInstance().addMessage("messages", message);
            }
        }
    }

    public List<ListaRequisitos> getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(List<ListaRequisitos> requisitos) {
        this.requisitos = requisitos;
    }

    public Object[] getObjectArray() {
        return objectArray;
    }

    public void setObjectArray(Object[] objectArray) {
        this.objectArray = objectArray;
    }

    public List<RequisitosSubdirector> getTblReq() {
        return tblReq;
    }

    public void setTblReq(List<RequisitosSubdirector> tblReq) {
        this.tblReq = tblReq;
    }

    public RequisitosDataModel getReqDM() {
        return reqDM;
    }

    public void setReqDM(RequisitosDataModel reqDM) {
        this.reqDM = reqDM;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public List<ListaRequisitos> getValoresDT() {
        return valoresDT;
    }

    public void setValoresDT(List<ListaRequisitos> valoresDT) {
        this.valoresDT = valoresDT;
    }

    public String getBitaProyecto() {
        return bitaProyecto;
    }

    public void setBitaProyecto(String bitaProyecto) {
        this.bitaProyecto = bitaProyecto;
    }

    public String getRutaImg() {
        return rutaImg;
    }

    public void setRutaImg(String rutaImg) {
        this.rutaImg = rutaImg;
    }

    public List<ListaRequisitos> getRequisitosImg() {
        return requisitosImg;
    }

    public void setRequisitosImg(List<ListaRequisitos> requisitosImg) {
        this.requisitosImg = requisitosImg;
    }

    public List<ListaRequisitos> getRequisitosSi() {
        return requisitosSi;
    }

    public void setRequisitosSi(List<ListaRequisitos> requisitosSi) {
        this.requisitosSi = requisitosSi;
    }

    public String getBitaProyOfic() {
        this.bitaProyOfic = bitaProyecto.substring(0, 2) + "%2F" + bitaProyecto.substring(3, 10) + "%2F" + bitaProyecto.substring(11, 13) + "%2F" + bitaProyecto.substring(14, 16);
        return bitaProyOfic;
    }

    public void setBitaProyOfic(String bitaProyOfic) {
        this.bitaProyOfic = bitaProyOfic;
    }

    public void selTurnadoDocsCheck() {
    	System.out.println("\nselTurnadoDocsCheck");
        turnaDocs = false; ////cveTipTram idTipTram edoGestTram, idTipoDoc, idDoc
        
        //tipoDoc = 7;  //String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc
        //si el tramite ya ha generado y guradado un oficio de preevencion por validación de checkList, este ya puede ser turnado
        documentoRespuestaDgira = daoDg.docsRespDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
        if (documentoRespuestaDgira.size() > 0) {
            if(tipoDoc == 4)
            { nombTipoOfic="Prevención por CheckList"; }
            if(tipoDoc == 3)
            { nombTipoOfic="Prevención por Pago de derechos"; }
            if(tipoDoc == 5)
            { nombTipoOfic="Prevención por CheckList y Pago de derechos"; }
            
         // pool url
            if(docsDgiraFlujo == null){
            	// en este caso obtenemos el documento dgira porque no se ha consultado
            	docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
            }
            
            List<DocumentoRespuestaDgira> lista;
            lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
            for (DocumentoRespuestaDgira x : lista) {
                nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
                
                try{
                    if(docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId() == GenericConstants.ESTATUS_DOCTO_FIRMADO){
                    	urlOfic = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoDoc+"&entidadfederativa="+edoGestTram+"&documentoid=1";
                    }else{
                    	urlOfic = x.getDocumentoUrl();
                    }
                }catch(Exception exx){
                	System.out.println("\nno asignó el urlOfic de docsDgiraFlujo ya que este ultimo es null");
                	//exx.printStackTrace();
                	if(x != null){
                		urlOfic = x.getDocumentoUrl();
                	}
                }
                
                documento = x;
                
            }
            turnaDocs = true;
        } else {
            turnaDocs = false;

        }
        //JOptionPane.showMessageDialog(null, "Debe:  " + turnaDocs, "Error", JOptionPane.INFORMATION_MESSAGE);
        
        System.out.println("turnaDocs :" +turnaDocs);
    }

    public SelectItem[] getEnvioSelectOne() {
        SelectItem[] items = null;
        items = new SelectItem[1];

        if (idDocumento > 0) {//JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
            if (sesion != null) {   //JOptionPane.showMessageDialog(null, "idDocumento:  " + idDocumento, "Error", JOptionPane.INFORMATION_MESSAGE);
                if (sesion.getAttribute("rol").toString().equals("dg")) //Director General
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al director de Ã¡rea
                    {
                        items[0] = new SelectItem("1", "Director General");
                    }
                    if (idDocumento == 2) //SI el doc esta en corrección, solo se podra turnar al evaluador
                    {
                        items[0] = new SelectItem("2", "Director de Área");
                    }
                }
                if (sesion.getAttribute("rol").toString().equals("da")) //Director de Ã�rea
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al director de Ã¡rea
                    {
                        items[0] = new SelectItem("1", "Director General");
                    }
                    if (idDocumento == 2) //SI el doc esta en corrección, solo se podra turnar al evaluador
                    {
                        items[0] = new SelectItem("3", "SubDirector");
                    }
                }
                if (sesion.getAttribute("rol").toString().equals("sd")) //Sub Director
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al director de Ã¡rea
                    {
                        items[0] = new SelectItem("2", "Director de Área");
                    }
                    if (idDocumento == 2) //SI el doc esta en corrección, solo se podra turnar al evaluador
                    {
                        if (tipoDoc != 4 && tipoDoc != 3) {
                            items[0] = new SelectItem("4", "Evaluador");
                        } else {
                            items = new SelectItem[1];
                            items[0] = new SelectItem("0", "--Seleccione estatus--");
                        }
                    }

                }
                if (sesion.getAttribute("rol").toString().equals("eva")) //Evaluador
                {
                    if (idDocumento == 3) //SI el doc esta aprovado, solo se podra turnar al  subdirector
                    {
                        items[0] = new SelectItem("3", "SubDirector");
                    }
                }
            }
        }
        if (idDocumento == 0) {
            items[0] = new SelectItem("0", "--Seleccione estatus--");
        }

        return items;
    }
    


    public StreamedContent getFile() {
    	System.out.println("getFile...");
        return file;
    }
    
    
    
    /***
     * Descarga el jar para firma y registra la cadena firma si no existe o en su caso muestra el token al usuario
     */
    public void siFirmar()
    {
    	System.out.println("\n\nsi firmar");
    	InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/resources/SignerClientAll.jar");
		file = new DefaultStreamedContent(stream, "application/java-archive", "SignerClientAll.jar");
		
		FacesContext fContext = FacesContext.getCurrentInstance();
		String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
		String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();
		tipoDocId = "1";  // para los oficios preventivos
		
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDoc :" + tipoDoc);
		System.out.println("documentoId : " + tipoDocId);		
		System.out.println("edoGestTramite : " + edoGestTram);		
		System.out.println("rol usuario : " + rol); //sesion.getAttribute("rol").toString());
		System.out.println("idAreaUsu : " + fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
		System.out.println("idUsu : " + fContext.getExternalContext().getSessionMap().get("idUsu"));
		System.out.println("-------------------------------------------------------------------------");
				
		// busca si ya existe el registro de cadena firma...
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);	
		
		String msg_numero_token = Utils.obtenerConstante(GenericConstants.FIRMAR_NUMERO_TOKEN);
		String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.FIRMAR_NO_REGISTRO_CADENAFIRMA);
		
		if(firma != null){
		
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));			
		}else{
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
	        Date thisDate = new Date();
	        String fecha = dateFormat.format(thisDate);
	        	        
	        String descripcion_documento = "Oficio de Prevención";	        
	        //String destinatario = usuariorecibetxt.equals("") ? "Nuevo destinatario" : usuariorecibetxt;	        
	        //String iniciales = "LDSM/ASR/AVA/GBA/JCTT";
	        //String infoaFirmar = "||"+fecha+"|"+bitacora+"|"+descripcion_documento+"|"+destinatario+"|"+iniciales+"||";
	        String infoaFirmar = "||"+bitacora+"|"+descripcion_documento+"||";
	        String infoaFirmarB64 = Base64.encodeBase64String(infoaFirmar.getBytes());
	        
	        System.out.println("fecha :" + fecha);
	        System.out.println("descripcion :" + descripcion_documento);
	        //System.out.println("destinatario :" + destinatario);
	        //System.out.println("iniciales :" + iniciales);
	        System.out.println("encodeB64 :" + infoaFirmarB64);
	        
			System.out.println("\n\nagregaCadena..");
			CadenaFirmaDao cadenaDao = new CadenaFirmaDao();
			BcAgregaCadenaRespuesta agregacadena = null;
			
			try {
				agregacadena = cadenaDao.agregaCadena(folio, infoaFirmarB64);
				
				if(agregacadena != null){
					 
					 firma = new CadenaFirma();

					 // firma.setClave(clave);
					 firma.setClaveProyecto(claveProyecto);
					 firma.setBitacora(bitacora);
					 firma.setClaveTramite2(cveTipTram);
					 firma.setIdTramite2(idTipTram);
					 firma.setTipoDocId(tipoDoc);
					 firma.setDocumentoId(tipoDocId);
					 
					 firma.setFolioProyecto(folio);
					 firma.setB64(infoaFirmarB64);
					 firma.setIdentificador(agregacadena.getIdentificador());
					 firma.setOperacion(String.valueOf(agregacadena.getTransferenciaOperacion()));
				 
					 new CadenaFirmaDao().agrega(firma);
					 
					 // despues de agregar la cadena de firma, se consulta para mostrar el identificador al usuario..
					 firma = null;
					 firma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);
					 
					 if(firma != null){
					   FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
					 }else{
						 FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
					 }
					 					 
				}else{
						 
					FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
				}	
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
			}
		}
    }
    
    
    
   

    /***
     * Limpia el panel de comentarios de la pantalla
     */
    public void limpiaComentarios(){
    	System.out.println("limpiaComentarios");
    	comentarioTurnado = "";
    	
    	RequestContext.getCurrentInstance().reset("dlgcomentarioscorreccion:pnlComentarios");
    	RequestContext.getCurrentInstance().update("dlgcomentarioscorreccion:pnlComentarios");
    }

    /***
     * Guarda los comentarios de corrección del oficio de prevención
     */
    public void corrigeOficChkList(){
    	
    	System.out.println("\n\ncorrigeOficChkList..");
    	System.out.println("comentarios : " + comentarioTurnado);
    	idDocumento = 2; // a corrección
    	
    	try {
			turnarcorregirguardar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    
    /***
     * Método que realiza el turnado del oficio recientemente firmado de prevención ( cuando el usuario no es un DG )
     * @throws Exception
     */
    public void turnaOficChkList(){
    	
    	System.out.println("\n\nturnaOficChkList..");
    	
    	FacesContext fContext = FacesContext.getCurrentInstance();
		String folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
		String bitacora = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString(); 
		String claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();  // new MenuDao().cveTramite(bitacora);
		String claveTramite = cveTipTram; //fContext.getExternalContext().getSessionMap().get("cveTramite").toString();
		int idTramite = idTipTram; //fContext.getExternalContext().getSessionMap().get("idTramite").toString();
		// String edoGestTram = fContext.getExternalContext().getSessionMap().get("edoGestTramite").toString();
		String rol = fContext.getExternalContext().getSessionMap().get("rol").toString();
		tipoDocId = "1";  // para los oficios preventivos
		
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("folio :" + folio);
		System.out.println("claveProyecto :" + claveProyecto);		
		System.out.println("bitacora :" + bitacora);
		System.out.println("claveTramite :" + claveTramite);
		System.out.println("idTramite :" + idTramite);
		System.out.println("tipoDocId :" + tipoDoc);
		System.out.println("documentoId : " + tipoDocId);		
		System.out.println("edoGestTramite : " + edoGestTram);		
		System.out.println("rol : " + rol);
		System.out.println("-------------------------------------------------------------------------");
    	
		if(isUrlDG == 1)    	
			idDocumento = GenericConstants.ESTATUS_DOCTO_FIRMADO; // 4 firmado
		else
			idDocumento = GenericConstants.ESTATUS_DOCTO_AUTORIZADO; // 3 autorizado ( turnar )
		
		// url que apunta al oficio dentro del folio( carpeta ). para este caso sera para ponerle esta url al codigo qr
		String ligaQR = Utils.ruta_liga_codigoqr + bitaProyecto;
		
		System.out.println("idDocumento : " + idDocumento);
		System.out.println("urlOfic : " + urlOfic);
		System.out.println("nombreOfic : " + nombreOfic);
		System.out.println("ligaQR : " + ligaQR);
		System.out.println("-------------------------------------------------------------------------");
    	
    	// busca si ya existe el registro de cadena firma...		
		CadenaFirmaDao firmaDao = new CadenaFirmaDao();
		CadenaFirma cadenafirma = firmaDao.getCadenaFirma(claveProyecto, bitacora, cveTipTram, idTipTram, tipoDoc, tipoDocId);
		
		String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
		String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
		String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
		String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);
				
		
				
		if(cadenafirma == null){
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_hay_token_registrado));
			return;
		}
    	
		// obtenemos la firmalog del usuario logueado ( por su cadenafirma y rol lo sabemos y no necesariamente existe este registro en este momento )
		CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenafirma, rol);
				
		// detallefirmado es objeto que tiene un estatus y descripcion y una lista de firmas del ws
		DetallesFirmado detallefirmado = null;
		try{
			detallefirmado = firmaDao.getFirmas(cadenafirma);
			
			// el ws no responde
			if(detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() != 0)){
				System.out.println("\nno responde el servicio web o no hay firmas...");
				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));		
				return;
			// como el ws si esta respondiendo, analizar las firmas a ver si alguna de ellas corresponde con la de firmaLog actual...
			}else if (detallefirmado != null && detallefirmado.getEstado() == 0 && ( detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)){
				
				
				// cadenafirmaLog puede ser null
				// detallefirmado si trae firmas del ws
				Firmas firmaws = firmaDao.GetFirmaWS(cadenafirma, detallefirmado);
							
				// para este caso si no hay firma del ws decimos que no ha firmado el usuario...
				if(firmaws == null){
					System.out.println("ultima firma válida : null");
					System.out.println("\nel usuario no ha firmado...");
					FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
					return;
				}else{
					System.out.println("ultima firma válida : " + firmaws.getRfc() + " - " + firmaws.getFecha());
					System.out.println("el usuario ya firmó y se guardará en cadenafirmalog...");
					// SE ASUME QUE SI FIRMÓ YA por tanto primero agregamos el registro de cadenafirmalog correspondiente...
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
					Date fcha = formatter.parse(firmaws.getFecha());
					boolean resp = false;
					
					if(cadenafirmaLog == null){
						
						CadenaFirmaLog firmaLog = new CadenaFirmaLog();
						firmaLog.setCadenaFirma(cadenafirma);
						firmaLog.setEstatus((short)1);
						firmaLog.setRol(rol);
						firmaLog.setFecha(fcha);
						firmaLog.setFirma(firmaws.getFirma());
						firmaLog.setRfc(firmaws.getRfc());
					
						resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
						System.out.println("firmalog agregada: " + resp);
						
					}else{
						// se actualiza cadenafirmalog del rol en sesion
						cadenafirmaLog.setRfc(firmaws.getRfc());
						cadenafirmaLog.setFecha(fcha);
						cadenafirmaLog.setFirma(firmaws.getFirma());
						resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
						System.out.println("firmalog modificada: " + resp);
						
					}
					
					if(resp == true){
						
						// y mandamos turnar...
						if(idDocumento == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
							
							turnarcorregirguardar();
							
						}else{
                                                        //documento.setDocumentoUrl(urlOfic);
                                                        //documento.setDocumentoNombre(nombreOfic);
							firmaDao.editarPdf(documento, cadenafirma, ligaQR);
							firmaDao.cierraLoteFirmas(Long.parseLong(cadenafirma.getOperacion()), cadenafirma.getIdentificador(),  cadenafirma.getFolioProyecto());
							turnarcorregirguardarDG();
						}
						
					}else{
						throw new Exception("Error: al guardar el log de la firma para el rol actual");
					}
				}
				
				
			}else{

				FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_se_realizo_turnado));
			}
		}catch(Exception ex){
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
		}    	
    }
    
    
    
    
    /***
     * Método que hace el turnado del documento firmado o la correccion del mismo segun sea el caso
     * @throws Exception
     */
	public void turnarcorregirguardar() throws Exception{
    	
    	System.out.println("\nturnarcorregirguardar..");
        int banderaNew = 0;
        int error = 0;
        // Integer valInt = 0;
        idAux = 0;
        maxIdHisto = 0;
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial subdirector;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        Historial situacATOO22 = null;
        Historial situacI00010 = null;
               

        FacesContext ctxt = FacesContext.getCurrentInstance();
        if (bitaProyecto != null) {
            try {
                proy = daoV.verificaERA2(bitaProyecto);
                if (proy != null) {
                    if (proy.getProyectoPK() != null) {
                        ctxt.getExternalContext().getSessionMap().put("userFolioProy", proy.getProyectoPK().getFolioProyecto());
                        ctxt.getExternalContext().getSessionMap().put("userSerialProy", proy.getProyectoPK().getSerialProyecto());
                        folio = proy.getProyectoPK().getFolioProyecto();
                    }
                }
            } catch (Exception ar) {
                System.out.println("Problema: " + ar.getMessage());
            }

        }

        if (bitaProyecto != null) {
            
            
            situacATOO22 = daoBitacora.datosProyHistStatus(bitaProyecto, "AT0022");
            situacI00010 = daoBitacora.datosProyHistStatus(bitaProyecto, "I00010");
            if (situacATOO22 != null || situacI00010 != null) {
                ctxt.addMessage("growl", new FacesMessage("Oficio de prevención no puede ser turnado, el trámite ya se encuentra prevenido o ha sido turnado al evaluador."));
            } else {
                //-------------------actualización en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

                maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
                for (Object o : maxDocsDgiraFlujoHist) {
                    idAux = Integer.parseInt(o.toString());
                    maxIdHisto = idAux.shortValue();
                }

                docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);

                if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                    docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                    docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                    docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                    daoDg.modifica(docsDgiraFlujoHistEdit);
                }

                //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
                docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");

                if (docsDgiraFlujo != null) {
                    if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                        banderaNew = 0;
                    }// registro nuevo
                    else {
                        banderaNew = 1;
                    }

                } else {
                    banderaNew = 1;
                }

                if (banderaNew == 1) {
                    docsDgiraFlujo = new DocumentoDgiraFlujo(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
                }

                catEstatusDoc = daoDg.idCatEstatusDoc(idDocumento);
                docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
                docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
                docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
                docsDgiraFlujo.setIdAreaRecibe(null);
                docsDgiraFlujo.setIdUsuarioRecibe(null);
                docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
                docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
                docsDgiraFlujo.setDocumentoDgiraObservacion(comentarioTurnado);

             // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
                UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
        		System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
        		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
        		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
        		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
        		System.out.println("rolId     :" + ur.getRolId().getIdRol());
        		
        		// Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
                CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());
                
                // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
               	docsDgiraFlujo.setRolId(rol);
                
                try {
                    if (banderaNew == 1) {
                        daoDg.agrega(docsDgiraFlujo);
                    }
                    if (banderaNew == 0) {
                        daoDg.modifica(docsDgiraFlujo);
                    }

                    maxIdHisto = (short) (idAux + 1);
                    docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);
                    docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                    docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                    docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                    docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                    docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                    docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                    docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                    docsDgiraFlujoHist.setDocumentoDgiraObservHist(comentarioTurnado);
                    try {
                        daoDg.agrega(docsDgiraFlujoHist);
                    } catch (Exception ex) {
                        error = error + 1;
                    }
                } catch (Exception e) {
                    error = error + 1;
                }

                if (error == 0) {
                    System.out.println("Terminó turnado de oficio de prevención ");

                    //------------implementación de segundas vÃ­as, solo cuando el oficio fue firmado por el DG y este mismo ha sido puesto en prevención
                    if (idDocumento == 4) {
                        System.out.println("idDocumento: " + idDocumento);
                        if (bitacora2 != null) {
                            System.out.println("idDocumento 444 ");
                            //se obtienen datos del subdirector que tiene asignado el proyecto, para asi poder realizar el cambio de situación en su nombre, aunque se encuentre en el perfil del DG
                            subdirector = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto, "AT0027");
                            sinatDgira = new SinatDgira();
                            sinatDgira.setId(0);
                            sinatDgira.setSituacion("AT0022");
                            sinatDgira.setIdEntidadFederativa(bitacora2.getBitaEntidadGestion());
                            sinatDgira.setIdClaveTramite(bitacora2.getBitaTipotram()); //STRING BITA_TIPOTRAM
                            sinatDgira.setIdTramite(bitacora2.getBitaIdTramite());
                            sinatDgira.setIdAreaEnvio(subdirector.getHistoAreaEnvio().trim());
                            sinatDgira.setIdAreaRecibe(subdirector.getHistoAreaEnvio().trim());
                            sinatDgira.setIdUsuarioEnvio(subdirector.getHistoIdUsuarioEnvio());
                            //sinatDgira.setIdUsuarioRecibe(1340);
                            sinatDgira.setGrupoTrabajo(new Short("0"));
                            sinatDgira.setBitacora(bitaProyecto);
                            sinatDgira.setFecha(new Date());
                            try {
                                daoBitacora.agrega(sinatDgira);
                                System.out.println("Terminó turnado de trámite a prevención ");
                                if(folio!=null)
                                {
                                    //comienza trámite de segundas vÃ­as  
                                    try { //1=iNFORMACIÃ“N ADICIONAL 2=PREEVENCIÃ“N
                                        segundasVias.proc(bitaProyecto, 2, folio,1,tipoDoc);
                                        System.out.println("Termino de segundas vÃ­as");
                                        ctxt.addMessage("growl", new FacesMessage("Trámite turnado a prevención. Trámite notificado a promovente."));
                                        //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "termina scrip.", null);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        ctxt.addMessage("growl", new FacesMessage("Problemas Segundas vÃ­as."));
                                        System.out.println("Problema con segundas vÃ­as: " + e.getMessage());
                                    }
                                }
                                else
                                {
                                    ctxt.addMessage("growl", new FacesMessage("Trámite turnado a prevención."));
                                }
                                

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                System.out.println("Problemas con turnado de oficio de prevención: " + ex.getMessage());
                            }
                        }

                    } else {
                        ctxt.addMessage("growl", new FacesMessage("Oficio de prevención turnado exitosamente."));
                    }
                    
                    comenatriosTab = daoBitacora.tablabOsevOfic(bitaProyecto, tipoDoc, "1", idEntidad);
                    System.out.println("comenatriosTab size: " + comenatriosTab.size());
                    List<Object[]> data;
                    for (Object[] reqNull : dao.getReqTramiteNull(bitaProyecto)) {
                        requisitosNull.add(new ListaRequisitos(reqNull));
                    }
                    for (Object[] reqNo : dao.getReqTramiteNo(bitaProyecto)) {
                        requisitosNo.add(new ListaRequisitos(reqNo));
                    }
                    
                    data = this.digital ? dao.getReqTramiteNullDigital(bitaProyecto) : dao.getReqTramiteNull(bitaProyecto);
                    for (Object[] reqNull : data) {
                        requisitosNull.add(new ListaRequisitos(reqNull));
                    }
                    data = this.digital ? dao.getReqTramiteNoDigital(bitaProyecto) : dao.getReqTramiteNo(bitaProyecto);
                    for (Object[] reqNo : data) {
                        requisitosNo.add(new ListaRequisitos(reqNo));
                    }

                    //----------------------verificar si el boton para turnar los  oficios, el que se encinetra dentro del apartado Turnado de Oficio    
                    idAux = 0;
                    maxIdHisto = 0;
                    //se verifica el id mÃ¡ximo de oficio a tratar (en este caso el tipo de prevención=4)
                    try {
                        maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");

                        for (Object o : maxDocsDgiraFlujoHist) {
                            idAux = Integer.parseInt(o.toString());
                            maxIdHisto = idAux.shortValue();
                        }
                    } catch (Exception xxx) {
                        //JOptionPane.showMessageDialog(null, "idDocumento:  " + xxx.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                    }
                    if (maxIdHisto > 0) {   //trae todo lo relaionado al id mÃ¡ximo de tabla DOCUMENTO_DGIRA_FLUJO_HIST, relativo a una bitacora
                        docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);
                        statusProy = daoBitacora.estatusTramBita(bitaProyecto);
                        try {
                            //estatus del oficio en cuestión 
                            statusDocRespDgira = daoDg.statusDocsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1", maxIdHisto);
                        } catch (Exception er) {
                            //JOptionPane.showMessageDialog(null, "idDocumento:  " + er.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }

                        for (Object o : statusDocRespDgira) {
                            statusDoc = Integer.parseInt(o.toString());
                        }
                    }
                    
                // pool delegaciones
                
                HistorialCIS303 = daoBitacora.numRegHistStatus(bitaProyecto,"CIS303");
                if (HistorialCIS303.size() == 1) //si el tamaÃ±o es igual a dos, indica que el trámite ya esta activo despues de haber sido prevenido
                { dePrevenc = true; }
                else { dePrevenc = false; }
                System.out.println("dePrevenc: " + dePrevenc);
                if(dePrevenc) //si ya viene de una reactivación por prevención, ya no puede guardar check list y no puede generar oficio de prevención
                { 
                    turnaDocsBtn = true; //ocultar bootn de turnado
                    URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                }
                else
                {
                    //JOptionPane.showMessageDialog(null,  "area envio: " + docsDgiraFlujoHistEdit.getIdAreaEnvioHist() + "   maxIdHisto: " + maxIdHisto , "Error", JOptionPane.INFORMATION_MESSAGE);
                    if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
                        
                    	String perfilUsr = "";
                    	try { // pool delegaciones
                            areaResiveDocCon = daoBitacora.jerarquiaTbArea(docsDgiraFlujoHistEdit.getIdAreaEnvioHist(), idEntidad);
                            //JOptionPane.showMessageDialog(null, "idDocumento:  " +areaResiveDocCon.size(), "Error", JOptionPane.INFORMATION_MESSAGE);
                        } catch (Exception er2) {
                            //JOptionPane.showMessageDialog(null, "err:  " + er2.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE); 
                        }
                        for (Object o : areaResiveDocCon) {
                            jerarquia = o.toString();
                        }
                        // pool delegaciones
                        if(docsDgiraFlujo == null){
                        	// en este caso obtenemos el documento dgira porque no se ha consultado
                            docsDgiraFlujo = daoDg.docsFlujoDgira(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
                        }
                        
                        try{
                        	perfilUsr = docsDgiraFlujo.getRolId().getNombreCorto().trim();
                        }catch(NullPointerException exnp){
                        	perfilUsr = jerarquia.toLowerCase();
                        }
                        
                        // el statusDoc y estatus del documento_dgira_flujo debe ser igual ya que al turnar setea el estatus en ambos registros ( padre y detalle )
                    	perfilSig = AppSession.siguienteRol(perfilUsr, docsDgiraFlujo.getEstatusDocumentoId().getEstatusDocumentoId());
                    	//perfilSig = AppSession.siguienteRol(perfilUsr, statusDoc);
                        System.out.println("perfilSig :" + perfilSig);
                        

                        //si el perfil en session es igual al perfil al que se le envio el oficio, perimite la edición del oficio
                        if (sesion.getAttribute("rol").toString().equals(perfilSig)) {
                            turnaDocsBtn = false;//visualizar boton de turnado
                            
                            if(sesion.getAttribute("rol").toString().equals("sd")) //solo el SD puede modificar y generar oficio de prevención
                            { 
                                URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; 
                            }
                            else
                            {
                                URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                            }
                            
                            //URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; //solo visualiza PDF y no lo modifica
                        } else {
                            turnaDocsBtn = true; //ocultar bootn de turnado
                            URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                            turnaDocs = true;
                        }
                    } else {
                        if(requisitosNull.size() == requisitos.size()) //para cuando aun no se ha guardado nada en el checkList
                        {
                            URLgenOficPre = ""; //solo abre una hoja en blanco
                        }
                        if(requisitosNo.size() > 0) //para cuando si se ha guardado que falatn requisitos en el checkList
                        {
                            //URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; 
                            if(sesion.getAttribute("rol").toString().equals("sd")) //solo el SD puede modificar y generar oficio de prevención
                            { 
                                URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; 
                            }
                            else
                            {
                                URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                            }
                        }
                        turnaDocsBtn = false;//visualizar boton de turnado
                    }
                }
            //------------------------------------------------------------
                } else {
                    ctxt.addMessage("growl", new FacesMessage("El turnado de oficio de prevención fallo, intente nuevamente."));
                }
            }

            //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
        }
    }
	
	
	/***
     * Metodo que realiza un guardado del documento, cambiandole la situacion correspondiente a la firma o corrección.
     * Este es unico para el DG
     * @throws IOException
     * @throws DocumentException
     */
    public void turnarcorregirguardarDG() throws IOException, DocumentException
    {
    	
    	System.out.println("\n\nturnarcorregirguardarDG..");  	   	
    	
        //tipoDoc=7;
          suspPorInfoAdic(tipoDoc);
          comenatriosTab = daoBitacora.tablabOsevOfic(bitaProyecto, tipoDoc,tipoDocId, idEntidad);
          System.out.println("comenatriosTab SegVíasGuradado 2 size: " + comenatriosTab.size());
            
        turnaDocsBtn = true; //ocultar bootn de turnado
        URLgenOficPre = Utils.ruta_servidor_hostserver + Utils.ruta_archivo_verpdf + "?bitacora="+bitaProyecto+"&clavetramite="+cveTipTram+"&idtramite="+idTipTram+"&tipodoc="+tipoDoc+"&entidadfederativa="+edoGestTram+"&documentoid=1";
        urlOfic = URLgenOficPre;
        
    }
	
	
    @SuppressWarnings("unused")
	public void suspPorInfoAdic(short tipoOfic)
    {

        int banderaNew = 0;
        int error = 0;
        Integer valInt = 0;
        idAux = 0;
        maxIdHisto = 0;
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitaProyecto);
        Historial remitente;
        VisorDao daoV = new VisorDao();
        Proyecto proy = null;
        Historial situacATOO22 = null;
        Historial situacI00010 = null;
        Integer resultado = 0;
        Integer resultVersion=-1;
        tipoDoc = tipoOfic;
        Boolean exitoCambSituac = false;
        //tipoDoc = 4 Oficio de prevencion
        //tipoDoc = 7 Oficio de Información adicional
        Integer soloTurnOfic=-1;
        SegundasViasView segundasViasView = new SegundasViasView("");                
        FacesContext ctxt = FacesContext.getCurrentInstance();
        
            if(bitaProyecto != null)
            {
                
                    //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------

                    maxDocsDgiraFlujoHist = daoDg.maxDocsDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    for (Object o : maxDocsDgiraFlujoHist) {
                        idAux =  Integer.parseInt(o.toString());
                        maxIdHisto = idAux.shortValue();
                    }

                    docsDgiraFlujoHistEdit = daoDg.docsFlujoDgiraHisto(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                    
                    if(docsDgiraFlujoHistEdit != null && maxIdHisto > 0)
                    {            
                        docsDgiraFlujoHistEdit.setIdAreaRecibeHist(idArea.trim());
                        docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(idusuario);
                        docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
                        daoDg.modifica(docsDgiraFlujoHistEdit);
                    }


                    //-------------------guardado en tabla docsDgiraFlujo, si existe el registro se actualiza de lo contrario se crea---------------
                    docsDgiraFlujo = daoDg.docsFlujoDgira(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);

                    if (docsDgiraFlujo != null) {
                        if (docsDgiraFlujo.getDocumentoDgiraFlujoPK() != null) {
                            banderaNew = 0;
                        }// registro nuevo
                        else {
                            banderaNew = 1;
                        }

                    } else {
                        banderaNew = 1;
                    }

                    if (banderaNew == 1) {
                        docsDgiraFlujo = new DocumentoDgiraFlujo(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId);
                    }

                    catEstatusDoc = daoDg.idCatEstatusDoc(idDocumento);
                    docsDgiraFlujo.setIdAreaEnvio(idArea.trim());
                    docsDgiraFlujo.setIdUsuarioEnvio(idusuario);
                    docsDgiraFlujo.setDocumentoDgiraFechaEnvio(new Date());
                    docsDgiraFlujo.setIdAreaRecibe(null);
                    docsDgiraFlujo.setIdUsuarioRecibe(null);
                    docsDgiraFlujo.setDocumentoDgiraFechaRecibe(null);
                    docsDgiraFlujo.setEstatusDocumentoId(catEstatusDoc);
                    docsDgiraFlujo.setDocumentoDgiraObservacion(comentarioTurnado);
                    
                 // obtengo el UsuarioRol del rol seleccionado por el usuario para entrar a la bitacora ( de la lista desplegable )
                    UsuarioRol ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
//            		System.out.println("\ninformación de usuario y area con el que entró a la bitacora:");
//            		System.out.println("usuario :" + ur.getUsuarioId().getIdusuario());
//            		System.out.println("idArea  :" + ur.getUsuarioId().getIdarea().trim());
//            		System.out.println("rol     :" + ur.getRolId().getNombreCorto());
//            		System.out.println("rolId     :" + ur.getRolId().getIdRol());
            		
            		// Obtendo el CatRol en base al rolId del UsuarioRol seleccionado de la lista desplegable
                    CatRol rol = daoBitacora.getRolPorId(ur.getRolId().getIdRol());
                    
                    // seteo el rolId con el que esta el usuario enviando el documento ( turnar y/o corregir )
                   	docsDgiraFlujo.setRolId(rol);

                    try {
                        if (banderaNew == 1) {
                            daoDg.agrega(docsDgiraFlujo);
                        }
                        if (banderaNew == 0) {
                            daoDg.modifica(docsDgiraFlujo);
                        }

                        maxIdHisto = (short) (idAux + 1);
                        docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(getBitaProyecto(), getCveTipTram(), getIdTipTram(), tipoDoc, getEdoGestTram(), tipoDocId, maxIdHisto);
                        docsDgiraFlujoHist.setIdAreaEnvioHist(idArea.trim());
                        docsDgiraFlujoHist.setIdUsuarioEnvioHist(idusuario);
                        docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
                        docsDgiraFlujoHist.setIdAreaRecibeHist(null);
                        docsDgiraFlujoHist.setIdUsuarioRecibeHist(null);
                        docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
                        docsDgiraFlujoHist.setEstatusDocumentoIdHist(catEstatusDoc);
                        docsDgiraFlujoHist.setDocumentoDgiraObservHist(comentarioTurnado);
                        try {
                            daoDg.agrega(docsDgiraFlujoHist);
                        } catch (Exception ex) {
                            error = error + 1;
                        }
                    } catch (Exception e) {
                        error = error + 1;
                    }

                    if (error == 0) 
                    {
                        System.out.println("Terminó turnado de oficio" );

                        if(idDocumento == 4) //si el estatus del documento es 4=frimado
                        {
                            if(tipoDoc == 7)//si es oficio de Información adicional
                            {
                                tipoSuspencion = 1;
                                tipoApertura=2;
                                System.out.println("Comienza turnado de Oficio de Info. Adic. idDocumento: " + idDocumento);
                                
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situación");
                                    //Situaciones para colocar tramite en situación de suspencion por Información adicional                                    
                                    if(cambioSituacion("AT0031","I00010", getBitaProyecto()) == 1)//eva-sd  
                                    {
                                        resultado = resultado + 1;
                                        if(cambioSituacion("AT0032","AT0027", getBitaProyecto()) == 1)//sd-da
                                        {
//                                            resultado = resultado + 1;
//                                            if(cambioSituacion("AT0004","dadg") == 1) //da-dg 
//                                            {
                                                resultado = resultado + 1;
                                                if(cambioSituacion("AT0008","dg", getBitaProyecto()) == 1)//dg-dg 
                                                {  
                                                    resultado = resultado + 1;
                                                    if(cambioSituacion("ATIF01","dgecc", getBitaProyecto()) == 1)//dg-ecc 
                                                    {
                                                        resultado = resultado + 1;                                                                
                                                    }                                                    
                                                }
//                                            }
                                        }
                                    }
                                    if(resultado == 4)
                                    { 
                                        try
                                        {
                                            if(folio!=null)
                                            {
                                                
                                                //CREACION DE VERSION 3 DE TRAMITE, solo aplica para oficio de Información adicional
                                                //si el resultVersion es 0= bien 1=mal
                                                resultVersion = segundasViasView.procVersiones(folio, (short)3);

                                                ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                                if(resultVersion == 0)
                                                { 
                                                    exitoCambSituac = true; 
                                                    System.out.println("Termino ingreso de version 3 de la MIA");
                                                }
                                                else {
                                                    exitoCambSituac = false; 
                                                    System.out.println("No Termino ingreso de version 3 de la MIA");
                                                }
                                                
                                            }
                                            else
                                            {
                                                exitoCambSituac = true; 
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            System.out.println("Problema con inserción de situaciones: " + ex.getMessage());
                                            ctxt.addMessage("growl", new FacesMessage("Trámite no turnado, intentelo mas tarde."));
                                            exitoCambSituac = false; 
                                        }
                                        
                                        
                                    }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5)//si es oficio de prevencion (3=pago de derechos  4=requisitos cofemer)
                            {
                                tipoSuspencion = 2;
                                tipoApertura=1;
                                System.out.println("Comienza turnado de Oficio de prevención idDocumento: " + idDocumento);
                                if (bitacora2 != null) 
                                {
                                    System.out.println("comienza cambios de situación");
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("W")) // tramites electronicos
                                    {
                                        //es -- por que se turna asi mismo
                                        if(cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)//if(cambioSituacion("AT0022","I00008") == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    if(bitacora2.getBitaTipoIngreso().trim().equals("-")) //tramites fisicos
                                    {
                                        //es -- por que se turna del SD al ECC
                                        if(cambioSituacion("AT0022","sdecc", getBitaProyecto()) == 1)
                                        {
                                            resultado = resultado + 1;
                                        } 
                                    }
                                    
                                    
                                    if(resultado == 1)
                                    { exitoCambSituac = true; }
                                    else
                                    { exitoCambSituac = false; }
                                }
                            }
                            if(tipoDoc == 1 || tipoDoc == 2 ||tipoDoc == 8 || tipoDoc == 9 || tipoDoc == 10 || tipoDoc == 11)// estos oficios no van a segundas vias ni afectan el flujo del PEIA
                            {
                                exitoCambSituac = true;
                            }
                            
                            soloTurnOfic = 0;
                        }
                        else // para cuando no se trata del firmado del documento
                        {
                            soloTurnOfic = 1;
                        }
                        
                        
                        if(exitoCambSituac)
                        {
                            if(folio!=null)
                            {
                                if(tipoDoc == 7 || tipoDoc == 4 || tipoDoc == 3 || tipoDoc == 5) //segundas vias solo aplica a oficio de info. adicional y de prevencion
                                {
                                    //comienza tramite de segundas vias  
                                    try { //1=iNFORMACION ADICIONAL 2=PREEVENCION
                                        segundasVias.proc(bitaProyecto, tipoSuspencion,folio,tipoApertura,tipoDoc);
                                        ctxt.addMessage("growl", new FacesMessage("Trámite turnado. Trámite notificado a promovente."));
                                        System.out.println("Termino de segundas vías");                                

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, inténtelo mas tarde."));
                                        System.out.println("Problema con segundas vías: " + e.getMessage());
                                    }
                                }
                                else
                                {ctxt.addMessage("growl", new FacesMessage("oficio firmado exitosamente"));}
                            }
                            else
                            {
                                ctxt.addMessage("growl", new FacesMessage("Trámite turnado exitosamente."));
                            }
                        }
                        else
                        {
                            if(soloTurnOfic==0) //si se trata de turnado de tramite en esatdo de firma de DG
                            {
                                System.out.println("Problema con cambio de situación de trámite, no se corrio procedimeinto de segundas vías");
                                ctxt.addMessage("growl", new FacesMessage("Oficio no turnado, intentelo mas tarde.Trámite no notificado a promovente."));
                            }
                            else//si se trata de turnado de tramite en esatdo de NO firma de DG
                            {
                                System.out.println("Se completo turnado de oficio (No es situación de firmado de DG)");
                                ctxt.addMessage("growl", new FacesMessage("Oficio turnado exitosamente."));
                            }
                        }
                        //FacesContext.getCurrentInstance().addMessage("messages", message);
                        turnaDocsBtn = true; 
                    } else {
                        ctxt.addMessage("growl", new FacesMessage("El turnado del oficio falló, intente nuevamente."));
                    }
            }
        
    }
	
    
    
    /***
     * Metodo que realiza un guardado del documento, cambiandole la situacion correspondiente a la firma o corrección
     * @param situacion
     * @param situacAnterior
     * @return
     */
	@SuppressWarnings("unused")
	public Integer cambioSituacion(String situacion, String situacAnterior, String bitacora) 
    {
        //situacion: Situacion a la que se desea cambiar el tramite
        //situacAnterior: Situacion en donde el tramite fue turnado, de aqui se obtiene el idAreaEnvia y idUsuarioEnvio(a quien se le habilitara el tramite cunado SINATEC libere el tramite de prenencion o algun paro de reloj )
        Historial remitente; ///idArea idusuario
        Historial remitente2;
        Integer bandera=0;
        String idAreaRecibe="";
        bitacora2 = (Bitacora) daoBitacora.busca(Bitacora.class, bitacora);
        DocumentoDgiraFlujoHist docsDgiraFlujoHist2;
        
        if (bitacora != null && bitacora2!= null) 
        {
            try 
            {       
                if(!situacAnterior.equals("--"))
                { //02000000
                    if(situacAnterior.equals("dg") || situacAnterior.equals("dadg") || situacAnterior.equals("dgecc") || situacAnterior.equals("sdecc"))
                    {
                        if(situacAnterior.equals("dg"))
                        {
                            if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                            {
                                idArea= idArea.trim();
                                //idusuario=idusuario;
                                idAreaRecibe =  idArea.trim();
                            }
                            else{
                                idArea= "02000000";
                                idusuario=3943;
                                idAreaRecibe =  "02000000";
                            }
                        }
                        if(situacAnterior.equals("dadg"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"AT0027");//
                            if(remitente != null)
                            {
                                idArea=remitente.getHistoAreaRecibe();
                                idusuario=remitente.getHistoIdUsuarioRecibe();   
                                                                
                                if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                { idAreaRecibe =  idArea.trim(); }
                                else
                                { idAreaRecibe = "02000000";  }
                            }
                        }
                        if(situacAnterior.equals("dgecc"))
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"CIS104");
                            if(remitente != null)
                            {
                                if(idArea.length()>0 && idusuario > 0 && sesion != null && sesion.getAttribute("rol").toString().equals("dg"))//idArea  idusuario
                                {
                                    idArea= idArea.trim();
                                    //idusuario=idusuario;
                                }
                                else
                                {
                                    idArea= "02000000";
                                    idusuario=3943;
                                }
                                idAreaRecibe = remitente.getHistoAreaEnvio(); 
                            }
                        }
                        if(situacAnterior.equals("sdecc")) //SD a ECC
                        { 
                            remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,"CIS104");
                            //traer el Id de usuario del subdirector
                            docsDgiraFlujoHist2 = daoDg.docsFlujoDgiraHisto(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, tipoDocId, (short)1 );
         
                            if(remitente != null)
                            {
                                idArea= docsDgiraFlujoHist2.getIdAreaEnvioHist() ;
                                idusuario = docsDgiraFlujoHist2.getIdUsuarioEnvioHist();                            
                                idAreaRecibe = remitente.getHistoAreaEnvio(); 
                            }
                        }
                            
                    }
                    else
                    {
                        remitente = (Historial) daoBitacora.datosProyHistStatus(bitaProyecto,situacAnterior);
                        if(remitente != null)
                        {
                            idArea=remitente.getHistoAreaRecibe();
                            if(remitente.getHistoIdUsuarioRecibe() != null)
                            idusuario=remitente.getHistoIdUsuarioRecibe(); 
                            idAreaRecibe = remitente.getHistoAreaEnvio(); 
                        }
                    }
                }
                else
                {idAreaRecibe = idArea;}//para situaciones AT0002, AT0003 y AT0011
                //FacesMessage message=null;                   
                sinatDgira = new SinatDgira();
                sinatDgira.setId(0);
                sinatDgira.setSituacion(situacion);
                sinatDgira.setIdEntidadFederativa(bitacora2.getBitaEntidadGestion());
                sinatDgira.setIdClaveTramite(bitacora2.getBitaTipotram()); //STRING BITA_TIPOTRAM
                sinatDgira.setIdTramite(bitacora2.getBitaIdTramite());
                sinatDgira.setIdAreaEnvio(idArea.trim());
                sinatDgira.setIdAreaRecibe(idAreaRecibe.trim());
                sinatDgira.setIdUsuarioEnvio(idusuario);
                //sinatDgira.setIdUsuarioRecibe(1340);
                sinatDgira.setGrupoTrabajo(new Short("0"));
                sinatDgira.setBitacora(bitacora);
                sinatDgira.setFecha(new Date());

                BitacoraDao bdao = new BitacoraDao();
                bdao.agrega(sinatDgira);
                
                
                //message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Trámite turnado correctamente.", null);                    
                System.out.println("Terminó turnado exitosamente, situación: " + situacion);               
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                bandera = 1;                
            } catch (Exception er) {
                bandera = 0;
                er.printStackTrace();
                //FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No se ha podido turnar el tramite, intente mas tarde.", null);
                //FacesContext.getCurrentInstance().addMessage("messages", message);
                System.out.println("Error en turnado a situación " + situacion + " de trámite :  " + er.getMessage() );
            }
        }
        else
        {
            bandera = 0;
        }
        return bandera;
    }
    
    
    public SelectItem[] todoOficDoc() {
        List<DocumentoRespuestaDgira> lista;

        lista = daoDg.docsRespDgiraBusq2(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");

        int size = lista.size() + 1;
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        items[0] = new SelectItem("0", "-- Seleccione --");
        i++;
        for (DocumentoRespuestaDgira x : lista) {
            documento = x;
            items[i++] = new SelectItem(x.getDocumentoNombre(), x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4));
        }
        return items;
    }

    public SelectItem[] getPrevOfic() {
        return combo;
    }

    public void observaciones() {
    }

    public void onCheckSubdirector(SelectEvent event) {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        ListaRequisitos eleSele = (ListaRequisitos) event.getObject();
        //System.out.println("listener onCheckSubdirector --> " + eleSele);
        /*
         try {
         //Si es 1, bandera activa de entregado a subdirector
         BigDecimal bdEntSD = (BigDecimal)eleSele.o[4];
         eleSele.entregadoSD = (bdEntSD.doubleValue() == 1);
         } catch (Exception e) {
         eleSele.entregadoSD = false;
         }
         */
        ctxt.addMessage("growl", new FacesMessage("Se borrarán las observaciones capturadas del requisito."));
        if (requisitosSi.size() == requisitosImg.size()) { 
            if(dePrevenc) //este mensaje es cuando el trámite no ha sido prevenido
            {
                ctxt.addMessage("growl", new FacesMessage("Ha validado el último requisito sin observaciones, la prevención ya no se encuentra disponible."));
            }
            else //este mensaje es cuando el trámite YA ha sido prevenido y reactivado
            {
                ctxt.addMessage("growl", new FacesMessage("Ha validado el último requisito sin observaciones."));
            }
        }
        eleSele.obsPre = "";
        eleSele.entregadoSD = true;
    }

    public void onUncheckSubdirector(UnselectEvent event) {
        ListaRequisitos eleSele = (ListaRequisitos) event.getObject();
        //System.out.println("listener onUncheckSubdirector --> " + eleSele);

        eleSele.entregadoSD = false;
    }

    public void onSelectAll(AjaxBehaviorEvent event) {
        //boolean bSel = event.selected;
        System.out.println("listener onSelectAll");
    }

    public void desicionTurnado() throws Exception {
        Integer resp1 = -1;
        Integer resp2 = -1;
        Boolean respFinal = false;
        FacesContext ctxt = FacesContext.getCurrentInstance();
        HistorialI00010 = daoBitacora.numRegHistStatus(bitaProyecto, "I00010");

        if (turnaEva.equals("S")) {
            if(HistorialI00010.isEmpty())
            {
                //turnado a Integración del expediente
                resp1 = cambioSituacion("AT0002", "--", getBitaProyecto());
            }
            //turnado a evaluación
            resp2 = cambioSituacion("AT0003", "--", getBitaProyecto());
            if (resp1 == 1 && resp2 == 1) {
                respFinal = true;
                System.out.println("tramite turnado a evaluación, con integración de expediente");
            } else {
                respFinal = false;
            }
        }
    
        if (respFinal) {
            ctxt.addMessage("growl", new FacesMessage("Trámite turnado correctamente."));
        } else {
            ctxt.addMessage("growl", new FacesMessage("Error al turnar el trámite, intente de nuevo."));
        }
    }
    
    public void desicionTurnadoNoInteg() throws Exception {
        Integer resp1 = -1;
        Integer resp2 = -1;
        Boolean respFinal = false;
        FacesContext ctxt = FacesContext.getCurrentInstance();

        if (turnaEva.equals("S")) {
            //turnado a NO Integración del expediente
            resp1 = cambioSituacion("AT0011", "--", getBitaProyecto());
            //turnado a evaluación
            resp2 = cambioSituacion("AT0003", "--", getBitaProyecto());
            if (resp1 == 1 && resp2 == 1) {
                respFinal = true;
                System.out.println("tramite turnado a evaluación ");
            } else {
                respFinal = false;
            }
        }
 
        if (respFinal) {
            ctxt.addMessage("growl", new FacesMessage("Trámite turnado correctamente."));
        } else {
            ctxt.addMessage("growl", new FacesMessage("Error al turnar el trámite, intente de nuevo."));
        }
    }

	public Tbusuario getUsuariorecibe() {
		return usuariorecibe;
	}

	public void setUsuariorecibe(Tbusuario usuariorecibe) {
		this.usuariorecibe = usuariorecibe;
	}

	public String getUsuariorecibetxt() {
		return usuariorecibetxt;
	}

	public void setUsuariorecibetxt(String usuariorecibetxt) {
		this.usuariorecibetxt = usuariorecibetxt;
	}

	public Boolean getDesechamiento() {
		return desechamiento;
	}

	public void setDesechamiento(Boolean desechamiento) {
		this.desechamiento = desechamiento;
	}
	
	public int getIsUrlDG() {
		return isUrlDG;
	}

	public void setIsUrlDG(int isUrlDG) {
		this.isUrlDG = isUrlDG;
	}

	public int getBandeja() {
		return bandeja;
	}

	public void setBandeja(int bandeja) {
		this.bandeja = bandeja;
	}

	public DetalleProyectoView getDetalleView() {
		return detalleView;
	}

	public void setDetalleView(DetalleProyectoView detalleView) {
		this.detalleView = detalleView;
	}

	
	
    
    
    

}
