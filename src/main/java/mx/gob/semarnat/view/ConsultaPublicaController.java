package mx.gob.semarnat.view;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import mx.gob.semarnat.dao.MenuDao;

import mx.gob.semarnat.mia.servicios.ServicioConsulta;

@ManagedBean(name = "ConPub")
@ViewScoped
public class ConsultaPublicaController {
	public ServicioConsulta servicioConsulta;
	private List<ConsultaPublicaHelper> list;
	private List<Object[]> listObj;
        private final MenuDao MenuDao= new MenuDao();
	
	@PostConstruct
	public void init() {
		setListObj();
	}
	
	public List<ConsultaPublicaHelper> getList(){
		return list;
	}

	public void setListObj() {
		servicioConsulta = new ServicioConsulta();
		list = new ArrayList<>();
		this.listObj = new ArrayList<>();
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String bitacoraNom = req.getParameter("numBitacora");
                
		ConsultaPublicaHelper cph;
		if (bitacoraNom == null) {
			bitacoraNom = req.getParameter("bitaNum");
		}
                String cve = MenuDao.cveTramite(bitacoraNom);
		try{
			this.listObj = servicioConsulta.getAllConsultas(bitacoraNom, cve);
			if(!listObj.isEmpty() && !Objects.equals(listObj, null)){
				for (Object[] object : listObj) {
					if(!object[0].equals("") && !object[1].equals("")){
						cph = new ConsultaPublicaHelper(object[0].toString(), object[1].toString(), object[2].toString(), Integer.parseInt(object[3].toString()), Integer.parseInt(object[4].toString()));
						list.add(cph);
					} 					
				}
			}
		}catch (NullPointerException e){
			e.printStackTrace(); 
		}
		System.out.println(list.size() + "<:::::::::::::::::::::");
	}

	public void setList(List<ConsultaPublicaHelper> list) {
		this.list = list;
	}


	@SuppressWarnings("serial")
	public class ConsultaPublicaHelper implements Serializable{
		String id;
		String remitente;
		String fecha;
		String imagen;
		
		
		public ConsultaPublicaHelper(String id, String remitente, String fecha, int flag, int imagen) {
			super();
			this.id = id;
			this.remitente = remitente;
			this.fecha = fecha;
			if (flag == 0)
				this.imagen ="dataComplete.jpg";
			else if (imagen > 0)
				this.imagen ="dataComplete.jpg";
			else
				this.imagen ="dataInComplete.jpg";
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getRemitente() {
			return remitente;
		}
		public void setRemitente(String remitente) {
			this.remitente = remitente;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		public String getImagen() {
			return imagen;
		}
		public void setImagen(String imagen) {
			this.imagen = imagen;
		}
		
	}
}