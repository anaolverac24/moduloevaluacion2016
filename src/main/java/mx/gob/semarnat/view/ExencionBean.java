package mx.gob.semarnat.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import mx.go.semarnat.services.ServicioDocument;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.BitacoraRequisitos;
import mx.gob.semarnat.dao.ExencionDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.mia.servicios.ServicioTokenGO;
import mx.gob.semarnat.mia.servicios.dgiramiae.ServicioArchivosProyecto;
import mx.gob.semarnat.mia.servicios.dgiramiae.ServicioExencionRequisitosEvaluador;
import mx.gob.semarnat.mia.servicios.evaluacion.ServicioBitacora_Proyecto;
import mx.gob.semarnat.model.bitacora.SemaforoBitacora;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.Document;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.dgira_miae.ExencionRequisitosEvaluador;
import mx.gob.semarnat.model.dgira_miae.ExencionRequisitosEvaluadorPk;
import mx.gob.semarnat.model.seguridad.TokenGO;
import mx.gob.semarnat.ws.WSRecuperaArchivos;

@ManagedBean(name = "exencionBean")
@ViewScoped
public class ExencionBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private final short SERIAL_UNO = 1;
    // Secciones
    private final short SECC_REV_SOL = 1;
    private final short SECC_AL_PAG_DER = 2;
    private final short SECC_AC = 3;
    private final short SECC_AN = 4;
    private final short SECC_RE = 5;
    //
    private final short SECC_REV_SOL_REQ_REV_SOL = 1;
    private final short SECC_AL_PAG_DER_REQ_AL_PAG_DER = 1;
    private final short SECC_AC_REQ_AC = 1;
    private final short SECC_AC_REQ_AC_2 = 2;
    private final short SECC_AC_REQ_AC_3 = 3;
    private final short SECC_RE_REQ_RE = 1;
    //
    private final String DATA_ICON_EMPTY = "dataEmpty.jpg";
    private final String DATA_ICON_INCOMPLETE = "dataInComplete.jpg";
    private final String DATA_ICON_COMPLETE = "dataComplete.jpg";
    private List<String> respuestasAcreditacion;
    private String dataIconRevSol = DATA_ICON_EMPTY, dataIconValPago = DATA_ICON_EMPTY;
    private String dataIconAcredt = DATA_ICON_EMPTY, dataIconArchAne = DATA_ICON_EMPTY;
    private final String DESC_REQ_REV_SOL = "¿Los datos del formulario de la solicitud de exención están correctos?";
    private final String DESC_REQ_VAL_PAG_DER = "¿El pago que se evidencía en el comprobante de pago es el correcto para el trámite?";
    private final String DESC_REQ_ACR_1 = "Identificación oficial vigente para personas físicas y representantes legales. Original para cotejo y copia simple.";
    private final String DESC_REQ_ACR_2 = "Acta constitutiva para el caso de personas morales. Original o copia certificada y copia simple para cotejo.";
    private final String DESC_REQ_ACR_3 = "Original o copia certificada y copia simple para cotejo del documento con el cual se acredita la representación legal del promovente. Para el caso de personas físicas: carta poder firmada ante dos testigos. Para el caso de personas morales; Poder notarial, solo en el caso de que la representación o las actuaciones para los que se encuentre facultado no se encuentren en el Acta Constitutiva.";
    private Integer idusuario;
    private String bitacoraNom;
    private long idExencion;
    private String img = "V.jpg";
    //
    private final VisorDao daoVisor = new VisorDao();
    private final ExencionDao exencionDao = new ExencionDao();

    private String cveProyecto;
    private String bitaProyecto;
    private String fechaFofiIngre;
    private String folioProyecto;
    private String promoTram;
    private String nombProyecto;
    private String situaTram;
    private String tipoTramite = "EXENCION";
    private String subsector;
    private int diasProceso;
    private int diasTramite;
    //
    private boolean digital;
    private boolean dePrevencion;
    private List<RevisionSolicitud> listaRevisionSolicitud;
    private List<ValidacionPagoDerecho> listaValidaPagoDerecho;
    private List<Acreditacion> listaAcreditacion;
    private List<ArchivoAnexo> listaArchivoAnexo;
    private String redaccionResolutivo;
    private short serial;
    private ServicioBitacora_Proyecto sbp;
    private ServicioExencionRequisitosEvaluador sere;
    private ServicioArchivosProyecto sap;

    private String document_id;
    private String template;
    private SemaforoBitacora semaforoBitacora;
    private BitacoraDao bitacoraDao;
    private boolean saveEnabled = false;
    private final BitacoraDao daoBitacora = new BitacoraDao();
    
    private BitacoraRequisitos bq;

    @PostConstruct
    public void init() {
        bitacoraDao = new BitacoraDao();
        FacesContext fContext = FacesContext.getCurrentInstance();
        idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
        String rol = (String) fContext.getExternalContext().getSessionMap().get("rol");
        if (rol != null && (rol.equalsIgnoreCase("eva") || rol.equalsIgnoreCase("sd"))) {
            saveEnabled = true;
        }
        //
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        bitacoraNom = req.getParameter("bitaNum");
        bitaProyecto = req.getParameter("bitaNum");
        bitacoraDao = new BitacoraDao();

        fContext.getExternalContext().getSessionMap().put("bitacoraProy", bitaProyecto);
        //
        sbp = new ServicioBitacora_Proyecto();
        sere = new ServicioExencionRequisitosEvaluador();
        sap = new ServicioArchivosProyecto();
        try {
            serial = SERIAL_UNO;
            Object[] datosExencionIdFolio = sere.encontrarExencionIdPorBitacora(bitaProyecto, serial);
            BigDecimal auxExe = (BigDecimal) (datosExencionIdFolio != null ? datosExencionIdFolio[0] : null);
            BigDecimal auxFol = (BigDecimal) (datosExencionIdFolio != null ? datosExencionIdFolio[1] : null);
            idExencion = (long) (auxExe != null ? auxExe.longValue() : 0);

            folioProyecto = (String) (auxFol != null ? auxFol.toString() : "");
            respuestasAcreditacion = cargarRspuestasAcreditacion();
            //
            cargarCabecera();
            bq = new BitacoraRequisitos();
            
            String cveTipTram = daoBitacora.cveTipTram(bitaProyecto);
            Integer idTipTram = daoBitacora.idTipTram(bitaProyecto);
            
            bq.generaRequisitos(bitacoraNom, folioProyecto, cveTipTram, idTipTram);
            digital = bq.isDigital();
            dePrevencion = false;
            cargarRevisionSolicitud();
            cargarValidacionPagoDerechos();
            cargarAcreditacion();
            cargarArchivosAnexos();
            cargaRedaccionResolutivo();
            semaforoBitacora = new SemaforoBitacora(bitacoraDao.obtenerSemaforosProcesoExencion(bitacoraNom).get(0));

            fContext.getExternalContext().getSessionMap().put("folioProyecto", folioProyecto);

            System.out.println("ID Exencion :: " + idExencion);
            System.out.println("folioProyecto :: " + folioProyecto);

        } catch (MiaExcepcion e) {
            e.printStackTrace();
        }

        System.out.println("Fin exencionBean");
    }

    private void cargaRedaccionResolutivo() {
        redaccionResolutivo = "";
        // Siempre se obtendrá 1 solo registro de la bd
        List<ExencionRequisitosEvaluador> l = sere.encontrarPorIdExencionPorSeccPorReq(idExencion, SECC_RE, SECC_RE_REQ_RE, serial);
        if (l != null && !l.isEmpty()) {
            ExencionRequisitosEvaluador ere = l.get(0);
            redaccionResolutivo = ere.getObservaciones();
        }

    }

    private void cargarArchivosAnexos() throws MiaExcepcion {
        List<ArchivosProyecto> listaArchivosAnexo = sap.encontrarPorFolioPorSerialPorCapSubSecApa(folioProyecto, serial, (short) 1, (short) 0, (short) 0, (short) 4);
        List<ArchivosProyecto> listaArchivosInforme = sap.encontrarPorFolioPorSerialPorCapSubSecApa(folioProyecto, serial, (short) 1, (short) 0, (short) 0, (short) 0);

        List<ArchivosProyecto> listaArchivos = new ArrayList<>();
        if (listaArchivosAnexo != null && !listaArchivosAnexo.isEmpty()) {
            listaArchivos.addAll(listaArchivosAnexo);
        }
        if (listaArchivosInforme != null && !listaArchivosInforme.isEmpty()) {
            listaArchivos.addAll(listaArchivosInforme);
        }

        listaArchivoAnexo = new ArrayList<>();

        if (!listaArchivos.isEmpty()) {
            List<Short> idsReq = new ArrayList<>();
            for (ArchivosProyecto ap : listaArchivos) {
                idsReq.add(ap.getSeqId());
            }
            // Existen archivos adjunos
            List<ExencionRequisitosEvaluador> l = sere.encontrarPorIdExencionPorSeccPorNReq(idExencion, SECC_AN, idsReq, serial);
            if (l != null && !l.isEmpty()) {
                for (ArchivosProyecto ap : listaArchivos) {
                    short idR = ap.getSeqId();
                    // Buscar si el requerimiento existe
                    boolean existe = false;
                    ArchivoAnexo aa = new ArchivoAnexo(ap.getNombre() + " " + ap.getDescripcion());
                    aa.setIdSec(SECC_AN);
                    aa.setIdReq(idR);
                    aa.setPos(listaArchivoAnexo.size() + 1);
                    aa.setUrl(ap.getUrl());
                    //
                    for (ExencionRequisitosEvaluador ere : l) {
                        if (idR == ere.getExencionRequisitoEvaluadorPK().getnRequisito()) {
                            //
                            aa.setObservaciones(ere.getObservaciones());
                            aa.setRespuesta(ere.getRespuesta());
                            //
                            existe = true;
                            break;
                        }
                    }
                    //
                    if (!existe) {
                        aa.setObservaciones("");
                        aa.setRespuesta("-1");
                    }
                    //
                    aa.setExiste(existe);
                    listaArchivoAnexo.add(aa);
                }
            } else {
                System.out.println("ExencionBean::cargarArchivosAnexos exencionRequisitosEvaluador Ningun requisito se ha dado de alta");
                // Ningún requisito se ha dado de alta
                for (ArchivosProyecto ap : listaArchivos) {
                    short idR = ap.getSeqId();
                    ArchivoAnexo aa = new ArchivoAnexo(ap.getNombre() + " " + ap.getDescripcion());
                    aa.setIdSec(SECC_AN);
                    aa.setIdReq(idR);
                    aa.setObservaciones("");
                    aa.setRespuesta("-1");
                    aa.setPos(listaArchivoAnexo.size() + 1);
                    aa.setUrl(ap.getUrl());
                    listaArchivoAnexo.add(aa);
                }
            }
        }
        // Si no existieran adjuntos, no se le muestra nada al evaluador

    }

    private void cargarAcreditacion() {
        // Se pueden obtener hasta 3 registros de la base de datos
        List<Short> idsReq = new ArrayList<>();
        idsReq.add(SECC_AC_REQ_AC);
        idsReq.add(SECC_AC_REQ_AC_2);
        idsReq.add(SECC_AC_REQ_AC_3);
        //
        List<ExencionRequisitosEvaluador> l = sere.encontrarPorIdExencionPorSeccPorNReq(idExencion, SECC_AC, idsReq, serial);
        listaAcreditacion = new ArrayList<>();
        try {

            if (l != null && !l.isEmpty()) {

                for (short idR : idsReq) {
                    // Buscar si el requerimiento existe
                    boolean existe = false;
                    Acreditacion acdt = new Acreditacion(obtenerDescripcionAcreditacion(idR));
                    acdt.setIdSec(SECC_AC);
                    acdt.setIdReq(idR);
                    acdt.setPos(idR);
                    List<ArchivosProyecto> result = sap.encontrarPorFolioPorSerialPorCapSubSecApa(folioProyecto, serial, (short) 1, (short) 0, (short) 0, (short) idR);
                    if (result.size() > 0) {
                        acdt.setUrl(result.get(0).getUrl());
                    }
                    //
                    for (ExencionRequisitosEvaluador ere : l) {
                        if (idR == ere.getExencionRequisitoEvaluadorPK().getnRequisito()) {
                            //
                            acdt.setObservaciones(ere.getObservaciones());
                            System.out.println("Se encontró una respuesta :: " + ere.getRespuesta());
                            acdt.setRespuesta(ere.getRespuesta());
                            //
                            existe = true;
                            break;
                        }
                    }
                    //
                    if (!existe) {
                        acdt.setObservaciones("");
                        acdt.setRespuesta("-1");
                    }
                    //
                    acdt.setExiste(existe);
                    listaAcreditacion.add(acdt);
                }
            } else {
                // Ningún requisito se ha dado de alta
                for (short idR : idsReq) {
                    Acreditacion acdt = new Acreditacion(obtenerDescripcionAcreditacion(idR));
                    acdt.setIdReq(idR);
                    acdt.setIdSec(SECC_AC);
                    acdt.setObservaciones("");
                    acdt.setRespuesta("-1");
                    acdt.setPos(idR);

                    List<ArchivosProyecto> result = sap.encontrarPorFolioPorSerialPorCapSubSecApa(folioProyecto, serial, (short) 1, (short) 0, (short) 0, (short) idR);
                    if (result.size() > 0) {
                        acdt.setUrl(result.get(0).getUrl());
                    }

                    listaAcreditacion.add(acdt);
                }
            }

        } catch (MiaExcepcion e) {
            e.printStackTrace();
        }

    }

    private String obtenerDescripcionAcreditacion(short idR) {
        String desc = "";
        switch (idR) {
            case SECC_AC_REQ_AC:
                desc = DESC_REQ_ACR_1;
                break;
            case SECC_AC_REQ_AC_2:
                desc = DESC_REQ_ACR_2;
                break;
            case SECC_AC_REQ_AC_3:
                desc = DESC_REQ_ACR_3;
                break;
        }
        return desc;
    }

    private List<String> cargarRspuestasAcreditacion() {
        respuestasAcreditacion = new ArrayList<>();
        String r1 = "Sí";
        String r2 = "No";
        String r3 = "No aplica";
        respuestasAcreditacion.add(r1);
        respuestasAcreditacion.add(r2);
        respuestasAcreditacion.add(r3);
        return respuestasAcreditacion;
    }

    private void cargarValidacionPagoDerechos() {
        // Siempre se obtendrá 1 solo registro de la bd
        List<ExencionRequisitosEvaluador> l = sere.encontrarPorIdExencionPorSeccPorReq(idExencion, SECC_AL_PAG_DER, SECC_AL_PAG_DER_REQ_AL_PAG_DER, serial);
        listaValidaPagoDerecho = new ArrayList<>();
        ValidacionPagoDerecho vpd = null;
        System.out.println("bitacoraNom"+bitacoraNom);
        if (l != null && !l.isEmpty()) {
            ExencionRequisitosEvaluador ere = l.get(0);
            vpd = new ValidacionPagoDerecho(DESC_REQ_VAL_PAG_DER, ere.getValidado());
            vpd.setIdReq(ere.getExencionRequisitoEvaluadorPK().getnRequisito());
            vpd.setIdSec(ere.getExencionRequisitoEvaluadorPK().getnSeccion());
            vpd.setObservaciones(ere.getObservaciones());
            vpd.setExiste(true);
        } else {
            vpd = new ValidacionPagoDerecho(DESC_REQ_VAL_PAG_DER, false);
            vpd.setIdReq(SECC_AL_PAG_DER_REQ_AL_PAG_DER);
            vpd.setIdSec(SECC_AL_PAG_DER);
            vpd.setObservaciones("");
        }
        //vpd.setPos(SECC_AL_PAG_DER_REQ_AL_PAG_DER);
        int data = digital ? sere.getReqTramiteDigital(bitacoraNom) : sere.getReqTramite(bitacoraNom);
        vpd.setPos(data);
    
        listaValidaPagoDerecho.add(vpd);
    }

    private void cargarRevisionSolicitud() {
        // Siempre se obtendrá 1 solo registro de la bd
        List<ExencionRequisitosEvaluador> l = sere.encontrarPorIdExencionPorSeccPorReq(idExencion, SECC_REV_SOL, SECC_REV_SOL_REQ_REV_SOL, serial);
        listaRevisionSolicitud = new ArrayList<>();
        RevisionSolicitud rs = null;
        if (l != null && !l.isEmpty()) {
            ExencionRequisitosEvaluador ere = l.get(0);
            rs = new RevisionSolicitud(DESC_REQ_REV_SOL, ere.getValidado());
            rs.setIdReq(ere.getExencionRequisitoEvaluadorPK().getnRequisito());
            rs.setIdSec(ere.getExencionRequisitoEvaluadorPK().getnSeccion());
            rs.setObservaciones(ere.getObservaciones());
            rs.setExiste(true);
        } else {
            rs = new RevisionSolicitud(DESC_REQ_REV_SOL, false);
            rs.setIdReq(SECC_REV_SOL_REQ_REV_SOL);
            rs.setIdSec(SECC_REV_SOL);
            rs.setObservaciones("");
        }
        //
        rs.setPos(SECC_REV_SOL_REQ_REV_SOL);
        rs.setIdExencion(idExencion);
        listaRevisionSolicitud.add(rs);
    }

    private void cargarCabecera() throws MiaExcepcion {
        Object[] o = sbp.encontrarPorBitacoraAlter(bitaProyecto);
        if (o != null) {
            cveProyecto = (String) o[0];
            nombProyecto = (String) o[1];
            promoTram = (String) o[2];
            subsector = String.valueOf(o[3]);
            BigDecimal auxDiasProceso = (BigDecimal) o[4];
            BigDecimal auxDiasTramite = (BigDecimal) o[5];
            diasProceso = auxDiasProceso.intValue();
            diasTramite = auxDiasTramite.intValue();
            situaTram = (String) o[6];
            fechaFofiIngre = (String) o[7];
            cargarImagenCabecera();
        }
    }

    private void cargarImagenCabecera() {
        switch (subsector) {
            case "Agropecuario": {
                img = "A.jpg";
                break;
            }
            case "Forestal": {
                img = "F.jpg";
                break;
            }
            case "Minero": {
                img = "M.jpg";
                break;
            }
            case "Pesquero": {
                img = "P.jpg";
                break;
            }
            case "Hidraulico": {
                img = "H.jpg";
                break;
            }
            case "Industrial": {
                img = "I.jpg";
                break;
            }
            case "Comunicaciones": {
                img = "V.jpg";
                break;
            }
            case "Desarrollo urbano": {
                img = "U.jpg";
                break;
            }
            case "Energia-Electricidad": {
                img = "E.jpg";
                break;
            }
            case "Energia-Gaseras": {
                img = "G.jpg";
                break;
            }
            case "Energia-Petroleo": {
                img = "X.jpg";
                break;
            }
            case "Turismo": {
                img = "T.jpg";
                break;
            }
            default: {
                img = "IP.jpg";
                break;
            }
        }

    }
    
    public StreamedContent getArchivoPago() {

        try {
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String strIdArchivo = (String) req.getParameter("idArchivo");

            File fArchivo = WSRecuperaArchivos.obtenerArchivo(Integer.parseInt(strIdArchivo));
            if (fArchivo != null) {
                InputStream stream = new FileInputStream(fArchivo);
                StreamedContent sFile = new DefaultStreamedContent(stream, "application/octet-stream", fArchivo.getName());

                return sFile;
            } else {

                return null;
            }

        } catch (Exception e) {

            return null;
        }
    }

    public StreamedContent getArchivo() {

        StreamedContent sc = null;

        System.out.println("value of id Exencion :: " + idExencion);
        String uploadDirectory = exencionDao.obtenerRutaSubida();
        System.out.println("Valueo of upload file :: " + uploadDirectory);
        String folioExencion = exencionDao.obtenerFolioByIdExencion(idExencion);

        String pdfExencion = uploadDirectory + folioExencion + File.separator + "Pve_SolicitudExcencion" + folioExencion + ".pdf";
        System.out.println("value of pdf path :: " + pdfExencion);

        File file = new File(pdfExencion);
        if (file != null) {
            InputStream is;
            try {
                is = new FileInputStream(file);
                sc = new DefaultStreamedContent(is, "application/pdf", file.getName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return sc;
    }

    public StreamedContent descargarArchivo(String url) {
        System.out.println("Url: " + url);
        StreamedContent sc = null;
        if (!url.equals("")) {
            File file = new File(url);
            InputStream is;
            try {
                is = new FileInputStream(file);
                sc = new DefaultStreamedContent(is, "application/octet-stream", file.getName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return sc;
    }

    // public void onCheckSubdirectorRevSol(SelectEvent event) {
    // RevisionSolicitud revSolSelect = (RevisionSolicitud) event.getObject();
    // // System.out.println("listener onCheckSubdirector --> " + eleSele);
    // /*
    // * try { //Si es 1, bandera activa de entregado a subdirector BigDecimal bdEntSD = (BigDecimal)eleSele.o[4]; eleSele.entregadoSD =
    // * (bdEntSD.doubleValue() == 1); } catch (Exception e) { eleSele.entregadoSD = false; }
    // */
    // // ctxt.addMessage("growl", new FacesMessage("Se borrarán las observaciones capturadas del requisito."));
    // // if (requisitosSi.size() == requisitosImg.size()) {
    // // if(dePrevenc) //este mensaje es cuando el trámite no ha sido prevenido
    // // {
    // // ctxt.addMessage("growl", new FacesMessage("Ha validado el último requisito sin observaciones, la prevención ya no se encuentra
    // // disponible."));
    // // }
    // // else //este mensaje es cuando el trámite YA ha sido prevenido y reactivado
    // // {
    // // ctxt.addMessage("growl", new FacesMessage("Ha validado el último requisito sin observaciones."));
    // // }
    // // }
    // // eleSele.obsPre = "";
    // // eleSele.entregadoSD = true;
    // revSolSelect.setValidado(true);
    // addMessage("growl", FacesMessage.SEVERITY_INFO, "Se borrarán las observaciones capturadas del requisito.");
    // }
    //
    // public void onUncheckSubdirectorRevSol(UnselectEvent event) {
    // RevisionSolicitud revSolSelect = (RevisionSolicitud) event.getObject();
    // // System.out.println("listener onUncheckSubdirector --> " + eleSele);
    //
    // // eleSele.entregadoSD = false;
    // revSolSelect.setValidado(false);
    // }
    //
    // public void onCheckSubdirectorValPagDer(SelectEvent event) {
    // ValidacionPagoDerecho valPagDerSelect = (ValidacionPagoDerecho) event.getObject();
    // valPagDerSelect.setValidado(true);
    // addMessage("growl", FacesMessage.SEVERITY_INFO, "Se borrarán las observaciones capturadas del requisito.");
    // }
    //
    // public void onUncheckSubdirectorValPagDer(UnselectEvent event) {
    // ValidacionPagoDerecho valPagDerSelect = (ValidacionPagoDerecho) event.getObject();
    // valPagDerSelect.setValidado(false);
    // }
    public void guardarRevSol(ActionEvent e) {
        for (RevisionSolicitud rs : listaRevisionSolicitud) {
            System.out.println("l1 " + rs.isValidado());
        }
        try {

            EvaluacionProyecto ep = new EvaluacionProyecto(bitaProyecto);
            RevisionSolicitud rs = listaRevisionSolicitud.get(0);
            ExencionRequisitosEvaluadorPk pk = new ExencionRequisitosEvaluadorPk(idExencion, SECC_REV_SOL, SECC_REV_SOL_REQ_REV_SOL);
            ExencionRequisitosEvaluador ere = new ExencionRequisitosEvaluador();

            if (sere.findById(pk) != null && !sere.findById(pk).toString().isEmpty()) {
                ere = sere.findById(pk);
            } else {
                ere.setExencionRequisitoEvaluadorPK(pk);
            }
            // En caso de existir, lo único que hace es actualizar los datos
            ere.setObservaciones(rs.getObservaciones());
            ere.setValidado(rs.isValidado());
            //
            ServicioExencionRequisitosEvaluador sG = new ServicioExencionRequisitosEvaluador();

            if (rs.isValidado()) {
                ep.setEvaCheckListRealizado("1");
            } else {
                ep.setEvaCheckListRealizado("0");
            }

            daoVisor.agrega(ep);
            sG.save(ere);

            //
            cargarRevisionSolicitud();
            addMessage("growl", FacesMessage.SEVERITY_INFO, "Revisión solicitud guardada exitosamente");
        } catch (MiaExcepcion e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    public void guardarValPag(ActionEvent e) {
        for (ValidacionPagoDerecho vp : listaValidaPagoDerecho) {
            System.out.println("l1 " + vp.isValidado());
        }
        try {
            ValidacionPagoDerecho vp = listaValidaPagoDerecho.get(0);
            ExencionRequisitosEvaluadorPk pk = new ExencionRequisitosEvaluadorPk(idExencion, SECC_AL_PAG_DER, SECC_AL_PAG_DER_REQ_AL_PAG_DER);
            ExencionRequisitosEvaluador ere = sere.findById(pk);
            if (!vp.isExiste() && ere == null) {
                // Genera Si no existe
                ere = new ExencionRequisitosEvaluador();
                ere.setExencionRequisitoEvaluadorPK(pk);
            }
            // En caso de existir, lo único que hace es actualizar los datos
            ere.setObservaciones(vp.getObservaciones());
            ere.setValidado(vp.isValidado());
            //
            ServicioExencionRequisitosEvaluador sG = new ServicioExencionRequisitosEvaluador();
            sG.save(ere);
            //
            cargarValidacionPagoDerechos();
            addMessage("growl", FacesMessage.SEVERITY_INFO, "Validacion Pago Derecho guardada exitosamente");
        } catch (MiaExcepcion e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    public void guardarAcredit(ActionEvent e) {
        for (Acreditacion acdt : listaAcreditacion) {
            System.out.println("l1 " + acdt.getRespuesta());
        }
        try {
            ServicioExencionRequisitosEvaluador sG = new ServicioExencionRequisitosEvaluador();
            for (Acreditacion acdt : listaAcreditacion) {
                ExencionRequisitosEvaluadorPk pk = new ExencionRequisitosEvaluadorPk(idExencion, SECC_AC, acdt.getIdReq());
                ExencionRequisitosEvaluador ere = sere.findById(pk);
                //
                if (!acdt.isExiste() && ere == null) {
                    // Genera Si no existe
                    ere = new ExencionRequisitosEvaluador();
                    ere.setExencionRequisitoEvaluadorPK(pk);
                }
                // En caso de existir, lo único que hace es actualizar los datos
                ere.setObservaciones(acdt.getObservaciones());
                ere.setRespuesta(acdt.getRespuesta());
                //
                sG.save(ere);
            }
            //
            cargarAcreditacion();
            addMessage("growl", FacesMessage.SEVERITY_INFO, "Acreditación guardada exitosamente");
        } catch (MiaExcepcion e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    public void guardarArchAnex(ActionEvent e) {

        try {
            ServicioExencionRequisitosEvaluador sG = new ServicioExencionRequisitosEvaluador();
            for (ArchivoAnexo aa : listaArchivoAnexo) {

                ExencionRequisitosEvaluadorPk pk = new ExencionRequisitosEvaluadorPk(idExencion, SECC_AN, aa.getIdReq());
                ExencionRequisitosEvaluador ere = sere.findById(pk);
                //
                if (!aa.isExiste() && ere == null) {
                    // Genera Si no existe
                    ere = new ExencionRequisitosEvaluador();
                    ere.setExencionRequisitoEvaluadorPK(pk);
                }
                // En caso de existir, lo único que hace es actualizar los datos
                ere.setObservaciones(aa.getObservaciones());
                ere.setRespuesta(aa.getRespuesta());

                //
                sG.save(ere);
            }
            cargarArchivosAnexos();
            addMessage("growl", FacesMessage.SEVERITY_INFO, "Archivos anexo guardado exitosamente");

        } catch (MiaExcepcion e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }

    public void guardarRedaResol(ActionEvent e) {
        ExencionRequisitosEvaluadorPk pk = new ExencionRequisitosEvaluadorPk(idExencion, SECC_RE, SECC_RE_REQ_RE);
        try {
            ExencionRequisitosEvaluador ere = sere.findById(pk);
            if (ere == null) {
                // Genera Si no existe
                ere = new ExencionRequisitosEvaluador();
                ere.setExencionRequisitoEvaluadorPK(pk);
            }
            // En caso de existir, lo único que hace es actualizar los datos
            ere.setObservaciones(redaccionResolutivo);
            //
            ServicioExencionRequisitosEvaluador sG = new ServicioExencionRequisitosEvaluador();
            sG.save(ere);
            cargaRedaccionResolutivo();
            addMessage("growl", FacesMessage.SEVERITY_INFO, "Redacción resolutivo guardado exitosamente");
        } catch (MiaExcepcion e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }

    private void addMessage(String componentId, Severity severity, String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(componentId, new FacesMessage(severity, message, message));
    }

    public void generaConexion(TokenGO token, Document expedient, String docId) {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String goApiURL = servletContext.getInitParameter("goApi_url");
        String goURL = servletContext.getInitParameter("go_url");
        HttpURLConnection conn;
        RequestContext context = RequestContext.getCurrentInstance();
        OutputStream os;

        String valueUtf8NP = bitaProyecto;
        try {
            if (token != null && (expedient == null || expedient.id == 0)) {

                URL url = new URL(goApiURL + "/expedients");
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("x-user-token", token.getToken());
                String input = "{\"expedient\":{\"project_id\":\"" + bitaProyecto + "\",\"name\":\"" + new String(template.getBytes("UTF-8")) + " - " + valueUtf8NP
                        + "\",\"body\":null,\"header\":null,\"footer\":null,\"document_id\":\"" + docId + "\",\"document_web\":1,\"assignee_id\":null,\"discussions_attributes\":[]}}";

                System.out.println("json ---->>> " + input);
                os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();
                System.out.println("os" + os.toString() + " conn msg:>_ " + conn.getResponseMessage() + " to string:>_ " + conn.toString());
                os.close();
                System.out.println("POST Response Code :: " + conn.getResponseCode());
                if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                }
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                String id = "";
                String output;
                while ((output = br.readLine()) != null) {
                    JSONParser parser = new JSONParser();
                    JSONObject json = (JSONObject) parser.parse(output);
                    json = (JSONObject) parser.parse(json.get("expedient").toString());
                    System.out.println(json.get("id"));
                    id = json.get("id").toString();
                }
                conn.disconnect();

                URL urlE = new URL(goURL + "/editor/expedient/" + id + "?token=" + token.getToken() + "&scope=expedients&project_id="+bitaProyecto);
                System.out.println("url :: " + urlE.toString());

                context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP + "')");

            } else {
                if (expedient != null) {
                    URL urlE = new URL(goURL + "/editor/expedient/" + expedient.getId().longValue() + "?token=" + token.getToken() + "&scope=expedients&project_id="+bitaProyecto);
                    System.out.println("url :: " + urlE.toString());
                    context.execute("window.open('" + urlE.toString() + "','" + valueUtf8NP + "')");
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(ExencionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExencionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ExencionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generarOficio(String docId) {

        System.out.println(bitacoraNom);
        System.out.println(docId);

        Document expedient = null;
        ServicioDocument document = new ServicioDocument();
        try {

            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String goApiURL = servletContext.getInitParameter("goApi_url");
            String goURL = servletContext.getInitParameter("go_url");
            System.out.println("Generar Oficio: " + template);
            System.out.println("IdUsuario: " + idusuario);
            System.out.println("GoApi URL: " + goApiURL);
            RequestContext context = RequestContext.getCurrentInstance();

            ServicioTokenGO servTkGo = new ServicioTokenGO();

            TokenGO token = servTkGo.getToken(idusuario);

            if (docId.equals("1176")) {
                template = "Prevención (Solicitud de Exención)";
                expedient = document.getDocByIdByType(docId, bitaProyecto);
                generaConexion(token, expedient, docId);
            } else if (docId.equals("1556")) {
                template = "Resolutivo Autorizado (Solicitud de Exención)";
                expedient = document.getDocByIdByType(docId, bitaProyecto);
                generaConexion(token, expedient, docId);
            } else if (docId.equals("1557")) {
                template = "Resolutivo Negado (Solicitud de Exención)";
                expedient = document.getDocByIdByType(docId, bitaProyecto);
                generaConexion(token, expedient, docId);
            } else if (docId.equals("consultar oficios")) {
                URL url = new URL(goURL + "/expedients/?token=" + token.getToken() + "&project_id=" + bitaProyecto + "&scope=expedients");
                context.execute("window.open('" + url.toString() + "','_blank')");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public String getCveProyecto() {
        return cveProyecto;
    }

    public void setCveProyecto(String cveProyecto) {
        this.cveProyecto = cveProyecto;
    }

    public String getFechaFofiIngre() {
        return fechaFofiIngre;
    }

    public void setFechaFofiIngre(String fechaFofiIngre) {
        this.fechaFofiIngre = fechaFofiIngre;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public String getPromoTram() {
        return promoTram;
    }

    public void setPromoTram(String promoTram) {
        this.promoTram = promoTram;
    }

    public String getNombProyecto() {
        return nombProyecto;
    }

    public void setNombProyecto(String nombProyecto) {
        this.nombProyecto = nombProyecto;
    }

    public String getSituaTram() {
        return situaTram;
    }

    public void setSituaTram(String situaTram) {
        this.situaTram = situaTram;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getSubsector() {
        return subsector;
    }

    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

    public int getDiasProceso() {
        return diasProceso;
    }

    public void setDiasProceso(int diasProceso) {
        this.diasProceso = diasProceso;
    }

    public int getDiasTramite() {
        return diasTramite;
    }

    public void setDiasTramite(int diasTramite) {
        this.diasTramite = diasTramite;
    }

    public String getBitacoraNom() {
        return bitacoraNom;
    }

    public void setBitacoraNom(String bitacoraNom) {
        this.bitacoraNom = bitacoraNom;
    }

    public String getBitaProyecto() {
        return bitaProyecto;
    }

    public void setBitaProyecto(String bitaProyecto) {
        this.bitaProyecto = bitaProyecto;
    }

    public List<RevisionSolicitud> getListaRevisionSolicitud() {
        return listaRevisionSolicitud;
    }

    public void setListaRevisionSolicitud(List<RevisionSolicitud> listaRevisionSolicitud) {
        this.listaRevisionSolicitud = listaRevisionSolicitud;
    }

    public boolean isDigital() {
        return digital;
    }

    public void setDigital(boolean digital) {
        this.digital = digital;
    }

    public String getDataIconRevSol() {
        return dataIconRevSol;
    }

    public void setDataIconRevSol(String dataIconRevSol) {
        this.dataIconRevSol = dataIconRevSol;
    }

    public String getDataIconValPago() {
        return dataIconValPago;
    }

    public void setDataIconValPago(String dataIconValPago) {
        this.dataIconValPago = dataIconValPago;
    }

    // public List<RevisionSolicitud> getSeleccionadosRevisionSolicitud() {
    // return seleccionadosRevisionSolicitud;
    // }
    //
    // public void setSeleccionadosRevisionSolicitud(List<RevisionSolicitud> seleccionadosRevisionSolicitud) {
    // this.seleccionadosRevisionSolicitud = seleccionadosRevisionSolicitud;
    // }
    public List<ValidacionPagoDerecho> getListaValidaPagoDerecho() {
        return listaValidaPagoDerecho;
    }

    public void setListaValidaPagoDerecho(List<ValidacionPagoDerecho> listaValidaPagoDerecho) {
        this.listaValidaPagoDerecho = listaValidaPagoDerecho;
    }

    public boolean isDePrevencion() {
        return dePrevencion;
    }

    public void setDePrevencion(boolean dePrevencion) {
        this.dePrevencion = dePrevencion;
    }

    public List<Acreditacion> getListaAcreditacion() {
        return listaAcreditacion;
    }

    public void setListaAcreditacion(List<Acreditacion> listaAcreditacion) {
        this.listaAcreditacion = listaAcreditacion;
    }

    public List<String> getRespuestasAcreditacion() {
        return respuestasAcreditacion;
    }

    public void setRespuestasAcreditacion(List<String> respuestasAcreditacion) {
        this.respuestasAcreditacion = respuestasAcreditacion;
    }

    public List<ArchivoAnexo> getListaArchivoAnexo() {
        return listaArchivoAnexo;
    }

    public void setListaArchivoAnexo(List<ArchivoAnexo> listaArchivoAnexo) {
        this.listaArchivoAnexo = listaArchivoAnexo;
    }

    public String getDocument_id() {
        return document_id;
    }

    public String getTemplate() {
        return template;
    }

    public String getRedaccionResolutivo() {
        return redaccionResolutivo;
    }

    public void setRedaccionResolutivo(String redaccionResolutivo) {
        this.redaccionResolutivo = redaccionResolutivo;
    }

    public ServicioBitacora_Proyecto getSbp() {
        return sbp;
    }

    public void setSbp(ServicioBitacora_Proyecto sbp) {
        this.sbp = sbp;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public ServicioExencionRequisitosEvaluador getSere() {
        return sere;
    }

    public void setSere(ServicioExencionRequisitosEvaluador sere) {
        this.sere = sere;
    }

    public long getIdExencion() {
        return idExencion;
    }

    public void setIdExencion(long idExencion) {
        this.idExencion = idExencion;
    }

    public short getSerial() {
        return serial;
    }

    public void setSerial(short serial) {
        this.serial = serial;
    }

    public ServicioArchivosProyecto getSap() {
        return sap;
    }

    public void setSap(ServicioArchivosProyecto sap) {
        this.sap = sap;
    }

    public String getDataIconAcredt() {
        return dataIconAcredt;
    }

    public void setDataIconAcredt(String dataIconAcredt) {
        this.dataIconAcredt = dataIconAcredt;
    }

    public String getDataIconArchAne() {
        return dataIconArchAne;
    }

    public void setDataIconArchAne(String dataIconArchAne) {
        this.dataIconArchAne = dataIconArchAne;
    }

    public SemaforoBitacora getSemaforoBitacora() {
        return semaforoBitacora;
    }

    public void setSemaforoBitacora(SemaforoBitacora semaforoBitacora) {
        this.semaforoBitacora = semaforoBitacora;
    }

    /**
     * @return the saveEnabled
     */
    public boolean isSaveEnabled() {
        return saveEnabled;
    }

    /**
     * @param saveEnabled the saveEnabled to set
     */
    public void setSaveEnabled(boolean saveEnabled) {
        this.saveEnabled = saveEnabled;
    }

}
