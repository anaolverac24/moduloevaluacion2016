/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.LoginDAO;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Ctdependencia;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbempleado;
import mx.gob.semarnat.model.seguridad.Tbusuario;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;


/**
 * @version 2
 * @author dante
 */
@ManagedBean(name = "administracionView")
@ViewScoped
public class AdministracionUsuariosView implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(AdministracionUsuariosView.class.getName());
    
	// Objeto de la clase Bitacoradao
    private final BitacoraDao dao = new BitacoraDao();
    
    // Objeto de la clase Logindao
    private final LoginDAO loginDao = new LoginDAO();
    
    // Lista que almacena los registros de Usuarios.
    private List<Tbusuario> lstUsr = new ArrayList<Tbusuario>();
    
    // Objeto del tipo Usuario para realizar el filtrado.
    private Tbusuario usuario = new Tbusuario();
    
    // Objeto Tbsusuario que almacena la informacion del usuario logueado
	private Tbusuario tbUsuario = new Tbusuario();
    
    // Lista que almacena los registros de Empleados.
    private List<Tbempleado> lstEmp = new ArrayList<Tbempleado>();
    
    // Objeto del tipo Empleado para realizar el filtrado.
    private Tbempleado empleado = new Tbempleado();
    
    // Titulo de la tabla de Administracion de Roles.
    private String titulo;
    
    // Roles seleccionados en la Administracion
    private String[] selectedRoles;
    
    // Catalogo de Roles
    private List<CatRol> catRol = new ArrayList<>();
    
    // Objeto Usuario_Rol
    private UsuarioRol usuarioRol = new UsuarioRol();
    
    // Fecha de vencimiento de un rol
    private Date fechaVencimiento;
    
    // Fecha de vencimiento de un rol
    private Date fechaVencimientoDA;
    
    // Fecha de vencimiento de un rol
    private Date fechaVencimientoSD;
    
    // Fecha de vencimiento de un rol
    private Date fechaVencimientoEVA;
    
    // Catalogo de dependencias
    private List<SelectItem> catDependencias = new ArrayList<SelectItem>();
    
    // Lista de dependencias
    private List<Ctdependencia> ctdependencias = new ArrayList<Ctdependencia>();
    
    private boolean checkDG;

    private Tbarea tbArea;
    /**
     * Bandera que indica los roles seleccionados para la administracion de los usuarios.
     */
    private int banderaRolSeleccionadoAdmin;
    /**
     * Bandera para la fecha de vencimiento del rol de DG.
     */
    private boolean banderaDG;
    /**
     * Bandera para la fecha de vencimiento del rol de DA.
     */    
    private boolean banderaDA;
    /**
     * Bandera para la fecha de vencimiento del rol de SD.
     */
    private boolean banderaSD;
    /**
     * Bandera para la fecha de vencimiento del rol de EVA.
     */
    private boolean banderaEVA;

    public AdministracionUsuariosView() {
		banderaRolSeleccionadoAdmin = 0;

    	logger.debug(Utils.obtenerLogConstructor(""));
        //Muestra el titulo de la tabla
        titulo = "Módulo de Administrar Roles";
        setCatRol(dao.getListRoles());
        ctdependencias = dao.getListaDependenciasDOC();
        HttpSession session = Util.getSession();
//        nombreUsuario = session.getAttribute("usuario").toString();
//        contrase�a = session.getAttribute("pass").toString();
//        System.out.println(session.getAttribute("usuario").toString());
        tbArea = (Tbarea) session.getAttribute("areaDelegacionUsuario");
        System.out.println(session.getAttribute("areaDelegacionUsuario").toString());
        //Se consulta el Area con su Entidad Federativa del usuario.
//        EntidadFederativaDAO entidadFederativaDAO = new EntidadFederativaDAO();
//        entidadFederativa = entidadFederativaDAO.consultarEntidadFederativaID(tbArea.getIdentidadfederativa());
        
        //Se manda en sesion la delegacion del usuario.
//        session.setAttribute("areaDelegacionUsuario", tbArea);
                
        selectedRoles = new String[6];
        
    }
	
	/**
	 * Metodo que se encarga de limpiar el objeto para realizar las busquedas
	 */
	public void limpiar(){
		empleado = new Tbempleado();
		usuario = new Tbusuario();
	}
	
	/**
	 * Metodo que despliega el listado de usuarios con base
	 *  en la busqueda de la pantalla de Administracion de Roles
	 */
	@SuppressWarnings("unchecked")
	public void buscarUsuarios(){
		System.out.println("\nbuscarUsuarios...");
//		if (nombreUsuario != null && contrase�a != null) {
//			tbUsuario = loginDao.autenticacion(nombreUsuario, contrase�a);
//		}
		HttpSession session = Util.getSession();
//		lstUsr = dao.getListUsuarioPorDelegacion(tbArea.getIdentidadfederativa());
        
        String rol = null;
        List<UsuarioRol> roles = (List<UsuarioRol>) session.getAttribute("roles");
        for (UsuarioRol usuarioRol : roles) {
        	if(usuarioRol.getRolId().getNombreCorto().equals("sad")){
				rol = usuarioRol.getRolId().getNombreCorto();
				break;
			}
			if(usuarioRol.getRolId().getNombreCorto().equals("admin")){
				rol = usuarioRol.getRolId().getNombreCorto();
			}
			
		}
		
		
		String nombre = empleado.getNombre();
		String aPaterno = empleado.getApellidopaterno();
		String aMaterno = empleado.getApellidomaterno();
		String idDependencia = usuario.getIddependencia();
		
		System.out.println("rol :" + rol);
		System.out.println("nombre :" + nombre);
		System.out.println("apaterno :" + aPaterno);
		System.out.println("amaterno :" + aMaterno);
		System.out.println("iddependencia :" + idDependencia);
		
		if(rol != null){
			// Busqueda para Usuarios con Rol de Administrador de Usuarios
			if(rol.equals("admin")){
				//Si todos los campos de busqueda estan vacios se consultan todos los usuarios
				if(nombre == "" && aPaterno == "" && aMaterno == "" && idDependencia == ""){
					lstUsr = dao.getListUsuarioPorDelegacion(tbArea.getIdentidadfederativa());
				}
				//Si algun campo esta vacio se utilizan tokens para la busqueda
				else if(idDependencia != ""){
					
					nombre = "%" + nombre + "%";
					aPaterno = "%" + aPaterno + "%";
					aMaterno = "%" + aMaterno + "%";
					// idDependencia = "%" + idDependencia + "%";
					lstUsr = dao.getListUsuarioBusquedaPorUnidadAdministrativa(nombre, aPaterno, aMaterno, idDependencia);
					
				}else{

					nombre = "%" + nombre + "%";
					aPaterno = "%" + aPaterno + "%";
					aMaterno = "%" + aMaterno + "%";
					//idDependencia = "%" + idDependencia + "%";					
					lstUsr = dao.getListUsuarioBusquedaPorDelegacion(nombre, aPaterno, aMaterno, tbArea.getIdentidadfederativa());
				}
			}
			// Busqueda para Usuarios con Rol de Super Administrador de Usuarios
			if(rol.equals("sad")){
				//Si todos los campos de busqueda estan vacios se consultan todos los usuarios
				if(nombre == "" && aPaterno == "" && aMaterno == "" && idDependencia == ""){
//					lstEmp = dao.getListaEmpleados();
					lstUsr = dao.getListaUsuarios();
				}
				else if(idDependencia != ""){
					
					nombre = "%" + nombre + "%";
					aPaterno = "%" + aPaterno + "%";
					aMaterno = "%" + aMaterno + "%";
					// idDependencia = "%" + idDependencia + "%";
					lstUsr = dao.getListUsuarioBusquedaPorUnidadAdministrativa(nombre, aPaterno, aMaterno, idDependencia);
					
				}else{
					
					nombre = "%" + nombre + "%";
					aPaterno = "%" + aPaterno + "%";
					aMaterno = "%" + aMaterno + "%";
					//idDependencia = "%" + idDependencia + "%";
					lstUsr = dao.getListUsuarioBusqueda(nombre, aPaterno, aMaterno);
				}
			}
		}
	}
	
	/**
	 * Metodo que se encarga de buscar un usuario a partir de su ID
	 * @param idusuario
	 */
	public void consultarEmp(int idusuario){
        selectedRoles = new String[6];
        banderaRolSeleccionadoAdmin = 0;
        
		System.out.println(idusuario);
//		List<Tbusuario> usrr = new ArrayList<>();
//		usrr = dao.getLUsuarioPorIdEmpleado(idusuario);
		usuario = dao.getUsuarioPorId(idusuario);
		banderaDG = false;
		banderaDA = false;
		banderaSD = false;
		banderaEVA = false;
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			//Se recupera la lista de roles del usuario ya existentes.
			List<UsuarioRol> rolesUsuarioConsultado = dao.consultarRolesUsuario(usuario.getIdusuario());
			int posicionRol = 0;
			//Se itera la lista de roles del usuario.
			for (UsuarioRol usuarioRol : rolesUsuarioConsultado) {
				//Si el usuario es SuperAdmin
				if (usuarioRol.getRolId().getIdRol() == 1) {
					selectedRoles[posicionRol] = usuarioRol.getRolId().getNombreCorto();
					banderaRolSeleccionadoAdmin = 1;

				}
				
				//Si el usuario es Administrador
				if (usuarioRol.getRolId().getIdRol() == 2) {
					selectedRoles[posicionRol] = usuarioRol.getRolId().getNombreCorto(); 					
					banderaRolSeleccionadoAdmin = 2;

				}
				
				//Si el usuario es Director General
				if (usuarioRol.getRolId().getIdRol() == 3) {
					selectedRoles[posicionRol] = usuarioRol.getRolId().getNombreCorto(); 
					banderaDG = true;
			    	String fechaVencimiento = usuarioRol.getFechaVencimiento().replace("(DOF ", "").replace(")", "");
					//Se convierte el formato de la fecha de vencimiento a dd-MM-yyyy
					this.fechaVencimiento = simpleDateFormat.parse(fechaVencimiento);
				}
					
				//Si el usuario es Director de Area
				if (usuarioRol.getRolId().getIdRol() == 4) {
					selectedRoles[posicionRol] = usuarioRol.getRolId().getNombreCorto(); 					
					banderaDA = true;
			    	String fechaVencimiento = usuarioRol.getFechaVencimiento().replace("(DOF ", "").replace(")", "");
					//Se convierte el formato de la fecha de vencimiento a dd-MM-yyyy
					this.fechaVencimientoDA = simpleDateFormat.parse(fechaVencimiento);
				}					

				//Si el usuario es Subdirector de Area
				if (usuarioRol.getRolId().getIdRol() == 5) {
					selectedRoles[posicionRol] = usuarioRol.getRolId().getNombreCorto(); 										
					banderaSD = true;
			    	String fechaVencimiento = usuarioRol.getFechaVencimiento().replace("(DOF ", "").replace(")", "");
					//Se convierte el formato de la fecha de vencimiento a dd-MM-yyyy
					this.fechaVencimientoSD = simpleDateFormat.parse(fechaVencimiento);
				}
				
				//Si el usuario es Evaluador
				if (usuarioRol.getRolId().getIdRol() == 6) {
					selectedRoles[posicionRol] = usuarioRol.getRolId().getNombreCorto(); 					
					banderaEVA = true;
			    	String fechaVencimiento = usuarioRol.getFechaVencimiento().replace("(DOF ", "").replace(")", "");
					//Se convierte el formato de la fecha de vencimiento a dd-MM-yyyy
					this.fechaVencimientoEVA = simpleDateFormat.parse(fechaVencimiento);
				}
				
				posicionRol++;
			}
			
			posicionRol = 0;
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo que guarda los roles asignados al usuario
	 */
	@SuppressWarnings("unused")
	public void guardar(){
        //Si ha seleccionado al menos un rol
		if (selectedRoles.length > 0) {
			//Se obtiene la fecha actual.
	        Date fechaSystem = Calendar.getInstance().getTime();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String fechaActualConvert = simpleDateFormat.format(fechaSystem);
			
			//Si se asigna una fecha de actualizacion.

			String convertFecha = "";
			String convertFechaDA = "";
			String convertFechaSD = "";
			String convertFechaEVA = "";
			
			String fechaVencimientoConvert = "";
			String fechaVencimientoConvertDA = "";
			String fechaVencimientoConvertSD = "";
			String fechaVencimientoConvertEVA = "";
			
			if (fechaVencimiento != null) {
				fechaVencimientoConvert = simpleDateFormat.format(fechaVencimiento);
				convertFecha = "(DOF " + fechaVencimientoConvert + ")";					
			
				//Si la fecha de vencimiento elegida es menor a la fecha actual.
				if (fechaVencimiento.compareTo(fechaSystem) < 0) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_ERROR, "", "La fecha de vencimiento del DG es menor a la fecha actual."));
					return;
				}
			}
			
			if (fechaVencimientoDA != null) {
				fechaVencimientoConvertDA = simpleDateFormat.format(fechaVencimientoDA);
				convertFechaDA = "(DOF " + fechaVencimientoConvertDA + ")";					
			
				//Si la fecha de vencimiento elegida es menor a la fecha actual.
				if (fechaVencimientoDA.compareTo(fechaSystem) < 0) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_ERROR, "", "La fecha de vencimiento del DA es menor a la fecha actual."));
					return;
				}
			}
			
			if (fechaVencimientoSD != null) {
				fechaVencimientoConvertSD = simpleDateFormat.format(fechaVencimientoSD);
				convertFechaSD = "(DOF " + fechaVencimientoConvertSD + ")";					

				//Si la fecha de vencimiento elegida es menor a la fecha actual.
				if (fechaVencimientoSD.compareTo(fechaSystem) < 0) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_ERROR, "", "La fecha de vencimiento del SD es menor a la fecha actual."));
					return;
				}
			}
			
			if (fechaVencimientoEVA != null) {
				fechaVencimientoConvertEVA = simpleDateFormat.format(fechaVencimientoEVA);
				convertFechaEVA = "(DOF " + fechaVencimientoConvertEVA + ")";					
			
				//Si la fecha de vencimiento elegida es menor a la fecha actual.
				if (fechaVencimientoEVA.compareTo(fechaSystem) < 0) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_ERROR, "", "La fecha de vencimiento del EVA es menor a la fecha actual."));
					return;
				}			
			}			
			
			try {
				
				guardarRolesUsuario(selectedRoles, convertFecha, convertFechaDA, convertFechaSD, convertFechaEVA);

			} catch (SQLException e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
						"", "Error al eliminar los roles del usuario, intente en otro momento"));
				e.printStackTrace();
			}			
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
					"", "Debes seleccionar al menos un rol al usuario"));			
		}

	}
	
	/**
	 * Permite guardar los roles del usuario.
	 * @param selectedRoles del usuario a guardar.
	 */
	public void guardarRolesUsuario(String[] selectedRoles, String convertFecha, 
			String convertFechaDA, String convertFechaSD, String convertFechaEVA) throws SQLException {

		//Se eliminan los roles del usuario.
		dao.eliminarRolesUsuario(usuario.getIdusuario());
		
		for (String rol : selectedRoles) {	
			for (CatRol catRol2 : catRol) {
				System.out.println(catRol2.getNombreCorto());
				if(catRol2.getNombreCorto().equals(rol)){
					System.out.println("Guardar El rol con el Usuario");
					int usrRolId = dao.getMaxUsuarioRolId();
					usuarioRol = new UsuarioRol();
					usuarioRol.setUsuarioRolId(usrRolId);
					usuarioRol.setUsuarioId(usuario);

					usuarioRol.setRolId(catRol2);	
					
					if (catRol2.getNombreCorto().equals("dg")) {
						this.usuarioRol.setFechaVencimiento(convertFecha);	
					}
					
					if (catRol2.getNombreCorto().equals("da")) {
						this.usuarioRol.setFechaVencimiento(convertFechaDA);
					}
					
					if (catRol2.getNombreCorto().equals("sd")) {
						this.usuarioRol.setFechaVencimiento(convertFechaSD);								
					}
					
					if (catRol2.getNombreCorto().equals("eva")) {
						this.usuarioRol.setFechaVencimiento(convertFechaEVA);
					}
					
					dao.agrega(usuarioRol);

					RequestContext reqcontEnv = RequestContext.getCurrentInstance();
					reqcontEnv.execute("PF('modalAdm').hide();PF('alert').show();");
				}
			}
		}		
	
	}

	/**
	 * Permite deshabilitar o habilitar los checkbox de los roles en la administracion.
	 */
	public void mostrarOcultarRolesSeleccionados() {
		//Si se seleccionan roles.
		if (selectedRoles.length > 0) {
			//Si el rol seleccionado es igual a SuperAdmin
			if (Arrays.asList(selectedRoles).contains("sad")) {
				banderaRolSeleccionadoAdmin = 1;
			} 
			
			if (Arrays.asList(selectedRoles).contains("admin")) {
				banderaRolSeleccionadoAdmin = 2;				
			} 
			
			if (!Arrays.asList(selectedRoles).contains("sad") || !Arrays.asList(selectedRoles).contains("admin")) {
				banderaRolSeleccionadoAdmin = 0;	
			}
				
			if (Arrays.asList(selectedRoles).contains("dg")) {
				banderaDG = true;					
			} else {
				banderaDG = false;
				this.fechaVencimiento = null;
			}
				
			if (Arrays.asList(selectedRoles).contains("da")) {
				banderaDA = true;
			} else {
				banderaDA = false;										
				this.fechaVencimientoDA = null;
			}
				
			if (Arrays.asList(selectedRoles).contains("sd")) {
				banderaSD = true;
			} else {
				banderaSD = false;					
				this.fechaVencimientoSD = null;
			}
				
			if (Arrays.asList(selectedRoles).contains("eva")) {
				banderaEVA = true;
			} else {
				banderaEVA = false;
				this.fechaVencimientoEVA = null;
			}

		} else {
			banderaRolSeleccionadoAdmin = 0;
			banderaDG = false;
			banderaDA = false;
			banderaSD = false;
			banderaEVA = false;

			this.fechaVencimiento = null;
			this.fechaVencimientoDA = null;
			this.fechaVencimientoSD = null;
			this.fechaVencimientoEVA = null;
		}
	}	
	
    public void addMessage() {
        String summary = checkDG ? "Checked" : "Unchecked";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
    }
    
    /**
     * Permite consultar la lista de todas las dependencias de Oficinas Centrales y Delegaciones.
     * @return la lista de dependencias.
     */
	@SuppressWarnings("unchecked")
	public List<SelectItem> getCatDependencias() {
		System.out.println("\ngetCatDependencias...");
        HttpSession session = Util.getSession();
        List<UsuarioRol> roles = (List<UsuarioRol>) session.getAttribute("roles");
        Tbarea tbarea = (Tbarea) session.getAttribute("areaDelegacionUsuario");
        
        boolean isSAD = false;
        boolean isADMIN = false;
        
        for (UsuarioRol usuarioRol : roles) {
        	if(usuarioRol.getRolId().getNombreCorto().equals("sad")){
        		isSAD = true;
        	}
        	if(usuarioRol.getRolId().getNombreCorto().equals("admin")){
        		isADMIN = true;
        	}
        }
        
        if(isSAD){
        	
        	//Si el usuario es SuperAdmin. 
        	List<Ctdependencia> n = dao.getListaDependenciasDOC();
            for (Ctdependencia c : n) {
                SelectItem s = new SelectItem(c.getIddependencia(), c.getNombredependencia());
                catDependencias.add(s);
            }
            
        }else if(isADMIN){ // Si el usuario es un administrador
	        	
	            // oficinas centrales
	            if (tbarea.getIdentidadfederativa().equals("09")) {
	        		    List<Ctdependencia> n = dao.getListaDependenciasOficinasCentrales();
	                    for (Ctdependencia c : n) {
	                        SelectItem s = new SelectItem(c.getIddependencia(), c.getNombredependencia());
	                        catDependencias.add(s);
	                    }
	            }else{	// delegaciones      
	            
		           		List<Ctdependencia> n = dao.getListaDependenciaFolio(tbarea.getCtdependencia().getIddependencia());                    
		                for (Ctdependencia c : n) {
		                	SelectItem s = new SelectItem(c.getIddependencia(), c.getNombredependencia());
		                    catDependencias.add(s);
		                }	
	            }
        }
        
		return catDependencias;
	}

	public List<Tbempleado> getLstEmp() {
		return lstEmp;
	}

	public void setLstEmp(List<Tbempleado> lstEmp) {
		this.lstEmp = lstEmp;
	}

	public Tbusuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Tbusuario usuario) {
		this.usuario = usuario;
	}
	
	public BitacoraDao getDao() {
		return dao;
	}

	public List<Tbusuario> getLstUsr() {
		return lstUsr;
	}

	public void setLstUsr(List<Tbusuario> lstUsr) {
		this.lstUsr = lstUsr;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Tbempleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Tbempleado empleado) {
		this.empleado = empleado;
	}

	public String[] getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(String[] selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	public boolean isCheckDG() {
		return checkDG;
	}

	public void setCheckDG(boolean checkDG) {
		this.checkDG = checkDG;
	}

	public List<CatRol> getCatRol() {
		return catRol;
	}

	public void setCatRol(List<CatRol> catRol) {
		this.catRol = catRol;
	}

	public UsuarioRol getUsuarioRol() {
		return usuarioRol;
	}

	public void setUsuarioRol(UsuarioRol usuarioRol) {
		this.usuarioRol = usuarioRol;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public List<Ctdependencia> getCtdependencias() {
		return ctdependencias;
	}

	public void setCtdependencias(List<Ctdependencia> ctdependencias) {
		this.ctdependencias = ctdependencias;
	}

	public void setCatDependencias(List<SelectItem> catDependencias) {
		this.catDependencias = catDependencias;
	}

	public LoginDAO getLoginDao() {
		return loginDao;
	}

	public Tbusuario getTbUsuario() {
		return tbUsuario;
	}

	public void setTbUsuario(Tbusuario tbUsuario) {
		this.tbUsuario = tbUsuario;
	}

	public Tbarea getTbArea() {
		return tbArea;
	}

	public void setTbArea(Tbarea tbArea) {
		this.tbArea = tbArea;
	}

	/**
	 * @return the banderaRolSeleccionadoAdmin
	 */
	public int getBanderaRolSeleccionadoAdmin() {
		return banderaRolSeleccionadoAdmin;
	}

	/**
	 * @param banderaRolSeleccionadoAdmin the banderaRolSeleccionadoAdmin to set
	 */
	public void setBanderaRolSeleccionadoAdmin(int banderaRolSeleccionadoAdmin) {
		this.banderaRolSeleccionadoAdmin = banderaRolSeleccionadoAdmin;
	}

	/**
	 * @return the banderaDG
	 */
	public boolean isBanderaDG() {
		return banderaDG;
	}

	/**
	 * @param banderaDG the banderaDG to set
	 */
	public void setBanderaDG(boolean banderaDG) {
		this.banderaDG = banderaDG;
	}

	/**
	 * @return the banderaDA
	 */
	public boolean isBanderaDA() {
		return banderaDA;
	}

	/**
	 * @param banderaDA the banderaDA to set
	 */
	public void setBanderaDA(boolean banderaDA) {
		this.banderaDA = banderaDA;
	}

	/**
	 * @return the banderaSD
	 */
	public boolean isBanderaSD() {
		return banderaSD;
	}

	/**
	 * @param banderaSD the banderaSD to set
	 */
	public void setBanderaSD(boolean banderaSD) {
		this.banderaSD = banderaSD;
	}

	/**
	 * @return the banderaEVA
	 */
	public boolean isBanderaEVA() {
		return banderaEVA;
	}

	/**
	 * @param banderaEVA the banderaEVA to set
	 */
	public void setBanderaEVA(boolean banderaEVA) {
		this.banderaEVA = banderaEVA;
	}

	/**
	 * @return the fechaVencimientoDA
	 */
	public Date getFechaVencimientoDA() {
		return fechaVencimientoDA;
	}

	/**
	 * @param fechaVencimientoDA the fechaVencimientoDA to set
	 */
	public void setFechaVencimientoDA(Date fechaVencimientoDA) {
		this.fechaVencimientoDA = fechaVencimientoDA;
	}

	/**
	 * @return the fechaVencimientoSD
	 */
	public Date getFechaVencimientoSD() {
		return fechaVencimientoSD;
	}

	/**
	 * @param fechaVencimientoSD the fechaVencimientoSD to set
	 */
	public void setFechaVencimientoSD(Date fechaVencimientoSD) {
		this.fechaVencimientoSD = fechaVencimientoSD;
	}

	/**
	 * @return the fechaVencimientoEVA
	 */
	public Date getFechaVencimientoEVA() {
		return fechaVencimientoEVA;
	}

	/**
	 * @param fechaVencimientoEVA the fechaVencimientoEVA to set
	 */
	public void setFechaVencimientoEVA(Date fechaVencimientoEVA) {
		this.fechaVencimientoEVA = fechaVencimientoEVA;
	}

}
