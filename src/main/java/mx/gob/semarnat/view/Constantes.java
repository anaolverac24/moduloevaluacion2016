package mx.gob.semarnat.view;



/**
 * Clase con las constantes para SIGEIA
 * @author Rodrigo
 */
public class Constantes {
	
	// configuracion_pool
    // SIGEIA DESARROLLO
 	public static final String URL_SIGEIA = "http://devsigeia.semarnat.gob.mx/SIGEIA5e5_desarrollo/bos/bos.php?parametros=";
 	// SIGEIA PRODUCCION
 	//Se modifico está línea, ya que se identifico junto con Ana que estaba apuntando a producción al tiempo que se compartio el proyecto
//    public static final String URL_SIGEIA = "https://devsigeia.semarnat.gob.mx/SIGEIA_MIA/BOS/BOS.php?parametros=";
    public static final String USUARIO_SIGEIA = "Usuario";
    public static final String PASSWORD_SIGEIA = "81563e7c8b6497434523fa5d7eba4f30";
    public static final String VERSION = "2";
    public static final String TRAMITE = "MIA";   
}
