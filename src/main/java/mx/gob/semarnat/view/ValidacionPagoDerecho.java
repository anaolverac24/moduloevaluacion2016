package mx.gob.semarnat.view;

public class ValidacionPagoDerecho {

	private final String DEFAULT_IMG = "/resources/images/crossmark.png";
	private final String CHECK_IMG = "/resources/images/checkmark.png";
	private String img;	
	private boolean validado;	
	private String observaciones;
	private final String descripcionRequisito;
	private int pos;
	private short idSec;
	private short idReq;
	private boolean existe;
	
	public ValidacionPagoDerecho(String descripcionRequisito, boolean validado) {
		this.img = DEFAULT_IMG;
		this.descripcionRequisito = descripcionRequisito;		
		this.validado = validado;
		if (this.validado) {
			this.img = CHECK_IMG;
		} else {
			this.img = DEFAULT_IMG;
		}
		this.existe = false;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getDescripcionRequisito() {
		return descripcionRequisito;
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public short getIdSec() {
		return idSec;
	}

	public void setIdSec(short idSec) {
		this.idSec = idSec;
	}

	public short getIdReq() {
		return idReq;
	}

	public void setIdReq(short idReq) {
		this.idReq = idReq;
	}

	public boolean isExiste() {
		return existe;
	}

	public void setExiste(boolean existe) {
		this.existe = existe;
	}

}
