package mx.gob.semarnat.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.primefaces.context.RequestContext;

import mx.go.semarnat.services.ServicioDocument;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.mia.servicios.ServicioTokenGO;
import mx.gob.semarnat.mia.servicios.dgiramiae.ServicioResutando;
import mx.gob.semarnat.model.dgira_miae.Document;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.Resultando;
import mx.gob.semarnat.model.seguridad.TokenGO;

@SuppressWarnings("serial")
@ManagedBean(name = "resultandoBean")
@ViewScoped
public class ResultandosBean extends BaseBean implements Serializable {
	private final String MOD_AGREGAR = "Agregar registro";
	private final String MOD_EDITAR = "Editar registro";
	private String bitacoraProyecto;
	private String folioProyecto;
	private boolean cargado;
	private Integer idusuario;
	private String folioDoc;
	private String referencia;
	private String categoriaDoc;
	private Date fechaEmision;
	private Date fechaRegistro;
	private String nombreRemi;
	private String dependenciaRemi;
	private String observaciones;
	private String redaccion;
	private String tituloModal;
	private boolean resultandosRepetidos;
	private List<Resultando> listaResultandos;
	private Resultando resultandoActualizable;
	private ServicioResutando servResultando;
	private String seleccionResultandosRepetidos;
	private String SUSTITUIR_REPETIDOS = "0";
	private String AGREGAR_REPETIDOS = "1";
	private String document_id;
	private String template;

	public ResultandosBean() {
		FacesContext fContext = FacesContext.getCurrentInstance();
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		folioProyecto = (String) fContext.getExternalContext().getSessionMap().get("userFolioProy");
		idusuario = (Integer) fContext.getExternalContext().getSessionMap().get("idUsu");
		document_id = req.getParameter("documentId");
		String template = req.getParameter("template");
		bitacoraProyecto = req.getParameter("numBitacora");
		System.out.println("folioProyecto: " + folioProyecto);
		System.out.println("idusuario: " + idusuario);
		System.out.println("document_id: " + document_id);
		System.out.println("template: " + template);
		System.out.println("bitacoraProyecto: " + bitacoraProyecto);
		if (idusuario != null && bitacoraProyecto != null && template != null && document_id != null) {

			servResultando = new ServicioResutando();
			try {
				this.template = new String(template.getBytes("UTF-8"));
				resultandosRepetidos = servResultando.generarResultandosPorBitacora(folioProyecto, bitacoraProyecto);
				inicializarBean();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MiaExcepcion e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("Fin resultandosBean");
	}

	private void inicializarBean() throws MiaExcepcion {
		tituloModal = MOD_AGREGAR;
		cargado = false;
		limpiarFormulario();
		listaResultandos = servResultando.encontrarPorBitacora(bitacoraProyecto);
	}

	public void genOf() {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String goApiURL = servletContext.getInitParameter("goApi_url");
		String goURL = servletContext.getInitParameter("go_url");
		System.out.println("Generar Oficio: " + template);
		System.out.println("IdUsuario: " + idusuario);
		System.out.println("GoApi URL: " + goApiURL);
		RequestContext context = RequestContext.getCurrentInstance();
		HttpURLConnection conn;             
                
		OutputStream os;
		VisorDao daoVisor = new VisorDao();
		ServicioTokenGO servTkGo = new ServicioTokenGO();
		ServicioDocument servDoc = new ServicioDocument();
		Proyecto proyecto = daoVisor.obtieneProyectoUltimaVersion(bitacoraProyecto);
		String nameProyect = proyecto != null ? proyecto.getProyNombre() : "";

                try {
			String valueUtf8NP = !nameProyect.isEmpty() ? new String(nameProyect.getBytes("UTF-8")) : "";
                        
			Document doc = servDoc.getDocByIdByType(document_id, bitacoraProyecto);
			TokenGO token = servTkGo.getToken(idusuario);
			
                        valueUtf8NP = valueUtf8NP.replaceAll("\"", "");
                        valueUtf8NP = valueUtf8NP.replaceAll("”", ""); 
                        valueUtf8NP = valueUtf8NP.replaceAll("“", "");
                        
                        System.out.println("nombre proy" + valueUtf8NP);
                        
			if (doc == null || doc.id == 0) {
				URL url = new URL(goApiURL + "/expedients");
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setRequestProperty("x-user-token", token.getToken());
				String input = "{\"expedient\":{\"project_id\":\"" + bitacoraProyecto 
						+ "\",\"name\":\"" + this.template + " - " + valueUtf8NP
						+ "\",\"body\":null,\"header\":null,\"footer\":null,\"document_id\":\"" 
						+ document_id + "\",\"document_web\":1,\"assignee_id\":null,\"discussions_attributes\":[]}}";

				os = conn.getOutputStream();
				os.write(input.getBytes());
				os.flush();
				System.out.println("os" + os.toString() + " conn msg:>_ " + conn.getResponseMessage() + " to string:>_ " + conn.toString());
				os.close();
				System.out.println("POST Response Code :: " + conn.getResponseCode());
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				String id = "";
				String output;
				while ((output = br.readLine()) != null) {
					JSONParser parser = new JSONParser();
					JSONObject json = (JSONObject) parser.parse(output);
					json = (JSONObject) parser.parse(json.get("expedient").toString());
					System.out.println(json.get("id"));
					id = json.get("id").toString();
				}
				conn.disconnect();

				URL urlE = new URL(goURL + "/editor/expedient/" + id + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
				System.out.println("url :: " + urlE.toString());
				context.execute("window.open('" + urlE.toString() + "','_newtab')");
			} else {
                                URL urlE = new URL(goURL + "/editor/expedient/" + doc.getId().toString() + "?token=" + token.getToken()+"&scope=expedients&project_id="+bitacoraProyecto);
                                System.out.println("url :: " + urlE.toString());
                                context.execute("window.open('" + urlE.toString() + "','_newtab')");
			}

		} catch (MiaExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void limpiarFormulario() {
		folioDoc = null;
		referencia = null;
		categoriaDoc = null;
		fechaEmision = null;
		fechaRegistro = null;
		nombreRemi = null;
		dependenciaRemi = null;
		observaciones = null;
		redaccion = null;
		resultandoActualizable = null;
	}

	public void aplicarSeleccionResultandosRepetidos(ActionEvent e) {

		System.out.println("Click en aplicarSeleccionResultandosRepetidos: " + seleccionResultandosRepetidos);
		try {
			if (seleccionResultandosRepetidos.equals(SUSTITUIR_REPETIDOS)) {
				boolean exito = servResultando.sustituirResultandosPorBitacora(folioProyecto, bitacoraProyecto);
				RequestContext.getCurrentInstance().execute("PF('dlogExistenRepetidos').hide();");
				if (exito) {
					inicializarBean();
					addMessage("growl", FacesMessage.SEVERITY_INFO, "Ajuste realizado exitosamente");
				} else {
					addMessage("growl", FacesMessage.SEVERITY_ERROR, "Error al realizar el ajuste");
				}
			}
			if (seleccionResultandosRepetidos.equals(AGREGAR_REPETIDOS)) {
				RequestContext.getCurrentInstance().execute("PF('dlogExistenRepetidos').hide();");
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			RequestContext.getCurrentInstance().execute("PF('dlogExistenRepetidos').hide();");
			addMessage("growl", FacesMessage.SEVERITY_ERROR, "Error al realizar el ajuste");
		} catch (MiaExcepcion e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			RequestContext.getCurrentInstance().execute("PF('dlogExistenRepetidos').hide();");
			addMessage("growl", FacesMessage.SEVERITY_ERROR, "Error al realizar el ajuste");
		}

	}

	public void guardar(ActionEvent e) {
		System.out.println("Click en guardar");
		ServicioResutando sR = new ServicioResutando();
		try {
			if (cargado) {
				// Edición
				modificarValores();
				resultandoActualizable = sR.save(resultandoActualizable);
				// Exito
				inicializarBean();
				RequestContext.getCurrentInstance().execute("PF('dlogResultando').hide();");
				addMessage("growl", FacesMessage.SEVERITY_INFO, "Registro editado exitosamente");
			} else {
				// Alta
				Resultando nuevoR = crearResultando();
				nuevoR = sR.save(nuevoR);
				// Exito
				inicializarBean();
				RequestContext.getCurrentInstance().execute("PF('dlogResultando').hide();");
				addMessage("growl", FacesMessage.SEVERITY_INFO, "Registro guardado exitosamente");
			}
		} catch (MiaExcepcion e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			// Fallo
			addMessage("growl", FacesMessage.SEVERITY_ERROR, "No se pudo guardar el registro");
		}
	}

	private void addMessage(String componentId, Severity severity, String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(componentId, new FacesMessage(severity, message, message));
	}

	private Resultando crearResultando() {
		Resultando r = new Resultando();
		//
		r.setAltaManual(true);
		r.setBitacoraProy(bitacoraProyecto);
		r.setCategoriaDoc(categoriaDoc);
		r.setDependenciaRemi(dependenciaRemi);
		r.setFechaElaboracionDoc(fechaEmision);
		r.setFechaRegistro(fechaRegistro);
		r.setFolioDoc(folioDoc);
		r.setFolioProy(folioProyecto);
		r.setNombreRemi(nombreRemi);
		r.setObservaciones(observaciones);
		r.setRedaccion(redaccion);
		r.setReferencia(referencia);
		r.setRelIngresoProy(null);
		r.setRelPublicGaceta(null);
		return r;
	}

	private void modificarValores() {
		resultandoActualizable.setCategoriaDoc(categoriaDoc);
		resultandoActualizable.setDependenciaRemi(dependenciaRemi);
		resultandoActualizable.setFechaElaboracionDoc(fechaEmision);
		resultandoActualizable.setFechaRegistro(fechaRegistro);
		resultandoActualizable.setFolioDoc(folioDoc);
		resultandoActualizable.setNombreRemi(nombreRemi);
		resultandoActualizable.setObservaciones(observaciones);
		resultandoActualizable.setRedaccion(redaccion);
		resultandoActualizable.setReferencia(referencia);
	}

	public void eliminar(ActionEvent e) {
		// Obtener objeto de lista de resultandos
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String idActividad = params.get("idSeleccion").toString();
		int position = Integer.parseInt(idActividad);
		Resultando r = listaResultandos.get(position);
		ServicioResutando sR = new ServicioResutando();
		try {
			sR.delete(r.getResultandoId());
			// Exito
			inicializarBean();
			addMessage("growl", FacesMessage.SEVERITY_INFO, "Registro eliminado exitosamente");
		} catch (MiaExcepcion e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void abrirModalResultandoNuevoRegistro(ActionEvent e) {
		System.out.println("abrirModalResultandoNuevoRegistro");
		tituloModal = MOD_AGREGAR;
		limpiarFormulario();
		cargado = false;
		RequestContext.getCurrentInstance().reset("resultForm:grid");
		RequestContext.getCurrentInstance().execute("PF('dlogResultando').show();");
		System.out.println("fin");
	}

	public void abrirModalResultandoEditarRegistro(ActionEvent e) {
		System.out.println("abrirModalResultandoEditarRegistro");
		tituloModal = MOD_EDITAR;
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String idActividad = params.get("idSeleccion").toString();
		int position = Integer.parseInt(idActividad);
		System.out.println("Position: " + position);
		Resultando r = listaResultandos.get(position);
		limpiarFormulario();
		cargarValores(r);
		cargado = true;
		RequestContext.getCurrentInstance().reset("resultForm:grid");
		RequestContext.getCurrentInstance().execute("PF('dlogResultando').show();");
	}

	private void cargarValores(Resultando r) {
		folioDoc = r.getFolioDoc();
		referencia = r.getReferencia();
		categoriaDoc = r.getCategoriaDoc();
		fechaEmision = r.getFechaElaboracionDoc();
		fechaRegistro = r.getFechaRegistro();
		nombreRemi = r.getNombreRemi();
		dependenciaRemi = r.getDependenciaRemi();
		observaciones = r.getObservaciones();
		redaccion = r.getRedaccion();
		resultandoActualizable = null;
		resultandoActualizable = r;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public boolean isCargado() {
		return cargado;
	}

	public void setCargado(boolean cargado) {
		this.cargado = cargado;
	}

	public String getCategoriaDoc() {
		return categoriaDoc;
	}

	public void setCategoriaDoc(String categoriaDoc) {
		this.categoriaDoc = categoriaDoc;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getRedaccion() {
		return redaccion;
	}

	public void setRedaccion(String redaccion) {
		this.redaccion = redaccion;
	}

	public String getFolioDoc() {
		return folioDoc;
	}

	public void setFolioDoc(String folioDoc) {
		this.folioDoc = folioDoc;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNombreRemi() {
		return nombreRemi;
	}

	public void setNombreRemi(String nombreRemi) {
		this.nombreRemi = nombreRemi;
	}

	public String getDependenciaRemi() {
		return dependenciaRemi;
	}

	public void setDependenciaRemi(String dependenciaRemi) {
		this.dependenciaRemi = dependenciaRemi;
	}

	public String getTituloModal() {
		return tituloModal;
	}

	public void setTituloModal(String tituloModal) {
		this.tituloModal = tituloModal;
	}

	public Resultando getResultandoActualizable() {
		return resultandoActualizable;
	}

	public void setResultandoActualizable(Resultando resultandoActualizable) {
		this.resultandoActualizable = resultandoActualizable;
	}

	public List<Resultando> getListaResultandos() {
		return listaResultandos;
	}

	public void setListaResultandos(List<Resultando> listaResultandos) {
		this.listaResultandos = listaResultandos;
	}

	public ServicioResutando getServResultando() {
		return servResultando;
	}

	public void setServResultando(ServicioResutando servResultando) {
		this.servResultando = servResultando;
	}

	public boolean isResultandosRepetidos() {
		return resultandosRepetidos;
	}

	public void setResultandosRepetidos(boolean resultandosRepetidos) {
		this.resultandosRepetidos = resultandosRepetidos;
	}

	public String getSeleccionResultandosRepetidos() {
		return seleccionResultandosRepetidos;
	}

	public void setSeleccionResultandosRepetidos(String seleccionResultandosRepetidos) {
		this.seleccionResultandosRepetidos = seleccionResultandosRepetidos;
	}

	public String getSUSTITUIR_REPETIDOS() {
		return SUSTITUIR_REPETIDOS;
	}

	public void setSUSTITUIR_REPETIDOS(String sUSTITUIR_REPETIDOS) {
		SUSTITUIR_REPETIDOS = sUSTITUIR_REPETIDOS;
	}

	public String getAGREGAR_REPETIDOS() {
		return AGREGAR_REPETIDOS;
	}

	public void setAGREGAR_REPETIDOS(String aGREGAR_REPETIDOS) {
		AGREGAR_REPETIDOS = aGREGAR_REPETIDOS;
	}

	public String getDocument_id() {
		return document_id;
	}

	public void setDocument_id(String document_id) {
		this.document_id = document_id;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Integer getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}

}
