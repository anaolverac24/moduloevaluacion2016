/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import mx.gob.semarnat.dao.APDao;
import mx.gob.semarnat.model.dgira_miae.CatDependenciaOficio;

/**
 *
 * @author Rodrigo
 */
@ManagedBean(name = "catDependenciaIdConverter")
@FacesConverter(forClass = CatDependenciaOficio.class)
public class CatDependenciaIdConverter implements Converter {

    APDao dao = new APDao();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        CatDependenciaOficio dependenciaOficio = new CatDependenciaOficio();
        if (value != null) {
            try {
                dependenciaOficio = (CatDependenciaOficio) dao.busca(CatDependenciaOficio.class, new Short(value));
            } catch (Exception e) {
            }
        }
        return dependenciaOficio;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof CatDependenciaOficio ? ((CatDependenciaOficio) value).getCatDependenciaId().toString() : null;
    }

}
