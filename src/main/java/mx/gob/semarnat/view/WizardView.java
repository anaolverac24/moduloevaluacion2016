/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import mx.gob.semarnat.dao.APDao;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.DirectorDao;
import mx.gob.semarnat.dao.VisorDao;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.CatDerechosCriteriosvalores;
import mx.gob.semarnat.model.dgira_miae.CatDerechosCriteriosvaloresPK;

import mx.gob.semarnat.model.dgira_miae.CriterioValoresProyecto;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.RequisitosSubdirector;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.secure.Util;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Rengerden
 */
@SuppressWarnings("serial")
@ManagedBean(name = "wizard")
@ViewScoped
public class WizardView implements Serializable {
    private static final Logger logger = Logger.getLogger(WizardView.class.getName());
    
    public static final int DERECHOS_LIM_INF_NIVEL_A    = 0;
    public static final int DERECHOS_LIM_SUP_NIVEL_A    = 3;
    public static final int DERECHOS_LIM_INF_NIVEL_B    = 5;
    public static final int DERECHOS_LIM_SUP_NIVEL_B    = 7;
    public static final int DERECHOS_LIM_INF_NIVEL_C    = 0;
    public static final int DERECHOS_LIM_SUP_NIVEL_C    = 9;
    public static final Character DERECHOS_NIVEL_A      = 'A';
    public static final Character DERECHOS_NIVEL_B      = 'B';
    public static final Character DERECHOS_NIVEL_C      = 'C';
    @ManagedProperty ( value = "#{detalleView}")
    private DetalleProyectoView detalleView;
    private final APDao APDao = new APDao();
    private final BitacoraDao dao = new BitacoraDao();
    private final DgiraMiaeDaoGeneral daoGral = new DgiraMiaeDaoGeneral();
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> todosProySubD = new ArrayList();
    private List<WizardHelper> lista;
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<String> temp = new ArrayList();
    private Integer vp;
    private String doc;
    private Integer sum;
    private Integer sum2;
    private String msg;
    private Integer valor = 0;
    private String cadena = "MG";
    private List<Object[]> niveles = new ArrayList<>();
    private String bitaProyecto;
    private String folioProyecto;
    private short serialProyecto;
    private String tramite;
    //String pruebaBita= "09/MG-0007/02/15";
    private BigDecimal bitamonto;
    private Double montoCalc = 0.0;
    private Double montoCalcProm = 0.0;
    private Double valorPromCalc;
    
    private final DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
    public EvaluacionProyecto eval;
    private String observacion;
    private boolean supera=true;
    private boolean llenado = false;
    private Proyecto proyecto = new Proyecto();
    
    private Integer mantoC=0;
    private Boolean oficPrev = false;
    private Integer pagoDere; 
    private Integer checkList;
    private Integer aux;
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> requisitos = new ArrayList();
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> requisitosSi = new ArrayList();
    private final DirectorDao daoDirec = new DirectorDao();
    
    private String sintesisGaseta;
    
    private String cveTipTram; 
    private Integer idTipTram;
    private String edoGestTram;
    
    private int sumaPromovente;
    private Boolean dePrevenc = false; //bandera que indica si un tramite ya vino de una reactivacion de tramite por prevencion o de una suspencion por info adicional
    private List<Historial> HistorialAT0001;     
    private List<Historial> HistorialCIS303;
    private List<Historial> NoHistorialAT0022; 
    private List<Historial> NoHistorialCIS106;
    private List<Historial> NoHistorial200501;
    private List<Historial> NoHistorialIRA020;
    private List<Historial> NoHistorialI00008;
    private String URLdb;
    private String URLgenOficPre;
    @SuppressWarnings("unused")
	private DocumentoDgiraFlujoHist docsDgiraFlujoHist;
    private DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
    private short maxIdHisto = 0;
    private String perfilSig;
    private Integer statusDoc;
    private List<Object[]> areaResiveDocCon = new ArrayList<Object[]>();
    private String jerarquia;
    @SuppressWarnings("unused")
	private String nombreOfic;
    private HttpSession sesion = Util.getSession();
    
    //Vista por defecto
    private int vistaVersion;
    
    //Existencia de versiones
    private ArrayList<Boolean> existeVersion = new ArrayList<Boolean>();
    private final int MAX_VERSIONES_EXISTENTES = 4;
    
   
    private HashMap<String, boolean[]> mapInfoAdicional;
    private boolean fisico;
    private short tipoDoc = 3;
    private List<Object[]> DocRespDgira = new ArrayList<Object[]>();
    
    private boolean bReaperturaPagoDerechos;
    private Tbarea tbarea = new Tbarea();
    private String idEntidad = "";

    private Boolean serialTres;
    private String folio;
    private final VisorDao daoVisor = new VisorDao();
   
	public Boolean getSerialTres() {
		return serialTres;
	}

	public void setSerialTres(Boolean serialTres) {
		this.serialTres = serialTres;
	}
    

    @SuppressWarnings({ "unused", "unchecked", "rawtypes" })
	public WizardView() {
        logger.debug(Utils.obtenerLogConstructor("Ini"));
        
        Integer aux2;
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext(); //parametros pasados por url
		Map params = ec.getRequestParameterMap();
        bitaProyecto = context.getExternalContext().getSessionMap().get("bitacoraProy").toString();
        cveTipTram = (String) context.getExternalContext().getSessionMap().get("cveTramite");
        idTipTram = (Integer) context.getExternalContext().getSessionMap().get("idTramite");
        edoGestTram = (String) context.getExternalContext().getSessionMap().get("edoGestTramite");
        
        tbarea = (Tbarea) sesion.getAttribute("areaDelegacionUsuario");
        idEntidad = tbarea.getIdentidadfederativa();
        fisico = !(bitaProyecto.charAt(5) == 'W');

        
        
        
        Proyecto proyecto = daoVisor.obtieneProyectoVersionAnterior(bitaProyecto);
        
        folio = proyecto.getProyectoPK().getFolioProyecto();
        serialTres = dao.proyecto(folio, (short)3) != null? true: false;
        
        
        
        
        if(fisico || context.getExternalContext().getSessionMap().get("userFolioProy") != null)
        {  
            if ( fisico ) {
                folioProyecto = bitaProyecto;
                serialProyecto = (short)1;
                
                valorPromCalc = (double)0;
                bitamonto =  dao.monto(bitaProyecto);
                
                //Criterios
                //Busca si ya existen registros, si no, los inserta
				List<CriterioValoresProyecto> lstTmp = (List<CriterioValoresProyecto>) 
                        daoDg.lista_namedQuery("CriterioValoresProyecto.findByBitacoraProyecto2", new Object[]{folioProyecto,serialProyecto}, new String[]{"folioProyecto","serialProyecto"});
                
                if (lstTmp == null || lstTmp.isEmpty()) {
                    List<Object[]> lstCvp = dao.obtenerCriterios();
                    CriterioValoresProyecto newCvp;
                    for (Object[] cvp : lstCvp) {
                        newCvp = new CriterioValoresProyecto(folioProyecto, serialProyecto, 
                                        new BigInteger(cvp[0].toString()), new BigInteger(cvp[1].toString()));
                        newCvp.setBitacoraProyecto(bitaProyecto);
                        dao.agrega(newCvp);
                    }
                }
                
                
            } else {
                folioProyecto = (String) context.getExternalContext().getSessionMap().get("userFolioProy");
                serialProyecto = (Short) context.getExternalContext().getSessionMap().get("userSerialProy");
                bitamonto =  dao.montoVEX(bitaProyecto);
            }
            
            //Cargar Proyecto  
            try {
                this.proyecto = dao.proyecto(folioProyecto, serialProyecto);
            } catch (Exception e) {
                System.out.println("No encontrado");
            }

            todosProySubD = dao.enWizar(); //TODOS LOS TRAMITES CON ID DE SITUACION I00010
            tramite = dao.idTramite(bitaProyecto);
    //        System.out.println("prueba " +tramite);
            lista = new ArrayList();

            niveles = dao.tablab(tramite);
            String desc = "";
            String id = todosProySubD.get(0)[0].toString();
            sum = 0;
            int i = 1, j = 0;
            int indice = 1;
            String aux;
            String aux3;
            //CriterioValoresProyecto c;
            List<Object[]> c;
            
            //List<CriterioValoresProyecto> criterios = (List<CriterioValoresProyecto>) daoDg.lista_namedQuery("CriterioValoresProyecto.findByBitacoraProyecto2", new Object[]{folioProyecto,serialProyecto}, new String[]{"folioProyecto","serialProyecto"});
            List<Object[]> critValoresProy = daoDg.criteriosValoresProyecto(folioProyecto, serialProyecto);
            Object[] criterio;
            
            for (Object[] o : todosProySubD) {
                if (!id.equals(o[0].toString()) || todosProySubD.size() == i) {
                    if (todosProySubD.size() == i) {
                    	desc = o[1].toString();
                        temp.add(o[2].toString());
                    }
                    if (critValoresProy.isEmpty()) {
                        lista.add(new WizardHelper(indice, desc, temp));                   
                    } else {
                    	criterio = buscaCriterio(critValoresProy, id);
                        if ( criterio != null) {
                            lista.add(new WizardHelper(Integer.parseInt(id), desc, temp, 
                                    criterio[9] != null ? criterio[9].toString() : "", //Descripción usuario
                                    criterio[6] != null ? Integer.parseInt(criterio[6].toString()) : 0, //Valor evaluador
                                    criterio[7] != null ? criterio[7].toString() : "", //Observacion
                                    criterio[5] != null ? Integer.parseInt(criterio[5].toString()) : 0, //Valor usuario/promovente
                                    criterio[8] != null ? criterio[8].toString() : "" //Descripción promovente
                            ));
                        } else {
                            lista.add(new WizardHelper(indice, desc, temp));    
                        }
                    }
                    temp = new ArrayList();
                    id = o[0].toString();
                    temp.add(o[2].toString());
                    indice++;
                } else {
                    temp.add(o[2].toString());
                    desc = o[1].toString();
                }
                i++;
            }
        }
            
        eval = (EvaluacionProyecto) daoDg.busca(EvaluacionProyecto.class, bitaProyecto);
        if (eval != null) {
            try{
            montoCalc = eval.getEvaMontCritEvaluador().doubleValue();
            montoCalcProm = eval.getEvaMontCritPromovente().doubleValue();
            if(montoCalc > montoCalcProm)
            {supera = true;}
            else{supera = false;}
            System.out.println("constructor-montoCalc: " + montoCalc);
            System.out.println("constructor-montoCalcProm: " + montoCalcProm);
            System.out.println("constructor-supera: " + supera);
            }catch(Exception e){
            System.out.println("constructor-supera: " + e.getMessage());}
        }else{
            //eval = new EvaluacionProyecto();
            eval = new EvaluacionProyecto(bitaProyecto);
        }
        
        if(fisico || context.getExternalContext().getSessionMap().get("userFolioProy") != null)
        {   
            //Calculo de la suma de puntaje del promovente y evaluador
            sumaPromovente = 0;
            sum = 0;
            for(WizardHelper ele : lista) {
                sumaPromovente += 
                        (ele.getValorProm() != null) ? ele.getValorProm() : 0;
                sum += 
                        (ele.getValorEval() != null ? ele.getValorEval() : 0);
            }
            valorPromCalc = calculaMontoEvaluador(tramite, sumaPromovente);
            montoCalc = calculaMontoEvaluador(tramite, sum);
            
            //Obtencion de la existencia de versiones
            ArrayList<Short> versiones;
            int iVersion;
            //Inicializacion arreglo de versiones
            for (int idx = 0; idx < MAX_VERSIONES_EXISTENTES+1; idx ++) {
                this.existeVersion.add(false);
            }
            
            //Promovente
            versiones = daoGral.obtieneVersionesProyecto(folioProyecto);
            for (Short version : versiones) {
                this.existeVersion.set((int)version, true);
            }
            
            //Evaluador
            versiones = daoGral.obtieneVersionesSIGEIA(folioProyecto);
            for (Short version : versiones) {
                this.existeVersion.set((int)version, true);
            }
            
            
            //No presentar de la version 3 en adelante si no ha pasado por la situacion SIS303
            if ( dao.isSituacionTramiteHist(bitaProyecto, "CIS104")) {
                vistaVersion = GenericConstants.VISTA_PROMOVENTE_1ER_INGRESO;
                for (int idx = GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL; idx < MAX_VERSIONES_EXISTENTES+1; idx++ ) {
                    this.existeVersion.set(idx, false);
                }
            } else {
                vistaVersion = GenericConstants.VISTA_PROMOVENTE_INFO_ADICIONAL;
            }
            

            System.out.println("Lista existencia de versiones --> " + this.existeVersion);

            //Indicacion de Informacion adicional en el menu
            mapInfoAdicional = generaMapaInfoAdicional(bitaProyecto);
        }
        
        HistorialAT0001 = dao.numRegHistStatus(bitaProyecto,"AT0001");
        HistorialCIS303 = dao.numRegHistStatus(bitaProyecto,"CIS303");
        NoHistorialAT0022 = dao.numRegHistStatus(bitaProyecto,"AT0022");
        NoHistorialCIS106 = dao.numRegHistStatus(bitaProyecto,"CIS106"); 
        NoHistorial200501 = dao.numRegHistStatus(bitaProyecto,"200501");
        NoHistorialIRA020 = dao.numRegHistStatus(bitaProyecto,"IRA020");
        NoHistorialI00008 = dao.numRegHistStatus(bitaProyecto,"I00008");
        
        if((NoHistorialI00008.size() == 1 || NoHistorialI00008.isEmpty()) && NoHistorialAT0022.isEmpty() && NoHistorialCIS106.isEmpty() && NoHistorial200501.isEmpty()  && HistorialAT0001.size() == 1 && HistorialCIS303.isEmpty() && NoHistorialIRA020.isEmpty())//si no vienen de ninguna activacion
        { dePrevenc = false;  }
        else 
        {  
            if (!NoHistorialAT0022.isEmpty() || !NoHistorialCIS106.isEmpty() || !NoHistorial200501.isEmpty()  || HistorialAT0001.size() == 2 || HistorialCIS303.size() == 1 || NoHistorialIRA020.size() == 1) //si el tamanio es igual a dos, indica que el tramite ya esta activo despues de haber sido prevenido
            { dePrevenc = true; }
            else 
            { 
                dePrevenc = false;
            }   
        }
        
        //Realiza los calculos de promovente
        if (!fisico) {
            seleccionaRespuestaEvaluador();
        }
        
        //Condicion para permitir capturar por el subdirector el pago de derechos en caso de haber regresado el tramite
        //de una prevencion
        bReaperturaPagoDerechos = dao.reaperturaPagoDerechos(bitaProyecto) && (valorPromCalc.compareTo(montoCalc) != 0);
        
 
        logger.debug(Utils.obtenerLogConstructor("Fin"));
    }
    
    
	private Object[] buscaCriterio(List<Object[]> lstCriterios, String idCriterio) {
        int idx = Integer.parseInt(idCriterio);
        Object[] objCrit = lstCriterios.get(idx-1);
        String strIdCrit = objCrit[3].toString();
        
        //Si el criterio buscado corresponde a su posicion en la lista
        if (strIdCrit.compareTo(idCriterio) == 0 ) {
            return objCrit;
        } else {
            //En caso contrario se busca el criterio
            for (Object[] obj : lstCriterios) {
                strIdCrit = obj[3].toString();
                if (strIdCrit.compareTo(idCriterio) == 0 ) {
                    return objCrit;
                }
            }
            return null;
        }
    }

    @SuppressWarnings("unchecked")
	public void verificaTipoOficio ()
    {
        //-------------------verificar tipo de oficio si es de pago de derechos=3 o de pago de rechos y checkList = 5----------
        
        if (sesion != null) 
        {   
            if (!sesion.getAttribute("rol").toString().equals("dg")) //Director General
            {
                if (!sesion.getAttribute("rol").toString().equals("dg")) 
                {
                    Double bitamontoDouble = 0.0;
                    if(bitamonto != null)
                    {
                        bitamontoDouble = bitamonto.doubleValue();//bitamonto monto promovente
                    }

                    //verificar si existen datos en el pago de derechos y checkList para levantar bandera y poder generar oficio de prevencion
                    List<Object[]> criterios = daoDg.critValProyEva(folioProyecto, serialProyecto);
                    List<RequisitosSubdirector> chkList = (List<RequisitosSubdirector>) daoDg.lista_namedQuery("RequisitosSubdirector.findByReqsdBitacora", new Object[]{bitaProyecto}, new String[]{"reqsdBitacora"});

                    if (!criterios.isEmpty()) {
                        for (Object o : criterios) {
                            aux = Integer.parseInt(o.toString());
                        }
                        if (aux == 0) {
                            pagoDere = 0; //pago de derechos vacios
                        } else {
                            //si el usuario lleno el formulario de pago de derechos se procede a comparar el monto del promovente y el monto calculado por el SD
                            if(bitamontoDouble > montoCalc)
                            {                                 
                                pagoDere = 1; //pago de derechos lleno pero con prevencion
                            }
                            else
                            {
                                pagoDere = 0; //pago de derechos NO lleno
                            }                            
                        }
                    }

                    //chkList: ver si la tabla del checklist tiene valores
                    //requisitos: trae la consulta de la tabla de checklist, sin distinguir si fueron o no validados por el subdirector
                    //requisitosSi: trae la consulta de la tabla de checklist, solo los que SI fueron validados por el subdirector
                    if (chkList.size() > 0 && (requisitos.size() == requisitosSi.size()) && (requisitos.size() > 0 && requisitosSi.size() > 0)) {
                        checkList = 0; //checkList lleno, sin ninguna observacion
                    } else {
                        checkList = 1;
                    }

                    if(pagoDere == 1 && checkList == 1) //si el checklist y el pago de derechos no estan llenos, se generara un oficio de ambos
                    { tipoDoc = 5; }
                    System.out.println("tipo oficio validacion de pago de derechos: " + tipoDoc);

                    //boton de Turnar Oficio
                    try {
                        DocRespDgira = daoDg.docsRespDgiraBusq(bitaProyecto, cveTipTram, idTipTram, tipoDoc, edoGestTram, "1");
                    } catch (Exception er33) {}

                    if (DocRespDgira.size() > 0) {            
                        for (Object o : DocRespDgira) {
                            URLdb = o.toString();
                        }
                    } else {            
                        URLdb = "";
                    }

                    System.out.println("dePrevenc: " + dePrevenc);
                    if(dePrevenc) //si ya viene de una reactivacion por prevencion, ya no puede guardar check list y no puede generar oficio de prevencion
                    { 
                        URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                    }
                    else
                    {

                        if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) 
                        {                
                        	// pool delegaciones
                            areaResiveDocCon = dao.jerarquiaTbArea(docsDgiraFlujoHistEdit.getIdAreaEnvioHist(), idEntidad);               
                            for (Object o : areaResiveDocCon) {
                                jerarquia = o.toString();
                            }
                            if (jerarquia.equals("DG")) {
                                //statusDoc
                                if (statusDoc == 3) {
                                    perfilSig = "dg";
                                }
                                if (statusDoc == 2) {
                                    perfilSig = "da";
                                }
                            }
                            if (jerarquia.equals("DA")) {
                                //statusDoc
                                if (statusDoc == 3) {
                                    perfilSig = "dg";
                                }
                                if (statusDoc == 2) {
                                    perfilSig = "sd";
                                }
                            }
                            if (jerarquia.equals("SD")) {
                                //statusDoc
                                if (statusDoc == 3) {
                                    perfilSig = "da";
                                }
                                if (statusDoc == 2) {
                                    perfilSig = "eva";
                                }
                            }
                            if (jerarquia.equals("JD")) {
                                //statusDoc
                                if (statusDoc == 3) {
                                    perfilSig = "sd";
                                }
                            }
                            //si el perfil en session es igual al perfil al que se le envio el oficio, perimite la edicion del oficio
                            if (sesion.getAttribute("rol").toString().equals(perfilSig)) {
                                URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; //solo visualiza PDF y no lo modifica
                            } else {
                                URLgenOficPre = URLdb; //solo visualiza PDF y no lo modifica
                            }
                        } else {
            //                turnaDocsBtn = false;//visualizar boton de turnado
                            URLgenOficPre = Utils.rutaGeneradorDoctos + cveTipTram + "," + idTipTram + "," + edoGestTram + "," + tipoDoc + "," + bitaProyecto + ",1"; //solo visualiza PDF y no lo modifica  bitaProyOfic
                        }

                    }
                }
            }
        }
    }
    
    
    /**
     * 
     * @param bitacora
     * @return 
     * 
     * Mapa:
     * Capitulo-Subcapitulo --> existe info adicional, hay modificacion de info
     *          "2-1"       -->         true,                   false
     */
    public HashMap<String,boolean[]> generaMapaInfoAdicional(String bitacora) {
        HashMap<String,boolean[]> mapRes = new HashMap<>();
        Integer idx;
        ArrayList<String[]> lstInfo;
        boolean[] bRes;
        
        //Inicializa el mapa con 10 capitulos
        for (idx = 1; idx <= 10; idx++) {
            mapRes.put("cap_" + idx.toString(), new boolean[]{false, false});
        }
        
        lstInfo = daoGral.obtieneComenariosProyectoCapitulo(bitacora);
        for (Object[] reg : lstInfo) {
            bRes = new boolean[2];
            bRes[0] = ((String)reg[1]).compareTo("1") == 0;
            bRes[1] = ((String)reg[2]).compareTo("1") == 0;
           
            mapRes.put( "cap_" + reg[0], bRes);
        }
        
        return mapRes;
    }

    
    /**
     * 
     * @param mapa
     * @param capitulo
     * @param subcapitulo
     * @param existe -- true si se solicita existencia, false si se solicita modificacion
     * @return 
     */
    @SuppressWarnings("rawtypes")
	public boolean estadoInfoAdicional(HashMap mapa, Integer capitulo, Integer subcapitulo, boolean existe) {
        boolean bRes = false;
        boolean[] info;
        int idx;
        String strUbicacion;
        
        idx = existe ? 0 : 1;//Existe informacion adicional o hay modificacion
        strUbicacion = capitulo + "-" + subcapitulo;
        try {
            info = (boolean[])mapa.get(strUbicacion);
            if ( info != null ){
                bRes = info[idx];
            } else {
                bRes = false;//No existe la ubicacion solicitada
            }
            
        } catch (Exception e){
            System.out.println("Ocurrió un error durante la obtención en el mapa de información adicional " + e.toString());
            e.printStackTrace();
        }
        
        return bRes;
    }

    public void guardaEva() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        FacesContext ctxt = FacesContext.getCurrentInstance();
        if (eval != null) {
            eval.setBitacoraProyecto(bitaProyecto);
        }
        try{
            
            daoDg.modifica(eval);
            ctxt.addMessage("growl", new FacesMessage("Síntesis para la gaceta fue guardada exitosamente."));
        }
        catch(Exception e)
        {   }
        detalleView.sintesisGacetaCompleto();
        if(detalleView.getActiveTab() != 0)
        	reqcontEnv.execute("PF('myTabView').select("+ detalleView.getActiveTab() +");");
        
    }

    public void guarda() {
        FacesContext ctxt = FacesContext.getCurrentInstance();
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        Integer banderaLLeno=0;
        CriterioValoresProyecto cvp;
        System.out.println("flag--------------");
        //Valida que todos los criterios hayan sido capturados
        int criterios = 0;
        for (WizardHelper ele : lista) {
            if ( ele.getValorEval() > 0) {
                criterios++;
            }
            if (ele.getValorProm() > 0) {
                criterios++;
            }
        }
        
        if ( criterios == (lista.size()*2) ) {//Si todos los criterios fueron capturados 
            
            //Cargar Proyecto
            try {
                this.proyecto = dao.proyecto(folioProyecto, serialProyecto);
            } catch (Exception e) {
                System.out.println("No encontrado");
            }

            for (WizardHelper helper : lista) {

                cvp = (CriterioValoresProyecto) daoDg.lista_namedQuery("CriterioValoresProyecto.findByIdCriterioANDFolioSerial", new Object[]{new BigInteger(helper.id.toString()), folioProyecto, serialProyecto}, new String[]{"idCriterio", "folioProyecto","serialProyecto"}).get(0);
                System.out.println("CVP = " + cvp + "**************************************");
                if(cvp!=null)
                { banderaLLeno = 1; } //editar
                else
                { banderaLLeno = 0; } //nuevo

                if(banderaLLeno == 0)
                { cvp = new CriterioValoresProyecto(folioProyecto, serialProyecto, new BigInteger(helper.id.toString()), new BigInteger(dao.valor(helper.id, helper.seleccion).toString()));   
                    //cvp.setProyecto((mx.gob.semarnat.model.dgira_miae.Proyecto) dao.busca(Proyecto.class, new ProyectoPK(folioProyecto, serialProyecto)));
                    cvp.setCatDerechosCriteriosvalores((CatDerechosCriteriosvalores) dao.busca(CatDerechosCriteriosvalores.class, new CatDerechosCriteriosvaloresPK(helper.id, helper.valor)));
                }
                cvp.setCriterioValorEvaluacion(new BigInteger(dao.valor(helper.id, helper.seleccion).toString()));
                if ( fisico || bReaperturaPagoDerechos ) {
                    cvp.setCriterioValorUsuario(new BigInteger(dao.valor(helper.id, helper.seleccionProm).toString()));
                }
                cvp.setBitacoraProyecto(bitaProyecto);
                cvp.setCriterioObservacionEva(helper.referencia);
                System.out.println("CVP = " + cvp + "**************************************22");
                try {
                    if(banderaLLeno == 0)
                    {  dao.agrega(cvp); }
                    if(banderaLLeno == 1)
                    {  dao.modifica(cvp);  }   

                } catch (Exception e) {

                }
            }
            calcula();
            
            if(checkList == 1 && pagoDere == 1 && supera)
            { oficPrev = true; }
            else { oficPrev = false; } //pagoDere  checkList 
            
            bReaperturaPagoDerechos = dao.reaperturaPagoDerechos(bitaProyecto) && (valorPromCalc.compareTo(montoCalc) != 0);    
            
            ctxt.addMessage("growl", new FacesMessage("Datos del pago de derechos guardados exitosamente.", doc));
            
        } else {
            reqcontEnv.execute("alert('Es requerido indicar todos los criterios, verifique su información e intente nuevamente.');");
            //JOptionPane.showMessageDialog(null, "Es requerido indicar todos los criterios, verifique su infomracion e intente nuevamente.", "Informacion faltante", JOptionPane.INFORMATION_MESSAGE);
        }    
        auxValida();
    }
    
    public void auxValida(){
    	RequestContext reqcontEnv = RequestContext.getCurrentInstance();
         detalleView.pagoValidadoCompleto(this.sum, this.sumaPromovente);
         
         if(detalleView.getActiveTab() != 0){
         	reqcontEnv.execute("PF('myTabView').select("+ detalleView.getActiveTab() +");");            
         }
    }

    public void seleccionaRespuesta() {
        int puntaje = 0;
        for (WizardHelper n : lista) {
            if ( n.seleccion == null || n.seleccion.compareTo("-1") == 0) {
                n.valor = 0;
                n.valorEval = 0;
            } else {
                vp = dao.valor(n.id, n.seleccion);
                n.valor = vp;
                n.valorEval = vp;

                doc = dao.descValor(n.id, n.seleccion);
            }
            
            puntaje += n.valorEval;
        }
        
        this.sum = puntaje;
        this.montoCalc = calculaMontoEvaluador(tramite, puntaje);

    }

    public void seleccionaRespuestaEvaluador() {
        int puntaje = 0;
        for (WizardHelper n : lista) {
            if ( n.seleccionProm == null || n.seleccionProm.compareTo("-1") == 0) {
                n.valorProm = 0;
            } else {
                vp = dao.valor(n.id, n.seleccionProm);
                n.valorProm = vp;
                //doc = dao.descValor(n.id, n.seleccion);
            }
            
            puntaje += n.valorProm;
        }
        
        this.sumaPromovente = puntaje;
        this.valorPromCalc = calculaMontoEvaluador(tramite, puntaje);

    }

    public boolean calcula() {
        sum = 0;
        sum2 = 0;
        FacesContext ctxt = FacesContext.getCurrentInstance();
        for (WizardHelper n : lista) {
//           System.out.println(" seleccion :  " + n.seleccion);
            if (n.seleccion != null) {
                vp = dao.valor(n.id, n.seleccion);
                valor = vp;
                doc = dao.descValor(n.id, n.seleccion);
                sum = sum + valor;
                sum2 = sum2 + n.getValorProm();
            } else {
            }
        }
        
        
        //-------verficar si el checkLista y la validacion de pago d derechos han sido llenadas, para asi poder habilitar el boton de oficio de prevnecion
        //verificar si existen datos en el pago de derechos y checkList para levantar bandera y poder generar oficio de prevencion
        List<Object[]> criterios1 = daoDg.critValProyEva(folioProyecto, serialProyecto);       
        if (!criterios1.isEmpty() && criterios1.get(0) != null)
        {            
            for (Object o : criterios1) {               
                aux = Integer.parseInt(o.toString());
            }             
            //JOptionPane.showMessageDialog(null,  "33xxx:  " + aux , "Error", JOptionPane.INFORMATION_MESSAGE);
            if( aux == 0)
            { pagoDere = 0; } 
            else { pagoDere = 1; }
        } else {
            pagoDere = 0;
        }
        requisitos = daoDirec.getReqTramite(bitaProyecto);
        requisitosSi = daoDirec.getReqTramiteSI(bitaProyecto);  
        if(requisitos.size()>0 &&  requisitosSi.size()>0)
        { checkList = 1; } 
        else { checkList = 0; } 
        
        
        APDao.guardaMonto(bitaProyecto, sum, folioProyecto);
        //montoCalc = fisico ? valorPromCalc : APDao.montoCalculado(bitaProyecto);
        
        EvaluacionProyecto ev = (EvaluacionProyecto) daoDg.busca(EvaluacionProyecto.class, bitaProyecto);
        if (ev != null) {
            ev.setEvaMontCritEvaluador(new BigDecimal(montoCalc));
            ev.setEvaMontCritPromovente(bitamonto);
            ev.setEvaValCritEvaluador(new Short(sum.toString()));
            ev.setEvaValCritPromovente(new Short(sum2.toString()));  
            ev.setEvaCheckListRealizado(checkList.toString());
            ev.setEvaPagoDerechosRealizado(pagoDere.toString());
            daoDg.modifica(ev);        
        }else{
            //eval = new EvaluacionProyecto();
            ev = new EvaluacionProyecto(bitaProyecto);
            ev.setEvaMontCritEvaluador(new BigDecimal(montoCalc));
            ev.setEvaMontCritPromovente(bitamonto);
            ev.setEvaValCritEvaluador(new Short(sum.toString()));
            ev.setEvaValCritPromovente(new Short(sum2.toString()));  
            ev.setEvaCheckListRealizado(checkList.toString());
            ev.setEvaPagoDerechosRealizado(pagoDere.toString());
            daoDg.agrega(ev);        
        }        

        if (montoCalc > bitamonto.doubleValue()) {
            ctxt.addMessage("growl", new FacesMessage("El monto otorgado por el promovente es menor al esperado, debe generar un oficio de prevencion en la sección validación de checklist.", doc));
            supera = true;
        }
        else{supera = false;}
        
        return true;
    }

    public boolean isSupera() {
        return supera;
    }

    public Double getMontoCalc() {
        return montoCalc;
    }

    public void setMontoCalc(Double montoCalc) {
        this.montoCalc = montoCalc;
    }

    /**
     * @return the todosProySubD
     */
    public List<Object[]> getTodosProySubD() {
        return todosProySubD;
    }

    /**
     * @param todosProySubD the todosProySubD to set
     */
    public void setTodosProySubD(List<Object[]> todosProySubD) {
        this.todosProySubD = todosProySubD;
    }

    public List<WizardHelper> getLista() {
        return lista;
    }

    public void setLista(List<WizardHelper> lista) {
        this.lista = lista;
    }

    public List<String> getTemp() {
        return temp;
    }

    public void setTemp(List<String> temp) {
        this.temp = temp;
    }

    public Integer getVp() {
        return vp;
    }

    public void setVp(Integer vp) {
        this.vp = vp;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public List<Object[]> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<Object[]> niveles) {
        this.niveles = niveles;
    }

    public String getBitaProyecto() {
        return bitaProyecto;
    }

    public void setBitaProyecto(String bitaProyecto) {
        this.bitaProyecto = bitaProyecto;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public String getTramite() {
        return tramite;
    }

    public void setTramite(String tramite) {
        this.tramite = tramite;
    }

    public BigDecimal getBitamonto() {
        return bitamonto;
    }

    public void setBitamonto(BigDecimal bitamonto) {
        this.bitamonto = bitamonto;
    }

    public EvaluacionProyecto getEval() {
        return eval;
    }

    public void setEval(EvaluacionProyecto eval) {
        this.eval = eval;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the llenado
     */
    public boolean isLlenado() {
        return llenado;
    }

    /**
     * @param llenado the llenado to set
     */
    public void setLlenado(boolean llenado) {
        this.llenado = llenado;
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the mantoC
     */
    public Integer getMantoC() {
        return mantoC;
    }

    /**
     * @param mantoC the mantoC to set
     */
    public void setMantoC(Integer mantoC) {
        this.mantoC = mantoC;
    }

    /**
     * @return the oficPrev
     */
    public Boolean getOficPrev() {
        return oficPrev;
    }

    /**
     * @param oficPrev the oficPrev to set
     */
    public void setOficPrev(Boolean oficPrev) {
        this.oficPrev = oficPrev;
    }

    /**
     * @return the pagoDere
     */
    public Integer getPagoDere() {
        return pagoDere;
    }

    /**
     * @param pagoDere the pagoDere to set
     */
    public void setPagoDere(Integer pagoDere) {
        this.pagoDere = pagoDere;
    }

    /**
     * @return the checkList
     */
    public Integer getCheckList() {
        return checkList;
    }

    /**
     * @param checkList the checkList to set
     */
    public void setCheckList(Integer checkList) {
        this.checkList = checkList;
    }

    /**
     * @return the sintesisGaseta
     */
    public String getSintesisGaseta() {
        return sintesisGaseta;
    }

    /**
     * @param sintesisGaseta the sintesisGaseta to set
     */
    public void setSintesisGaseta(String sintesisGaseta) {
        this.sintesisGaseta = sintesisGaseta;
    }

    /**
     * @return the cveTipTram
     */
    public String getCveTipTram() {
        return cveTipTram;
    }

    /**
     * @param cveTipTram the cveTipTram to set
     */
    public void setCveTipTram(String cveTipTram) {
        this.cveTipTram = cveTipTram;
    }

    /**
     * @return the idTipTram
     */
    public Integer getIdTipTram() {
        return idTipTram;
    }

    /**
     * @param idTipTram the idTipTram to set
     */
    public void setIdTipTram(Integer idTipTram) {
        this.idTipTram = idTipTram;
    }

    /**
     * @return the edoGestTram
     */
    public String getEdoGestTram() {
        return edoGestTram;
    }

    /**
     * @param edoGestTram the edoGestTram to set
     */
    public void setEdoGestTram(String edoGestTram) {
        this.edoGestTram = edoGestTram;
    }

    public int getSumaPromovente() {
        return sumaPromovente;
    }

    public void setSumaPromovente(int sumaPromovente) {
        this.sumaPromovente = sumaPromovente;
    }

    public int getVistaVersion() {
        return vistaVersion;
    }

    public void setVistaVersion(int vistaVersion) {
        this.vistaVersion = vistaVersion;
    }

    public ArrayList<Boolean> getExisteVersion() {
        return existeVersion;
    }

    /**
     * @return the mapInfoAdicional
     */
    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    /**
     * @param mapInfoAdicional the mapInfoAdicional to set
     */
    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

    /**
     * @return the dePrevenc
     */
    public Boolean getDePrevenc() {
        return dePrevenc;
    }

    /**
     * @param dePrevenc the dePrevenc to set
     */
    public void setDePrevenc(Boolean dePrevenc) {
        this.dePrevenc = dePrevenc;
    }

    /**
     * @return the URLgenOficPre
     */
    public String getURLgenOficPre() {
        return URLgenOficPre;
    }

    /**
     * @param URLgenOficPre the URLgenOficPre to set
     */
    public void setURLgenOficPre(String URLgenOficPre) {
        this.URLgenOficPre = URLgenOficPre;
    }

    public boolean isFisico() {
        return fisico;
    }

    public Double getValorPromCalc() {
        return valorPromCalc;
    }

    public boolean isbReaperturaPagoDerechos() {
        return bReaperturaPagoDerechos;
    }


    public class WizardHelper implements Serializable {

        private Integer id;
        private String descripcion;
        private List<String> respuesta;
        private String seleccion;
        private String seleccionProm;
        private Integer valor;
        private Integer valorProm;
        private String referencia;
        private Integer valorEval;

        public WizardHelper(Integer id, String descripcion, List<String> respuesta) {
            this.id = id;
            this.descripcion = descripcion;
            this.respuesta = respuesta;
            this.valorEval = 0;
            this.seleccionProm = "-1";
            this.valorProm = 0;
        }

        public WizardHelper(Integer id, String descripcion, List<String> respuesta, String seleccion, Integer valor, String referencia, 
                Integer valorProm, String seleccionProm) { //, Integer valorEval
            this.id = id;
            this.descripcion = descripcion;
            this.respuesta = respuesta;
            this.seleccion = seleccion;
            this.seleccionProm = seleccionProm;
            this.valorEval = valor;
            this.referencia = referencia;
            this.valorProm = valorProm;
           // this.valorEval = valorEval;
            //this.valorEval = 0;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public List<String> getRespuesta() {
            return respuesta;
        }

        public void setRespuesta(List<String> respuesta) {
            this.respuesta = respuesta;
        }

        public String getSeleccion() {
            return seleccion;
        }

        public void setSeleccion(String seleccion) {
            this.seleccion = seleccion;
        }

        public String getReferencia() {
            return referencia;
        }

        public void setReferencia(String referencia) {
            this.referencia = referencia;
        }

        public Integer getValor() {
            return valor;
        }

        /**
         * @return the valorProm
         */
        public Integer getValorProm() {
            return valorProm;
        }

        /**
         * @param valorProm the valorProm to set
         */
        public void setValorProm(Integer valorProm) {
            this.valorProm = valorProm;
        }

        /**
         * @return the valorEval
         */
        public Integer getValorEval() {
            return valorEval;
        }

        /**
         * @param valorEval the valorEval to set
         */
        public void setValorEval(Integer valorEval) {
            this.valorEval = valorEval;
        }

        public String getSeleccionProm() {
            return seleccionProm;
        }

        public void setSeleccionProm(String seleccionProm) {
            this.seleccionProm = seleccionProm;
        }
    }
    
    //<editor-fold desc="Getters & Setters" defaultstate="collapsed">
    public int getSumatoria() {
        int s = 0;
        for (WizardHelper n : lista) {
            if (n.seleccion != null) {
                s += n.valor;
            }
        }
        return s;
    }

    public double calculaMontoEvaluador(String idTramite, int puntaje) {
        Character idNivel;
        double monto = 0;
        
        if (puntaje >= DERECHOS_LIM_INF_NIVEL_A && puntaje <= DERECHOS_LIM_SUP_NIVEL_A ) {
            idNivel = DERECHOS_NIVEL_A;
        } else if (puntaje >= DERECHOS_LIM_INF_NIVEL_B && puntaje <= DERECHOS_LIM_SUP_NIVEL_B ) {
            idNivel = DERECHOS_NIVEL_B;
        } else if (puntaje >= DERECHOS_LIM_INF_NIVEL_C && puntaje <= DERECHOS_LIM_SUP_NIVEL_C ) {
            idNivel = DERECHOS_NIVEL_C;
        } else {
            idNivel = ' ';
        }
        
        monto = daoDg.obtenerMontoDerechos(idNivel, idTramite);
        
        return monto;
    }


	public DetalleProyectoView getDetalleView() {
		return detalleView;
	}


	public void setDetalleView(DetalleProyectoView detalleView) {
		this.detalleView = detalleView;
	}
}
