package mx.gob.semarnat.mia.servicios;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class ServicioConsulta {
	
	public List<Object[]> getAllConsultas(String bitacora, String cveProyecto){
		StringBuilder sb = new StringBuilder();
		List<Object[]> list = new ArrayList<>();
		sb.append("SELECT D.IDDOCUMENTO, CASE WHEN D.NOMBREREMITENTE IS NULL THEN 'N/A' ");
		sb.append("ELSE D.NOMBREREMITENTE END  AS REMITENTE, TO_CHAR(D.FECHAREGISTRO, 'DD/MM/YYYY') AS FECHA_REG,  CASE WHEN D.FECHALIMITE IS NULL THEN 0 ");
		sb.append("ELSE 1 END AS HAS_FLIM, CASE when (TO_DATE(d.FECHALIMITE,'dd/mm/yy')-TO_DATE(d.FECHAREGISTRO,'dd/mm/yy')) > 0 then 1 ");
		sb.append("ELSE 0 END as en_tiempo FROM CORRESPONDENCIA.TB_DOCUMENTO D INNER JOIN CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO C ");
		sb.append("ON D.IDCATEGORIA = C.IDCATEGORIA INNER JOIN CORRESPONDENCIA.TB_RELDOCTRAMITE R ON D.IDDOCUMENTO = R.IDDOCUMENTO ");
		sb.append("WHERE  R.BITACORAPROYECTO IN ('").append(bitacora).append("','").append(cveProyecto).append("') ORDER BY D.IDDOCUMENTO");
		EntityManager em = MiaSingleton.getInstance().createEntityManager();
		System.out.println("SQL::::> " + sb.toString());
		Query q = em.createNativeQuery(sb.toString());
		if (!q.getResultList().isEmpty()){
			System.out.println("no vacio");
			list = q.getResultList();
		} else{
			System.out.println("si vacio");
			Date fecha = new Date();
			Object[] o = {"","", fecha, 0, 0};
			list.add(o);
		}
		return list;
	}
}
