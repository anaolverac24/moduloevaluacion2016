package mx.gob.semarnat.mia.servicios.dgiramiae;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Query;

import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.ExencionRequisitosEvaluador;
import mx.gob.semarnat.model.dgira_miae.ExencionRequisitosEvaluadorPk;

public class ServicioExencionRequisitosEvaluador extends GenericDaoMia<ExencionRequisitosEvaluador, ExencionRequisitosEvaluadorPk> {

    public List<ExencionRequisitosEvaluador> encontrarPorIdExencionPorSeccPorReq(long idExencion, short nsecc, short nreq, short serial) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT REQ.* FROM EXENCION_REQUISITOS_EVALUADOR REQ ");
        sb.append("INNER JOIN EXENCION EX ON REQ.ID_EXENCION = EX.ID_EXENCION ");
        sb.append("WHERE EX.ID_EXENCION = " + idExencion + " ");
        sb.append("AND EX.SERIAL = " + serial + " ");
        sb.append("AND REQ.nseccion = " + nsecc + " AND REQ.nrequisito = " + nreq + " ");
        List<ExencionRequisitosEvaluador> l = null;
        Query q = this.getEntityManager().createNativeQuery(sb.toString(), ExencionRequisitosEvaluador.class);
        l = q.getResultList();
        this.getEm().getTransaction().commit();
        // try {
        // l = this.findByNamedQuery("ExencionRequisitosEvaluador.findByExeBySeccByReq", idExencion, nsecc, nreq);
        // } catch (MiaExcepcion e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        return l;
    }

    public List<ExencionRequisitosEvaluador> encontrarPorIdExencionPorSeccPorNReq(long idExencion, short nsecc, List<Short> idsReq, short serial) {
        String ids = "";
        for (short idR : idsReq) {
            ids += idR + ",";
        }
        ids = ids.substring(0, ids.length() - 1);
        //
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT REQ.* FROM EXENCION_REQUISITOS_EVALUADOR REQ ");
        sb.append("INNER JOIN EXENCION EX ON REQ.ID_EXENCION = EX.ID_EXENCION AND EX.ID_EXENCION = " + idExencion + " ");
        sb.append("WHERE EX.SERIAL = " + serial + " ");
        sb.append("AND REQ.nseccion = " + nsecc + " AND REQ.nrequisito IN (" + ids + ") ");
        List<ExencionRequisitosEvaluador> l = null;
        Query q = this.getEntityManager().createNativeQuery(sb.toString(), ExencionRequisitosEvaluador.class);
        l = q.getResultList();
        this.getEm().getTransaction().commit();
        return l;
    }

    public Object[] encontrarExencionIdPorBitacora(String bitacora, short serial) throws MiaExcepcion {
        String sql = "SELECT ID_EXENCION, bgtramitedid as folio FROM DGIRA_MIAE2.EXENCION WHERE NUMERO_BITACORA = '" + bitacora + "' AND SERIAL = " + serial + " AND LOTE IS NOT NULL";

        Object[] datos = null;
        try {
            Query q = this.getEntityManager().createNativeQuery(sql);
            List<Object[]> l = q.getResultList();
            this.getEm().getTransaction().commit();
            //
            if (l != null && !l.isEmpty()) {
                datos = l.get(0);
            }
        } catch (Exception e) {
            this.getEm().getTransaction().rollback();
            throw new MiaExcepcion("GenericDAO.errorConsultar");
        } finally {
            this.getEm().close();
            this.setEm(null);
        }

        return datos;
    }
    
    
    @SuppressWarnings("unchecked")
	public int getReqTramiteDigital(String bitacora) {
            BigDecimal l = null;
        try {
        Query q = this.getEntityManager().createNativeQuery("SELECT DISTINCT B.REQSD_NUMERO_REQUISITO FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B"+
										" WHERE B.REQSD_BITACORA = '"+ bitacora +"' AND"
										+" B.REQSD_NOMBRE_REQUISITO like '%pago de derechos%'");
         l =  (BigDecimal) q.getSingleResult();
         } catch (Exception e) {
            this.getEm().getTransaction().rollback();
          
        } finally {
            this.getEm().close();
            this.setEm(null);
        }
        int r = 0;
        if(l!= null)
            r=l.intValue();
        return r;
       
    }
        
        @SuppressWarnings("unchecked")
	public int getReqTramite(String bitacora) {
            BigDecimal l = null;
        try {
        Query q = this.getEntityManager().createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO " +
                                                            "FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B , " +
                                                            "  CATALOGO_TRAMITES.CAT_REQUISITOS C " +
                                                            "WHERE B.REQSD_NUMERO_REQUISITO = C.ID_REQUISITO " +
                                                            "AND B.REQSD_BITACORA           = '"+ bitacora +"' ANDB.REQSD_NOMBRE_REQUISITO like '%pago de derechos%'");
         l =  (BigDecimal) q.getSingleResult();
         } catch (Exception e) {
            this.getEm().getTransaction().rollback();
          
        } finally {
            this.getEm().close();
            this.setEm(null);
        }
        int r = 0;
        if(l!= null)
            r=l.intValue();
        return r;
       
    }

}
