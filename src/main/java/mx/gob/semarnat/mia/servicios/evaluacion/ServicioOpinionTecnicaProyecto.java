package mx.gob.semarnat.mia.servicios.evaluacion;

import java.util.List;
import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.OpinionTecnicaProyecto;

public class ServicioOpinionTecnicaProyecto extends GenericDaoMia<OpinionTecnicaProyecto, OpinionTecnicaProyecto> {
	
	public OpinionTecnicaProyecto updateSeleccion (String bitacora, long idDependencia) throws MiaExcepcion {
		String sql = "select  * from OPINION_TECNICA_PROYECTO "
				+ "where BITACORA_PROYECTO = '" + bitacora + "' "
				+ "and cat_dependencia_id = " + idDependencia;
		
		System.out.println(sql);
		OpinionTecnicaProyecto opinion = (OpinionTecnicaProyecto) this.getEntityManager().createNativeQuery(sql, OpinionTecnicaProyecto.class).getSingleResult();
		
		opinion.setOpiniontSeleccion(1);
		this.save(opinion);
		
		return opinion;
	}
	
	public OpinionTecnicaProyecto updateDesSeleccion (String bitacora, long idDependencia, String idOficio) throws MiaExcepcion {
		String sql = "select  * from OPINION_TECNICA_PROYECTO "
				+ "where BITACORA_PROYECTO = '" + bitacora + "' "
				+ "and cat_dependencia_id = " + idDependencia;
		
		System.out.println(sql);
		OpinionTecnicaProyecto opinion = (OpinionTecnicaProyecto) this.getEntityManager().createNativeQuery(sql, OpinionTecnicaProyecto.class).getSingleResult();
		
		opinion.setOpiniontSeleccion(0);
                opinion.setOpiniontOficio(Integer.valueOf(idOficio));
		this.save(opinion);
		
		return opinion;
	}
        
        public OpinionTecnicaProyecto findOficio (String bitacora, long idDependencia) throws MiaExcepcion {
		String sql = "select  * from OPINION_TECNICA_PROYECTO "
				+ "where BITACORA_PROYECTO = '" + bitacora + "' "
				+ "and cat_dependencia_id = " + idDependencia;
		
		System.out.println(sql);
		OpinionTecnicaProyecto opinion = (OpinionTecnicaProyecto) this.getEntityManager().createNativeQuery(sql, OpinionTecnicaProyecto.class).getSingleResult();
				
		return opinion;
	}
        
        public boolean findExpedient (String bitacora, long idDependencia) throws MiaExcepcion {
            String sql = "select  * from OPINION_TECNICA_PROYECTO "
                        + " where BITACORA_PROYECTO = '" + bitacora + "' "
                        + " and cat_dependencia_id = " + idDependencia
                        + " and OPINIONT_OFICIO <> 0 ";
		
            System.out.println(sql);
            List<OpinionTecnicaProyecto> opinion = this.getEntityManager().createNativeQuery(sql, OpinionTecnicaProyecto.class).getResultList();
            if (opinion.size() > 0) {
                return true;
            } else {
                return false;
            }	
	}
}
