package mx.gob.semarnat.mia.servicios.evaluacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import mx.gob.semarnat.mia.servicios.GenericDaoEvaluacion;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.bitacora.HistorialPK;

public class ServicioBitacora_Historial extends GenericDaoEvaluacion<Historial, HistorialPK> {

	public Historial encontrarPorPK(int consecutivo, String numero) throws MiaExcepcion {
		HistorialPK pk = new HistorialPK();
		pk.setHistoConsecutiv(consecutivo);
		pk.setHistoNumero(numero);
		//
		Historial p = null;
		try {
			p = this.findById(pk);
		} catch (MiaExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MiaExcepcion("ServicioProyecto.errorConsultar");
		}

		return p;
	}

	@SuppressWarnings("unchecked")
	public boolean existeBitacoraPorSituaciones(String bitacora, String[] situaciones) throws MiaExcepcion {
		StringBuilder sb = new StringBuilder();
		String sit = "";
		for (String s : situaciones) {
			sit += s + ",";
		}
		sb.append("SELECT B.BITA_NUMERO FROM BITACORA.BITACORA B ");
		sb.append("INNER JOIN PROYECTO P ");
		sb.append("ON P.BITACORA_PROYECTO = B.BITA_NUMERO ");
		// sb.append("WHERE B.BITA_NUMERO = '09/MPW0001/10/16'");
		sb.append("WHERE B.BITA_NUMERO = '" + bitacora + "'");
		// sb.append("AND B.BITA_SITUACION IN('AT0002','AT0003','AT0031','AT0032','AT0008')");
		sb.append("AND B.BITA_SITUACION IN(" + sit.substring(0, sit.length() - 1) + ")");
		String query = sb.toString();
		System.out.println(query);
		//
		List<Object> l;
		boolean bitacoraEncontrada = false;
		try {
			Query q = getEntityManager().createNativeQuery(query);
			l = q.getResultList();
			commit();
			//
			if (l != null && l.size() > 0) {
				bitacoraEncontrada = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorActualizar");
		} finally {
                    try{
                        this.getEm().clear();
                    } catch(Exception e){
                        e.printStackTrace();
                    }
		}
		return bitacoraEncontrada;
	}

	public Historial encontrarPorBitacoraPorSituacion(String bitacora, String situacion) throws MiaExcepcion {
		Historial h = null;
		try {
			List<Historial> l = this.findByNamedQuery("Historial.findByBitacoraBySituacion", bitacora, situacion);
			if (l != null && l.size() > 0) {
				h = l.get(0);
			}
		} catch (MiaExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MiaExcepcion("ServicioProyecto.errorConsultar");
		}

		return h;
	}

	public Date obtenFechaTurnadoPorBitacoraPorSituacion(String bitacora, String situacion) throws MiaExcepcion {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT B.BITA_FECHATUR FROM BITACORA.BITACORA B ");
		sb.append("INNER JOIN PROYECTO P ");
		sb.append("ON P.BITACORA_PROYECTO = B.BITA_NUMERO ");
		sb.append("WHERE B.BITA_NUMERO = '" + bitacora + "'");
		sb.append("AND B.BITA_SITUACION = '" + situacion + "'");
		String query = sb.toString();
		System.out.println(query);
		//
		List<Object> l;
		Date fechaTur = null;
		try {
			Query q = getEntityManager().createNativeQuery(query);
			l = q.getResultList();
			commit();
			//
			if (l != null && l.size() > 0) {
				fechaTur = (Date) l.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorActualizar");
		} finally {
			this.getEm().close();
		}
		return fechaTur;
	}

	public int diasHabilesRestantesPorRangoFechas(Date fechaPivote, Date fechaActual, int maximoDiasHabiles) throws MiaExcepcion {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		String fP = sdf.format(fechaPivote);
		String fA = sdf.format(fechaActual);
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT FUNC_DIAS_TRANSCURRIDOS( ");
		sb.append("(SELECT SUMA_DIAS_HABILES(TO_DATE('" + fP + "','dd/MM/yy')," + maximoDiasHabiles + ") FROM DUAL),");
		sb.append("TO_DATE('" + fA + "','dd/MM/yy')");
		sb.append(")*-1 as DIAS_RESTANTES FROM DUAL");
		String query = sb.toString();
		System.out.println(query);
		//
		BigDecimal bd;
		int n;
		try {
			Query q = getEntityManager().createNativeQuery(query);
			bd = (BigDecimal) q.getSingleResult();
			n = Integer.parseInt(bd.toString());
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorActualizar");
		} finally {
			this.getEm().close();
		}
		return n;
	}
        
    public int diasRestantesPresentarIA(String bitacora) throws MiaExcepcion {
        String sql = "SELECT"
                + "    (SELECT DIAS_NOTIFICADO FROM sinat_usdb.sinat_oficio_notificacion"
                + "     WHERE bita_numero = :bitacora AND id_tipo_oficio = 1) -"
                + "    (SELECT dias_proceso_notificado FROM sinat_usdb.sinat_oficio_notificacion"
                + "     WHERE bita_numero = :bitacora AND id_tipo_oficio = 1) AS DIAS_RESTANTES "
                + "FROM DUAL";
        BigDecimal bd;
        int n;
        try {
            Query q = getEntityManager().createNativeQuery(sql);
            q.setParameter("bitacora", bitacora);
            bd = (BigDecimal) q.getSingleResult();
            n = Integer.parseInt(bd.toString());
        } catch (NumberFormatException e) {
            rollback();
            throw new MiaExcepcion("GenericDAO.errorActualizar");
        } finally {
            this.getEm().close();
        }
        return n;
    }
}
