package mx.gob.semarnat.mia.servicios;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class GenericDaoEvaluacion<T, ID extends Serializable> implements GenericDaoEvaluacionInterface<T, ID> {

	private final Class<T> persistentClass;
	private EntityManager em = null;
	// private EntityTransaction transaccion = null;
	private boolean autoCommit;

	@SuppressWarnings("unchecked")
	public GenericDaoEvaluacion() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this.autoCommit = true;
	}

	@Override
	public Class<T> getEntityClass() {
		return persistentClass;

	}

	@Override
	public T findById(ID id) throws MiaExcepcion {
		final T result;
		try {
			result = getEntityManager().find(this.getEntityClass(), id);
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorConsultar");
		} finally {
			em.close();
			em = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() throws MiaExcepcion {
		List<T> l = null;
		try {
			Query q = getEntityManager().createQuery("FROM " + this.getEntityClass().getSimpleName());
			l = (List<T>) q.getResultList();
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorConsultar");
		} finally {
			em.close();
			em = null;
		}
		return l;
	}

	@Override
	public T singleResultByNamedQuery(String queryName, Object... params) throws MiaExcepcion {
		final T result;
		try {
			Query q = getEntityManager().createNamedQuery(queryName);
			for (int i = 0; i < params.length; i++) {
				q.setParameter(i + 1, params[i]);
			}
			result = (T) q.getSingleResult();
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorConsultar");
		} finally {
			em.close();
			em = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQuery(String queryName, Object... params) throws MiaExcepcion {
		final List<T> result;
		try {
			Query q = getEntityManager().createNamedQuery(queryName);
			for (int i = 0; i < params.length; i++) {
				q.setParameter(i + 1, params[i]);
			}
			result = (List<T>) q.getResultList();
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorConsultar");
		} finally {
			em.close();
			em = null;
		}
		return result;
	}

	@Override
	public int updateNativeQuery(String query) throws MiaExcepcion {
		int rs;
		try {
			Query q = getEntityManager().createNativeQuery(query);
			rs = q.executeUpdate();
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorActualizar");
		} finally {
			em.close();
			em = null;
		}

		return rs;
	}

	@Override
	public T save(T entity) throws MiaExcepcion {
		final T savedEntity;
		try {
			savedEntity = getEntityManager().merge(entity);
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorGuardar");
		} finally {
			em.close();
			em = null;
		}
		return savedEntity;
	}

	@Override
	public void delete(ID id) throws MiaExcepcion {
		try {
			T entity = getEntityManager().find(persistentClass, id);
			getEntityManager().remove(entity);
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorEliminar");
		} finally {
			em.close();
			em = null;
		}
	}

	public EntityManager getEntityManager() {
		txInit();
		return this.em;
	}

	public void rollback() {
		this.em.getTransaction().rollback();
	}

	protected void commit() {
		if (this.autoCommit) {
			this.em.getTransaction().commit();
		}
	}

	protected void txInit() {
		if (this.em == null || !this.em.isOpen()) {
			this.em = EvaluacionSingleton.getInstance().createEntityManager();
		}
		// if (this.transaccion == null) {
		// this.transaccion = em.getTransaction();
		// }
		if (this.em.getTransaction() != null && !this.em.getTransaction().isActive()) {
			this.em.getTransaction().begin();
		}
	}

	public boolean isAutoCommit() {
		return autoCommit;
	}

	public void setAutoCommit(boolean autoCommit) {
		this.autoCommit = autoCommit;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<T> selectNativeQuery(String query) throws MiaExcepcion {
		final List<T> result;
		try {
			Query q = getEntityManager().createNativeQuery(query);
			result = q.getResultList();
			commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorActualizar");
		} finally {
			em.close();
		}
		return result;
	}
}
