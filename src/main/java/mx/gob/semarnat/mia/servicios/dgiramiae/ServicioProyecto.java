/**
 * 
 */
package mx.gob.semarnat.mia.servicios.dgiramiae;

import java.math.BigDecimal;

import javax.persistence.Query;

import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoPK;

/**
 * @author SyesSoftware
 *
 */
public class ServicioProyecto extends GenericDaoMia<Proyecto, ProyectoPK> {
	public static final short MIA_SERIAL_PROYECTO_INFO_ADICIONAL = 3;

	public Proyecto encontrarPorPK(String folioProyecto, short serialProyecto) throws MiaExcepcion {
		ProyectoPK pk = new ProyectoPK();
		pk.setFolioProyecto(folioProyecto);
		pk.setSerialProyecto(serialProyecto);
		//
		Proyecto p = null;
		try {
			p = this.findById(pk);
		} catch (MiaExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MiaExcepcion("ServicioProyecto.errorConsultar");
		}

		return p;
	}

	public Proyecto encontrarPorPKYBitacoraExiste(String folioProyecto, short serialProyecto) throws MiaExcepcion {
		Proyecto p = null;
		try {
			p = this.singleResultByNamedQuery("Proyecto.findByPKAndBitacoraExists", folioProyecto, serialProyecto);
		} catch (MiaExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MiaExcepcion("ServicioProyecto.errorConsultar");
		}

		return p;
	}

	public Proyecto crearProyectoInformacionAdicionalSerial3SinoExiste(String folio, String bitacora, String clave, Long lote, Short sector, Short subsector, String entidad, String municipo,
			BigDecimal inversion, Integer empleosP, Integer empleaosT, String estatus, Short tipo, Short estudioId, String competencia) throws MiaExcepcion {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO PROYECTO ( ");
		sb.append("FOLIO_PROYECTO,SERIAL_PROYECTO, ");
		sb.append("CLAVE_PROYECTO,PROY_LOTE, ");
		sb.append("NSEC,NSUB, ");
		sb.append("PROY_ENT_AFECTADO,PROY_MUN_AFECTADO, ");
		sb.append("PROY_INVERSION_REQUERIDA,PROY_EMPLEOS_PERMANENTES, ");
		sb.append("PROY_EMPLEOS_TEMPORALES,ESTATUS_PROYECTO, ");
		sb.append("NTIPO,ESTUDIO_RIESGO_ID, ");
		sb.append("PROY_COMPETENCIA) ");
		//
		sb.append("SELECT '" + folio + "', " + MIA_SERIAL_PROYECTO_INFO_ADICIONAL + ", ");
		sb.append(clave != null ? "'" + clave + "'," : "" + null + ",");
		sb.append("" + lote + ", ");
		sb.append("" + sector + "," + subsector + ", ");
		sb.append(entidad != null ? "'" + entidad + "'," : "" + null + ",");
		sb.append(municipo != null ? "'" + municipo + "'," : "" + null + ",");
		sb.append(inversion != null ? "" + inversion.toString() + "," : "" + null + ",");
		sb.append("" + empleosP + ",");
		sb.append("" + empleaosT + ",");
		sb.append(estatus != null ? "'" + estatus + "'," : "" + null + ",");
		sb.append("" + tipo + "," + estudioId + ", ");
		sb.append(competencia != null ? "'" + competencia + "'" : "" + null + "");
		//
		sb.append("FROM DUAL WHERE NOT EXISTS ");
		sb.append("(SELECT * FROM PROYECTO WHERE FOLIO_PROYECTO = '" + folio + "' ");
		sb.append("AND SERIAL_PROYECTO = " + MIA_SERIAL_PROYECTO_INFO_ADICIONAL + ") ");
		sb.append("AND EXISTS ");
		sb.append("(SELECT * FROM PROYECTO WHERE FOLIO_PROYECTO = '" + folio + "' ");
		sb.append("AND SERIAL_PROYECTO = 1 AND BITACORA_PROYECTO = '" + bitacora + "') ");
		String query = sb.toString();
		System.out.println(query);
		//
		Proyecto proyecto;
		try {
			int rs = updateNativeQuery(query);
			if (rs > 0) {
				System.out.println("Se creo el proyecto");
			} else {
				System.out.println("El proyecto ya existe");
			}
			proyecto = encontrarPorPK(folio, MIA_SERIAL_PROYECTO_INFO_ADICIONAL);
		} catch (MiaExcepcion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new MiaExcepcion("ServicioProyecto.errorConsultar");
		}
		return proyecto;
	}
}
