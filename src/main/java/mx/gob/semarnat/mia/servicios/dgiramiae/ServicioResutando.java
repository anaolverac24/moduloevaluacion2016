package mx.gob.semarnat.mia.servicios.dgiramiae;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.Resultando;

public class ServicioResutando extends GenericDaoMia<Resultando, Long> {
	private final int CONSULTA_RESULTANDO_UNO = 1;
	private final int CONSULTA_RESULTANDO_DOS_REL_ING_PRO = 2;
	private final int CONSULTA_RESULTANDO_TRE_REL_PUB_GAC = 3;
	private final int CONSULTA_RESULTANDO_EXISTS = 4;
	private final int CONSULTA_RESULTANDO_DELETE = 5;
	private final int CONSULTA_RESULTANDO_BITACORA = 6;

	public List<Resultando> encontrarTodos() throws MiaExcepcion {
		List<Resultando> l = this.findAll();
		if (l == null) {
			l = new ArrayList<>();
		}
		return l;
	}

	public List<Resultando> encontrarPorBitacora(String bitacoraProyecto) throws MiaExcepcion {
		List<Resultando> l;
		try {
			Query q = getEntityManager().createNativeQuery(getSQL(null, bitacoraProyecto, CONSULTA_RESULTANDO_BITACORA), Resultando.class);
			l = q.getResultList();
			getEm().getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorGuardar");
		} finally {
			if (getEm() != null && getEm().isOpen()) {
				getEm().close();
				setEm(null);
			}
		}
		if (l == null) {
			l = new ArrayList<>();
		}
		return l;
	}

	public boolean sustituirResultandosPorBitacora(String folioProyecto, String bitacoraProyecto) throws ParseException, MiaExcepcion {
		boolean exito = false;
		try {
			Query q;
			q = getEntityManager().createNativeQuery(getSQL(folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_DELETE));
			q.executeUpdate();
			getEm().getTransaction().commit();
			//
			// Generar nuevamente los registros
			boolean a = generarResultandosPorBitacora(folioProyecto, bitacoraProyecto);
			exito = true;
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorGuardar");
		} finally {
			if (getEm() != null && getEm().isOpen()) {
				getEm().close();
				setEm(null);
			}
		}
		return exito;
	}

	public boolean generarResultandosPorBitacora(String folioProyecto, String bitacoraProyecto) throws ParseException, MiaExcepcion {
		// List<Long> idsResultandosRepetidos = new ArrayList<>();
		boolean repetidos = false;
		try {
			Query q;
			q = getEntityManager().createNativeQuery(getSQL(folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_EXISTS));
			BigDecimal c = (BigDecimal) q.getSingleResult();
			int existe = 0;
			if (c != null) {
				existe = c.intValue();
			}
			System.out.println("Existe: " + existe);
			getEm().getTransaction().commit();
			if (existe == 0) {
				// No existen registros en la bitacora
				// Consulta Uno
				q = getEntityManager().createNativeQuery(getSQL(folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_UNO));
				List<Object[]> l1 = q.getResultList();
				getEm().getTransaction().commit();
				for (Object[] o : l1) {
					Resultando r = null;
					// String folioDoc = (String) o[0];
					// r = buscarResultando(folioDoc, bitacoraProyecto, CONSULTA_RESULTANDO_UNO);
					// if (r == null) {
					r = crearResultando(o, folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_UNO);
					r = getEntityManager().merge(r);
					System.out.println("Registro guardado");
					getEm().getTransaction().commit();
					// } else {
					// idsResultandosRepetidos.add(r.getResultandoId());
					// }
				}
				// Consulta Dos
				q = getEntityManager().createNativeQuery(getSQL(folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_DOS_REL_ING_PRO));
				List<Object[]> l2 = q.getResultList();
				getEm().getTransaction().commit();
				for (Object[] o : l2) {
					Resultando r = null;
					r = crearResultando(o, folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_DOS_REL_ING_PRO);
					r = getEntityManager().merge(r);
					System.out.println("Registro guardado C2");
					getEm().getTransaction().commit();
				}
				// Consulta Tres
				q = getEntityManager().createNativeQuery(getSQL(folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_TRE_REL_PUB_GAC));
				List<Object[]> l3 = q.getResultList();
				getEm().getTransaction().commit();
				for (Object[] o : l3) {
					Resultando r = null;
					r = crearResultando(o, folioProyecto, bitacoraProyecto, CONSULTA_RESULTANDO_TRE_REL_PUB_GAC);
					r = getEntityManager().merge(r);
					System.out.println("Registro guardado C3");
					getEm().getTransaction().commit();

				}
			} else {
				repetidos = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			rollback();
			throw new MiaExcepcion("GenericDAO.errorGuardar");
		} finally {
			if (getEm() != null && getEm().isOpen()) {
				getEm().close();
				setEm(null);
			}
		}
		return repetidos;
	}

	private String getSQL(String folioProyecto, String bitacoraProyecto, int consulta) {
		StringBuilder sb = new StringBuilder();
		switch (consulta) {
		case CONSULTA_RESULTANDO_UNO:
			sb.append("SELECT D.IDDOCUMENTO,D.NUMREFERENCIA,C.DESCRIPCION AS CATEGORIA_DOC,D.FECHAELABORACION, ");
			sb.append("D.FECHAREGISTRO,D.NOMBREREMITENTE,D.DEPENDENCIAREMITENTE,D.OBSERVACIONES ");
			sb.append("FROM CORRESPONDENCIA.TB_DOCUMENTO D ");
			sb.append("INNER JOIN CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO C ON D.IDCATEGORIA = C.IDCATEGORIA ");
			sb.append("INNER JOIN CORRESPONDENCIA.TB_RELDOCTRAMITE R ON D.IDDOCUMENTO = R.IDDOCUMENTO ");
			sb.append("WHERE (R.BITACORAPROYECTO = '" + folioProyecto + "' OR R.BITACORAPROYECTO = '" + bitacoraProyecto + "') ");
			sb.append("AND C.IDCATEGORIA NOT IN ('4619', '4609', '4753','172', '4562') ");
			sb.append("ORDER BY D.FECHAELABORACION ");
			break;
		case CONSULTA_RESULTANDO_DOS_REL_ING_PRO:
			sb.append("SELECT DISTINCT C.CVE AS IDDOCUMENTO,C.NUMERO_BITA AS REFERENCIA,NULL AS CATEGORIA_DOC,B.BITA_FOFI_RECEPCION AS FECHAELABORACION, ");
			sb.append("NULL AS FECHAREGISTRO,C.NOMBRE AS NOMBREREMITENTE,T.NOMBRE_CORTO AS DEPENDENCIAREMITENTE,CONCAT(TR.COFEMER_CLAVE, CONCAT(' ',TR.DESCRIPCION)) AS OBSERVACIONES ");
			sb.append("FROM BITACORA_TEMATICA.PROYECTO C ");
			sb.append("INNER JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA ");
			sb.append("INNER JOIN CATALOGO_TRAMITES.MODALIDAD T ON B.BITA_ID_TRAMITE = T.ID_TRAMITE ");
			sb.append("INNER JOIN CATALOGO_TRAMITES.TRAMITE TR ON TR.ID_TRAMITE = B.BITA_ID_TRAMITE ");
			sb.append("WHERE C.NUMERO_BITA = '" + bitacoraProyecto + "'");
			break;
		case CONSULTA_RESULTANDO_TRE_REL_PUB_GAC:
			sb.append("SELECT NULL AS IDDOCUMENTO, G.SEPARATA AS REFERENCIA, NULL AS CATEGORIA_DOC, NULL AS FECHAELABORACION, ");
			sb.append("G.F_PUBLICACION AS FECHAREGISTRO,NULL AS NOMBREREMITENTE, NULL AS DEPENDENCIAREMITENTE,G.DESCRIPCION AS OBSERVACIONES ");
			sb.append("FROM DGIRA_MIAE2.GACETAS G ");
			sb.append("INNER JOIN DGIRA_MIAE2.GACETAS_PROYECTOS P ON G.ID_GACETA = P.ID_GACETA ");
			sb.append("WHERE ( ");
			sb.append("P.CLAVE_PROYECTO = ( ");
			sb.append("SELECT COALESCE(T.CVE,'') AS CLAVE_PROYECTO_ASOCIADA ");
			sb.append("FROM BITACORA_TEMATICA.PROYECTO T ");
			sb.append("WHERE T.NUMERO_BITA LIKE '" + bitacoraProyecto + "' ");
			sb.append(") OR P.CLAVE_PROYECTO = '" + bitacoraProyecto + "')");

			break;
		case CONSULTA_RESULTANDO_EXISTS:
			sb.append("SELECT COUNT(*) ");
			sb.append("FROM DUAL ");
			sb.append("WHERE EXISTS (SELECT R.* FROM RESULTANDOS R WHERE BITACORA_PROYECTO = '" + bitacoraProyecto + "')");
			break;
		case CONSULTA_RESULTANDO_DELETE:
			sb.append("DELETE FROM RESULTANDOS ");
			sb.append("WHERE BITACORA_PROYECTO = '" + bitacoraProyecto + "'");
			break;
		case CONSULTA_RESULTANDO_BITACORA:
			sb.append("SELECT R.* FROM RESULTANDOS R ");
			sb.append("WHERE R.BITACORA_PROYECTO = '").append(bitacoraProyecto).append("' ORDER BY R.FECHA_ELABORACION_DOCUMENTO ASC");
			break;
		}
		//
		return sb.toString();
	}

	private Resultando crearResultando(Object[] o, String folioProyecto, String bitacoraProyecto, int consulta) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		String folioDoc = (String) o[0];
		String referencia = (String) o[1];
		String categoriaDoc = (String) o[2];
		Timestamp tsFechaElaboracion = (Timestamp) o[3];
		Timestamp tsFechaRegistro = (Timestamp) o[4];
		String nombreRemitente = (String) o[5];
		String dependenciaRemitente = (String) o[6];
		String observaciones = (String) o[7];
		//
		Date fechaElaboracion = tsFechaElaboracion != null ? new Date(tsFechaElaboracion.getTime()) : null;
		Date fechaRegistro = tsFechaRegistro != null ? new Date(tsFechaRegistro.getTime()) : null;
		String ftFechaElaboracion = fechaElaboracion != null ? sdf.format(fechaElaboracion) : null;
		String ftFechaRegistro = fechaRegistro != null ? sdf.format(fechaRegistro) : null;
		//
		System.out.println("fol " + folioDoc);
		System.out.println("ref " + referencia);
		System.out.println("cat " + categoriaDoc);
		System.out.println("fechEla " + fechaElaboracion + ", format: " + ftFechaElaboracion);
		System.out.println("fechReg " + fechaRegistro + ", format: " + ftFechaRegistro);
		System.out.println("nomRem " + nombreRemitente);
		System.out.println("depRem " + dependenciaRemitente);
		System.out.println("observ " + observaciones);
		//
		Resultando r = new Resultando();
		r.setAltaManual(false);
		r.setBitacoraProy(bitacoraProyecto);
		r.setCategoriaDoc(categoriaDoc);
		r.setDependenciaRemi(dependenciaRemitente);
		r.setFechaElaboracionDoc(fechaElaboracion);
		r.setFechaRegistro(fechaRegistro);
		r.setFolioDoc(folioDoc);
		r.setFolioProy(folioProyecto);
		r.setNombreRemi(nombreRemitente);
		r.setObservaciones(observaciones);
		r.setReferencia(referencia);
		r.setRedaccion(null);
		//
		switch (consulta) {
		case CONSULTA_RESULTANDO_UNO:
			r.setRelIngresoProy(null);
			r.setRelPublicGaceta(null);
			break;
		case CONSULTA_RESULTANDO_DOS_REL_ING_PRO:
			r.setRelIngresoProy(true);
			r.setRelPublicGaceta(null);
			break;
		case CONSULTA_RESULTANDO_TRE_REL_PUB_GAC:
			r.setRelIngresoProy(null);
			r.setRelPublicGaceta(true);
			break;
		}
		//
		return r;
	}

	private Resultando buscarResultando(String folioDoc, String bitacoraProyecto, int consulta) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT R.* FROM RESULTANDOS R ");
		sb.append("WHERE R.FOLIO_DOCUMENTO " + (folioDoc != null ? " = '" + folioDoc + "' " : " IS NULL ") + " AND R.ALTA_MANUAL = '0' AND R.BITACORA_PROYECTO = '" + bitacoraProyecto + "' ");
		switch (consulta) {
		case CONSULTA_RESULTANDO_UNO:
			sb.append("AND R.RELATIVO_INGRESO_POYECTO IS NULL AND R.RELATIVO_PUBLICACION_GACETA IS NULL");
			break;
		case CONSULTA_RESULTANDO_DOS_REL_ING_PRO:
			sb.append("AND R.RELATIVO_INGRESO_POYECTO = '1' AND R.RELATIVO_PUBLICACION_GACETA IS NULL");
			break;
		case CONSULTA_RESULTANDO_TRE_REL_PUB_GAC:
			break;
		}
		System.out.println(sb.toString());
		Query q = getEntityManager().createNativeQuery(sb.toString(), Resultando.class);
		List<Resultando> l = q.getResultList();
		getEm().getTransaction().commit();
		if (l != null && l.size() == 1) {
			return l.get(0);
		} else {
			return null;
		}
	}
}
