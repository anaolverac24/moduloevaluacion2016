package mx.gob.semarnat.mia.servicios.evaluacion;

import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.ProyectoOfiGobierno;

public class ServicioOficioNotiGobiernos extends GenericDaoMia<ProyectoOfiGobierno, Long> {
	public boolean guarda(ProyectoOfiGobierno gobierno) throws MiaExcepcion {
		String q = "INSERT INTO PROYECTO_OFI_GOBIERNO ( " +
				" BITACORA_PROYECTO, DEPENDENCIA_ID) " +
				"VALUES ('" + gobierno.getBitacoraProyecto() + "'," + gobierno.getDepenciaId()+ ")";
			System.out.println(q);
			
			try {
				int rs = updateNativeQuery(q);
				return rs > 0;
			} catch (MiaExcepcion e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new MiaExcepcion("ServicioProyecto.errorConsultar");
			}
	}
	
	public boolean elemina(ProyectoOfiGobierno gobierno) throws MiaExcepcion {
		String q = "DELETE FROM PROYECTO_OFI_GOBIERNO " +
				" WHERE BITACORA_PROYECTO = '" + gobierno.getBitacoraProyecto() + "' AND DEPENDENCIA_ID = " + gobierno.getDepenciaId();
			System.out.println(q);
			
			try {
				int rs = updateNativeQuery(q);
				return rs > 0;
			} catch (MiaExcepcion e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new MiaExcepcion("ServicioProyecto.errorConsultar");
			}
	}
}
