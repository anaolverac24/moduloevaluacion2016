package mx.gob.semarnat.mia.servicios;

import mx.gob.semarnat.model.seguridad.TokenGO;

public class ServicioTokenGO extends GenericDaoMia<TokenGO, Integer>{
	
	public TokenGO getToken(int idUsiario)throws MiaExcepcion{
		try {
			
			return (TokenGO) this.findByNamedQuery("TokenGO.findByIdUsiario", idUsiario).get(0);
		} catch (MiaExcepcion e) {
			e.printStackTrace();
		}
		return null;
	}
}
