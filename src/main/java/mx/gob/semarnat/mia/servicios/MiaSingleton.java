package mx.gob.semarnat.mia.servicios;

import javax.persistence.EntityManagerFactory;

import mx.gob.semarnat.dao.PersistenceManager;

public class MiaSingleton {

	private static String UNIDAD_DE_PERSISTENCIA = "DGIRA_MIAE";
	private static EntityManagerFactory factory = null;

	public static EntityManagerFactory getInstance() {
		if (factory == null) {
			factory = PersistenceManager.getEmfMIAE();
		}
		return factory;
	}

	public static void close() {
		if (factory != null) {
			factory.close();
		}
		factory = null;
	}
}
