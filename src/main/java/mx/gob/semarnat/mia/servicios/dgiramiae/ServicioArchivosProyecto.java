package mx.gob.semarnat.mia.servicios.dgiramiae;

import java.util.List;

import javax.persistence.Query;

import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;

public class ServicioArchivosProyecto extends GenericDaoMia<ArchivosProyecto, Short> {

	public List<ArchivosProyecto> encontrarPorFolioPorSerialPorCapSubSecApa(String folio, short serial, short cap, short sub, short sec, short apa) throws MiaExcepcion {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT AP.* FROM ARCHIVOS_PROYECTO AP ");
		sb.append("WHERE AP.CAPITULO_ID = " + cap + " AND AP.SUBCAPITULO_ID = " + sub + " ");
		sb.append("AND AP.SECCION_ID = " + sec + " AND AP.APARTADO_ID = " + apa + " ");
		sb.append("AND AP.FOLIO_SERIAL = '" + folio + "' AND AP.SERIAL_PROYECTO = " + serial + " ");

		List<ArchivosProyecto> l = null;
		try {
			Query q = this.getEntityManager().createNativeQuery(sb.toString(), ArchivosProyecto.class);
			l = q.getResultList();
		} catch (Exception e) {
			throw new MiaExcepcion("GenericDAO.errorConsultar");
		}
		return l;

	}
}
