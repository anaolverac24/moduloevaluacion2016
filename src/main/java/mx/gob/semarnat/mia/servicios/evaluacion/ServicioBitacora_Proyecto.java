package mx.gob.semarnat.mia.servicios.evaluacion;

import java.util.List;

import javax.persistence.Query;

import mx.gob.semarnat.mia.servicios.GenericDaoEvaluacion;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.bitacora.Proyecto;

public class ServicioBitacora_Proyecto extends GenericDaoEvaluacion<Proyecto, String> {

	public Object[] encontrarPorBitacora(String bitacora) throws MiaExcepcion {
		Object[] o = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT C.CVE,C.NOMBRE AS PROYECTO, ");
		sb.append("CASE WHEN P.DFI_RAZON_SOCIAL IS NULL ");
		sb.append("THEN P.DFI_NOMBRE || ' ' || P.DFI_APELLIDO_PATERNO || ' ' || P.DFI_APELLIDO_MATERNO ");
		sb.append("ELSE P.DFI_RAZON_SOCIAL END AS promovente, ");
		sb.append("S.SUBSECTOR, B.BITA_DIAS_PROCESO, B.BITA_DIAS_TRAMITE, SIT.DESCRIPCION, TO_CHAR(B.BITA_FOFI_RECEPCION,'dd/MM/yyyy') ");
		sb.append("FROM BITACORA_TEMATICA.PROYECTO C ");
		sb.append("INNER JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA ");
		sb.append("INNER JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM ");
		sb.append("INNER JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY ");
		sb.append("INNER JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA ");
		sb.append("INNER JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO ");
		sb.append("INNER JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA ");
		sb.append("INNER JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB ");
		sb.append("INNER JOIN CATALOGO_FLUJOS.TBSITUACION SIT ON SIT.IDSITUACION = B.BITA_SITUACION ");
		sb.append("WHERE C.NUMERO_BITA = '" + bitacora + "'");
		System.out.println(sb.toString());
		try{
			Query q = this.getEntityManager().createNativeQuery(sb.toString());
			List<Object[]> l = q.getResultList();
			if (l != null && !l.isEmpty()) {
				o = l.get(0);
			}
			this.getEm().getTransaction().commit();
		}catch(Exception e){
			this.getEm().getTransaction().rollback();
			e.printStackTrace();
			throw new MiaExcepcion("Error al consultar el proyecto");
		}finally{
			this.getEm().close();
			this.setEm(null);
		}
		
		return o;

	}
	
	public Object[] encontrarPorBitacoraAlter(String bitacora) throws MiaExcepcion {
		Object[] o = null;
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT  ");
		sb.append("CASE WHEN X.BITA_PROY IS NULL THEN '-' ELSE X.BITA_PROY END AS CVE, ");
		sb.append("CASE WHEN X.FRACCION_ARTICULO IS NULL THEN '-' ");
		sb.append("	 ELSE X.FRACCION_ARTICULO ");
		sb.append("END AS NOMBRE, ");
		sb.append("CASE WHEN (D.DFI_RAZON_SOCIAL IS NULL) ");
		sb.append("	 THEN (D.DFI_NOMBRE||' '||D.DFI_APELLIDO_PATERNO||' '||D.DFI_APELLIDO_MATERNO) ");
		sb.append("	 ELSE  D.DFI_RAZON_SOCIAL ");
		sb.append("END AS PROMOVENTE, ");
		sb.append("'0' as SUBSECTOR, ");
		sb.append("A.BITA_DIAS_PROCESO, ");
		sb.append("A.BITA_DIAS_TRAMITE, ");
		sb.append("E.DESCRIPCION,  ");
		sb.append("TO_CHAR(A.BITA_FOFI_RECEPCION,'dd/MM/yyyy')  ");
		sb.append("FROM BITACORA.BITACORA A ");
		sb.append("JOIN PADRON.DATOS_FISCALES D ON D.PROM_ID = A.BITA_ID_PROM ");
		sb.append("JOIN CATALOGO_FLUJOS.TBSITUACION E ON E.IDSITUACION = A.BITA_SITUACION ");
		sb.append("LEFT JOIN DGIRA_MIAE2.EXENCION X ON X.NUMERO_BITACORA = A.BITA_NUMERO ");
		sb.append("WHERE A.BITA_ENTIDAD_ORIGEN = '09' ");
		sb.append("AND   D.ID_TIPO_DIRECCION = '03' ");
		sb.append("AND   A.BITA_SITUACION IN ('AT0027','I00010','AT0022','CIS106', '200501','AT0003', 'ATIF01', 'CIS301', 'CIS104', 'CIS302', 'CIS201', 'B00009', '040201','IRA020','CIS303') ");
		sb.append("AND   A.BITA_FOFI_RECEPCION >= TO_DATE('01/01/2015','DD/MM/YYYY') ");
		sb.append("AND   A.BITA_TIPOTRAM IN ('DC') ");
		sb.append("AND   A.BITA_NUMERO = '"+ bitacora +"'");
		System.out.println(sb.toString());
		try{
			Query q = this.getEntityManager().createNativeQuery(sb.toString());
			List<Object[]> l = q.getResultList();
			if (l != null && !l.isEmpty()) {
				o = l.get(0);
			}
			this.getEm().getTransaction().commit();
		}catch(Exception e){
			this.getEm().getTransaction().rollback();
			e.printStackTrace();
			throw new MiaExcepcion("Error al consultar el proyecto");
		}finally{
			this.getEm().close();
			this.setEm(null);
		}
		
		return o;

	}
	
}
