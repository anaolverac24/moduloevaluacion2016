package mx.gob.semarnat.mia.servicios;

import java.util.ResourceBundle;

public class MiaMensajes {

	private final static ResourceBundle rb = ResourceBundle.getBundle("mx.gob.semarnat.mia.servicios.ln.general");

	public static String getMessageByKey(String key) {
		return rb.getString(key);
	}
}
