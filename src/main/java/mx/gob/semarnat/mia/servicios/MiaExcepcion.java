package mx.gob.semarnat.mia.servicios;

@SuppressWarnings("serial")
public class MiaExcepcion extends Exception {

	private String key;
	private boolean b;

	public MiaExcepcion(String key, boolean b) {
		this.key = key;
		this.b = b;
	}

	public MiaExcepcion(String key) {
		this.key = key;
	}

	public String getMensajeInterfaz() {
		String mensaje = "";
		if (b) {
			mensaje = key;
		} else {
			mensaje = MiaMensajes.getMessageByKey(key);
		}

		return mensaje;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isB() {
		return b;
	}

	public void setB(boolean b) {
		this.b = b;
	}
}
