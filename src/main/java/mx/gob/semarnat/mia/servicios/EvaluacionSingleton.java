package mx.gob.semarnat.mia.servicios;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.gob.semarnat.dao.PersistenceManager;
import mx.gob.semarnat.dao.PoolEntityManagers;

public class EvaluacionSingleton {
	private static String UNIDAD_DE_PERSISTENCIA = "moduloEvaluacion";
	private static EntityManagerFactory factory = null;

	public static EntityManagerFactory getInstance() {
		if (factory == null) {
			factory = PersistenceManager.getEmfBitacora();
		}
		return factory;
	}

	public static void close() {
		if (factory != null) {
			factory.close();
		}
		factory = null;
	}
}
