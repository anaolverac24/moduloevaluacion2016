package mx.gob.semarnat.mia.servicios;

import java.io.Serializable;
import java.util.List;

public interface GenericDaoMiaInterface<T, ID extends Serializable> {
	Class<T> getEntityClass();

	T findById(final ID id) throws MiaExcepcion;

	List<T> findAll() throws MiaExcepcion;

	List<T> findByNamedQuery(final String queryName, Object... params) throws MiaExcepcion;
	
	T singleResultByNamedQuery(final String queryName, Object... params) throws MiaExcepcion;
	
	int updateNativeQuery(String query) throws MiaExcepcion;

	T save(final T entity) throws MiaExcepcion;

	void delete(final ID id) throws MiaExcepcion;
}
