/**
 * @author eescalona
 */

package mx.gob.semarnat.utils;

import java.util.ResourceBundle;
import static mx.gob.semarnat.utils.GenericConstants.ARCH_CONF_GENERAL;

public class Utils {
    private static final ResourceBundle rbConf = ResourceBundle.getBundle(ARCH_CONF_GENERAL);
    
    public static final String rutaGeneradorDoctos = Utils.obtenerConstante(GenericConstants.CONST_SERVIDOR_ADMIN) +
                        Utils.obtenerConstante(GenericConstants.CONST_PETICION_GENERADOR_DOCTOS);
    
    public static final String rutaServletInicio = Utils.obtenerConstante(GenericConstants.CONST_SERVIDOR_ADMIN) +
                        Utils.obtenerConstante(GenericConstants.CONST_PETICION_SERVLET_INICIO);
    
    public static final String rutaReportesServlet = Utils.obtenerConstante(GenericConstants.CONST_SERVIDOR_ADMIN) +
                        Utils.obtenerConstante(GenericConstants.CONST_PETICION_SERVLET);
    
    public static final String wsRecuperacionArchivos = Utils.obtenerConstante(GenericConstants.CONST_PETICION_WEBSERVICE);
    
    /***
     * ruta al hostserver donde esta publicado el ModuloEvaluacion
     */
    public static final String ruta_servidor_hostserver = Utils.obtenerConstante(GenericConstants.CONST_SERVIDOR_HOSTNAME);    
    
//    /***
//     * ruta del servidor donde esta publicada la aplicacion del DGIRA_ADMIN
//     */
//    public static final String ruta_servidor_hostadmin = Utils.obtenerConstante(GenericConstants.CONST_SERVIDOR_ADMIN);
    
    /***
     * ruta ModuloEvaluacion/faces/Restringido/modarchivos/downloadPDF.xhtml
     */
    public static final String ruta_archivo_verpdf = Utils.obtenerConstante(GenericConstants.CONST_PETICION_VER_PDF);
    
    /***
     * url xxxx  que se va a incrustrar en el codigo QR
     */
    public static final String ruta_liga_codigoqr = Utils.obtenerConstante(GenericConstants.CONST_RUTA_LIGA_CODIGOQR);
    
    
    public static String obtenerConstante(String constante) {
        String valor; 
        
        valor = rbConf.getString(constante);
        
        return (valor != null ? valor : "");
    }
    
    public static String obtenerLogConstructor(String pre) {
        String cadena;
        cadena = pre;
        cadena += " -- Constructor de vista";
        //cadena += " --> ";
        
        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:s:S");
        //cadena += sdf.format(new Date());
        
        return cadena;
    }
    
}
