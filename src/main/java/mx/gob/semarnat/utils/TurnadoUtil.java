package mx.gob.semarnat.utils;

//<editor-fold defaultstate="collapsed" desc="Imports">
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.dao.CadenaFirmaDao;
import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.dao.OficiosDao;
import mx.gob.semarnat.dao.procedures.SegundasVias;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.CadenaFirma;
import mx.gob.semarnat.model.bitacora.CadenaFirmaLog;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.bitacora.HistorialPK;
import mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoPK;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.oficios.TipoDocumento;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbusuario;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.secure.AppSession;
import net.firel.BcAgregaCadenaRespuesta;
import net.firel.DetallesFirmado;
import net.firel.Firmas;
import org.apache.tomcat.util.codec.binary.Base64;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
//</editor-fold>

/**
 *
 * @author Ana Olvera
 */
@ManagedBean
@ViewScoped
public class TurnadoUtil implements Serializable {

    FacesContext fContext;
    private OficiosDao dao = new OficiosDao();
    private DgiraMiaeDaoGeneral daoFlujo = new DgiraMiaeDaoGeneral();
    private BitacoraDao daoSituaciones = new BitacoraDao();
    //Flujo e historial del documento
    private DocumentoDgiraFlujo situacion = new DocumentoDgiraFlujo();
    private List<Object[]> maxDocsDgiraFlujoHist = new ArrayList<Object[]>();
    private DocumentoDgiraFlujoHist docsDgiraFlujoHist;
    private DocumentoDgiraFlujoHist docsDgiraFlujoHistEdit;
    private DocumentoRespuestaDgira oficioBd;
    private Integer idAux = 0;
    private short maxIdHisto = 0;
    //Banderas para visualizar botones
    private boolean panel = false;
    private boolean panelDocumento = false;
    private boolean panelMensaje = false;
    private boolean btnTurnado = true;
    private TipoDocumento tipoOficio;
    private StreamedContent file;
    //============ Variables para obtener los datos de firma =======================
    private String folio;
    private String claveProyecto;
    private String bitacoraProyecto;
    private String titulo;
    //============ Información del rol seleccionado =======================
    private UsuarioRol ur = new UsuarioRol();
    //============ Ejecución de segundas vias Sinatec =======================
    private SegundasVias segVias = new SegundasVias();
    public TurnadoUtil() {
        this.tipoOficio = new TipoDocumento();
        this.folio = "";
        this.claveProyecto = "";
        this.titulo = "";
    }

    @PostConstruct
    public void init() {
        initContext();
        this.oficioBd = new DocumentoRespuestaDgira();
        this.bitacoraProyecto = fContext.getExternalContext().getSessionMap().get("bitacoraProy").toString();
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos para la tabla DOCUMENTO_DGIRA_FLUJO_HIST">
    public boolean insertarRegistroHist(DocumentoRespuestaDgira doc, UsuarioRol usuario, Historial areaRecibe) {
        boolean respuesta = false;
        maxDocsDgiraFlujoHist = daoFlujo.maxDocsDgiraFlujoHist(doc.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), doc.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                doc.getDocumentoRespuestaDgiraPK().getIdTramite(), doc.getDocumentoRespuestaDgiraPK().getTipoDocId(), doc.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(), doc.getDocumentoRespuestaDgiraPK().getDocumentoId());
        for (Object o : maxDocsDgiraFlujoHist) {
            idAux = Integer.parseInt(o.toString());
            maxIdHisto = idAux.shortValue();
        }
        maxIdHisto = (short) (idAux + 1);
        docsDgiraFlujoHist = new DocumentoDgiraFlujoHist(doc.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), doc.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                doc.getDocumentoRespuestaDgiraPK().getIdTramite(), doc.getDocumentoRespuestaDgiraPK().getTipoDocId(), doc.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(),
                doc.getDocumentoRespuestaDgiraPK().getDocumentoId(), maxIdHisto);
        docsDgiraFlujoHist.setIdAreaEnvioHist(usuario.getUsuarioId().getIdarea().trim());
        docsDgiraFlujoHist.setIdUsuarioEnvioHist(usuario.getUsuarioId().getIdusuario());
        docsDgiraFlujoHist.setDocumentoDgiraFeEnvHist(new Date());
        docsDgiraFlujoHist.setIdAreaRecibeHist(areaRecibe.getHistoAreaRecibe());
        docsDgiraFlujoHist.setIdUsuarioRecibeHist(areaRecibe.getHistoIdUsuarioRecibe());
        docsDgiraFlujoHist.setDocumentoDgiraFeRecHist(null);
        docsDgiraFlujoHist.setEstatusDocumentoIdHist(new CatEstatusDocumento(new Short("3")));
        docsDgiraFlujoHist.setDocumentoDgiraObservHist("");
        try {
            daoFlujo.agrega(docsDgiraFlujoHist);
        } catch (Exception ex) {
            System.out.println("Ocurrió un error al agregar el historial del documento");
        }
        return respuesta;
    }

    public boolean actualizaHistorial(DocumentoRespuestaDgira doc, UsuarioRol usuario) {
        boolean respuesta = false;

        //-------------------actualizacion en tabla docsDgiraFlujoHist, si existe el registro se actualiza la parte que recibe el documento---------------
        maxDocsDgiraFlujoHist = daoFlujo.maxDocsDgiraFlujoHist(doc.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), doc.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                doc.getDocumentoRespuestaDgiraPK().getIdTramite(), doc.getDocumentoRespuestaDgiraPK().getTipoDocId(), doc.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(), doc.getDocumentoRespuestaDgiraPK().getDocumentoId());
        for (Object o : maxDocsDgiraFlujoHist) {
            idAux = Integer.parseInt(o.toString());
            maxIdHisto = idAux.shortValue();
        }

        docsDgiraFlujoHistEdit = daoFlujo.docsFlujoDgiraHisto(doc.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), doc.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                doc.getDocumentoRespuestaDgiraPK().getIdTramite(), doc.getDocumentoRespuestaDgiraPK().getTipoDocId(), doc.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(),
                doc.getDocumentoRespuestaDgiraPK().getDocumentoId(), maxIdHisto);

        if (docsDgiraFlujoHistEdit != null && maxIdHisto > 0) {
            docsDgiraFlujoHistEdit.setIdUsuarioRecibeHist(usuario.getUsuarioId().getIdusuario());
            docsDgiraFlujoHistEdit.setDocumentoDgiraFeRecHist(new Date());
            daoFlujo.modifica(docsDgiraFlujoHistEdit);
            respuesta = true;
        }
        return respuesta;
    }
//</editor-fold>

    public void prepararTurnado(String nombreTab) {
        initContext();
        ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
        switch (nombreTab) {
            case "resolutivo":
                cargarOficioResolutivo();
                break;
            case "resolutivoExencion":
                cargarOficioResolutivoExcencion();                
                break;
            case "informacionAdicional":
                cargaOficio((short)7);               
                break;
            case "prevencionExcencion":
                cargaOficio((short)3);
                break;
            case "prevencion":
                cargaOficioPrevencion();
                break;
            default:
                System.out.println("No es una opción valida");
                break;
        }
        
        try {
            File oficio = new File(this.oficioBd.getDocumentoUrl());
            System.out.println(oficio.exists());
            if (!oficio.exists()) {
                this.oficioBd.setDocumentoUrl("../../../ModuloEvaluacion/preview?tipo=oficio&bitaProyecto=" + bitacoraProyecto 
                        +"&idDocumento=" + this.oficioBd.getDocumentoRespuestaDgiraPK().getDocumentoId()
                        + "&tipoOficio=" + this.oficioBd.getDocumentoRespuestaDgiraPK().getTipoDocId());
            }
        } catch (Exception e) {
            System.out.println("No se a cerrado el borrador de oficio");
        }
        
        if (this.oficioBd.getDocumentoRespuestaDgiraPK() == null) {
            this.panelDocumento = false;
            this.panelMensaje = true;
        } else {
            if (consultarSituacion(ur, this.oficioBd)) {
                this.btnTurnado = true;
            } else {
                this.btnTurnado = false;
            }
            this.panelDocumento = true;
            this.panelMensaje = false;
            fContext.getExternalContext().getSessionMap().put("documentoRespuesta", oficioBd);
            fContext.getExternalContext().getSessionMap().put("tipoDocumento", tipoOficio);
        }
        this.panel = true;
    }

    //<editor-fold defaultstate="collapsed" desc="Método para determinar si el documento fue turnado a la persona logueada">
    public boolean consultarSituacion(UsuarioRol rol, DocumentoRespuestaDgira doc) {
        boolean resp = false;
        switch (rol.getRolId().getNombreCorto()) {
            case "eva":
                System.out.println("El usuario logueado es un evaluador/área técnica");
                this.situacion = dao.recuperaSituacion(doc);
                if (this.situacion.getDocumentoDgiraFlujoPK() == null || (this.situacion.getEstatusDocumentoId().getEstatusDocumentoId() == 4 && this.situacion.getRolId().getIdRol() == rol.getRolId().getIdRol())) {
                    resp = true;
                }
                break;
            case "sd":
                System.out.println("El usuario logueado es un subdirector/área técnica");
                this.situacion = dao.recuperaSituacion(doc);
                if (doc.getDocumentoRespuestaDgiraPK().getTipoDocId() == 3 || doc.getDocumentoRespuestaDgiraPK().getTipoDocId() == 4 || doc.getDocumentoRespuestaDgiraPK().getTipoDocId() == 5) {
                    if (this.situacion.getDocumentoDgiraFlujoPK() == null || (this.situacion.getEstatusDocumentoId().getEstatusDocumentoId() == 4 && this.situacion.getRolId().getIdRol() == rol.getRolId().getIdRol())) {
                        actualizaHistorial(doc, rol);
                        resp = true;
                    }
                } else if (this.situacion.getDocumentoDgiraFlujoPK() != null && this.situacion.getRolId().getIdRol() == rol.getRolId().getIdRol()) {
                    actualizaHistorial(doc, rol);
                    resp = true;
                }
                break;
            case "da":
                System.out.println("El usuario logueado es un director de área/subdelegado");
                this.situacion = dao.recuperaSituacion(doc);
                if (this.situacion.getDocumentoDgiraFlujoPK() != null && this.situacion.getRolId().getIdRol() == rol.getRolId().getIdRol()) {
                    actualizaHistorial(doc, rol);
                    resp = true;
                }
                break;
            case "dg":
                System.out.println("El usuario logueado es el director general/delegado");
                this.situacion = dao.recuperaSituacion(doc);
                if (this.situacion.getDocumentoDgiraFlujoPK() != null && this.situacion.getRolId().getIdRol() == rol.getRolId().getIdRol()) {
                    actualizaHistorial(doc, rol);
                    resp = true;
                }
                break;
            default:
                System.out.println("El usuario no tiene un rol válido");
                break;
        }
        return resp;
    }
//</editor-fold>

    public void cargarOficioResolutivo() {
        this.oficioBd = dao.buscarResolutivo(bitacoraProyecto);
        this.tipoOficio = dao.recuperarTipoDoc(oficioBd);
    }

    public void cargarOficioResolutivoExcencion() {
        this.oficioBd = dao.buscarResolutivoExcencion(bitacoraProyecto);
        this.tipoOficio = dao.recuperarTipoDoc(oficioBd);
    }
    
    public void cargaOficioPrevencion() {
        this.oficioBd = dao.buscarPrevencion(bitacoraProyecto);            
        this.tipoOficio = dao.recuperarTipoDoc(oficioBd);
    }
    
    public void cargaOficio(short tipoDocId) {
        this.oficioBd = dao.buscarOficio(bitacoraProyecto, tipoDocId);      
        this.tipoOficio = dao.recuperarTipoDoc(oficioBd);
    }
    
    public void generarTokenFirma(DocumentoRespuestaDgira documento) {
        initContext();
        System.out.println("Entrando al método para generar el token...");
        DocumentoDgiraFlujo flujo = new DocumentoDgiraFlujo();
        Historial historial = new Historial();
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream("/resources/SignerClientAll.jar");
        file = new DefaultStreamedContent(stream, "application/java-archive", "SignerClientAll.jar");
        
        System.out.println(fContext.getExternalContext().getSessionMap().get("userFolioProy"));
        if (fContext.getExternalContext().getSessionMap().get("userFolioProy") != null) {
            folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
        } else {
            folio = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        }
        
        bitacoraProyecto = documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto();
        tipoOficio = (TipoDocumento) fContext.getExternalContext().getSessionMap().get("tipoDocumento");
        titulo = tipoOficio.getTipoDocDescripcion();
        ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());

        if (claveProyecto == null || claveProyecto.isEmpty()) {
            if (fContext.getExternalContext().getSessionMap().get("cveProyecto") != null) {
                claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();
            } else {
                claveProyecto = (String) fContext.getExternalContext().getSessionMap().get("cveTramite");
            }
        }

        System.out.println("-------------------------------------------------------------------------");
        System.out.println("folio :" + folio);
        System.out.println("claveProyecto :" + claveProyecto);
        System.out.println("bitacora :" + bitacoraProyecto);
        System.out.println("claveTramite :" + documento.getDocumentoRespuestaDgiraPK().getClaveTramite());
        System.out.println("idTramite :" + documento.getDocumentoRespuestaDgiraPK().getIdTramite());
        System.out.println("tipoDocId :" + documento.getDocumentoRespuestaDgiraPK().getTipoDocId());
        System.out.println("documentoId : " + documento.getDocumentoRespuestaDgiraPK().getDocumentoId());
        System.out.println("edoGestTramite : " + documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa());
        System.out.println("rol usuario : " + fContext.getExternalContext().getSessionMap().get("rol")); //sesion.getAttribute("rol").toString());
        System.out.println("idAreaUsu : " + fContext.getExternalContext().getSessionMap().get("idAreaUsu"));
        System.out.println("idUsu : " + fContext.getExternalContext().getSessionMap().get("idUsu"));
        System.out.println("titulo oficio : " + titulo);
        System.out.println("-------------------------------------------------------------------------");

        // busca si ya existe el registro de cadena firma...
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacoraProyecto, documento.getDocumentoRespuestaDgiraPK().getClaveTramite(), documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId());

        String msg_numero_token = Utils.obtenerConstante(GenericConstants.FIRMAR_NUMERO_TOKEN);
        String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.FIRMAR_NO_REGISTRO_CADENAFIRMA);

        if (firma != null) {

            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
        } else {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            Date thisDate = new Date();
            String fecha = dateFormat.format(thisDate);

            String descripcion_documento = titulo;
            String infoaFirmar = "||" + bitacoraProyecto + "|" + descripcion_documento + "||";
            String infoaFirmarB64 = Base64.encodeBase64String(infoaFirmar.getBytes());

            System.out.println("fecha :" + fecha);
            System.out.println("descripcion :" + descripcion_documento);
            System.out.println("encodeB64 :" + infoaFirmarB64);

            System.out.println("\n\nagregaCadena..");
            CadenaFirmaDao cadenaDao = new CadenaFirmaDao();
            BcAgregaCadenaRespuesta agregacadena = null;

            try {
                agregacadena = cadenaDao.agregaCadena(folio, infoaFirmarB64);

                if (agregacadena != null) {

                    firma = new CadenaFirma();

                    // firma.setClave(clave);
                    firma.setClaveProyecto(claveProyecto);
                    firma.setBitacora(bitacoraProyecto);
                    firma.setClaveTramite2(documento.getDocumentoRespuestaDgiraPK().getClaveTramite());
                    firma.setIdTramite2(documento.getDocumentoRespuestaDgiraPK().getIdTramite());
                    firma.setTipoDocId(documento.getDocumentoRespuestaDgiraPK().getTipoDocId());
                    firma.setDocumentoId(documento.getDocumentoRespuestaDgiraPK().getDocumentoId());

                    firma.setFolioProyecto(folio);
                    firma.setB64(infoaFirmarB64);
                    firma.setIdentificador(agregacadena.getIdentificador());
                    firma.setOperacion(String.valueOf(agregacadena.getTransferenciaOperacion()));

                    new CadenaFirmaDao().agrega(firma);

                    // despues de agregar la cadena de firma, se consulta para mostrar el identificador al usuario..
                    firma = null;
                    firma = firmaDao.getCadenaFirma(claveProyecto, bitacoraProyecto, documento.getDocumentoRespuestaDgiraPK().getClaveTramite(), documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId());

                    if (firma != null) {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_numero_token + " : " + firma.getIdentificador()));
                    } else {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
                    }

                } else {

                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
                }
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
            }
        }
        switch (ur.getRolId().getNombreCorto().trim()) {
            case "eva":
                System.out.println("El usuario que va firmar es un evaluador/área técnica");
                historial = recuperaAreaEnvio(bitacoraProyecto, "AT0027");
                if (guardaFlujoDocumento(documento, ur, historial)) {
                    System.out.println("El registro se insertó correctamente");
                } else {
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("No se pudo guardar el flujo"));
                }
                break;
            case "sd":
                
                System.out.println("El usuario que va a firmar es un subdirector/área técnica");
                if (documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 3 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 4 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 5) {
                    historial = recuperaAreaEnvio(bitacoraProyecto, "AT0027");
                    if (guardaFlujoDocumento(documento, ur, historial)) {
                        System.out.println("El registro se insertó correctamente");
                    } else {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("No se pudo guardar el flujo"));
                    }
                } else {
                    historial = recuperaAreaEnvio(bitacoraProyecto, "CIS104");
                    situacion = dao.recuperaSituacion(documento);
                    situacion.setIdAreaEnvio(ur.getUsuarioId().getIdarea().trim());
                    situacion.setIdUsuarioEnvio(ur.getUsuarioId().getIdusuario());
                    situacion.setIdAreaRecibe(historial.getHistoAreaRecibe());
                    situacion.setIdUsuarioRecibe(historial.getHistoIdUsuarioRecibe());
                    situacion.setEstatusDocumentoId(new CatEstatusDocumento(new Short("4")));
                    situacion.setRolId(ur.getRolId());
                    if (actualizarFlujoDocumento(situacion)) {
                        System.out.println("La situación del documento fue actualizada correctamente");
                    } else {
                        FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("No se pudo guardar el flujo"));
                    }
                }                                
                break;
            case "da":
                System.out.println("El usuario que va a firmar es un director de área/subdelegado");
                Tbarea areaPadre = dao.recuperarAreaPadre(ur.getUsuarioId());
                situacion = dao.recuperaSituacion(documento);
                situacion.setIdAreaEnvio(ur.getUsuarioId().getIdarea().trim());
                situacion.setIdUsuarioEnvio(ur.getUsuarioId().getIdusuario());
                situacion.setIdAreaRecibe(areaPadre.getIdareapadre());
                situacion.setIdUsuarioRecibe(null);
                situacion.setEstatusDocumentoId(new CatEstatusDocumento(new Short("4")));
                situacion.setRolId(ur.getRolId());
                if (actualizarFlujoDocumento(situacion)) {
                    System.out.println("La situación del documento fue actualizada correctamente");
                } else {
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("No se pudo guardar el flujo"));
                }
                break;
            case "dg":
                System.out.println("El usuario logueado que va a firmar es el director general/delegado");
                situacion = dao.recuperaSituacion(documento);
                situacion.setIdAreaEnvio(ur.getUsuarioId().getIdarea().trim());
                situacion.setIdUsuarioEnvio(ur.getUsuarioId().getIdusuario());
                situacion.setEstatusDocumentoId(new CatEstatusDocumento(new Short("4")));
                situacion.setRolId(ur.getRolId());
                if (actualizarFlujoDocumento(situacion)) {
                    System.out.println("La situación del documento fue actualizada correctamente");
                } else {
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("No se pudo guardar el flujo"));
                }
                break;
            default:
                System.out.println("El usuario no tiene un rol válido");
                break;
        }

    }

    public void turnarDocumento() {
        System.out.println("Entrando al método para turnar el documento...");
        initContext();
        List<DocumentoDgiraFlujoHist> historialTempDgiraMiae2 = new ArrayList();
        DocumentoDgiraFlujo flujo = new DocumentoDgiraFlujo();
        Historial historial = new Historial();
        DocumentoRespuestaDgira documento = (DocumentoRespuestaDgira) fContext.getExternalContext().getSessionMap().get("documentoRespuesta");
        bitacoraProyecto = documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto();
        
        if (fContext.getExternalContext().getSessionMap().get("cveProyecto") != null) {
            claveProyecto = fContext.getExternalContext().getSessionMap().get("cveProyecto").toString();
        } else {
            claveProyecto = (String) fContext.getExternalContext().getSessionMap().get("cveTramite");
        }
        
        if (fContext.getExternalContext().getSessionMap().get("userFolioProy") != null) {
            folio = fContext.getExternalContext().getSessionMap().get("userFolioProy").toString();
        } else {
            folio = (String) fContext.getExternalContext().getSessionMap().get("folioProyecto");
        }
        
        ur = AppSession.GetUsuarioRolPorRol(AppSession.GetRolSeleccionado());
        situacion = dao.recuperaSituacion(documento);
        // busca si ya existe el registro de cadena firma...
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        CadenaFirma firma = firmaDao.getCadenaFirma(claveProyecto, bitacoraProyecto, documento.getDocumentoRespuestaDgiraPK().getClaveTramite(), documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId());

        String msg_no_turno = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
        String msg_no_registro_cadenafirma = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);

        try {
            if (situacion.getEstatusDocumentoId().getEstatusDocumentoId() == 4) {
                if (firma != null) {
                    System.out.println("El documento ya está firmado");
                    switch (ur.getRolId().getNombreCorto().trim()) {
                        case "eva":
                            System.out.println("El usuario logueado es un evaluador/área técnica");
                            if (almacenarFirmaLog(firma, ur)) {
                                historial = recuperaAreaEnvio(bitacoraProyecto, "AT0027");
                                flujo = daoFlujo.docsFlujoDgira(bitacoraProyecto, documento.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId());
                                flujo.setEstatusDocumentoId(new CatEstatusDocumento(new Short("3")));
                                flujo.setDocumentoDgiraObservacion(null);
                                //Se agrega el valor del rol al que se envía el documento para que nadie lo firme ni turne
                                flujo.setRolId(new CatRol(5));
                                if (actualizarFlujoDocumento(flujo)) {
                                    insertarRegistroHist(documento, ur, historial);
                                    setBtnTurnado(false);
                                } else {
                                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_turno));
                                }
                            }
                            break;
                        case "sd":
                            System.out.println("El usuario logueado es un subdirector/área técnica");
                            if (almacenarFirmaLog(firma, ur)) {
                                historial = recuperaAreaEnvio(bitacoraProyecto, "CIS104");
                                flujo = daoFlujo.docsFlujoDgira(bitacoraProyecto, documento.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId());
                                flujo.setEstatusDocumentoId(new CatEstatusDocumento(new Short("3")));
                                flujo.setDocumentoDgiraObservacion(null);
                                //Se agrega el valor del rol al que se envía el documento para que nadie lo firme ni turne
                                flujo.setRolId(new CatRol(4));
                                if (actualizarFlujoDocumento(flujo)) {
                                    insertarRegistroHist(documento, ur, historial);
                                    setBtnTurnado(false);
                                } else {
                                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_turno));
                                }
                            }
                            break;
                        case "da":
                            System.out.println("El usuario logueado es un director de área/subdelegado");
                            if (almacenarFirmaLog(firma, ur)) {
                                Tbarea areaPadre = dao.recuperarAreaPadre(ur.getUsuarioId());
                                historial = new Historial(new HistorialPK(bitacoraProyecto, 1));
                                historial.setHistoAreaRecibe(areaPadre.getIdareapadre());
                                flujo = daoFlujo.docsFlujoDgira(bitacoraProyecto, documento.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId());
                                flujo.setEstatusDocumentoId(new CatEstatusDocumento(new Short("3")));
                                flujo.setDocumentoDgiraObservacion(null);
                                //Se agrega el valor del rol al que se envía el documento para que nadie lo firme ni turne
                                flujo.setRolId(new CatRol(3));
                                if (actualizarFlujoDocumento(flujo)) {
                                    insertarRegistroHist(documento, ur, historial);
                                    setBtnTurnado(false);
                                } else {
                                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_turno));
                                }
                            }
                            break;
                        case "dg":
                            System.out.println("El usuario logueado es el director general/delegado");
                            if (firmarPdf(documento, firma, ur)) {
                                historialTempDgiraMiae2 = dao.recuperarHistorialDgiraMiae2(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId());
                                if (documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 3 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 4 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 5) {
                                    System.out.println("Es un oficio de prevención");
                                    if (guardarSituacionesSINATDocRes(historialTempDgiraMiae2, documento, ur)) {
                                        suspenderTramite(documento, ur, claveProyecto, folio);
                                    }
                                } else {
                                    if (guardarSituacionesSINATDocRes(historialTempDgiraMiae2, documento, ur)) {
                                        suspenderTramite(documento, ur, claveProyecto, folio);
                                    }
                                }
                                historial = recuperaAreaEnvio(bitacoraProyecto, "010110");
                                flujo = daoFlujo.docsFlujoDgira(bitacoraProyecto, documento.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(),
                                        documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId());
                                flujo.setEstatusDocumentoId(new CatEstatusDocumento(new Short("3")));
                                flujo.setDocumentoDgiraObservacion(null);
                                //Se agrega el valor del rol al que se envía el documento para que nadie lo firme ni turne
                                flujo.setRolId(new CatRol(7));
                                if (actualizarFlujoDocumento(flujo)) {
                                    insertarRegistroHist(documento, ur, historial);
                                    setBtnTurnado(false);
                                } else {
                                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_turno));
                                }

                            }
                            break;
                        default:
                            System.out.println("El usuario no tiene un rol válido");
                            break;
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
            }
        } catch (NullPointerException e) {
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_no_registro_cadenafirma));
        }
    }

    public Historial recuperaAreaEnvio(String bitacora, String situacion) {
        Historial area = new Historial();
        area = dao.recuperaAreaAsignada(bitacora, situacion);
        return area;
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos para el flujo de los documentos en DGIRA_MIAE2 y SINAT">
    public boolean guardaFlujoDocumento(DocumentoRespuestaDgira documento, UsuarioRol usuario, Historial historial) {
        boolean resultado = false;
        DocumentoDgiraFlujo nuevoRegistro = new DocumentoDgiraFlujo();
        nuevoRegistro.setDocumentoDgiraFlujoPK(new DocumentoDgiraFlujoPK(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), documento.getDocumentoRespuestaDgiraPK().getClaveTramite(),
                documento.getDocumentoRespuestaDgiraPK().getIdTramite(), documento.getDocumentoRespuestaDgiraPK().getTipoDocId(),
                documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa(), documento.getDocumentoRespuestaDgiraPK().getDocumentoId()));
        nuevoRegistro.setIdAreaEnvio(usuario.getUsuarioId().getIdarea().trim());
        nuevoRegistro.setIdAreaRecibe(historial.getHistoAreaRecibe().trim());
        nuevoRegistro.setIdUsuarioEnvio(usuario.getUsuarioId().getIdusuario());
        nuevoRegistro.setIdUsuarioRecibe(historial.getHistoIdUsuarioRecibe());
        nuevoRegistro.setDocumentoDgiraFechaEnvio(new Date());
        nuevoRegistro.setDocumentoDgiraFechaRecibe(null);
        nuevoRegistro.setEstatusDocumentoId(new CatEstatusDocumento(new Short("4")));
        nuevoRegistro.setDocumentoDgiraObservacion(null);
        nuevoRegistro.setRolId(usuario.getRolId());
        try {
            daoFlujo.agrega(nuevoRegistro);
            resultado = true;
        } catch (Exception e) {
        }
        return resultado;
    }

    public boolean actualizarFlujoDocumento(DocumentoDgiraFlujo flujo) {
        boolean resultado = false;
        try {
            daoFlujo.modifica(flujo);
            resultado = true;
        } catch (Exception e) {
        }
        return resultado;
    }

    public boolean guardarSituacionesSINATDocRes(List<DocumentoDgiraFlujoHist> historialDgiraMiae2, DocumentoRespuestaDgira doc, UsuarioRol usuario) {
        boolean resultado = false;
        SinatDgira situacion = new SinatDgira();
        short tipoDocId = doc.getDocumentoRespuestaDgiraPK().getTipoDocId();
        try {
            for (DocumentoDgiraFlujoHist hist : historialDgiraMiae2) {
                switch (hist.getDocumentoDgiraFlujoHistPK().getDocumentoDgiraIdHist()) {
                    case 1:
                        situacion.setId(0);
                        situacion.setSituacion("AT0031");
                        situacion.setBitacora(hist.getDocumentoDgiraFlujoHistPK().getBitacoraProyecto());
                        situacion.setIdEntidadFederativa(hist.getDocumentoDgiraFlujoHistPK().getIdEntidadFederativa());
                        situacion.setIdClaveTramite(hist.getDocumentoDgiraFlujoHistPK().getClaveTramite());
                        situacion.setIdTramite(hist.getDocumentoDgiraFlujoHistPK().getIdTramite());
                        situacion.setFecha(hist.getDocumentoDgiraFeEnvHist());
                        situacion.setIdAreaEnvio(hist.getIdAreaEnvioHist().trim());
                        situacion.setIdAreaRecibe(hist.getIdAreaRecibeHist().trim());
                        situacion.setIdUsuarioEnvio(hist.getIdUsuarioEnvioHist());
                        situacion.setIdUsuarioRecibe(hist.getIdUsuarioRecibeHist());
                        situacion.setGrupoTrabajo(new Short("0"));
                        daoSituaciones.agrega(situacion);
                        resultado = true;
                        break;
                    case 2:
                        situacion.setId(0);
                        situacion.setSituacion("AT0032");
                        situacion.setBitacora(hist.getDocumentoDgiraFlujoHistPK().getBitacoraProyecto());
                        situacion.setIdEntidadFederativa(hist.getDocumentoDgiraFlujoHistPK().getIdEntidadFederativa());
                        situacion.setIdClaveTramite(hist.getDocumentoDgiraFlujoHistPK().getClaveTramite());
                        situacion.setIdTramite(hist.getDocumentoDgiraFlujoHistPK().getIdTramite());
                        situacion.setFecha(hist.getDocumentoDgiraFeEnvHist());
                        situacion.setIdAreaEnvio(hist.getIdAreaEnvioHist().trim());
                        situacion.setIdAreaRecibe(hist.getIdAreaRecibeHist().trim());
                        situacion.setIdUsuarioEnvio(hist.getIdUsuarioEnvioHist());
                        situacion.setIdUsuarioRecibe(hist.getIdUsuarioRecibeHist());
                        situacion.setGrupoTrabajo(new Short("0"));
                        daoSituaciones.agrega(situacion);
                        resultado = true;
                        break;
                    case 3:
                        situacion.setId(0);
                        situacion.setSituacion("AT0008");
                        situacion.setBitacora(hist.getDocumentoDgiraFlujoHistPK().getBitacoraProyecto());
                        situacion.setIdEntidadFederativa(hist.getDocumentoDgiraFlujoHistPK().getIdEntidadFederativa());
                        situacion.setIdClaveTramite(hist.getDocumentoDgiraFlujoHistPK().getClaveTramite());
                        situacion.setIdTramite(hist.getDocumentoDgiraFlujoHistPK().getIdTramite());
                        situacion.setFecha(hist.getDocumentoDgiraFeEnvHist());
                        situacion.setIdAreaEnvio(hist.getIdAreaEnvioHist().trim());
                        situacion.setIdAreaRecibe(hist.getIdAreaRecibeHist().trim());
                        situacion.setIdUsuarioEnvio(hist.getIdUsuarioEnvioHist());
                        situacion.setIdUsuarioRecibe(hist.getIdUsuarioRecibeHist());
                        situacion.setGrupoTrabajo(new Short("0"));
                        daoSituaciones.agrega(situacion);
                        resultado = true;
                        break;
                    default:
                        System.out.println("No es un objeto válido");
                        break;
                }
            }
        } catch (Exception e) {
        }
        return resultado;
    }

    public boolean suspenderTramite(DocumentoRespuestaDgira documento, UsuarioRol usuario, String claveProy, String folioProy) {
        SinatDgira situacion = new SinatDgira();
        boolean respuesta = false;
        short tipoSuspension = 0;
        int tipoActualizacion = 0;
        String descripcionDoc = "";
        if (documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 7) {
            System.out.println("Es un oficio de info adicional");
            tipoSuspension = 1;
            tipoActualizacion = 3;
            descripcionDoc = "Oficio de Información adicional del proyecto " + claveProy;
            
            situacion.setId(0);
            situacion.setSituacion("ATIF01");
            situacion.setBitacora(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
            situacion.setIdEntidadFederativa(documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa());
            situacion.setIdClaveTramite(documento.getDocumentoRespuestaDgiraPK().getClaveTramite());
            situacion.setIdTramite(documento.getDocumentoRespuestaDgiraPK().getIdTramite());
            situacion.setFecha(new Date());
            situacion.setIdAreaEnvio(usuario.getUsuarioId().getIdarea().trim());
            situacion.setIdAreaRecibe(recuperaAreaEnvio(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), "010110").getHistoAreaRecibe().trim());
            situacion.setIdUsuarioEnvio(usuario.getUsuarioId().getIdusuario());
            situacion.setGrupoTrabajo(new Short("0"));
            
            try {
                daoSituaciones.agrega(situacion);
                segVias.cerrarTurnado(documento, tipoSuspension, folio, tipoActualizacion, descripcionDoc, usuario, claveProy);
                respuesta = true;
            } catch (Exception e) {
                System.out.println("El trámite no pudo ser suspendido");
            }
            
        }
        if (documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 3 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 4 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 5) {
            System.out.println("Es un oficio de Prevencion");
                        

            tipoSuspension = 2;
            
            tipoActualizacion = 1;
            
            descripcionDoc = "Oficio de Prevención del proyecto " + claveProy;
            
            situacion.setId(0);
            situacion.setSituacion("AT0022");
            situacion.setBitacora(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
            situacion.setIdEntidadFederativa(documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa());
            situacion.setIdClaveTramite(documento.getDocumentoRespuestaDgiraPK().getClaveTramite());
            situacion.setIdTramite(documento.getDocumentoRespuestaDgiraPK().getIdTramite());
            situacion.setFecha(new Date());
            situacion.setIdAreaEnvio(usuario.getUsuarioId().getIdarea().trim());
            situacion.setIdAreaRecibe(recuperaAreaEnvio(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), "010110").getHistoAreaRecibe().trim());
            situacion.setIdUsuarioEnvio(usuario.getUsuarioId().getIdusuario());
            situacion.setGrupoTrabajo(new Short("0"));
            
            try {
                daoSituaciones.agrega(situacion);
                segVias.cerrarTurnado(documento, tipoSuspension, folio, tipoActualizacion, descripcionDoc, usuario, claveProy);
                respuesta = true;
            } catch (Exception e) {
                System.out.println("El trámite no pudo ser suspendido");
            }
            
        }
        if (documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 12 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 13 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 14 || documento.getDocumentoRespuestaDgiraPK().getTipoDocId() == 21) {
            //<editor-fold defaultstate="collapsed" desc="1.- Crea situacion en SINAT">
            situacion.setId(0);
            situacion.setSituacion("ATR001");
            situacion.setBitacora(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
            situacion.setIdEntidadFederativa(documento.getDocumentoRespuestaDgiraPK().getIdEntidadFederativa());
            situacion.setIdClaveTramite(documento.getDocumentoRespuestaDgiraPK().getClaveTramite());
            situacion.setIdTramite(documento.getDocumentoRespuestaDgiraPK().getIdTramite());
            situacion.setFecha(new Date());
            situacion.setIdAreaEnvio(usuario.getUsuarioId().getIdarea().trim());
            situacion.setIdAreaRecibe(recuperaAreaEnvio(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(), "010110").getHistoAreaRecibe().trim());
            situacion.setIdUsuarioEnvio(usuario.getUsuarioId().getIdusuario());
            situacion.setGrupoTrabajo(new Short("0"));
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="2.- Asigna valores para el procedimiento de Segundas Vias">
            /*Es un valor entero que indica el id del tipo de suspensión que origino el(los) oficios a 
            subir al volumen del SINATEC, se relaciona con la tabla CATALOGOS.CAT_TIPO_SUSPENDIDO.*/
            tipoSuspension = 6;
            /*Es un valor entero que indica el asunto a modificar del trámite. 
            Los valores permitidos son:
                1 = Solicita apertura al proceso de actualización de requisitos del trámite. por prevencion
                2 = Solicita apertura al proceso de acceso al link de sistema satelital.  informacion adicional
                3 = Solicita apertura a los proceso de actualización de requisitos del trámite y acceso al link de sistema satelital.
                4 = Cierre del tramite.*/
            tipoActualizacion = 4;
            descripcionDoc = "Oficio resolutivo del proyecto "+claveProy;
//</editor-fold>
            try {
                daoSituaciones.agrega(situacion);
                segVias.cerrarTurnado(documento, tipoSuspension, folio, tipoActualizacion, descripcionDoc, usuario, claveProy);
                respuesta = true;
            } catch (Exception e) {
                System.out.println("El trámite no pudo ser suspendido");
            }                
        }
        return respuesta;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos para la firma del documento">
    public boolean firmarPdf(DocumentoRespuestaDgira documento, CadenaFirma cadenaFirma, UsuarioRol rol) {
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);
        String msg_no_hay_token_registrado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_HAY_TOKEN);
        String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
        String msg_no_se_realizo_turnado = Utils.obtenerConstante(GenericConstants.TURNAR_NO_SE_REALIZO_TURNADO);
        String ligaQR = Utils.ruta_liga_codigoqr + documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto();
        boolean resp = almacenarFirmaLog(cadenaFirma, rol);
        try {
            if (resp == true) {
                firmaDao.editarPdf(documento, cadenaFirma, ligaQR);
                firmaDao.cierraLoteFirmas(Long.parseLong(cadenaFirma.getOperacion()), cadenaFirma.getIdentificador(), cadenaFirma.getFolioProyecto());

            } else {
                throw new Exception("Error: al guardar el log de la firma para el rol actual");
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp = false;
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
        }        
        return resp;
    }

    public boolean almacenarFirmaLog(CadenaFirma cadenaFirma, UsuarioRol rol) {
        String msg_error_al_turnar = Utils.obtenerConstante(GenericConstants.TURNAR_ERROR_AL_TURNAR);
        String msg_usuario_no_ha_firmado = Utils.obtenerConstante(GenericConstants.TURNAR_USUARIO_NO_HA_FIRMADO);
        CadenaFirmaDao firmaDao = new CadenaFirmaDao();
        // obtenemos la firmalog del usuario logueado ( por su rol y nivel lo sabemos y no necesariamente existe este registro en este momento )
        CadenaFirmaLog cadenafirmaLog = firmaDao.getCadenaFirmaLog(cadenaFirma, rol.getRolId().getNombreCorto().trim());
        // detallefirmado es objeto que tiene un estatus y descripcion y una lista de firmas del ws
        DetallesFirmado detallefirmado = null;
        boolean resp = false;
        try {
            detallefirmado = firmaDao.getFirmas(cadenaFirma);
            // el ws no responde
            if (detallefirmado == null || (detallefirmado != null && detallefirmado.getEstado() != 0)) {
                System.out.println("\nno responde el servicio web o no hay firmas...");
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
                // como el ws si esta respondiendo, analizar las firmas a ver si alguna de ellas corresponde con la de firmaLog actual...
            } else if (detallefirmado != null && detallefirmado.getEstado() == 0 && (detallefirmado.getFirmas() != null && detallefirmado.getFirmas().length > 0)) {

                // cadenafirmaLog puede ser null
                // detallefirmado si trae firmas del ws
                Firmas firmaws = firmaDao.GetFirmaWS(cadenaFirma, detallefirmado);

                // para este caso si no hay firma del ws decimos que no ha firmado el usuario...
                if (firmaws == null) {
                    System.out.println("ultima firma válida : null");
                    System.out.println("\nel usuario no ha firmado...");
                    FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_usuario_no_ha_firmado));
                } else {
                    System.out.println("ultima firma válida : " + firmaws.getRfc() + " - " + firmaws.getFecha());
                    System.out.println("el usuario ya firmó y se guardará en cadenafirmalog...");
                    // SE ASUME QUE SI FIRMÓ YA por tanto primero agregamos el registro de cadenafirmalog correspondiente...
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
                    Date fcha = formatter.parse(firmaws.getFecha());

                    if (cadenafirmaLog == null) {

                        CadenaFirmaLog firmaLog = new CadenaFirmaLog();
                        firmaLog.setCadenaFirma(cadenaFirma);
                        firmaLog.setEstatus((short) 1);
                        firmaLog.setRol(rol.getRolId().getNombreCorto().trim());
                        firmaLog.setFecha(fcha);
                        firmaLog.setFirma(firmaws.getFirma());
                        firmaLog.setRfc(firmaws.getRfc());

                        resp = new CadenaFirmaDao().agregaCadenaFirmaLog(firmaLog);
                        System.out.println("firmalog agregada: " + resp);

                    } else {
                        // se actualiza cadenafirmalog del rol en sesion
                        cadenafirmaLog.setRfc(firmaws.getRfc());
                        cadenafirmaLog.setFecha(fcha);
                        cadenafirmaLog.setFirma(firmaws.getFirma());
                        resp = new CadenaFirmaDao().modificaCadenaFirmaLog(cadenafirmaLog);
                        System.out.println("firmalog modificada: " + resp);

                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage(msg_error_al_turnar));
        }
        return resp;
    }
//</editor-fold>

    public void initContext() {
        this.fContext = FacesContext.getCurrentInstance();
    }

    public boolean isPanel() {
        return panel;
    }

    public void setPanel(boolean panel) {
        this.panel = panel;
    }

    public boolean isPanelDocumento() {
        return panelDocumento;
    }

    public void setPanelDocumento(boolean panelDocumento) {
        this.panelDocumento = panelDocumento;
    }

    public boolean isPanelMensaje() {
        return panelMensaje;
    }

    public void setPanelMensaje(boolean panelMensaje) {
        this.panelMensaje = panelMensaje;
    }

    public DocumentoRespuestaDgira getOficioBd() {
        return oficioBd;
    }

    public void setOficioBd(DocumentoRespuestaDgira oficioBd) {
        this.oficioBd = oficioBd;
    }

    public TipoDocumento getTipoOficio() {
        return tipoOficio;
    }

    public void setTipoOficio(TipoDocumento tipoOficio) {
        this.tipoOficio = tipoOficio;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public boolean isBtnTurnado() {
        return btnTurnado;
    }

    public void setBtnTurnado(boolean btnTurnado) {
        this.btnTurnado = btnTurnado;
    }

}
