/**
 * @author eescalona
 */

package mx.gob.semarnat.utils;

public class GenericConstants {
    
    public static final String ARCH_CONF_GENERAL                    = "resources.conf";
    
    public static final String CONST_SERVIDOR_HOSTNAME              = "hostServer";
    public static final String CONST_SERVIDOR_ADMIN                 = "hostAdmin";
    public static final String CONST_PETICION_GENERADOR_DOCTOS      = "peticionGeneradorDoctos";
    public static final String CONST_PETICION_VER_PDF               = "rutaVerPdf";    
    public static final String CONST_RUTA_DOCTOS_RESPUESTA          = "rutaDoctosRespuesta";
    public static final String CONST_RUTA_LIGA_CODIGOQR             = "rutaLigaCodigoQR";
    public static final String CONST_PETICION_SERVLET_INICIO        = "peticionServletInicio";
    public static final String CONST_PETICION_SERVLET               = "peticionServlet";
    
    public static final String CONST_PETICION_WEBSERVICE            = "peticionWebService";
    

    //Evaluación - Tipos de vista según versión/serial
    public static final int VISTA_PROMOVENTE_1ER_INGRESO            = 1;
    public static final int VISTA_EVALUADOR_1ER_INGRESO             = 2;
    public static final int VISTA_PROMOVENTE_INFO_ADICIONAL         = 3;
    public static final int VISTA_EVALUADOR_INFO_ADICIONAL          = 4;
    
    //Fecha inicial de consulta de trámites
    public static final String FECHA_CONSULTA_TRAMITES              = "01/01/2015";

    //Tipos de oficios
    public static final short TIPO_OFICIO_OPINION_TECNICA                     = 1;
    public static final short TIPO_OFICIO_NOTIFICACION_A_GOBIERNOS            = 2;
    public static final short TIPO_OFICIO_PREVENCION_PAGO_DERECHOS            = 3;
    
    
    public static final short TIPO_OFICIO_PREVENCION_REQUISITOS_COFEMER       = 4;
    public static final short TIPO_OFICIO_PREVENCION_PAGO_Y_REQUISITOS        = 5;
    public static final short TIPO_OFICIO_DESECHAMIENTO					      = 6;
    public static final short TIPO_OFICIO_INFORMACION_ADICIONAL               = 7;
    
    //Consulta pública
    public static final short TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION          = 8;
    public static final short TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE          = 9;
    public static final short TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL = 10;
    public static final short TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS          = 11;
    
    public static final short TIPO_OFICIO_RESOLUTIVO_AUTORIZADO			       = 12;
    public static final short TIPO_OFICIO_RESOLUTIVO_AUTORIZADO_CONDICIONADO   = 13;
    public static final short TIPO_OFICIO_RESOLUTIVO_NEGADO				       = 14;
    
    //Estatus documento/oficio
    public static final short ESTATUS_DOCTO_REVISION                = 1;
    public static final short ESTATUS_DOCTO_CORRECCION              = 2;
    public static final short ESTATUS_DOCTO_AUTORIZADO              = 3;
    public static final short ESTATUS_DOCTO_FIRMADO                 = 4;

    //Indicadores visuales
    public static final int ID_DIAS_NOTIFICACION_PREVENCION         = 2;
    public static final int ID_DIAS_NOTIFICACION_INFO_ADICIONAL     = 1;
    
    //WebService obtención de archivos
    public static final int WS_RESP_OK                              = 0;
    public static final int WS_TIPO_DOCTO_GENERICO                  = 1;
    public static final int WS_TIPO_DOCTO_REQUISITOS_GENERICOS      = 2;
    public static final int WS_TIPO_DOCTO_INFORMACION_TEMATICA      = 3;
    
    // niveles que representa el login determinado de un usuario dependiendo del documento que firme
    public static final short NIVEL_1            = 1;
    public static final short NIVEL_2            = 2;
    public static final short NIVEL_3            = 3;
    public static final short NIVEL_4            = 4;
    
    // Servicio Web de firma electronica
    public static final String CONST_WS_URL = "ws_firma_url";
    public static final String CONST_WS_ENTIDAD = "ws_firma_entidad";
    public static final String CONST_WS_OPERADOR = "ws_firma_operador";
    public static final String CONST_WS_CLAVE = "ws_firma_clave";
    public static final String CONST_WS_DIGESTION = "ws_firma_digestion";
    public static final String CONST_WS_TIPOCODIFICACION = "ws_firma_tipocodificacion";
    public static final String CONST_WS_AVISO = "ws_firma_aviso";
    public static final String CONST_WS_ESTADO_TRANSFERENIA = "ws_estado_transferencia";
    public static final String CONST_WS_ESTADO_DESCRIPCION = "ws_estado_descripcion";
    
    
    // mensajes en el proceso/boton de firmar ( los oficios )
    // cuando indica el numero de token que se registro en base a la alta en el ws
    public static final String FIRMAR_NUMERO_TOKEN = "firma_numero_token";
    // cuando al dar 'firmar' ocurre algun error y no registra el registro en cadenafirma despues de haber invocado el metodo de alta del ws
    public static final String FIRMAR_NO_REGISTRO_CADENAFIRMA = "firma_noregistro_cadenafirma";
    
    
    // mensajes correspondientes al turnado y correccion de oficios.
    // cuando un usurio intenta turnar y no se encuentra el encabezado de cadenafirma(token del oficio que previamente tuvo que guardarse al dar 'firmar')
    public static final String TURNAR_NO_HAY_TOKEN = "turnar_no_hay_token";
    // cuando al intentar turnar, se deduce que el usuario no ha firmado previamente
    public static final String TURNAR_USUARIO_NO_HA_FIRMADO = "turnar_usuario_no_ha_firmado";
    public static final String TURNAR_NO_SE_REALIZO_TURNADO = "turnar_no_se_realizo_turnado";
    public static final String TURNAR_ERROR_AL_TURNAR = "turnar_error_al_turnar";
    
    
    
    //Consulta pública
    public static final String  TIPO_OFICIO_CONSULTA_PUBLICA_DELEGACION_S          = "TIPO OFICIO CONSULTA PUBLICA DELEGACION";
    public static final String TIPO_OFICIO_CONSULTA_PUBLICA_PROMOVENTE_S           = "TIPO OFICIO CONSULTA PUBLICA PROMOVENTE";
    public static final String TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL_S  = "TIPO OFICIO CONSULTA PUBLICA CIUDADANO PRINCIPAL";
    public static final String TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANOS_S           = "TIPO OFICIO CONSULTA PUBLICA CIUDADANOS";
    
}
