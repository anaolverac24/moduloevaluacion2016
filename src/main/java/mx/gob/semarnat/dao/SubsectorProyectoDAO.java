package mx.gob.semarnat.dao;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;

import mx.gob.semarnat.model.catalogos.EntidadFederativa;
import mx.gob.semarnat.model.catalogos.Sector;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;

public class SubsectorProyectoDAO {
	
	private final EntityManager em = PersistenceManager.getEmfBitacora().createEntityManager();
    
    public List<SubsectorProyecto> getAllSubsectorProy() {
		return em.createNativeQuery("select * from CATALOGOS.SUBSECTOR_PROYECTO", SubsectorProyecto.class).getResultList();
    }
    
    public List<SubsectorProyecto> getSubsectorProyBySector(int sector) {
    	String sql = "select * from CATALOGOS.SUBSECTOR_PROYECTO WHERE NSEC = ?1";
    	Query q =  em.createNativeQuery(sql, SubsectorProyecto.class);
    	q.setParameter(1, sector);
		return q.getResultList();
    }
}
