/**
 *
 */
package mx.gob.semarnat.dao;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;

import mx.gob.semarnat.model.catalogos.EntidadFederativa;

/**
 * @author dpaniagua.
 *
 */
public class EntidadFederativaDAO {

    /**
     * Entity persistente para las transacciones a la tabla de
     * EntidadFederativa.
     */
    private final EntityManager em = PersistenceManager.getEmfMIAE().createEntityManager();

    /**
     * Permite consultar la entidad federativa del usuario loggeado.
     *
     * @param idEntidadFederativa
     * @return la entidad federativa.
     * @throws SQLException en caso de error al ejecutar la consulta.
     */
    public EntidadFederativa consultarEntidadFederativaID(String idEntidadFederativa) throws SQLException {
        //Se recupera
        Query q = em.createQuery("from EntidadFederativa where idEntidadFederativa = :idEntidadFederativa");
        q.setParameter("idEntidadFederativa", idEntidadFederativa);
        EntidadFederativa entidadFederativa = (EntidadFederativa) q.getSingleResult();
        return entidadFederativa;
    }

    public List<EntidadFederativa> getAllEntidadF() {
        return em.createNamedQuery("EntidadFederativa.findAll", EntidadFederativa.class).getResultList();
    }
}
