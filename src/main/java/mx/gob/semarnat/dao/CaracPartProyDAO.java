/**
 *
 */
package mx.gob.semarnat.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.model.dgira_miae.CaracPartProy;
import mx.gob.semarnat.model.dgira_miae.CatNaturaleza;
import mx.gob.semarnat.model.dgira_miae.CatTemporalidad;

/**
 * @author dpaniagua.
 *
 */
public class CaracPartProyDAO {

    private final EntityManager emfMIAE = PersistenceManager.getEmfMIAE().createEntityManager();

    /**
     * Permite actualizar la Caracteristica Particular del proyecto.
     *
     * @param caracPartProy
     */
    public void actualizarCaracteristica(CaracPartProy caracPartProy) throws Exception {
        try {
            emfMIAE.getTransaction().begin();
            emfMIAE.merge(caracPartProy);
            emfMIAE.getTransaction().commit();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
    }

    public List<CatNaturaleza> getCatNaturaleza() {
        List<CatNaturaleza> resultado = new ArrayList<>();
        try {
            Query q = emfMIAE.createQuery("SELECT c FROM CatNaturaleza c order by c.naturalezaDescripcion");
            resultado = q.getResultList();
        } catch (Exception e) {
        }
        return resultado;
    }

    public List<CatTemporalidad> getCatTemporalidad() {
        List<CatTemporalidad> resultado = new ArrayList<>();
        try {
            Query q = emfMIAE.createQuery("SELECT c FROM CatTemporalidad c order by c.temporalidadDescripcion");
            resultado = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return resultado;
    }

    /**
     * Se consulta el max valor de la sequencia obtenida.
     *
     * @return el valor maximo de la sequencia
     */
    public Short consultarMAXCaracSequence() throws Exception {
        Short resultado = new Short("0");
        try {
            Query q = emfMIAE.createQuery("select max(idCaracSeq) from CaracPartProy");
            resultado = (Short) q.getSingleResult();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return resultado;
    }

    /**
     * Se consulta el valor maximo de las caracteristicas para cargarlas.
     *
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return el maximo valor de las caracteristicas.
     */
    public Short getMaxCaracteristica(String folio, short serial) throws Exception {
        Short resultado = new Short("0");
        try {
            Query q = emfMIAE.createQuery("SELECT max(cp.caracPartProyPK.caracteristicaId) FROM CaracPartProy cp where cp.caracPartProyPK.folioProyecto = :folio and cp.caracPartProyPK.serialProyecto =:serial ");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            resultado = (Short) q.getSingleResult();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return resultado;
    }

    /**
     * Permite guardar las caracteristicas del proyecto.
     *
     * @param caracPartProy a guardar.
     * @throws Exception en caso de error al guardar.
     */
    public void guardarCaracteristica(CaracPartProy caracPartProy) throws Exception {
        EntityManager mia = PersistenceManager.getEmfMIAE().createEntityManager();
        try {
            mia.getTransaction().begin();
            short caracteristicaID = (short) (caracPartProy.getCaracPartProyPK().getCaracteristicaId() + 1);
            caracPartProy.getCaracPartProyPK().setCaracteristicaId(caracteristicaID);
            mia.persist(caracPartProy);
            mia.getTransaction().commit();
        } catch (Exception e) {
        } finally {
            mia.clear();
        }
    }

    /**
     * Permite buscar las caracteristicas particulares del promovente pero que
     * contenga registros de SIGEIA.
     *
     * @param folio
     * @param serial
     * @return la lista de registros de SIGEAI en caracteristicas particulares
     * del Promovente.
     */
    public List<CaracPartProy> getCaracPartSIGEIAPROM(String folio, short serial) {
        List<CaracPartProy> listaCarac = new ArrayList<>();
        try {
            Query q = emfMIAE.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial and r.estatusSigProm = '0' order by r.idCaracSeq");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            listaCarac = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return listaCarac;
    }

    public List<CaracPartProy> getCaracPart(String folio, short serial) {
        List<CaracPartProy> listaCarac = new ArrayList<>();
        try {
            Query q = emfMIAE.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial and r.estatusSigProm = '1' order by r.idCaracSeq");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            listaCarac = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return listaCarac;
    }

    /**
     * Permite consultar la lista de caracteristicas particulares del proyecto
     * del promovente con su informacion de SIGEIA.
     *
     * @param folioProyecto
     * @param serialProyecto
     * @return
     */
    public List<CaracPartProy> getCaracPartOrderBySIGEIA(String folioProyecto, short serialProyecto) {
        List<CaracPartProy> listaCarac = new ArrayList<>();
        try {
            Query q = emfMIAE.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial "
                    + "order by r.estatusSigProm");
            q.setParameter("folio", folioProyecto);
            q.setParameter("serial", serialProyecto);
            listaCarac = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return listaCarac;
    }

    /**
     * Permite consultar la naturaleza del proyecto.
     *
     * @param idNaturaleza
     * @return la naturaleza del proyecto.
     * @throws Exception en caso de error al consultar la naturaleza.
     */
    public CatNaturaleza consultarNaturaleza(int idNaturaleza) throws Exception {
        CatNaturaleza resultado = new CatNaturaleza();
        try {
            Query q = emfMIAE.createQuery("FROM CatNaturaleza where naturalezaId = " + idNaturaleza);
            resultado = (CatNaturaleza) q.getSingleResult();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return resultado;
    }

    /**
     * Permite consultar la temporalidad del proyecto.
     *
     * @param idTemporalidad
     * @return la temporalidad del proyecto.
     * @throws Exception en caso de error al consultar la temporalidad.
     */
    public CatTemporalidad consultarTemporalidad(int idTemporalidad) throws Exception {
        CatTemporalidad resultado = new CatTemporalidad();
        try {
            Query q = emfMIAE.createQuery("FROM CatTemporalidad where temporalidadId = " + idTemporalidad);
            resultado = (CatTemporalidad) q.getSingleResult();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return resultado;
    }

    public CatUnidadMedida getCatUnidadMedida(Short ctunClve) {
        CatUnidadMedida catUnidadMedida = new CatUnidadMedida();
        try {
            Query q = emfMIAE.createQuery("SELECT c FROM CatUnidadMedida c where c.ctunClve=:ctunClve order by c.ctunDesc");
            q.setParameter("ctunClve", ctunClve);
            catUnidadMedida = (CatUnidadMedida) q.getSingleResult();
        } catch (Exception e) {
        } finally {
            emfMIAE.clear();
        }
        return catUnidadMedida;

    }
}
