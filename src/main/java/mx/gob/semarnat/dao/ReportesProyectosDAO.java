/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Paty
 */
@SuppressWarnings("serial")
public class ReportesProyectosDAO implements Serializable {

    EntityManager em = PersistenceManager.getEmfBitacora().createEntityManager();
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    public List<Object[]> busquedaRepProyIngreso(Date fechaInicio, Date fechaFin, String claveProyecto, String entidad,
            int riesgo, int sector, String entidadGestion, String municipo, String estudio, int subsector) {
        String sql = "SELECT DISTINCT\n"
                + "  NVL(BT.CVE,'-') CLAVE,\n"
                + "  NVL(EG.NOMBRE_ENTIDAD_FEDERATIVA,'-') AS ENTIDAD_GESTION,\n"
                + "  NVL(EA.NOMBRE_ENTIDAD_FEDERATIVA,'-') AS ENTIDAD_AFECTADA,\n"
                + "  NVL(MUN.NOMBRE_DELEGACION_MUNICIPIO,'-') AS MUNICIPIO_AFECTADO,\n"
                + "  CASE\n"
                + "    WHEN DF.DFI_RAZON_SOCIAL IS NULL THEN DF.DFI_NOMBRE || ' ' || DF.DFI_APELLIDO_PATERNO || ' ' || DF.DFI_APELLIDO_MATERNO\n"
                + "    ELSE DF.DFI_RAZON_SOCIAL END AS PROMOVENTE,\n"
                + "  NVL(BT.NOMBRE,'-') NOMBRE_PROYECTO,\n"
                + "  CASE WHEN (BI.BITA_TIPOTRAM = 'DM' OR BI.BITA_TIPOTRAM = 'DL') THEN 'Si'\n"
                + "       WHEN (BI.BITA_TIPOTRAM = 'IP' AND P.ESTUDIO_RIESGO_ID = 1) THEN 'Si'\n"
                + "       WHEN (BI.BITA_TIPOTRAM = 'DC') THEN 'No aplica'\n"
                + "       ELSE 'No' END AS Riesgo,"
                + "  CASE\n"
                + "    WHEN (BI.BITA_TIPOTRAM = 'IP') THEN 'RECEPCION Y EVALUACION DEL INFORME PREVENTIVO'\n"
                + "    WHEN (BI.BITA_TIPOTRAM = 'MP') THEN 'MIA PARTICULAR.- MOD A: NO  INCLUYE RIESGO'\n"
                + "    WHEN (BI.BITA_TIPOTRAM = 'DM') THEN 'MIA PARTICULAR.- MOD B: INCLUYE RIESGO'\n"
                + "    WHEN (BI.BITA_TIPOTRAM = 'MG') THEN 'MIA REGIONAL.- MOD A: NO INCLUYE RIESGO'\n"
                + "    WHEN (BI.BITA_TIPOTRAM = 'DL') THEN 'MIA REGIONAL.- MOD B: INCLUYE RIESGO'\n"
                + "    WHEN (BI.BITA_TIPOTRAM = 'DC') THEN 'SOLICITUD DE EXENCION'\n"
                + "    ELSE 'SIN TIPO DE SOLICITUD' END AS TIPO_TRAMITE,\n"
                + "  NVL(SEC.SECTOR, '-') SECTOR,\n"
                + "  NVL(SS.SUBSECTOR, '-') SUBSECTOR,\n"
                + "  BI.BITA_FOFI_RECEPCION AS FECHA_INGRESO\n"
                + "FROM BITACORA.BITACORA BI\n"
                + "LEFT JOIN BITACORA_TEMATICA.PROYECTO BT ON BT.NUMERO_BITA = BI.BITA_NUMERO\n"
                + "LEFT JOIN PADRON.DATOS_FISCALES DF ON BI.BITA_ID_PROM = DF.PROM_ID\n"
                + "LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO DOM ON BT.CVE = DOM.PROY\n"
                + "LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA EG ON BI.BITA_ENTIDAD_GESTION = EG.ID_ENTIDAD_FEDERATIVA\n"
                + "LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO MUN ON DOM.MUN_DEL = MUN.ID_DELEGACION_MUNICIPIO\n"
                + "LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA EA ON MUN.ENTIDAD_FEDERATIVA = EA.ID_ENTIDAD_FEDERATIVA\n"
                + "LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO SS ON BT.SUBSECTOR = SS.NSUB\n"
                + "LEFT JOIN CATALOGOS.SECTOR_PROYECTO SEC ON SEC.NSEC = SS.NSEC\n"
                + "LEFT JOIN BITACORA.BITACORA_RESOLUCION BR ON BI.BITA_NUMERO = BR.BIRE_BITACORA\n"
                + "LEFT JOIN CATALOGOS.CAT_MODO_RESOL MR ON BR.BIRE_MODO = MR.CVE\n"
                + "LEFT JOIN DGIRA_MIAE2.PROYECTO P ON BI.BITA_NUMERO = P.BITACORA_PROYECTO\n"
                + "WHERE BI.BITA_FOFI_RECEPCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd') AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";

        System.out.println("+ fechas: " + df.format(fechaInicio) + ", " + df.format(fechaFin));
        if (claveProyecto != null && !claveProyecto.equals("")) {
            System.out.println("+ clave proy: " + claveProyecto);
            sql += " AND BT.CVE LIKE '%" + claveProyecto + "%'";
        }
        if (entidadGestion != null && !entidadGestion.equals("")) {
            System.out.println("+ entidad gest: " + entidadGestion);
            sql += " AND EG.ID_ENTIDAD_FEDERATIVA = '" + entidadGestion + "'";//09
        }
        if (entidad != null && !entidad.equals("")) {
            System.out.println("+ entidad: " + entidad);
            sql += " AND EA.ID_ENTIDAD_FEDERATIVA = '" + entidad + "'";//05
        }
        if (municipo != null && !municipo.equals("")) {
            System.out.println("+ municipio: " + municipo);
            sql += " AND MUN.ID_DELEGACION_MUNICIPIO = '" + municipo + "'";//05
        }
        if (riesgo != 0) {//validar el cero por default que se asigna
            System.out.println("+ riesgo: " + riesgo);
            if (riesgo == 2) {
                riesgo = 0;
            }
            sql += " AND NVL(P.ESTUDIO_RIESGO_ID,0) = " + riesgo;//05
        }
        if (estudio != null && !estudio.equals("")) {
            System.out.println("+ estudio: " + estudio);
            sql += " AND BI.BITA_TIPOTRAM = '" + estudio + "'";//05
        }
        if (sector != 0) {//validar el cero por default que se asigna
            System.out.println("+ sector: " + sector);
            sql += " AND SEC.NSEC = " + sector;//05
        }
        if (subsector != 0) {//validar el cero por default que se asigna
            System.out.println("+ subsector: " + subsector);
            sql += " AND SS.NSUB = " + subsector;//05
        }
//		    + " AND SUBSEC.NSUB = 1 AND SUBSEC.NSEC = 1";
        @SuppressWarnings("unchecked")
        List<Object[]> result = em.createNativeQuery(sql).getResultList();
        System.out.println("Busqueda t1: size: " + result.size());
        return result;
    }

    public List<Object[]> resumenTodos() {//1
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> resumenPorSubsector(Date fechaInicio, Date fechaFin, int numSubsector) {//2
        String sql = "SELECT S.SUBSECTOR AS SUBSECTOR, COUNT(S.NSUB) AS TOTAL"
                + " FROM BITACORA_TEMATICA.PROYECTO C"
                + " LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO"
                + " WHERE B.BITA_FOFI_RECEPCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd')"
                + " 				    AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";
        if (numSubsector != 0) {
            System.out.println(" + nsub: " + numSubsector);
            sql += " AND S.NSUB = " + numSubsector;
        }
        sql += " GROUP BY S.SUBSECTOR";
        sql += " ORDER BY TOTAL DESC";

        return em.createNativeQuery(sql).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> resumenPorTipoTramite(Date fechaInicio, Date fechaFin, String tipoTram_estudio) {//3
        String sql = "SELECT"
                + " CASE"
                + "   WHEN (B.BITA_TIPOTRAM = 'IP') THEN 'RECEPCION Y EVALUACION DEL INFORME PREVENTIVO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'MP') THEN 'MIA PARTICULAR.- MOD A:  INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'DM') THEN 'MIA PARTICULAR.- MOD B: INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'MG') THEN 'MIA REGIONAL.- MOD A: NO INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'DL') THEN 'MIA REGIONAL.- MOD B: NO INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'DC') THEN 'SOLICITUD DE EXENCION'"
                + "   END AS TIPO_TRAMITE,"
                + " count(B.BITA_TIPOTRAM) as total"
                + " FROM BITACORA.BITACORA B"
                + " LEFT JOIN BITACORA_TEMATICA.PROYECTO C ON B.BITA_NUMERO = C.NUMERO_BITA"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO"
                + " WHERE(B.BITA_TIPOTRAM IN('IP','MP','DM','MG','DL','DC'))"
                + " AND B.BITA_FOFI_RECEPCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd')"
                + " 				  AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";
        if (tipoTram_estudio != null && !tipoTram_estudio.equals("")) {
            System.out.println("+ estudio: " + tipoTram_estudio);
            sql += " AND B.BITA_TIPOTRAM = '" + tipoTram_estudio + "'";
        }
        sql += " GROUP BY B.BITA_TIPOTRAM";
        return em.createNativeQuery(sql).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> resumenPorEntidad(Date fechaInicio, Date fechaFin, String entidad) {//4
        String sql = "SELECT"
                + "   F.NOMBRE_ENTIDAD_FEDERATIVA as ENTIDAD,"
                + "   COUNT(F.NOMBRE_ENTIDAD_FEDERATIVA) AS TOTAL"
                + " FROM BITACORA_TEMATICA.PROYECTO C"
                + " LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P.PROM_ID = B.BITA_ID_PROM"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D.PROY"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D.MUN_DEL = M.ID_DELEGACION_MUNICIPIO"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M.ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M.CVE"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO"
                + " WHERE B.BITA_FOFI_RECEPCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd')"
                + " 				    AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";
        if (entidad != null && !entidad.equals("")) {
            System.out.println("+ entidad fed: " + entidad);
            sql += " AND F.ID_ENTIDAD_FEDERATIVA = '" + entidad + "'";
        }
        sql += " GROUP BY F.NOMBRE_ENTIDAD_FEDERATIVA";
        return em.createNativeQuery(sql).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> busquedaRepProyResueltos(Date fechaInicio, Date fechaFin, String claveProyecto, String entidad,
            int riesgo, int sector, String entidadGestion, String municipo, String estudio, int subsector,
            String direccion, String subdir, String eval, int eficiencia, int sentidoResol) {
        List<Object[]> result;
        String sql = "SELECT DISTINCT\n"
                + "C.CVE,\n"
                + "E.NOMBRE_ENTIDAD_FEDERATIVA AS ENTIDAD_GESTION,\n"
                + "F.NOMBRE_ENTIDAD_FEDERATIVA AS ENTIDAD_AFECTADA,\n"
                + "M.NOMBRE_DELEGACION_MUNICIPIO AS MUNICIPIO_AFECTADO,\n"
                + "CASE WHEN P .DFI_RAZON_SOCIAL IS NULL\n"
                + "   THEN P.DFI_NOMBRE || ' ' || P .DFI_APELLIDO_PATERNO || ' ' || P .DFI_APELLIDO_MATERNO\n"
                + "   ELSE P.DFI_RAZON_SOCIAL END AS PROMOVENTE,\n"
                + "C.NOMBRE AS NOMBRE_PROY,\n"
                + "CASE WHEN NVL(PE.ESTUDIO_RIESGO_ID,0) = 0 THEN 'No' ELSE 'Sí' END AS Riesgo,\n"
                + "CASE\n"
                + "   WHEN (B.BITA_TIPOTRAM = 'IP') THEN 'RECEPCION Y EVALUACION DEL INFORME PREVENTIVO'\n"
                + "   WHEN (B.BITA_TIPOTRAM = 'MP') THEN 'MIA PARTICULAR.- MOD A: NO INCLUYE RIESGO'\n"
                + "   WHEN (B.BITA_TIPOTRAM = 'DM') THEN 'MIA PARTICULAR.- MOD B: INCLUYE RIESGO'\n"
                + "   WHEN (B.BITA_TIPOTRAM = 'MG') THEN 'MIA REGIONAL.- MOD A: NO INCLUYE RIESGO'\n"
                + "   WHEN (B.BITA_TIPOTRAM = 'DL') THEN 'MIA REGIONAL.- MOD B: INCLUYE RIESGO'\n"
                + "   WHEN (B.BITA_TIPOTRAM = 'DC') THEN 'SOLICITUD DE EXENCION'\n"
                + "   ELSE '' END AS TIPO_TRAMITE,\n"
                + "V.SECTOR,\n"
                + "S.SUBSECTOR,\n"
                + "B.BITA_FOFI_RECEPCION AS FECHA_INGRESO,\n"
                + "AX.AREA AS DIRECCION,\n"
                + "CR.ROL_NOMBRE AS ROL,--subdireccion\n"
                + "EMP.NOMBRE||' '||EMP.APELLIDOPATERNO||' '||EMP.APELLIDOMATERNO AS EVAL,\n"
                + "B.BITA_FOFI_RESOLUCION AS FECHA_RESOL,\n"
                + "M .CVE AS RESOL_SINAT,\n"
                + "M .RESOLUCION AS SENTIDO_RES,\n"
                + "CASE WHEN C.INVERSION IS NULL THEN '0' ELSE C.INVERSION END AS INVERSION,\n"
                + "CASE WHEN PE.PROY_EMPLEOS_TEMPORALES IS NULL THEN  0 ELSE PE.PROY_EMPLEOS_TEMPORALES END AS EMP_TMP,\n"
                + "CASE WHEN PE.PROY_EMPLEOS_PERMANENTES IS NULL THEN 0 ELSE PE.PROY_EMPLEOS_PERMANENTES END AS EMP_PERM,\n"
                + "CASE WHEN C.NO_EMPLEADOS IS NULL THEN PE.PROY_EMPLEOS_PERMANENTES+PROY_EMPLEOS_TEMPORALES+0\n"
                + "   ELSE C.NO_EMPLEADOS END AS NO_EMPL,--total_empl\n"
                + "CASE WHEN (B.BITA_DIAS_TRAMITE - B.BITA_DIAS_PROCESO) < 0 THEN 0\n"
                + "   ELSE (B.BITA_DIAS_TRAMITE - B.BITA_DIAS_PROCESO) END AS DIAS_DISPONIBLES,\n"
                + "B.BITA_DIAS_PROCESO AS DIAS_UTILIZADOS,\n"
                + "CASE WHEN (B.BITA_DIAS_TRAMITE - B.BITA_DIAS_PROCESO) < 0 THEN 0 ELSE 1 END AS EN_TIEMPO--//--eficiencia\n"
                + "FROM BITACORA.BITACORA B\n"
                + "INNER JOIN BITACORA_TEMATICA.PROYECTO C ON B.BITA_NUMERO = C.NUMERO_BITA\n"
                + "LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM\n"
                + "LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY\n"
                + "LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA\n"
                + "LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO\n"
                + "LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA\n"
                + "LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB\n"
                + "LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC\n"
                + "LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA\n"
                + "LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M.CVE\n"
                + "INNER JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO\n"
                + "RIGHT JOIN SEGURIDAD.TBAREA AX ON (AX.IDAREA = B.BITA_AREA_RECIBE)\n"
                + "LEFT JOIN SEGURIDAD.TBUSUARIO UZ ON UZ.IDUSUARIO = B.BITA_ID_USUARIO_RECIBE\n"
                + "LEFT JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = UZ.IDEMPLEADO\n"
                + "LEFT JOIN DGIRA_MIAE2.USUARIO_ROL UR ON UR.USUARIO_ID = UZ.IDUSUARIO\n"
                + "LEFT JOIN DGIRA_MIAE2.CAT_ROL CR ON CR.ROL_ID = UR.ROL_ID\n"
                + "WHERE B.BITA_FOFI_RESOLUCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd') AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')\n";

        System.out.println("+ fechas: " + df.format(fechaInicio) + ", " + df.format(fechaFin));
        if (claveProyecto != null && !claveProyecto.equals("")) {
            System.out.println("+ clave proy: " + claveProyecto);
            sql += " AND C.CVE LIKE '%" + claveProyecto + "%'";
        }
        if (entidadGestion != null && !entidadGestion.equals("")) {
            System.out.println("+ entidad gest: " + entidadGestion);
            sql += " AND E.ID_ENTIDAD_FEDERATIVA = '" + entidadGestion + "'";//09
        }
        if (entidad != null && !entidad.equals("")) {//ENTIDAD AFECTADA
            System.out.println("+ entidad: " + entidad);
            sql += " AND F.ID_ENTIDAD_FEDERATIVA = '" + entidad + "'";//05
        }
        if (municipo != null && !municipo.equals("")) {
            System.out.println("+ municipio: " + municipo);
            sql += " AND M.ID_DELEGACION_MUNICIPIO = '" + municipo + "'";//05
        }
        if (riesgo != 0) {//validar el cero por default que se asigna
            System.out.println("+ riesgo: " + riesgo);
            if (riesgo == 2) {
                riesgo = 0;
            }
            sql += " AND NVL(PE.ESTUDIO_RIESGO_ID,0) = " + riesgo;//05
        }
        if (estudio != null && !estudio.equals("")) {//ESTUDIO DE RIESGO
            System.out.println("+ estudio: " + estudio);
            sql += " AND B.BITA_TIPOTRAM = '" + estudio + "'";//05
        }
        if (sector != 0) {//validar el cero por default que se asigna
            System.out.println("+ sector: " + sector);
            sql += " AND V.NSEC = " + sector;//05
        }
        if (subsector != 0) {//validar el cero por default que se asigna
            System.out.println("+ subsector: " + subsector);
            sql += " AND S.NSUB = " + subsector;
//		    + " AND SUBSEC.NSUB = 1 AND SUBSEC.NSEC = 1";
        }
        //-REPORTES DE PROYECTOS RESUELTOS
        if (direccion != null && !direccion.equals("")) {//ESTUDIO DE RIESGO
            System.out.println("+ direccion: " + direccion);
            String dep[] = direccion.split("+");
//    		sql += " AND (AX.IDDEPENDENCIA = 'MEDAMB000001' AND AX.IDAREA = '01020502')";
            sql += " AND (AX.IDDEPENDENCIA = '" + dep[0] + "' AND AX.IDAREA = '" + dep[1] + "')";
        }
        if (subdir != null && !subdir.equals("")) {//ESTUDIO DE RIESGO
            System.out.println("+ subdirector id empl: " + subdir);
            sql += " AND CR.ROL_ID = 5";//subdir
        }
        if (eval != null && !eval.equals("")) {//ESTUDIO DE RIESGO
            System.out.println("+ Evaluador id empl: " + eval);
            sql += " AND EMP.IDEMPLEADO = '" + eval + "'";
        }
        if (sentidoResol != 0) {//validar el cero por default que se asigna
            System.out.println("+ SENTIDO RESOL: " + sentidoResol);
            sql += " AND M.CVE = " + sentidoResol;
        }
        if (eficiencia != 0) {
            System.out.println("+ Eficiencia(en tiempo): " + eficiencia);
            if (eficiencia == 2) {
                eficiencia = 0;//no
                sql += " AND (B.BITA_DIAS_TRAMITE - B.BITA_DIAS_PROCESO) < 0";
            } else {//1=Si
                sql += " AND (B.BITA_DIAS_TRAMITE - B.BITA_DIAS_PROCESO) > 0";
            }
        }
        result = em.createNativeQuery(sql).getResultList();
        System.out.println(">>> Finliza busqueda de reportes resueltos > size: " + result.size());
        return result;
    }

    public List<Object[]> resumenPorSubsectorProyRes(Date fechaInicio, Date fechaFin, int subsetor) {
        String sql = "SELECT"
                + "   S.SUBSECTOR,"
                + "   SUM(CASE WHEN (R.BIRE_MODO = 1	OR R.BIRE_MODO = 4)THEN 1 ELSE 0 END)AS AUTORIZADOS,"
                + "   SUM(CASE WHEN (R.BIRE_MODO = 2)THEN 1 ELSE 0 END)AS NO_AUT,"
                + "   (SUM(CASE WHEN (R.BIRE_MODO = 1	OR R.BIRE_MODO = 4)THEN 1 ELSE 0 END)+SUM(CASE WHEN (R.BIRE_MODO = 2)THEN 1 ELSE 0 END)) AS TOTAL"
                + " FROM BITACORA_TEMATICA.PROYECTO C"
                + " LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO"
                + " LEFT JOIN SEGURIDAD.TBAREA AX ON (AX.IDAREA = B.BITA_AREA_RECIBE)"
                + " LEFT JOIN SEGURIDAD.TBUSUARIO UZ ON UZ.IDUSUARIO = B.BITA_ID_USUARIO_RECIBE"
                + " LEFT JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = UZ.IDEMPLEADO"
                + " LEFT JOIN DGIRA_MIAE2.USUARIO_ROL UR ON UR.USUARIO_ID = UZ.IDUSUARIO"
                + " LEFT JOIN DGIRA_MIAE2.CAT_ROL CR ON CR.ROL_ID = UR.ROL_ID"
                + " WHERE B.BITA_FOFI_RESOLUCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd')"
                + "                                 AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";
        if (subsetor != 0) {
            System.out.println(" + subsector: " + subsetor);
            sql += " AND S.NSUB = " + subsetor;
        }
        sql += " GROUP BY S.SUBSECTOR";
        List<Object[]> result = em.createNativeQuery(sql).getResultList();
        System.out.println("Resumen por subsector > size: " + result.size());
        return result;
    }

    public List<Object[]> resumenPorTipoTramProyRes(Date fechaInicio, Date fechaFin, String estudio) {
        String sql = "SELECT"
                + " CASE"
                + "   WHEN (B.BITA_TIPOTRAM = 'IP') THEN 'RECEPCION Y EVALUACION DEL INFORME PREVENTIVO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'MP') THEN 'MIA PARTICULAR.- MOD A: NO INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'DM') THEN 'MIA PARTICULAR.- MOD B: INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'MG') THEN 'MIA REGIONAL.- MOD A: NO INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'DL') THEN 'MIA REGIONAL.- MOD B: INCLUYE RIESGO'"
                + "   WHEN (B.BITA_TIPOTRAM = 'DC') THEN 'SOLICITUD DE EXENCION'"
                + "   ELSE 'OTRO'"
                + "   END AS TIPO_TRAMITE,"
                + " SUM(CASE WHEN (R.BIRE_MODO = 1	OR R.BIRE_MODO = 4)THEN 1 ELSE 0 END)AS AUTORIZADOS,"
                + " SUM(CASE WHEN (R.BIRE_MODO = 2)THEN 1 ELSE 0 END)AS NO_AUT,"
                + " (SUM(CASE WHEN (R.BIRE_MODO = 1	OR R.BIRE_MODO = 4)THEN 1 ELSE 0 END)+SUM(CASE WHEN (R.BIRE_MODO = 2)THEN 1 ELSE 0 END)) AS TOTAL"
                + " FROM BITACORA_TEMATICA.PROYECTO C"
                + " LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO"
                + " LEFT JOIN SEGURIDAD.TBAREA AX ON (AX.IDAREA = B.BITA_AREA_RECIBE)"
                + " LEFT JOIN SEGURIDAD.TBUSUARIO UZ ON UZ.IDUSUARIO = B.BITA_ID_USUARIO_RECIBE"
                + " LEFT JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = UZ.IDEMPLEADO"
                + " LEFT JOIN DGIRA_MIAE2.USUARIO_ROL UR ON UR.USUARIO_ID = UZ.IDUSUARIO"
                + " LEFT JOIN DGIRA_MIAE2.CAT_ROL CR ON CR.ROL_ID = UR.ROL_ID"
                + " WHERE B.BITA_FOFI_RESOLUCION IS NOT NULL"
                + " AND B.BITA_FOFI_RESOLUCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd')"
                + "                               AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";
        if (estudio != null && !estudio.equals("")) {//ESTUDIO DE RIESGO
            System.out.println(" + estudio: " + estudio);
            sql += " AND B.BITA_TIPOTRAM = '" + estudio + "'";
        }
        sql += " GROUP BY B.BITA_TIPOTRAM";
        List<Object[]> result = em.createNativeQuery(sql).getResultList();
        System.out.println("Resumen por tipo de tramite > size: " + result.size());
        return result;
    }

    public List<Object[]> resumenPorEntidadProyRes(Date fechaInicio, Date fechaFin, String entidad) {
        String sql = "SELECT"
                + " 	F.NOMBRE_ENTIDAD_FEDERATIVA as ENTIDAD,"
                + " 	SUM(CASE WHEN (R.BIRE_MODO = 1	OR R.BIRE_MODO = 4)THEN 1 ELSE 0 END)AS AUTORIZADOS,"
                + " 	SUM(CASE WHEN (R.BIRE_MODO = 2)THEN 1 ELSE 0 END)AS NO_AUT,"
                + " 	(SUM(CASE WHEN (R.BIRE_MODO = 1	OR R.BIRE_MODO = 4)THEN 1 ELSE 0 END)+SUM(CASE WHEN (R.BIRE_MODO = 2)THEN 1 ELSE 0 END)) AS TOTAL"
                + " FROM BITACORA_TEMATICA.PROYECTO C"
                + " LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO"
                + " LEFT JOIN SEGURIDAD.TBAREA AX ON (AX.IDAREA = B.BITA_AREA_RECIBE)"
                + " LEFT JOIN SEGURIDAD.TBUSUARIO UZ ON UZ.IDUSUARIO = B.BITA_ID_USUARIO_RECIBE"
                + " LEFT JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = UZ.IDEMPLEADO"
                + " LEFT JOIN DGIRA_MIAE2.USUARIO_ROL UR ON UR.USUARIO_ID = UZ.IDUSUARIO"
                + " LEFT JOIN DGIRA_MIAE2.CAT_ROL CR ON CR.ROL_ID = UR.ROL_ID"
                + " WHERE  B.BITA_FOFI_RESOLUCION IS NOT NULL"
                + " AND B.BITA_FOFI_RESOLUCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd')"
                + "                               AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";
        if (entidad != null && !entidad.equals("")) {//ESTUDIO DE RIESGO
            System.out.println(" + entidad fed: " + entidad);
            sql += " AND E.ID_ENTIDAD_FEDERATIVA = '" + entidad + "'";
        }
        sql += " GROUP BY F.NOMBRE_ENTIDAD_FEDERATIVA";
        List<Object[]> result = em.createNativeQuery(sql).getResultList();
        System.out.println("Resumen por subsector > size: " + result.size());
        return result;
    }

    public List<Object[]> resumenPorEficienciaProyRes(Date fechaInicio, Date fechaFin, int eficiencia) {
        String sql = "SELECT"
                + "   TO_CHAR(B.BITA_FOFI_RESOLUCION,'fm  Month  ','nls_date_language=spanish') AS MES,"
                + "   SUM(CASE WHEN (B.BITA_DIAS_TRAMITE > B.BITA_DIAS_PROCESO) THEN 1 ELSE 0 END) AS EN_TIEMPO,"
                + "   SUM(CASE WHEN (B.BITA_DIAS_TRAMITE < B.BITA_DIAS_PROCESO) THEN 1  ELSE 0 END) AS FUERA_TIEMPO,"
                + "   (SUM(CASE WHEN (B.BITA_DIAS_TRAMITE < B.BITA_DIAS_PROCESO) THEN 1 ELSE 0 END)+SUM(CASE WHEN (B.BITA_DIAS_TRAMITE > B.BITA_DIAS_PROCESO) THEN 1  ELSE 0 END)) AS TOTAL,"
                + "   (SUM(CASE WHEN (B.BITA_DIAS_TRAMITE > B.BITA_DIAS_PROCESO) THEN 1 ELSE 0 END)/(SUM(CASE WHEN (B.BITA_DIAS_TRAMITE < B.BITA_DIAS_PROCESO) THEN 1 ELSE 0 END)+SUM(CASE WHEN (B.BITA_DIAS_TRAMITE > B.BITA_DIAS_PROCESO) THEN 1  ELSE 0 END))) AS PENT,"
                + "   (SUM(CASE WHEN (B.BITA_DIAS_TRAMITE < B.BITA_DIAS_PROCESO) THEN 1 ELSE 0 END)/(SUM(CASE WHEN (B.BITA_DIAS_TRAMITE < B.BITA_DIAS_PROCESO) THEN 1 ELSE 0 END)+SUM(CASE WHEN (B.BITA_DIAS_TRAMITE > B.BITA_DIAS_PROCESO) THEN 1  ELSE 0 END))) AS PFT"
                + " FROM BITACORA_TEMATICA.PROYECTO C"
                + " LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D .PROY"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO"
                + " LEFT JOIN SEGURIDAD.TBAREA AX ON (AX.IDAREA = B.BITA_AREA_RECIBE)"
                + " LEFT JOIN SEGURIDAD.TBUSUARIO UZ ON UZ.IDUSUARIO = B.BITA_ID_USUARIO_RECIBE"
                + " LEFT JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = UZ.IDEMPLEADO"
                + " LEFT JOIN DGIRA_MIAE2.USUARIO_ROL UR ON UR.USUARIO_ID = UZ.IDUSUARIO"
                + " LEFT JOIN DGIRA_MIAE2.CAT_ROL CR ON CR.ROL_ID = UR.ROL_ID"
                + " WHERE B.BITA_FOFI_RESOLUCION IS NOT NULL"
                + " AND B.BITA_FOFI_RESOLUCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd')"
                + "                               AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')";
        if (eficiencia != 0) {
            System.out.println("+ Eficiencia(en tiempo): " + eficiencia);
            if (eficiencia == 2) {//para evitar 0 por default que no es seleccionado
                eficiencia = 0;//no
                sql += " AND (B.BITA_DIAS_TRAMITE - B.BITA_DIAS_PROCESO) < 0";
            } else {//1=Si
                sql += " AND (B.BITA_DIAS_TRAMITE - B.BITA_DIAS_PROCESO) > 0";
            }
        }
        sql += " GROUP BY B.BITA_FOFI_RESOLUCION";

        List<Object[]> result = em.createNativeQuery(sql).getResultList();
        System.out.println("Resumen por subsector > size: " + result.size());
        return result;
    }

    public List<Object[]> resumenPorResumenInversionProyRes(Date fechaInicio, Date fechaFin) {
        String sql = "SELECT\n"
                + " S.SUBSECTOR,\n"
                + " SUM(CASE WHEN (R.BIRE_MODO = 1	OR R.BIRE_MODO = 4)THEN 1 ELSE 0 END)AS AUTORIZADOS,\n"
                + " SUM(CASE WHEN(TO_NUMBER(TRIM(REPLACE(REPLACE(NVL(C.INVERSION, '0'),'$', '0'),' ', '')), '9999999999999999.99999'))= 0 THEN 0 ELSE 1 END) AS P_REP_inv,\n"
                + " SUM(TO_NUMBER(TRIM(REPLACE(REPLACE(NVL(C.INVERSION, '0'),'$', '0'),' ', '')), '9999999999999999.99999')) AS INV_EN_PESOS,\n"
                + " SUM(CASE WHEN PE.PROY_EMPLEOS_PERMANENTES IS NOT NULL OR PE.PROY_EMPLEOS_TEMPORALES IS NOT NULL THEN 1 ELSE 0 END)as PROY_C_EM,\n"
                + " SUM((CASE WHEN PE.PROY_EMPLEOS_PERMANENTES IS NOT NULL THEN PE.PROY_EMPLEOS_PERMANENTES ELSE 0 END)+(CASE WHEN PE.PROY_EMPLEOS_TEMPORALES IS NOT NULL THEN PE.PROY_EMPLEOS_TEMPORALES ELSE 0 END)) AS TOT_EMPLEOS,\n"
                + " SUM((CASE WHEN PE.PROY_EMPLEOS_PERMANENTES IS NOT NULL THEN PE.PROY_EMPLEOS_PERMANENTES ELSE 0 END))AS TOT_EMP_PERM,\n"
                + " SUM((CASE WHEN PE.PROY_EMPLEOS_TEMPORALES IS NOT NULL THEN PE.PROY_EMPLEOS_TEMPORALES ELSE 0 END))AS TOT_EMP_TEMP,\n"
                + " ((SUM(TO_NUMBER(TRIM(REPLACE(REPLACE(NVL(C.INVERSION, '0'),'$', '0'),' ', '')), '9999999999999999.99999'))) /\n"
                + "  (SELECT\n"
                + "    SUM(TO_NUMBER(NVL(REGEXP_REPLACE(C.INVERSION, '[^0-9]', ''), '0'))) AS INV_EN_PESOS\n"
                + "    FROM BITACORA_TEMATICA.PROYECTO C\n"
                + "    LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA\n"
                + "    LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM\n"
                + "    LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D.PROY\n"
                + "    LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA\n"
                + "    LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO\n"
                + "    LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA\n"
                + "    LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB\n"
                + "    LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC\n"
                + "    LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA\n"
                + "    LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE\n"
                + "    LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO\n"
                + "    LEFT JOIN SEGURIDAD.TBAREA AX ON (AX.IDDEPENDENCIAPADRE = B.BITA_IDDEPENDENCIA_RECIBE)\n"
                + "    LEFT JOIN SEGURIDAD.TBUSUARIO UZ ON UZ.IDUSUARIO = B.BITA_ID_USUARIO_RECIBE\n"
                + "    LEFT JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = UZ.IDEMPLEADO\n"
                + "    LEFT JOIN DGIRA_MIAE2.USUARIO_ROL UR ON UR.USUARIO_ID = UZ.IDUSUARIO\n"
                + "    LEFT JOIN DGIRA_MIAE2.CAT_ROL CR ON CR.ROL_ID = UR.ROL_ID\n"
                + "    WHERE  B.BITA_FOFI_RESOLUCION IS NOT NULL AND C.INVERSION IS NOT NULL and (R.BIRE_MODO = 4 or R.BIRE_MODO = 1))\n"
                + "  ) as PARTICIPACION\n"
                + " FROM BITACORA_TEMATICA.PROYECTO C\n"
                + " LEFT JOIN BITACORA.BITACORA B ON B.BITA_NUMERO = C.NUMERO_BITA\n"
                + " LEFT JOIN PADRON.DATOS_FISCALES P ON P .PROM_ID = B.BITA_ID_PROM\n"
                + " LEFT JOIN BITACORA_TEMATICA.PROY_DOMICILIO D ON C.CVE = D.PROY\n"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA E ON B.BITA_ENTIDAD_GESTION = E .ID_ENTIDAD_FEDERATIVA\n"
                + " LEFT JOIN CATALOGOS.DELEGACION_MUNICIPIO M ON D .MUN_DEL = M .ID_DELEGACION_MUNICIPIO\n"
                + " LEFT JOIN CATALOGOS.ENTIDAD_FEDERATIVA F ON M .ENTIDAD_FEDERATIVA = F.ID_ENTIDAD_FEDERATIVA\n"
                + " LEFT JOIN CATALOGOS.SUBSECTOR_PROYECTO S ON C.SUBSECTOR = S.NSUB\n"
                + " LEFT JOIN CATALOGOS.SECTOR_PROYECTO V ON V.NSEC = S.NSEC\n"
                + " LEFT JOIN BITACORA.BITACORA_RESOLUCION R ON B.BITA_NUMERO = R.BIRE_BITACORA\n"
                + " LEFT JOIN CATALOGOS.CAT_MODO_RESOL M ON R.BIRE_MODO = M .CVE\n"
                + " LEFT JOIN DGIRA_MIAE2.PROYECTO PE ON B.BITA_NUMERO = PE.BITACORA_PROYECTO\n"
                + " LEFT JOIN SEGURIDAD.TBAREA AX ON (AX.IDDEPENDENCIAPADRE = B.BITA_IDDEPENDENCIA_RECIBE)\n"
                + " LEFT JOIN SEGURIDAD.TBUSUARIO UZ ON UZ.IDUSUARIO = B.BITA_ID_USUARIO_RECIBE\n"
                + " LEFT JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = UZ.IDEMPLEADO\n"
                + " LEFT JOIN DGIRA_MIAE2.USUARIO_ROL UR ON UR.USUARIO_ID = UZ.IDUSUARIO\n"
                + " LEFT JOIN DGIRA_MIAE2.CAT_ROL CR ON CR.ROL_ID = UR.ROL_ID\n"
                + " WHERE  B.BITA_FOFI_RESOLUCION IS NOT NULL AND C.INVERSION IS NOT NULL"
                + " AND B.BITA_FOFI_RESOLUCION BETWEEN TO_DATE ('" + df.format(fechaInicio) + "', 'yyyy/mm/dd') AND TO_DATE ('" + df.format(fechaFin) + "', 'yyyy/mm/dd')"
                + " GROUP BY S.SUBSECTOR";
        List<Object[]> result = em.createNativeQuery(sql).getResultList();
        System.out.println("Resumen por subsector > size: " + result.size());
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getPersonasByRol(int rol) {
        String sql = "SELECT DISTINCT"// --UR.USUARIO_ID, UR.ROL_ID, US.USUARIO,
                + "   EMP.IDEMPLEADO,"
                + "   EMP.NOMBRE||' '||EMP.APELLIDOPATERNO||' '||EMP.APELLIDOMATERNO AS SUBDIRECTOR"
                + " FROM USUARIO_ROL UR"
                + " INNER JOIN SEGURIDAD.TBUSUARIO US ON US.IDUSUARIO = UR.USUARIO_ID"
                + " INNER JOIN SEGURIDAD.TBEMPLEADO EMP ON EMP.IDEMPLEADO = US.IDEMPLEADO"
                + " WHERE UR.ROL_ID = " + rol;// 5--SUBDIRECTOR, 6-directores
        return em.createNativeQuery(sql).getResultList();
    }

}
