/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import mx.gob.semarnat.model.catalogos.CatTratamientos;
import mx.gob.semarnat.model.catalogos.DelegacionMunicipio;
import mx.gob.semarnat.model.catalogos.EntidadFederativa;
import mx.gob.semarnat.model.dgira_miae.AccidentesProyecto;
import mx.gob.semarnat.model.dgira_miae.ActividadEtapa;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.AnpProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArticuloReglaAnp;
import mx.gob.semarnat.model.dgira_miae.CatEstEsp;
import mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento;
import mx.gob.semarnat.model.dgira_miae.CatTipoDocDgira;
import mx.gob.semarnat.model.dgira_miae.ClimaProyecto;
import mx.gob.semarnat.model.dgira_miae.ConsultaPublicaProyecto;
import mx.gob.semarnat.model.dgira_miae.ConsultaPublicaProyectoPK;
import mx.gob.semarnat.model.dgira_miae.ContaminanteProyecto;
import mx.gob.semarnat.model.dgira_miae.ConvenioProy;
import mx.gob.semarnat.model.dgira_miae.CuerposAguaProyecto;
import mx.gob.semarnat.model.dgira_miae.DisposicionProyecto;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.dgira_miae.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.dgira_miae.FaunaProyecto;
import mx.gob.semarnat.model.dgira_miae.FloraProyecto;
import mx.gob.semarnat.model.dgira_miae.InfobioProyecto;
import mx.gob.semarnat.model.dgira_miae.InstrUrbanos;
import mx.gob.semarnat.model.dgira_miae.InversionEtapas;
import mx.gob.semarnat.model.dgira_miae.Microcuenca;
import mx.gob.semarnat.model.dgira_miae.NormaProyecto;
import mx.gob.semarnat.model.dgira_miae.OeGralTerrit;
import mx.gob.semarnat.model.dgira_miae.OeMarinos;
import mx.gob.semarnat.model.dgira_miae.OePoligEnvol;
import mx.gob.semarnat.model.dgira_miae.OeRegionales1;
import mx.gob.semarnat.model.dgira_miae.OeRegionales2;
import mx.gob.semarnat.model.dgira_miae.OeRegionales3;
import mx.gob.semarnat.model.dgira_miae.PdumProyecto;
import mx.gob.semarnat.model.dgira_miae.PoetmProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoPK;
import mx.gob.semarnat.model.dgira_miae.ProyectoTO;
import mx.gob.semarnat.model.dgira_miae.ReglamentoProyecto;
import mx.gob.semarnat.model.dgira_miae.ReiaProyObras;
import mx.gob.semarnat.model.dgira_miae.RepLegalProyecto;
import mx.gob.semarnat.model.dgira_miae.ServicioProyecto;
import mx.gob.semarnat.model.dgira_miae.SueloVegetacionProyecto;
import mx.gob.semarnat.model.dgira_miae.SustanciaProyecto;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;
import mx.gob.semarnat.utils.GenericConstants;

import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import java.io.StringReader;

/**
 *
 * @author Rodrigo
 */
@SuppressWarnings("deprecation")
public class DgiraMiaeDaoGeneral {

    private final EntityManager em = PersistenceManager.getEmfMIAE().createEntityManager();
    private final EntityManager emfMIAE = em;
    //<editor-fold desc="Max id" defaultstate="collapsed">
    
    /**
     * Obtiene el registro mas grande dentro de una tabla
     *
     * @param tipo
     * @param col
     * @return
     */
    public Short MAX(Class<?> tipo, String col) {
        Short s = 0;
        try {
            Query q = em.createQuery("SELECT MAX(a." + col + ") FROM " + tipo.getName() + " a ");
            s = (Short) q.getSingleResult();
        } catch (Exception e) {
        }
        return s != null ? s : 0;
    }//</editor-fold>

    //<editor-fold desc="Listado completo" defaultstate="collapsed">
    /**
     * Consulta todos los datos dentro de una tabla
     *
     * @param tipo
     * @return
     */
    public List<?> listado(Class<?> tipo) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        cq.select(cq.from(tipo));
        return em.createQuery(cq).getResultList();
    }//</editor-fold>

    //<editor-fold desc="Búsqueda por Id" defaultstate="collapsed">
    /**
     * Busca en la tabla el objeto con el id proporcionado
     *
     * @param tipo
     * @param id
     * @return
     */
    public Object busca(Class<?> tipo, Object id) {
        return em.find(tipo, id);
    }//</editor-fold>

    //<editor-fold desc="Agrega registro a la tabla" defaultstate="collapsed">
    /**
     * Persiste objeto de entidad mapeada
     *
     * @param o
     */
    public void agrega(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                em.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }//</editor-fold>

    //<editor-fold desc="Actualiza registro del objeto mapeado" defaultstate="collapsed">
    /**
     * Actualiza objeto de una entidad mapeada
     *
     * @param o
     */
    public void modifica(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.merge(o);
            tx.commit();
//        em.close();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }//</editor-fold>

    //<editor-fold desc="Elimina registro de la base de datos por Id" defaultstate="collapsed">
    /**
     * Elimina registro de la base de datos
     *
     * @param tipo
     * @param id
     */
    public void elimina(Class<?> tipo, Object id) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        Object o = em.find(tipo, id);
        try {
            if (o != null) {
                tx.begin();
                em.remove(o);
                tx.commit();
            }
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }//</editor-fold>

    //<editor-fold desc="Listado a partir de un namedQuery" defaultstate="collapsed">
    /**
     * Hace listado a partir de un NamedQuery dentro de la entidad mapeada
     *
     * @param qry
     * @param args
     * @param paramName
     * @return
     */
    public List<?> lista_namedQuery(String qry, Object[] args, String[] paramName) {
        Query q = em.createNamedQuery(qry);
        for (int i = 0; i < args.length; i++) {
            q.setParameter(paramName[i], args[i]);
        }
        return q.getResultList();
    }//</editor-fold>
    
    
    @SuppressWarnings("unchecked")
	public List<ArticuloReglaAnp> getArticuloReglaAnp(String folio, short serial, String tipo) {
        Query q = emfMIAE.createQuery("SELECT r FROM ArticuloReglaAnp r WHERE r.articuloReglaAnpPK.folioProyecto = :folio and r.articuloReglaAnpPK.serialProyecto = :serial and r.anpArticuloRegla = :tipo");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("tipo", tipo);
        return q.getResultList();
    }  
    
    
    //=====================================================================================================================================================
    @SuppressWarnings("unchecked")
	public List<NormaProyecto> consultarNormas(String folio, Short serial) {
    	//EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
    	Query q = emfMIAE.createQuery("SELECT e FROM NormaProyecto e WHERE e.normaProyectoPK.folioProyecto = :folio and e.normaProyectoPK.serialProyecto = :serial ORDER BY e.normaProyectoPK.normaProyId ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }   
    //=====================================================================================================================================================    
    
    /**
     * Recupera la informacion capturada para el Glosario
     * @param folio Numero asignado por SINATEC al tramite
     * @param serial [1] indica si es ingreso y [3] indica que Evaluador solicitó informacion adicional 
     * @return Texto capturado para el Glosario
     */
    public Object glosarioPro(String folio, Short serial)
    {
   	 Query q = emfMIAE.createNativeQuery("SELECT dbms_lob.substr(PROY_GLOSARIO, 4000, 1 ) FROM PROYECTO WHERE FOLIO_PROYECTO =:folio AND SERIAL_PROYECTO=:serial");
   	 q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getSingleResult();
   }
    
    
    //Metodo para impacto ambiental
    @SuppressWarnings("unchecked")
	public List<Object> impactoAmbiente(String folio, Short serial)
    {
   	 Query q = emfMIAE.createNativeQuery("SELECT IMPAC_AMBPROY_ID, CAT_ETAPA.ETAPA_DESCRIPCION, IMPACT_INDENT, IMPACT_DESC, IMPAC_CARAC, IMPAC_EVA, CAT_TIPO_IMPACTO.TIPO_IMPACTO_DESCRIPCION, IMPAC_INDIC FROM IMPAC_AMB_PROYECTO INNER JOIN CAT_ETAPA ON CAT_ETAPA.ETAPA_ID= IMPAC_AMB_PROYECTO.ETAPA_ID INNER JOIN CAT_TIPO_IMPACTO ON CAT_TIPO_IMPACTO.TIPO_IMPACTO_ID = IMPAC_AMB_PROYECTO.IMPACTO_ID WHERE IMPAC_AMB_PROYECTO.FOLIO_PROYECTO=:folio AND IMPAC_AMB_PROYECTO.SERIAL_PROYECTO=:serial");
   	 q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        @SuppressWarnings("rawtypes")
		List l = q.getResultList();
        return  l;
   }
        
    
    //SELECT NUMERAL_ID, NORMA_NUMERAL, NORMA_REQUISITO, NORMA_VINCULACION FROM NUMERAL WHERE FOLIO_PROYECTO= '8440' AND SERIAL_PROYECTO=1
    @SuppressWarnings("unchecked")
	public List<Object> nums (String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT NUMERAL_ID, NORMA_NUMERAL, NORMA_REQUISITO, NORMA_VINCULACION FROM NUMERAL WHERE FOLIO_PROYECTO=:folio AND SERIAL_PROYECTO=:serial");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    
    
    //SELECT A.NORMA_PROY_ID as NORMA_PROY_ID, CASE A.NORMA_ID  WHEN 9999 THEN A.NORMA_DESCRIPCION  ELSE B.NORMA_NOMBRE END AS NORMA_NOMBRE, A.NORMA_FECH_PUBLIC AS NORMA_FECH_PUBLIC, A.ID_ARCHIVO_PROGRAMA FROM NORMA_PROYECTO A, CAT_NORMA B WHERE A.NORMA_ID = B.NORMA_ID AND A.FOLIO_PROYECTO = 8440 AND A.SERIAL_PROYECTO = 1 ORDER BY NORMA_PROY_ID
    @SuppressWarnings("unchecked")
	public List<Object> normass (String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT A.NORMA_PROY_ID as NORMA_PROY_ID, CASE A.NORMA_ID  WHEN 9999 THEN A.NORMA_DESCRIPCION  ELSE B.NORMA_NOMBRE END AS NORMA_NOMBRE, A.NORMA_FECH_PUBLIC AS NORMA_FECH_PUBLIC, A.ID_ARCHIVO_PROGRAMA FROM NORMA_PROYECTO A, CAT_NORMA B WHERE A.NORMA_ID = B.NORMA_ID AND A.FOLIO_PROYECTO =:folio AND A.SERIAL_PROYECTO =:serial ORDER BY NORMA_PROY_ID");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    
    
    //SELECT REGLAMENTO_PROY_ID, REGLAMENTO_DESCRIPCION, REGLAMENTO_FECHA_PUBLICACION, REGLAMENTO_ARTICULO, REGLAMENTO_VICULACION, ID_ARCHIVO_PROYECTO FROM REGLAMENTO_PROYECTO WHERE FOLIO_PROYECTO = '8440' AND SERIAL_PROYECTO = 1
    @SuppressWarnings("unchecked")
	public List<Object> reglam (String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT REGLAMENTO_PROY_ID, REGLAMENTO_DESCRIPCION, REGLAMENTO_FECHA_PUBLICACION, REGLAMENTO_ARTICULO, REGLAMENTO_VICULACION, ID_ARCHIVO_PROYECTO FROM REGLAMENTO_PROYECTO WHERE FOLIO_PROYECTO =:folio AND SERIAL_PROYECTO =:serial");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    
    //METODO PARA LEYES
    @SuppressWarnings("unchecked")
	public List<Object> leyess(String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT LEY_FE_ID, LEY_FE_DESCRIP, FECHA_ULTIMA_ACTUALIZACION, LEY_FE_ARTICULO, LEY_FE_VINCULACION, ID_ARCHIVO_PROGRAMA FROM Ley_Fed_Est_Proyecto WHERE FOLIO_PROYECTO =:folio AND SERIAL_PROYECTO =:serial");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    
    
    //Metodo para otras dispocisiones
    @SuppressWarnings("unchecked")
	public List<DisposicionProyecto> dispo(String folio, Short serial) {
    	Query q = emfMIAE.createQuery("SELECT e FROM DisposicionProyecto e WHERE e.disposicionProyectoPK.folioProyecto = :folio and e.disposicionProyectoPK.serialProyecto = :serial order by e.disposicionProyectoPK.disposicionProyId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    
    //Metodo para convenios
	@SuppressWarnings("unchecked")
	public List<ConvenioProy> conv(String folio, Short serial) {
		Query q = emfMIAE.createQuery("SELECT e FROM ConvenioProy e WHERE e.convenioProyPK.folioProyecto = :folio and e.convenioProyPK.serialProyecto = :serial");
		q.setParameter("serial", serial);
		q.setParameter("folio", folio);
		return q.getResultList();
	}
    
    //Metodo para 	POET
    @SuppressWarnings("unchecked")
	public List<Object> poet(String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT POETM_PROYECTO.POETM_ID, POETM_PROYECTO.POETM_TIPO, POETM_PROYECTO.POETM_NOMBRE_INSTRUMENTO, POETM_PROYECTO.POETM_NOMBRE_UGA, POETM_PROYECTO.POETM_POLITICA_AMBIENTAL, POETM_PROYECTO.POETM_USO_PREDOMINANTE, POETM_PROYECTO.POETM_CRITERIO, CASE POETM_COMPATIBLE WHEN 'S' THEN 'SI' ELSE 'NO' END AS POETM_COMPATIBLE, POETM_VINCULACION, ARCHIVOS_PROYECTO.SEQ_ID FROM POETM_PROYECTO LEFT JOIN ARCHIVOS_PROYECTO ON POETM_PROYECTO.ID_ARCHIVO_PROYECTO = ARCHIVOS_PROYECTO.SEQ_ID WHERE POETM_PROYECTO.FOLIO_PROYECTO =:folio AND POETM_PROYECTO.SERIAL_PROYECTO =:serial ORDER BY POETM_PROYECTO.POETM_ID ASC");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    //Metodo para 	ANP
    @SuppressWarnings("unchecked")
	public List<Object> anp(String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT ANP_ID, ANP_TIPO, ANP_NOMBRE, ANP_CATEGORIA_MANEJO, ANP_FECHA_MANEJO, ANP_ULTIMO_DECRETO, ID_ARCHIVO_PROGRAMA FROM ANP_PROYECTO  WHERE FOLIO_PROYECTO =:folio AND SERIAL_PROYECTO =:serial");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    //Metodo para 	PDU
    @SuppressWarnings("unchecked")
	public List<Object> pdu(String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT PDUM_ID, PDUM_NOMBRE, PDUM_USOS, PDUM_CLAVE_USOS, PDUM_COS, PDUM_CUS, PDUM_VINCULACION, ID_ARCHIVO_PROGRAMA FROM PDUM_PROYECTO WHERE FOLIO_PROYECTO =:folio AND SERIAL_PROYECTO=:serial ");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    
    
    
    //Metodo para uso de Explosivos    
    @SuppressWarnings("unchecked")
	public List<Object> explosivos(String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT A.ETAPA_ID, C.ETAPA_DESCRIPCION, A.EXPLOSIVO_ETAPA_ID, B.ACTIVIDAD_NOMBRE, A.ACTIVIDAD_JUSTIFICACION, A.ACTIVIDAD_METODOLOGIA FROM EXPLOSIVO_ACTIVIDAD_ETAPA A, ACTIVIDAD_ETAPA B, CAT_ETAPA C WHERE A.ACTIVIDAD_ETAPA_ID = B.ACTIVIDAD_ETAPA_ID AND A.ETAPA_ID = B.ETAPA_ID AND A.FOLIO_PROYECTO  = B.FOLIO_PROYECTO AND A.SERIAL_PROYECTO = B.SERIAL_PROYECTO AND A.ETAPA_ID = C.ETAPA_ID AND A.FOLIO_PROYECTO=:folio AND A.SERIAL_PROYECTO=:serial ORDER BY A.ETAPA_ID, A.EXPLOSIVO_ETAPA_ID");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }
    
    //Metodo para sacar la lista de valores
    
    @SuppressWarnings("unchecked")
	public List<Object> Predios(String folio, Short serial)
    {
    	 Query q = emfMIAE.createNativeQuery("SELECT P.PREDIO_COLIN_ID, P.PREDIO_NOMBRE, C.CLASIFICACION_DESCRIPCION, C2.CLASIFICACION_B, R.REFERENCIA_DESCRIPCION, P.PREDIO_DESCRIPCION FROM PREDIOCOLIN_PROY  P, CAT_CLASIFICACION C, CAT_CLASIFICACION_B C2, CAT_REFERENCIA R WHERE P.CLASIFICACION_ID = C.CLASIFICACION_ID AND P.CLASIFICACION_B_ID = C2.CLASIFICACION_B_ID AND P.REFERENCIA_ID = R.REFERENCIA_ID AND P.FOLIO_PROYECTO =:folio AND P.SERIAL_PROYECTO =:serial ORDER BY P.PREDIO_COLIN_ID");
    	 q.setParameter("serial", serial);
         q.setParameter("folio", folio);
         @SuppressWarnings("rawtypes")
		List l = q.getResultList();
         return  l;
    }

    @SuppressWarnings("unchecked")
	public List<ActividadEtapa> getActividadesEtapa(String folio, Short serial, Short etapaId) {
        Query q = emfMIAE.createQuery("SELECT e FROM ActividadEtapa e WHERE e.actividadEtapaPK.folioProyecto = :folio and e.actividadEtapaPK.serialProyecto = :serial and e.actividadEtapaPK.etapaId = :etapaId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("etapaId", etapaId);
        return q.getResultList();
    }
    
    /**
     * Recupera las Actividades asociadas a una Etapa durante la captura del tramite
     * @param folio Numero asignado por SINATEC al tramite
     * @param serial [1] indica si es ingreso y [3] indica que Evaluador solicitó informacion adicional
     * @param etapa Id de la etapa de la que se desean consultar las actividades
     * @return Lista de actividades asociadas a la etapa especificada
     */
    @SuppressWarnings("unchecked")
	public List<Object> actividadEtapa(String folio, Short serial, Short etapa)
    {
    	Query q = emfMIAE.createNativeQuery("SELECT ACTIVIDAD_ETAPA_ID, ACTIVIDAD_NOMBRE, dbms_lob.substr(ACTIVIDAD_DESCRIPCION, 1000, 1 ) FROM ACTIVIDAD_ETAPA WHERE FOLIO_PROYECTO =:folio AND SERIAL_PROYECTO =:serial AND ETAPA_ID =:etapa");
    	q.setParameter("folio", folio);
    	q.setParameter("serial", serial);
    	q.setParameter("etapa", etapa);
    	@SuppressWarnings("rawtypes")
		List l = q.getResultList();
    	return l;
    }
    
    //Metodo para Consultar los archivos
	public List<ArchivosProyecto> archivosProyecto(short capituloId, short subcapituloId, short seccionId,
			short apartadoId, String folioProyecto, short serialProyecto) {
		try {
			// Query q = emfMIAE.createNativeQuery("SELECT NOMBRE, DESCRIPCION, SEQ_ID FROM ARCHIVOS_PROYECTO WHERE FOLIO_SERIAL =:folio AND SERIAL_PROYECTO =:serial AND CAPITULO_ID =:capitulo AND SUBCAPITULO_ID =:subcapitulo AND SECCION_ID=:seccion AND APARTADO_ID =:apartado");
			Query q = emfMIAE.createQuery("FROM ArchivosProyecto e WHERE e.folioSerial = :folio and e.serialProyecto = :serial and e.capituloId = :capitulo and e.subCapituloId = :subcapitulo and e.seccionId = :seccion and e.apartadoId = :apartado order by e.id");
			q.setParameter("apartado", apartadoId);
			q.setParameter("capitulo", capituloId);
			q.setParameter("folio", folioProyecto);
			q.setParameter("serial", serialProyecto);
			q.setParameter("seccion", seccionId);
			q.setParameter("subcapitulo", subcapituloId);
			@SuppressWarnings("unchecked")
			List<ArchivosProyecto> archivosProyecto = q.getResultList();

			return archivosProyecto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
    
	
	public List<CuerposAguaProyecto> getCuerposAgua(String folioProyecto, short serialProyecto) {
        Query q = emfMIAE.createQuery("SELECT cap FROM CuerposAguaProyecto cap WHERE  cap.cuerposAguaProyectoPK.folioProyecto = :folioProyecto and cap.cuerposAguaProyectoPK.serialProyecto = :serialProyecto");        
        q.setParameter("folioProyecto", folioProyecto);
        q.setParameter("serialProyecto", serialProyecto);        
        @SuppressWarnings("unchecked")
		List<CuerposAguaProyecto> listaCuerposAgua = q.getResultList();
        return listaCuerposAgua;
	}
	
	
    
    @SuppressWarnings("unchecked")
	public List<AnexosProyecto> getAnexos(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        @SuppressWarnings("rawtypes")
		List<AnexosProyecto> r = new ArrayList();
        try {
            if (capituloId != 0) {
                //EntityManager em = emf.createEntityManager();
                Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
                q.setParameter("apartado", apartadoId);
                q.setParameter("capitulo", capituloId);
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("seccion", seccionId);
                q.setParameter("subcapitulo", subcapituloId);
                r = q.getResultList();
                //em.close();
            } else {
                //obtiene todos los adjuntos del folio
                //EntityManager em = emf.createEntityManager();
                Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                r = q.getResultList();
                //em.close();
            }
        } catch (Exception e) {

        }

        return r;

    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object[]> sustanciaProy(String numFolio, short serial) 
    {
        Query q = emfMIAE.createNativeQuery("SELECT SEQ_ID, SUST_PROY_ID, ETAPA_DESCRIPCION, SUSTANCIA_DESCRIPCION, SUSTANCIA_CANTIDAD_ALMACENADA || ' ' || CTUN_DESC AS CANTIDAD_UNIDAD, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO FROM (SELECT B.ETAPA_ID, FOLIO_PROYECTO, SERIAL_PROYECTO, SUST_PROY_ID, ID_ARCHIVO_PROGRAMA, ETAPA_DESCRIPCION, CASE A.SUSTANCIA_ID WHEN 9999 THEN A.SUSTANCIA_PROMOVENTE ELSE C.SUSTANCIA_DESCRIPCION END AS SUSTANCIA_DESCRIPCION, SUSTANCIA_CANTIDAD_ALMACENADA, (SELECT CTUN_DESC FROM CATALOGOS.CAT_UNIDAD_MEDIDA D WHERE C.SUSTANCIA_UNIDAD = D.CTUN_CLVE) AS CTUN_DESC FROM SUSTANCIA_PROYECTO A, CAT_ETAPA B, CAT_SUSTANCIA_ALTAM_RIESGOSA C WHERE A.ETAPA_ID = B.ETAPA_ID AND A.SUSTANCIA_ID = C.SUSTANCIA_ID AND A.FOLIO_PROYECTO =:folio AND A.SERIAL_PROYECTO =:serial ) SUST LEFT JOIN ARCHIVOS_PROYECTO AP ON SUST.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID ORDER BY SUST.ETAPA_ID, SUST_PROY_ID");
        q.setParameter("folio", numFolio);
        q.setParameter("serial", serial);
        List l = q.getResultList();
        return l;
    }
    
    @SuppressWarnings("unchecked")
	public List<PdumProyecto> getPdumProyecto(String folio, Short serial) {
        Query q = em.createQuery("SELECT e FROM PdumProyecto e  WHERE e.pdumProyectoPK.folioProyecto = :folio and e.pdumProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<InstrUrbanos> getInstrUbr(String folio, Short serial) {
        Query q = em.createQuery("SELECT a FROM InstrUrbanos a  WHERE a.instrUrbanosPK.numFolio = :fol and a.instrUrbanosPK.version = :clveProy");
        q.setParameter("fol", folio);
        q.setParameter("clveProy", serial);
        return q.getResultList();
    }
    
    public Short getMaxPdumProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = em.createQuery("SELECT max(e.pdumProyectoPK.pdumId) FROM PdumProyecto e  WHERE e.pdumProyectoPK.folioProyecto = :folio and e.pdumProyectoPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 0;
        }

        return id;
    }
    
    //<editor-fold desc="Consulta con cláusula where" defaultstate="collapsed">
    /**
     * Consulta con cláusula where
     *
     * @param tipo
     * @param atributeName
     * @param o
     * @return
     */
    public List<?> listado_where(Class<?> tipo, String atributeName, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName), o));
        return em.createQuery(cq).getResultList();
    }//</editor-fold>

    //<editor-fold desc="Consulta con cláusula WHERE y LIKE" defaultstate="collapsed">
    /**
     * Consulta con cláusula WHERE y LIKE
     *
     * @param tipo
     * @param atributeName
     * @param o
     * @return
     */
    public List<?> listado_like(Class<?> tipo, String atributeName, Object o) {

        System.out.println("tipo: " + tipo.getName());
        System.out.println("att: " + atributeName);
        System.out.println("obj: " + o.getClass().getName() + " " + o.toString());

        Query q = em.createQuery("SELECT o FROM " + tipo.getName() + " o WHERE UPPER(o." + atributeName + ") like '" + o + "' ");

        return q.getResultList();
    }//</editor-fold>

    //<editor-fold desc="Búsqueda por columna diferente al Id, devuelve un solo resultado" defaultstate="collapsed">
    /**
     * Búsqueda por columna diferente al Id, devuelve un solo resultado
     *
     * @param tipo
     * @param atributeName
     * @param o
     * @return
     */
    public Object objeto_where(Class<?> tipo, String atributeName, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName), o));
        return em.createQuery(cq).getSingleResult();
    }//</editor-fold>

    //<editor-fold desc="Busqueda de los datos del promovente" defaultstate="collapsed">
    /**
     *
     * @param idTramite
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<Vexdatosusuario> datosPromovente(Integer idTramite) {
        Query q = em.createQuery("SELECT DISTINCT a  FROM Vexdatosusuario a WHERE a.bgtramiteid=:idTramite");
        q.setParameter("idTramite", idTramite);
        @SuppressWarnings("rawtypes")
		List l = q.getResultList();
        return l;
    }//</editor-fold>

    @SuppressWarnings("unchecked")
	public List<Vexdatosusuariorep> datosPrepLeg(Integer idTramite) {
        Query q = em.createQuery("SELECT DISTINCT a FROM Vexdatosusuariorep a WHERE a.bgtramiteid = :idTramite");
        q.setParameter("idTramite", idTramite);
        @SuppressWarnings("rawtypes")
		List l = q.getResultList();

        return l;
    }
    
    //<editor-fold desc="Obtiene representante legal por CURP" defaultstate="collapsed">
    /**
     *
     * @param curp
     * @param id
     * @return
     */
    public Vexdatosusuariorep repLegPorCurp(String curp, Integer id) {
        Query q = em.createQuery("SELECT v FROM Vexdatosusuariorep v WHERE v.vccurp = :vccurp AND v.bgtramiteid=:id");
        q.setParameter("vccurp", curp);
        q.setParameter("id", id);
        //return (Vexdatosusuariorep) q.getSingleResult();

        Vexdatosusuariorep p = new Vexdatosusuariorep();
        try {
            p = (Vexdatosusuariorep) q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;

    }//</editor-fold>

    public RepLegalProyecto getRepLegalCurp(String folio, short serial, String curp) {
        Query q = em.createQuery("SELECT r FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.folioProyecto = :folio and r.repLegalProyectoPK.serialProyecto = :serial and r.repLegalProyectoPK.representanteCurp = :curp");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("curp", curp);
        return (RepLegalProyecto) q.getResultList().get(0);
    }
    
    public List<Object[]> critValProy(BigInteger idC, String bitaproy) {
        Query q = em.createQuery("SELECT c.descripcionValor, v.criterioValorEvaluacion, v.criterioObservacionEva, v.criterioValorUsuario FROM CriterioValoresProyecto v, CatDerechosCriteriosvalores c WHERE v.criterioValoresProyectoPK.idCriterio =:idCrit AND v.bitacoraProyecto =:proyBita and v.criterioValorEvaluacion = c.catDerechosCriteriosvaloresPK.idValor and v.criterioValoresProyectoPK.idCriterio = c.catDerechosCriteriosvaloresPK.idCriterio");
        q.setParameter("idCrit", idC);
        q.setParameter("proyBita", bitaproy);
        @SuppressWarnings("unchecked")
		List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> critValProyEva(String folio, short serial) {
        Query q = em.createNativeQuery("SELECT SUM(V.CRITERIO_VALOR_EVALUACION)  FROM Criterio_Valores_Proyecto v WHERE V.FOLIO_PROYECTO ='" + folio + "' and V.SERIAL_PROYECTO =" + serial);
        //q.setParameter("folioP", folio);
        //q.setParameter("serialP", serial);
        @SuppressWarnings("unchecked")
		List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> critValProy3(BigInteger idC, String folioproy, short serial) {
        Query q = em.createQuery("SELECT c.descripcionValor, v.criterioValorEvaluacion, v.criterioObservacionEva, v.criterioValorUsuario, c.descripcionValor "
                + "FROM CriterioValoresProyecto v, CatDerechosCriteriosvalores c "
                + "WHERE v.criterioValoresProyectoPK.idCriterio =:idCrit "
                + "AND v.criterioValoresProyectoPK.folioProyecto =:folio "
                + "AND v.criterioValoresProyectoPK.serialProyecto =:serial "
                + "and v.criterioValorEvaluacion = c.catDerechosCriteriosvaloresPK.idValor "
                + "and v.criterioValoresProyectoPK.idCriterio = c.catDerechosCriteriosvaloresPK.idCriterio "
        );

        q.setParameter("idCrit", idC);
        q.setParameter("folio", folioproy);
        q.setParameter("serial", serial);
        @SuppressWarnings("unchecked")
		List<Object[]> l = q.getResultList();
        return l;
    }
    
    public List<Object[]> criteriosValoresProyecto(String folioproy, short serial) {
        Query q = em.createNativeQuery(
		"SELECT CVP2.*, C2.DESCRIPCION_VALOR AS DESC_EVA FROM CAT_DERECHOS_CRITERIOSVALORES C2 RIGHT JOIN " +
		"( " +
		"    SELECT CVP.*, C1.DESCRIPCION_VALOR AS DESC_USUARIO FROM CAT_DERECHOS_CRITERIOSVALORES C1 RIGHT JOIN  " +
		"    (SELECT TO_CHAR(FOLIO_PROYECTO), SERIAL_PROYECTO, BITACORA_PROYECTO, ID_CRITERIO, ID_VALOR, CRITERIO_VALOR_USUARIO, CRITERIO_VALOR_EVALUACION, CRITERIO_OBSERVACION_EVA " +
                "       FROM CRITERIO_VALORES_PROYECTO WHERE FOLIO_PROYECTO = :folio AND SERIAL_PROYECTO = :serial) CVP " +
		"    ON CVP.ID_CRITERIO = C1.ID_CRITERIO " +
		"      AND CVP.CRITERIO_VALOR_USUARIO = C1.VALOR " +
		") CVP2  " +
		" ON CVP2.ID_CRITERIO = C2.ID_CRITERIO " +
		"AND CVP2.CRITERIO_VALOR_EVALUACION = C2.VALOR " +
                "order by cvp2.id_criterio"        
        );

        q.setParameter("folio", folioproy);
        q.setParameter("serial", serial);
        @SuppressWarnings("unchecked")
		List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> critValProy2(BigInteger idC, String bitaproy) {
        Query q = em.createQuery("SELECT c.descripcionValor, c.catDerechosCriteriosvaloresPK.idValor, v.criterioObservacionEva, v.criterioValorUsuario FROM CriterioValoresProyecto v, CatDerechosCriteriosvalores c WHERE v.criterioValoresProyectoPK.idCriterio =:idCrit AND v.bitacoraProyecto =:proyBita and v.criterioValoresProyectoPK.idValor = c.catDerechosCriteriosvaloresPK.idValor and v.criterioValoresProyectoPK.idCriterio = c.catDerechosCriteriosvaloresPK.idCriterio");
        q.setParameter("idCrit", idC);
        q.setParameter("proyBita", bitaproy);
        @SuppressWarnings("unchecked")
		List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> critValProy4(BigInteger idC, String folioproy, short serial) {
        Query q = em.createQuery("SELECT c.descripcionValor, c.catDerechosCriteriosvaloresPK.idValor, v.criterioObservacionEva, v.criterioValorUsuario, c.descripcionValor "
                + "FROM CriterioValoresProyecto v, CatDerechosCriteriosvalores c "
                + "WHERE v.criterioValoresProyectoPK.idCriterio =:idCrit "
                + "AND v.criterioValoresProyectoPK.folioProyecto =:folio "
                + "AND v.criterioValoresProyectoPK.serialProyecto =:serial "
                + "and v.criterioValoresProyectoPK.idValor = c.catDerechosCriteriosvaloresPK.idValor "
                + "and v.criterioValoresProyectoPK.idCriterio = c.catDerechosCriteriosvaloresPK.idCriterio "
        );
        q.setParameter("idCrit", idC);
        q.setParameter("folio", folioproy);
        q.setParameter("serial", serial);
        @SuppressWarnings("unchecked")
		List<Object[]> l = q.getResultList();
        return l;
    }

    @SuppressWarnings("unchecked")
	public List<Microcuenca> getMicrocuenca(String folio, Short serial) {
        Query q = em.createQuery("SELECT e FROM Microcuenca e  WHERE e.microcuencaPK.numFolio = :folio and e.microcuencaPK.version = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getAcuiferos2(String folio, Short serial, String cveProy) {
        Query q = em.createQuery("SELECT e.clvAcui, e.nomAcui, e.descDispo, e.fechaDof, e.sobreexp FROM Acuiferos e  WHERE e.acuiferosPK.numFolio = :folio and e.acuiferosPK.version = :serial group by e.clvAcui, e.nomAcui, e.descDispo, e.fechaDof, e.sobreexp");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }

    @SuppressWarnings("unchecked")
	public List<Object[]> usoSueloVegetacion2(String Folio, String ClveProy, short version) {
        Query q = em.createQuery("SELECT a.comp, a.descrip, a.tipEcov, a.tipoGen ,a.faseVs, a.area, a.supEa FROM UsoSueloVeget a  WHERE a.usoSueloVegetPK.numFolio = :fol and a.usoSueloVegetPK.cveProy = :clveProy and a.usoSueloVegetPK.version = :versions order by a.supEa ");
        q.setParameter("fol", Folio);
        q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        return q.getResultList();
    }

    public Short getMaxSueloVegetacionProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = em.createQuery("SELECT max(a.sueloVegetacionProyectoPK.sueloVegProyId) FROM SueloVegetacionProyecto a where a.sueloVegetacionProyectoPK.folioProyecto = :folio and a.sueloVegetacionProyectoPK.serialProyecto = :serial ");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 0;
        }

        return id;
    }

    /*
    public List<Object> getSueloVegetacionProyecto(String folio, Short serial) {
        Query q = emfMIAE.createNativeQuery("SELECT SUELO_VEG_PROY_ID, SUELO_COMPONENTE, SUELO_DESCRIPCION, SUELO_TIPO_ECOV, SUELO_TIPO_GEN, SUELO_FASE_VS, SUELO_AREA_SIGEIA, CASE  SUELO_VALIDA_RESULTADO WHEN 'S' THEN 'SI' ELSE 'NO' END AS  SUELO_VALIDA_RESULTADO, SUELO_CAT_USO, SUELO_DIAGNOSTICO  FROM SUELO_VEGETACION_PROYECTO WHERE FOLIO_PROYECTO =:folio AND SERIAL_PROYECTO =:serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        List l = q.getResultList();
        return l;
    }
    */
    
    @SuppressWarnings("unchecked")
	public List<SueloVegetacionProyecto> getSueloVegetacionProyecto(String folio, Short serial) {
        Query q = emfMIAE.createQuery("SELECT e FROM SueloVegetacionProyecto e  WHERE e.sueloVegetacionProyectoPK.folioProyecto = :folio and e.sueloVegetacionProyectoPK.serialProyecto = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    @SuppressWarnings("unchecked")
	public List<FloraProyecto> getFloraProyecto(String folio, Short serial) {
        Query q = emfMIAE.createQuery("SELECT e FROM FloraProyecto e  WHERE e.floraProyectoPK.folioProyecto = :folio and e.floraProyectoPK.serialProyecto = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    
    @SuppressWarnings("unchecked")
	public List<FaunaProyecto> getFaunaProyecto(String folio, Short serial) {
    	Query q = emfMIAE.createQuery("SELECT e FROM FaunaProyecto e  WHERE e.faunaProyectoPK.folioProyecto = :folio and e.faunaProyectoPK.serialProyecto = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<Object> EstudiosEsp(String folio, Short serial) {
    	System.out.println("ESTE ES EL SERIAL " + serial);
    	 Query q = emfMIAE.createNativeQuery("SELECT ESTUDIO_DESCRIPCION, ANEXO_NOMBRE, EE.ESTUDIO_ID AS ESTUDIO_ID FROM ESTUDIOS_ESP_PROY EE, CAT_EST_ESP CT WHERE EE.ESTUDIO_ESPECIAL = CT.ESTUDIO_ID  AND EE.FOLIO_SERIAL =:folio AND EE.SERIAL_PROYECTO =:serial");
         q.setParameter("folio", "folio");
         q.setParameter("serial", serial);
         @SuppressWarnings("unchecked")
		List<Object> l = q.getResultList();
         return l;
    }

    
    public String getInformacionAdicional(short capitulo, short subCapitulo, short seccion, short apartado, String bproy) {
        String ret = "0";
        
        Query q = em.createQuery("SELECT e.infoAdicional, e.secModificada FROM ComentarioProyecto e  WHERE e.comentarioProyectoPK.capituloId = :cap and e.comentarioProyectoPK.subcapituloId = :sub and e.comentarioProyectoPK.seccionId = :sec and e.comentarioProyectoPK.apartadoId = :apa and e.comentarioProyectoPK.bitacoraProyecto = :bp");
        q.setParameter("cap", capitulo);
        q.setParameter("sub", subCapitulo);
        q.setParameter("sec", seccion);
        q.setParameter("apa", apartado);
        q.setParameter("bp", bproy);
        try {
            Object[] c = (Object[]) q.getSingleResult();
            
            System.out.println("Comentario Proyecto " + c);
            System.out.println("info ad " + c[0]);
            System.out.println("sec mod" + c[1]);
            
            if (c[0] != null && c[0].equals("1")) {
                ret = "1";
            }

            if (c[1] != null && c[1].equals("1")) {
                ret = "2";
            }

        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obtención de la información adicional [INFOADICIONAL] " + e.toString());
            e.printStackTrace();
            ret = "0";
        }

        return ret;
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> docsRespDgira(String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc) {
        Query q = em.createNativeQuery("SELECT BITACORA_PROYECTO, CLAVE_TRAMITE, ID_TRAMITE, TIPO_DOC_ID, ID_ENTIDAD_FEDERATIVA, DOCUMENTO_ID, DOCUMENTO_DGIRA, DOCUMENTO_NOMBRE, DOCUMENTO_URL, DOCUMENTO_UBICACION FROM DOCUMENTO_RESPUESTA_DGIRA "
                + "WHERE BITACORA_PROYECTO = '" + numBita + "' AND CLAVE_TRAMITE = '" + clveTram + "' "
                + "AND ID_TRAMITE = "+ idTram + " AND TIPO_DOC_ID = " + tipoDoc + "  AND ID_ENTIDAD_FEDERATIVA = '" + entFed + "' "
                + " AND DOCUMENTO_ID = '" + idDoc + "'");
        
        List<Object[]> l = q.getResultList();
        return l;        
    }
    
    @SuppressWarnings("unchecked")
	public List<CatEstatusDocumento> getCatStatusDocs() {
        Query q = em.createQuery("SELECT c FROM CatEstatusDocumento c where c.estatusDocumentoId in (2,4)");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<CatEstatusDocumento> getCatStatusDocs2() {
        Query q = em.createQuery("SELECT c FROM CatEstatusDocumento c where c.estatusDocumentoId != 1 and c.estatusDocumentoId != 4 ");
        return q.getResultList();
    }//DocumentoDgiraFlujoHist  DocumentoDgiraFlujo
    
    @SuppressWarnings("unchecked")
	public List<CatEstatusDocumento> getCatStatusDocs3() {
        Query q = em.createQuery("SELECT c FROM CatEstatusDocumento c where c.estatusDocumentoId = 3 ");
        return q.getResultList();
    }
    
    /***
     * Regresa un objeto DocumentoDgiraFlujo
     * @param numBita
     * @param clveTram
     * @param idTram
     * @param tipoDoc
     * @param entFed
     * @param idDoc
     * @return
     */
    public DocumentoDgiraFlujo docsFlujoDgira(String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc) {
        Query q = em.createQuery("SELECT a FROM DocumentoDgiraFlujo a WHERE a.documentoDgiraFlujoPK.bitacoraProyecto=:bita and a.documentoDgiraFlujoPK.claveTramite=:clveTram "
        		+ "and a.documentoDgiraFlujoPK.idTramite=:idTram and a.documentoDgiraFlujoPK.tipoDocId=:idTipDoc "
        		+ "and a.documentoDgiraFlujoPK.idEntidadFederativa=:entFed and a.documentoDgiraFlujoPK.documentoId=:idDoc");
        q.setParameter("bita", numBita);
        q.setParameter("clveTram", clveTram);
        q.setParameter("idTram", idTram);
        q.setParameter("idTipDoc", tipoDoc);
        q.setParameter("entFed", entFed);
        q.setParameter("idDoc", idDoc); 

        try {
            return (DocumentoDgiraFlujo) q.getSingleResult();
        } catch (Exception e) {
        	System.out.println("No se encontro la entidad, por lo tanto se regresara una vacia.");
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return new DocumentoDgiraFlujo();
    }
    
    public DocumentoDgiraFlujoHist docsFlujoDgiraHisto(String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc, short idDocHisto) {
        Query q = em.createQuery("SELECT a FROM DocumentoDgiraFlujoHist a WHERE a.documentoDgiraFlujoHistPK.bitacoraProyecto=:bita and a.documentoDgiraFlujoHistPK.claveTramite=:clveTram and a.documentoDgiraFlujoHistPK.idTramite=:idTram and a.documentoDgiraFlujoHistPK.tipoDocId=:idTipDoc and a.documentoDgiraFlujoHistPK.idEntidadFederativa=:entFed and a.documentoDgiraFlujoHistPK.documentoId=:idDoc and a.documentoDgiraFlujoHistPK.documentoDgiraIdHist=:idDocHisto");
        q.setParameter("bita", numBita);
        q.setParameter("clveTram", clveTram);
        q.setParameter("idTram", idTram);
        q.setParameter("idTipDoc", tipoDoc);
        q.setParameter("entFed", entFed);
        q.setParameter("idDoc", idDoc); 
        q.setParameter("idDocHisto", idDocHisto); 

        try {
            return (DocumentoDgiraFlujoHist) q.getSingleResult();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return new DocumentoDgiraFlujoHist();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> maxDocsDgiraFlujoHist(String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc) {
        Query q = em.createNativeQuery("SELECT  CASE WHEN (MAX(A.DOCUMENTO_DGIRA_ID_HIST) is null)  then 0 ELSE (MAX(A.DOCUMENTO_DGIRA_ID_HIST)) END " +
        "FROM DOCUMENTO_DGIRA_FLUJO_HIST A " +
        "WHERE A.BITACORA_PROYECTO = '" + numBita + "' and A.CLAVE_TRAMITE ='" + clveTram + "' " +
        "and A.ID_TRAMITE = " + idTram + " and A.TIPO_DOC_ID = " + tipoDoc  +
        "and A.ID_ENTIDAD_FEDERATIVA = '" + entFed + "' and A.DOCUMENTO_ID = '" + idDoc + "' ");
        return q.getResultList();
    }
    
    public CatEstatusDocumento idCatEstatusDoc(short id) {
        Query q = em.createQuery("SELECT a FROM CatEstatusDocumento a WHERE a.estatusDocumentoId=:idDoc");
        q.setParameter("idDoc", id); 

        try {
            return (CatEstatusDocumento) q.getSingleResult();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return new CatEstatusDocumento();
    }  //
    
    @SuppressWarnings("unchecked")
	public List<Object[]> docsRespDgiraBusq(String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc) {
        Query q = em.createNativeQuery("SELECT A.DOCUMENTO_URL  FROM DOCUMENTO_RESPUESTA_DGIRA A " +
        "WHERE A.BITACORA_PROYECTO = '" + numBita + "' and A.CLAVE_TRAMITE = '" + clveTram + "' and " +
        "A.ID_TRAMITE = " + idTram + " and A.TIPO_DOC_ID = " + tipoDoc + " and " +
        "A.ID_ENTIDAD_FEDERATIVA = '" + entFed + "' and A.DOCUMENTO_ID = '" + idDoc + "' ");

        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
    /***
     * Regresa una lista de DocumentoRespuestaDgira
     * @param numBita
     * @param clveTram
     * @param idTram
     * @param tipoDoc
     * @param entFed
     * @param idDoc
     * @return
     */
	public List<DocumentoRespuestaDgira> docsRespDgiraBusq2(String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc) {
        Query q = em.createQuery("SELECT A  FROM DocumentoRespuestaDgira A WHERE A.documentoRespuestaDgiraPK.bitacoraProyecto=:numBitaV  and A.documentoRespuestaDgiraPK.claveTramite=:clveTramV  and A.documentoRespuestaDgiraPK.idTramite=:idTramV and A.documentoRespuestaDgiraPK.tipoDocId=:tipoDocV and A.documentoRespuestaDgiraPK.idEntidadFederativa=:entFedV and A.documentoRespuestaDgiraPK.documentoId =:idDocV ");
        q.setParameter("numBitaV", numBita);
        q.setParameter("clveTramV", clveTram);
        q.setParameter("idTramV", idTram);
        q.setParameter("tipoDocV", tipoDoc);
        q.setParameter("entFedV", entFed);
        q.setParameter("idDocV", idDoc); 
        @SuppressWarnings("rawtypes")
		List l = q.getResultList();
        return l;
    }
    

    
    @SuppressWarnings("unchecked")
    /***
     * Regresa el ESTATUS_DOCUMENTO_ID_HIST de DOCUMENTO_DGIRA_FLUJO_HIST
     * @param numBita
     * @param clveTram
     * @param idTram
     * @param tipoDoc
     * @param entFed
     * @param idDoc
     * @param idDocHisto
     * @return
     */
	public List<Object[]> statusDocsRespDgiraBusq(String numBita, String clveTram, int idTram, short tipoDoc, String entFed, String idDoc, short idDocHisto) {
        Query q = em.createNativeQuery("SELECT A.ESTATUS_DOCUMENTO_ID_HIST  FROM DOCUMENTO_DGIRA_FLUJO_HIST A " +
        "WHERE A.BITACORA_PROYECTO = '" + numBita + "' and A.CLAVE_TRAMITE = '" + clveTram + "' and " +
        "A.ID_TRAMITE = " + idTram + " and A.TIPO_DOC_ID = " + tipoDoc + " and " +
        "A.ID_ENTIDAD_FEDERATIVA = '" + entFed + "' and A.DOCUMENTO_ID = '" + idDoc + "' " +
        "ORDER BY A.DOCUMENTO_DGIRA_ID_HIST");       

        return q.getResultList();
    }
    
    public EstudioRiesgoProyecto cargaEstudioRiesgo(Proyecto proyecto) {
        EstudioRiesgoProyecto e = null;

        System.out.println("Folio " + proyecto.getProyectoPK().getFolioProyecto());
        System.out.println("Serial " + proyecto.getProyectoPK().getFolioProyecto());

        try {
            Query q = em.createQuery("SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRiesgoProyectoPK.folioProyecto = :folio and e.estudioRiesgoProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
            q.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
            e = (EstudioRiesgoProyecto) q.getSingleResult();
        } catch (NoResultException err) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        }

        if (e == null) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

        }

        return e;
    }
    
    public EstudioRiesgoProyecto estudioRiesgo(String folio, Short serial)
    {
    	Query q = emfMIAE.createQuery("SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRiesgoProyectoPK.folioProyecto = :folio AND e.estudioRiesgoProyectoPK.serialProyecto = :serial");
    	q.setParameter("folio",folio);
    	q.setParameter("serial", serial);    
    	return (EstudioRiesgoProyecto) q.getSingleResult();
    }
    
    
    @SuppressWarnings("unchecked")
	public List<SustanciaProyecto> getSustancias(String folio, Short serial) {
        try {
            Query q = em.createQuery("SELECT e FROM SustanciaProyecto e WHERE e.sustanciaProyectoPK.folioProyecto = :folio and e.sustanciaProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<SustanciaProyecto>();
        }                   
    }
    
    @SuppressWarnings("unchecked")
	public List<PoetmProyecto> getPoetmProyecto(String folio, Short serial) {
        try {
            Query q = em.createQuery("SELECT e FROM PoetmProyecto e  WHERE e.poetmProyectoPK.folioProyecto = :folio and e.poetmProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<PoetmProyecto>();
        }             
    }
    
    //================================================================================================================================================
    //Metodo para hacer consukta de reglamentos
    /*@
     * Recibe el folio y  el serial te retorna una lista de los valores seleccionados
     * 
     */
    @SuppressWarnings("unchecked")
	public List<ReglamentoProyecto> reglamentos(String folio, Short serial) {
        try {
            Query q = em.createQuery("SELECT r FROM ReglamentoProyecto r  WHERE r.reglamentoProyectoPK.folioProyecto = :folio and r.reglamentoProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<ReglamentoProyecto>();
        }             
    }
    
    
    @SuppressWarnings("unchecked")
	public List<AnpProyecto> consultaANP(String folio, Short serial) {
        try {
            Query q = em.createQuery("SELECT a FROM AnpProyecto a WHERE a.anpProyectoPK.folioProyecto = :folio and a.anpProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<AnpProyecto>();
        }             
    }
    
    
    //================================================================================================================================================    
    
    public List<OeRegionales1> getOeReg1(String folio, String clveProy, String cveArea, short version) {
        try {
            Query q = em.createQuery("SELECT e FROM OeRegionales1 e WHERE e.oeRegionales1PK.numFolio = :folio and e.oeRegionales1PK.cveProy = :cveProy and e.oeRegionales1PK.version = :version");
            q.setParameter("folio", folio);
            q.setParameter("cveProy", clveProy);
            q.setParameter("version", version);
            @SuppressWarnings("unchecked")
			List<OeRegionales1> lstRes = q.getResultList();
            
            return lstRes;
            
        } catch (Exception e) {
            return new ArrayList<OeRegionales1>();
        }
        
    }
    
    @SuppressWarnings("unchecked")
	public List<OeRegionales2> getOeReg2(String folio, String clveProy, String cveArea, short version) {
        try {
            Query q = em.createQuery("SELECT e FROM OeRegionales2 e WHERE e.oeRegionales2PK.numFolio = :folio and e.oeRegionales2PK.cveProy = :cveProy and e.oeRegionales2PK.version = :version");
            q.setParameter("folio", folio);
            q.setParameter("cveProy", clveProy);
            q.setParameter("version", version);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<OeRegionales2>();
        }            
    }

    @SuppressWarnings("unchecked")
	public List<OeRegionales3> getOeReg3(String folio, String clveProy, String cveArea, short version) {
        try {
            Query q = em.createQuery("SELECT e FROM OeRegionales3 e WHERE e.oeRegionales3PK.numFolio = :folio and e.oeRegionales3PK.cveProy = :cveProy and e.oeRegionales3PK.version = :version");
            q.setParameter("folio", folio);
            q.setParameter("cveProy", clveProy);
            q.setParameter("version", version);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<OeRegionales3>();
        }            
    }

    @SuppressWarnings("unchecked")
	public List<OeGralTerrit> getOeGral(String folio, String clveProy, String cveArea, short version) {
        try {
            Query q = em.createQuery("SELECT e FROM OeGralTerrit e WHERE e.oeGralTerritPK.numFolio = :folio and e.oeGralTerritPK.cveProy = :cveProy and e.oeGralTerritPK.version = :version");
            q.setParameter("folio", folio);
            q.setParameter("cveProy", clveProy);
            q.setParameter("version", version);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<OeGralTerrit>();
        }              
    }

    @SuppressWarnings("unchecked")
	public List<OeMarinos> getOeMarinos(String folio, String clveProy, String cveArea, short version) {
        try {
            Query q =em.createQuery("SELECT e FROM OeMarinos e WHERE e.oeMarinosPK.numFolio = :folio and e.oeMarinosPK.cveProy = :cveProy and e.oeMarinosPK.version = :version");
            q.setParameter("folio", folio);
            q.setParameter("cveProy", clveProy);
            q.setParameter("version", version);
            return q.getResultList();

        } catch (Exception e) {
            return new ArrayList<OeMarinos>();
        }              
            
    }

    @SuppressWarnings("unchecked")
	public List<OePoligEnvol> getOePoligEnvol(String folio, String clveProy, String cveArea, short version) {
        try {
            Query q = em.createQuery("SELECT e FROM OePoligEnvol e WHERE e.oePoligEnvolPK.numFolio = :folio and e.oePoligEnvolPK.cveProy = :cveProy and e.oePoligEnvolPK.version = :version");
            q.setParameter("folio", folio);
            q.setParameter("cveProy", clveProy);
            q.setParameter("version", version);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<OePoligEnvol>();
        }              
            
    }
    
    @SuppressWarnings("unchecked")
	public List<ContaminanteProyecto> getContaminanteProyecto(String folio, short serial) {
        try {
            Query q = em.createQuery("SELECT e FROM ContaminanteProyecto e WHERE e.contaminanteProyectoPK.folioProyecto = :folio and e.contaminanteProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            return q.getResultList();
            
        } catch (Exception e) {
            return new ArrayList<ContaminanteProyecto>();
        }              
            
    }
    
    public Short getMaxPoetmProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = em.createQuery("SELECT max(e.poetmProyectoPK.poetmId) FROM PoetmProyecto e  WHERE e.poetmProyectoPK.folioProyecto = :folio and e.poetmProyectoPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 0;
        }

        return id;
    }
    
    /**
     * Método para obtener el monto de acuerdo al nivel (A/B/C) y tipo de proyecto
     * En caso de existir varios años, devolverá el monto del año más reciente existente.
     * @param idNivel
     * @param idEstudio
     * @return 
     * 
     * @author eescalona
     */
    public double obtenerMontoDerechos(Character idNivel, String idEstudio) {
        double monto = 0;
        
        try {
            Query q = em.createQuery("SELECT d.monto FROM CatDerechos d "
                    + "WHERE d.catDerechosPK.idNivel = :idNivel AND d.catDerechosPK.idEstudio = :idEstudio "
                    + "ORDER BY d.year DESC");
            q.setParameter("idNivel", idNivel);
            q.setParameter("idEstudio", idEstudio);
            
            @SuppressWarnings("unchecked")
			List<Double> lstMontos = (List<Double>)q.getResultList();
            if ( !lstMontos.isEmpty() ) {
                monto = lstMontos.get(0);
            }
            
        } catch (Exception e) {
            System.err.println("Ocurrió un error durante la obtención del monto del catálogo CAT_DERECHOS " + e.toString());
        }
        
        return monto;
    }
    
    /**
     * Obtiene listado de las versiones existentes para un proyecto a partir de su folio (SIGEIA)
     * @param folioProyecto
     * @return Listado de versiones
     */
    @SuppressWarnings("unchecked")
	public ArrayList<Short> obtieneVersionesSIGEIA(String folioProyecto) {
        try {
            Query q = em.createQuery("SELECT DISTINCT(p.proysigPK.version) FROM Proysig p WHERE p.proysigPK.numFolio = :folio "
                    + "ORDER BY p.proysigPK.version");
            q.setParameter("folio", folioProyecto);
            
            return (ArrayList<Short>)q.getResultList();
            
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Método que obtiene un listado de solo capitulos que tienen información adicional y/o sección modificada
     * @param bitacora
     * @return 
     */
    @SuppressWarnings("unchecked")
	public ArrayList<String[]> obtieneComenariosProyectoCapitulo(String bitacora) {
        ArrayList<String[]> lstRes = null;
        try {
            Query q = em.createNativeQuery(
                "select to_char(capitulo_id), to_char(max(info_adicional)) as info_adicional, to_char(max(sec_modificada)) as sec_modificada from " +
                "( " +
                "    select capitulo_id, subcapitulo_id, " +
                "        case when info_adicional = '1' then 1 " +
                "        else 0 end as info_adicional, " +
                "        case when sec_modificada = '1' then 1 " +
                "        else 0 end as sec_modificada   " +
                "    from comentario_proyecto where bitacora_proyecto = '"+ bitacora +"' " +
                ") group by capitulo_id "
            );
            
            lstRes = (ArrayList<String[]>)q.getResultList();
            
        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obteción de comentariosProyecto " + e.toString());
            e.printStackTrace();
        }
        
        return lstRes;
        
    }
    
    public HashMap<String,boolean[]> generaMapaInfoAdicionalCapitulo(String bitacora, String capitulo) {
        HashMap<String,boolean[]> mapRes = new HashMap<>();
        ArrayList<String[]> lstInfo;
        boolean[] bRes;
        
        lstInfo = obtieneComenariosProyectoSubCap(bitacora, capitulo);
        for (Object[] reg : lstInfo) {
            bRes = new boolean[2];
            bRes[0] = ((String)reg[1]).compareTo("1") == 0;
            bRes[1] = ((String)reg[2]).compareTo("1") == 0;
           
            mapRes.put( "cap_" + reg[0], bRes);
        }
        
        return mapRes;
    }    
    
    /**
     * Método que obtiene un listado de capitulos y subcapitulos que tienen información adicional y/o sección modificada
     * @param bitacora
     * @return 
     */
    @SuppressWarnings("unchecked")
	public ArrayList<String[]> obtieneComenariosProyectoSubCap(String bitacora, String capitulo) {
        ArrayList<String[]> lstRes = null;
        try {
            Query q = em.createNativeQuery(
                "select capitulo_id || '_' || subcapitulo_id || '_' || seccion_id || '_' || apartado_id as ubicacion,  " +
                "    to_char(max(info_adicional)) as info_adicional, to_char(max(sec_modificada)) as sec_modificada from " +
                "( " +
                "    select capitulo_id, subcapitulo_id, seccion_id, apartado_id, " +
                "        case when info_adicional = '1' then 1 " +
                "        else 0 end as info_adicional, " +
                "        case when sec_modificada = '1' then 1 " +
                "        else 0 end as sec_modificada   " +
                "    from comentario_proyecto where bitacora_proyecto = '"+ bitacora +"' and capitulo_id = " + capitulo +
                ") group by capitulo_id, subcapitulo_id, seccion_id, apartado_id "                    
            );
            
            lstRes = (ArrayList<String[]>)q.getResultList();
            
        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obteción de comentariosProyecto " + e.toString());
            e.printStackTrace();
        }
        
        return lstRes;
        
    }
    /**
     * Obtiene listado de las versiones existentes para un proyecto a partir de su folio (DGIRA)
     * @param folioProyecto
     * @return Listado de versiones
     */
    @SuppressWarnings("unchecked")
	public ArrayList<Short> obtieneVersionesProyecto(String folioProyecto) {
        try {

            Query q = em.createQuery("SELECT DISTINCT(p.proyectoPK.serialProyecto) FROM Proyecto_DGIRA p WHERE p.proyectoPK.folioProyecto = :folio ORDER BY p.proyectoPK.serialProyecto");
            q.setParameter("folio", folioProyecto);
            
            return (ArrayList<Short>)q.getResultList();
            
        } catch (Exception e) {
            return null;
        }
    }    
    
    public void merge(Object object) throws Exception {
        try {
            em.getTransaction().begin();
            object = em.merge(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getCatTipoDoc(Integer tipoTram) {
        Query q = em.createNativeQuery("select A.TIPO_DOC_ID, A.TIPO_DOC_DESCRIPCION from CAT_TIPO_DOC_DGIRA A WHERE A.TIPO_DOC_USUARIO = 'RT' AND A.ID_TRAMITE2="+tipoTram);
        return q.getResultList();
    }
    @SuppressWarnings("unchecked")
	public List<Object[]> getCatTipoDocNeg(Integer tipoTram) {
        Query q = em.createNativeQuery("select A.TIPO_DOC_ID, A.TIPO_DOC_DESCRIPCION from CAT_TIPO_DOC_DGIRA A WHERE A.TIPO_DOC_USUARIO = 'RT' AND A.ID_TRAMITE2="+tipoTram+"  AND A.TIPO_DOC_DESCRIPCION LIKE '%NEGADO%'");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<CatTipoDocDgira> getCatTipoDoc2() {
        Query q = em.createQuery("SELECT c FROM CatTipoDocDgira c");
       
        return q.getResultList();
    }
    
    public DocumentoRespuestaDgira getDocumentotoRespuestaDgira(String bitacora, String idDdocumento) {
        Query q = em.createQuery("SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto "
                + "AND d.documentoRespuestaDgiraPK.documentoId = :documentoId");
        q.setParameter("bitacoraProyecto", bitacora);
        q.setParameter("documentoId", idDdocumento);
        
        DocumentoRespuestaDgira resp;
        
        try {
            @SuppressWarnings("unchecked")
			List<DocumentoRespuestaDgira> lstRes = q.getResultList();
            resp = lstRes.get(0);
            
        } catch (Exception e){
            resp = null;
        }
        
        return resp;
        
    }
    
    public Integer generarIDDoctoRespDGIRA(String bitacora, short idTipoDoc){
        String strRes;
        Integer iRes;
        
        Query q = em.createQuery("SELECT d.documentoRespuestaDgiraPK.documentoId FROM DocumentoRespuestaDgira d "
                + "WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto "
                + "  AND d.documentoRespuestaDgiraPK.tipoDocId = :tipoDocId "
                + "ORDER BY d.documentoRespuestaDgiraPK.documentoId DESC");
        
        q.setParameter("bitacoraProyecto", bitacora);
        q.setParameter("tipoDocId", idTipoDoc);
        q.setMaxResults(1);
        
        try {
            strRes = (String)q.getSingleResult();
            iRes = Integer.parseInt(strRes);
            iRes++;
        } catch (Exception e) {
            iRes = 1;
        }
        
        return iRes;
    }
    
    public DocumentoRespuestaDgira getDocumentotoRespuestaDgira(String bitacora, String idDdocumento, short idTipoDoc ) {
        Query q = em.createQuery("SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto "
                + "AND d.documentoRespuestaDgiraPK.documentoId = :documentoId AND d.documentoRespuestaDgiraPK.tipoDocId = :tipoDocId");
        q.setParameter("bitacoraProyecto", bitacora);
        q.setParameter("documentoId", idDdocumento);
        q.setParameter("tipoDocId", idTipoDoc);
        
        DocumentoRespuestaDgira resp;
        
        try {
            @SuppressWarnings("unchecked")
			List<DocumentoRespuestaDgira> lstRes = q.getResultList();
            resp = lstRes.get(0);
            
        } catch (Exception e){
            resp = null;
        }
        
        return resp;
        
    }
    
    public boolean tieneCiudadanoPrincipal(String bitacora) {
        boolean bRes = false;
        Query q = em.createQuery("SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto "
                + "AND d.documentoRespuestaDgiraPK.tipoDocId = :tipoDocId");
        q.setParameter("bitacoraProyecto", bitacora);
        q.setParameter("tipoDocId", GenericConstants.TIPO_OFICIO_CONSULTA_PUBLICA_CIUDADANO_PRINCIPAL);
        
        try {
            @SuppressWarnings("unchecked")
			List<DocumentoRespuestaDgira> lstRes = q.getResultList();
            if (!lstRes.isEmpty()) {
                bRes = true;
            }
        } catch (Exception e){}
        
        return bRes;
        
    }
    
    @SuppressWarnings("unchecked")
	public List<ClimaProyecto> getClimaProyecto(String folio, Short serial) {
        Query q = em.createQuery("SELECT e FROM ClimaProyecto e  WHERE e.climaProyectoPK.folioProyecto = :folio and e.climaProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getClimas2(String folio, Short serial, String cveProy) {
        Query q = em.createQuery("SELECT c.climaTipo, c.grupomapa2, c.desTem, c.descPrec FROM Climas c WHERE num_Folio = :fol and version = :version group by c.climaTipo, c.grupomapa2, c.desTem, c.descPrec ");
        q.setParameter("fol", folio);
        q.setParameter("version", serial);
//        q.setParameter("clveProy", cveProy);
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getMicrocuenca2(String folio, Short serial, String cveProy) {
        Query q = em.createQuery("SELECT e.cueHid, e.subcHid, e.nomMic FROM Microcuenca e  WHERE e.microcuencaPK.numFolio = :folio and e.microcuencaPK.version = :serial group by e.cueHid, e.subcHid, e.nomMic");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<CatEstEsp> getCatEstEsp() {
        Query q = em.createQuery("SELECT e FROM CatEstEsp e order by e.estudioDescripcion");
        return q.getResultList();
    }
    
    public String  getGlosario(String folio, short serial) {
        Query q = em.createQuery("SELECT p.glosario FROM Proyecto_TO p WHERE p.proyectoPK.folioProyecto = :folio AND p.proyectoPK.serialProyecto = :serial");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (String) q.getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
	public void actualizaConsultaPublica(String bitacora, String clave, String respuesta) {
        List<ConsultaPublicaProyecto> lstConsulta;
        ConsultaPublicaProyecto eleConsulta;
        try {
            lstConsulta =
                (List<ConsultaPublicaProyecto>)lista_namedQuery("ConsultaPublicaProyecto.findByEvaBitacoraProyectoId", new Object[]{bitacora,clave}, new String[]{"evaBitacoraProyecto", "claveDgira"});
            
            if (lstConsulta != null && !lstConsulta.isEmpty()) {//update
                 eleConsulta = lstConsulta.get(0);
                 eleConsulta.setConsultaPublicaRespuesta(respuesta);
                 merge(eleConsulta);
            } else {//insert
                eleConsulta = new ConsultaPublicaProyecto(new ConsultaPublicaProyectoPK(bitacora, clave));
                eleConsulta.setConsultaPublicaRespuesta(respuesta);
                agrega(eleConsulta);
            }

        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obtención de consulta_publica_proyecto " + e.toString());
            e.printStackTrace();
        }
        
    }
    
    public String getCostoSDPagDerec(String bitacora) {
        String cadena = "";
        Query q = em.createNativeQuery("SELECT CASE WHEN (B.EVA_MONT_CRIT_EVALUADOR IS NULL) THEN ('0.0') ELSE (B.EVA_MONT_CRIT_EVALUADOR || '') END FROM EVALUACION_PROYECTO B WHERE B.BITACORA_PROYECTO = '"+ bitacora +"'");
//        List<Object[]> l= q.getResultList();
        cadena = q.getResultList().toString();
        return cadena;
    }
    
    
    //Para lista de consulta
    @SuppressWarnings("unchecked")
	public List<Object[]> prediosColindantes(String folio, Short serial)
    {
    	Query q = em.createNativeQuery("SELECT PREDIOCOLIN_PROY.PREDIO_COLIN_ID, PREDIOCOLIN_PROY.PREDIO_NOMBRE, "
    								 + "CAT_CLASIFICACION.CLASIFICACION_DESCRIPCION,CAT_TEMPORALIDAD.TEMPORALIDAD_DESCRIPCION, "
    								 + "CAT_REFERENCIA.REFERENCIA_DESCRIPCION, CAT_CLASIFICACION_B.CLASIFICACION_B "
    								 + "FROM PREDIOCOLIN_PROY "
    								 + "INNER JOIN CAT_CLASIFICACION  ON CAT_CLASIFICACION.CLASIFICACION_ID = PREDIOCOLIN_PROY.CLASIFICACION_ID "
    								 + "INNER JOIN CAT_TEMPORALIDAD   ON CAT_TEMPORALIDAD.TEMPORALIDAD_ID= PREDIOCOLIN_PROY.TEMPORALIDAD_ID "
    								 + "INNER JOIN CAT_REFERENCIA     ON CAT_REFERENCIA.REFERENCIA_ID = PREDIOCOLIN_PROY.REFERENCIA_ID "
    								 + "INNER JOIN CAT_CLASIFICACION_B ON CAT_CLASIFICACION_B.CLASIFICACION_B_ID = PREDIOCOLIN_PROY.CLASIFICACION_B_ID "
    								 + "WHERE PREDIOCOLIN_PROY.FOLIO_PROYECTO = :folio AND PREDIOCOLIN_PROY.SERIAL_PROYECTO = :serial");
    	q.setParameter("folio", folio);
    	q.setParameter("serial", serial);
    	return q.getResultList();    	
    }
    
	public static Paragraph parseHtmlParagraph(String html) {
		if (html == null || html.isEmpty()) {
//			System.out.println("hola from null:::::::::" + html + ":::::");
			return new Paragraph("");
		} else {
//			System.out.println("hola from not null:::::::::" + html);
		//seccion8_4.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_GLOSARIO")) + "</p>"));
			Paragraph pa = new Paragraph();
			try {
				List<Element> tl = HTMLWorker.parseToList(new StringReader(html.replace("<hr", "<p").replace("</hr", "</p")), null);
				for (Element e : tl) {
					pa.add(e);
				}
			} catch (Exception ioe) {
				ioe.printStackTrace();
			}
			return pa;
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<InversionEtapas> getInversiones(String folio, short serial) {
        Query q = emfMIAE.createQuery("SELECT a FROM InversionEtapas a where a.inversionEtapasPK.folioProyecto = :folio and a.inversionEtapasPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }
	
	public InversionEtapas getInversion(String folio, short serial, short inversionEtapaId) {
        Query q = emfMIAE.createQuery("SELECT a FROM InversionEtapas a where a.inversionEtapasPK.folioProyecto = :folio and a.inversionEtapasPK.serialProyecto =:serial and  a.inversionEtapasPK.inversionEtapaId = :inversionEtapaId");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("inversionEtapaId", inversionEtapaId);
        return (InversionEtapas) q.getSingleResult();
    }
    
	public ProyectoTO proyecto(String folio, short serial) {
        return (ProyectoTO) emfMIAE.find(ProyectoTO.class, new ProyectoPK(folio, serial));
    }
	
	@SuppressWarnings("unchecked")
	public List<ServicioProyecto> getServicios(String folio, short serial, short etapaId) {
        Query q = emfMIAE.createQuery("SELECT a FROM ServicioProyecto a where a.servicioProyectoPK.folioProyecto = :folio and a.servicioProyectoPK.serialProyecto = :serial and a.etapaId = :etapaId");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("etapaId", etapaId);
        return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<InfobioProyecto> getInfoBios(String folio, short serial) {
        Query q = emfMIAE.createQuery("SELECT a FROM InfobioProyecto a where a.infobioProyectoPK.folioProyecto = :folio and a.infobioProyectoPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<AccidentesProyecto> getAccidentes(String folio, short serial) {
        Query q = emfMIAE.createQuery("SELECT a FROM AccidentesProyecto a where a.accidentesProyectoPK.folioProyecto = :folio and a.accidentesProyectoPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<ReiaProyObras> getReiaProyObras(String folio) {
        Query q = emfMIAE.createQuery("SELECT a FROM ReiaProyObras a where a.folio = :folio and a.obra.obra NOT LIKE 'NO'");
        q.setParameter("folio", folio);
        return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<EntidadFederativa> getEstados() {
        Query q = emfMIAE.createQuery("SELECT a FROM EntidadFederativa a order by a.nombreEntidadFederativa");
        return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<DelegacionMunicipio> getMunicipios(String estado) {
        Query q = emfMIAE.createQuery("SELECT a FROM DelegacionMunicipio a WHERE a.entidadFederativa.idEntidadFederativa = :estado ORDER BY a.nombreDelegacionMunicipio");
        q.setParameter("estado", estado);
        return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<CatTratamientos> getTratamientos() {
		Query q = emfMIAE.createQuery("SELECT a FROM CatTratamientos a");
        return q.getResultList();
	}
	
}
