package mx.gob.semarnat.dao;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Ana Olvera
 */
public class EmfListener implements ServletContextListener{
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        PersistenceManager.closeEntityManagerFactory();
    }
}
