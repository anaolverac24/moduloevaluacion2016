package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import mx.gob.semarnat.model.dgira_miae.CriterioValoresProyecto;
import mx.gob.semarnat.model.dgira_miae.OpinionTecnicaProyecto;
import mx.gob.semarnat.model.dgira_miae.RequisitosSubdirector;
import mx.gob.semarnat.model.seguridad.Tbsituacion;

@SuppressWarnings("serial")
public class DirectorDao implements Serializable{
    EntityManager em = PersistenceManager.getEmfBitacora().createEntityManager();
    
    @SuppressWarnings("unchecked")
	public List<Tbsituacion> getSituaciones(){
        Query q = em.createQuery("SELECT a FROM Tbsituacion a");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getTitTramite(String bitacora) {
        Query q = em.createNativeQuery("SELECT C.NOMBRE_REQUISITO FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B , CATALOGO_TRAMITES.CAT_REQUISITOS C WHERE B.REQSD_NUMERO_REQUISITO = C.ID_REQUISITO AND B.REQSD_BITACORA = '"+ bitacora +"'");
        return q.getResultList();
    }
    
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramite(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO , C.NOMBRE_REQUISITO , C.ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B , CATALOGO_TRAMITES.CAT_REQUISITOS C WHERE B.REQSD_NUMERO_REQUISITO = C.ID_REQUISITO AND B.REQSD_BITACORA = '"+ bitacora +"'");
        List<Object[]> l = q.getResultList();
        return l;
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramiteDigital(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO , B.REQSD_NOMBRE_REQUISITO , B.REQSD_NUMERO_REQUISITO AS ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B WHERE B.REQSD_BITACORA = '"+ bitacora +"'");
        List<Object[]> l = q.getResultList();
        return l;
    }
    
	@SuppressWarnings("unchecked")
	public List<Object[]> getTramitesAprovados(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO, B.REQSD_NOMBRE_REQUISITO, B.REQSD_NUMERO_REQUISITO AS ID_REQUISITO, B.REQSD_ENTREGADO_ECC,	B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B WHERE B.REQSD_BITACORA = '"+ bitacora +"' AND B.REQSD_ENTREGADO_SD = 1 ");
        List<Object[]> l = q.getResultList();
        return l;
    }
	
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramiteSI(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO , C.NOMBRE_REQUISITO , C.ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B , CATALOGO_TRAMITES.CAT_REQUISITOS C WHERE B.REQSD_NUMERO_REQUISITO = C.ID_REQUISITO AND B.REQSD_BITACORA = '"+ bitacora + "' AND B.REQSD_ENTREGADO_SD = 1");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramiteSIDigital(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO, B.REQSD_NOMBRE_REQUISITO , B.REQSD_NUMERO_REQUISITO AS ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B WHERE B.REQSD_BITACORA = '"+ bitacora + "' AND B.REQSD_ENTREGADO_SD = 1");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramiteNull(String bitacora) {
//        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO, C.NOMBRE_REQUISITO , C.ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES " +
//        "FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B " +
//        "WHERE B.REQSD_BITACORA = '"+ bitacora + "' AND  B.REQSD_ENTREGADO_SD IS NULL");
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO , C.NOMBRE_REQUISITO , C.ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES " +
        "FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B , CATALOGO_TRAMITES.CAT_REQUISITOS C " +
        "WHERE B.REQSD_NUMERO_REQUISITO = C.ID_REQUISITO AND B.REQSD_BITACORA = '"+ bitacora + "' AND  B.REQSD_ENTREGADO_SD IS NULL");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramiteNullDigital(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO , B.REQSD_NOMBRE_REQUISITO , B.REQSD_NUMERO_REQUISITO AS ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES " +
        "FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B  " +
        "WHERE B.REQSD_BITACORA = '"+ bitacora + "' AND  B.REQSD_ENTREGADO_SD IS NULL");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramiteNo(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO , B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES " +
            " FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B WHERE B.REQSD_BITACORA = '"+ bitacora + "' AND  B.REQSD_ENTREGADO_SD = 0");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getReqTramiteNoDigital(String bitacora) {
        Query q = em.createNativeQuery("SELECT B.REQSD_NUMERO_REQUISITO , B.REQSD_NOMBRE_REQUISITO , B.REQSD_NUMERO_REQUISITO AS ID_REQUISITO, B.REQSD_ENTREGADO_ECC, B.REQSD_ENTREGADO_SD, B.REQSD_OBSERVACIONES " +
        "FROM DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR B " +
        "WHERE B.REQSD_BITACORA = '"+ bitacora + "' AND  B.REQSD_ENTREGADO_SD = 0");
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public List<RequisitosSubdirector> getReqMarcado(String bitacora){
        Query q = em.createQuery("SELECT a FROM RequisitosSubdirector a WHERE a.requisitosSubdirectorPK.reqsdBitacora =:numBita");
        q.setParameter("numBita", bitacora);
        return q.getResultList();
    }
    
    public void guardarReq(String bitacora, Integer noReq, String obs, Integer valSD ) throws Exception {
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("UPDATE DGIRA_MIAE2.REQUISITOS_SUBDIRECTOR SET REQSD_OBSERVACIONES = '"+obs+"' , REQSD_ENTREGADO_SD ="+valSD+" WHERE REQSD_BITACORA='"+bitacora+"' AND REQSD_NUMERO_REQUISITO="+noReq);
            q.executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getTitCapitulos(){
        Query q = em.createNativeQuery("SELECT A.CAPITULO_DESCRIPCION FROM DGIRA_MIAE2.CAT_CAPITULO A GROUP BY A.CAPITULO_ID,A.CAPITULO_DESCRIPCION ORDER BY A.CAPITULO_ID");
        return q.getResultList();
    }
    
    //<editor-fold desc="Listado a partir de un namedQuery" defaultstate="collapsed">
    /**
     * Hace listado a partir de un NamedQuery dentro de la entidad mapeada
     * @param qry
     * @param args
     * @param paramName
     * @return 
     */
    public List<?> lista_namedQuery(String qry, Object[] args, String[] paramName) {
        Query q = em.createNamedQuery(qry);
        for (int i = 0; i < args.length; i++) {
            q.setParameter(paramName[i], args[i]);
        }
        return q.getResultList();
    }//</editor-fold>
    
    //<editor-fold desc="Busqueda por Id" defaultstate="collapsed">
    /**
     * Busca en la tabla el objeto con el id proporcionado
     * @param tipo
     * @param id
     * @return 
     */
    public Object busca(Class<?> tipo, Object id) {
        return em.find(tipo, id);
    }//</editor-fold
    
    //<editor-fold desc="Actualiza registro del objeto mapeado" defaultstate="collapsed">
    /**
     * Actualiza objeto de una entidad mapeada
     * @param o 
     */
    public void modifica(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.merge(o);
            tx.commit();
//        em.close();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }//</editor-fold>
    
     @SuppressWarnings("unchecked")
	public List<Object[]> getValorCritEvaluador(String folio, Short serial){
        Query q = em.createQuery("SELECT a FROM CriterioValoresProyecto a WHERE a.criterioValoresProyectoPK.folioProyecto =:folio and a.criterioValoresProyectoPK.serialProyecto =:serial AND a.criterioValorEvaluacion is not null");  //a.criterioValoresProyectoPK.serialProyecto  =: serial AND a. CRITERIO_VALOR_EVALUACION  IS NOT NULL"
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
     
	 @SuppressWarnings("unchecked")
	public List<CriterioValoresProyecto> getValorCritEvaluador2(String folio, Short serial){
        Query q = em.createQuery("SELECT a FROM CriterioValoresProyecto a WHERE a.criterioValoresProyectoPK.folioProyecto =:folio and a.criterioValoresProyectoPK.serialProyecto =:serial AND a.criterioValorEvaluacion is not null");  //a.criterioValoresProyectoPK.serialProyecto  =: serial AND a. CRITERIO_VALOR_EVALUACION  IS NOT NULL"
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getValoresPago(String folio, Short serial){
		Query q = em.createQuery("SELECT sum(a.criterioValorEvaluacion), sum(a.criterioValorUsuario) FROM CriterioValoresProyecto a WHERE a.criterioValoresProyectoPK.folioProyecto =:folio AND a.criterioValoresProyectoPK.serialProyecto =:serial AND a.criterioValorEvaluacion is not NULL "); 
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
	}
	
    @SuppressWarnings("unchecked")
	public List<Object[]> getValorAnalisisPree(String bita){
        Query q = em.createQuery("SELECT a FROM AnalisisPreliminarProyecto a WHERE a.analisisPreliminarProyectoPK.bitacoraProyecto =:bita ");
        q.setParameter("bita", bita);
        return q.getResultList();
    }
    
    @SuppressWarnings("unchecked")
    public List<OpinionTecnicaProyecto> getOpinionTecnica(String bita){
        Query q = em.createNamedQuery("OpinionTecnicaProyecto.findByBitacoraProyecto");
        q.setParameter("bitacoraProyecto", bita);
        return q.getResultList();
    }
    
//    public void setOpinonTecnica(OpinionTecnicaProyecto op){
//        em.merge(op);
//    }
}
