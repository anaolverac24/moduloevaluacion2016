package mx.gob.semarnat.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.model.catalogos.DelegacionMunicipio;

public class DelegacionMunicipioDAO {

    private final EntityManager em = PersistenceManager.getEmfBitacora().createEntityManager();

    public List<DelegacionMunicipio> getAllDelegacionMunicipio() {
        List<DelegacionMunicipio> resultado = new ArrayList<>();
        try {
            Query q = em.createNamedQuery("DelegacionMunicipio.findAll", DelegacionMunicipio.class);
            resultado = q.getResultList();
        } catch (Exception e) {
        } finally {
            em.clear();
        }
        return resultado;
    }

    public List<DelegacionMunicipio> getAllDelegacionMunicipioByEntidad(String ef) {
        List<DelegacionMunicipio> resultado = new ArrayList<>();
        try {
            String sql = "SELECT * FROM CATALOGOS.DELEGACION_MUNICIPIO WHERE ENTIDAD_FEDERATIVA = ?1";
            Query q = em.createNativeQuery(sql, DelegacionMunicipio.class);
            q.setParameter(1, ef);
            resultado = (List<DelegacionMunicipio>) q.getResultList();
        } catch (Exception e) {
        } finally {
            em.clear();
        }
        return resultado;
    }
}
