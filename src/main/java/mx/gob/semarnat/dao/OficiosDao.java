package mx.gob.semarnat.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo;
import mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.oficios.TipoDocumento;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbusuario;

/**
 *
 * @author Ana Olvera
 */
public class OficiosDao {
    
    private final EntityManager emfOficios = PersistenceManager.getEmfMIAE().createEntityManager();
    private final EntityManager emfEvaluacion = PersistenceManager.getEmfBitacora().createEntityManager();
    
    public DocumentoRespuestaDgira buscarOficio(String bitacoraProyecto, int tipoDocId, int documentoId){
        DocumentoRespuestaDgira resultado = new DocumentoRespuestaDgira();
        Query q;
        try {
            q = emfOficios.createNamedQuery("DocumentoRespuestaDgira.findByBitacoraDocumento");
            q.setParameter("bitacoraProyecto", bitacoraProyecto);
            q.setParameter("tipoDocId", tipoDocId);
            q.setParameter("documentoId", documentoId);
            resultado = (DocumentoRespuestaDgira) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encontró documento");
        }finally{
            emfOficios.clear();
        }
        return resultado;
    }
    
    public DocumentoRespuestaDgira buscarResolutivo(String bitacoraProyecto){
        DocumentoRespuestaDgira resultado = new DocumentoRespuestaDgira();
        Query q;
        try {
            q = emfOficios.createNamedQuery("DocumentoRespuestaDgira.findByResolutivoMIA");
            q.setParameter("bitacoraProyecto", bitacoraProyecto);
            resultado = (DocumentoRespuestaDgira) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encontró documento");
        }finally{
            emfOficios.clear();
        }
        return resultado;
    }
    
    public DocumentoRespuestaDgira buscarResolutivoExcencion(String bitacoraProyecto){
        DocumentoRespuestaDgira resultado = new DocumentoRespuestaDgira();
        Query q;
        try {
            q = emfOficios.createNamedQuery("DocumentoRespuestaDgira.findByResolutivoDC");
            q.setParameter("bitacoraProyecto", bitacoraProyecto);
            resultado = (DocumentoRespuestaDgira) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encontró documento");
        }finally{
            emfOficios.clear();
        }
        return resultado;
    }
    
    public DocumentoRespuestaDgira buscarPrevencion(String bitacoraProyecto){
        DocumentoRespuestaDgira resultado = new DocumentoRespuestaDgira();
        Query q;
        try {
            q = emfOficios.createNamedQuery("DocumentoRespuestaDgira.findByPrevencionMIA");
            q.setParameter("bitacoraProyecto", bitacoraProyecto);
            resultado = (DocumentoRespuestaDgira) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encontró documento");
        }finally{
            emfOficios.clear();
        }
        return resultado;
    }
    
    public DocumentoRespuestaDgira buscarOficio(String bitacoraProyecto,short tipoDocId){
        DocumentoRespuestaDgira resultado = new DocumentoRespuestaDgira();
        Query q;
        try {
            q = emfOficios.createNamedQuery("DocumentoRespuestaDgira.findByBitacoraTipoDocumento");
            q.setParameter("bitacoraProyecto", bitacoraProyecto);
            q.setParameter("tipoDocId", tipoDocId);
            resultado = (DocumentoRespuestaDgira) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encontró documento");
        }finally{
            emfOficios.clear();
        }
        return resultado;
    }
    
    public TipoDocumento recuperarTipoDoc(DocumentoRespuestaDgira doc){
        TipoDocumento tipoDoc = new TipoDocumento();
        Query q;
        try {
            q = emfOficios.createNamedQuery("TipoDocumento.findDescripcionByPK");
            q.setParameter("claveTramite", doc.getDocumentoRespuestaDgiraPK().getClaveTramite());
            q.setParameter("idTramite", doc.getDocumentoRespuestaDgiraPK().getIdTramite());
            q.setParameter("tipoDocId", doc.getDocumentoRespuestaDgiraPK().getTipoDocId());
            tipoDoc = (TipoDocumento) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encontró el tipo de documento");
        }finally{
            emfOficios.clear();
        }
        return tipoDoc;
    }
    
    public DocumentoDgiraFlujo recuperaSituacion(DocumentoRespuestaDgira documento){
        DocumentoDgiraFlujo resultado = new DocumentoDgiraFlujo();
        Query q;
        try {
            q = emfOficios.createNamedQuery("DocumentoDgiraFlujo.findByDocumentoDgiraPK");
            q.setParameter("bitacoraProyecto", documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
            q.setParameter("tipoDocId", documento.getDocumentoRespuestaDgiraPK().getTipoDocId());
            resultado = (DocumentoDgiraFlujo) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encuentra la bitácora");
        }finally{
            emfOficios.clear();
        }
        return resultado;
    }
    
    public Historial recuperaAreaAsignada(String bitacora, String situacion){
        Historial respuesta = new Historial();
        Query q;
        try {
            q = emfEvaluacion.createNamedQuery("Historial.findByHistoNumeroSit");
            q.setParameter("histoNumero", bitacora);
            q.setParameter("histoSituacionActual", situacion);
            respuesta = (Historial) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("No se encontró la situación");
        }finally{
            emfEvaluacion.clear();
        }
        return respuesta;
    }
    
    public Tbarea recuperarAreaPadre(Tbusuario areaPerfil){
        Tbarea resultado = new Tbarea();
        Query q;
        try {
            q = emfEvaluacion.createNamedQuery("Tbarea.findByIdareaDependencia");
            q.setParameter("idarea", areaPerfil.getIdarea());
            q.setParameter("iddependencia", areaPerfil.getIddependencia());
            resultado = (Tbarea) q.getSingleResult();
        } catch (Exception e) {
        }finally{
            emfEvaluacion.clear();
        }
        return resultado;
    }
    
    public List<DocumentoDgiraFlujoHist> recuperarHistorialDgiraMiae2(String bitacora, short tipoDocId){
        List<DocumentoDgiraFlujoHist> resultado = new ArrayList<>();
        Query q;
        try {
            q = emfOficios.createNamedQuery("DocumentoDgiraFlujoHist.findByBitacoraProyectoTipoDoc");
            q.setParameter("bitacoraProyecto", bitacora);
            q.setParameter("tipoDocId", tipoDocId);
            resultado = q.getResultList();
        } catch (Exception e) {
            System.out.println("Hubo un problema al consultar el historial temporal");
        }finally{
            emfOficios.clear();
        }
        return resultado;
    }

}
