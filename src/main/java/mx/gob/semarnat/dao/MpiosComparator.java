/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 *
 * @author Rodrigo
 */
public class MpiosComparator implements Comparator<Object[]>{

    @Override
    public int compare(Object[] o1, Object[] o2) {
        BigDecimal b1, b2;
        b1 = new BigDecimal(o1[1].toString());
        b2 = new BigDecimal(o2[1].toString());
        return b1.intValue() - b2.intValue();
    }

}
