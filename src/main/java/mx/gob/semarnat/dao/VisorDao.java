/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.model.catalogos.CatVialidad;
import mx.gob.semarnat.model.catalogos.RamaProyecto;
import mx.gob.semarnat.model.catalogos.SectorProyecto;
import mx.gob.semarnat.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.model.catalogos.TipoProyecto;
import mx.gob.semarnat.model.dgira_miae.AnexosProyecto;
import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.CatMineralesReservados;
import mx.gob.semarnat.model.dgira_miae.CatNorma;
import mx.gob.semarnat.model.dgira_miae.CatObra;
import mx.gob.semarnat.model.dgira_miae.CatParqueIndustrial;
import mx.gob.semarnat.model.dgira_miae.CatPdu;
import mx.gob.semarnat.model.dgira_miae.CatPoet;
import mx.gob.semarnat.model.dgira_miae.CatTipoClima;
import mx.gob.semarnat.model.dgira_miae.CatTipoVegetacion;
import mx.gob.semarnat.model.dgira_miae.ComentarioProyecto;
import mx.gob.semarnat.model.dgira_miae.EspecificacionAnexo;
import mx.gob.semarnat.model.dgira_miae.EspecificacionProyecto;
import mx.gob.semarnat.model.dgira_miae.EstudioRiesgoProyecto;
import mx.gob.semarnat.model.dgira_miae.ImpacAmbProyecto;
import mx.gob.semarnat.model.dgira_miae.MedPrevImpactProy;
import mx.gob.semarnat.model.dgira_miae.ParqueIndustrialProyecto;
import mx.gob.semarnat.model.dgira_miae.PduProyecto;
import mx.gob.semarnat.model.dgira_miae.PoetProyecto;
import mx.gob.semarnat.model.dgira_miae.PreguntaClima;
import mx.gob.semarnat.model.dgira_miae.PreguntaMineral;
import mx.gob.semarnat.model.dgira_miae.PreguntaObra;
import mx.gob.semarnat.model.dgira_miae.PreguntaProyecto;
import mx.gob.semarnat.model.dgira_miae.PreguntaVegetacion;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoPK;
import mx.gob.semarnat.model.dgira_miae.RespTecProyecto;
import mx.gob.semarnat.model.dgira_miae.SustanciaAnexo;
import mx.gob.semarnat.model.sinat.SinatSinatec;
import mx.gob.semarnat.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.model.sinatec.Vexdatosusuariorep;


/**
 *
 * @author Paty
 */
public class VisorDao implements Serializable{
    private final EntityManager emfBitacora = PersistenceManager.getEmfBitacora().createEntityManager();
    private final EntityManager emfMIAE = PersistenceManager.getEmfMIAE().createEntityManager();   
    private final EntityManager em = PersistenceManager.getEmfBitacora().createEntityManager();
   
    
    public Proyecto cargaProyecto(String folioProyecto, short serialProyecto) {
        return (Proyecto) emfMIAE.find(Proyecto.class, new ProyectoPK(folioProyecto, serialProyecto));
    }
    
    public Bitacora datosBitaInd(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a FROM Bitacora a WHERE a.bitaNumero =:bitacora ");
        q.setParameter("bitacora", numBita);
       
        try {
            return (Bitacora) q.getSingleResult();
        } catch (Exception e) {
           // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return new Bitacora(); 
    }
    
    public List<Object[]> detalleProyCap(String numBita) {
        Query q = emfBitacora.createQuery(" SELECT A.bitaNumero, A.bitaIdProm, A.bitaSituacion ,C.cve, C.nombre, CONCAT(D.dfiNombre,' ' ,D.dfiApellidoMaterno,' ' ,D.dfiApellidoPaterno,D.dfiRazonSocial), A.bitaFechRegistro, A.bitaFmaxResolucion, CASE WHEN (A.bitaTipotram = 'IP') THEN 'RECEPCION, EVALUACION Y RESOLUCION DEL INFORME PREVENTIVO' WHEN (A.bitaTipotram = 'MP') THEN 'MIA PARTICULAR.- MOD A: NO INCLUYE RIESGO' WHEN (A.bitaTipotram = 'DM') THEN 'MIA PARTICULAR.- MOD B: INCLUYE RIESGO' WHEN (A.bitaTipotram = 'MG') THEN 'MIA REGIONAL.- MOD A: NO INCLUYE RIESGO' WHEN (A.bitaTipotram = 'DL') THEN 'MIA REGIONAL.- MOD B: INCLUYE RIESGO' ELSE '' END, E.descripcion FROM Bitacora A, Proyecto C, DatosFiscales D, Tbsituacion E WHERE A.bitaNumero = C.numeroBita AND A.bitaIdProm = D.datosFiscalesPK.promId AND D.datosFiscalesPK.idTipoDireccion = 03 AND A.bitaSituacion = E.idsituacion AND A.bitaNumero =:bitaNum");
        q.setParameter("bitaNum", numBita);
        List<Object[]> l = q.getResultList();
        return l;       
    }
    
    public List<EspecificacionProyecto> getEspecificacionesProyecto(String folio, short serial, short norma) {
        Query q = emfMIAE.createQuery(" SELECT e FROM EspecificacionProyecto e, CatEspecificacion as ce where e.especificacionProyectoPK.folioProyecto = :folio and e.especificacionProyectoPK.serialProyecto = :serial and e.especificacionProyectoPK.normaId = :norma and e.especificacionProyectoPK.especificacionId = ce.catEspecificacionPK.especificacionId and e.especificacionProyectoPK.normaId = ce.catEspecificacionPK.normaId ORDER BY ce.especificacionOrden ASC", EspecificacionProyecto.class);
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("norma", norma);
        List<EspecificacionProyecto> l = q.getResultList();
        return l;       
    }
    
    public Proyecto verificaERA(String numBita) { 
        Query q = emfMIAE.createNamedQuery("ProyectoD.findByBitacoraProyecto");
        q.setParameter("bitacoraProyecto", numBita);
        Proyecto p = new Proyecto();
        try {
            p = (Proyecto) q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
//            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
           //JOptionPane.showMessageDialog(null, "folio:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return p;
    }
    
    public Proyecto verificaERA3(String numBita) { 
        Query q = emfMIAE.createNamedQuery("ProyectoD.findByBitacoraProyectoSerial");
        q.setParameter("bitacoraProyecto", numBita);
        Proyecto p = new Proyecto();
        try {
            p = (Proyecto) q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
//            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
           //JOptionPane.showMessageDialog(null, "folio:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return p;
    }
    
    public Proyecto verificaERA2(String numBita) { 
        String strFolio = "";
        Query q1 = emfMIAE.createNamedQuery("SinatSinatec.findByBitacora");
        q1.setParameter("bitacora", numBita);
        
        List<SinatSinatec> lstReg = (List<SinatSinatec>)q1.getResultList();

        if ( lstReg.isEmpty() ) {//Si no existe el registro en SINAT_USDB.SINAT_SINATEC
            if (!numBita.isEmpty()) {
                boolean fisico = !(numBita.charAt(5) == 'W');
                if (fisico) {
                    strFolio = numBita;
                }
            }
        } else {
            strFolio = lstReg.get(0).getFolio().toString();
        }
        
        Proyecto p = null;
        Query q = emfMIAE.createNamedQuery("ProyectoD.findByFolioProyectoSerial");
        q.setParameter("folio", strFolio);

        List<Proyecto> lstProy = (List<Proyecto>)q.getResultList();

        if ( !lstProy.isEmpty() ) {
            p = lstProy.get(0);
        }
        
        return p;
    }
    
    public Proyecto obtieneProyectoUltimaVersion(String numBita) { 
        String strFolio = "";
        Query q1 = emfMIAE.createNamedQuery("SinatSinatec.findByBitacora");
        q1.setParameter("bitacora", numBita);
        
        List<SinatSinatec> lstReg = (List<SinatSinatec>)q1.getResultList();
        
        if ( lstReg.isEmpty() ) {//Si no existe el registro en SINAT_USDB.SINAT_SINATEC
            boolean fisico = !(numBita.charAt(5) == 'W');
            if (fisico) {
                strFolio = numBita;
            }
        } else {
            strFolio = lstReg.get(0).getFolio().toString();
        }
        
        Proyecto p = null;
        Query q = emfMIAE.createNamedQuery("ProyectoD.findByFolioProyectoSerial");
        q.setParameter("folio", strFolio);

        List<Proyecto> lstProy = (List<Proyecto>)q.getResultList();

        if ( !lstProy.isEmpty() ) {
            p = lstProy.get(0);
        }
        
        return p;
    }
    
    /**
     * Obtiene el proyecto con el serial previo al más reciente
     * @param numBita
     * @return <Proyecto>Proyecto previo al más reciente</Proyecto>
     */
    public Proyecto obtieneProyectoVersionAnterior(String numBita) { 
        Query q1 = emfMIAE.createNamedQuery("SinatSinatec.findByBitacora");
        q1.setParameter("bitacora", numBita);
        
        List<SinatSinatec> lstReg = (List<SinatSinatec>)q1.getResultList();
        
        Proyecto p = null;
        
        if (!lstReg.isEmpty()) {
            Query q = emfMIAE.createNamedQuery("ProyectoD.findByFolioProyectoOrderBySerial");
            q.setParameter("folio", lstReg.get(0).getFolio().toString());
            
            List<Proyecto> lstProy = (List<Proyecto>)q.getResultList();
            
            if ( lstProy.size() > 1 ) {
                p = lstProy.get(1);
            } else if ( lstProy.size() > 0 ) {//Si solo existe un serial y se pide el primero
                p = lstProy.get(0);
            }
        }
        
        return p;
    }
    
    /**
     * Obtiene el proyecto con el serial previo al más reciente
     * @param numBita
     * @return <Proysig>Versión más alta del proyecto de evaluador SIGEIA</Proysig>
     */
    public Short obtieneProyectoUltimaVersionSIGEIA(String numBita) { 
        Short p = null;
        
        Query q1 = emfMIAE.createNamedQuery("SinatSinatec.findByBitacora");
        q1.setParameter("bitacora", numBita);
        
        List<SinatSinatec> lstReg = (List<SinatSinatec>)q1.getResultList();
        
        if (!lstReg.isEmpty()) {
            Query q = emfMIAE.createQuery("SELECT p.proysigPK.version FROM Proysig p WHERE p.proysigPK.numFolio = :folio ORDER BY p.proysigPK.version DESC");
            q.setParameter("folio", lstReg.get(0).getFolio().toString());
            
            List<Short> lstProy = (List<Short>)q.getResultList();
            
            if ( lstProy.size() > 1 ) {
                p = lstProy.get(0);
            }
        }
        
        return p;
    }
    
    public List<Object[]> verificaERAList(String numBita) { 
        Query q = emfMIAE.createNamedQuery("ProyectoD.findByBitacoraProyecto");
        q.setParameter("bitacoraProyecto", numBita);
        List<Object[]> l = q.getResultList();
        return l;  
    }
    
    public Proyecto detalleFolProy(String numFolio) { 

        Query q = emfMIAE.createQuery("SELECT p FROM Proyecto_DGIRA p WHERE p.proyectoPK.folioProyecto =:folioProyecto");
        q.setParameter("folioProyecto", numFolio);
        Object o = null;

        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (Proyecto) o;
    }
    
    public CatNorma descNorma(Short idNorma) {
        Query q = emfMIAE.createQuery("SELECT p FROM CatNorma p WHERE p.normaId =:folioProyecto");
        q.setParameter("folioProyecto", idNorma);
        Object o = null;

        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (CatNorma) o;
    }
    
    
    
    
    
    public List<Object[]> pregNorma(String numFolio, short serial) {
        Query q = emfMIAE.createQuery(" select pp.preguntaProyectoPK.folioProyecto, "
                + "pp.preguntaProyectoPK.serialProyecto, pp.preguntaProyectoPK.preguntaId , "
                + "CASE WHEN (pp.preguntaRespuesta = 'S') THEN 'SI' ELSE 'NO' END, cp.normaId, "
                + "cp.preguntaDescripcion, cp.preguntaContinua "
                + "from PreguntaProyecto pp, CatPregunta cp "
                + "where pp.preguntaProyectoPK.preguntaId = cp.preguntaId and "
                + "pp.preguntaProyectoPK.folioProyecto =:folioProy and "
                + "pp.preguntaProyectoPK.serialProyecto =:serial ");
        //Query q = emfMIAE.createQuery(" select pp.preguntaProyectoPK.serialProyecto, pp.preguntaProyectoPK.serialProyecto, pp.preguntaProyectoPK.preguntaId, pp.climaId, pp.vegetacionId ,pp.preguntaRespuesta, cp.normaId, cp.preguntaDescripcion, cp.preguntaContinua  from PreguntaProyecto pp, CatPregunta cp where pp.preguntaProyectoPK.preguntaId = cp.preguntaId and pp.preguntaProyectoPK.folioProyecto ='2941' and pp.preguntaProyectoPK.folioProyecto =1");
        
        q.setParameter("folioProy", numFolio);
        q.setParameter("serial", serial);
        
        List<Object[]> l = q.getResultList();
        return l;       
    }
    
    public PreguntaProyecto pregNorma2(String numFolio, short serial, short pregId) {
        Query q = emfMIAE.createQuery(" select pp from PreguntaProyecto pp  where pp.preguntaProyectoPK.preguntaId=:idPreg and pp.preguntaProyectoPK.folioProyecto =:folioProy and pp.preguntaProyectoPK.serialProyecto=:serial ");
        q.setParameter("idPreg", pregId);
        q.setParameter("folioProy", numFolio);
        q.setParameter("serial", serial);
        
        try {
            return (PreguntaProyecto) q.getSingleResult();
        } catch (Exception e) {
        }
        return new PreguntaProyecto();     
    }
    
    //--------------update a objeto    
    public void merge(Object object) {
        try {
            emfMIAE.getTransaction().begin();
            emfMIAE.merge(object);
            emfMIAE.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            if (emfMIAE.getTransaction().isActive()) {
                emfMIAE.getTransaction().rollback();
            }
        } finally {
        }
    }
    
    public CatTipoVegetacion getCatTipoVegetacion(Short idV) {
        return (CatTipoVegetacion) emfMIAE.find(CatTipoVegetacion.class, idV);
    }

    public CatTipoClima getCatTpoclima(Short idClima) {
        return (CatTipoClima) emfMIAE.find(CatTipoClima.class, idClima);
    }

    public PreguntaVegetacion getPreguntaVegetacion(String folioProyecto, short serialProyecto) {
        Query q = emfMIAE.createQuery("SELECT p FROM PreguntaVegetacion p WHERE p.preguntaVegetacionPK.folioProyecto = :folio and p.preguntaVegetacionPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        try {
            return (PreguntaVegetacion) q.getSingleResult();
        } catch (Exception e) {

        }
        return new PreguntaVegetacion();

    }

    public PreguntaClima getPreguntaClima(String folioProyecto, short serialProyecto) {
 
        Query q = emfMIAE.createQuery("SELECT p FROM PreguntaClima p WHERE p.preguntaClimaPK.folioProyecto = :folio and p.preguntaClimaPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        try {
            return (PreguntaClima) q.getSingleResult();
        } catch (Exception e) {

        }
        return new PreguntaClima();
//        return (PreguntaClima)enMgr.find(PreguntaProyecto.class, new PreguntaProyectoPK(folioProyecto, serialProyecto, serialProyecto))
    }
    
     public CatMineralesReservados getMineral(Short idMineral) {
        return (CatMineralesReservados) emfMIAE.find(CatMineralesReservados.class, idMineral);
    }

    public List<PreguntaMineral> getMinerales(String folioProyecto, short serialProyecto) {
        Query q = emfMIAE.createQuery("SELECT m FROM PreguntaMineral m WHERE m.preguntaMineralPK.folioProyecto = :folio and m.preguntaMineralPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);

        return q.getResultList();
    }
    
    public List<PreguntaObra> getObras(String folioProyecto, short serialProyecto) {
        Query q = emfMIAE.createQuery("SELECT o from PreguntaObra o where o.preguntaObraPK.folioProyecto = :folio and o.preguntaObraPK.serialProyecto = :serial");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        return q.getResultList();
    }
    
    public List<CatTipoClima> tipoClima() {
        Query q = emfMIAE.createQuery("SELECT a FROM CatTipoClima a");
        List l = q.getResultList();
        return l;
    }

    public List<CatObra> tipoObra() {
        Query q = emfMIAE.createQuery("SELECT a FROM CatObra a");
        List l = q.getResultList();
        return l;
    }

    public List<CatTipoVegetacion> tipoVegetacion() {
        Query q = emfMIAE.createQuery("SELECT a FROM CatTipoVegetacion a");
        List l = q.getResultList();
        return l;
    }

    public List<CatUnidadMedida> unidadMedida(String tipo) {
        Query q = emfMIAE.createQuery("SELECT a FROM CatUnidadMedida a");
        List l = q.getResultList();
        return l;
    }

    public List<SelectItem> unidadMedidaSelectItem() {
        List<SelectItem> lista = new LinkedList<SelectItem>();
        Query q = emfMIAE.createQuery("SELECT a FROM CatUnidadMedida a");
        List<CatUnidadMedida> l = q.getResultList();
        for (CatUnidadMedida c : l) {
            SelectItem temp = new SelectItem(c, c.getCtunDesc());
            lista.add(temp);
        }
        return lista;
    }

    public List<SelectItem> mineralSelectItem() {
        List<SelectItem> lista = new LinkedList<SelectItem>();
        Query q = emfMIAE.createNamedQuery("CatMineralesReservados.findAll");
        List<CatMineralesReservados> l = q.getResultList();
        for (CatMineralesReservados m : l) {
            SelectItem temp = new SelectItem(m, m.getDescripcionMinerales());
            lista.add(temp);
        }
        return lista;
    }
    
    public List<Object[]> proyectoObras(String numFolio, short serial, short preguntaId) { //tabla proyecto de MIA-E, con sus equivalntes en catlogos
       
        Query q = em.createNativeQuery("SELECT B.OBRA_DESCRIPCION,A.DIMENSIONES_OBRA,C.CTUN_DESC,A.SUPERFICIE_OBRA,C.CTUN_DESC as DES2,A.AFECTACION_OBRA " +
        "FROM DGIRA_MIAE2.PREGUNTA_OBRA A, DGIRA_MIAE2.CAT_OBRA B, CATALOGOS.CAT_UNIDAD_MEDIDA C " +
        "WHERE A.FOLIO_PROYECTO='" + numFolio + "' AND A.SERIAL_PROYECTO=" + serial + " AND A.PREGUNTA_ID=" + preguntaId + " AND A.OBRA_ID = B.OBRA_ID " +
        "AND A.DIMENSIONES_UNIDAD = C.CTUN_CLVE AND A.SUPERFICIE_UNIDAD = C.CTUN_CLVE");
        
        List<Object[]> l = q.getResultList();
        return l;       
    } 
    
    
    
//-------------------supuesto2  CatParqueIndustrial
    public CatParqueIndustrial descParqInd(Short idParqInd) {
        Query q = emfMIAE.createQuery("SELECT p FROM CatParqueIndustrial p WHERE p.parqueId =:id");
        q.setParameter("id", idParqInd);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (CatParqueIndustrial) o;
    }
    
    public ParqueIndustrialProyecto parqInd(String folioProyecto, short serialProyecto) {
        Query q = emfMIAE.createQuery("SELECT p FROM ParqueIndustrialProyecto p WHERE p.parqueIndustrialProyectoPK.folioProyecto=:folio and p.parqueIndustrialProyectoPK.serialProyecto=:serial ");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (ParqueIndustrialProyecto) o;
    }
 //-------------------supuesto3  CatParqueIndustrial
    public CatPdu descPDU(Short idPDU) {
        Query q = emfMIAE.createQuery("SELECT p FROM CatPdu p WHERE p.pduId =:id");
        q.setParameter("id", idPDU);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (CatPdu) o;
    }
    
    public PduProyecto PDU(String folioProyecto, short serialProyecto) {
        Query q = emfMIAE.createQuery("SELECT p FROM PduProyecto p WHERE p.pduProyectoPK.folioProyecto =:folio and p.pduProyectoPK.serialProyecto=:serial ");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (PduProyecto) o;
    }
    
    public CatPoet descPOET(Short idPDU) {
        Query q = emfMIAE.createQuery("SELECT p FROM CatPoet p WHERE p.poetId =:id");
        q.setParameter("id", idPDU);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (CatPoet) o;
    }
    
    public PoetProyecto POET(String folioProyecto, short serialProyecto) {
        Query q = emfMIAE.createQuery("SELECT p FROM PoetProyecto p WHERE p.poetProyectoPK.folioProyecto =:folio and p.poetProyectoPK.serialProyecto=:serial ");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return (PoetProyecto) o;
    }
    
    public List<?> listado_where(Class<?> tipo, String atributeName, Object o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName), o));
        return emfMIAE.createQuery(cq).getResultList();
    }
    
    //------------------CAPITULO 2
    
    public List<Object[]> proyectoCat(String bitacora, String numFolio, short serial) { //tabla proyecto de MIA-E, con sus equivalntes en catlogos
       
        Query q = em.createNativeQuery("select A.FOLIO_PROYECTO, A.SERIAL_PROYECTO, A.CLAVE_PROYECTO, A.BITACORA_PROYECTO, A.PROY_SUPUESTO, A.PROY_NOMBRE, G.SECTOR, F.SUBSECTOR, E.RAMA, D.TIPO_PROYECTO, H.NOMBRE_ENTIDAD_FEDERATIVA , I.NOMBRE_DELEGACION_MUNICIPIO, A.PROY_DOM_ESTABLECIDO, C.DESCRIPCION , A.PROY_NOMBRE_VIALIDAD, A.PROY_NUMERO_EXTERIOR, A.PROY_NUMERO_INTERIOR, B.NOMBRE , A.PROY_NOMBRE_ASENTAMIENTO, A.PROY_CODIGO_POSTAL, A.PROY_UBICACION_DESCRITA, A.PROY_INVERSION_REQUERIDA, A.PROY_MEDIDAS_PREVENCION, A.PROY_EMPLEOS_PERMANENTES, A.PROY_EMPLEOS_TEMPORALES, A.PROY_TIEMPO_VIDA_ANIOS, A.PROY_TIEMPO_VIDA_MESES, A.PROY_TIEMPO_VIDA_SEMANAS, A.PROY_DESC_MEDIDAS_COMPEN, A.PROY_DESC_PROTEC_CONSERV, A.PROY_DESC_PARTICULAR, A.PROY_DESC_AMBIENTE, A.PROY_DIAGRAMA_GANTT, A.PROY_RESP_TEC_IGUAL_LEGAL, A.RESP_TEC_ID, A.ESTUDIO_RIESGO_ID, A.PROY_INVERSION_FEDERAL_ORI, A.PROY_INVERSION_ESTATAL_ORI, A.PROY_INVERSION_MUNICIPAL_ORI, A.PROY_INVERSION_PRIVADA_ORI, A.PROY_REPRESENTANTE_ID, A.PROY_DESC_NAT, A.PROY_DEMANDA_SERV, A.PROY_DESC_PAISAJE, A.PROY_DIAG_AMBIENTAL, A.PROY_JUST_METODOLOGIA, A.PROY_REQUI_MONTOS, A.PROY_ESCE_SIN_P, A.PROY_ESCE_CON_P, A.PROY_ESCE_CON_PYMED, A.PROY_DESC_EVALU_ALTER, A.ESTATUS_PROYECTO, A.PROY_LOCALIDAD, A.PROY_NORMA_ID, A.PROY_SUP_POET_PDU,  A.PROY_LOTE, A.PROY_VALOR_CRITERIO, A.PROY_CRITERIO_MONTO FROM DGIRA_MIAE2.PROYECTO A,  CATALOGOS.CAT_VIALIDAD C, CATALOGOS.CAT_TIPO_ASEN B,  CATALOGOS.TIPO_PROYECTO D, CATALOGOS.RAMA_PROYECTO E,CATALOGOS.SUBSECTOR_PROYECTO F, CATALOGOS.SECTOR_PROYECTO G, CATALOGOS.ENTIDAD_FEDERATIVA H, CATALOGOS.DELEGACION_MUNICIPIO I  WHERE A.BITACORA_PROYECTO = '" + bitacora + "' AND A.FOLIO_PROYECTO = '" +  numFolio+ "'   AND A.SERIAL_PROYECTO = " + serial + " AND A.NSEC = G.NSEC AND A.NSEC = F.NSEC AND A.NSUB = F.NSUB AND A.NSUB = E.NSUB AND A.NRAMA = E.NRAMA  AND A.NRAMA = D.NRAMA  AND A.NTIPO = D.NTIPO AND A.PROY_ENT_AFECTADO = H.ID_ENTIDAD_FEDERATIVA AND  A.PROY_MUN_AFECTADO = I.ID_DELEGACION_MUNICIPIO   AND A.CVE_TIPO_VIAL = C.CVE_TIPO_VIAL  AND A.TIPO_ASENTAMIENTO_ID = B.CVE_TIPO_ASEN");
        
        List<Object[]> l = q.getResultList();
        return l;       
    }  //
    
    public SectorProyecto sectorProy(Short nsec) { 
       
        Query q = emfBitacora.createQuery("select a from SectorProyecto a where a.nsec =:sec");
        q.setParameter("sec", nsec);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (SectorProyecto) o;   
    }
    
    public SubsectorProyecto subSecProy(Short nsec, Short nsub) { 
       
        Query q = emfBitacora.createQuery("select a from SubsectorProyecto a where a.nsec =:sec and a.nsub =:sub");
        q.setParameter("sec", nsec);
        q.setParameter("sub", nsub);
        Object o = null;
        try {
            o = q.getSingleResult();
       
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (SubsectorProyecto) o;   
    }
    
    public RamaProyecto ramaProy(Short nsub, Short nrama) { 
       
        Query q = emfBitacora.createQuery("select a from RamaProyecto a where a.nsub =:sub and a.nrama =:rama");
        q.setParameter("sub", nsub);
        q.setParameter("rama", nrama);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (RamaProyecto) o;   
    }
    
    public TipoProyecto tipoProy(Short nrama, Short ntipo) { 
       
        Query q = emfBitacora.createQuery("select a from TipoProyecto a where a.nrama =:rama and a.ntipo =:tipo");
        q.setParameter("rama", nrama);
        q.setParameter("tipo", ntipo);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (TipoProyecto) o;   
    }
    
    public List<Object[]> edoMasAfectado(String Folio, Short version, Integer id) {
//        EntityManager em = emf1.createEntityManager();

        Query q = emfMIAE.createQuery("SELECT a.cEdomun , a.clvMunici , a.nomEstado, a.nomMunici "
                + "FROM MpiosCruzadavshambre a "
                + " WHERE a.mpiosCruzadavshambrePK.numFolio='" + Folio
                + "' and a.mpiosCruzadavshambrePK.version =" + version
                + " and a.mpiosCruzadavshambrePK.idr =" + id);

        List l = q.getResultList();
//        em.close();
        return l;
    }

    public List<Object[]> edoTodosAfectado(String Folio, short version) {
        System.out.println("folioNum  " + Folio + "    vers  " + version);

        //Primero se verifica si existen obras
        Query q = emfMIAE.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'OBRA' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                .setParameter("vers", version);

        String vObra = q.getSingleResult().toString();

        //Se verifica si existen predios.
        q = emfMIAE.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'PREDIO' and a.mpiosCruzadavshambrePK.numFolio=:folioNum  and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                .setParameter("vers", version);

        String vPredio = q.getSingleResult().toString();

        String vFiltroPredioObra = "";

        if (Integer.valueOf(vObra) > 0) {
            vFiltroPredioObra = "OBRA";
        } else if (Integer.valueOf(vPredio) > 0) {
            vFiltroPredioObra = "PREDIO";
        };

        System.out.println("---- Obras " + vObra + "---  Predios  " + vPredio);
        
               q = emfMIAE.createQuery("SELECT a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici, to_char(round(sum(a.area), 2), 'fm999,999,999,999,999.00' ) FROM MpiosCruzadavshambre a "
                       + "WHERE a.comp = '"+ vFiltroPredioObra + "' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers"
                       + " GROUP BY a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici")
                .setParameter("folioNum", Folio)
                .setParameter("vers", version);

        //Regresa la lista de resultados
        return q.getResultList();
    }
        
    public List<Object[]> proySigSum(String Folio, String ClveProy, short version) {
        Query q;
        q = em.createQuery("SELECT a.proysigPK.cveArea, a.comp, sum(a.shapeArea) FROM Proysig a  WHERE a.proysigPK.numFolio=:fol  and a.proysigPK.cveProy=:clveProy and a.proysigPK.version=:versions GROUP BY a.comp, a.proysigPK.cveArea  ");
        q.setParameter("fol", Folio);
        q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        return l;
    }
    
    public List<Object[]> mpiosSumObra(String Folio, short version) {
        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = em.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio  AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = em.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from dgira_miae2.mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='OBRA')");
            q.setParameter("folio", Folio);
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            q2 = em.createNativeQuery("select m.idr from dgira_miae2.mpios_cruzadavshambre m where m.area = "
                    + "(select distinct max(m.area) from dgira_miae2.mpios_cruzadavshambre where num_folio=:folio and version=:version) "
                    + "and num_folio = :folio and version=:version and nom_estado=:estado AND comp='OBRA' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("estado", string);
            q2.setParameter("version", version);
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }

    public List<Object[]> mpiosSumPred(String Folio, short version) {
        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = em.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio  AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = em.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from dgira_miae2.mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='PREDIO')");
            q.setParameter("folio", Folio);
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            q2 = em.createNativeQuery("select m.idr from dgira_miae2.mpios_cruzadavshambre m where m.area = "
                    + "(select distinct max(m.area) from dgira_miae2.mpios_cruzadavshambre where num_folio=:folio and version=:version) "
                    + "and num_folio = :folio and version=:version and nom_estado=:estado AND comp='PREDIO' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("estado", string);
            q2.setParameter("version", version);
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }
    
    public CatVialidad tipoVialidad(Short tipVialidad) { 
       
        Query q = emfBitacora.createQuery("select a from CatVialidad a where a.cveTipoVial =:tipVial");
        q.setParameter("tipVial", tipVialidad);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (CatVialidad) o;   
    }
    
    public CatTipoAsen tipoAsenta(Short tipoAsent) { 
       
        Query q = emfBitacora.createQuery("select a from CatTipoAsen a where a.cveTipoAsen =:ntipoAsen");
        q.setParameter("ntipoAsen", tipoAsent);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (CatTipoAsen) o;   
    }
    
    public List<AnexosProyecto> getAnexos(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        List<AnexosProyecto> r = new ArrayList();
        try {
            if (capituloId != 0) {
            	
                Query q = emfMIAE.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
                q.setParameter("apartado", apartadoId);
                q.setParameter("capitulo", capituloId);
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("seccion", seccionId);
                q.setParameter("subcapitulo", subcapituloId);
                r = q.getResultList();
                //em.close();
            } else {
                //obtiene todos los adjuntos del folio               
                Query q = emfMIAE.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                r = q.getResultList();
                //em.close();
            }
        } catch (Exception e) {

        }

        return r;

    }
    
    
    public List<ArchivosProyecto> getArchivosProyecto(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        List<ArchivosProyecto> r = new ArrayList<ArchivosProyecto>();
        try {
            if (capituloId != 0) {
                
                Query q = emfMIAE.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.apartadoId = :apartado and a.capituloId = :capitulo and a.folioSerial = :folio and a.serialProyecto = :serial and a.seccionId = :seccion and a.subCapituloId = :subcapitulo");
                q.setParameter("apartado", apartadoId);
                q.setParameter("capitulo", capituloId);
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("seccion", seccionId);
                q.setParameter("subcapitulo", subcapituloId);
                r = q.getResultList();
                //em.close();
            } else {
                //obtiene todos los adjuntos del folio
                
                Query q = emfMIAE.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.folioSerial = :folio and a.serialProyecto = :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                r = q.getResultList();
                //em.close();
            }
        } catch (Exception e) {

        }

        return r;

    }
    
    public List<Object[]> proySigSupProyecto(String Folio, short version) {
        
        Query q = emfMIAE.createQuery("SELECT a.comp, a.descrip, to_char(round(a.shapeArea, 2), 'fm999,999,999,999,999.00') FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions ORDER BY a.comp "); // and a.proysigPK.version=:vers
        q.setParameter("fol", Folio);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        return l;
    }
      
    public List<Object[]> proySigSumObras(String Folio, short version) {
//        EntityManager em = emf1.createEntityManager();
        //JOptionPane.showMessageDialog(null, "Folio:  " + Folio + "  Clave: " +ClveProy + "   versio: " + version, "Error", JOptionPane.INFORMATION_MESSAGE);  
       
        Query q = emfMIAE.createQuery("SELECT to_char(round(sum(a.shapeArea), 2), 'fm999,999,999,999,999.00' ), to_char(round(sum(a.shapeArea), 2), 'fm999,999,999,999,999.00'), to_char(round(sum(a.shapeArea), 2), 'fm999,999,999,999,999.00') FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions and a.comp='OBRA'"); // and a.proysigPK.version=:vers
        q.setParameter("fol", Folio);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        
        return l;
    }

    public List<Object[]> proySigSumPredios(String Folio, short version) {
//        EntityManager em = emf1.createEntityManager();
        //JOptionPane.showMessageDialog(null, "Folio:  " + Folio + "  Clave: " +ClveProy + "   versio: " + version, "Error", JOptionPane.INFORMATION_MESSAGE);  
        Query q = emfMIAE.createQuery("SELECT to_char(round(sum(a.shapeArea), 2), 'fm999,999,999,999,999.00'), to_char(round(sum(a.shapeArea), 2), 'fm999,999,999,999,999.00'), to_char(round(sum(a.shapeArea), 2), 'fm999,999,999,999,999.00') FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions and a.comp='PREDIO'"); // and a.proysigPK.version=:vers
        q.setParameter("fol", Folio);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        //JOptionPane.showMessageDialog(null, "refenrencia1111:  " + l.size(), "Error", JOptionPane.INFORMATION_MESSAGE);  
        //em.clear();
        return l;
    }
    
    //------------------SINATEC---------------------- 
    public List<Vexdatosusuariorep> datosPrepLeg(Integer idTramite) {
        //EntityManager emS = emfSinatec.createEntityManager();
        Query q = emfBitacora.createQuery("SELECT DISTINCT a FROM Vexdatosusuariorep a WHERE a.bgtramiteid=:idTramite");
        q.setParameter("idTramite", idTramite);
        List l = q.getResultList();
//        em.close();
        return l;
    }

    public void borraRepLegal(String id) {//metodo para borrar todos los representantes legales ligados al proyecto
        emfBitacora.clear();
        emfBitacora.getTransaction().begin();
        Query q = emfBitacora.createQuery("DELETE FROM RepLegalProyecto r WHERE "
                + "r.repLegalProyectoPK.folioProyecto = :folioProyecto");
        q.setParameter("folioProyecto", id);
        q.executeUpdate();
        emfBitacora.getTransaction().commit();
    }

    public Vexdatosusuariorep repLegPorCurp(String curp, Integer id) {
       // EntityManager emS = emfSinatec.createEntityManager();
        Query q = emfBitacora.createQuery("SELECT v FROM Vexdatosusuariorep v WHERE v.vccurp = :vccurp AND v.bgtramiteid=:id");
        q.setParameter("vccurp", curp);
        q.setParameter("id", id);
        return (Vexdatosusuariorep) q.getSingleResult();
    }

    public List<Vexdatosusuario> datosPromovente(Integer idTramite) {
       //EntityManager em = emfSinatec.createEntityManager();
        Query q = emfBitacora.createQuery("SELECT DISTINCT a  FROM Vexdatosusuario a WHERE a.bgtramiteid=:idTramite");
        q.setParameter("idTramite", idTramite);
        List l = q.getResultList();
//        em.close();
        return l;
    }

    public List<Object[]> datosPromovente2(Integer idTramite) {
       // EntityManager em = emfSinatec.createEntityManager();
        Query q = emfBitacora.createQuery("SELECT DISTINCT b.descripcion, b.descripcion  FROM Vexdatosusuario a, hola.CatVialidad b  WHERE  a.bgcveTipoVialLk=b.cveTipoVial   and  a.bgtramiteid=:idTramite");
        q.setParameter("idTramite", idTramite);
        List l = q.getResultList();
//        em.close();
        return l;
    }
    
    //-------------------------------------
    public List<?> listado_where_comp(Class<?> tipo, String atributeName, String nestedAttrib, Object o) {
        CriteriaBuilder cBuilder = emfMIAE.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName).get(nestedAttrib), o));
        return emfMIAE.createQuery(cq).getResultList();
    }
    
    public List<?> listado_where_compImpactoProy(Class<?> tipo, String[] atributeName, String[] nestedAttrib, Object[] o) {
        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName[0]).get(nestedAttrib[0]), o[0]),
                cBuilder.equal(r.get(atributeName[1]).get(nestedAttrib[1]), o[1]));
        return em.createQuery(cq).getResultList();
    }
    
    
    public List<?> listado_where_compSust(Class<?> tipo, String[] atributeName, String[] nestedAttrib, Object[] o) {
     
        CriteriaBuilder cBuilder = emfMIAE.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        Root<?> r = cq.from(tipo);
        cq.select(r);
        cq.where(cBuilder.equal(r.get(atributeName[0]).get(nestedAttrib[0]), o[0]),
                cBuilder.equal(r.get(atributeName[1]).get(nestedAttrib[1]), o[1]));
        return emfMIAE.createQuery(cq).getResultList();
      
    }
    
    public Object busca(Class<?> tipo, Object id) {
        return emfMIAE.find(tipo, id);
    }
    
    public Object buscaCat(Class<?> tipo, Object id) {
        return em.find(tipo, id);
    }
    
    public RespTecProyecto repTecProy(String numFolio, short serial) { 
       
        Query q = emfMIAE.createQuery("select a from RespTecProyecto a where a.respTecProyectoPK.folioProyecto=:folio and a.respTecProyectoPK.serialProyecto=:serial");
        q.setParameter("folio", numFolio);
        q.setParameter("serial", serial);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (RespTecProyecto) o;   
    }
    
    public List<?> listado(Class<?> tipo) {
        CriteriaBuilder cBuilder = emfBitacora.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cBuilder.createQuery();
        cq.select(cq.from(tipo));
        return emfBitacora.createQuery(cq).getResultList();
    }
    
    public CatUnidadMedida buscaUnid(Short claveUnid)
    {
        Query q = emfBitacora.createQuery("select a from CatUnidadMedida a where a.ctunClve=:clave");
        q.setParameter("clave", claveUnid);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (CatUnidadMedida) o;  
    }
    //-----------------CAPITULO 3
    
        
    public List<Object[]> sustanciaProy(String numFolio, short serial) 
    {
        Query q = emfMIAE.createQuery("SELECT A.sustanciaProyectoPK.folioProyecto , A.sustanciaProyectoPK.serialProyecto , C.etapaDescripcion , CASE WHEN (B.sustanciaDescripcion = 'Otra') THEN CONCAT(B.sustanciaDescripcion ,': ' ,A.sustanciaPromovente) ELSE B.sustanciaDescripcion END , A.sustanciaCantidadAlmacenada , F.ctunDesc , A.sustanciaPromovente , E.sustanciaAnexoPK.sustanciaAnexoId, E.anexoNombre, E.anexoDesc FROM SustanciaProyecto A, CatSustanciaAltamRiesgosa B, CatEtapa C, SustanciaAnexo E, CatUnidadMedida F WHERE A.sustanciaProyectoPK.folioProyecto =:folio AND A.sustanciaProyectoPK.serialProyecto =:serial AND A.sustanciaId = B.sustanciaId AND A.etapaId = C.etapaId AND E.sustanciaAnexoPK.folioProyecto = A.sustanciaProyectoPK.folioProyecto AND E.sustanciaAnexoPK.serialProyecto = A.sustanciaProyectoPK.serialProyecto AND A.sustanciaProyectoPK.sustProyId = E.sustanciaAnexoPK.sustanciaProyId AND A.ctunClve = F.ctunClve ");
        q.setParameter("folio", numFolio);
        q.setParameter("serial", serial);
        
        List l = q.getResultList();        
        System.out.println("Size of sustancias :::::::::::::::: " + (l != null ? l.size() : 0));
        
        return l;
    }
    
    
    public List<Object[]> emiResDesProy(String numFolio, short serial)      
    {
        Query q = emfMIAE.createQuery("SELECT A.contaminanteProyectoPK.folioProyecto , A.contaminanteProyectoPK.serialProyecto , F.tipoContaminanteDescripcion , B.contaminanteNombre , E.etapaDescripcion , A.contaminanteCantidad , C.ctunDesc , A.contaminanteMedidaControl FROM ContaminanteProyecto A, CatContaminante B, CatUnidadMedida C, CatEtapa E, CatTipoContaminante F WHERE A.contaminanteProyectoPK.folioProyecto =:folio AND A.contaminanteProyectoPK.serialProyecto =:serial AND A.etapaId = E.etapaId AND A.contaminanteProyectoPK.contaminanteId = B.contaminanteId AND B.tipoContaminanteId = F.tipoContaminanteId AND A.ctunClave = C.ctunClve");
        q.setParameter("folio", numFolio);
        q.setParameter("serial", serial);
        List l = q.getResultList();
        System.out.println("Size of residuos :::::::::::::::: " + (l != null ? l.size() : 0));
        return l;
    }
    
    
    public List<Object[]> impactoProy(String numFolio, short serial)      
    {
        Query q = emfMIAE.createQuery("SELECT A.impactoProyectoPK.folioProyecto , A.impactoProyectoPK.serialProyecto , C.etapaDescripcion , A.impactoMedidaMitigacion , A.impactoMedidaAdicional , A.impactoDescripcion FROM ImpactoProyecto A, CatEtapa C WHERE A.impactoProyectoPK.folioProyecto =:folio AND A.impactoProyectoPK.serialProyecto=:serial AND A.etapaId = C.etapaId ");
        q.setParameter("folio", numFolio);
        q.setParameter("serial", serial);
        List l = q.getResultList();
        return l;
    }
    
    
    //--------------comienza capitulo 2 de MIA
    
    public List<Object[]> getCriterios(String folio, short serial) {
        Query q = emfMIAE.createQuery("SELECT A.criteriosProyectoPK.folioProyecto, A.criteriosProyectoPK.serialProyecto, B.criterioNombre, A.criterioDescripcion, A.criterio, A.criterioId, A.criteriosProyectoPK.criterioProyId FROM CriteriosProyecto A, CatCriterio B WHERE A.criteriosProyectoPK.folioProyecto = :folio and A.criteriosProyectoPK.serialProyecto =:serial   AND A.criterioId = B.criterioId ");
        
        //Se agrego el campo faltante en la entidad y en la consulta de la tabla
        
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        List l = q.getResultList();
        return l;
    }
    
    public List<MedPrevImpactProy> getMedPreventivas(String folio, short serial) {
        Query q = emfMIAE.createQuery("SELECT m FROM MedPrevImpactProy m WHERE m.medPrevImpactProyPK.folioProyecto = :folio AND m.medPrevImpactProyPK.serialProyecto = :serial");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        List l = q.getResultList();
        return l;
    }
    
    /*
     * M�todo para Obtener la informaci�n de impacto ambiental
     * */
    
    public ImpacAmbProyecto impactoAmbiente(String folio, Short serial) {
        Query q = emfMIAE.createQuery("SELECT i FROM ImpacAmbProyecto i WHERE i.impacAmbProyectoPK.folioProyecto = :folio AND i.impacAmbProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
       List l = q.getResultList();
       return (ImpacAmbProyecto) l;
    }
    
    
    public EstudioRiesgoProyecto cargaEstudioRiesgo(Proyecto proyecto) {
        EstudioRiesgoProyecto e;

        System.out.println("Folio " + proyecto.getProyectoPK().getFolioProyecto());
        System.out.println("Serial " + proyecto.getProyectoPK().getFolioProyecto());

        try {
            Query q = emfMIAE.createQuery("SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRiesgoProyectoPK.folioProyecto = :folio and e.estudioRiesgoProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
            q.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
            e = (EstudioRiesgoProyecto) q.getSingleResult();
        } catch (NoResultException err) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        }

        if (e == null) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

        }

        return e;
    }
    
    public List<EspecificacionAnexo> getEspecifiacionesAnexos(String folio, String especifiacionId, Short serialProeycto, Short normaId) {
      
        Query q = emfMIAE.createQuery("SELECT a FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.especificacionId = :especificacion and a.especificacionAnexoPK.serialProyecto = :serial and a.especificacionAnexoPK.normaId = :norma");
        q.setParameter("folio", folio);
        q.setParameter("especificacion", especifiacionId);
        q.setParameter("serial", serialProeycto);
        q.setParameter("norma", normaId);
        List l = q.getResultList();

        return l;
    }
    
    public List<SustanciaAnexo> getSustanciaAnexos(String folioProyecto, Short serialProyecto, Short sustanciaProyId) {
        
        List<SustanciaAnexo> hs;
            if(sustanciaProyId!=null){
                Query q = emfMIAE.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto = :serial AND s.sustanciaAnexoPK.sustanciaProyId= :id");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("id", sustanciaProyId);
                hs = q.getResultList();
            }else{
                Query q = emfMIAE.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto= :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                hs = q.getResultList();
            }
            return hs;
    }
    
    
    public ComentarioProyecto buscaComentario(String bitacoraProy, Short capituloId, Short subcapituloId, Short seccionId, Short apartadoId ) { //, Integer idusuario
        Query q = emfMIAE.createQuery("SELECT p FROM ComentarioProyecto p WHERE p.comentarioProyectoPK.bitacoraProyecto=:bitacora and p.comentarioProyectoPK.capituloId=:capitulo and p.comentarioProyectoPK.subcapituloId=:subcapitulo and p.comentarioProyectoPK.seccionId=:seccion and p.comentarioProyectoPK.apartadoId=:apartado"); // and p.comentarioProyectoPK.comentarioIdusuario=:Idusuario
        q.setParameter("bitacora", bitacoraProy); //capitulo
        q.setParameter("capitulo", capituloId);
        q.setParameter("subcapitulo", subcapituloId);
        q.setParameter("seccion", seccionId);
        q.setParameter("apartado", apartadoId);
       // q.setParameter("Idusuario", idusuario);
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (ComentarioProyecto) o;
    }
       
    public void agrega(Object object) {
        try {
            emfMIAE.getTransaction().begin();
            emfMIAE.persist(object);
            emfMIAE.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            if (emfMIAE.getTransaction().isActive()) {
                emfMIAE.getTransaction().rollback();
            }
        } finally {
        }
    }
    
    public List<Object[]> proySig2(String Folio, short version) {
    	Query q = emfMIAE.createQuery("SELECT a.comp, a.descrip, to_char(round(a.shape/10000,4),'999,999,999.9999'),to_char(round(a.shape,2),'999,999,999,999.99') "
                + "FROM Proysig a WHERE NUM_FOLIO = :fol and VERSION = :versions");
        q.setParameter("fol", Folio);
        q.setParameter("versions", version);
        return q.getResultList();
    }
    
    public String evaProyecto(String bitacora) { 
        String cadena = "";
        Query q = emfMIAE.createQuery("SELECT p.evaSistesisProyecto FROM EvaluacionProyecto p WHERE p.bitacoraProyecto =:bitacoraProyecto");
        q.setParameter("bitacoraProyecto", bitacora);
        Object o = null;

        try {
            cadena = q.getSingleResult().toString();
        } catch (NoResultException e) {
            System.out.println("EROR: Bitacora no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
//            e.printStackTrace();
        }
        return cadena;
    }
    
    public List<Object[]> mpiosSumObra(String Folio, String ClveProy, short version) {
        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = emfMIAE.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);
        //q.setParameter("cve", ClveProy);
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = emfMIAE.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='OBRA')");
            q.setParameter("folio", Folio);
            //q.setParameter("cve", ClveProy);
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            
            q2 = emfMIAE.createNativeQuery("select m.idr from mpios_cruzadavshambre m where m.area = ("
                    + "select distinct max(m.area) from mpios_cruzadavshambre where num_folio=:folio and version = :version) and num_folio = :folio and version = :version and nom_estado=:estado AND comp='OBRA' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("version", version);
            q2.setParameter("estado", string);
            
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }

    public List<Object[]> mpiosSumPred(String Folio, String ClveProy, short version) {
        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = emfMIAE.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);
        //q.setParameter("cve", ClveProy);
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = emfMIAE.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='PREDIO')");
            q.setParameter("folio", Folio);
          //  q.setParameter("cve", ClveProy);
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            q2 = emfMIAE.createNativeQuery("select m.idr from mpios_cruzadavshambre m where m.area = ("
                    + "select distinct max(m.area) from mpios_cruzadavshambre where num_folio=:folio) and num_folio = :folio and version = :version and nom_estado=:estado AND comp='PREDIO' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("version", version);
            q2.setParameter("estado", string);
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }

    public List<Object[]> edoMasAfectado(String Folio, String ClveProy, Short version, Integer id) {
        Query q = emfMIAE.createQuery("SELECT a.cEdomun , a.clvMunici , a.nomEstado, a.nomMunici "
                + "FROM MpiosCruzadavshambre a "
                + " WHERE a.mpiosCruzadavshambrePK.numFolio='" + Folio
                //+ "' and a.mpiosCruzadavshambrePK.cveProy='" + ClveProy
                + "' and a.mpiosCruzadavshambrePK.version =" + version
                + " and a.mpiosCruzadavshambrePK.idr =" + id);

        List l = q.getResultList();
        return l;
    }

    public List<Object[]> edoTodosAfectado(String Folio, String ClveProy, short version) {
        System.out.println("folioNum  " + Folio + "   proyClve " + ClveProy + "   vers  " + version);

        //Primero se verifica si existen obras
        Query q = emfMIAE.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'OBRA' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        String vObra = q.getSingleResult().toString();

        //Se verifica si existen predios.
        q = emfMIAE.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'PREDIO' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        String vPredio = q.getSingleResult().toString();

        String vFiltroPredioObra = "";

        if (Integer.valueOf(vObra) > 0) {
            vFiltroPredioObra = "OBRA";
        } else if (Integer.valueOf(vPredio) > 0) {
            vFiltroPredioObra = "PREDIO";
        };

        System.out.println("---- Obras " + vObra + "---  Predios  " + vPredio);
        
               q = emfMIAE.createQuery("SELECT a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici, sum(a.area) FROM MpiosCruzadavshambre a "
                       + "WHERE a.comp = '"+ vFiltroPredioObra + "' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers"
                       + " GROUP BY a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        //Regresa la lista de resultados
        return q.getResultList();
    }

    
    
    
}
