/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import mx.gob.semarnat.model.seguridad.Tbarea;
import mx.gob.semarnat.model.seguridad.Tbusuario;

/**
 *
 * @author Paty
 */
@SuppressWarnings("serial")
public class LoginDAO implements Serializable {

    EntityManager emfLogin = PersistenceManager.getEmfBitacora().createEntityManager();

    public Object busca(Class<?> tipo, Object id) {
        return emfLogin.find(tipo, id);
    }

    public Tbusuario autenticacion(String usuario, String password) {
        Query q = emfLogin.createQuery("SELECT a FROM Tbusuario a WHERE a.usuario =:usua and a.contrasena=:pass AND a.estatus=1");
        q.setParameter("usua", usuario);
        q.setParameter("pass", password);

        try {
            return (Tbusuario) q.getSingleResult();
        } catch (Exception e) {
        	System.out.println(e.getMessage());
        	e.printStackTrace();
        }
        return new Tbusuario();
    }
    
    public Tbusuario getUsuario(Integer idUsuario) {
    	
    	try {
    		Query q = emfLogin.createNamedQuery("Tbusuario.findByIdusuario");
            q.setParameter("idusuario", idUsuario);
            return (Tbusuario) q.getSingleResult();
        } catch (Exception e) {
        }
        return null;
    }

    public Tbarea buscaPerf(Tbusuario tbUsuario) {
        Query q = emfLogin.createQuery("SELECT v FROM Tbarea v WHERE v.tbareaPK.idarea =:area AND v.tbareaPK.iddependencia =:dependencia");
        q.setParameter("area", tbUsuario.getIdarea());
        q.setParameter("dependencia", tbUsuario.getIddependencia());
        return (Tbarea) q.getSingleResult();
    }

}
