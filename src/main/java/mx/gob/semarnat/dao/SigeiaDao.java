/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import mx.gob.semarnat.model.dgira_miae.Acuiferos;
import mx.gob.semarnat.model.dgira_miae.AnpEstatal;
import mx.gob.semarnat.model.dgira_miae.AnpFederal;
import mx.gob.semarnat.model.dgira_miae.AnpMuicipal;
import mx.gob.semarnat.model.dgira_miae.AnpProyecto;
import mx.gob.semarnat.model.dgira_miae.Climas;
import mx.gob.semarnat.model.dgira_miae.HidrologiaProyecto;
import mx.gob.semarnat.model.dgira_miae.InstrUrbanos;
import mx.gob.semarnat.model.dgira_miae.Microcuenca;
import mx.gob.semarnat.model.dgira_miae.OeGralTerrit;
import mx.gob.semarnat.model.dgira_miae.OeMarinos;
import mx.gob.semarnat.model.dgira_miae.OePoligEnvol;
import mx.gob.semarnat.model.dgira_miae.OeRegionales1;
import mx.gob.semarnat.model.dgira_miae.OeRegionales2;
import mx.gob.semarnat.model.dgira_miae.OeRegionales3;
import mx.gob.semarnat.model.dgira_miae.Proysig;
import mx.gob.semarnat.model.dgira_miae.UsoSueloVeget;

/**
 *
 * @author mauricio
 */
public class SigeiaDao {

	private final EntityManager emfMIAE = PersistenceManager.getEmfMIAE().createEntityManager();
    //EntityManager sin = EntityFactory.getSinatec().createEntityManager();

    public List<AnpProyecto> anpProyecto(String folio, Short serial) {
        Query q = emfMIAE.createQuery("SELECT a FROM AnpProyecto a  WHERE a.anpProyectoPK.folioProyecto = :fol and a.anpProyectoPK.serialProyecto = :clveProy ");
        q.setParameter("fol", folio);
        q.setParameter("clveProy", serial);
        System.out.println("Anpbusca");
        return q.getResultList();
    }

    public List<InstrUrbanos> getInstrUbr(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT a FROM InstrUrbanos a  WHERE a.instrUrbanosPK.numFolio = :fol and a.instrUrbanosPK.version = :clveProy");
        q.setParameter("fol", folio);
        q.setParameter("clveProy", serial);        
        return q.getResultList();
    }

    public List<Object[]> getInstrUbr2(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT a.comp, a.descrip, a.usoclaspol, a.clvUsocp, a.instUrb, sum(a.area)  FROM InstrUrbanos a  WHERE a.instrUrbanosPK.numFolio = :fol and a.instrUrbanosPK.version = :clveProy group by a.comp, a.descrip, a.usoclaspol, a.clvUsocp, a.instUrb");
        q.setParameter("fol", folio);
        q.setParameter("clveProy", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }
    
    public List<Object[]> usoSueloVegetacion2(String Folio, String ClveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT a.comp, a.descrip, a.tipEcov, a.tipoGen ,a.faseVs, round(sum(a.area),2) as areaM2, round(sum(a.area)/10000,4) as ha, a.tipEcov FROM UsoSueloVeget a  WHERE a.usoSueloVegetPK.numFolio = :fol and a.usoSueloVegetPK.version = :versions group by a.comp, a.descrip, a.tipEcov, a.tipoGen ,a.faseVs order by a.comp ");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        return q.getResultList();
    }
    
    
    public List<UsoSueloVeget> usoSueloVegetacion(String Folio, String ClveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT a FROM UsoSueloVeget a  WHERE a.usoSueloVegetPK.numFolio = :fol and a.usoSueloVegetPK.version = :versions ");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        return q.getResultList();
    }
    /**
     * Permite recuperar la lista de registros de Caracteristicas Particulares del proyecto de SIGEIA.
     * @param folio
     * @param serial
     * @param idCaracteristica
     * @return
     */
    public List<Object[]> consultarCaracPartProySIGEIA(String folio) throws Exception {
        Query q = emfMIAE.createQuery("SELECT usv.comp, usv.area, to_char(round(sum(usv.area),2),'999G999G999G999D99') as sueloAreaSigeia "
        						+ "FROM UsoSueloVeget usv WHERE usv.usoSueloVegetPK.numFolio = :folio AND usv.comp = 'OBRA' group by usv.comp, usv.area");
        q.setParameter("folio", folio);
        return q.getResultList();
    }    

    public List<Proysig> proySig(String Folio, String ClveProy, short version) {
//        Query q = sig.createQuery("SELECT a FROM Proysig a  WHERE a.proysigPK.numFolio = :fol and a.proysigPK.cveProy = :clveProy and a.proysigPK.version = :versions ");
        Query q = emfMIAE.createNativeQuery("SELECT * FROM");
//        Query q = sig.createQuery("SELECT a FROM Proysig a  WHERE a.numFolio = :fol and a.cveProy = :clveProy and a.version = :versions ");
        q.setParameter("fol", Folio);
        q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        return q.getResultList();
    }

    public List<Object[]> proySig2(String Folio, String ClveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT a.comp, a.descrip, round(a.shape/10000,4),round(a.shape,2) FROM Proysig a WHERE NUM_FOLIO = :fol and VERSION = :versions ORDER BY a.id");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        return q.getResultList();
    }

    public Object proySigTotObra(String Folio, String ClveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT sum(round(a.shape,2)) FROM Proysig a WHERE NUM_FOLIO = :fol and VERSION = :versions and COMP like '%OBRA%'");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        return q.getSingleResult();
    }

    public Object proySigTotPredio(String Folio, String ClveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT sum(round(a.shape,2)) FROM Proysig a WHERE NUM_FOLIO = :fol and VERSION = :versions and COMP like '%PREDIO%'");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        return q.getSingleResult();
    }

    public List<Object[]> proySigSum(String Folio, String ClveProy, short version) {
        Query q;
        q = emfMIAE.createQuery("SELECT a.proysigPK.cveArea, a.comp, sum(a.shapeArea) FROM Proysig a  WHERE a.proysigPK.numFolio=:fol and a.proysigPK.version=:versions GROUP BY a.comp, a.proysigPK.cveArea  ");
        q.setParameter("fol", Folio);
        //q.setParameter("clveProy", ClveProy);
        q.setParameter("versions", version);
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> edoMasAfectado(String Folio, String ClveProy, Short version, Integer id) {
        Query q = emfMIAE.createQuery("SELECT a.cEdomun , a.clvMunici , a.nomEstado, a.nomMunici "
                + "FROM MpiosCruzadavshambre a "
                + " WHERE a.mpiosCruzadavshambrePK.numFolio='" + Folio
                //+ "' and a.mpiosCruzadavshambrePK.cveProy='" + ClveProy
                + "' and a.mpiosCruzadavshambrePK.version =" + version
                + " and a.mpiosCruzadavshambrePK.idr =" + id);

        List l = q.getResultList();
        return l;
    }

    public List<Object[]> edoTodosAfectado(String Folio, String ClveProy, short version) {
        System.out.println("folioNum  " + Folio + "   proyClve " + ClveProy + "   vers  " + version);

        //Primero se verifica si existen obras
        Query q = emfMIAE.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'OBRA' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        String vObra = q.getSingleResult().toString();

        //Se verifica si existen predios.
        q = emfMIAE.createQuery("SELECT count(a.area) FROM MpiosCruzadavshambre a WHERE a.comp = 'PREDIO' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        String vPredio = q.getSingleResult().toString();

        String vFiltroPredioObra = "";

        if (Integer.valueOf(vObra) > 0) {
            vFiltroPredioObra = "OBRA";
        } else if (Integer.valueOf(vPredio) > 0) {
            vFiltroPredioObra = "PREDIO";
        };

        System.out.println("---- Obras " + vObra + "---  Predios  " + vPredio);
        
               q = emfMIAE.createQuery("SELECT a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici, sum(a.area) FROM MpiosCruzadavshambre a "
                       + "WHERE a.comp = '"+ vFiltroPredioObra + "' and a.mpiosCruzadavshambrePK.numFolio=:folioNum and a.mpiosCruzadavshambrePK.version =:vers"
                       + " GROUP BY a.cEdomun, a.clvMunici, a.nomEstado, a.nomMunici")
                .setParameter("folioNum", Folio)
                //.setParameter("proyClve", ClveProy)
                .setParameter("vers", version);

        //Regresa la lista de resultados
        return q.getResultList();
    }

    public List<Object[]> mpiosSumObra(String Folio, String ClveProy, short version) {
        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = emfMIAE.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);
        //q.setParameter("cve", ClveProy);
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = emfMIAE.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='OBRA')");
            q.setParameter("folio", Folio);
            //q.setParameter("cve", ClveProy);
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            
            q2 = emfMIAE.createNativeQuery("select m.idr from mpios_cruzadavshambre m where m.area = ("
                    + "select distinct max(m.area) from mpios_cruzadavshambre where num_folio=:folio and version = :version) and num_folio = :folio and version = :version and nom_estado=:estado AND comp='OBRA' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("version", version);
            q2.setParameter("estado", string);
            
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }

    public List<Object[]> mpiosSumPred(String Folio, String ClveProy, short version) {
        Query q, q2;
        Object o1, o2;
        List<Object[]> lista = new ArrayList<Object[]>();
        q = emfMIAE.createQuery("SELECT DISTINCT c.nomEstado FROM MpiosCruzadavshambre c WHERE c.mpiosCruzadavshambrePK.numFolio=:folio AND c.mpiosCruzadavshambrePK.version=:version");
        q.setParameter("folio", Folio);
        //q.setParameter("cve", ClveProy);
        q.setParameter("version", version);
        List<String> edos = q.getResultList();
        for (String string : edos) {
            q = emfMIAE.createNativeQuery("SELECT SUM(mayor) FROM (SELECT SUM(a.area) as mayor from mpios_cruzadavshambre a where a.num_folio = :folio and a.version = :version AND a.nom_estado=:edomun AND a.comp='PREDIO')");
            q.setParameter("folio", Folio);
          //  q.setParameter("cve", ClveProy);
            q.setParameter("version", version);
            q.setParameter("edomun", string);
            q2 = emfMIAE.createNativeQuery("select m.idr from mpios_cruzadavshambre m where m.area = ("
                    + "select distinct max(m.area) from mpios_cruzadavshambre where num_folio=:folio) and num_folio = :folio and version = :version and nom_estado=:estado AND comp='PREDIO' order by m.area desc");
            q2.setParameter("folio", Folio);
            q2.setParameter("version", version);
            q2.setParameter("estado", string);
            o1 = q.getSingleResult();
            if (o1 != null) {
                o2 = q2.getResultList().get(0);
                lista.add(new Object[]{o2, o1});
            }
        }
        Collections.sort(lista, new MpiosComparator());
        return lista;
    }

    public List<OeGralTerrit> getOeGralTerrit(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM OeGralTerrit e WHERE e.oeGralTerritPK.numFolio = :folio and e.oeGralTerritPK.cveArea = :cveArea and e.oeGralTerritPK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("cveArea", cveArea);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<OeRegionales1> getOeReg1(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM OeRegionales1 e WHERE e.oeRegionales1PK.numFolio = :folio and e.oeRegionales1PK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getOeReg1_2(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios FROM OeRegionales1 e WHERE e.oeRegionales1PK.numFolio = :folio and e.oeRegionales1PK.version = :version group by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios order by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }
    
    public List<OeRegionales2> getOeReg2(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM OeRegionales2 e WHERE e.oeRegionales2PK.numFolio = :folio and e.oeRegionales2PK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getOeReg2_2(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios FROM OeRegionales2 e WHERE e.oeRegionales2PK.numFolio = :folio and e.oeRegionales2PK.version = :version group by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios order by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }
    
    public List<OeRegionales3> getOeReg3(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM OeRegionales3 e WHERE e.oeRegionales3PK.numFolio = :folio and e.oeRegionales3PK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getOeReg3_2(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios FROM OeRegionales3 e WHERE e.oeRegionales3PK.numFolio = :folio and e.oeRegionales3PK.version = :version group by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios order by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }
    
    public List<OeGralTerrit> getOeGral(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM OeGralTerrit e WHERE e.oeGralTerritPK.numFolio = :folio and e.oeGralTerritPK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }    

    public List<Object[]> getOeGral_2(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e.nom, e.uab, e.politica FROM OeGralTerrit e WHERE e.oeGralTerritPK.numFolio = :folio and e.oeGralTerritPK.version = :version group by e.nom, e.uab, e.politica order by e.nom, e.uab, e.politica");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }
    
    public List<OeMarinos> getOeMarinos(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM OeMarinos e WHERE e.oeMarinosPK.numFolio = :folio and e.oeMarinosPK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getOeMarinos_2(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios FROM OeMarinos e WHERE e.oeMarinosPK.numFolio = :folio and e.oeMarinosPK.version = :version group by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios order by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }
    public List<OePoligEnvol> getOePoligEnvol(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM OePoligEnvol e WHERE e.oePoligEnvolPK.numFolio = :folio and e.oePoligEnvolPK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getOePoligEnvol_2(String folio, String clveProy, String cveArea, short version) {
        Query q = emfMIAE.createQuery("SELECT e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios FROM OePoligEnvol e WHERE e.oePoligEnvolPK.numFolio = :folio and e.oePoligEnvolPK.version = :version group by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios order by e.tipoOe, e.nomOe, e.uga, e.politica, e.usoPred, e.criterios");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }
    
    public List<AnpEstatal> getAnpEstatal(String folio, String clveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM AnpEstatal e WHERE e.anpEstatalPK.numFolio = :folio and e.anpEstatalPK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<AnpFederal> getAnpFederal(String folio, String clveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM AnpFederal e WHERE e.anpFederalPK.numFolio = :folio and e.anpFederalPK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<AnpMuicipal> getAnpMuicipal(String folio, String clveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT e FROM AnpMuicipal e WHERE e.anpMuicipalPK.numFolio = :folio and e.anpMuicipalPK.version = :version");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getAnpEstatal2(String folio, String clveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT e.nombre, e.categoria, e.fDec, e.fechaHora FROM AnpEstatal e WHERE e.anpEstatalPK.numFolio = :folio and e.anpEstatalPK.version = :version group by e.nombre, e.categoria, e.fDec, e.fechaHora");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getAnpFederal2(String folio, String clveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT e.nombre, e.catManejo, e.ultDecret, e.fechaHora FROM AnpFederal e WHERE e.anpFederalPK.numFolio = :folio and e.anpFederalPK.version = :version group by e.nombre, e.catManejo, e.ultDecret, e.fechaHora");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Object[]> getAnpMuicipal2(String folio, String clveProy, short version) {
        Query q = emfMIAE.createQuery("SELECT e.nombre, e.categoria, e.fDec, e.fechaHora FROM AnpMuicipal e WHERE e.anpMuicipalPK.numFolio = :folio and e.anpMuicipalPK.version = :version group by e.nombre, e.categoria, e.fDec, e.fechaHora");
        q.setParameter("folio", folio);
        //q.setParameter("cveProy", clveProy);
        q.setParameter("version", version);
        return q.getResultList();
    }
    
    public List<Object[]> getClimas2(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT c.climaTipo, c.grupomapa2, c.desTem, c.descPrec FROM Climas c WHERE num_Folio = :fol and version = :version group by c.climaTipo, c.grupomapa2, c.desTem, c.descPrec ");
        q.setParameter("fol", folio);
        q.setParameter("version", serial);
//        q.setParameter("clveProy", cveProy);
        return q.getResultList();
    }

    public List<Climas> getClimas(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT e FROM Climas e  WHERE e.climasPK.numFolio = :folio and e.climasPK.version = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }

    
    public List<Microcuenca> getMicrocuenca(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT e FROM Microcuenca e  WHERE e.microcuencaPK.numFolio = :folio and e.microcuencaPK.version = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }
    
    public List<HidrologiaProyecto> getHidrologia(String folio, Short serial) {
        Query q = emfMIAE.createQuery("SELECT h FROM HidrologiaProyecto h  WHERE h.id.folioProyecto = :folio and h.id.serialProyecto = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", new BigDecimal(serial));
        return q.getResultList();
    }
    
    public List<Object[]> getMicrocuenca2(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT e.cueHid, e.subcHid, e.nomMic FROM Microcuenca e  WHERE e.microcuencaPK.numFolio = :folio and e.microcuencaPK.version = :serial group by e.cueHid, e.subcHid, e.nomMic");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }

    public List<Acuiferos> getAcuiferos(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT e FROM Acuiferos e  WHERE e.acuiferosPK.numFolio = :folio and e.acuiferosPK.version = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }

    public List<Object[]> getAcuiferos2(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT e.clvAcui, e.nomAcui, e.descDispo, e.fechaDof, e.sobreexp FROM Acuiferos e  WHERE e.acuiferosPK.numFolio = :folio and e.acuiferosPK.version = :serial group by e.clvAcui, e.nomAcui, e.descDispo, e.fechaDof, e.sobreexp");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }

    public List<Object[]> getLocalidIndigenas(String folio, Short serial, String cveProy) {
        Query q = emfMIAE.createQuery("SELECT e.nomLoc, e.nomEnt, e.nomMun, e.pobmas, e.pobfem, e.totviviend, e.pobtot2, e.nomtipo, e.tipoloc, e.ambito, e.pobIndi FROM LocalidIndigenas e  WHERE e.localidIndigenasPK.numFolio = :folio and e.localidIndigenasPK.version = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }

    /**
     * 
     * @param o 
     */
    public void agrega(Object o) {
    	emfMIAE.clear();
        EntityTransaction tx = emfMIAE.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
            	emfMIAE.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }    
    
    public static void main(String[] a) {
        EntityManager mia = PersistenceManager.getEmfMIAE().createEntityManager();
        
        Query q = mia.createQuery("SELECT e FROM Acuiferos e  WHERE e.acuiferosPK.numFolio = :folio and e.acuiferosPK.version = :serial");
        q.setParameter("folio", "2123");
        q.setParameter("serial", (short) 1);
        //q.setParameter("cveProy", "0");

        List<Object> e = q.getResultList();
        for (Object c : e) {
            System.out.println(" " + c);
        }

    }

}
