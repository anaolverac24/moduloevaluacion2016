package mx.gob.semarnat.dao;

import java.sql.Types;

import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.type.StandardBasicTypes;

public class CustomOracleDialect extends Oracle10gDialect {

	public CustomOracleDialect() {
		registerHibernateType(Types.NVARCHAR, StandardBasicTypes.STRING.getName());
		registerColumnType( Types.VARCHAR, "nvarchar($1)" );
	}
	
}
