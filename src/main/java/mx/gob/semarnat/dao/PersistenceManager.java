package mx.gob.semarnat.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Ana Olvera
 */
public class PersistenceManager {

    public static final boolean DEBUG = true;

   // private static EntityManagerFactory emfBitacora = Persistence.createEntityManagerFactory("MOD_EVAL");;    
    private static EntityManagerFactory emfMiaE = Persistence.createEntityManagerFactory("MOD_EVAL");

    
    private PersistenceManager() {
    }

    public static EntityManagerFactory getEmfBitacora() {

        return emfMiaE;
    }
    
    public static EntityManagerFactory getEmfMIAE() {

        return emfMiaE;
    }


    
    public static void closeEntityManagerFactory() {

//        if (emfBitacora != null) {
//            emfBitacora.close();
//            emfBitacora = null;
//            if (DEBUG) {
//                System.out.println("n*** emfBitacora Persistence finished at " + new java.util.Date());
//            }
//        }
//        
//        if (emfMiaE != null) {
//            emfMiaE.close();
//            emfMiaE = null;
//            if (DEBUG) {
//                System.out.println("n*** emfMiaE Persistence finished at " + new java.util.Date());
//            }
//        }
    }

}
