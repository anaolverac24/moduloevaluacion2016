package mx.gob.semarnat.dao;

import java.util.List;

import javax.persistence.EntityManager;

import mx.gob.semarnat.model.catalogos.Sector;
import mx.gob.semarnat.model.catalogos.SectorProyecto;

public class SectorProyectoDAO {
	
	private final EntityManager em = PersistenceManager.getEmfBitacora().createEntityManager();
    
    public List<SectorProyecto> getAllSectorP() {
		return em.createNativeQuery("select * from CATALOGOS.SECTOR_PROYECTO", SectorProyecto.class).getResultList();
    }
}
