/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.model.StreamedContent;

import mx.gob.semarnat.model.dgira_miae.ArchivosProyecto;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;

@ManagedBean
public class ArchivoBean {

    /**
     *
     */
    @SuppressWarnings("unused")
    private static final long serialVersionUID = 3213037381217208435L;
    protected HttpServletResponse response;

    /**
     * Creates a new instance of ArchivosBean
     */
    public ArchivoBean() {
    }

    @SuppressWarnings("unused")
    private String folioProyecto;
    private StreamedContent file;

    Short serialProyecto = (short) 0;
    Short normaId = (short) 0;

    short capituloId = (short) 0;
    short subcapituloId = (short) 0;
    short seccionId = (short) 0;
    short apartadoId = (short) 0;
    short sustanciaId = (short) 0;
    short estudioId = (short) 0;

    List<Map<String, String>> adjuntos = new ArrayList<Map<String, String>>();
    EntityManager em = PersistenceManager.getEmfMIAE().createEntityManager();

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        this.folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
    }

    /**
     * Muestra un archivo en base a us id secuencial
     *
     * @param SeqId del archivo en la BD
     */
    public void ver_archivo(short SeqId) {
        System.out.println("ver_archivo... " + SeqId);
        response = null;
        //EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        String filePath = "";
        String ext = "";
        Query q = em.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.seqId = :seqId");
        q.setParameter("seqId", SeqId);

        try {
            ArchivosProyecto e = (ArchivosProyecto) q.getSingleResult();
            filePath = e.getUrl();
            ext = e.getExtension().toLowerCase().trim();
            File downloadFile = new File(filePath);

            String mimeType = "";
            String responseHeaderType = "";

            //Se valida si la extencion del archivo cumple para mostrar el contenido en el browser
            if (ext.equals("pdf") || ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
                if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
                    mimeType = "image/" + ext;
                }
                if (ext.equals("pdf")) {
                    mimeType = "application/pdf";
                }
                responseHeaderType = "inline";
            } else {
                mimeType = "application/octet";
                responseHeaderType = "attachment";
            }
            processRequest(mimeType, responseHeaderType, downloadFile.getPath(), e.getFileName(), e.getExtension());
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IllegalStateException illegalException) {
            System.out.println("Excepcion de tipo IllegalStateException controloda.");
        } catch (Exception err) {
            System.out.println("Excepcion al ver el archivo: " + err.toString());
        }finally{
            em.clear();
        }
    }

    /**
     * Muestra un archivo en base a us id secuencial
     *
     * @param SeqId del archivo en la BD
     */
    public void ver_archivo_PDF(String bitacora, String clavetramite, short idtramite, short tipodoc, String entidadfederativa, String documentoid) {
        System.out.println("ver_archivo PDF... ");
        System.out.println("bitacora...         " + bitacora);
        System.out.println("clavetramite...     " + clavetramite);
        System.out.println("idtramite...        " + idtramite);
        System.out.println("tipodoc...          " + tipodoc);
        System.out.println("entidadfederativa.. " + entidadfederativa);
        System.out.println("documentoid..       " + documentoid);

        response = null;
//        EntityManager em = PoolEntityManagers.getEmfMIAE().createEntityManager();
        String filePath = "";
        String nombreOfic = "";
        String ext = "";

        DgiraMiaeDaoGeneral daoDg = new DgiraMiaeDaoGeneral();
        List<DocumentoRespuestaDgira> lista = daoDg.docsRespDgiraBusq2(bitacora, clavetramite, idtramite, tipodoc, entidadfederativa, documentoid);
        for (DocumentoRespuestaDgira x : lista) {
            filePath = x.getDocumentoUbicacion();
            nombreOfic = x.getDocumentoNombre().substring(0, x.getDocumentoNombre().length() - 4);
            ext = x.getDocumentoNombre().substring(x.getDocumentoNombre().length() - 3, x.getDocumentoNombre().length());
        }

        System.out.println("filePath..    " + filePath);
        System.out.println("nombreOfic..  " + nombreOfic);
        System.out.println("extension ..  " + ext);
//      ext = "pdf";

        try {
            File downloadFile = new File(filePath);
            String mimeType = "";
            String responseHeaderType = "";

            //Se valida si la extencion del archivo cumple para mostrar el contenido en el browser
//            if (ext.equals("pdf") || ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
//            	if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
//            		mimeType = "image/" + ext;
//		        }
//		        if (ext.equals("pdf")) {
            mimeType = "application/pdf";
//		        }
            responseHeaderType = "inline";
//            } else { 
//            	mimeType = "application/octet";
//            	responseHeaderType = "attachment";
//            }          
            processRequest(mimeType, responseHeaderType, downloadFile.getPath(), nombreOfic, ext);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IllegalStateException illegalException) {
            System.out.println("Excepcion de tipo IllegalStateException controloda.");
        } catch (Exception err) {
            System.out.println("Excepcion al ver el archivo: " + err.toString());
        }
    }

    protected void processRequest(String myneType, String responseHeaderType, String pathFile, String fileName, String extension)
            throws ServletException, IOException {
        response = ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse());
        response.setContentType("text/html;charset=UTF-8");

        ServletOutputStream outs = response.getOutputStream();
        //---------------------------------------------------------------
        // Set the output data's mime type
        //---------------------------------------------------------------
        response.setContentType(myneType);  // MIME type for pdf doc
        //---------------------------------------------------------------
        // create an input stream from fileURL
        //---------------------------------------------------------------	
        File file = new File(pathFile);

        //------------------------------------------------------------
        // Content-disposition header - don't open in browser and
        // set the "Save As..." filename.
        // *There is reportedly a bug in IE4.0 which  ignores this...
        //------------------------------------------------------------	    
        response.setHeader("Content-disposition", responseHeaderType + "; filename=\"" + fileName + "." + extension + "\"");

        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {

            InputStream isr = new FileInputStream(file);
            bis = new BufferedInputStream(isr);
            bos = new BufferedOutputStream(outs);
            byte[] buff = new byte[2048];
            int bytesRead;
            // Simple read/write loop.
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
        } catch (Exception e) {
            System.out.println("Exception ----- Message ---" + e);
        } finally {
            if (bis != null) {
                bis.close();
            }
            if (bos != null) {
                bos.close();
            }
        }
    }

    /**
     * @return the file
     */
    public StreamedContent getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(StreamedContent file) {
        this.file = file;
    }
}
