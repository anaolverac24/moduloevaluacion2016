/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Rengerden
 */
@SuppressWarnings("serial")
public class MenuDao implements Serializable {
   
    private final EntityManager emfBitacora = PersistenceManager.getEmfBitacora().createEntityManager();
    private final EntityManager emfDgira = PersistenceManager.getEmfMIAE().createEntityManager();
 
    //clave del proyecto
    public String cveTramite (String bitacora){
         String cve="";
         try {
            
        
            Query q = emfBitacora.createQuery("SELECT v.cve FROM Proyecto_Bitacora v where v.numeroBita=:bitacora ");
            //Query q = emfBitacora.createNativeQuery("select CVE from BITACORA_TEMATICA.PROYECTO  where NUMERO_BITA='"+bitacora+"'");
              //Query q = emfBitacora.createNativeQuery("select v.CVE from BITACORA_TEMATICA.PROYECTO v  where NUMERO_BITA='09/MPW0010/12/14'");
                //Query q = emfBitacora.createQuery("SELECT v.cve FROM Proyecto_Bitacora v WHERE v.numeroBita");
            q.setParameter("bitacora", bitacora);
             cve=(String)q.getSingleResult();
             } catch (Exception e) {
        }
        return cve;
         
        
    }
    
    //nombre del proyecto
    public String nombProy (String bitacora){
         String cve="";
         try {
            
        
            Query q = emfBitacora.createQuery("SELECT v.nombre FROM Proyecto_Bitacora v where v.numeroBita=:bitacora ");
            //Query q = emfBitacora.createNativeQuery("select CVE from BITACORA_TEMATICA.PROYECTO  where NUMERO_BITA='"+bitacora+"'");
              //Query q = emfBitacora.createNativeQuery("select v.CVE from BITACORA_TEMATICA.PROYECTO v  where NUMERO_BITA='09/MPW0010/12/14'");
                //Query q = emfBitacora.createQuery("SELECT v.cve FROM Proyecto_Bitacora v WHERE v.numeroBita");
            q.setParameter("bitacora", bitacora);
             cve=(String)q.getSingleResult();
             } catch (Exception e) {
        }
        return cve;
         
        
    }
    
    //bandera para ver el tipo de ubicacion guardada
  public char DirTramReq (String folio){
      char TramReq=' ';
      try {
          Query q = emfDgira.createNativeQuery("select  PROY_DOM_ESTABLECIDO from PROYECTO where  FOLIO_PROYECTO =:folio");
          q.setParameter("folio", folio);
          TramReq= (char)q.getSingleResult();
      } catch (Exception e) {
          System.out.println("Error DirTramReq: " + e);
      }
      return TramReq;
  }
  //direccion tramite establecido
  public String dirTramEst (String folio){
      String dirTramEst="";
      try {
          Query q = emfDgira.createNativeQuery("select c.DESCRIPCION||':  '|| P.PROY_NOMBRE_VIALIDAD ||'   Num. Ext: '||P.PROY_NUMERO_EXTERIOR||' Num.Int:   '||P.PROY_NUMERO_INTERIOR||' '||a.NOMBRE||': '||P.PROY_NOMBRE_ASENTAMIENTO||'    CP: '||P.PROY_CODIGO_POSTAL from CATALOGOS.CAT_VIALIDAD c ,CATALOGOS.CAT_TIPO_ASEN a ,DGIRA_MIAE2.PROYECTO p  where c.CVE_TIPO_VIAL = (select CVE_TIPO_VIAL from DGIRA_MIAE2.PROYECTO where  FOLIO_PROYECTO ='"+folio+"' ) and A.CVE_TIPO_ASEN =(select CVE_TIPO_VIAL from DGIRA_MIAE2.PROYECTO where  FOLIO_PROYECTO ='"+folio+"' ) and P.FOLIO_PROYECTO = '"+folio+"'");
//          q.setParameter("folio", folio);
          dirTramEst =(String) q.getSingleResult();
      } catch (Exception e) {
           System.out.println("Error dirTramEst: " + e);
      }
      return dirTramEst;
  }
    //direccion tramite establecido 
  
  public String dirUbicDesc (String folio){
       String ubic="";
       try {
          Query q = emfDgira.createQuery("SELECT v.proyUbicacionDescrita from Proyecto_DGIRA v where v.proyectoPK.folioProyecto=:folio");
          q.setParameter("folio", folio);
          ubic= (String)q.getSingleResult();
      } catch (Exception e) {
           System.out.println("Error dirUbicDesc: " + e);
      }
       return ubic;
        
  }
  //Tipo de Tramite
  public Integer cveTipTram (String bitacora){
      Integer TramReq=0;
      try {
            Query q = emfBitacora.createQuery("SELECT v.bgidTramiteLk FROM Vexdatostramite v WHERE v.vcbitacora=:bitacora");
             q.setParameter("bitacora", bitacora);
             TramReq= (Integer)q.getSingleResult();
      } catch (Exception e) {
          System.out.println("Error cveTipTram: " + e );
      }
  
  return TramReq;
  }
  
  //Tipo de Tramite por tabla bitacora para tramites fisicos
  public Integer cveTipTramBita (String bitacora){
      Integer TramReq=0;
      try {
            Query q = emfBitacora.createQuery("SELECT v.bitaIdTramite FROM Bitacora v WHERE v.bitaNumero=:bitacora");
             q.setParameter("bitacora", bitacora);
             TramReq= (Integer)q.getSingleResult();
      } catch (Exception e) {
          System.out.println("Error cveTipTram: " + e );
      }
  
  return TramReq;
  }
    
    public String fechaIng (String bitacora){
       String fechaIng= "";
        try {
            Query q = emfBitacora.createNativeQuery("select TO_CHAR(BITA_FOFI_RECEPCION,'dd/MM/yyyy') FROM BITACORA.BITACORA where BITA_NUMERO =:bitacora");
            q.setParameter("bitacora", bitacora);
            fechaIng = (String)q.getSingleResult();
            System.out.println("fechaConsulta: "+ fechaIng);
        } catch (Exception e) {
            System.out.println("Error fechaIng " + e);
        }
     return fechaIng;
    
    }

    public String situaTram (String bitacora){
    String situacion="";
        try {
            Query q = emfBitacora.createNativeQuery("select DESCRIPCION from CATALOGO_FLUJOS.TBSITUACION WHERE IDSITUACION = (SELECT BITA_SITUACION from BITACORA.BITACORA where BITA_NUMERO = '"+bitacora+"')");
//            q.setParameter("bitacora", bitacora);
            situacion = (String) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error situacion " + e);
        }
    return situacion; 
    }

    public String subsecTram (String bitacora){
        String subsector="";
        try {
            Query q = emfBitacora.createNativeQuery("select SUBSECTOR from CATALOGOS.SUBSECTOR_PROYECTO where NSUB=(select Subsector from BITACORA_TEMATICA.PROYECTO where NUMERO_BITA = '"+bitacora+"')");
//            q.setParameter("bitacora", bitacora);
            subsector = (String) q.getSingleResult();
        } catch (Exception e) {            
            System.out.println("Error subsector" + e);
        }
        return subsector;
    } 
    
    public String promTram (String folio){
        String promov="";
        try {
            Query q = emfBitacora.createNativeQuery("select VCNOMBRE||' '||VCAPELLIDOPATERNO||' '||VCAPELLIDOMATERNO from SINATEC.VEXDATOSUSUARIO where BGTRAMITEID ="+folio);
            promov = (String) q.getSingleResult();
            promov = promov.trim();
//            q.setParameter("folio", folio);
        } catch (Exception e) {
            System.out.println("Error promTram " + e);
            promov = "";
        }
    return promov;
    }
    
    //nombre promovente persona moral
    public String nomMora (String folio){
    String moral="";
        try {
            Query q = emfBitacora.createNativeQuery("select  VCRAZONSOCIAL from SINATEC.VEXDATOSUSUARIO where BGTRAMITEID ="+folio);
            moral = q.getSingleResult().toString();
        } catch (Exception e) {
             System.out.println("Error nomMora " + e);
        }
     return moral;
    
    }

    public BigDecimal diasProceso (String bitacora){
    BigDecimal dias= new BigDecimal("0.0");
    
        try {
            Query q = emfBitacora.createNativeQuery("SELECT BITA_DIAS_PROCESO FROM BITACORA.BITACORA WHERE BITA_NUMERO=:bitacora");
            //Query q = emfBitacora.createQuery("SELECT v.bitaDiasProceso FROM Bitacora v WHERE v.bitaNumero="+bitacora);
            q.setParameter("bitacora", bitacora);
            dias = (BigDecimal)q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error Dias Proceso " + e);
        }
      return dias;
    }
    
     public Integer diasTramite (String bitacora){
    Integer diasT=0;

    
        try {
            Query q = emfBitacora.createQuery("SELECT v.bitaDiasTramite FROM Bitacora v WHERE v.bitaNumero=:bitacora");
            q.setParameter("bitacora", bitacora);
            diasT = (Integer)q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error Dias Tramite " + e);
        }
      return diasT;
    }
     
}


