package mx.gob.semarnat.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import mx.gob.semarnat.model.bitacora.CadenaFirma;
import mx.gob.semarnat.model.bitacora.CadenaFirmaLog;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.utils.Utils;
import net.firel.Auxiliar;
import net.firel.BcAgregaCadenaRespuesta;
import net.firel.DetallesFirmado;
import net.firel.DetallesOperacion;
import net.firel.Firmas;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfEncodings;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.util.Locale;
import mx.go.semarnat.services.ServicioDocument;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;

/**
 * *
 * Clase que administra las lecturas al servicio web de firmas de oficios asi
 * como la logica de los mismos procesos de firma y turnado
 *
 * @author Paul Montoya
 *
 */
@SuppressWarnings("serial")
public class CadenaFirmaDao implements Serializable {

    private final EntityManager em = PersistenceManager.getEmfMIAE().createEntityManager();

    public void agrega(Object o) {
        em.clear();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                em.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        } finally {
            em.clear();
        }
    }

    public void modifica(Object clase) {
        try {
            em.getTransaction().begin();
            em.merge(clase);
            em.getTransaction().commit();
        } catch (Exception e) {
        } finally {
            em.clear();
        }
    }

    // -----------------------   CADENA FIRMA Y CADENA FIRMA LOG  ------------------------------------------
    /**
     * *
     * obtiene el objeto CadenaFirma que representa el encabezado de firma de un
     * oficio
     *
     * @param clave la clave del registro de CadenaFirma
     * @return
     */
    public CadenaFirma getCadenaFirma(BigDecimal clave) {
        CadenaFirma resultado = new CadenaFirma();
        try {
            resultado = (CadenaFirma) em.find(CadenaFirma.class, clave);
        } catch (Exception e) {
        } finally {
            em.clear();
        }
        return resultado;
    }

    /**
     * *
     * Obtiene el objeto CadenaFirma que representa el encabezado de firma de un
     * oficio
     *
     * @param claveProyecto clave de proyecto
     * @param bitacora bitacora
     * @param claveTramite2 clavetramite
     * @param idTramite2 id tramite
     * @param tipoDocId tipo doc id
     * @param documentoId documento id
     * @return
     */
    public CadenaFirma getCadenaFirma(String claveProyecto, String bitacora, String claveTramite2, int idTramite2, short tipoDoc, String tipoDocId) {

        Object o = null;
        Query q1 = em.createQuery("SELECT c FROM CadenaFirma c WHERE c.claveProyecto = :claveProyecto "
                + " and c.bitacora = :bitacora"
                + " and c.claveTramite2 = :claveTramite2"
                + " and c.idTramite2 = :idTramite2"
                + " and c.tipoDocId = :tipoDocId"
                + " and c.documentoId = :documentoId"
        );
        q1.setParameter("claveProyecto", claveProyecto);
        q1.setParameter("bitacora", bitacora);
        q1.setParameter("claveTramite2", claveTramite2);
        q1.setParameter("idTramite2", idTramite2);
        q1.setParameter("tipoDocId", tipoDoc);
        q1.setParameter("documentoId", tipoDocId);

        try {
            o = q1.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("ERROR: CadenaFirma NO Encontrada.");
        } catch (Exception e) {
            System.out.println("ERROR: CadenaFirma NO Encontrada.");
            System.out.println("" + e.getLocalizedMessage());
        } finally {
            em.clear();
        }

        return (CadenaFirma) o;
    }

    @SuppressWarnings("unchecked")
    /**
     * *
     * Obtiene la lista de CadenaFirmaLog de la CadenaFirma indicada
     *
     * @param cadenaFirma
     * @return
     */
    public List<CadenaFirmaLog> getCadenaFirmaLogList(CadenaFirma cadenaFirma) {
        List<CadenaFirmaLog> l = new ArrayList<CadenaFirmaLog>(0);
        try {
            Query q1 = em.createQuery("SELECT c FROM CadenaFirmaLog c WHERE c.cadenaFirma = :cadenaFirma");
            q1.setParameter("cadenaFirma", cadenaFirma);
            l = q1.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.clear();
        }
        return l;
    }
    
    
    public String getConsecutivo(String bitacora ) {
        String s = "";
        try {
            
            String sql = "SELECT CONCAT(B.CVE " +
                        "  ||'-', " +
                        "  (SELECT COUNT(1) " +
                        "  FROM DOCUMENTO_RESPUESTA_DGIRA " +
                        "  WHERE BITACORA_PROYECTO = :bitaString " +
                        "  )) AS NumeroConsecutivo " +
                        "FROM BITACORA_TEMATICA.PROYECTO B " +
                        "WHERE B.NUMERO_BITA = :bitaString";
            System.out.println("sql>>>>>>>>>>>consecutivo:"+sql);
            Query q1 = em.createNativeQuery(sql);
            q1.setParameter("bitaString", bitacora);
            s = (String)q1.getSingleResult();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.clear();
        }
        return s;
    }

    @SuppressWarnings("unchecked")
    /**
     * *
     * Obtiene un HashMap de <rol(string), CadenaFirmaLog> de la CadenaFirma
     * indicada. Donde la clave es el nombre del rol
     *
     * @param cadenaFirma
     * @return
     */
    private HashMap<String, CadenaFirmaLog> getCadenaFirmaLogHashList(CadenaFirma cadenaFirma) {

        System.out.println("getCadenaFirmaLogHashList..");
        List<CadenaFirmaLog> l = new ArrayList<CadenaFirmaLog>(0);
        HashMap<String, CadenaFirmaLog> hash = new HashMap<String, CadenaFirmaLog>();
        try {
            Query q1 = em.createQuery("SELECT c FROM CadenaFirmaLog c WHERE c.cadenaFirma = :cadenaFirma");
            q1.setParameter("cadenaFirma", cadenaFirma);
            l = q1.getResultList();

            if (l != null && l.size() > 0) {
                for (CadenaFirmaLog log : l) {
                    hash.put(log.getRol(), log);
                }
            }

            for (CadenaFirmaLog log2 : l) {
                System.out.println(log2.getRol() + " - " + log2.getRfc() + " - " + log2.getFirma());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.clear();
        }
        return hash;
    }

    /**
     * *
     * Obtiene la CadenaFirmaLog de la CadenaFirma y rol especificados
     *
     * @param cadenaFirma
     * @param rol
     * @return
     */
    public CadenaFirmaLog getCadenaFirmaLog(CadenaFirma cadenaFirma, String rol) {
        try {
            System.out.println("\ngetCadenaFirmaLog :" + cadenaFirma.getClave() + " " + rol);
            Query q1 = em.createQuery("SELECT c FROM CadenaFirmaLog c WHERE c.cadenaFirma = :cadenaFirma and c.rol = :rol");
            q1.setParameter("cadenaFirma", cadenaFirma);
            q1.setParameter("rol", rol);
            return (CadenaFirmaLog) q1.getSingleResult();
        } catch (NoResultException ex1) {

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.clear();
        }
        return null;
    }

    /**
     * *
     * Crea un registro, una CadenaFirmaLog
     *
     * @param CadenaFirmaLog object
     * @return
     */
    public boolean agregaCadenaFirmaLog(CadenaFirmaLog o) {
        em.clear();
        boolean resp = false;
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                em.persist(o);
                tx.commit();
                resp = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        } finally {
            em.clear();
        }
        return resp;
    }

    /**
     * *
     * Actualiza la CadenaFirmaLog especificada
     *
     * @param CadenaFirmaLog object
     * @return
     */
    public boolean modificaCadenaFirmaLog(CadenaFirmaLog o) {
        boolean resp = false;
        try {
            em.getTransaction().begin();
            em.merge(o);
            em.getTransaction().commit();
            resp = true;
        } catch (Exception e) {
            e.printStackTrace();
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } finally {
            em.clear();
        }
        return resp;
    }

    // -----------------------   METODOS CONSULTAN Y GUARDAN EN EL WEB SERVICE  -----------------------------------------
    /**
     * *
     * Invoca un metodo de alta en el ws, para dar inicio a las firmas de un
     * oficio y cuyo metodo devuelve un token que registramos en la bd para
     * posteriores firmas de otros usuarios
     *
     * @param referencia
     * @param aviso
     * @param cadenaBase64
     * @return
     * @throws Exception
     * @throws RemoteException
     */
    public BcAgregaCadenaRespuesta agregaCadena(String referencia, String cadenaBase64) throws RemoteException, Exception {

        BcAgregaCadenaRespuesta respuesta = null;
        // String entidad="mia",operador="operadormia",clave="12121212",digestion="1",tipoCodificacion="3";

        // ResourceBundle rbConf = ResourceBundle.getBundle(ARCH_CONF_GENERAL);
        String url = Utils.obtenerConstante(GenericConstants.CONST_WS_URL);
        String entidad = Utils.obtenerConstante(GenericConstants.CONST_WS_ENTIDAD);
        String operador = Utils.obtenerConstante(GenericConstants.CONST_WS_OPERADOR);
        String clave = Utils.obtenerConstante(GenericConstants.CONST_WS_CLAVE);
        String digestion = Utils.obtenerConstante(GenericConstants.CONST_WS_DIGESTION);
        String tipoCodificacion = Utils.obtenerConstante(GenericConstants.CONST_WS_TIPOCODIFICACION);
        String aviso = Utils.obtenerConstante(GenericConstants.CONST_WS_AVISO);

        // try {
        Auxiliar aux = new Auxiliar(url, entidad, operador, clave, digestion, tipoCodificacion);
        respuesta = aux.agregaCadena(referencia, aviso, cadenaBase64);

        /*}catch(Exception ex){
			ex.printStackTrace();	
		}*/
        return respuesta;
    }

    /**
     * *
     * Consulta la lista de firmas existentes en el servicio web
     *
     * @param firma
     * @return Devuelve un objeto DetallesFirmado que contiene informacion
     * ademas una lista con las firmas (Firmas[])
     * @throws NumberFormatException
     * @throws RemoteException
     * @throws Exception
     */
    public DetallesFirmado getFirmas(CadenaFirma firma) throws NumberFormatException, RemoteException, Exception {

        System.out.println("\nFirmas del WS:");
        System.out.println("operacion : " + firma.getOperacion());
        System.out.println("identificador : " + firma.getIdentificador());
        System.out.println("folioProyecto : " + firma.getFolioProyecto());

        DetallesFirmado detalles = null;
        // String entidad="mia",operador="operadormia",clave="12121212",digestion="1",tipoCodificacion="3";

        String url = Utils.obtenerConstante(GenericConstants.CONST_WS_URL);
        String entidad = Utils.obtenerConstante(GenericConstants.CONST_WS_ENTIDAD);
        String operador = Utils.obtenerConstante(GenericConstants.CONST_WS_OPERADOR);
        String clave = Utils.obtenerConstante(GenericConstants.CONST_WS_CLAVE);
        String digestion = Utils.obtenerConstante(GenericConstants.CONST_WS_DIGESTION);
        String tipoCodificacion = Utils.obtenerConstante(GenericConstants.CONST_WS_TIPOCODIFICACION);

        Auxiliar aux = new Auxiliar(url, entidad, operador, clave, digestion, tipoCodificacion);
        detalles = aux.getFirmas(Long.parseLong(firma.getOperacion()), firma.getIdentificador(), firma.getFolioProyecto());

        return detalles;
    }

    /**
     * *
     * Realiza el cierre lote firmas, es decir invoca a un servicio web que
     * realiza un cierre para finalizar el proceso de firmas de un oficio
     *
     * @param transferenciaOperacion
     * @param identificador
     * @param referencia
     * @return
     * @throws NumberFormatException
     * @throws RemoteException
     * @throws Exception
     */
    public DetallesOperacion cierraLoteFirmas(long transferenciaOperacion, String identificador, String referencia) throws NumberFormatException, RemoteException, Exception {

        System.out.println("\n\ncierraLoteFirmas");
        System.out.println("operacion : " + transferenciaOperacion);
        System.out.println("identificador : " + identificador);
        System.out.println("folioProyecto : " + referencia);

        DetallesOperacion detalles = null;

        String url = Utils.obtenerConstante(GenericConstants.CONST_WS_URL);
        String entidad = Utils.obtenerConstante(GenericConstants.CONST_WS_ENTIDAD);
        String operador = Utils.obtenerConstante(GenericConstants.CONST_WS_OPERADOR);
        String clave = Utils.obtenerConstante(GenericConstants.CONST_WS_CLAVE);
        String digestion = Utils.obtenerConstante(GenericConstants.CONST_WS_DIGESTION);
        String tipoCodificacion = Utils.obtenerConstante(GenericConstants.CONST_WS_TIPOCODIFICACION);
        String estadoTransferencia = Utils.obtenerConstante(GenericConstants.CONST_WS_ESTADO_TRANSFERENIA);
        String estadoDescripcion = Utils.obtenerConstante(GenericConstants.CONST_WS_ESTADO_DESCRIPCION);

        Auxiliar aux = new Auxiliar(url, entidad, operador, clave, digestion, tipoCodificacion);
        detalles = aux.cierraLoteFirmas(transferenciaOperacion, identificador, referencia, estadoTransferencia, estadoDescripcion);

        return detalles;
    }

    /**
     * *
     * Edita el pdf agregandole la lista de firmas electronicas al final..
     *
     * @param documento objeto con url fisica del servidor donde se va a guardar
     * el pdf modificado con las firmas y nombre sin la extencion "pdf"
     * @param cadenaFirma
     * @param ligaQR es la liga a donde apunta el codigo QR
     * @throws IOException
     * @throws DocumentException
     */
    public void editarPdf(DocumentoRespuestaDgira documento, CadenaFirma cadenaFirma, String ligaQR) throws IOException, DocumentException, Exception {

        System.out.println("editarPDF");

        String urlDisco = Utils.obtenerConstante(GenericConstants.CONST_RUTA_DOCTOS_RESPUESTA);
        String destino = urlDisco + cadenaFirma.getFolioProyecto() + "/" + documento.getDocumentoNombre();
        String aux = urlDisco + cadenaFirma.getFolioProyecto() + "/" + "pdfaux.pdf";
        String srcImage = urlDisco + cadenaFirma.getFolioProyecto() + "/" + "qrCode.png";

        System.out.println("origen : " + documento.getDocumentoUbicacion());
        System.out.println("destino : " + destino);
        System.out.println("url QR :" + ligaQR);

        // primero genero el codigo qr con el texto de urlOficio
        File fileqr = new File(srcImage);
        try {

            generateQR(fileqr, ligaQR, 150, 150);
            System.out.println("QRCode Generated: " + fileqr.getAbsolutePath());

            String qrString = decoder(fileqr);
            System.out.println("Text QRCode: " + qrString);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // ---------------------------------

        // Leo el contenido de mi PDF base
        PdfReader reader = new PdfReader(documento.getDocumentoUbicacion());

        // apartir del original.. creo un nuevo pdf auxiliar ..
        // este pdf auxiliar contiene una replica del pdf original mas la ultima hoja del original clonada al pdf auxiliar...
        int n = reader.getNumberOfPages();
        Document document = new Document();
        PdfCopy copy = new PdfCopy(document, new FileOutputStream(aux));
        document.open();
        int i = 0;
        for (i = 0; i < n;) {
            copy.addPage(copy.getImportedPage(reader, ++i));
        }
        copy.addPage(copy.getImportedPage(reader, i));
        document.close();

        // -------------------------------------------
        // Leo el contenido de mi PDF base ( ahora el base es el pdf auxiliar )
        reader = new PdfReader(aux);
        
        int totalPages = reader.getNumberOfPages()-1;
        
        PdfDictionary dict = reader.getPageN(reader.getNumberOfPages());
        PdfObject object = dict.getDirectObject(PdfName.CONTENTS);
        if (object instanceof PRStream) {
            PRStream stream = (PRStream)object;
            byte[] data = PdfReader.getStreamBytes(stream);
            
            String f = PdfEncodings.convertToString(data , PdfObject.TEXT_PDFDOCENCODING);
            stream.setData(PdfEncodings.convertToBytes(f.replace("Consecutivo:","Consecutivo: "+getConsecutivo(cadenaFirma.getBitacora())).replace("TTPS",""+totalPages), PdfObject.TEXT_PDFDOCENCODING));
        }
        
        
        // Creo el stamper especificando el contenido base y el archivo de salida
        PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(destino));
        // Obtengo el contenido del pdf. Si utilizo getUnderContent lo que agregue aparecera debajo del contenido de mi PDF original
        // si utilizo getOverContent los elementos agregados apareceran encima del contenido de mi PDF original        
        System.out.println("Numero de paginas : " + reader.getNumberOfPages());

        // PdfContentByte cb = stamp.getUnderContent(reader.getNumberOfPages());
        PdfContentByte cb;

        //Creo una fuente        
        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        String numeroOficio = getNumeroOficio(documento);
        for (int j = 1; j < reader.getNumberOfPages(); j++) {
            cb = stamp.getOverContent(j);

            cb.beginText();

            /// NUMERO DE OFICIO
            cb.setFontAndSize(bf, 11);
            cb.setTextMatrix(525, 755);
            cb.showText(numeroOficio);

            /// FECHA
            if (j == 1) {
                String fecha = getDate();
                cb.setTextMatrix(330, new Float(720.5));
                cb.showText("Ciudad de México, a " + fecha);
            }

            cb.endText();
        }
        
        ////Ultima hoja
//            cb = stamp.getUnderContent(reader.getNumberOfPages() - 1);
//           
//            cb.beginText();
//
//            /// NUMERO DE OFICIO
//            cb.setFontAndSize(bf, 11);
//            cb.setTextMatrix(50, 75);
//            System.out.println("cadenaFirma.getBitacora()"+cadenaFirma.getBitacora());
//            cb.showText("Consecutivo:"+ getConsecutivo(cadenaFirma.getBitacora()));
//            
//            cb.endText();

        cb = stamp.getOverContent(reader.getNumberOfPages());
        // --- inicio   ------
        // limpio la ultima hoja del pdf ( la parte del encabezado y pie se mantienen intactos solamente, lo demas se reemplaza por lineas blancas )
        Font f = new Font();
        f.setColor(BaseColor.WHITE);
        f.setSize(25f);

        // limpia el cuerpo de la hoja, abajo del encabezado....
        for (int ii = 795; ii > 100; ii -= 1) {
            writeToAbsolutePosition(cb, "________________________________________", 30, ii, 600, 0, f);
        }
        // limpia la parte ultima donde dice hoja x de x...
        for (int ii = 90; ii > 5; ii -= 1) {
            writeToAbsolutePosition(cb, "________________________________________", 30, ii, 600, 0, f);
        }
        // --- fin  ----

        // escribir el texto que acompaña de lado al codigo qr ( esto se hacer primero que otros textos ya que es un ColumnText y no se veria abajo )
        Font bold = new Font(FontFamily.HELVETICA, 8, Font.BOLD);
        Chunk chunk = new Chunk("http://goo.gl/QiNqR", bold);
        chunk.setAnchor("http://apps1.semarnat.gob.mx/consultatramite/estado.php?_origen=Busqueda_directa_1&_idBitacora=" + cadenaFirma.getBitacora());
        Phrase p = new Phrase();
        p.add(chunk);
        writeToAbsolutePosition(cb, p, 288, 246, 600, 0, f);

        // Indico que comienzo a escribir texto
        cb.beginText();
        // Indico la fuente a utilizar
        cb.setFontAndSize(bf, 8);

        System.out.println("FIRMAS REGISTRADAS: ");
        List<CadenaFirmaLog> listalog = getCadenaFirmaLogList(cadenaFirma);

        String eva = "", sd = "", da = "", dg = "";
        String evaTS = "", sdTS = "", daTS = "", dgTS = "";

        for (CadenaFirmaLog log_ : listalog) {

            System.out.println(log_.getClave() + " " + log_.getRfc() + " " + log_.getFecha().toString() + " " + log_.getFirma());
            try {
                if (log_.getRol().equalsIgnoreCase("eva")) {
                    eva = log_.getFirma();
                    evaTS = log_.getFecha().toString();
                } else if (log_.getRol().equalsIgnoreCase("sd")) {
                    sd = log_.getFirma();
                    sdTS = log_.getFecha().toString();
                } else if (log_.getRol().equalsIgnoreCase("da")) {
                    da = log_.getFirma();
                    daTS = log_.getFecha().toString();
                } else if (log_.getRol().equalsIgnoreCase("dg")) {
                    dg = log_.getFirma();
                    dgTS = log_.getFecha().toString();
                }
            } catch (Exception ex) {
            }
        }

        if (eva == null) {
            eva = "";
        }
        if (sd == null) {
            sd = "";
        }
        if (da == null) {
            da = "";
        }
        if (dg == null) {
            dg = "";
        }

        // Indico la posicion en la que va a ser colocado el texto
        int x = 60;
        int y = 620;

        // total de caracteres de cada renglon de la cadena firma
        int caracteresrenglon = 100;

        System.out.println("Firma DG : " + dg);
        System.out.println("TIME STAMP DG : " + dgTS);
        System.out.println("Firma DA : " + da);
        System.out.println("TIME STAMP DA : " + daTS);
        System.out.println("Firma SD : " + sd);
        System.out.println("TIME STAMP SD : " + sdTS);
        System.out.println("Firma EVA : " + eva);
        System.out.println("TIME STAMP EVA : " + evaTS);

        cb.setTextMatrix(x, y);
        y -= 10;
        cb.showText("DIRECTOR GENERAL:");
        if (dg != null && dg.length() > 0) {

//	        cb.setTextMatrix(x, y); y-=10;
//	        cb.showText(dg.substring(0, dg.length()/2));	        
//	        cb.setTextMatrix(x, y); y-=40;
//	        cb.showText(dg.substring(dg.length()/2));
            int total = dg.length();
            int inicio = 0, fin = 0;
            caracteresrenglon = caracteresrenglon > total ? total : caracteresrenglon;
            int renglones = (total / caracteresrenglon) + 1;

            for (int a = 1; a <= renglones; a++) {
                fin += a == renglones ? (total - (caracteresrenglon * (a - 1))) : caracteresrenglon;
                cb.setTextMatrix(x, y);
                cb.showText(dg.substring(inicio, fin));
                inicio = fin;
                y -= 10;
            }
            cb.setTextMatrix(x, y);
            cb.showText(dgTS);
            y -= 30;
        }

        cb.setTextMatrix(x, y);
        y -= 10;
        cb.showText("DIRECTOR DE ÁREA:");
        if (da != null && da.length() > 0) {

//        	cb.setTextMatrix(x, y); y-=10;
//          cb.showText(da.substring(0, da.length()/2));
//	        cb.setTextMatrix(x, y); y-=40;
//	        cb.showText(da.substring(da.length()/2));
            int total = da.length();
            int inicio = 0, fin = 0;
            caracteresrenglon = caracteresrenglon > total ? total : caracteresrenglon;
            int renglones = (total / caracteresrenglon) + 1;

            for (int a = 1; a <= renglones; a++) {
                fin += a == renglones ? (total - (caracteresrenglon * (a - 1))) : caracteresrenglon;
                cb.setTextMatrix(x, y);
                cb.showText(da.substring(inicio, fin));
                inicio = fin;
                y -= 10;
            }
            cb.setTextMatrix(x, y);
            cb.showText(daTS);
            y -= 30;
        }

        cb.setTextMatrix(x, y);
        y -= 10;
        cb.showText("SUBDIRECTOR DE ÁREA");
        if (sd != null && sd.length() > 0) {

//        	cb.setTextMatrix(x, y); y-=10;
//	        cb.showText(sd.substring(0, sd.length()/2));
//	        cb.setTextMatrix(x, y); y-=40;
//	        cb.showText(sd.substring(sd.length()/2));
            int total = sd.length();
            int inicio = 0, fin = 0;
            caracteresrenglon = caracteresrenglon > total ? total : caracteresrenglon;
            int renglones = (total / caracteresrenglon) + 1;

            for (int a = 1; a <= renglones; a++) {
                fin += a == renglones ? (total - (caracteresrenglon * (a - 1))) : caracteresrenglon;
                cb.setTextMatrix(x, y);
                cb.showText(sd.substring(inicio, fin));
                inicio = fin;
                y -= 10;
            }
            cb.setTextMatrix(x, y);
            cb.showText(sdTS);
            y -= 30;
        }

        if (!eva.equals("")) {
            cb.setTextMatrix(x, y);
            y -= 10;
            cb.showText("EVALUADOR");

            if (eva.length() > 0) {

//		        cb.setTextMatrix(x, y); y-=10;
//		        cb.showText(eva.substring(0, eva.length()/2));
//		        cb.setTextMatrix(x, y); //y-=40;
//		        cb.showText(eva.substring(eva.length()/2));
                int total = eva.length();
                int inicio = 0, fin = 0;
                caracteresrenglon = caracteresrenglon > total ? total : caracteresrenglon;
                int renglones = (total / caracteresrenglon) + 1;

                for (int a = 1; a <= renglones; a++) {
                    fin += a == renglones ? (total - (caracteresrenglon * (a - 1))) : caracteresrenglon;
                    cb.setTextMatrix(x, y);
                    cb.showText(eva.substring(inicio, fin));
                    inicio = fin;
                    y -= 10;
                }
                cb.setTextMatrix(x, y);
                cb.showText(evaTS);
                //y-=30;
            }
        }

        // Creo una imagen para agregarla y le pongo propiedades de posicion y escala
        // Image image = Image.getInstance(new URL(URLIMAGEN));
        com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(srcImage);
        image.setAbsolutePosition(x, 120);
        // image.scalePercent(3);
        cb.addImage(image);

        cb.setFontAndSize(bf, 9);
        cb.setTextMatrix(95, 120);
        cb.showText("Número de validación");

        cb.setFontAndSize(bf, 7);
        cb.setTextMatrix(100, 110);
        cb.showText(cadenaFirma.getBitacora());
        /*
        La versión electrónica del presente documento, su integridad y autoría se podrá comprobar a través de la página electrónica de la Secretaría de
        Medio Ambiente y Recursos Naturales por medio de la siguiente liga: http://goo.gl/QiNqR; para lo cual, será necesario capturar el número de
        validación de la presente representación impresa del documento digital. De igual manera, podrá verificar por medio del código QR para lo cual, se
        recomienda descargar una aplicación de lectura de éste tipo de códigos a su dispositivo móvil
         */
        cb.setTextMatrix(240, 250);
        cb.showText("La versión electrónica del presente documento, su integridad y autoría se podrá comprobar a través");
        cb.setTextMatrix(240, 240);
        cb.showText("de la página electrónica de la Secretaría de Medio Ambiente y Recursos Naturales   por medio de la");
        cb.setTextMatrix(240, 230);
        cb.showText("siguiente liga:");
        cb.setTextMatrix(240, 220);
        cb.showText("De  igual manera, podrá verificar por medio del código QR para lo cual, se recomienda descargar una");
        cb.setTextMatrix(240, 210);
        cb.showText("aplicación de lectura de éste tipo de códigos a su dispositivo móvil");

        // tamanio del texto de "hoja de firmas"
        cb.setFontAndSize(bf, 7);
        cb.setTextMatrix(275, 30);
        cb.showText("HOJA DE FIRMAS");

        // COLOCAR EL NUMERO DE OFICIO
        cb.setFontAndSize(bf, 11);
        cb.setTextMatrix(525, 755);
        cb.showText(numeroOficio);

        // Indico que termine de agregar texto
        cb.endText();

        // Agrego una imagen, la cual ya tiene las propiedades de posicion
        // cb.addImage(image, false);
        // Cierro el stamper y se crea el archivo.
        stamp.close();
        reader.close();

        // ahora elimino el pdf auxiliar....
        try {
            Files.deleteIfExists(Paths.get(aux));
        } catch (NoSuchFileException x1) {
            System.err.format("%s: no such" + " file or directory%n", aux);
        } catch (DirectoryNotEmptyException x2) {
            System.err.format("%s not empty%n", aux);
        } catch (IOException x3) {
            // File permission problems are caught here.
            System.err.println(x3);
        }
    }

    private static String getDate() {
        SimpleDateFormat formateador1 = new SimpleDateFormat(
                "MMMM", new Locale("es"));
        Date fechaDate1 = new Date();
        String mes = formateador1.format(fechaDate1);

        mes = mes.substring(0, 1).toUpperCase() + mes.substring(1, mes.length());
        System.out.println(mes);

        SimpleDateFormat formateador = new SimpleDateFormat(
                "dd 'de " + mes + " de' yyyy", new Locale("ES"));
        Date fechaDate = new Date();
        String fecha = formateador.format(fechaDate);
        System.out.println(fecha);

        return fecha;
    }

    //<editor-fold defaultstate="collapsed" desc="Metodo para obtener el oficio de DGIRA_GO">
    private static String getNumeroOficio(DocumentoRespuestaDgira documento) {
        int idGO;
        mx.gob.semarnat.model.dgira_miae.Document expedient = new mx.gob.semarnat.model.dgira_miae.Document();
        try {
            String claveTramite = documento.getDocumentoRespuestaDgiraPK().getClaveTramite();
            switch (documento.getDocumentoRespuestaDgiraPK().getTipoDocId()) {
                //Prevención por requisitos
                case 4:
                    idGO = 94;
                    break;
                case 3:
                    if (claveTramite.equals("DC")) {
                        //Oficio de Prevención Solicitud de Exención
                        idGO = 1176;
                    } else {
                        //Prevención pago de derechos
                        idGO = 118;
                    }
                    break;
                case 5:
                    if (claveTramite.equals("IP")) {
                        //Oficio de Prevención(IP)
                        idGO = 2022;
                    } else {
                        //Prevención por pago de derechos y requisitos
                        idGO = 124;
                    }
                    break;
                //Solicitud de Información Adicional
                case 7:
                    idGO = 684;
                    break;
                case 12:
                    if (claveTramite.equals("IP")) {
                        //Resolutivo Procedente IP
                        idGO = 1143;
                    } else {
                        //Oficio Autorizado
                        idGO = 542;
                    }
                    break;
                //Oficio Autorizado Condicionado
                case 13:
                    idGO = 1499;
                    break;
                case 14:
                    if (claveTramite.equals("IP")) {
                        //Resolutivo No Procedente IP
                        idGO = 1149;
                    } else {
                        //Oficio Resolutivo Negado
                        idGO = 1532;
                    }
                    break;
                //Resolutivo Desistido
                case 21:
                    idGO = 384;
                    break;
                //Generación de Oficio Resolutivo: Desechamiento
                case 6:
                    idGO = 513;
                    break;
                case 2:
                    if (claveTramite.equals("DC")) {
                        //Oficio Resolutivo Negado (Solicitud de Exención)
                        idGO = 1556;
                    } else {
                        //Notificación a Gobiernos
                        idGO = 380;
                    }
                    break;
                //Oficio de Consulta Pública
                case 10:
                    idGO = 983;
                    break;
                //Oficio de Consulta Pública (Promovente)
                case 9:
                    idGO = 1077;
                    break;
                //Oficio de Consulta Pública (Unidad Administrativa)
                case 8:
                    idGO = 1078;
                    break;
                //Oficio de Consulta Pública (Ciudadano Secundario)
                case 11:
                    idGO = 1079;
                    break;
                case 1:
                    if (claveTramite.equals("DC")) {
                        //Oficio Resolutivo Autorizado (Solicitud de Exención)
                        idGO = 1557;
                    } else {
                        //Oficio Opinión Técnica
                        idGO = 1012;
                    }
                    break;
                default:
                    idGO = 0;
                    break;
            }

            ServicioDocument sd = new ServicioDocument();

            expedient = sd.findByNamedQuery("Document.findByProjectIdAndDocumentId",
                    documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto(),
                    idGO).get(0);

            System.out.println(":: ProjectId :: " + documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
            System.out.println(":: DocumentId :: " + idGO);

        } catch (MiaExcepcion ex) {
            ex.printStackTrace();
        }

        return String.valueOf(expedient.id);
    }
    //</editor-fold>

    private static void writeToAbsolutePosition(PdfContentByte cb, String text, float xMargin, float yMargin, float width, float height, Font fontStyle) {
        ColumnText ct = new ColumnText(cb);
        Phrase myText = new Phrase(text, fontStyle);
        ct.setSimpleColumn(xMargin, yMargin, width, height);
        try {
            ct.addElement(myText);
            ct.go();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * *
     *
     * @param cb PdfContentByte cb
     * @param myText Phrase myText
     * @param xMargin posicion x
     * @param yMargin posicion y
     * @param width ancho
     * @param height alto
     * @param fontStyle Font fontStyle
     */
    private static void writeToAbsolutePosition(PdfContentByte cb, Phrase myText, float xMargin, float yMargin, float width, float height, Font fontStyle) {
        ColumnText ct = new ColumnText(cb);
        ct.setSimpleColumn(xMargin, yMargin, width, height);
        try {
            ct.addElement(myText);
            ct.go();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * *
     * Regresa la ultima firma realizada electronicamente por cada persona
     *
     * @param detalles contiene la lista de firmas extraidas previamente del ws
     * de consulta de firmas
     * @return regresa un registro por persona
     * @throws ParseException, Exception
     */
    public static HashMap<String, Firmas> GetUltimaFirmaPorRfc(DetallesFirmado detalles) throws ParseException, Exception {

        System.out.println("GetUltimaFirmaPorRfc...");
        HashMap<String, Firmas> mapa = new HashMap<String, Firmas>();

        if (detalles != null && detalles.getFirmas().length > 0) {

            for (Firmas f : detalles.getFirmas()) {

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
                Date date = null;
                //try {
                date = formatter.parse(f.getFecha());
                /*} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/

                if (!mapa.containsKey(f.getRfc())) {

                    mapa.put(f.getRfc(), f);

                } else {

                    Firmas firmax = mapa.get(f.getRfc());
                    Date fechax = null;
                    //try {
                    fechax = formatter.parse(firmax.getFecha());
                    /*} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
                    if (date != null && fechax != null) {
                        if (date.after(fechax)) {
                            mapa.put(f.getRfc(), f);
                        }
                    }
                }
            }
        }
        return mapa;
    }

    /**
     * *
     * Regresa la ultima firma realizada electronicamente para el oficio actual
     * sin importar que RFC sea
     *
     * @param detalles contiene la lista de firmas extraidas previamente del ws
     * de consulta de firmas
     * @return regresa la ultima firma registrada
     * @throws ParseException, Exception
     */
    private static Firmas GetUltimaFirmaDelWSParaElOficio(DetallesFirmado detalles) throws ParseException, Exception {

        System.out.println("GetUltimaFirmaDelWSParaElOficio...");
        Firmas firma = null;
        if (detalles != null && detalles.getFirmas().length > 0) {

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
            firma = detalles.getFirmas()[0];
            Date date = formatter.parse(firma.getFecha());
            Date fechax = null;

            for (Firmas f : detalles.getFirmas()) {

                fechax = formatter.parse(f.getFecha());

                if (date != null && fechax != null) {
                    if (fechax.after(date)) {
                        firma = null;
                        firma = f;
                        date = formatter.parse(f.getFecha());
                    }
                }
            }
        }
        if (firma == null) {
            System.out.println("null");
        } else {
            System.out.println(firma.getRfc() + " - " + firma.getFecha());
        }
        return firma;
    }

    /**
     * *
     * Regresa una Firma valida ( si es que la hay ) para indicar que el usuario
     * ya firmó el oficio
     *
     * @param cadenaFirma
     * @param detalles
     * @return
     * @throws Exception
     * @throws ParseException
     */
    public Firmas GetFirmaWS(CadenaFirma cadenaFirma, DetallesFirmado detalles) throws ParseException, Exception {

        System.out.println("\nGetFirmaWS...");

        // obtengo la ultima firma del ws para el oficio en cuestion... ( que esta implicito en los detalles)
        // fm puede ser null ya que tal vez no hay firmas en el WS..
        Firmas fm = GetUltimaFirmaDelWSParaElOficio(detalles);

        // obtengo un hashtable de la lista de cadenafirmalog ( de la cadena firma en cuestion )
        HashMap<String, CadenaFirmaLog> cadenaFirmaLogRegistradas = getCadenaFirmaLogHashList(cadenaFirma);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");

        // si fm es null significa que el ws no tiene registrada ninguna firma para el oficio...
        // o si en cadenafirmalog no hay elementos... no hay nada que validar y se regresa fm..
        if (fm == null || cadenaFirmaLogRegistradas.size() == 0) {
            return fm;
        }

        // primero ver si 'fm' se realizó despues que todas las firmas ya registradas en 'cadenaFirmaLogList' para asegurar que no esté ya registrada en cadenafirmalog...
        Date fechaultimafirma_ws = formatter.parse(fm.getFecha());
        for (String key : cadenaFirmaLogRegistradas.keySet()) {

            CadenaFirmaLog cadenafirmalogregistrado = cadenaFirmaLogRegistradas.get(key);
            if (fechaultimafirma_ws.before(cadenafirmalogregistrado.getFecha()) || fechaultimafirma_ws.equals(cadenafirmalogregistrado.getFecha())) {
                return null;
            }
        }

        return fm;
    }

    /**
     * *
     * Obtiene un objeto de Firma (electronica) que corresponde a la ultima
     * firma registrada en el ws de consulta ( sin importar ningun parametro
     * adicional )
     *
     * @param detalles
     * @return un objeto Firma que representa una firma de la lista de firmas
     * que devuelve el ws de consulta de firmas propiamente...
     * @throws Exception
     * @throws ParseException
     */
    public static Firmas GetUltimaFirmaGlobal(DetallesFirmado detalles) throws ParseException, Exception {

        System.out.println("GetUltimaFirmaGlobal...");
        String rfc = "";
        Date fechaultima = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
        HashMap<String, Firmas> mapa = GetUltimaFirmaPorRfc(detalles);
        for (String key : mapa.keySet()) {

            Firmas value = mapa.get(key);
            Date date = null;
            //try {
            date = formatter.parse(value.getFecha());

            if (fechaultima == null || (fechaultima != null & date.after(fechaultima))) {
                fechaultima = date;
                rfc = value.getRfc();
            }
            /*} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        }
        return mapa.get(rfc);
    }

    /**
     * *
     * Obtiene el objeto de Firma mas reciente (electronica) de la lista de
     * objetos que trae el Map..
     *
     * @param mapa
     * @return
     * @throws ParseException, Exception
     */
    public static Firmas GetUltimaFirmaMapa(HashMap<String, Firmas> mapa) throws ParseException, Exception {

        String rfc = "";
        Date fechaultima = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");
        for (String key : mapa.keySet()) {

            Firmas value = mapa.get(key);
            Date date = null;
            // try {
            date = formatter.parse(value.getFecha());

            if (fechaultima == null || (fechaultima != null & date.after(fechaultima))) {
                fechaultima = date;
                rfc = value.getRfc();
            }
            /*} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        }
        return mapa.get(rfc);
    }

    /**
     * *
     * Obtiene la ultima firma registrada en el ws de consulta de firmas, pero
     * de la persona o rfc indicado
     *
     * @param detalles
     * @param rfc
     * @return
     * @throws Exception
     * @throws ParseException
     */
    private Firmas GetUltimaFirmaDelRFC(DetallesFirmado detalles, String rfc) throws ParseException, Exception {
        System.out.println("GetUltimaFirmaDelRFC...");
        HashMap<String, Firmas> listaultimafirmaporrfc = GetUltimaFirmaPorRfc(detalles);
        return listaultimafirmaporrfc.get(rfc);
    }

    /**
     * *
     * Regresa una Firmas si es que hay unas mas actual para el rfc que traemos
     * en cadenafirmalog
     *
     * @param detalles
     * @param cadenafirmaLog
     * @return
     * @throws Exception
     * @throws ParseException
     */
    public Firmas GetUltimaFirmaSuperiorDelRFCActual(DetallesFirmado detalles, CadenaFirmaLog cadenafirmaLog) throws ParseException, Exception {

        System.out.println("GetUltimaFirmaSuperiorDelRFCActual...");
        Firmas fm = GetUltimaFirmaDelRFC(detalles, cadenafirmaLog.getRfc());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd H:mm:ss");

        Date fechanueva = null;
        System.out.println("fecha de cadenafirmalog :" + cadenafirmaLog.getFecha().toString());
        try {
            fechanueva = formatter.parse(fm.getFecha());
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (fechanueva != null) {

            System.out.println("fecha nueva :" + formatter.format(fechanueva));
            if (fechanueva.after(cadenafirmaLog.getFecha())) {
                return fm;
            }

        } else {
            System.out.println("fecha nueva : null");
        }
        return null;
    }

    /**
     * *
     * Regresa la ultima firma registrada en el ws cuyo rfc no se encuentre ya
     * resitrado en cadenafirmalog
     *
     * @param detalles representa el objeto DetallesFirmado que devuelve la
     * consulta de ws de firmas
     * @param cadenafirma encabezado que sirve para traer la lista de firmas log
     * internamente en el metodo.
     * @return
     * @throws Exception
     * @throws ParseException
     */
    public Firmas GetUltimaFirmaExcluidadeCadenaFirmaLogList(DetallesFirmado detalles, CadenaFirma cadenafirma) throws ParseException, Exception {

        System.out.println("GetUltimaFirmaExcluidadeCadenaFirmaLogList...");
        // try{
        // firma mas recietne por persona que ha firmado el ws
        HashMap<String, Firmas> listaultimafirmaporrfc = GetUltimaFirmaPorRfc(detalles);
        // lista de firmas registradas en cadenafirmalog
        List<CadenaFirmaLog> cadenafirmaloglist = getCadenaFirmaLogList(cadenafirma);

        // iterar la lista cadenafirmaloglist para en base al rfc ir borrando del mapa de firmas devueltos en el ws
        for (CadenaFirmaLog cad : cadenafirmaloglist) {
            if (listaultimafirmaporrfc.containsKey(cad.getRfc())) {
                listaultimafirmaporrfc.remove(cad.getRfc());
            }
        }

        // de los elementos que quedaron en el map de listaultimafirmaporrfc, obtener el mayor...
        return GetUltimaFirmaMapa(listaultimafirmaporrfc);
        /*}catch(Exception ex){
			ex.printStackTrace();
		}*/
        // return null;
    }

    private File generateQR(File file, String text, int h, int w) throws Exception {

        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix matrix = writer.encode(text, com.google.zxing.BarcodeFormat.QR_CODE, w, h);

        BufferedImage image = new BufferedImage(matrix.getWidth(), matrix.getHeight(), BufferedImage.TYPE_INT_RGB);
        image.createGraphics();

        Graphics2D graphics = (Graphics2D) image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, matrix.getWidth(), matrix.getHeight());
        graphics.setColor(Color.BLACK);

        for (int i = 0; i < matrix.getWidth(); i++) {
            for (int j = 0; j < matrix.getHeight(); j++) {
                if (matrix.get(i, j)) {
                    graphics.fillRect(i, j, 1, 1);
                }
            }
        }

        ImageIO.write(image, "png", file);

        return file;

    }

    private String decoder(File file) throws Exception {

        FileInputStream inputStream = new FileInputStream(file);

        BufferedImage image = ImageIO.read(inputStream);

        //int width = image.getWidth();
        //int height = image.getHeight();
        //int[] pixels = new int[width * height];
        LuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        // decode the barcode
        QRCodeReader reader = new QRCodeReader();
        Result result = reader.decode(bitmap);
        return result.getText();
    }
}
