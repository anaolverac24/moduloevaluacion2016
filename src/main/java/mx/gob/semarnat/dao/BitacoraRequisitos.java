package mx.gob.semarnat.dao;

import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gob.semarnat.model.dgira_miae.RequisitosSubdirector;
import mx.gob.semarnat.model.dgira_miae.RequisitosSubdirectorPK;
import mx.gob.semarnat.utils.GenericConstants;
import mx.gob.semarnat.ws.WSRecuperaArchivos;

public class BitacoraRequisitos {
	
    private final String PSTRequisitos_AO = "{call DGIRA_MIAE2.TRASLADA_REQUISITOS(?)}";    
    private Connection connection = null;
    // ================= POOL DE DESARROLLO ==========================
    final String driverDB = "oracle.jdbc.OracleDriver";
    final String urlDB = "jdbc:oracle:thin:@scan-db.semarnat.gob.mx:1525/SINDESA";    
    final String userDB = "DGIRA_MIAE2";
    final String passDB = "DGIRA_MIAE2";
    // ================= POOL DE PRODUCCION ==========================
    /*final String driverDB = "oracle.jdbc.OracleDriver";
    final String urlDB = "jdbc:oracle:thin:@rac1-scan.semarnat.gob.mx:1525/SINAT";    
    final String userDB = "DGIRA_MIAE2";
    final String passDB = "Dg1r4MiaE2";*/
    
    private boolean digital = false;
    
    private Connection getConexion() throws ClassNotFoundException, SQLException {
        Class.forName(driverDB);
        connection = DriverManager.getConnection(urlDB, userDB, passDB);
        return connection;
    }

    private void closeConnection() throws SQLException {
        connection.close();
    }

    //SP de carga de avance del proyecto
    private void ejecutaSP(String bitacora) {
        try {
            //guarda avance en SINATEC
            CallableStatement cs = getConexion().prepareCall(PSTRequisitos_AO);
            cs.setString(1, bitacora);
            cs.execute();
            closeConnection();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(BitacoraRequisitos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("unchecked")
	private void ejecutaWS(String bitacora, String folio, String claveTipoTramite, Integer idTipoTramite) {
        DgiraMiaeDaoGeneral daoGeneral = new DgiraMiaeDaoGeneral();
        
        //Obtencion de registros en la BD
        List<RequisitosSubdirector> reqSubd = (List<RequisitosSubdirector>)
                daoGeneral.lista_namedQuery("RequisitosSubdirector.findByReqsdBitacora", new Object[]{bitacora}, new String[]{"reqsdBitacora"});
        
        //Se busca la existencia elementos, para comprobar si ya fueron insertados los documentos
        if ( reqSubd.isEmpty() ) {
            HashMap<Long,String> mapArchivos = new HashMap<>();

            //Obtencion de todos los documentos obtenidos por WS
            mapArchivos = WSRecuperaArchivos.obtenerListaArchivos(Integer.parseInt(folio), GenericConstants.WS_TIPO_DOCTO_GENERICO);
            mapArchivos.putAll(WSRecuperaArchivos.obtenerListaArchivos(Integer.parseInt(folio), GenericConstants.WS_TIPO_DOCTO_REQUISITOS_GENERICOS));
            
            //No existen, por lo que se insertaran
            Set<Long> llaves = mapArchivos.keySet();
            RequisitosSubdirector reqSub;
            
            for(Long llave : llaves) {
                //Inserta registro del tramite
                reqSub = new RequisitosSubdirector(new RequisitosSubdirectorPK(bitacora, BigInteger.valueOf(llave), claveTipoTramite), 
                        BigInteger.valueOf(idTipoTramite), mapArchivos.get(llave));
                reqSub.setReqsdEntregadoEcc(BigInteger.ONE);
                
                daoGeneral.agrega(reqSub);
                
            }
        }
    }
    
    public void generaRequisitos(String bitacora, String folio, String claveTipoTramite, Integer idTipoTramite) {
        //De acuerdo al tipo de bitacora se ejecuta el procedimiento SP o se extrae via WS
        if(bitacora.charAt(5) == 'W') {//WS
            digital = true;
            ejecutaWS(bitacora, folio, claveTipoTramite, idTipoTramite);
        } else {//StoredProcedure
            digital = false;
            ejecutaSP(bitacora);
        }
    }

    public boolean isDigital() {
        return digital;
    }

}
