/**
 * 
 */
package mx.gob.semarnat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.UsuarioRol;

/**
 * @author dpaniagua.
 *
 */
public class UsuarioRolDAO {
	/**
	 * Entity Manager para realizar las transacciones a la base de datos de DGIRA_MIAE2.
	 */
    private final EntityManager emfDgira = PersistenceManager.getEmfMIAE().createEntityManager();
    /**
     * Permite consultar la lista de roles del usuario.
     * @param idUsuario a consultar.
     * @return la lista de roles del usuario.
     */
    public List<UsuarioRol> consultarRolesByIdUsuario(int idUsuario) throws Exception {
        Query q = emfDgira.createQuery("from UsuarioRol ur WHERE ur.usuarioId.idusuario = :idUsuario");
        
        q.setParameter("idUsuario", idUsuario);
        List<UsuarioRol> rolesUsuario = q.getResultList();
        return rolesUsuario;    	
	}
    
    /**
     * Permite consultar la lista de roles del usuario.
     * @param idUsuario a consultar.
     * @return la lista de roles del usuario.
     */
    public int eliminarRolByIdUsuarioAndByIdRol(int idUsuario, int idRolUsuario) throws Exception {
    	emfDgira.getTransaction().begin();
    	Query query = emfDgira.createQuery("DELETE FROM UsuarioRol ur WHERE ur.usuarioId.idusuario = :idUsuario AND ur.rolId.idRol = :idRol");
    	query.setParameter("idUsuario", idUsuario);
    	query.setParameter("idRol", idRolUsuario);
    	int eliminarRol = query.executeUpdate();
    	
    	emfDgira.getTransaction().commit();    	
    	
    	return eliminarRol;
	}    
}
