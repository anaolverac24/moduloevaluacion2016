package mx.gob.semarnat.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class ExencionDao {
	
	private final EntityManager emfMIAE = PersistenceManager.getEmfMIAE().createEntityManager();
	
	public String obtenerRutaSubida() {
		
		String result = null;
		try {
			Query q = emfMIAE.createNativeQuery("SELECT PAR_RUTA FROM CAT_PARAMETRO WHERE PAR_DESCRIPCION = 'ADJUNTOS'");      
			result = (String) q.getSingleResult();
		} catch(Exception e) {
			e.printStackTrace();
		}

        return result;
    }
	
	
	public String obtenerFolioByIdExencion(long idExencion) {
		
		String result = "";
		try {
			Query q = emfMIAE.createNativeQuery("select e.BGTRAMITEDID from EXENCION e where e.ID_EXENCION = :idExencion");
			q.setParameter("idExencion", idExencion);
			
			result = (String) q.getSingleResult().toString();
		} catch(Exception e) {
			e.printStackTrace();
		}

        return result;
    }

}
