/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

import java.awt.HeadlessException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import mx.gob.semarnat.model.dgira_miae.AnalisisPreliminarProyecto;
import mx.gob.semarnat.model.dgira_miae.ConsultaPublicaProyecto;

/**
 *
 * @author Rengerden
 */
public class APDao implements Serializable {

    private final EntityManager emfanalisis = EntityFactoryAnalisis.getDGIRA_MIAE().createEntityManager();

    //<editor-fold desc="Listado a partir de un namedQuery" defaultstate="collapsed">
    /**
     * Hace listado a partir de un NamedQuery dentro de la entidad mapeada
     * @param qry
     * @param args
     * @param paramName
     * @return 
     */
    public List<?> lista_namedQuery(String qry, Object[] args, String[] paramName) {
        Query q;
        List<?> resultado = new ArrayList<>();
        try {
            q = emfanalisis.createNamedQuery(qry);
            for (int i = 0; i < args.length; i++) {
                q.setParameter(paramName[i], args[i]);
            }
            resultado = q.getResultList();
        } catch (Exception e) {
        }finally{
            emfanalisis.clear();
        }
        return resultado;
    }//</editor-fold>
    
    public List<Object[]> analisisUno() {
        List<Object[]> l=null;
        try
        {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion from CatAnalisisPreliminar c WHERE c.catCategoriaId=1");
            l = q.getResultList();
        }
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null,  "suma: " + e.getMessage(), "  Exito", JOptionPane.INFORMATION_MESSAGE);
        }finally{
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisDos() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=2");
            l = q.getResultList();
        } catch (Exception e) {
        }finally{
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisTres() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=3");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisCuatro() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=4");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisCinco() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=5");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisSeis() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=6");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisSiete() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=7");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisOcho() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=8");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisNueve() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=9");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisDiez() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=10");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisOnce() {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=11");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisDoce() {
        List<Object[]> l = null;
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=12");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public List<Object[]> analisisTrece() {
        List<Object[]> l = null;
        try {
            Query q = emfanalisis.createQuery("SELECT c.catAnalisisId, c.catAnalisisDescripcion FROM CatAnalisisPreliminar c WHERE c.catCategoriaId=13");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }

    public void guardaMonto(String bita, Integer sum, String folio) {
        try {
            emfanalisis.getTransaction().begin();
            //Query q = emfanalisis.createQuery("UPDATE Proyecto c SET c.proyValorCriterio= " +sum+" where c.bitacoraProyecto='09/MG-0007/02/15'");  
            Query q = emfanalisis.createQuery("UPDATE Proyecto_DGIRA c SET c.proyValorCriterio=" + sum + " where c.bitacoraProyecto=?1 OR c.proyectoPK.folioProyecto='" + folio + "'");
            q.setParameter(1, bita);
            q.executeUpdate();
            emfanalisis.getTransaction().commit();
//           JOptionPane.showMessageDialog(null,  "Guardado con Exito  suma: " + sum, "  Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (HeadlessException e) {
            //JOptionPane.showMessageDialog(null, "Error " + e, "Exito", JOptionPane.INFORMATION_MESSAGE);
            emfanalisis.getTransaction().rollback();
        }finally {
            emfanalisis.clear();
        }

    }

    public Double montoCalculado(String bitacora) {
        Double montoCalc = 0.0;
        try {
            Query q = emfanalisis.createQuery("SELECT c.monto FROM CatDerechos c WHERE c.limiteInferior  <= (SELECT p.proyValorCriterio FROM Proyecto_DGIRA p WHERE p.bitacoraProyecto='" + bitacora + "') AND c.catDerechosPK.idEstudio='MG' AND c.limiteSuperior >= (SELECT q.proyValorCriterio from Proyecto_DGIRA q where q.bitacoraProyecto='" + bitacora + "')");
            montoCalc = (Double) q.getSingleResult();
            return montoCalc;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, "Error " + e, "Exito", JOptionPane.INFORMATION_MESSAGE);

        }finally {
            emfanalisis.clear();
        }
        return montoCalc;

    }

    public void guardaMontoCalc(Double monto, String bitacora, String folio) {
        try {
            emfanalisis.getTransaction().begin();
            Query q = emfanalisis.createQuery("UPDATE Proyecto_DGIRA p SET P.proyCriterioMonto = " + monto + " WHERE p.bitacoraProyecto ='" + bitacora + "' OR p.proyectoPK.folioProyecto ='" + folio + "'");
            q.executeUpdate();
            emfanalisis.getTransaction().commit();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, "Error " + e, "Exito", JOptionPane.INFORMATION_MESSAGE);
            emfanalisis.getTransaction().rollback();
        } finally {
            emfanalisis.clear();
        }

    }

    public List<?> listadoCompleto(Class<?> tipo) {
        List<?> resultado = new ArrayList<>();
        try {
            CriteriaBuilder cBuilder = emfanalisis.getCriteriaBuilder();
            CriteriaQuery<Object> cq = cBuilder.createQuery();
            cq.select(cq.from(tipo));
            resultado = emfanalisis.createQuery(cq).getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return resultado;
    }

    public Object busca(Class<?> tipo, Object id) {
        Object resultado = new Object();
        try {
            resultado = emfanalisis.find(tipo, id);
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return resultado;
    }
    
    public List<Object[]>  critVal(String bitacora, short id) {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT v.analisisPreliminarProyectoPK.bitacoraProyecto, v.analisisPreliminarProyectoPK.catAnalisisId, v.analisisRespuesta, v.analisisObservacion FROM AnalisisPreliminarProyecto v WHERE v.analisisPreliminarProyectoPK.catAnalisisId =:idCrit AND v.analisisPreliminarProyectoPK.bitacoraProyecto =:bitaProy");            
            q.setParameter("bitaProy", bitacora);
            q.setParameter("idCrit", id);
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }
    
    public List<Object[]>  respConsultPub(String bitacora, String id) {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfanalisis.createQuery("SELECT v.consultaPublicaProyectoPK.evaBitacoraProyecto, v.consultaPublicaProyectoPK.claveDgira, v.consultaPublicaRespuesta FROM ConsultaPublicaProyecto v WHERE v.consultaPublicaProyectoPK.evaBitacoraProyecto =:bitaProy AND v.consultaPublicaProyectoPK.claveDgira =:clveDGIRA");            
            q.setParameter("bitaProy", bitacora);
            q.setParameter("clveDGIRA", id);
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfanalisis.clear();
        }
        return l;
    }
    
    /**
     * 
     * @param o 
     */
    public void agrega(Object o) {
        emfanalisis.clear();
        EntityTransaction tx = emfanalisis.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                emfanalisis.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        }
        emfanalisis.clear();
    }
    
    /**
     *
     * @param clase
     */
    public void modifica(Object clase) {
        try {
            emfanalisis.getTransaction().begin();
            emfanalisis.merge(clase);
            emfanalisis.getTransaction().commit();
        } catch (Exception e) {
        } finally{
            emfanalisis.clear();
        }
    }//</editor-fold>    
       
    public AnalisisPreliminarProyecto analisisConsulta(String numBita, short id) {
        Query q = emfanalisis.createQuery("SELECT C FROM AnalisisPreliminarProyecto C WHERE C.analisisPreliminarProyectoPK.bitacoraProyecto=:bitaProy and C.analisisPreliminarProyectoPK.catAnalisisId=:idCrit");
        q.setParameter("bitaProy", numBita);
        q.setParameter("idCrit", id);
        try {
            return (AnalisisPreliminarProyecto) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
        } finally {
            emfanalisis.clear();
        }
        return new AnalisisPreliminarProyecto();
    }
    
    public ConsultaPublicaProyecto cpConsulta(String numBita, String id) {
        Query q = emfanalisis.createQuery("SELECT C FROM ConsultaPublicaProyecto C WHERE C.consultaPublicaProyectoPK.evaBitacoraProyecto=:bitaProy and C.consultaPublicaProyectoPK.claveDgira=:idCrit");
        q.setParameter("bitaProy", numBita);
        q.setParameter("idCrit", id);
        try {
            return (ConsultaPublicaProyecto) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
        } finally {
            emfanalisis.clear();
        }
        return new ConsultaPublicaProyecto();
    }    
    
}
