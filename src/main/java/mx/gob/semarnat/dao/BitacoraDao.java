/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao;

//<editor-fold defaultstate="collapsed" desc="Imports">
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import mx.gob.semarnat.model.bitacora.Bitacora;
import mx.gob.semarnat.model.bitacora.Historial;
import mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto;
import mx.gob.semarnat.model.dgira_miae.Proyecto;
import mx.gob.semarnat.model.dgira_miae.ProyectoPK;
import mx.gob.semarnat.model.seguridad.CatRol;
import mx.gob.semarnat.model.seguridad.Ctdependencia;
import mx.gob.semarnat.model.seguridad.Tbarea;//</editor-fold>
import mx.gob.semarnat.model.seguridad.Tbempleado;
import mx.gob.semarnat.model.seguridad.Tbusuario;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatDgira;
import mx.gob.semarnat.model.sinat.SinatSinatec;
import mx.gob.semarnat.model.sinatec.Vexdatostramite;
import mx.gob.semarnat.utils.GenericConstants;

/**
 *
 * @author Paty
 */
@SuppressWarnings("serial")
public class BitacoraDao implements Serializable {

    private final EntityManager emfBitacora = PersistenceManager.getEmfBitacora().createEntityManager();
    private final EntityManager emfDgira = PersistenceManager.getEmfMIAE().createEntityManager();

    public Proyecto proyecto(String folio, short serial) {
        return (Proyecto) emfDgira.find(Proyecto.class, new ProyectoPK(folio, serial));
    }

    /**
     * Permite eliminar los roles del usuario.
     *
     * @param idUsuario consultado.
     */
    public int eliminarRolesUsuario(int idUsuario) throws SQLException {
        int eliminarRoles = 0;
        try {
            emfBitacora.getTransaction().begin();
            Query query = emfBitacora.createQuery("DELETE FROM UsuarioRol WHERE usuarioId.idusuario = :idUsuario");
            query.setParameter("idUsuario", idUsuario);
            eliminarRoles = query.executeUpdate();
            
            emfBitacora.getTransaction().commit();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return eliminarRoles;
    }

    /**
     * Permite consultar la lista de roles del usuario para su edicion.
     *
     * @param idUsuario consultado.
     * @return la lista de roles del usuario.
     * @throws SQLException en caso de error al ejecutar la consulta.
     */
    @SuppressWarnings("unchecked")
    public List<UsuarioRol> consultarRolesUsuario(int idUsuario) throws SQLException {
        List<UsuarioRol> rolesUsuarioConsultado = new ArrayList<>();
        try {
            Query query = emfBitacora.createQuery("FROM UsuarioRol WHERE USUARIO_ID = :idUsuario");
            query.setParameter("idUsuario", idUsuario);
            
            rolesUsuarioConsultado = query.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }

        return rolesUsuarioConsultado;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Historial> enVentanilla() {
        List l = new ArrayList();
        try {
            Query q = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.histoSituacionActual = '010110'");
            l = q.getResultList();
        } catch (Exception e) {
        }
        return l;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Bitacora> enECC() {
        List l = new ArrayList();
        try {
            Query q = emfBitacora.createQuery("SELECT a FROM Bitacora a WHERE a.bitaSituacion= 'CIS104' AND a.bitaIdTramite IN (63,64,65,66,67) AND a.bitaFlujoClave = 32 ORDER BY a.bitaIdTramite");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }

    //<editor-fold desc="metodos generales">
    /**
     *
     * @param <T>
     * @param o
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getListObject(Object o) {
        List l = new ArrayList();
        try {
            l = emfBitacora.createQuery("SELECT o FROM " + o.getClass().getName() + " o ").getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }

    /**
     *
     * @param o
     */
    public void agrega(Object o) {
        emfBitacora.clear();
        EntityTransaction tx = emfBitacora.getTransaction();
        tx.begin();
        try {
            if (o != null && tx.isActive()) {
                emfBitacora.persist(o);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        } finally {
            emfBitacora.clear();
        }
    }

    /**
     *
     * @param tipo
     * @param id
     * @return
     */
    public Object busca(Class<?> tipo, Object id) {
        return emfBitacora.find(tipo, id);
    }

    /**
     *
     * @param clase
     */
    public void modifica(Object clase) {
        try {
            emfDgira.getTransaction().begin();
            emfDgira.merge(clase);
            emfDgira.getTransaction().commit();
        } catch (Exception e) {
        } finally {
            emfDgira.clear();
        }
    }//</editor-fold>

    //<editor-fold desc="En recepción del director">
    /**
     * Pantalla superior del DA
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepDirector2(Tbarea tbarea) {
        String sql = "SELECT * FROM DGIRA_MIAE2.VW_DIRECTOR_RECEP WHERE BITA_TIPO_INGRESO <> 'A' "
                + " AND BITA_AREA_RECIBE = " + tbarea.getTbareaPK().getIdarea() + " AND BITA_IDDEPENDENCIA_RECIBE = '" + tbarea.getIddependenciapadre() + "' "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de 
        //tramites fisicos, web y de otra dependencia

        Query q = emfBitacora.createNativeQuery(sql);

        List<Object[]> l;
        try {
            l = q.getResultList();
        } catch (Exception e) {
            l = new ArrayList<Object[]>();
        } finally {
            emfBitacora.clear();
        }

        return l;
    }//</editor-fold>

    //<editor-fold>
    /**
     *
     * Pantalla inferior del DA
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepDirectorTurnados(Tbarea tbarea) {

        String sql = "SELECT * FROM DGIRA_MIAE2.VW_DIRECTOR_TURNADOS WHERE BITA_TIPO_INGRESO <> 'A' "
                + " AND HISTO_AREA_RECIBE =:area AND BITA_IDDEPENDENCIA_RECIBE = :dependencia "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de 
        //tramites fisicos, web y de otra dependencia

        Query q = emfBitacora.createNativeQuery(sql);
        q.setParameter("area", tbarea.getTbareaPK().getIdarea());
        q.setParameter("dependencia", tbarea.getIddependenciapadre());

        List<Object[]> l;
        try {
            l = q.getResultList();
        } catch (Exception e) {
            l = new ArrayList<Object[]>();
        }finally {
            emfBitacora.clear();
        }

        return l;
    }//</editor-fold>

    //<editor-fold>
    /**
     *
     * @param bitacora
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> detalleProyDirector(String bitacora) {
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfBitacora.createQuery("SELECT A.bitaNumero , A.bitaIdProm , D.datosFiscalesPK.dfiRfc, A.bitaFofiRecepcion ,  A.bitaFechatur , A.bitaFecharec, A.bitaFmaxResolucion, F.descripcion, G.nombreCorto, E.descripcion, C.cve, C.nombre, H.sector, I.subsector, (SELECT J.rama FROM RamaProyecto J WHERE J.nrama = C.rama AND J.nsub = C.subsector ), (SELECT K.tipoProyecto FROM TipoProyecto K WHERE K.nrama = C.rama AND K.ntipo = C.tipoProy ), CASE WHEN (D.dfiRazonSocial IS NULL) THEN (CONCAT(D.dfiNombre,' ' ,D.dfiApellidoMaterno,' ' ,D.dfiApellidoPaterno)) ELSE D.dfiRazonSocial END, (SELECT L.area FROM Tbarea L WHERE L.identidadfederativa='09' AND  L.tbareaPK.idarea IN(SELECT DISTINCT B.histoAreaEnvio FROM Historial B WHERE A.bitaNumero = B.historialPK.histoNumero AND B.histoSituacionActual in('AT0001','AT0027') ) ) FROM Bitacora A, Proyecto_Bitacora C, DatosFiscales D , Tbsituacion E  , Tramite F, Modalidad G, SectorProyecto H, SubsectorProyecto I WHERE  A.bitaNumero =:numBita AND A.bitaNumero = C.numeroBita AND A.bitaIdProm = D.datosFiscalesPK.promId AND D.datosFiscalesPK.idTipoDireccion = 03 AND A.bitaSituacion = E.idsituacion AND A.bitaIdTramite = F.idTramite AND A.bitaIdTramite = G.modalidadPK.idTramite AND C.sector = H.nsec AND C.subsector = I.nsub    ");
            q.setParameter("numBita", bitacora);
            l = q.getResultList();
        } catch (Exception e) {
        }
        return l;
    }//</editor-fold>

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Tbarea> direccionTodos() {
        List l = new ArrayList();
        try {
            System.out.println("\ndireccionTodos...");
            Query q = emfBitacora.createQuery("SELECT a FROM Tbarea a WHERE a.tbareaPK.idarea in('02011500', '02011700', '02020200', '02020400', '02011100', '02011400', '02011200', '02020100', '02011600', '02011300', '02070100') AND a.estatus=1 ORDER BY a.tbareaPK.idarea");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }

    //<editor-fold>
    /**
     *
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Tbarea> direccionTodos(Tbarea area) {

        System.out.println("\ndireccionTodos...");

        Query q = null;

        List l = new ArrayList();
        try {
// si es de oficina central ademas de los parametros.. filtra la jerarquia 'JD'
            if (area.getIdentidadfederativa().equals("09")) {
                
                q = emfBitacora.createQuery("SELECT a FROM Tbarea a WHERE a.tbareaPK.idarea in('02011500', '02011700', '02020200', '02020400', '02011100', '02011400', '02011200', '02020100', '02011600', '02011300', '02070100') AND a.estatus=1 ORDER BY a.tbareaPK.idarea");
                
            } else {
                
                q = emfBitacora.createQuery("SELECT a FROM Tbarea a WHERE a.tbareaPK.iddependencia =:idDependencia AND a.iddependenciapadre =:idDependenciaPadre AND a.idareapadre =:idAreaPadre AND a.estatus=1 ORDER BY a.tbareaPK.idarea");
                
                String iddep = area.getCtdependencia().getIddependencia();
                String iddeppadre = area.getIddependenciapadre();
                String idareapadre = area.getIdareapadre();
                
                System.out.println("idDependencia : " + iddep);
                System.out.println("idDependenciaPadre : " + iddeppadre);
                System.out.println("idAreaPadre : " + idareapadre);
                
                q.setParameter("idDependencia", iddep);
                q.setParameter("idDependenciaPadre", iddeppadre);
                q.setParameter("idAreaPadre", idareapadre);
                
            }
            
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }//</editor-fold>

    //<editor-fold desc="Datos del proyecto Sinat en tabla Historial" defaultstate="collapsed">
    /**
     *
     * @param numBita
     * @return
     */
    public Historial datosProyHistorialGral(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora");
        q.setParameter("bitacora", numBita);

        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Bitacora no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            emfBitacora.clear();
        }
        return (Historial) o;
    }//</editor-fold>
    //<editor-fold desc="Datos del proyecto Sinat en tabla Historial" defaultstate="collapsed">

    /**
     *
     * @param numBita
     * @return
     */
    public Historial datosProyHistorial(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora and a.histoSituacionActual in('I00008','AT0027')");
        q.setParameter("bitacora", numBita);

        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Bitacora no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            emfBitacora.clear();
        }
        return (Historial) o;
    }//</editor-fold>

    public Historial datosProyHistorial2(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora and a.histoSituacionActual='I00010'");
        q.setParameter("bitacora", numBita);

        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Bitacora no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            emfBitacora.clear();
        }
        return (Historial) o;
    }

    //datosProyHistStatus: selecciona el ultimo registro que se encuntre con el estatus buscado
    public Historial datosProyHistStatus(String numBita, String situacion) {
        Object o = null;

        Query q1 = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora and a.histoSituacionActual=:situac");
        q1.setParameter("bitacora", numBita);
        q1.setParameter("situac", situacion);

        if (!q1.getResultList().isEmpty()) {
            if (q1.getResultList().size() == 1) {
                try {
                    o = q1.getSingleResult();
                } catch (NoResultException e) {
                    System.out.println("EROR: Bitacora no encontrado");
                } catch (Exception e) {
                    System.out.println("" + e.getLocalizedMessage());
                    e.printStackTrace();
                } finally {
                    emfBitacora.clear();
                }
            } else {

                Query q = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora and a.histoSituacionActual=:situac  and a.historialPK.histoConsecutiv=(SELECT MAX(p1.historialPK.histoConsecutiv) FROM Historial p1  WHERE p1.historialPK.histoNumero=:bitacora and p1.histoSituacionActual=:situac)");
                q.setParameter("bitacora", numBita);
                q.setParameter("situac", situacion);
                try {
                    o = q.getSingleResult();
                } catch (NoResultException e) {
                    System.out.println("EROR: Bitacora no encontrado");
                } catch (Exception e) {
                    System.out.println("" + e.getLocalizedMessage());
                    e.printStackTrace();
                } finally {
                    emfBitacora.clear();
                }
            }
        }

        return (Historial) o;
    }

    /**
     * *
     * Regresa un histórico-situación determinada para la bitacora indicada (
     * para ver si la bitácora ya paso por una situacion determinada )
     *
     * @param numBita
     * @param situacion
     * @return
     */
    public Historial datosProyHistStatus2(String numBita, String situacion) {
        Object o = null;

        Query q1 = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora and a.histoSituacionActual=:situac");
        q1.setParameter("bitacora", numBita);
        q1.setParameter("situac", situacion);

        if (!q1.getResultList().isEmpty()) {
            if (q1.getResultList().size() == 1) {
                try {
                    o = q1.getSingleResult();
                } catch (NoResultException e) {
                    System.out.println("EROR: Bitacora no encontrado");
                } catch (Exception e) {
                    System.out.println("" + e.getLocalizedMessage());
                    e.printStackTrace();
                } finally {
                    emfBitacora.clear();
                }
            } else {

                Query q = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora and a.histoSituacionActual=:situac  and a.historialPK.histoConsecutiv=(SELECT MIN(p1.historialPK.histoConsecutiv) FROM Historial p1  WHERE p1.historialPK.histoNumero=:bitacora and p1.histoSituacionActual=:situac)");
                q.setParameter("bitacora", numBita);
                q.setParameter("situac", situacion);
                try {
                    o = q.getSingleResult();
                } catch (NoResultException e) {
                    System.out.println("EROR: Bitacora no encontrado");
                } catch (Exception e) {
                    System.out.println("" + e.getLocalizedMessage());
                    e.printStackTrace();
                } finally {
                    emfBitacora.clear();
                }
            }
        }

        return (Historial) o;
    }

    /**
     * *
     * Regresa la lista de historico de la bitácora que cumpla con la situacion
     * actual indicada ( varios registros de la bitacora donde la situacion sea
     * la indicada )
     *
     * @param numBita
     * @param situacion
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Historial> numRegHistStatus(String numBita, String situacion) {
        List<Historial> lstReg = new ArrayList<>();
        try {
            Query q1 = emfBitacora.createQuery("SELECT a FROM Historial a WHERE a.historialPK.histoNumero=:bitacora and a.histoSituacionActual=:situac order by a.historialPK.histoConsecutiv");
            q1.setParameter("bitacora", numBita);
            q1.setParameter("situac", situacion);
            
            lstReg = (List<Historial>) q1.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return lstReg;
    }

    //<editor-fold desc="Datos del proyecto Sinat" defaultstate="collapsed">
    /**
     *
     * @param numBita
     * @return
     */
    public SinatDgira datosProySinatDgira(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a FROM SinatDgira a WHERE a.bitacora=:bitacora and a.situacion in('I00008','AT0027')");
        q.setParameter("bitacora", numBita);

        try {
            return (SinatDgira) q.getSingleResult();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        } finally {
            emfBitacora.clear();
        }
        return new SinatDgira();
    }//</editor-fold>

    public String datosProySinatDgiraSituacionAT0027(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a.observaciones FROM SinatDgira a WHERE a.bitacora=:bitacora AND a.situacion in ('AT0027') AND a.id = (SELECT MAX(s.id) FROM SinatDgira s WHERE s.bitacora=:bitacora AND s.situacion in ('AT0027'))");
        q.setParameter("bitacora", numBita);

        try {
            return q.getSingleResult().toString();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        } finally {
            emfBitacora.clear();
        }
        return "";
    }

    public String datosProySinatDgiraSituacionI00010(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a.observaciones FROM SinatDgira a WHERE a.bitacora=:bitacora AND a.situacion = 'I00010' AND a.id = (SELECT MAX(s.id) FROM SinatDgira s WHERE s.bitacora=:bitacora AND s.situacion = 'I00010')");
        q.setParameter("bitacora", numBita);
//        System.out.println(q.getSingleResult());
        try {
            return q.getSingleResult().toString();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        } finally {
            emfBitacora.clear();
        }
        return "";
    }

    /**
     * *
     * Regresa un alista de SinatDgira que indica si hay registros de estos con
     * la bitacora y situacion indicada
     *
     * @param numBita
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<SinatDgira> datosProySinatDgira2(String numBita) {
        List<SinatDgira> l = new ArrayList<>();
        try {
            Query q = emfBitacora.createQuery("SELECT a FROM SinatDgira a WHERE a.bitacora=:bitacora and a.situacion in('AT0027')");
            q.setParameter("bitacora", numBita);
            
            l = (List<SinatDgira>) q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }

    //<editor-fold desc="Subdirector" defaultsatte="collapsed">
    /**
     * Subdirector
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepSubDirec(Tbarea tbarea) {
        String sql = "SELECT * FROM DGIRA_MIAE2.VW_SUBDIRECTOR_RECEP WHERE BITA_TIPO_INGRESO <> 'A' "
                + " AND BITA_AREA_RECIBE = " + tbarea.getTbareaPK().getIdarea() + " AND BITA_IDDEPENDENCIA_RECIBE = '" + tbarea.getIddependenciapadre() + "' "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de 
        //tramites fisicos, web y de otra dependencia

        Query q = emfBitacora.createNativeQuery(sql);

        List<Object[]> l;
        try {
            l = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            l = new ArrayList<Object[]>();
        } finally {
            emfBitacora.clear();
        }
        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Proyectos en recepcion del evaluador">
    /**
     * Proyectos en recepción del evaluador
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepEvaluador(Tbarea tbarea) {

        String sql = "SELECT * FROM DGIRA_MIAE2.VW_EVALUADOR_RECEP WHERE BITA_TIPO_INGRESO <> 'A' "
                + " AND BITA_AREA_RECIBE = " + tbarea.getTbareaPK().getIdarea() + " AND BITA_IDDEPENDENCIA_RECIBE = '" + tbarea.getIddependenciapadre() + "' "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de 
        //tramites fisicos, web y de otra dependencia
        Query q = emfBitacora.createNativeQuery(sql);

        List<Object[]> l;
        try {
            l = q.getResultList();
        } catch (Exception e) {
            l = new ArrayList<Object[]>();
            e.printStackTrace();
        } finally {
            emfBitacora.clear();
        }

        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Proyectos en turnados del evaluador">
    /**
     * Proyectos en recepcion del evaluador
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepEvaluadorTurnados(Tbarea tbarea) {

        String sql = "SELECT * FROM DGIRA_MIAE2.VW_EVALUADOR_TURNADOS WHERE BITA_TIPO_INGRESO <> 'A' "
                + " AND BITA_AREA_RECIBE = " + tbarea.getTbareaPK().getIdarea() + " AND BITA_IDDEPENDENCIA_RECIBE = '" + tbarea.getIddependenciapadre() + "' "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de 
        //tramites fisicos, web y de otra dependencia
        Query q = emfBitacora.createNativeQuery(sql);

        List<Object[]> l;
        try {
            l = q.getResultList();
        } catch (Exception e) {
            l = new ArrayList<Object[]>();
        } finally {
            emfBitacora.clear();
        }

        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Proyectos con Oficio de prevneción pendientes de firma en recepcion del DG">
    /**
     * Proyectos con Oficio de prevencion pendientes de firma en recepcion del
     * DG Pantalla superior del DG
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepDGOfic(short tipoDocId, Tbarea tbarea) {

        String sql = "SELECT * FROM DGIRA_MIAE2.VW_DG_EN_EVAL WHERE BITA_TIPO_INGRESO <> 'A' "
                 + " AND BITA_IDDEPENDENCIA_RECIBE = '" + tbarea.getTbareaPK().getIddependencia() + "' "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de  
        //tramites fisicos, web y de otra dependencia
        Query q = emfBitacora.createNativeQuery(sql);
        
//        q.setParameter("area", tbarea.getIdareapadre());
        
        List<Object[]> l = new ArrayList<Object[]>();
        List<Object[]> l2 = new ArrayList<Object[]>();
        try {
            l = q.getResultList();
            // agrupar los registros por bitacora ya que la lista trae todos los oficios de cada bitacora ( agruparlos )
            HashMap<String, String> ht = new HashMap<>();
            for (Object[] obj : l) {
                // System.out.println(obj[11]);
                if (!ht.containsKey(obj[0].toString())) {
                    ht.put(obj[0].toString(), "");
                    l2.add(obj);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            emfBitacora.clear();
        }

        return l2;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Proyectos con Oficio de prevnecion pendientes de firma en recepcion del DG">
    /**
     * Proyectos con Oficio de prevencion pendientes de firma en recepcion del
     * DG Pantalla inferior del DG
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepDGOficFirmados(short tipoDocId, Tbarea tbarea) {

        String sql = "SELECT * FROM DGIRA_MIAE2.VW_DG_RES WHERE BITA_TIPO_INGRESO <> 'A' "
                + " AND BITA_IDDEPENDENCIA_RECIBE = '" + tbarea.getTbareaPK().getIddependencia() + "' "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de 
        //tramites fisicos, web y de otra dependencia

        Query q = emfBitacora.createNativeQuery(sql);
//        q.setParameter("area", tbarea.getIdareapadre());

        List<Object[]> l = new ArrayList<Object[]>();
        List<Object[]> l2 = new ArrayList<Object[]>();
        try {
//            q.setParameter("fechaTramites", new SimpleDateFormat("dd/MM/yyyy").parse(GenericConstants.FECHA_CONSULTA_TRAMITES));

            l = q.getResultList();

            // agrupar los registros por bitacora ya que la lista trae todos los oficios de cada bitacora ( agruparlos )
            HashMap<String, String> ht = new HashMap<>();
            for (Object[] obj : l) {

                if (!ht.containsKey(obj[0].toString())) {
                    ht.put(obj[0].toString(), "");
                    l2.add(obj);
                }
            }

        } catch (Exception e) {
            l = new ArrayList<Object[]>();
        } finally {
            emfBitacora.clear();
        }
        return l2;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Proyectos en direccion">
    /**
     * Proyectos en direccion
     *
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Bitacora> enDireccion() {
        List l = new ArrayList();
        try {
            Query q = emfBitacora.createQuery("SELECT a FROM Bitacora a WHERE a.bitaSituacion in('I00008','AT0027') AND a.bitaIdTramite IN (63,64,65,66,67) AND a.bitaFlujoClave = 32 ORDER BY a.bitaIdTramite");
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Detalle del proyecto dentro de SubDirector">
    /**
     * Detalle del proyecto dentro de SubDirector
     *
     * @param bitacora
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> detalleProySubDirector(String bitacora) {
        //Query q = emfBitacora.createQuery("SELECT A.bitaNumero , A.bitaIdProm , D.datosFiscalesPK.dfiRfc, A.bitaFofiRecepcion ,  A.bitaFechatur , A.bitaFecharec, A.bitaFmaxResolucion, F.descripcion, G.nombreCorto, E.descripcion, C.cve, C.nombre, H.sector, I.subsector, (SELECT J.rama FROM RamaProyecto J WHERE J.nrama = C.rama AND J.nsub = C.subsector ), (SELECT K.tipoProyecto FROM TipoProyecto K WHERE K.nrama = C.rama AND K.ntipo = C.tipoProy ), CASE WHEN (D.dfiRazonSocial IS NULL) THEN (CONCAT(D.dfiNombre,' ' ,D.dfiApellidoMaterno,' ' ,D.dfiApellidoPaterno)) ELSE D.dfiRazonSocial END, (SELECT L.area FROM Tbarea L WHERE L.identidadfederativa='09' AND L.tbareaPK.idarea IN(SELECT DISTINCT B.bitaAreaEnvio FROM Bitacora B WHERE A.bitaNumero = B.bitaNumero AND B.bitaSituacion = 'I00008' ) ), (SELECT L.area FROM Tbarea L WHERE L.identidadfederativa='09' AND L.tbareaPK.idarea IN(SELECT DISTINCT B.bitaAreaRecibe FROM Bitacora B WHERE A.bitaNumero = B.bitaNumero AND B.bitaSituacion = 'I00008' ) ) FROM Bitacora A, Proyecto_Bitacora C, DatosFiscales D , Tbsituacion E  , Tramite F, Modalidad G, SectorProyecto H, SubsectorProyecto I WHERE  A.bitaNumero =:numBita AND A.bitaNumero = C.numeroBita AND A.bitaIdProm = D.datosFiscalesPK.promId AND D.datosFiscalesPK.idTipoDireccion = 03 AND A.bitaSituacion = E.idsituacion AND A.bitaIdTramite = F.idTramite AND A.bitaIdTramite = G.modalidadPK.idTramite AND C.sector = H.nsec AND C.subsector = I.nsub");
        List<Object[]> l = new ArrayList<>();
        try {
            Query q = emfBitacora.createNativeQuery("SELECT A.bita_Numero, A.bita_Id_Prom, D.dfi_Rfc, A.bita_Fofi_Recepcion, A.bita_Fechatur,	A.bita_Fecharec, A.bita_Fmax_Resolucion, F.descripcion,	G.nombre_Corto, E.descripcion as DESCRIPCION_1, C.cve,	C.nombre,	H.sector,	I.subsector,\n"
                    + "(SELECT J.rama FROM CATALOGOS.Rama_Proyecto J WHERE J.nrama = C.rama AND J.nsub = C.subsector),	(SELECT	K .tipo_Proyecto FROM CATALOGOS.Tipo_Proyecto K	WHERE	K .nrama = C.rama	AND K .ntipo = C.tipo_Proy),\n"
                    + "CASE WHEN (D .dfi_Razon_Social IS NULL) THEN (	D .dfi_Nombre || ' ' || D .dfi_Apellido_Materno || ' ' ||	D .dfi_Apellido_Paterno) ELSE	D .dfi_Razon_Social END,\n"
                    + "(SELECT L.area FROM SEGURIDAD.Tbarea L WHERE L.IDENTIDADFEDERATIVA = '09' AND L.idarea IN (SELECT DISTINCT B.histo_Area_Envio FROM	BITACORA.Historial B WHERE	A .bita_Numero = B.histo_Numero	AND B.HISTO_SITUACION_ACTUAL IN ('I00008', 'AT0027')\n"
                    + "AND B.histo_Area_Recibe <> '0'AND B.histo_Consecutiv = (SELECT MAX (B.histo_Consecutiv) FROM BITACORA.Historial B	WHERE	A .bita_Numero = B.histo_Numero	))),\n"
                    + "(SELECT L.area FROM SEGURIDAD.Tbarea L WHERE L.identidadfederativa = '09' AND L.idarea IN (SELECT DISTINCT B.histo_Area_Recibe FROM Bitacora.Historial B	WHERE	A .bita_Numero = B.histo_Numero	AND B.histo_Situacion_Actual IN ('I00008', 'AT0027')	\n"
                    + "AND B.histo_Area_Recibe <> '0')) FROM BITACORA.BITACORA A,	BITACORA_TEMATICA.PROYECTO C, PADRON.DATOS_FISCALES D, CATALOGO_FLUJOS.TBSITUACION E,	CATALOGO_TRAMITES.Tramite F,	CATALOGO_TRAMITES.Modalidad G,\n"
                    + "CATALOGOS.Sector_Proyecto H, CATALOGOS.Subsector_Proyecto I WHERE	A .bita_Numero = ?1 AND A .bita_Numero = C.numero_Bita AND A .bita_Id_Prom = D .prom_Id AND D .id_Tipo_Direccion = 03 AND A .bita_Situacion = E .idsituacion\n"
                    + "AND A .bita_Id_Tramite = F.id_Tramite AND A .bita_Id_Tramite = G.id_Tramite AND C.sector = H .nsec AND C.subsector = I.nsub");
            q.setParameter(1, bitacora);
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Todos los proyectos de los evaluadores">
    /**
     * Todos los proyectos de los evaluadores
     *
     * @param areaPadre
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Tbarea> evaluadoresTodos(String areaPadre) {
        List l = null;
        try {
            Query q = emfBitacora.createQuery("SELECT A FROM Tbarea A WHERE A.tbareaPK.iddependencia = 'UDEADM940001' AND A.iddependenciapadre ='UDEADM940001' AND A.idareapadre =:idAreaPadre AND A.jerarquia = 'JD' AND A.estatus = 1 ORDER BY A.siglas");
            q.setParameter("idAreaPadre", areaPadre);
            
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }//</editor-fold>

    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<Tbarea> evaluadoresTodos(Tbarea area, String areaPadre) {

        List l = new ArrayList();
        try {
            System.out.println("\nevaluadoresTodos...");
            
            Query q = null;

            // si es de oficina central ademas de los parametros.. filtra la jerarquia 'JD'
            if (area.getIdentidadfederativa().equals("09")) {
                
                q = emfBitacora.createQuery("SELECT A FROM Tbarea A WHERE A.tbareaPK.iddependencia = 'UDEADM940001' AND A.iddependenciapadre ='UDEADM940001' AND A.idareapadre =:idAreaPadre AND A.jerarquia = 'JD' AND A.estatus = 1 ORDER BY A.siglas");
                q.setParameter("idAreaPadre", areaPadre);
                System.out.println("idAreaPadre : " + areaPadre);
                
            } else {
                // si es de otra delegación no filtra la jerarquia.. solo los demás parámetros
                q = emfBitacora.createQuery("SELECT A FROM Tbarea A WHERE A.tbareaPK.iddependencia =:idDependencia AND A.iddependenciapadre =:idDependenciaPadre AND A.idareapadre =:idAreaPadre AND A.estatus = 1 ORDER BY A.siglas");
                
                String iddep = area.getCtdependencia().getIddependencia();
                String iddeppadre = area.getIddependenciapadre();
                String idareapadre = area.getIdareapadre();
                
                System.out.println("idDependencia : " + iddep);
                System.out.println("idDependenciaPadre : " + iddeppadre);
                System.out.println("idAreaPadre : " + idareapadre);
                
                q.setParameter("idDependencia", iddep);
                q.setParameter("idDependenciaPadre", iddeppadre);
                q.setParameter("idAreaPadre", idareapadre);
            }
            
            l = q.getResultList();
        } catch (Exception e) {
        } finally {
            emfBitacora.clear();
        }
        return l;
    }

    //<editor-fold defaultstate="collapsed" desc="Consulta los proyectos en subdireccion turnados">
    /**
     * Consulta los proyectos en subdireccion turnados
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enRecepSubDirecTurnados(Tbarea tbarea) {
        
        String area =tbarea.getTbareaPK().getIdarea().trim();
        String dependencia = tbarea.getIddependenciapadre().trim();

        String sql = "SELECT * FROM DGIRA_MIAE2.VW_SUBDIRECTOR_TURNADOS WHERE BITA_TIPO_INGRESO NOT IN 'A' "
                + " AND BITA_AREA_RECIBE ='" + area + "' AND BITA_IDDEPENDENCIA_RECIBE ='" + dependencia + "' "
                + " ORDER BY BITA_FECH_REGISTRO DESC"; // Esta consulta referencia a una vista con 
        //los datos necesarios para llenar la lista 
        //ademas de tener 3 campos extras uno para el 
        //area que recibe, otro para la dependencia que 
        //recibe  y un tercero para la distincion de 
        //tramites fisicos, web y de otra dependencia
        Query q = emfBitacora.createNativeQuery(sql);

        List<Object[]> l;
        try {
            l = q.getResultList();
        } catch (Exception e) {
            l = new ArrayList<Object[]>();
        }

        return l;
    }

    //<editor-fold defaultstate="collapsed" desc="Consulta para el Wizard">
    /**
     * Consulta para el Wizard
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> enWizar() {
        Query q = emfDgira.createNativeQuery("select C.CRITERIO_ID, C.CRITERIO_DESCRIPCION, B.DESCRIPCION_VALOR, C.CRITERIO_IDESTATUS from DGIRA_MIAE2.CAT_DERECHOS_CRITERIOS C , DGIRA_MIAE2.CAT_DERECHOS_CRITERIOSVALORES B where B.ID_CRITERIO= C.CRITERIO_ID Order by C.CRITERIO_ORDEN");
        List<Object[]> l = q.getResultList();
        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Todos los criterios valores disponibles">
    /**
     * Todos los criterios valores disponibles
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> obtenerCriterios() {
        Query q = emfDgira.createNativeQuery(
                "select B.ID_CRITERIO, B.ID_VALOR, C.CRITERIO_DESCRIPCION  "
                + "from DGIRA_MIAE2.CAT_DERECHOS_CRITERIOS C, DGIRA_MIAE2.CAT_DERECHOS_CRITERIOSVALORES B  "
                + "where B.ID_CRITERIO = C.CRITERIO_ID and B.ID_VALOR = 1 Order by C.CRITERIO_ORDEN "
        );
        List<Object[]> l = q.getResultList();
        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta a CatDerechosCriteriosValores">
    /**
     * Consulta a CatDerechosCriteriosValores
     *
     * @param idCriterio
     * @param descValor
     * @return
     */
    public Integer valor(Integer idCriterio, String descValor) {
        Query q = emfDgira.createQuery("select c.valor from  CatDerechosCriteriosvalores c WHERE c.descripcionValor =:b AND c.catDerechosCriteriosvaloresPK.idCriterio =:a ");
        q.setParameter("b", descValor);
        q.setParameter("a", idCriterio);
        BigInteger valor = (BigInteger) q.getSingleResult();

        return valor.intValue();

    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de CatDerechosCriteriorValores">
    /**
     * Consulta de CatDerechosCriteriorValores
     *
     * @param a
     * @param b
     * @return
     */
    public String descValor(Integer a, String b) {
        Query q = emfDgira.createQuery("select a.referenciaDoc from CatDerechosCriteriosvalores a WHERE a.descripcionValor=:b AND a.catDerechosCriteriosvaloresPK.idCriterio =:a");
        q.setParameter("b", b);
        q.setParameter("a", a);
        String desc = q.getSingleResult().toString();
        return desc;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de CatDerechos">
    /**
     *
     * @param valor
     * @return
     */
    public List<Object[]> tablab(String valor) {
        //Query q = emfBitacora.createNativeQuery("select D.ID_NIVEL, D.MONTO, D.LIMITE_INFERIOR, D.LIMITE_SUPERIOR from CAT_DERECHOS D where D.ID_ESTUDIO =:VALOR");
        Query q = emfDgira.createQuery("select a.catDerechosPK.idNivel, a.monto,a.limiteInferior, a.limiteSuperior from CatDerechos a where a.catDerechosPK.idEstudio =:valor");
        q.setParameter("valor", valor);
        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Consulta de tipo de tramite">
    /**
     *
     * @param bitacora
     * @return
     */
    public String idTramite(String bitacora) {
        Query q = emfBitacora.createQuery("SELECT c.bitaTipotram FROM Bitacora c WHERE c.bitaNumero='" + bitacora + "'");
        String tramite = q.getSingleResult().toString();

        return tramite;
    }//</editor-fold>

    //<editor-fold desc="Calculo del monto del proyecto" defaultstate="collapsed">
    /**
     * Consulta del monto
     *
     * @param bitacora
     * @return
     */
    public BigDecimal monto(String bitacora) {
        Query q = emfBitacora.createQuery(" SELECT c.bitaMonto FROM Bitacora c  WHERE c.bitaNumero='" + bitacora + "'");
        BigDecimal num = (BigDecimal) q.getSingleResult();
        return num;
    }//</editor-fold>

    /**
     * Obtiene el monto de la vista VEXDATOSTRAMITE
     *
     * @param folio
     * @return
     */
    @SuppressWarnings("unchecked")
    public BigDecimal montoVEX(String bitacora) {
        BigDecimal monto, folio;

        Query q = emfBitacora.createNamedQuery("SinatSinatec.findByBitacora");
        q.setParameter("bitacora", bitacora);
        List<SinatSinatec> lstRes = q.getResultList();
        if (!lstRes.isEmpty()) {
            folio = lstRes.get(0).getFolio();
        } else {
            return null;
        }

        q = emfBitacora.createNamedQuery("Vexdatostramite.findByBgtramiteidOrderByFecha");
        q.setParameter("bgtramiteid", folio.intValue());
        List<Vexdatostramite> lstTramite = q.getResultList();
        Vexdatostramite tramite;
        if (!lstTramite.isEmpty()) {
            tramite = (Vexdatostramite) lstTramite.get(0);
        } else {
            return null;
        }

        BigInteger biMonto = tramite.getMontoConredondeo();
        monto = new BigDecimal(biMonto);

        return monto;

    }//</editor-fold>

    public boolean reaperturaPagoDerechos(String bitacora) {
        boolean bRes = false;

        String qry
                = "SELECT   "
                + "    COUNT(*)  "
                + "FROM BITACORA.BITACORA B, BITACORA.HISTORIAL H  "
                + "WHERE B.BITA_NUMERO = H.HISTO_NUMERO "
                + "  AND B.BITA_SITUACION in('I00008', 'AT0027') "
                + "  AND H.HISTO_SITUACION_ACTUAL = '200501' "
                + "  AND B.BITA_NUMERO = :bitacora ";

        Query q = emfBitacora.createNativeQuery(qry);
        q.setParameter("bitacora", bitacora);

        try {
            BigDecimal res = (BigDecimal) q.getSingleResult();
            if (res.intValue() > 0) {
                bRes = true;
            }

        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la consulta " + e.getMessage());
            e.printStackTrace();
        }

        return bRes;
    }

    //<editor-fold desc="Detalle de capitulos" defaultstate="collapsed">
    /**
     *
     * @param numBita
     * @return
     */
    public List<Object[]> detalleProyCap(String numBita) {
        Query q = emfBitacora.createQuery(" SELECT A.bitaIdProm FROM Bitacora A WHERE A.bitaNumero=:bitaNum");
        q.setParameter("bitaNum", numBita);
        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Datos de la bitacora">
    /**
     *
     * @param numBita
     * @return
     */
    public Bitacora datosBitaInd(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a FROM Bitacora a WHERE a.bitaNumero =:bitacora ");
        q.setParameter("bitacora", numBita);

        try {
            return (Bitacora) q.getSingleResult();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return new Bitacora();
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="datos del proyecto por bitacora">
    /**
     *
     * @param numBita
     * @return
     */
    public Proyecto datosProyInd(String numBita) {
        Query q = emfBitacora.createQuery("SELECT a FROM Proyecto_DGIRA a WHERE a.numeroBita=:bitacora ");
        q.setParameter("bitacora", numBita);
        try {
            return (Proyecto) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
        }
        return new Proyecto();
    }//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="consulta a tabal SINAT_SINATEC">
    /**
     * consulta a tabal SINAT_SINATEC
     *
     * @param numBita
     * @return
     */
    public SinatSinatec sinatSintec(String numBita) {

        Query q = emfBitacora.createQuery("SELECT p FROM SinatSinatec p WHERE p.bitacora =:bitaNum");
        q.setParameter("bitaNum", numBita);
        Object o = null;

        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
        }
        return (SinatSinatec) o;
    }//</editor-fold>

    public EvaluacionProyecto datosEvaProy(String numBita) {
        Query q = emfDgira.createQuery("SELECT a FROM EvaluacionProyecto a WHERE a.bitacoraProyecto=:bitacora");
        q.setParameter("bitacora", numBita);

        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (EvaluacionProyecto) o;
    }

    public List<Object[]> detalleSitTram(String bitacora, String situacion) {
        Query q = emfBitacora.createQuery("SELECT A.bitaNumero , A.bitaIdProm, A.bitaSituacion FROM Bitacora A WHERE  A.bitaNumero =:numBita and A.bitaSituacion=:situac");
        q.setParameter("numBita", bitacora);
        q.setParameter("situac", situacion);
        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> consultaPublica(String numBita) {
        Query q = emfBitacora.createNativeQuery(
                "SELECT B.BITACORAPROYECTO, C.DESCRIPCION, A.DEPENDENCIAREMITENTE, A.NOMBREREMITENTE, A.IDDOCUMENTO, A.IDPRIORIDAD,  "
                + "	   A.IDINSTRUCCION, A.IDUSUARIOREGISTRO, A.IDAREA, A.FECHAREGISTRO, A.OBSERVACIONES, A.FECHAACUSE, A.NUMREFERENCIA,  "
                + "	   A.FECHAELABORACION, A.FECHALIMITE, A.IDUSUARIORESPONSABLE, A.IDENTIDADFEDERATIVA, A.IDESTADO, A.BORRADOR,  "
                + "	   A.IDCATEGORIA, A.INSTRUCCIONADICIONAL, A.TELEFONOREMITENTE, A.NOMBREDIRIGIDOA, A.AREADIRIGIDOA,  "
                + "	   A.DOCUMENTORESPUESTA, A.NUMEXP_ZOFE, A.NUMOFICIALIA_ZOFE, A.IDCATCORRESP, A.PUESTOREMITENTE "
                + " FROM CORRESPONDENCIA.TB_DOCUMENTO A "
                + " INNER JOIN CORRESPONDENCIA.TB_RELDOCTRAMITE B ON (A.IDDOCUMENTO = B.IDDOCUMENTO) "
                + " INNER JOIN CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO C ON (A.IDCATEGORIA = C.IDCATEGORIA) "
                + " WHERE C.IDCATEGORIA IN (SELECT IDCATEGORIA FROM CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO "
                + " WHERE IDTIPODOCUMENTO IN (SELECT IDTIPODOCUMENTO "
                + " from CORRESPONDENCIA.CAT_TIPODOCUMENTO "
                + " WHERE IDENTIDADFEDERATIVA = '09' "
                + "  AND IDAREA = '02000000' "
                + "  AND ESTATUS = 1) "
                + "  AND ESTATUS = 1 "
                + "  AND DESCRIPCION LIKE '%SOLICITUD DE CONSULTA PUBLICA%') "
                + "  AND (B.BITACORAPROYECTO = '" + numBita + "' OR B.BITACORAPROYECTO =  "
                + "        (SELECT CVE FROM BITACORA_TEMATICA.PROYECTO WHERE NUMERO_BITA = '" + numBita + "')) "
                + " ORDER BY A.FECHAREGISTRO ASC");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    /**
     * Permite obtener la lista de proyectos de la consulta publica a partir del
     * num de bitacora y la entidad federativa del usuario.
     *
     * @param numBita
     * @param idEntidad
     * @return la lista de proyectos.
     */
    public List<Object[]> consultaPublica(String numBita, String idEntidad) {
        Query q = emfBitacora.createNativeQuery(
                "SELECT B.BITACORAPROYECTO, C.DESCRIPCION, A.DEPENDENCIAREMITENTE, A.NOMBREREMITENTE, A.IDDOCUMENTO, A.IDPRIORIDAD,  "
                + "	   A.IDINSTRUCCION, A.IDUSUARIOREGISTRO, A.IDAREA, A.FECHAREGISTRO, A.OBSERVACIONES, A.FECHAACUSE, A.NUMREFERENCIA,  "
                + "	   A.FECHAELABORACION, A.FECHALIMITE, A.IDUSUARIORESPONSABLE, A.IDENTIDADFEDERATIVA, A.IDESTADO, A.BORRADOR,  "
                + "	   A.IDCATEGORIA, A.INSTRUCCIONADICIONAL, A.TELEFONOREMITENTE, A.NOMBREDIRIGIDOA, A.AREADIRIGIDOA,  "
                + "	   A.DOCUMENTORESPUESTA, A.NUMEXP_ZOFE, A.NUMOFICIALIA_ZOFE, A.IDCATCORRESP, A.PUESTOREMITENTE "
                + " FROM CORRESPONDENCIA.TB_DOCUMENTO A "
                + " INNER JOIN CORRESPONDENCIA.TB_RELDOCTRAMITE B ON (A.IDDOCUMENTO = B.IDDOCUMENTO) "
                + " INNER JOIN CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO C ON (A.IDCATEGORIA = C.IDCATEGORIA) "
                + " WHERE C.IDCATEGORIA IN (SELECT IDCATEGORIA FROM CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO "
                + " WHERE IDTIPODOCUMENTO IN (SELECT IDTIPODOCUMENTO "
                + " from CORRESPONDENCIA.CAT_TIPODOCUMENTO "
                + " WHERE IDENTIDADFEDERATIVA = '" + idEntidad + "' "
                + "  AND IDAREA = '02000000' "
                + "  AND ESTATUS = 1) "
                + "  AND ESTATUS = 1 "
                + "  AND DESCRIPCION LIKE '%SOLICITUD DE CONSULTA PUBLICA%') "
                + "  AND (B.BITACORAPROYECTO = '" + numBita + "' OR B.BITACORAPROYECTO =  "
                + "        (SELECT CVE FROM BITACORA_TEMATICA.PROYECTO WHERE NUMERO_BITA = '" + numBita + "')) "
                + " ORDER BY A.FECHAREGISTRO ASC");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    //DatosFiscales
    public List<Object[]> nombrePromovente(String idPromovente) {
        Query q = emfBitacora.createNativeQuery("SELECT  CASE WHEN (DFI_NOMBRE IS NOT NULL) THEN DFI_NOMBRE ||' ' || DFI_APELLIDO_PATERNO ||' ' || DFI_APELLIDO_MATERNO ELSE A.DFI_RAZON_SOCIAL END AS nombreprom  FROM PADRON.DATOS_FISCALES A  "
                + "WHERE A.PROM_ID = '" + idPromovente + "' AND A.ID_TIPO_DIRECCION = '03' ");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    //oficios de opinion tecnica
    public List<String[]> oficOpTec(String numBita, String cve) {
        Query q = emfBitacora.createNativeQuery(
                " SELECT A.IDDOCUMENTO || ' - ' || A.DEPENDENCIAREMITENTE "
                + //                "          B.BITACORAPROYECTO, C.DESCRIPCION, A.DEPENDENCIAREMITENTE, A.NOMBREREMITENTE, A.IDDOCUMENTO, A.IDPRIORIDAD,  " +
                //                "	   A.IDINSTRUCCION, A.IDUSUARIOREGISTRO, A.IDAREA, A.FECHAREGISTRO, A.OBSERVACIONES, A.FECHAACUSE, A.NUMREFERENCIA,  " +
                //                "	   A.FECHAELABORACION, A.FECHALIMITE, A.IDUSUARIORESPONSABLE, A.IDENTIDADFEDERATIVA, A.IDESTADO, A.BORRADOR,  " +
                //                "	   A.IDCATEGORIA, A.INSTRUCCIONADICIONAL, A.TELEFONOREMITENTE, A.NOMBREDIRIGIDOA, A.AREADIRIGIDOA,  " +
                //                "	   A.DOCUMENTORESPUESTA, A.NUMEXP_ZOFE, A.NUMOFICIALIA_ZOFE, A.IDCATCORRESP, A.PUESTOREMITENTE " +
                " FROM CORRESPONDENCIA.TB_DOCUMENTO A "
                + " INNER JOIN CORRESPONDENCIA.TB_RELDOCTRAMITE B ON (A.IDDOCUMENTO = B.IDDOCUMENTO) "
                + " INNER JOIN CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO C ON (A.IDCATEGORIA = C.IDCATEGORIA) "
                + " WHERE C.IDCATEGORIA IN (SELECT IDCATEGORIA FROM CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO "
                + " WHERE IDTIPODOCUMENTO IN (SELECT IDTIPODOCUMENTO "
                + " from CORRESPONDENCIA.CAT_TIPODOCUMENTO "
                + " WHERE IDENTIDADFEDERATIVA = '09' "
                + "  AND IDAREA = '02000000' "
                + "  AND ESTATUS = 1) "
                + "  AND ESTATUS = 1 "
                + "  AND DESCRIPCION LIKE '%OPINION TECNICA SOLICITADA%') "
                + "  AND (B.BITACORAPROYECTO = '" + numBita + "' OR B.BITACORAPROYECTO =  "
                + "        (SELECT CVE FROM BITACORA_TEMATICA.PROYECTO WHERE NUMERO_BITA = '" + cve + "')) "
                + " ORDER BY A.FECHAREGISTRO ASC");
        @SuppressWarnings("unchecked")
        List<String[]> l = q.getResultList();
        return l;
    }

    /**
     * Permite consulta la lista de oficios de opiniones tecnicas con respecto a
     * la entidad del area en la que se encuentren.
     *
     * @param numBita
     * @param cve
     * @param tbarea
     * @return la lista de oficios.
     */
    public List<String[]> oficOpTec(String numBita, String cve, Tbarea tbarea) {
        Query q = emfBitacora.createNativeQuery(
                " SELECT A.IDDOCUMENTO || ' - ' || A.DEPENDENCIAREMITENTE "
                + //                "          B.BITACORAPROYECTO, C.DESCRIPCION, A.DEPENDENCIAREMITENTE, A.NOMBREREMITENTE, A.IDDOCUMENTO, A.IDPRIORIDAD,  " +
                //                "	   A.IDINSTRUCCION, A.IDUSUARIOREGISTRO, A.IDAREA, A.FECHAREGISTRO, A.OBSERVACIONES, A.FECHAACUSE, A.NUMREFERENCIA,  " +
                //                "	   A.FECHAELABORACION, A.FECHALIMITE, A.IDUSUARIORESPONSABLE, A.IDENTIDADFEDERATIVA, A.IDESTADO, A.BORRADOR,  " +
                //                "	   A.IDCATEGORIA, A.INSTRUCCIONADICIONAL, A.TELEFONOREMITENTE, A.NOMBREDIRIGIDOA, A.AREADIRIGIDOA,  " +
                //                "	   A.DOCUMENTORESPUESTA, A.NUMEXP_ZOFE, A.NUMOFICIALIA_ZOFE, A.IDCATCORRESP, A.PUESTOREMITENTE " +
                " FROM CORRESPONDENCIA.TB_DOCUMENTO A "
                + " INNER JOIN CORRESPONDENCIA.TB_RELDOCTRAMITE B ON (A.IDDOCUMENTO = B.IDDOCUMENTO) "
                + " INNER JOIN CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO C ON (A.IDCATEGORIA = C.IDCATEGORIA) "
                + " WHERE C.IDCATEGORIA IN (SELECT IDCATEGORIA FROM CORRESPONDENCIA.CAT_CATEGORIADOCUMENTO "
                + " WHERE IDTIPODOCUMENTO IN (SELECT IDTIPODOCUMENTO "
                + " from CORRESPONDENCIA.CAT_TIPODOCUMENTO "
                + " WHERE IDENTIDADFEDERATIVA = '" + tbarea.getIdentidadfederativa() + "' "
                + "  AND IDAREA = '02000000' "
                + "  AND ESTATUS = 1) "
                + "  AND ESTATUS = 1 "
                + "  AND DESCRIPCION LIKE '%OPINION TECNICA SOLICITADA%') "
                + "  AND (B.BITACORAPROYECTO = '" + numBita + "' OR B.BITACORAPROYECTO =  "
                + "        (SELECT CVE FROM BITACORA_TEMATICA.PROYECTO WHERE NUMERO_BITA = '" + numBita + "')) "
                + " ORDER BY A.FECHAREGISTRO ASC");
        @SuppressWarnings("unchecked")
        List<String[]> l = q.getResultList();
        return l;
    }

    public String cveTramite(String bitacora) {
        String cve = "";
        try {
            Query q = emfBitacora.createQuery("SELECT v.cve FROM Proyecto_Bitacora v where v.numeroBita=:bitacora ");
            q.setParameter("bitacora", bitacora);
            cve = (String) q.getSingleResult();
        } catch (Exception e) {
        }
        return cve;
    }
    //

    public List<Object[]> fechaDoc(String idDoc) {
        Query q = emfBitacora.createNativeQuery("SELECT  A.FECHAREGISTRO FROM CORRESPONDENCIA.TB_DOCUMENTO A   WHERE A.IDDOCUMENTO LIKE '%" + idDoc + "%'");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    //id de Tramite
    public Integer idTipTram(String bitacora) {
        Integer TramReq = 0;
        try {
            Query q = emfBitacora.createQuery("SELECT v.bitaIdTramite FROM Bitacora v WHERE v.bitaNumero=:bitacora");
            q.setParameter("bitacora", bitacora);
            TramReq = (Integer) q.getSingleResult();//(Integer)q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error cveTipTram: " + e.getMessage());
        }
        return TramReq;
    }

    //clave de Tramite
    public String cveTipTram(String bitacora) {
        String TramReq = null;
        try {
            Query q = emfBitacora.createQuery("SELECT v.bitaTipotram FROM Bitacora v WHERE v.bitaNumero=:bitacora");
            q.setParameter("bitacora", bitacora);
            TramReq = (String) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error cveTipTram: " + e);
        }
        return TramReq;
    }

    //entidad de gestion del Tramite
    public String entGestTram(String bitacora) {
        String TramReq = null;
        try {
            Query q = emfBitacora.createQuery("SELECT v.bitaEntidadGestion FROM Bitacora v WHERE v.bitaNumero=:bitacora");
            q.setParameter("bitacora", bitacora);
            TramReq = (String) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error cveTipTram: " + e);
        }
        return TramReq;
    }

    //estus  del Tramite en tabla historial
    public String estatusTramHist(String bitacora) {
        String TramReq = null;
        try {
            Query q = emfBitacora.createQuery("SELECT v.histoSituacionActual FROM Historial v WHERE v.historialPK.histoNumero=:bitacora");
            q.setParameter("bitacora", bitacora);
            TramReq = (String) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error situación actual del historial: " + e);
        }
        return TramReq;
    }

    //estus  del Tramite en tabla bitacora
    public String estatusTramBita(String bitacora) {
        String TramReq = null;
        try {
            Query q = emfBitacora.createQuery("SELECT v.bitaSituacion FROM Bitacora v WHERE v.bitaNumero=:bitacora");
            q.setParameter("bitacora", bitacora);
            TramReq = (String) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error situación actual del historial: " + e);
        }
        return TramReq;
    }

    public Tbarea datosDirecGral() {
        Query q = emfBitacora.createQuery("SELECT a FROM Tbarea a WHERE a.tbareaPK.iddependencia = 'UDEADM940001' and a.identidadfederativa = '09' AND a.tbareaPK.idarea = '02000000' and a.jerarquia='DG' ");
        //List l = q.getResultList();
        try {
            return (Tbarea) q.getSingleResult();
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null,  "error RRRR:  " + e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
        }
        return new Tbarea();
    }

//    @SuppressWarnings("unchecked")
//	public List<Object[]> jerarquiaTbArea(String area) {
//        Query q = emfBitacora.createNativeQuery("select A.JERARQUIA from SEGURIDAD.TBAREA A WHERE A.IDDEPENDENCIA = 'UDEADM940001' AND A.IDENTIDADFEDERATIVA = '09' AND A.IDAREA = '" + area + "'");
//
//        List<Object[]> l = q.getResultList();
//        return l; 
//    }
    @SuppressWarnings("unchecked")
    public List<Object[]> jerarquiaTbArea(String area, String identidad) {
        // Query q = emfBitacora.createNativeQuery("select A.JERARQUIA from SEGURIDAD.TBAREA A WHERE A.IDENTIDADFEDERATIVA = '" + identidad + "' AND A.IDAREA = '" + area + "'");
        Query q = emfBitacora.createNativeQuery("SELECT DISTINCT U.JERARQUIA "
                + " FROM SEGURIDAD.TBAREA U "
                + " INNER JOIN SEGURIDAD.CTDEPENDENCIA O ON O.IDDEPENDENCIA=U.IDDEPENDENCIA "
                + " WHERE 1=1 "
                + " AND O.IDDEPENDENCIA IN ('SAMICO990009','SAMICO990010','SAMICO990011','SAMICO990012','SAMICO990015','SAMICO990016','SAMICO990013', "
                + " 						  'SAMICO990014','SAMICO990017','SAMICO990018','SAMICO990019','SAMICO990020','SAMICO990021','SAMICO990022', "
                + "                         'SAMICO990023','SAMICO990024','SAMICO990025','SAMICO990026','SAMICO990027','SAMICO990028','SAMICO990029', "
                + "                         'SAMICO990030','SAMICO990123','SAMICO990031','SAMICO990032','SAMICO990033','SAMICO990034','SAMICO990035', "
                + "                         'SAMICO990036','SAMICO990037','SAMICO990038','MEDAMB000001','UDEADM940001') "
                + " AND IDENTIDADFEDERATIVA IS NOT NULL AND U.JERARQUIA IS NOT NULL "
                + " AND IDENTIDADFEDERATIVA = '" + identidad + "' "
                + " AND IDAREA = '" + area + "'");

        List<Object[]> l = q.getResultList();
        return l;
    }

    public String getPerfilUsuario(Integer idUsuario) {
        String perfil = "";
        Tbusuario usr;
        Query q = emfBitacora.createNamedQuery("Tbusuario.findByIdusuario");
        q.setParameter("idusuario", idUsuario);

        @SuppressWarnings("unchecked")
        List<Tbusuario> lstUsr = q.getResultList();
        if (!lstUsr.isEmpty()) {
            usr = lstUsr.get(0);

            Query qJ = emfBitacora.createQuery("SELECT t FROM Tbarea t "
                    + "WHERE t.tbareaPK.idarea = :idArea AND t.tbareaPK.iddependencia = :idDependencia");
            qJ.setParameter("idArea", usr.getIdarea());
            qJ.setParameter("idDependencia", usr.getIddependencia());

            @SuppressWarnings("unchecked")
            List<Tbarea> lstRes = qJ.getResultList();

            if (!lstRes.isEmpty()) {
                Tbarea area = lstRes.get(0);
                perfil = area.getJerarquia();
            }
        }

        return perfil;
    }

    public Integer diasNotificacion(String numBita, Integer idTipoOficio, boolean isProceso) {
        Integer iRes = null;
        String qry = "SELECT " + (isProceso ? "dias_proceso_notificado" : "dias_notificado")
                + " FROM sinat_usdb.sinat_oficio_notificacion WHERE bita_numero = '" + numBita + "' AND id_tipo_oficio = " + idTipoOficio.toString();
        System.out.println("Sentencia: " + qry);
        Query q = emfBitacora.createNativeQuery(qry);

        try {
            BigDecimal res = (BigDecimal) q.getSingleResult();
            iRes = res.intValue();
        } catch (NoResultException e) {
            System.out.println("ERROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
        }
        return iRes;
    }

    public boolean existeProyectoEnNotificacion(String numBita) {
        boolean bRes = false;

        String qry = "SELECT COUNT(*) FROM SINAT_USDB.SINAT_OFICIO_NOTIFICACION where bita_numero = '" + numBita + "'";

        Query q = emfBitacora.createNativeQuery(qry);

        try {
            BigDecimal res = (BigDecimal) q.getSingleResult();
            if (res.intValue() > 0) {
                bRes = true;
            }

        } catch (Exception e) {
            System.out.println("Ocurrio un error durante la consulta " + e.getMessage());
            e.printStackTrace();
        }

        return bRes;
    }

    public List<Object[]> tablabOsevOfic(String bita, short tipoofic, String idDoc, String idEntidad) { //traer observaciones de los oficios
        //Query q = emfBitacora.createNativeQuery("select D.ID_NIVEL, D.MONTO, D.LIMITE_INFERIOR, D.LIMITE_SUPERIOR from CAT_DERECHOS D where D.ID_ESTUDIO =:VALOR");
        Query q = emfBitacora.createNativeQuery("SELECT A.BITACORA_PROYECTO, A.TIPO_DOC_ID,B.SIGLAS, C.ESTATUS_DOCUMENTO_DESCRIPCION, A.DOCUMENTO_DGIRA_OBSERV_HIST, A.DOCUMENTO_DGIRA_FE_ENV_HIST "
                + "FROM DGIRA_MIAE2.DOCUMENTO_DGIRA_FLUJO_HIST A, SEGURIDAD.TBAREA B, DGIRA_MIAE2.CAT_ESTATUS_DOCUMENTO C "
                + "WHERE A.BITACORA_PROYECTO = '" + bita + "' AND A.ID_AREA_ENVIO_HIST = B.IDAREA AND A.TIPO_DOC_ID =  " + tipoofic + " AND A.DOCUMENTO_ID = '" + idDoc + "' "
                + //      "AND B.IDDEPENDENCIA = 'UDEADM940001' AND B.IDENTIDADFEDERATIVA = '" + idEntidad + "' AND A.ESTATUS_DOCUMENTO_ID_HIST = C.ESTATUS_DOCUMENTO_ID " +
                "AND B.IDENTIDADFEDERATIVA = '" + idEntidad + "' AND A.ESTATUS_DOCUMENTO_ID_HIST = C.ESTATUS_DOCUMENTO_ID "
                + "ORDER BY A.DOCUMENTO_DGIRA_ID_HIST "); //A.DOCUMENTO_DGIRA_ID_HIST

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> obtenerObservacionesOficios(String bita, short tipoofic, String idDoc) {
        Query q = emfBitacora.createNativeQuery("SELECT A.BITACORA_PROYECTO, A.TIPO_DOC_ID,B.SIGLAS, C.ESTATUS_DOCUMENTO_DESCRIPCION, A.DOCUMENTO_DGIRA_OBSERV_HIST, A.DOCUMENTO_DGIRA_FE_ENV_HIST "
                + "FROM DGIRA_MIAE2.DOCUMENTO_DGIRA_FLUJO_HIST A, SEGURIDAD.TBAREA B, DGIRA_MIAE2.CAT_ESTATUS_DOCUMENTO C "
                + "WHERE A.BITACORA_PROYECTO = '" + bita + "' AND A.ID_AREA_ENVIO_HIST = B.IDAREA AND A.TIPO_DOC_ID =  " + tipoofic + " AND A.DOCUMENTO_ID = '" + idDoc + "'"
                + "AND B.IDDEPENDENCIA = 'UDEADM940001' AND B.IDENTIDADFEDERATIVA = '09' AND A.ESTATUS_DOCUMENTO_ID_HIST = C.ESTATUS_DOCUMENTO_ID "
                + "ORDER BY A.DOCUMENTO_DGIRA_FE_ENV_HIST DESC ");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    /**
     * Metodo sobrecargado obtenerObservacionesOficios que obtiene el
     * identificador de la entidad de manera dinamica
     *
     * @param bita
     * @param tipoofic
     * @param idDoc
     * @param idEntidad
     * @return
     */
    public List<Object[]> obtenerObservacionesOficios(String bita, short tipoofic, String idDoc, String idEntidad) {
        Query q = emfBitacora.createNativeQuery("SELECT A.BITACORA_PROYECTO, A.TIPO_DOC_ID,B.SIGLAS, C.ESTATUS_DOCUMENTO_DESCRIPCION, A.DOCUMENTO_DGIRA_OBSERV_HIST, A.DOCUMENTO_DGIRA_FE_ENV_HIST "
                + "FROM DGIRA_MIAE2.DOCUMENTO_DGIRA_FLUJO_HIST A, SEGURIDAD.TBAREA B, DGIRA_MIAE2.CAT_ESTATUS_DOCUMENTO C "
                + "WHERE A.BITACORA_PROYECTO = '" + bita + "' AND A.ID_AREA_ENVIO_HIST = B.IDAREA AND A.TIPO_DOC_ID =  " + tipoofic + " AND A.DOCUMENTO_ID = '" + idDoc + "'"
                + "AND B.IDDEPENDENCIA = 'UDEADM940001' AND B.IDENTIDADFEDERATIVA = '" + idEntidad + "' AND A.ESTATUS_DOCUMENTO_ID_HIST = C.ESTATUS_DOCUMENTO_ID "
                + "ORDER BY A.DOCUMENTO_DGIRA_FE_ENV_HIST DESC ");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<Object[]> obtenerObservacionesOficiosTodos(String bita, short tipoofic) {
        Query q = emfBitacora.createNativeQuery("SELECT A.BITACORA_PROYECTO, A.TIPO_DOC_ID,B.SIGLAS, C.ESTATUS_DOCUMENTO_DESCRIPCION, A.DOCUMENTO_DGIRA_OBSERV_HIST, A.DOCUMENTO_DGIRA_FE_ENV_HIST, A.DOCUMENTO_ID "
                + "FROM DGIRA_MIAE2.DOCUMENTO_DGIRA_FLUJO_HIST A, SEGURIDAD.TBAREA B, DGIRA_MIAE2.CAT_ESTATUS_DOCUMENTO C "
                + "WHERE A.BITACORA_PROYECTO = '" + bita + "' AND A.ID_AREA_ENVIO_HIST = B.IDAREA AND A.TIPO_DOC_ID =  " + tipoofic
                + "AND B.IDDEPENDENCIA = 'UDEADM940001' AND B.IDENTIDADFEDERATIVA = '09' AND A.ESTATUS_DOCUMENTO_ID_HIST = C.ESTATUS_DOCUMENTO_ID "
                + "ORDER BY A.DOCUMENTO_DGIRA_FE_ENV_HIST DESC ");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    /**
     * Metodo sobrecargado obtenerObservacionesOficiosTodos que obtiene el
     * identificador de la entidad de manera dinamica
     *
     * @param bita
     * @param tipoofic
     * @param idEntidad
     * @return List<Object[]> l
     */
    public List<Object[]> obtenerObservacionesOficiosTodos(String bita, short tipoofic, String idEntidad) {
        Query q = emfBitacora.createNativeQuery("SELECT A.BITACORA_PROYECTO, A.TIPO_DOC_ID,B.SIGLAS, C.ESTATUS_DOCUMENTO_DESCRIPCION, A.DOCUMENTO_DGIRA_OBSERV_HIST, A.DOCUMENTO_DGIRA_FE_ENV_HIST, A.DOCUMENTO_ID "
                + "FROM DGIRA_MIAE2.DOCUMENTO_DGIRA_FLUJO_HIST A, SEGURIDAD.TBAREA B, DGIRA_MIAE2.CAT_ESTATUS_DOCUMENTO C "
                + "WHERE A.BITACORA_PROYECTO = '" + bita + "' AND A.ID_AREA_ENVIO_HIST = B.IDAREA AND A.TIPO_DOC_ID =  " + tipoofic
                + "AND B.IDDEPENDENCIA = 'UDEADM940001' AND B.IDENTIDADFEDERATIVA = '" + idEntidad + "' AND A.ESTATUS_DOCUMENTO_ID_HIST = C.ESTATUS_DOCUMENTO_ID "
                + "ORDER BY A.DOCUMENTO_DGIRA_FE_ENV_HIST DESC ");

        @SuppressWarnings("unchecked")
        List<Object[]> l = q.getResultList();
        return l;
    }

    public boolean isSituacionTramiteHist(String bitacora, String situacion) {
        boolean bRes = false;
        Query q = emfBitacora.createNativeQuery("select count(*) from bitacora.historial H where H.histo_numero = :bitacora AND H.HISTO_SITUACION_ACTUAL = :situacion ");
        q.setParameter("bitacora", bitacora);
        q.setParameter("situacion", situacion);

        try {

            BigDecimal res = (BigDecimal) q.getSingleResult();
            if (res.intValue() > 0) {
                bRes = true;
            }

        } catch (Exception e) {
            System.out.println("Ocurrio un error durante la consulta " + e.getMessage());
            e.printStackTrace();
        }

        return bRes;
    }

    public List<Tbempleado> getListaEmpleados() {
        Query q = emfBitacora.createNamedQuery("Tbempleado.findAll");
        @SuppressWarnings("unchecked")
        List<Tbempleado> lstEmp = q.getResultList();
        return lstEmp;
    }

    public List<Tbempleado> getListaEmpleadosBusqueda(String nombre, String apellidopaterno, String apellidomaterno) {

        Query q = emfBitacora.createQuery("SELECT t FROM Tbempleado t WHERE t.nombre LIKE :nombre and t.apellidopaterno LIKE :apellidopaterno and t.apellidomaterno LIKE :apellidomaterno");
        q.setParameter("nombre", nombre);
        q.setParameter("apellidopaterno", apellidopaterno);
        q.setParameter("apellidomaterno", apellidomaterno);
        @SuppressWarnings("unchecked")
        List<Tbempleado> lstEmp = q.getResultList();
        return lstEmp;
    }

    public Tbusuario getUsuarioPorIdEmpleado(String idempleado) {

        Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t WHERE t.idempleado.idempleado = :idempleado");
        q.setParameter("idempleado", idempleado);
        Tbusuario usr = (Tbusuario) q.getSingleResult();
        return usr;
    }

    public Tbusuario getUsuarioPorId(int idusuario) {
        Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t WHERE t.idusuario = :idusuario");
        q.setParameter("idusuario", idusuario);
        Tbusuario usr = (Tbusuario) q.getSingleResult();
        return usr;
    }

    public List<Tbusuario> getLUsuarioPorIdEmpleado(String idempleado) {

        Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t WHERE t.idempleado.idempleado = :idempleado");
        q.setParameter("idempleado", idempleado);
        @SuppressWarnings("unchecked")
        List<Tbusuario> usr = q.getResultList();
        return usr;
    }

    /**
     * Metodo que Obtiene un listado con todos los usuarios existentes
     *
     * @return lstUsr
     */
    @SuppressWarnings("unchecked")
    public List<Tbusuario> getListaUsuarios() {
        System.out.println("\ngetListaUsuarios()");
        Query q = emfBitacora.createNamedQuery("Tbusuario.findAll");
        List<Tbusuario> lstUsr = q.getResultList();
        return lstUsr;
    }

    /**
     * Metodo que obtiene un listado con los usuarios coincidentes con una
     * busqueda filtrado por nombre o apellidopaterno o apellidomaterno o
     * cualquier cruce de ellos
     *
     * @param nombre
     * @param apellidopaterno
     * @param apellidomaterno
     * @return usr
     */
    @SuppressWarnings("unchecked")
    public List<Tbusuario> getListUsuarioBusqueda(String nombre, String apellidopaterno, String apellidomaterno) {

        nombre = nombre.toUpperCase();
        apellidopaterno = apellidopaterno.toUpperCase();
        apellidomaterno = apellidomaterno.toUpperCase();

        System.out.println("\ngetListUsuarioBusqueda(" + nombre + "," + apellidopaterno + "," + apellidomaterno + ")");

        Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t WHERE upper(t.idempleado.nombre) LIKE :nombre and upper(t.idempleado.apellidopaterno) LIKE :apellidopaterno and (upper(t.idempleado.apellidomaterno) LIKE :apellidomaterno OR t.idempleado.apellidomaterno IS NULL)");
        q.setParameter("nombre", nombre);
        q.setParameter("apellidopaterno", apellidopaterno);
        q.setParameter("apellidomaterno", apellidomaterno);
        List<Tbusuario> usr = q.getResultList();
        return usr;
    }

    /**
     * Metodo que obtiene la lista de Roles
     *
     * @return
     */
    public List<CatRol> getListRoles() {
        Query q = emfBitacora.createNamedQuery("CatRol.findAll");
        @SuppressWarnings("unchecked")
        List<CatRol> roles = q.getResultList();
        return roles;
    }

    public CatRol getRolPorId(int rolId) {
        Query q = emfBitacora.createNamedQuery("CatRol.findById");
        q.setParameter("idRol", rolId);
        CatRol rol = (CatRol) q.getSingleResult();
        return rol;
    }

    /**
     * Metodo que obtiene el id maximo de la tabla Usuario_Rol
     *
     * @return r
     */
    public int getMaxUsuarioRolId() {
//    	String fecha = "%";
        int r = 0;
        try {
            Query q = emfBitacora.createQuery("SELECT max(ur.usuarioRolId) FROM UsuarioRol ur");
//            q.setParameter("fecha", fecha);
            r = (int) q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        r++;
        return r;
    }

    /**
     * Metodo que Obtiene un listado con las dependencias existentes
     *
     * @return dependencias
     */
    public List<Ctdependencia> getListaDependenciasDelegaciones() {
        System.out.println("\ngetListaDependenciasDelegaciones()");
        Query q = emfBitacora.createQuery("FROM Ctdependencia WHERE iddependencia "
                + "IN ('SAMICO990009', 'SAMICO990010', 'SAMICO990011', 'SAMICO990012', 'SAMICO990013', "
                + "'SAMICO990014', 'SAMICO990015', 'SAMICO990016', 'SAMICO990017', 'SAMICO990018', "
                + "'SAMICO990019', 'SAMICO990020', 'SAMICO990021', 'SAMICO990022', 'SAMICO990023', "
                + "'SAMICO990024', 'SAMICO990025', 'SAMICO990026', 'SAMICO990027', 'SAMICO990028', "
                + "'SAMICO990029', 'SAMICO990030', 'SAMICO990031', 'SAMICO990032', 'SAMICO990033', "
                + "'SAMICO990034', 'SAMICO990035', 'SAMICO990036', 'SAMICO990037', 'SAMICO990038', 'SAMICO990123') ORDER BY nombredependencia");
        @SuppressWarnings("unchecked")
        List<Ctdependencia> dependencias = q.getResultList();
        return dependencias;
    }

    /**
     * *
     * Obtiene unicamente la dependencia indicada en el folio
     *
     * @param iddependencia
     * @return
     */
    public List<Ctdependencia> getListaDependenciaFolio(String iddependencia) {
        System.out.println("\ngetListaDependenciaFolio(" + iddependencia + ")");
        Query q = emfBitacora.createQuery("FROM Ctdependencia WHERE iddependencia = '" + iddependencia + "'");
        @SuppressWarnings("unchecked")
        List<Ctdependencia> dependencias = q.getResultList();
        return dependencias;
    }

    /**
     * Metodo que Obtiene un listado con las dependencias existentes
     *
     * @return dependencias
     */
    public List<Ctdependencia> getListaDependenciasOficinasCentrales() {
        Query q = emfBitacora.createQuery("FROM Ctdependencia WHERE iddependencia "
                + "IN ('MEDAMB000001', 'UDEADM940001') ORDER BY nombredependencia");
        @SuppressWarnings("unchecked")
        List<Ctdependencia> dependencias = q.getResultList();
        return dependencias;
    }

    /**
     * Metodo que Obtiene un listado con las dependencias existentes
     *
     * @return dependencias
     */
    public List<Ctdependencia> getListaDependenciasDOC() {
        Query q = emfBitacora.createQuery("FROM Ctdependencia WHERE iddependencia "
                + "IN ('SAMICO990009', 'SAMICO990010', 'SAMICO990011', 'SAMICO990012', 'SAMICO990013', "
                + "'SAMICO990014', 'SAMICO990015', 'SAMICO990016', 'SAMICO990017', 'SAMICO990018', "
                + "'SAMICO990019', 'SAMICO990020', 'SAMICO990021', 'SAMICO990022', 'SAMICO990023', "
                + "'SAMICO990024', 'SAMICO990025', 'SAMICO990026', 'SAMICO990027', 'SAMICO990028', "
                + "'SAMICO990029', 'SAMICO990030', 'SAMICO990031', 'SAMICO990032', 'SAMICO990033', "
                + "'SAMICO990034', 'SAMICO990035', 'SAMICO990036', 'SAMICO990037', 'SAMICO990038', 'SAMICO990123', 'MEDAMB000001', 'UDEADM940001') "
                + "ORDER BY nombredependencia");
        @SuppressWarnings("unchecked")
        List<Ctdependencia> dependencias = q.getResultList();
        return dependencias;
    }

    /**
     * Metodo que obtiene el listado de Usuarios a partir de un area
     *
     * @param identidadfederativa
     * @return usr
     */
    public List<Tbusuario> getListUsuarioPorDelegacion(String identidadfederativa) {

        System.out.println("\ngetListUsuarioPorDelegacion(" + identidadfederativa + ")");
//        Query q2 = emfBitacora.createQuery("SELECT t FROM Tbusuario t WHERE t.idempleado.nombre LIKE :nombre and t.idempleado.apellidopaterno LIKE :apellidopaterno and t.idempleado.apellidomaterno LIKE :apellidomaterno and t.iddependencia LIKE :idDependencia");
        Query q = emfBitacora.createQuery("SELECT tu FROM Tbusuario tu, Tbarea ta WHERE tu.idarea = ta.tbareaPK.idarea and tu.iddependencia = ta.tbareaPK.iddependencia and ta.identidadfederativa = :identidadfederativa");
        q.setParameter("identidadfederativa", identidadfederativa);
        @SuppressWarnings("unchecked")
        List<Tbusuario> usr = q.getResultList();
        return usr;
    }

    /**
     * Metodo que obtiene un listado con los usuarios coincidentes con una
     * busqueda
     *
     * @param nombre
     * @param apellidopaterno
     * @param apellidomaterno
     * @param identidadfederativa
     * @return usr
     */
    public List<Tbusuario> getListUsuarioBusquedaPorDelegacion(String nombre, String apellidopaterno, String apellidomaterno, String identidadfederativa) {

        nombre = nombre.toUpperCase();
        apellidopaterno = apellidopaterno.toUpperCase();
        apellidomaterno = apellidomaterno.toUpperCase();

        System.out.println("\ngetListUsuarioBusquedaPorDelegacion(" + nombre + "," + apellidopaterno + "," + apellidomaterno + "," + identidadfederativa + ")");

        Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t, Tbarea ta WHERE upper(t.idempleado.nombre) LIKE :nombre and upper(t.idempleado.apellidopaterno) LIKE :apellidopaterno and (upper(t.idempleado.apellidomaterno) LIKE :apellidomaterno OR t.idempleado.apellidomaterno IS NULL) "
                + " and t.idarea = ta.tbareaPK.idarea and t.iddependencia = ta.tbareaPK.iddependencia and ta.identidadfederativa = :identidadfederativa");
        q.setParameter("nombre", nombre);
        q.setParameter("apellidopaterno", apellidopaterno);
        q.setParameter("apellidomaterno", apellidomaterno);
        q.setParameter("identidadfederativa", identidadfederativa);
        @SuppressWarnings("unchecked")
        List<Tbusuario> usr = q.getResultList();
        return usr;
    }

    /**
     * *
     * Método que obtiene la lista de usuarios coincidentes segun los filtros
     * like de (nombre, apellidopaterno, apellidomaterno) y la unidad
     * administrativa
     *
     * @param nombre
     * @param apellidopaterno
     * @param apellidomaterno
     * @param idDependencia
     * @return
     */
    public List<Tbusuario> getListUsuarioBusquedaPorUnidadAdministrativa(String nombre, String apellidopaterno, String apellidomaterno, String idDependencia) {

        nombre = nombre.toUpperCase();
        apellidopaterno = apellidopaterno.toUpperCase();
        apellidomaterno = apellidomaterno.toUpperCase();

        System.out.println("\ngetListUsuarioBusquedaPorUnidadAdministrativa(" + nombre + "," + apellidopaterno + "," + apellidomaterno + "," + idDependencia + ")");

        // Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t, Tbarea ta WHERE upper(t.idempleado.nombre) LIKE :nombre and upper(t.idempleado.apellidopaterno) LIKE :apellidopaterno and upper(t.idempleado.apellidomaterno) LIKE :apellidomaterno and t.iddependencia = :idDependencia and t.idarea = ta.tbareaPK.idarea");
        // Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t WHERE upper(t.idempleado.nombre) LIKE :nombre and upper(t.idempleado.apellidopaterno) LIKE :apellidopaterno and (upper(t.idempleado.apellidomaterno) LIKE :apellidomaterno OR t.idempleado.apellidomaterno IS NULL) and t.iddependencia = :idDependencia ");
        Query q = emfBitacora.createQuery("SELECT t FROM Tbusuario t, Tbarea ta WHERE upper(t.idempleado.nombre) LIKE :nombre and upper(t.idempleado.apellidopaterno) LIKE :apellidopaterno and (upper(t.idempleado.apellidomaterno) LIKE :apellidomaterno  OR t.idempleado.apellidomaterno IS NULL) and t.iddependencia = :idDependencia and t.idarea = ta.tbareaPK.idarea and t.iddependencia = ta.tbareaPK.iddependencia");
        q.setParameter("nombre", nombre);
        q.setParameter("apellidopaterno", apellidopaterno);
        q.setParameter("apellidomaterno", apellidomaterno);
        q.setParameter("idDependencia", idDependencia);
        @SuppressWarnings("unchecked")
        List<Tbusuario> usr = q.getResultList();
        return usr;
    }

    public List<Object[]> recuperarMaxDocsSinatResolucion() {
        List<Object[]> resultado = new ArrayList<>();
        Query q;
        try {
            q = emfBitacora.createNativeQuery("SELECT  CASE WHEN (MAX(R.ID) is null)  then 0 ELSE (MAX(R.ID)) END "
                    + "FROM SINAT_USDB.SINAT_RESOLUCION R");
            resultado = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Ocurrió un error");
        } finally {
            emfBitacora.clear();
        }
        return resultado;
    }

    public List<Object[]> recuperarMaxDocsNotificaciones() {
        List<Object[]> resultado = new ArrayList<>();
        Query q;
        try {
            q = emfBitacora.createNativeQuery("SELECT  CASE WHEN (MAX(O.ID_OFICIO_NOTIFICACION) is null)  then 0 ELSE (MAX(O.ID_OFICIO_NOTIFICACION)) END "
                    + "FROM SINAT_USDB.SINAT_OFICIO_NOTIFICACION O");
            resultado = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Ocurrió un error");
        } finally {
            emfBitacora.clear();
        }
        return resultado;
    }

    /**
     * Metodo que obtiene los semaforos de los procesos de exencion
     *
     * @param Numero de proyecto
     * @return usr
     */
    public List<Object[]> obtenerSemaforosProcesoExencion(String bitacora) {

        String query = "SELECT "
                + "	  BITA_NUMERO, "
                + "	  CREACION, "
                + "	  BITA_DIAS_TRAMITE, "
                + "	  BITA_DIAS_PROCESO, "
                + "	  CASE  "
                + "	    WHEN (BITA_DIAS_TRAMITE - BITA_DIAS_PROCESO) < 0 THEN 0  "
                + "	    ELSE (BITA_DIAS_TRAMITE - BITA_DIAS_PROCESO)  "
                + "	  END AS DIAS_RESTANTES_PROCESO, "
                + "	  CASE  "
                + "	    WHEN BITA_EVALUACION = 1 AND BITA_DIAS_PROCESO <= 4 THEN 4 - BITA_DIAS_PROCESO "
                + "	    ELSE 0  "
                + "	  END AS DIAS_REST_PROC_EVAL, "
                + "	  CASE  "
                + "	    WHEN BITA_EVALUACION = 1 THEN BITA_DIAS_NOTIFICAR "
                + "	    ELSE 0  "
                + "	  END AS BITA_DIAS_NOTIFICAR, "
                + "	  CASE  "
                + "	    WHEN BITA_EVALUACION = 1 THEN BITA_DIAS_PROCESO_NOTIFICAR "
                + "	    ELSE 0  "
                + "	  END AS BITA_DIAS_PROCESO_NOTIFICAR, "
                + "	  CASE  "
                + "	    WHEN BITA_EVALUACION = 1 AND BITA_DIAS_PROCESO_NOTIFICAR = 0 THEN BITA_DIAS_NOTIFICAR "
                + "	    WHEN BITA_EVALUACION = 1 AND BITA_DIAS_PROCESO_NOTIFICAR < BITA_DIAS_NOTIFICAR THEN BITA_DIAS_NOTIFICAR - BITA_DIAS_PROCESO_NOTIFICAR "
                + "	    WHEN BITA_EVALUACION = 1 AND BITA_DIAS_PROCESO_NOTIFICAR >= BITA_DIAS_NOTIFICAR THEN 0 "
                + "	    ELSE 0  "
                + "	  END AS DIAS_REST_NOT, "
                + "	  BITA_NOTIFICAR,  "
                + "	  BITA_NOTIFICADO, "
                + "	  BITA_EVALUACION, "
                + "	  BITA_CONCLUIDA "
                + "	FROM "
                + "	  BITACORA.BITACORA "
                + "	WHERE  "
                + "	  BITA_NUMERO = '" + bitacora + "'";

        System.out.println("BitacoraDao :: obtenerSemaforosProcesoExencion " + query);

        Query q = emfBitacora.createNativeQuery(query);

        List<Object[]> response;
        try {
            response = q.getResultList();
        } catch (Exception e) {
            response = new ArrayList<Object[]>();
        } finally {
            emfBitacora.clear();
        }

        return response;
    }

}
