/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.dao;

import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Rengerden
 */
public class EntityFactoryAnalisis {
    
    
    /**
     * @return the DGIRA_MIAE
     */
    public static EntityManagerFactory getDGIRA_MIAE() {
        return PersistenceManager.getEmfMIAE();
    }

}
