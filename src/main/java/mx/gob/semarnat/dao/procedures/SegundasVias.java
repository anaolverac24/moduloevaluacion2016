/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.dao.procedures;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import mx.gob.semarnat.dao.BitacoraDao;
import mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira;
import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.model.sinat.SinatOficioNotificacion;
import mx.gob.semarnat.model.sinat.SinatResolucion;

/**
 *
 * @author mauricio
 */
public class SegundasVias {
	
    private BitacoraDao daoSituaciones = new BitacoraDao();
    private Connection connection = null;
    private static final String driverDB = "oracle.jdbc.OracleDriver";
    // ================= POOL DE DESARROLLO ==========================
    private static final String urlDB = "jdbc:oracle:thin:@scan-db.semarnat.gob.mx:1525/SINDESA";  								 
    private static final String userDB = "DGIRA_MIAE2";  // SINATEC
    private static final String passDB = "DGIRA_MIAE2";  // SINATEC
    private static final String userDBdgira= "DGIRA_MIAE2";
    private static final String passDBdgira = "DGIRA_MIAE2";
    // ================= POOL DE PRODUCCION ==========================
    /*private static final String urlDB = "jdbc:oracle:thin:@rac1-scan.semarnat.gob.mx:1525/SINAT";  								 
    private static final String userDB = "DGIRA_MIAE2";  // SINATEC
    private static final String passDB = "Dg1r4MiaE2";  // SINATEC
    private static final String userDBdgira= "DGIRA_MIAE2";
    private static final String passDBdgira = "Dg1r4MiaE2";*/
    private List<Object[]> maxIdSinatResolucion = new ArrayList<>();
    private List<Object[]> maxIdNotificacion = new ArrayList<>();
    private int idAux = 0;

    private Connection getConexion() throws ClassNotFoundException, SQLException {
        Class.forName(driverDB);
        connection = DriverManager.getConnection(urlDB, userDB, passDB);
        return connection;
    }

    @SuppressWarnings("unused")
	private void closeConnection() throws SQLException {
        connection.close();
    }
    
    private Connection getConexionDGIRA() throws ClassNotFoundException, SQLException {
        Class.forName(driverDB);
        connection = DriverManager.getConnection(urlDB, userDBdgira, passDBdgira);
        return connection;
    }

    //<editor-fold defaultstate="collapsed" desc="Porceso de turnado original">
    public void proc(String vBitacora, int eintiposuspension, String folioProy, int tipoApertura, short tipoOficio) throws Exception {
        Connection con = getConexion();
        Connection conDGIRA = getConexionDGIRA();
        int lote;
        ////eintiposuspension 1=INFORMACION ADICIONAL 2=PREEVENCION
        String evArchivo = "";
        Statement sta = con.createStatement();
        ResultSet rsa = sta.executeQuery("SELECT DOCUMENTO_NOMBRE FROM DGIRA_MIAE2.DOCUMENTO_RESPUESTA_DGIRA WHERE BITACORA_PROYECTO = '" + vBitacora + "' AND TIPO_DOC_ID = " + tipoOficio);
        if (rsa.next()){
            evArchivo = rsa.getString("DOCUMENTO_NOMBRE");
        }

        // Inserta situaciones
        PreparedStatement insSituacion = con.prepareCall("Insert into SINAT_USDB.SINAT_OFICIO_NOTIFICACION(NUMERO_OFICIO, ID_TIPO_OFICIO, FECHA_OFICIO, ARCHIVO_OFICIO, BITA_NUMERO) values('1',?,sysdate,?,?)");
        insSituacion.setInt(1, eintiposuspension);
        insSituacion.setString(2, evArchivo);
        insSituacion.setString(3, vBitacora);
        insSituacion.executeUpdate();

        // Se obtiene el lote para indicar a sinatec
        int ebgfolio = 4568;
        ebgfolio = Integer.parseInt(folioProy);
        
        String evcdescripciongeneral = "Archivo de prueba para la prevención";

        CallableStatement csLote = con.prepareCall("{call SINATEC.PSTFOLIODOCSATLOTE_RG(?,?,?,?,?)}");
        csLote.setInt(1, ebgfolio);
        csLote.setString(2, evcdescripciongeneral);
        csLote.registerOutParameter(3, Types.INTEGER);
        csLote.registerOutParameter(4, Types.INTEGER);
        csLote.registerOutParameter(5, Types.VARCHAR);
        csLote.execute();

        if (csLote.getInt(4) != 0) {
            throw new Exception("Error al ejecutar PSTFOLIODOCSATLOTE_RG: " + csLote.getString(5));
        } else {
            lote = csLote.getInt(3);
            System.out.println("Lote: " + lote);
        }

        // Inserta lote en tabla EVALUACION_PROYECTO
        PreparedStatement inslLoteBDDGIRA = null;
        if(eintiposuspension == 1) //Informacion adicional
        {
            inslLoteBDDGIRA = conDGIRA.prepareCall("UPDATE DGIRA_MIAE2.EVALUACION_PROYECTO SET EVA_LOTE_INFO_ADICIONAL= ? WHERE BITACORA_PROYECTO = ? ");
        }
        if(eintiposuspension == 2) //prevencion
        {
            inslLoteBDDGIRA = conDGIRA.prepareCall("UPDATE DGIRA_MIAE2.EVALUACION_PROYECTO SET EVA_LOTE_PREVENCION = ? WHERE BITACORA_PROYECTO = ? ");
        }
        if (inslLoteBDDGIRA != null) {
            inslLoteBDDGIRA.setInt(1, lote);
            inslLoteBDDGIRA.setString(2, vBitacora);
            inslLoteBDDGIRA.executeUpdate();
            
            inslLoteBDDGIRA.close();
        }
        
        // Carga oficio de notificacion
        evcdescripciongeneral = "Archivo de prevención para prueba de segundas vías";

        CallableStatement csOficio = con.prepareCall("{call SINATEC.PSTFOLIODOCNOTIFICA_RG(?,?,?,?,?,?,?)}");
        csOficio.setInt(1, lote);
        csOficio.setInt(2, ebgfolio);
        csOficio.setString(3, evcdescripciongeneral);
        csOficio.setString(4, evArchivo);
        csOficio.setString(5, "NI1");//ni1
        csOficio.registerOutParameter(6, Types.INTEGER);
        csOficio.registerOutParameter(7, Types.VARCHAR);
        csOficio.execute();

        if (csOficio.getInt(6) != 0) {
            throw new Exception("Error al ejecutar PSTFOLIODOCNOTIFICA_RG,:  " + csOficio.getString(6));
        } else {
            System.out.println("Mesaje de Oficio de Notificación del avance del trámite:  " + csOficio.getString(6));
        }

        // fin de proceso del tramite, que ya esta al 100%
        int eintipoactualizacion = tipoApertura; 
        //1 = Solicita apertura al proceso de actualizacion de requisitos del tramite
        //2 = Solicita apertura al proceso de acceso al link de sistema satelital.
        //3 = Solicita apertura a los proceso de actualizacion de requisitos del tramite y acceso al link de sistema satelital.

        CallableStatement csFin = con.prepareCall("{call SINATEC.PSTFOLIODOCSATLOTENOTIFICA_RG(?,?,?,?,?,?)}");
        csFin.setInt(1, lote);
        csFin.setInt(2, 1);
        csFin.setInt(3, eintipoactualizacion);
        csFin.setInt(4, eintiposuspension);
        csFin.registerOutParameter(5, Types.INTEGER);
        csFin.registerOutParameter(6, Types.VARCHAR);
        csFin.execute();

        if (csFin.getInt(5) != 0) {
            throw new Exception("Error al ejecutar PSTFOLIODOCSATLOTENOTIFICA_RG,:  " + csFin.getString(6));
        } else {
            System.out.println("Mesaje de Cierre del tramite:  " + csFin.getString(6));
        }
        
        con.close();
    }
//</editor-fold>

    public void cerrarTurnado(DocumentoRespuestaDgira documento, short eintiposuspension, String folioProy, int tipoActualizacion, String descripcionDocumento, UsuarioRol usuario, String claveProy) throws Exception {
        /*Nota: El tipo de suspensión que origino el(los) oficios a subir al volumen del SINATEC, 
        se relaciona con la tabla CATALOGOS.CAT_TIPO_SUSPENDIDO.*/
        Connection con = getConexion();
        Connection conDGIRA = getConexionDGIRA();
        Statement sta = con.createStatement();
        SinatOficioNotificacion notificacionSinatec = new SinatOficioNotificacion();
        SinatResolucion registroResolutivo = new SinatResolucion();
        int ebgfolio = Integer.parseInt(folioProy);
        int lote;
        String evcdescripciongeneral = descripcionDocumento;
        //eintiposuspension 2=INFORMACION ADICIONAL 1=PREEVENCION 6=RESOLUTIVO
        String evArchivo = documento.getDocumentoNombre();

        //<editor-fold defaultstate="collapsed" desc="1.- Insertar datos en la tabla SINAT_OFICIO_NOTIFICACION">
        maxIdNotificacion = daoSituaciones.recuperarMaxDocsNotificaciones();
        for(Object o : maxIdNotificacion){
            idAux = Integer.parseInt(o.toString());
        }
        notificacionSinatec.setIdOficioNotificacion(idAux + 1);
        notificacionSinatec.setNumeroOficio(documento.getDocumentoRespuestaDgiraPK().getDocumentoId());
        notificacionSinatec.setIdTipoOficio(eintiposuspension);
        notificacionSinatec.setFechaOficio(new Date());
        notificacionSinatec.setArchivoOficio(documento.getDocumentoNombre());
        notificacionSinatec.setBitaNumero(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
        try {
            daoSituaciones.agrega(notificacionSinatec);
        } catch (Exception e) {
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="1.1.- Si el tipo de suspensión es la conclusión entonces inserta en DGIRA_RESOLUCION">
        if (eintiposuspension == 6) {
            //Se obtiene el ID de la resolucion
            maxIdSinatResolucion = daoSituaciones.recuperarMaxDocsSinatResolucion();
            for (Object o : maxIdSinatResolucion) {
                idAux = Integer.parseInt(o.toString());
            }
            registroResolutivo.setId(idAux + 1);
            registroResolutivo.setBitacora(documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
            //Se registra el folio del oficio
            registroResolutivo.setFolioResolucion(documento.getDocumentoRespuestaDgiraPK().getDocumentoId());
            //Se registra la modalidad de la resolucion, las modalidades se obtienen de la tabla
            //CATALOGOS.CAT_MODO_RESOL
            switch (documento.getDocumentoRespuestaDgiraPK().getTipoDocId()) {
                // Desechamiento
                case 6:
                    registroResolutivo.setIdModoResolucion(Short.valueOf("6"));
                    break;
                // Autorizado
                case 12:
                    registroResolutivo.setIdModoResolucion(Short.valueOf("1"));
                    break;
                // Autorizado condicionado
                case 13:
                    registroResolutivo.setIdModoResolucion(Short.valueOf("4"));
                    break;
                // Negado
                case 14:
                    registroResolutivo.setIdModoResolucion(Short.valueOf("2"));
                    break;
                // Desistido
                case 21:
                    registroResolutivo.setIdModoResolucion(Short.valueOf("7"));
                    break;

            }
            registroResolutivo.setVigIndefinida(new Short("0"));
            registroResolutivo.setFechaResolucion(new Date());
            registroResolutivo.setFechaRegistro(new Date());
            registroResolutivo.setIdUsuarioRegistro(usuario.getUsuarioId().getIdusuario());
            try {
                daoSituaciones.agrega(registroResolutivo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="2.- Solicitud de lote de carga SINATEC">
        CallableStatement csLote = con.prepareCall("{call SINATEC.PSTFOLIODOCSATLOTE_RG(?,?,?,?,?)}");
        csLote.setInt(1, ebgfolio);
        csLote.setString(2, evcdescripciongeneral);
        csLote.registerOutParameter(3, Types.INTEGER);
        csLote.registerOutParameter(4, Types.INTEGER);
        csLote.registerOutParameter(5, Types.VARCHAR);
        csLote.execute();

        if (csLote.getInt(4) != 0) {
            throw new Exception("Error al ejecutar PSTFOLIODOCSATLOTE_RG: " + csLote.getString(5));
        } else {
            lote = csLote.getInt(3);
            System.out.println("Lote:::::: " + lote);
        }

        // Inserta lote en tabla EVALUACION_PROYECTO para la informacion adicional y prevencion
        PreparedStatement inslLoteBDDGIRA = null;
        if (eintiposuspension == 1) //Informacion adicional
        {
            System.out.println(":::::::::::::::::::::: INFORMACION ADICIONAL PASO 2 ::::::::::::::::::");
            inslLoteBDDGIRA = conDGIRA.prepareCall("UPDATE DGIRA_MIAE2.EVALUACION_PROYECTO SET EVA_LOTE_INFO_ADICIONAL= ? WHERE BITACORA_PROYECTO = ? ");
        }
        if (eintiposuspension == 2) //prevencion
        {
            System.out.println(":::::::::::::::::::::: PREVENCION PASO 2 ::::::::::::::::::");                    
            inslLoteBDDGIRA = conDGIRA.prepareCall("UPDATE DGIRA_MIAE2.EVALUACION_PROYECTO SET EVA_LOTE_PREVENCION = ? WHERE BITACORA_PROYECTO = ? ");
        }
        if (inslLoteBDDGIRA != null) {
            inslLoteBDDGIRA.setInt(1, lote);
            inslLoteBDDGIRA.setString(2, documento.getDocumentoRespuestaDgiraPK().getBitacoraProyecto());
            inslLoteBDDGIRA.executeUpdate();

            inslLoteBDDGIRA.close();
        }

        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="3.- Carga oficio de notificacion">
        CallableStatement csOficio = con.prepareCall("{call SINATEC.PSTFOLIODOCNOTIFICA_RG(?,?,?,?,?,?,?)}");
        csOficio.setInt(1, lote);
        csOficio.setInt(2, ebgfolio);
        csOficio.setString(3, evcdescripciongeneral);
        csOficio.setString(4, evArchivo);
        csOficio.setString(5, "NI1");//ni1
        csOficio.registerOutParameter(6, Types.INTEGER);
        csOficio.registerOutParameter(7, Types.VARCHAR);
        csOficio.execute();
        
        System.out.println("CARGA OFICIO NOTIFICACION");
        System.out.println(lote);
        System.out.println(ebgfolio);
        System.out.println(evcdescripciongeneral);
        System.out.println(evArchivo);
        System.out.println("NI1");//ni1
        if (csOficio.getInt(6) != 0) {
            throw new Exception("Error al ejecutar PSTFOLIODOCNOTIFICA_RG,:  " + csOficio.getString(6));
        } else {
            System.out.println("Mesaje de Oficio de Notificación del avance del trámite:  " + csOficio.getString(6));
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="4.- Se envía el documento a SINATEC">
        // fin de proceso del tramite, que ya esta al 100%
        //1 = Solicita apertura al proceso de actualizacion de requisitos del tramite
        //2 = Solicita apertura al proceso de acceso al link de sistema satelital (infoadicional).
        //3 = Solicita apertura a los proceso de actualizacion de requisitos del tramite y acceso al link de sistema satelital.
        //4 = Resolución del trámite
        CallableStatement csFin = con.prepareCall("{call SINATEC.PSTFOLIODOCSATLOTENOTIFICA_RG(?,?,?,?,?,?)}");
        csFin.setInt(1, lote);
        csFin.setInt(2, 1);
        csFin.setInt(3, tipoActualizacion);
        csFin.setInt(4, eintiposuspension);
        csFin.registerOutParameter(5, Types.INTEGER);
        csFin.registerOutParameter(6, Types.VARCHAR);
        csFin.execute();

        System.out.println("Se envía el documento a SINATEC");
        System.out.println(lote);
        System.out.println(1);
        System.out.println(tipoActualizacion);
        System.out.println(eintiposuspension);
        
        if (csFin.getInt(5) != 0) {
            throw new Exception("Error al ejecutar PSTFOLIODOCSATLOTENOTIFICA_RG,:  " + csFin.getString(6));
        } else {
            System.out.println("Mesaje de Cierre del tramite:  " + csFin.getString(6));
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="4.1 Si se trata del resolutivo se ejecuta el cierre del trámite">
        if(eintiposuspension==6){
            CallableStatement csConclusion = con.prepareCall("{call SINATEC.PSTTRAMITECONCLUSION_RG(?,?,?,?,?)}");
            csConclusion.setInt(1, ebgfolio);
            // Es un valor entero (0 cero) que indica que trámite a concluido
            csConclusion.setInt(2, 0);
            csConclusion.setString(3, claveProy);
            csConclusion.registerOutParameter(4, Types.INTEGER);
            csConclusion.registerOutParameter(5, Types.VARCHAR);
        }
        
//</editor-fold>

        con.close();
    }

    public static void main(String[] args) {
        SegundasVias s = new SegundasVias();
        try {
            s.proc("09/IPW0003/11/14", 1, "4568",1,(short)7);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
