package mx.gob.semarnat.dao.procedures;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChecklistProcedure {
    private final String PSTCHECKLIST_AO = "{call DGIRA_MIAE2.TRASLADA_REQUISITOS(?)}";
    
    //<editor-fold desc="Parametros para la conexión nativa" defaultstate="collapsed">
    private Connection connection = null;
    final String driverDB = "oracle.jdbc.OracleDriver";
    final String urlDB = "jdbc:oracle:thin:@localhost:1521:SINDESA";
    final String userDB = "DGIRA_MIAE2";
    final String passDB = "DGIRA_MIAE2";
    
    private Connection getConexion() throws ClassNotFoundException, SQLException {
        Class.forName(driverDB);
        connection = DriverManager.getConnection(urlDB, userDB, passDB);
        return connection;
    }

    private void closeConnection() throws SQLException {
        connection.close();
    }//</editor-fold>
    
    //<editor-fold desc="SP de carga de checklist" defaultstate="collapsed">
    /**
     * 
     * @param bitacora
     * @throws ClassNotFoundException 
     */
    public void ejecutarSP(String bitacora) throws ClassNotFoundException{
        String msj = "err";
        try {
            CallableStatement cs = getConexion().prepareCall(PSTCHECKLIST_AO);
            cs.setString(1,bitacora);
            cs.execute();
            closeConnection();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ChecklistProcedure.class.getName()).log(Level.SEVERE,null,ex);
            System.out.println(msj);
        }
    }//</editor-fold>
}
