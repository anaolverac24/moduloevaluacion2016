package mx.gob.semarnat.dao.procedures;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Chely
 */
public class RequisitosDataModel extends ListDataModel<Object> implements SelectableDataModel<Object> {

    public RequisitosDataModel() {
    }

    public RequisitosDataModel(List<Object> list) {
        super(list);
    }

    @Override
    public Object getRowKey(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getRowData(String rowKey) {
        List<Object> reqObjects = (List<Object>) this.getWrappedData();
        for(Object o : reqObjects){
            return o;
        }
        return null;
    }
    
}
