package mx.gob.semarnat.model.oficios;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ana Olvera
 */
@Entity
@Table(name = "TIPO_DOCUMENTO", schema = "DGIRA_RESOLUTIVO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoDocumento.findAll", query = "SELECT t FROM TipoDocumento t")
    , @NamedQuery(name = "TipoDocumento.findByClaveTramite2", query = "SELECT t FROM TipoDocumento t WHERE t.tipoDocumentoPK.claveTramite2 = :claveTramite2")
    , @NamedQuery(name = "TipoDocumento.findByIdTramite2", query = "SELECT t FROM TipoDocumento t WHERE t.tipoDocumentoPK.idTramite2 = :idTramite2")
    , @NamedQuery(name = "TipoDocumento.findByTipoDocId", query = "SELECT t FROM TipoDocumento t WHERE t.tipoDocumentoPK.tipoDocId = :tipoDocId")
    , @NamedQuery(name = "TipoDocumento.findByTipoDocDescripcion", query = "SELECT t FROM TipoDocumento t WHERE t.tipoDocDescripcion = :tipoDocDescripcion")
    , @NamedQuery(name = "TipoDocumento.findByTipoDocUsuario", query = "SELECT t FROM TipoDocumento t WHERE t.tipoDocUsuario = :tipoDocUsuario"),
    @NamedQuery(name = "TipoDocumento.findDescripcionByPK", query = "SELECT t FROM TipoDocumento t WHERE t.tipoDocumentoPK.claveTramite2 = :claveTramite AND t.tipoDocumentoPK.idTramite2 = :idTramite AND t.tipoDocumentoPK.tipoDocId = :tipoDocId")})
public class TipoDocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TipoDocumentoPK tipoDocumentoPK;
    @Size(max = 300)
    @Column(name = "TIPO_DOC_DESCRIPCION")
    private String tipoDocDescripcion;
    @Size(max = 4)
    @Column(name = "TIPO_DOC_USUARIO")
    private String tipoDocUsuario;

    public TipoDocumento() {
    }

    public TipoDocumento(TipoDocumentoPK tipoDocumentoPK) {
        this.tipoDocumentoPK = tipoDocumentoPK;
    }

    public TipoDocumento(String claveTramite2, int idTramite2, short tipoDocId) {
        this.tipoDocumentoPK = new TipoDocumentoPK(claveTramite2, idTramite2, tipoDocId);
    }

    public TipoDocumentoPK getTipoDocumentoPK() {
        return tipoDocumentoPK;
    }

    public void setTipoDocumentoPK(TipoDocumentoPK tipoDocumentoPK) {
        this.tipoDocumentoPK = tipoDocumentoPK;
    }

    public String getTipoDocDescripcion() {
        return tipoDocDescripcion;
    }

    public void setTipoDocDescripcion(String tipoDocDescripcion) {
        this.tipoDocDescripcion = tipoDocDescripcion;
    }

    public String getTipoDocUsuario() {
        return tipoDocUsuario;
    }

    public void setTipoDocUsuario(String tipoDocUsuario) {
        this.tipoDocUsuario = tipoDocUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoDocumentoPK != null ? tipoDocumentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDocumento)) {
            return false;
        }
        TipoDocumento other = (TipoDocumento) object;
        if ((this.tipoDocumentoPK == null && other.tipoDocumentoPK != null) || (this.tipoDocumentoPK != null && !this.tipoDocumentoPK.equals(other.tipoDocumentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tipoDocDescripcion;
    }

}
