package mx.gob.semarnat.model.oficios;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ana Olvera
 */
@Embeddable
public class TipoDocumentoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CLAVE_TRAMITE2")
    private String claveTramite2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TRAMITE2")
    private int idTramite2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_DOC_ID")
    private short tipoDocId;

    public TipoDocumentoPK() {
    }

    public TipoDocumentoPK(String claveTramite2, int idTramite2, short tipoDocId) {
        this.claveTramite2 = claveTramite2;
        this.idTramite2 = idTramite2;
        this.tipoDocId = tipoDocId;
    }

    public String getClaveTramite2() {
        return claveTramite2;
    }

    public void setClaveTramite2(String claveTramite2) {
        this.claveTramite2 = claveTramite2;
    }

    public int getIdTramite2() {
        return idTramite2;
    }

    public void setIdTramite2(int idTramite2) {
        this.idTramite2 = idTramite2;
    }

    public short getTipoDocId() {
        return tipoDocId;
    }

    public void setTipoDocId(short tipoDocId) {
        this.tipoDocId = tipoDocId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claveTramite2 != null ? claveTramite2.hashCode() : 0);
        hash += (int) idTramite2;
        hash += (int) tipoDocId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDocumentoPK)) {
            return false;
        }
        TipoDocumentoPK other = (TipoDocumentoPK) object;
        if ((this.claveTramite2 == null && other.claveTramite2 != null) || (this.claveTramite2 != null && !this.claveTramite2.equals(other.claveTramite2))) {
            return false;
        }
        if (this.idTramite2 != other.idTramite2) {
            return false;
        }
        if (this.tipoDocId != other.tipoDocId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.oficios.TipoDocumentoPK[ claveTramite2=" + claveTramite2 + ", idTramite2=" + idTramite2 + ", tipoDocId=" + tipoDocId + " ]";
    }

}
