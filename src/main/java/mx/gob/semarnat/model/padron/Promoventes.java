/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.padron;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "PROMOVENTES", schema="PADRON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Promoventes.findAll", query = "SELECT p FROM Promoventes p"),
    @NamedQuery(name = "Promoventes.findByPromId", query = "SELECT p FROM Promoventes p WHERE p.promId = :promId"),
    @NamedQuery(name = "Promoventes.findByPromRfcVigente", query = "SELECT p FROM Promoventes p WHERE p.promRfcVigente = :promRfcVigente"),
    @NamedQuery(name = "Promoventes.findByPromFechaAlta", query = "SELECT p FROM Promoventes p WHERE p.promFechaAlta = :promFechaAlta"),
    @NamedQuery(name = "Promoventes.findByPromStatus", query = "SELECT p FROM Promoventes p WHERE p.promStatus = :promStatus"),
    @NamedQuery(name = "Promoventes.findByTprId", query = "SELECT p FROM Promoventes p WHERE p.tprId = :tprId"),
    @NamedQuery(name = "Promoventes.findByPromOrigen", query = "SELECT p FROM Promoventes p WHERE p.promOrigen = :promOrigen"),
    @NamedQuery(name = "Promoventes.findByPromFecAlta", query = "SELECT p FROM Promoventes p WHERE p.promFecAlta = :promFecAlta"),
    @NamedQuery(name = "Promoventes.findByPromFecBaja", query = "SELECT p FROM Promoventes p WHERE p.promFecBaja = :promFecBaja"),
    @NamedQuery(name = "Promoventes.findByPromFecModi", query = "SELECT p FROM Promoventes p WHERE p.promFecModi = :promFecModi"),
    @NamedQuery(name = "Promoventes.findByPromRfcAnterior", query = "SELECT p FROM Promoventes p WHERE p.promRfcAnterior = :promRfcAnterior"),
    @NamedQuery(name = "Promoventes.findByConpNombre", query = "SELECT p FROM Promoventes p WHERE p.conpNombre = :conpNombre"),
    @NamedQuery(name = "Promoventes.findByConpObservacion", query = "SELECT p FROM Promoventes p WHERE p.conpObservacion = :conpObservacion"),
    @NamedQuery(name = "Promoventes.findByPromWarning", query = "SELECT p FROM Promoventes p WHERE p.promWarning = :promWarning"),
    @NamedQuery(name = "Promoventes.findByPromFecEntrega", query = "SELECT p FROM Promoventes p WHERE p.promFecEntrega = :promFecEntrega"),
    @NamedQuery(name = "Promoventes.findByPromSemObservacion", query = "SELECT p FROM Promoventes p WHERE p.promSemObservacion = :promSemObservacion"),
    @NamedQuery(name = "Promoventes.findByPromTramGrisVerde", query = "SELECT p FROM Promoventes p WHERE p.promTramGrisVerde = :promTramGrisVerde")})
public class Promoventes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROM_ID")
    private String promId;
    @Column(name = "PROM_RFC_VIGENTE")
    private String promRfcVigente;
    @Column(name = "PROM_FECHA_ALTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date promFechaAlta;
    @Column(name = "PROM_STATUS")
    private Character promStatus;
    @Basic(optional = false)
    @Column(name = "TPR_ID")
    private String tprId;
    @Column(name = "PROM_ORIGEN")
    private String promOrigen;
    @Column(name = "PROM_FEC_ALTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date promFecAlta;
    @Column(name = "PROM_FEC_BAJA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date promFecBaja;
    @Column(name = "PROM_FEC_MODI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date promFecModi;
    @Column(name = "PROM_RFC_ANTERIOR")
    private String promRfcAnterior;
    @Column(name = "CONP_NOMBRE")
    private String conpNombre;
    @Column(name = "CONP_OBSERVACION")
    private String conpObservacion;
    @Column(name = "PROM_WARNING")
    private Character promWarning;
    @Column(name = "PROM_FEC_ENTREGA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date promFecEntrega;
    @Column(name = "PROM_SEM_OBSERVACION")
    private String promSemObservacion;
    @Column(name = "PROM_TRAM_GRIS_VERDE")
    private Character promTramGrisVerde;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "promoventes")
    private Collection<DatosFiscales> datosFiscalesCollection;

    public Promoventes() {
    }

    public Promoventes(String promId) {
        this.promId = promId;
    }

    public Promoventes(String promId, String tprId) {
        this.promId = promId;
        this.tprId = tprId;
    }

    public String getPromId() {
        return promId;
    }

    public void setPromId(String promId) {
        this.promId = promId;
    }

    public String getPromRfcVigente() {
        return promRfcVigente;
    }

    public void setPromRfcVigente(String promRfcVigente) {
        this.promRfcVigente = promRfcVigente;
    }

    public Date getPromFechaAlta() {
        return promFechaAlta;
    }

    public void setPromFechaAlta(Date promFechaAlta) {
        this.promFechaAlta = promFechaAlta;
    }

    public Character getPromStatus() {
        return promStatus;
    }

    public void setPromStatus(Character promStatus) {
        this.promStatus = promStatus;
    }

    public String getTprId() {
        return tprId;
    }

    public void setTprId(String tprId) {
        this.tprId = tprId;
    }

    public String getPromOrigen() {
        return promOrigen;
    }

    public void setPromOrigen(String promOrigen) {
        this.promOrigen = promOrigen;
    }

    public Date getPromFecAlta() {
        return promFecAlta;
    }

    public void setPromFecAlta(Date promFecAlta) {
        this.promFecAlta = promFecAlta;
    }

    public Date getPromFecBaja() {
        return promFecBaja;
    }

    public void setPromFecBaja(Date promFecBaja) {
        this.promFecBaja = promFecBaja;
    }

    public Date getPromFecModi() {
        return promFecModi;
    }

    public void setPromFecModi(Date promFecModi) {
        this.promFecModi = promFecModi;
    }

    public String getPromRfcAnterior() {
        return promRfcAnterior;
    }

    public void setPromRfcAnterior(String promRfcAnterior) {
        this.promRfcAnterior = promRfcAnterior;
    }

    public String getConpNombre() {
        return conpNombre;
    }

    public void setConpNombre(String conpNombre) {
        this.conpNombre = conpNombre;
    }

    public String getConpObservacion() {
        return conpObservacion;
    }

    public void setConpObservacion(String conpObservacion) {
        this.conpObservacion = conpObservacion;
    }

    public Character getPromWarning() {
        return promWarning;
    }

    public void setPromWarning(Character promWarning) {
        this.promWarning = promWarning;
    }

    public Date getPromFecEntrega() {
        return promFecEntrega;
    }

    public void setPromFecEntrega(Date promFecEntrega) {
        this.promFecEntrega = promFecEntrega;
    }

    public String getPromSemObservacion() {
        return promSemObservacion;
    }

    public void setPromSemObservacion(String promSemObservacion) {
        this.promSemObservacion = promSemObservacion;
    }

    public Character getPromTramGrisVerde() {
        return promTramGrisVerde;
    }

    public void setPromTramGrisVerde(Character promTramGrisVerde) {
        this.promTramGrisVerde = promTramGrisVerde;
    }

    @XmlTransient
    public Collection<DatosFiscales> getDatosFiscalesCollection() {
        return datosFiscalesCollection;
    }

    public void setDatosFiscalesCollection(Collection<DatosFiscales> datosFiscalesCollection) {
        this.datosFiscalesCollection = datosFiscalesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promId != null ? promId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promoventes)) {
            return false;
        }
        Promoventes other = (Promoventes) object;
        if ((this.promId == null && other.promId != null) || (this.promId != null && !this.promId.equals(other.promId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Promoventes[ promId=" + promId + " ]";
    }
    
}
