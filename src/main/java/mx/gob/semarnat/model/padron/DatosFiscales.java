/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.padron;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "DATOS_FISCALES", schema="PADRON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosFiscales.findAll", query = "SELECT d FROM DatosFiscales d"),
    @NamedQuery(name = "DatosFiscales.findByDfiRfc", query = "SELECT d FROM DatosFiscales d WHERE d.datosFiscalesPK.dfiRfc = :dfiRfc"),
    @NamedQuery(name = "DatosFiscales.findByPromId", query = "SELECT d FROM DatosFiscales d WHERE d.datosFiscalesPK.promId = :promId"),
    @NamedQuery(name = "DatosFiscales.findByIdTipoDireccion", query = "SELECT d FROM DatosFiscales d WHERE d.datosFiscalesPK.idTipoDireccion = :idTipoDireccion"),
    @NamedQuery(name = "DatosFiscales.findByConsIdTipoDireccion", query = "SELECT d FROM DatosFiscales d WHERE d.datosFiscalesPK.consIdTipoDireccion = :consIdTipoDireccion"),
    @NamedQuery(name = "DatosFiscales.findByDfiNombre", query = "SELECT d FROM DatosFiscales d WHERE d.dfiNombre = :dfiNombre"),
    @NamedQuery(name = "DatosFiscales.findByDfiApellidoPaterno", query = "SELECT d FROM DatosFiscales d WHERE d.dfiApellidoPaterno = :dfiApellidoPaterno"),
    @NamedQuery(name = "DatosFiscales.findByDfiApellidoMaterno", query = "SELECT d FROM DatosFiscales d WHERE d.dfiApellidoMaterno = :dfiApellidoMaterno"),
    @NamedQuery(name = "DatosFiscales.findByDfiRazonSocial", query = "SELECT d FROM DatosFiscales d WHERE d.dfiRazonSocial = :dfiRazonSocial"),
    @NamedQuery(name = "DatosFiscales.findByDfiCalle", query = "SELECT d FROM DatosFiscales d WHERE d.dfiCalle = :dfiCalle"),
    @NamedQuery(name = "DatosFiscales.findByDfiColonia", query = "SELECT d FROM DatosFiscales d WHERE d.dfiColonia = :dfiColonia"),
    @NamedQuery(name = "DatosFiscales.findByIdDelegacionMunicipio", query = "SELECT d FROM DatosFiscales d WHERE d.idDelegacionMunicipio = :idDelegacionMunicipio"),
    @NamedQuery(name = "DatosFiscales.findByIdEntidadFederativa", query = "SELECT d FROM DatosFiscales d WHERE d.idEntidadFederativa = :idEntidadFederativa"),
    @NamedQuery(name = "DatosFiscales.findByDfiCodigoPostal", query = "SELECT d FROM DatosFiscales d WHERE d.dfiCodigoPostal = :dfiCodigoPostal"),
    @NamedQuery(name = "DatosFiscales.findByDfiCurp", query = "SELECT d FROM DatosFiscales d WHERE d.dfiCurp = :dfiCurp"),
    @NamedQuery(name = "DatosFiscales.findByDfiTipPer", query = "SELECT d FROM DatosFiscales d WHERE d.dfiTipPer = :dfiTipPer"),
    @NamedQuery(name = "DatosFiscales.findByDfiNumExt", query = "SELECT d FROM DatosFiscales d WHERE d.dfiNumExt = :dfiNumExt"),
    @NamedQuery(name = "DatosFiscales.findByDfiNumInt", query = "SELECT d FROM DatosFiscales d WHERE d.dfiNumInt = :dfiNumInt"),
    @NamedQuery(name = "DatosFiscales.findByDfiLocalidad", query = "SELECT d FROM DatosFiscales d WHERE d.dfiLocalidad = :dfiLocalidad"),
    @NamedQuery(name = "DatosFiscales.findByDfiActPrincipal", query = "SELECT d FROM DatosFiscales d WHERE d.dfiActPrincipal = :dfiActPrincipal"),
    @NamedQuery(name = "DatosFiscales.findByCveTipoAsen", query = "SELECT d FROM DatosFiscales d WHERE d.cveTipoAsen = :cveTipoAsen"),
    @NamedQuery(name = "DatosFiscales.findByCveTipoVial", query = "SELECT d FROM DatosFiscales d WHERE d.cveTipoVial = :cveTipoVial"),
    @NamedQuery(name = "DatosFiscales.findByDfiRepresentanteLegal", query = "SELECT d FROM DatosFiscales d WHERE d.dfiRepresentanteLegal = :dfiRepresentanteLegal"),
    @NamedQuery(name = "DatosFiscales.findByEstatus", query = "SELECT d FROM DatosFiscales d WHERE d.estatus = :estatus")})
public class DatosFiscales implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DatosFiscalesPK datosFiscalesPK;
    @Column(name = "DFI_NOMBRE")
    private String dfiNombre;
    @Column(name = "DFI_APELLIDO_PATERNO")
    private String dfiApellidoPaterno;
    @Column(name = "DFI_APELLIDO_MATERNO")
    private String dfiApellidoMaterno;
    @Column(name = "DFI_RAZON_SOCIAL")
    private String dfiRazonSocial;
    @Column(name = "DFI_CALLE")
    private String dfiCalle;
    @Column(name = "DFI_COLONIA")
    private String dfiColonia;
    @Column(name = "ID_DELEGACION_MUNICIPIO")
    private String idDelegacionMunicipio;
    @Column(name = "ID_ENTIDAD_FEDERATIVA")
    private String idEntidadFederativa;
    @Column(name = "DFI_CODIGO_POSTAL")
    private String dfiCodigoPostal;
    @Column(name = "DFI_CURP")
    private String dfiCurp;
    @Column(name = "DFI_TIP_PER")
    private String dfiTipPer;
    @Column(name = "DFI_NUM_EXT")
    private String dfiNumExt;
    @Column(name = "DFI_NUM_INT")
    private String dfiNumInt;
    @Column(name = "DFI_LOCALIDAD")
    private String dfiLocalidad;
    @Column(name = "DFI_ACT_PRINCIPAL")
    private String dfiActPrincipal;
    @Column(name = "CVE_TIPO_ASEN")
    private Short cveTipoAsen;
    @Column(name = "CVE_TIPO_VIAL")
    private Short cveTipoVial;
    @Column(name = "DFI_REPRESENTANTE_LEGAL")
    private String dfiRepresentanteLegal;
    @Column(name = "ESTATUS")
    private Short estatus;
    @JoinColumn(name = "PROM_ID", referencedColumnName = "PROM_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Promoventes promoventes;

    public DatosFiscales() {
    }

    public DatosFiscales(DatosFiscalesPK datosFiscalesPK) {
        this.datosFiscalesPK = datosFiscalesPK;
    }

    public DatosFiscales(String dfiRfc, String promId, String idTipoDireccion, short consIdTipoDireccion) {
        this.datosFiscalesPK = new DatosFiscalesPK(dfiRfc, promId, idTipoDireccion, consIdTipoDireccion);
    }

    public DatosFiscalesPK getDatosFiscalesPK() {
        return datosFiscalesPK;
    }

    public void setDatosFiscalesPK(DatosFiscalesPK datosFiscalesPK) {
        this.datosFiscalesPK = datosFiscalesPK;
    }

    public String getDfiNombre() {
        return dfiNombre;
    }

    public void setDfiNombre(String dfiNombre) {
        this.dfiNombre = dfiNombre;
    }

    public String getDfiApellidoPaterno() {
        return dfiApellidoPaterno;
    }

    public void setDfiApellidoPaterno(String dfiApellidoPaterno) {
        this.dfiApellidoPaterno = dfiApellidoPaterno;
    }

    public String getDfiApellidoMaterno() {
        return dfiApellidoMaterno;
    }

    public void setDfiApellidoMaterno(String dfiApellidoMaterno) {
        this.dfiApellidoMaterno = dfiApellidoMaterno;
    }

    public String getDfiRazonSocial() {
        return dfiRazonSocial;
    }

    public void setDfiRazonSocial(String dfiRazonSocial) {
        this.dfiRazonSocial = dfiRazonSocial;
    }

    public String getDfiCalle() {
        return dfiCalle;
    }

    public void setDfiCalle(String dfiCalle) {
        this.dfiCalle = dfiCalle;
    }

    public String getDfiColonia() {
        return dfiColonia;
    }

    public void setDfiColonia(String dfiColonia) {
        this.dfiColonia = dfiColonia;
    }

    public String getIdDelegacionMunicipio() {
        return idDelegacionMunicipio;
    }

    public void setIdDelegacionMunicipio(String idDelegacionMunicipio) {
        this.idDelegacionMunicipio = idDelegacionMunicipio;
    }

    public String getIdEntidadFederativa() {
        return idEntidadFederativa;
    }

    public void setIdEntidadFederativa(String idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    public String getDfiCodigoPostal() {
        return dfiCodigoPostal;
    }

    public void setDfiCodigoPostal(String dfiCodigoPostal) {
        this.dfiCodigoPostal = dfiCodigoPostal;
    }

    public String getDfiCurp() {
        return dfiCurp;
    }

    public void setDfiCurp(String dfiCurp) {
        this.dfiCurp = dfiCurp;
    }

    public String getDfiTipPer() {
        return dfiTipPer;
    }

    public void setDfiTipPer(String dfiTipPer) {
        this.dfiTipPer = dfiTipPer;
    }

    public String getDfiNumExt() {
        return dfiNumExt;
    }

    public void setDfiNumExt(String dfiNumExt) {
        this.dfiNumExt = dfiNumExt;
    }

    public String getDfiNumInt() {
        return dfiNumInt;
    }

    public void setDfiNumInt(String dfiNumInt) {
        this.dfiNumInt = dfiNumInt;
    }

    public String getDfiLocalidad() {
        return dfiLocalidad;
    }

    public void setDfiLocalidad(String dfiLocalidad) {
        this.dfiLocalidad = dfiLocalidad;
    }

    public String getDfiActPrincipal() {
        return dfiActPrincipal;
    }

    public void setDfiActPrincipal(String dfiActPrincipal) {
        this.dfiActPrincipal = dfiActPrincipal;
    }

    public Short getCveTipoAsen() {
        return cveTipoAsen;
    }

    public void setCveTipoAsen(Short cveTipoAsen) {
        this.cveTipoAsen = cveTipoAsen;
    }

    public Short getCveTipoVial() {
        return cveTipoVial;
    }

    public void setCveTipoVial(Short cveTipoVial) {
        this.cveTipoVial = cveTipoVial;
    }

    public String getDfiRepresentanteLegal() {
        return dfiRepresentanteLegal;
    }

    public void setDfiRepresentanteLegal(String dfiRepresentanteLegal) {
        this.dfiRepresentanteLegal = dfiRepresentanteLegal;
    }

    public Short getEstatus() {
        return estatus;
    }

    public void setEstatus(Short estatus) {
        this.estatus = estatus;
    }

    public Promoventes getPromoventes() {
        return promoventes;
    }

    public void setPromoventes(Promoventes promoventes) {
        this.promoventes = promoventes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (datosFiscalesPK != null ? datosFiscalesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosFiscales)) {
            return false;
        }
        DatosFiscales other = (DatosFiscales) object;
        if ((this.datosFiscalesPK == null && other.datosFiscalesPK != null) || (this.datosFiscalesPK != null && !this.datosFiscalesPK.equals(other.datosFiscalesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.DatosFiscales[ datosFiscalesPK=" + datosFiscalesPK + " ]";
    }
    
}
