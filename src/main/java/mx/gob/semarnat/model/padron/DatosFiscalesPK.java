/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.padron;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Paty
 */
@Embeddable
public class DatosFiscalesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "DFI_RFC")
    private String dfiRfc;
    @Basic(optional = false)
    @Column(name = "PROM_ID")
    private String promId;
    @Basic(optional = false)
    @Column(name = "ID_TIPO_DIRECCION")
    private String idTipoDireccion;
    @Basic(optional = false)
    @Column(name = "CONS_ID_TIPO_DIRECCION")
    private short consIdTipoDireccion;

    public DatosFiscalesPK() {
    }

    public DatosFiscalesPK(String dfiRfc, String promId, String idTipoDireccion, short consIdTipoDireccion) {
        this.dfiRfc = dfiRfc;
        this.promId = promId;
        this.idTipoDireccion = idTipoDireccion;
        this.consIdTipoDireccion = consIdTipoDireccion;
    }

    public String getDfiRfc() {
        return dfiRfc;
    }

    public void setDfiRfc(String dfiRfc) {
        this.dfiRfc = dfiRfc;
    }

    public String getPromId() {
        return promId;
    }

    public void setPromId(String promId) {
        this.promId = promId;
    }

    public String getIdTipoDireccion() {
        return idTipoDireccion;
    }

    public void setIdTipoDireccion(String idTipoDireccion) {
        this.idTipoDireccion = idTipoDireccion;
    }

    public short getConsIdTipoDireccion() {
        return consIdTipoDireccion;
    }

    public void setConsIdTipoDireccion(short consIdTipoDireccion) {
        this.consIdTipoDireccion = consIdTipoDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dfiRfc != null ? dfiRfc.hashCode() : 0);
        hash += (promId != null ? promId.hashCode() : 0);
        hash += (idTipoDireccion != null ? idTipoDireccion.hashCode() : 0);
        hash += (int) consIdTipoDireccion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosFiscalesPK)) {
            return false;
        }
        DatosFiscalesPK other = (DatosFiscalesPK) object;
        if ((this.dfiRfc == null && other.dfiRfc != null) || (this.dfiRfc != null && !this.dfiRfc.equals(other.dfiRfc))) {
            return false;
        }
        if ((this.promId == null && other.promId != null) || (this.promId != null && !this.promId.equals(other.promId))) {
            return false;
        }
        if ((this.idTipoDireccion == null && other.idTipoDireccion != null) || (this.idTipoDireccion != null && !this.idTipoDireccion.equals(other.idTipoDireccion))) {
            return false;
        }
        if (this.consIdTipoDireccion != other.consIdTipoDireccion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.DatosFiscalesPK[ dfiRfc=" + dfiRfc + ", promId=" + promId + ", idTipoDireccion=" + idTipoDireccion + ", consIdTipoDireccion=" + consIdTipoDireccion + " ]";
    }
    
}
