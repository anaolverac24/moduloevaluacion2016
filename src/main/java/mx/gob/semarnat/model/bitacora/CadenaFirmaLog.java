package mx.gob.semarnat.model.bitacora;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Montoya
 */
@Entity
@Table(name = "CADENA_FIRMA_LOG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CadenaFirmaLog.findAll", query = "SELECT c FROM CadenaFirmaLog c"),
    @NamedQuery(name = "CadenaFirmaLog.findByClave", query = "SELECT c FROM CadenaFirmaLog c WHERE c.clave = :clave"),
    @NamedQuery(name = "CadenaFirmaLog.findByRol", query = "SELECT c FROM CadenaFirmaLog c WHERE c.rol = :rol"),
    @NamedQuery(name = "CadenaFirmaLog.findByRfc", query = "SELECT c FROM CadenaFirmaLog c WHERE c.rfc = :rfc"),
    @NamedQuery(name = "CadenaFirmaLog.findByFecha", query = "SELECT c FROM CadenaFirmaLog c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "CadenaFirmaLog.findByEstatus", query = "SELECT c FROM CadenaFirmaLog c WHERE c.estatus = :estatus")})
public class CadenaFirmaLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator="InvSeqCFL") 
    @SequenceGenerator(name="InvSeqCFL",sequenceName="seq_proy_cadena_firma_log")
    @Basic(optional = false)
    @Column(name = "CLAVE")
    private Long clave;
//    @Basic(optional = false)
//    @Column(name = "NIVEL")
//    private short nivel;
    @Basic(optional = false)
    @Column(name = "ROL")
    private String rol;
    @Basic(optional = false)
    @Column(name = "RFC")
    private String rfc;
    @Basic(optional = false)
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private short estatus;
    @JoinColumn(name = "CADENA_FIRMA", referencedColumnName = "CLAVE")
    @ManyToOne(optional = false)
    private CadenaFirma cadenaFirma;    
    @Basic(optional = true)
    @Column(name = "FIRMA")
    private String firma;

    public CadenaFirmaLog() {
    }

    public CadenaFirmaLog(Long clave) {
        this.clave = clave;
    }

    public CadenaFirmaLog(Long clave, /*short nivel, */String rol, String rfc, Date fecha, short estatus, String firma) {
        this.clave = clave;
        //this.nivel = nivel;
        this.rol = rol;
        this.rfc = rfc;
        this.fecha = fecha;
        this.estatus = estatus;
        this.firma = firma;
    }

    public Long getClave() {
        return clave;
    }

    public void setClave(Long clave) {
        this.clave = clave;
    }

//    public short getNivel() {
//        return nivel;
//    }
//
//    public void setNivel(short nivel) {
//        this.nivel = nivel;
//    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public short getEstatus() {
        return estatus;
    }

    public void setEstatus(short estatus) {
        this.estatus = estatus;
    }

    public CadenaFirma getCadenaFirma() {
        return cadenaFirma;
    }

    public void setCadenaFirma(CadenaFirma cadenaFirma) {
        this.cadenaFirma = cadenaFirma;
    }
    
    

    public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CadenaFirmaLog)) {
            return false;
        }
        CadenaFirmaLog other = (CadenaFirmaLog) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.bitacora.CadenaFirmaLog[ clave=" + clave + " ]";
    }
    
}