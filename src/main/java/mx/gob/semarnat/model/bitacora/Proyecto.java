/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.bitacora;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity(name = "Proyecto_Bitacora")
@Table(name = "PROYECTO", schema="BITACORA_TEMATICA")
@XmlRootElement(name = "Proyecto_Bitacora")
@NamedQueries({
    @NamedQuery(name = "Proyecto_Bitacora.FindAll", query = "SELECT p FROM Proyecto_Bitacora p"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByCve", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.cve = :cve"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByNumeroBita", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.numeroBita = :numeroBita"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByNombre", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByResolucion", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.resolucion = :resolucion"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByTipoProy", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.tipoProy = :tipoProy"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByEntContable", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.entContable = :entContable"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByTipoMoneda", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.tipoMoneda = :tipoMoneda"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByInversion", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.inversion = :inversion"),
    @NamedQuery(name = "Proyecto_Bitacora.FindBySector", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.sector = :sector"),
    @NamedQuery(name = "Proyecto_Bitacora.FindBySubsector", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.subsector = :subsector"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByRama", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.rama = :rama"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByTipoEmpleo", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.tipoEmpleo = :tipoEmpleo"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByNoEmpleados", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.noEmpleados = :noEmpleados"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByAsunto", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.asunto = :asunto"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByNoaplicainversion", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.noaplicainversion = :noaplicainversion"),
    @NamedQuery(name = "Proyecto_Bitacora.FindByNoaplicaempleos", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.noaplicaempleos = :noaplicaempleos"),
    @NamedQuery(name = "Proyecto_Bitacora.FindBySintesis", query = "SELECT p FROM Proyecto_Bitacora p WHERE p.sintesis = :sintesis")})
public class Proyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CVE")
    private String cve;
    @Column(name = "NUMERO_BITA")
    private String numeroBita;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "RESOLUCION")
    private Short resolucion;
    @Column(name = "TIPO_PROY")
    private Short tipoProy;
    @Column(name = "ENT_CONTABLE")
    private String entContable;
    @Column(name = "TIPO_MONEDA")
    private String tipoMoneda;
    @Column(name = "INVERSION")
    private String inversion;
    @Column(name = "SECTOR")
    private Short sector;
    @Column(name = "SUBSECTOR")
    private Short subsector;
    @Column(name = "RAMA")
    private Short rama;
    @Column(name = "TIPO_EMPLEO")
    private Character tipoEmpleo;
    @Column(name = "NO_EMPLEADOS")
    private Integer noEmpleados;
    @Column(name = "ASUNTO")
    private Short asunto;
    @Column(name = "NOAPLICAINVERSION")
    private Short noaplicainversion;
    @Column(name = "NOAPLICAEMPLEOS")
    private Short noaplicaempleos;
    @Column(name = "SINTESIS")
    private String sintesis;

    public Proyecto() {
    }

    public Proyecto(String cve) {
        this.cve = cve;
    }

    public Proyecto(String cve, String nombre) {
        this.cve = cve;
        this.nombre = nombre;
    }

    public String getCve() {
        return cve;
    }

    public void setCve(String cve) {
        this.cve = cve;
    }

    public String getNumeroBita() {
        return numeroBita;
    }

    public void setNumeroBita(String numeroBita) {
        this.numeroBita = numeroBita;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Short getResolucion() {
        return resolucion;
    }

    public void setResolucion(Short resolucion) {
        this.resolucion = resolucion;
    }

    public Short getTipoProy() {
        return tipoProy;
    }

    public void setTipoProy(Short tipoProy) {
        this.tipoProy = tipoProy;
    }

    public String getEntContable() {
        return entContable;
    }

    public void setEntContable(String entContable) {
        this.entContable = entContable;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getInversion() {
        return inversion;
    }

    public void setInversion(String inversion) {
        this.inversion = inversion;
    }

    public Short getSector() {
        return sector;
    }

    public void setSector(Short sector) {
        this.sector = sector;
    }

    public Short getSubsector() {
        return subsector;
    }

    public void setSubsector(Short subsector) {
        this.subsector = subsector;
    }

    public Short getRama() {
        return rama;
    }

    public void setRama(Short rama) {
        this.rama = rama;
    }

    public Character getTipoEmpleo() {
        return tipoEmpleo;
    }

    public void setTipoEmpleo(Character tipoEmpleo) {
        this.tipoEmpleo = tipoEmpleo;
    }

    public Integer getNoEmpleados() {
        return noEmpleados;
    }

    public void setNoEmpleados(Integer noEmpleados) {
        this.noEmpleados = noEmpleados;
    }

    public Short getAsunto() {
        return asunto;
    }

    public void setAsunto(Short asunto) {
        this.asunto = asunto;
    }

    public Short getNoaplicainversion() {
        return noaplicainversion;
    }

    public void setNoaplicainversion(Short noaplicainversion) {
        this.noaplicainversion = noaplicainversion;
    }

    public Short getNoaplicaempleos() {
        return noaplicaempleos;
    }

    public void setNoaplicaempleos(Short noaplicaempleos) {
        this.noaplicaempleos = noaplicaempleos;
    }

    public String getSintesis() {
        return sintesis;
    }

    public void setSintesis(String sintesis) {
        this.sintesis = sintesis;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cve != null ? cve.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.cve == null && other.cve != null) || (this.cve != null && !this.cve.equals(other.cve))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Proyecto[ cve=" + cve + " ]";
    }
    
}
