/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.bitacora;



import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Montoya
 */
@Entity
@Table(name = "CADENA_FIRMA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CadenaFirma.findAll", query = "SELECT c FROM CadenaFirma c"),
    @NamedQuery(name = "CadenaFirma.findByClave", query = "SELECT c FROM CadenaFirma c WHERE c.clave = :clave"),
    @NamedQuery(name = "CadenaFirma.findByClaveProyecto", query = "SELECT c FROM CadenaFirma c WHERE c.claveProyecto = :claveProyecto"),
    @NamedQuery(name = "CadenaFirma.findByFolioProyecto", query = "SELECT c FROM CadenaFirma c WHERE c.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "CadenaFirma.findByBitacora", query = "SELECT c FROM CadenaFirma c WHERE c.bitacora = :bitacora"),
    @NamedQuery(name = "CadenaFirma.findByClaveTramite2", query = "SELECT c FROM CadenaFirma c WHERE c.claveTramite2 = :claveTramite2"),
    @NamedQuery(name = "CadenaFirma.findByIdTramite2", query = "SELECT c FROM CadenaFirma c WHERE c.idTramite2 = :idTramite2"),
    @NamedQuery(name = "CadenaFirma.findByTipoDocId", query = "SELECT c FROM CadenaFirma c WHERE c.tipoDocId = :tipoDocId"),
    @NamedQuery(name = "CadenaFirma.findByDocumentoId", query = "SELECT c FROM CadenaFirma c WHERE c.documentoId = :documentoId"),
    @NamedQuery(name = "CadenaFirma.findByIdentificador", query = "SELECT c FROM CadenaFirma c WHERE c.identificador = :identificador"),
    @NamedQuery(name = "CadenaFirma.findByOperacion", query = "SELECT c FROM CadenaFirma c WHERE c.operacion = :operacion"),
    @NamedQuery(name = "CadenaFirma.findByB64", query = "SELECT c FROM CadenaFirma c WHERE c.b64 = :b64")})
public class CadenaFirma implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(generator="InvSeqCF") 
    @SequenceGenerator(name="InvSeqCF",sequenceName="seq_proy_cadena_firma")
    @Basic(optional = false)
    @Column(name = "CLAVE")
    private BigDecimal clave;
    @Basic(optional = false)
    @Column(name = "CLAVE_PROYECTO")
    private String claveProyecto;
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "BITACORA")
    private String bitacora;
    @Basic(optional = false)
    @Column(name = "CLAVE_TRAMITE2")
    private String claveTramite2;
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE2")
    private int idTramite2;
    @Basic(optional = false)
    @Column(name = "TIPO_DOC_ID")
    private short tipoDocId;
    @Basic(optional = false)
    @Column(name = "DOCUMENTO_ID")
    private String documentoId;
    @Basic(optional = false)
    @Column(name = "IDENTIFICADOR")
    private String identificador;
    @Basic(optional = false)
    @Column(name = "OPERACION")
    private String operacion;
    @Column(name = "B64")
    private String b64;

    public CadenaFirma() {
    }

    public CadenaFirma(BigDecimal clave) {
        this.clave = clave;
    }

    public CadenaFirma(BigDecimal clave, String claveProyecto, String folioProyecto, String bitacora, String claveTramite2, int idTramite2, short tipoDocId, String documentoId, String identificador, String operacion) {
        this.clave = clave;
        this.claveProyecto = claveProyecto;
        this.folioProyecto = folioProyecto;
        this.bitacora = bitacora;
        this.claveTramite2 = claveTramite2;
        this.idTramite2 = idTramite2;
        this.tipoDocId = tipoDocId;
        this.documentoId = documentoId;
        this.identificador = identificador;
        this.operacion = operacion;
    }

    public BigDecimal getClave() {
        return clave;
    }

    public void setClave(BigDecimal clave) {
        this.clave = clave;
    }

    public String getClaveProyecto() {
        return claveProyecto;
    }

    public void setClaveProyecto(String claveProyecto) {
        this.claveProyecto = claveProyecto;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getClaveTramite2() {
        return claveTramite2;
    }

    public void setClaveTramite2(String claveTramite2) {
        this.claveTramite2 = claveTramite2;
    }

    public int getIdTramite2() {
        return idTramite2;
    }

    public void setIdTramite2(int idTramite2) {
        this.idTramite2 = idTramite2;
    }

    public short getTipoDocId() {
        return tipoDocId;
    }

    public void setTipoDocId(short tipoDocId) {
        this.tipoDocId = tipoDocId;
    }

    public String getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(String documentoId) {
        this.documentoId = documentoId;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getB64() {
        return b64;
    }

    public void setB64(String b64) {
        this.b64 = b64;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CadenaFirma)) {
            return false;
        }
        CadenaFirma other = (CadenaFirma) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.bitacora.CadenaFirma[ clave=" + clave + " ]";
    }
    
}
    

