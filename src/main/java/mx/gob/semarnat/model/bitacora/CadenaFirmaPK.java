/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.bitacora;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Paul Montoya
 */
@SuppressWarnings("serial")
@Embeddable
public class CadenaFirmaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "CLAVE_PROYECTO")
    private String claveProyecto;
    @Basic(optional = false)
    @Column(name = "BITACORA")
    private String bitacora;
    @Basic(optional = false)
    @Column(name = "CLAVE_TRAMITE2")
    private String claveTramite2;
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE2")
    private int idTramite2;
    @Basic(optional = false)
    @Column(name = "TIPO_DOC_ID")
    private short tipoDocId;
    @Basic(optional = false)
    @Column(name = "DOCUMENTO_ID")
    private String documentoId;

    public CadenaFirmaPK() {
    }

    public CadenaFirmaPK(String claveProyecto, String bitacora, String claveTramite2, int idTramite2, short tipoDocId, String documentoId) {
        this.claveProyecto = claveProyecto;
        this.bitacora = bitacora;
        this.claveTramite2 = claveTramite2;
        this.idTramite2 = idTramite2;
        this.tipoDocId = tipoDocId;
        this.documentoId = documentoId;
    }

    public String getClaveProyecto() {
        return claveProyecto;
    }

    public void setClaveProyecto(String claveProyecto) {
        this.claveProyecto = claveProyecto;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getClaveTramite2() {
        return claveTramite2;
    }

    public void setClaveTramite2(String claveTramite2) {
        this.claveTramite2 = claveTramite2;
    }

    public int getIdTramite2() {
        return idTramite2;
    }

    public void setIdTramite2(int idTramite2) {
        this.idTramite2 = idTramite2;
    }

    public short getTipoDocId() {
        return tipoDocId;
    }

    public void setTipoDocId(short tipoDocId) {
        this.tipoDocId = tipoDocId;
    }

    public String getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(String documentoId) {
        this.documentoId = documentoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claveProyecto != null ? claveProyecto.hashCode() : 0);
        hash += (bitacora != null ? bitacora.hashCode() : 0);
        hash += (claveTramite2 != null ? claveTramite2.hashCode() : 0);
        hash += (int) idTramite2;
        hash += (int) tipoDocId;
        hash += (documentoId != null ? documentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CadenaFirmaPK)) {
            return false;
        }
        CadenaFirmaPK other = (CadenaFirmaPK) object;
        if ((this.claveProyecto == null && other.claveProyecto != null) || (this.claveProyecto != null && !this.claveProyecto.equals(other.claveProyecto))) {
            return false;
        }
        if ((this.bitacora == null && other.bitacora != null) || (this.bitacora != null && !this.bitacora.equals(other.bitacora))) {
            return false;
        }
        if ((this.claveTramite2 == null && other.claveTramite2 != null) || (this.claveTramite2 != null && !this.claveTramite2.equals(other.claveTramite2))) {
            return false;
        }
        if (this.idTramite2 != other.idTramite2) {
            return false;
        }
        if (this.tipoDocId != other.tipoDocId) {
            return false;
        }
        if ((this.documentoId == null && other.documentoId != null) || (this.documentoId != null && !this.documentoId.equals(other.documentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.bitacora.CadenaFirmaPK[ claveProyecto=" + claveProyecto + ", bitacora=" + bitacora + ", claveTramite2=" + claveTramite2 + ", idTramite2=" + idTramite2 + ", tipoDocId=" + tipoDocId + ", documentoId=" + documentoId + " ]";
    }
    
}
