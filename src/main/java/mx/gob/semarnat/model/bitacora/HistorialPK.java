/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.bitacora;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Paty
 */
@Embeddable
public class HistorialPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "HISTO_NUMERO")
    private String histoNumero;
    @Basic(optional = false)
    @Column(name = "HISTO_CONSECUTIV")
    private Integer histoConsecutiv;

    public HistorialPK() {
    }

    public HistorialPK(String histoNumero, Integer histoConsecutiv) {
        this.histoNumero = histoNumero;
        this.histoConsecutiv = histoConsecutiv;
    }

    public String getHistoNumero() {
        return histoNumero;
    }

    public void setHistoNumero(String histoNumero) {
        this.histoNumero = histoNumero;
    }

    public Integer getHistoConsecutiv() {
        return histoConsecutiv;
    }

    public void setHistoConsecutiv(Integer histoConsecutiv) {
        this.histoConsecutiv = histoConsecutiv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (histoNumero != null ? histoNumero.hashCode() : 0);
        hash += (histoConsecutiv != null ? histoConsecutiv.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialPK)) {
            return false;
        }
        HistorialPK other = (HistorialPK) object;
        if ((this.histoNumero == null && other.histoNumero != null) || (this.histoNumero != null && !this.histoNumero.equals(other.histoNumero))) {
            return false;
        }
        if ((this.histoConsecutiv == null && other.histoConsecutiv != null) || (this.histoConsecutiv != null && !this.histoConsecutiv.equals(other.histoConsecutiv))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.HistorialPK[ histoNumero=" + histoNumero + ", histoConsecutiv=" + histoConsecutiv + " ]";
    }
    
}
