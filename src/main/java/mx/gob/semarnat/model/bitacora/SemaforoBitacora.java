package mx.gob.semarnat.model.bitacora;

import java.io.Serializable;
import java.math.BigDecimal;

public class SemaforoBitacora implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2365286859390537110L;
	
	private String numeroBitacora;
	private int diasTramite;
	private int diasProceso;
	private int diasRestantes;
	private int diasRestantesEvaluacion;
	private int diasNotificar;
	private int diasProcesoNotificar;
	private int diasRestantesPorNotificar;
	private int evaluacion;
	private int concluido;
	
	public SemaforoBitacora(Object[] obj) {
				
		this.numeroBitacora = (String)obj[0];
		this.diasTramite = ((BigDecimal)obj[2]).intValue();
		this.diasProceso = ((BigDecimal)obj[3]).intValue();;
		this.diasRestantes = ((BigDecimal)obj[4]).intValue();
		this.diasRestantesEvaluacion = ((BigDecimal)obj[5]).intValue();
		this.diasNotificar = ((BigDecimal)obj[6]).intValue();;
		this.diasProcesoNotificar = ((BigDecimal)obj[7]).intValue();;
		this.diasRestantesPorNotificar = ((BigDecimal)obj[8]).intValue();
		this.evaluacion = ((BigDecimal)obj[11]).intValue();
		this.concluido = ((BigDecimal)obj[12]).intValue();
    }
	
	
	public String getNumeroBitacora() {
		return numeroBitacora;
	}
	public void setNumeroBitacora(String numeroBitacora) {
		this.numeroBitacora = numeroBitacora;
	}
	public int getDiasTramite() {
		return diasTramite;
	}
	public void setDiasTramite(int diasTramite) {
		this.diasTramite = diasTramite;
	}
	public int getDiasProceso() {
		return diasProceso;
	}
	public void setDiasProceso(int diasProceso) {
		this.diasProceso = diasProceso;
	}
	public int getDiasRestantes() {
		return diasRestantes;
	}
	public void setDiasRestantes(int diasRestantes) {
		this.diasRestantes = diasRestantes;
	}
	public int getDiasRestantesEvaluacion() {
		return diasRestantesEvaluacion;
	}
	public void setDiasRestantesEvaluacion(int diasRestantesEvaluacion) {
		this.diasRestantesEvaluacion = diasRestantesEvaluacion;
	}
	public int getDiasNotificar() {
		return diasNotificar;
	}
	public void setDiasNotificar(int diasNotificar) {
		this.diasNotificar = diasNotificar;
	}
	public int getDiasProcesoNotificar() {
		return diasProcesoNotificar;
	}
	public void setDiasProcesoNotificar(int diasProcesoNotificar) {
		this.diasProcesoNotificar = diasProcesoNotificar;
	}
	public int getDiasRestantesPorNotificar() {
		return diasRestantesPorNotificar;
	}
	public void setDiasRestantesPorNotificar(int diasRestantesPorNotificar) {
		this.diasRestantesPorNotificar = diasRestantesPorNotificar;
	}
	public int getEvaluacion() {
		return evaluacion;
	}
	public void setEvaluacion(int evaluacion) {
		this.evaluacion = evaluacion;
	}
	public int getConcluido() {
		return concluido;
	}
	public void setConcluido(int concluido) {
		this.concluido = concluido;
	}
	
	
	
}
