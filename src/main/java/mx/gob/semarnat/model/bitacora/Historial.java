/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.bitacora;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "HISTORIAL", schema="BITACORA")
@XmlRootElement
@NamedQueries({
	//Queries
	@NamedQuery(name = "Historial.findByBitacoraBySituacion", query = "SELECT h FROM Historial h, Bitacora b WHERE h.historialPK.histoNumero = ?1 AND h.histoSituacionActual = ?2 AND b.bitaNumero = h.historialPK.histoNumero"),
	//Fin Queries
    @NamedQuery(name = "Historial.findAll", query = "SELECT h FROM Historial h"),
    @NamedQuery(name = "Historial.findByHistoNumero", query = "SELECT h FROM Historial h WHERE h.historialPK.histoNumero = :histoNumero"),
    @NamedQuery(name = "Historial.findByHistoNumeroSit", query = "SELECT h FROM Historial h WHERE h.historialPK.histoNumero = :histoNumero AND h.histoSituacionActual = :histoSituacionActual"),
    @NamedQuery(name = "Historial.findByHistoConsecutiv", query = "SELECT h FROM Historial h WHERE h.historialPK.histoConsecutiv = :histoConsecutiv"),
    @NamedQuery(name = "Historial.findByHistoFlujo", query = "SELECT h FROM Historial h WHERE h.histoFlujo = :histoFlujo"),
    @NamedQuery(name = "Historial.findByHistoClaveTramite", query = "SELECT h FROM Historial h WHERE h.histoClaveTramite = :histoClaveTramite"),
    @NamedQuery(name = "Historial.findByHistoIdTramite", query = "SELECT h FROM Historial h WHERE h.histoIdTramite = :histoIdTramite"),
    @NamedQuery(name = "Historial.findByHistoSituacionActual", query = "SELECT h FROM Historial h WHERE h.histoSituacionActual = :histoSituacionActual"),
    @NamedQuery(name = "Historial.findByHistoFecharec", query = "SELECT h FROM Historial h WHERE h.histoFecharec = :histoFecharec"),
    @NamedQuery(name = "Historial.findByHistoFechatur", query = "SELECT h FROM Historial h WHERE h.histoFechatur = :histoFechatur"),
    @NamedQuery(name = "Historial.findByHistoAreaEnvio", query = "SELECT h FROM Historial h WHERE h.histoAreaEnvio = :histoAreaEnvio"),
    @NamedQuery(name = "Historial.findByHistoAreaRecibe", query = "SELECT h FROM Historial h WHERE h.histoAreaRecibe = :histoAreaRecibe"),
    @NamedQuery(name = "Historial.findByHistoPmoralRecibe", query = "SELECT h FROM Historial h WHERE h.histoPmoralRecibe = :histoPmoralRecibe"),
    @NamedQuery(name = "Historial.findByHistoPmoralEnvio", query = "SELECT h FROM Historial h WHERE h.histoPmoralEnvio = :histoPmoralEnvio"),
    @NamedQuery(name = "Historial.findByHistoIdUsuarioEnvio", query = "SELECT h FROM Historial h WHERE h.histoIdUsuarioEnvio = :histoIdUsuarioEnvio"),
    @NamedQuery(name = "Historial.findByHistoIdUsuarioRecibe", query = "SELECT h FROM Historial h WHERE h.histoIdUsuarioRecibe = :histoIdUsuarioRecibe"),
    @NamedQuery(name = "Historial.findByHistoRespEnvio", query = "SELECT h FROM Historial h WHERE h.histoRespEnvio = :histoRespEnvio"),
    @NamedQuery(name = "Historial.findByHistoRespRecibe", query = "SELECT h FROM Historial h WHERE h.histoRespRecibe = :histoRespRecibe"),
    @NamedQuery(name = "Historial.findByHistoGestion", query = "SELECT h FROM Historial h WHERE h.histoGestion = :histoGestion"),
    @NamedQuery(name = "Historial.findByHistoDiasTramite", query = "SELECT h FROM Historial h WHERE h.histoDiasTramite = :histoDiasTramite"),
    @NamedQuery(name = "Historial.findByHistoDiasProceso", query = "SELECT h FROM Historial h WHERE h.histoDiasProceso = :histoDiasProceso"),
    @NamedQuery(name = "Historial.findByHistoNotificar", query = "SELECT h FROM Historial h WHERE h.histoNotificar = :histoNotificar"),
    @NamedQuery(name = "Historial.findByHistoDiasNotificar", query = "SELECT h FROM Historial h WHERE h.histoDiasNotificar = :histoDiasNotificar"),
    @NamedQuery(name = "Historial.findByHistoDiasProcesoNotificar", query = "SELECT h FROM Historial h WHERE h.histoDiasProcesoNotificar = :histoDiasProcesoNotificar"),
    @NamedQuery(name = "Historial.findByHistoNotificado", query = "SELECT h FROM Historial h WHERE h.histoNotificado = :histoNotificado"),
    @NamedQuery(name = "Historial.findByHistoDiasNotificado", query = "SELECT h FROM Historial h WHERE h.histoDiasNotificado = :histoDiasNotificado"),
    @NamedQuery(name = "Historial.findByHistoDiasProcesoNotificado", query = "SELECT h FROM Historial h WHERE h.histoDiasProcesoNotificado = :histoDiasProcesoNotificado"),
    @NamedQuery(name = "Historial.findByHistoSuspension", query = "SELECT h FROM Historial h WHERE h.histoSuspension = :histoSuspension"),
    @NamedQuery(name = "Historial.findByHistoConcluido", query = "SELECT h FROM Historial h WHERE h.histoConcluido = :histoConcluido"),
    @NamedQuery(name = "Historial.findByCreacion", query = "SELECT h FROM Historial h WHERE h.creacion = :creacion"),
    @NamedQuery(name = "Historial.findByHistoObserva", query = "SELECT h FROM Historial h WHERE h.histoObserva = :histoObserva"),
    @NamedQuery(name = "Historial.findByHistoCveDocto", query = "SELECT h FROM Historial h WHERE h.histoCveDocto = :histoCveDocto"),
    @NamedQuery(name = "Historial.findByHistoFechRegistro", query = "SELECT h FROM Historial h WHERE h.histoFechRegistro = :histoFechRegistro"),
    @NamedQuery(name = "Historial.findByHistoFechaprogramada", query = "SELECT h FROM Historial h WHERE h.histoFechaprogramada = :histoFechaprogramada")})
public class Historial implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HistorialPK historialPK;
    @Basic(optional = false)
    @Column(name = "HISTO_FLUJO")
    private int histoFlujo;
    @Basic(optional = false)
    @Column(name = "HISTO_CLAVE_TRAMITE")
    private String histoClaveTramite;
    @Basic(optional = false)
    @Column(name = "HISTO_ID_TRAMITE")
    private int histoIdTramite;
    @Basic(optional = false)
    @Column(name = "HISTO_SITUACION_ACTUAL")
    private String histoSituacionActual;
    @Column(name = "HISTO_FECHAREC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date histoFecharec;
    @Column(name = "HISTO_FECHATUR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date histoFechatur;
    @Column(name = "HISTO_AREA_ENVIO")
    private String histoAreaEnvio;
    @Column(name = "HISTO_AREA_RECIBE")
    private String histoAreaRecibe;
    @Column(name = "HISTO_PMORAL_RECIBE")
    private String histoPmoralRecibe;
    @Column(name = "HISTO_PMORAL_ENVIO")
    private String histoPmoralEnvio;
    @Column(name = "HISTO_ID_USUARIO_ENVIO")
    private Integer histoIdUsuarioEnvio;
    @Column(name = "HISTO_ID_USUARIO_RECIBE")
    private Integer histoIdUsuarioRecibe;
    @Column(name = "HISTO_RESP_ENVIO")
    private String histoRespEnvio;
    @Column(name = "HISTO_RESP_RECIBE")
    private String histoRespRecibe;
    @Basic(optional = false)
    @Column(name = "HISTO_GESTION")
    private short histoGestion;
    @Column(name = "HISTO_DIAS_TRAMITE")
    private Integer histoDiasTramite;
    @Column(name = "HISTO_DIAS_PROCESO")
    private Long histoDiasProceso;
    @Basic(optional = false)
    @Column(name = "HISTO_NOTIFICAR")
    private short histoNotificar;
    @Basic(optional = false)
    @Column(name = "HISTO_DIAS_NOTIFICAR")
    private int histoDiasNotificar;
    @Basic(optional = false)
    @Column(name = "HISTO_DIAS_PROCESO_NOTIFICAR")
    private int histoDiasProcesoNotificar;
    @Basic(optional = false)
    @Column(name = "HISTO_NOTIFICADO")
    private short histoNotificado;
    @Basic(optional = false)
    @Column(name = "HISTO_DIAS_NOTIFICADO")
    private int histoDiasNotificado;
    @Basic(optional = false)
    @Column(name = "HISTO_DIAS_PROCESO_NOTIFICADO")
    private int histoDiasProcesoNotificado;
    @Basic(optional = false)
    @Column(name = "HISTO_SUSPENSION")
    private short histoSuspension;
    @Basic(optional = false)
    @Column(name = "HISTO_CONCLUIDO")
    private short histoConcluido;
    @Basic(optional = false)
    @Column(name = "CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creacion;
    @Column(name = "HISTO_OBSERVA")
    private String histoObserva;
    @Column(name = "HISTO_CVE_DOCTO")
    private String histoCveDocto;
    @Basic(optional = false)
    @Column(name = "HISTO_FECH_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date histoFechRegistro;
    @Column(name = "HISTO_FECHAPROGRAMADA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date histoFechaprogramada;

    public Historial() {
    }

    public Historial(HistorialPK historialPK) {
        this.historialPK = historialPK;
    }

    public Historial(HistorialPK historialPK, int histoFlujo, String histoClaveTramite, int histoIdTramite, String histoSituacionActual, short histoGestion, short histoNotificar, int histoDiasNotificar, int histoDiasProcesoNotificar, short histoNotificado, int histoDiasNotificado, int histoDiasProcesoNotificado, short histoSuspension, short histoConcluido, Date creacion, Date histoFechRegistro) {
        this.historialPK = historialPK;
        this.histoFlujo = histoFlujo;
        this.histoClaveTramite = histoClaveTramite;
        this.histoIdTramite = histoIdTramite;
        this.histoSituacionActual = histoSituacionActual;
        this.histoGestion = histoGestion;
        this.histoNotificar = histoNotificar;
        this.histoDiasNotificar = histoDiasNotificar;
        this.histoDiasProcesoNotificar = histoDiasProcesoNotificar;
        this.histoNotificado = histoNotificado;
        this.histoDiasNotificado = histoDiasNotificado;
        this.histoDiasProcesoNotificado = histoDiasProcesoNotificado;
        this.histoSuspension = histoSuspension;
        this.histoConcluido = histoConcluido;
        this.creacion = creacion;
        this.histoFechRegistro = histoFechRegistro;
    }

    public Historial(String histoNumero, Integer histoConsecutiv) {
        this.historialPK = new HistorialPK(histoNumero, histoConsecutiv);
    }

    public HistorialPK getHistorialPK() {
        return historialPK;
    }

    public void setHistorialPK(HistorialPK historialPK) {
        this.historialPK = historialPK;
    }

    public int getHistoFlujo() {
        return histoFlujo;
    }

    public void setHistoFlujo(int histoFlujo) {
        this.histoFlujo = histoFlujo;
    }

    public String getHistoClaveTramite() {
        return histoClaveTramite;
    }

    public void setHistoClaveTramite(String histoClaveTramite) {
        this.histoClaveTramite = histoClaveTramite;
    }

    public int getHistoIdTramite() {
        return histoIdTramite;
    }

    public void setHistoIdTramite(int histoIdTramite) {
        this.histoIdTramite = histoIdTramite;
    }

    public String getHistoSituacionActual() {
        return histoSituacionActual;
    }

    public void setHistoSituacionActual(String histoSituacionActual) {
        this.histoSituacionActual = histoSituacionActual;
    }

    public Date getHistoFecharec() {
        return histoFecharec;
    }

    public void setHistoFecharec(Date histoFecharec) {
        this.histoFecharec = histoFecharec;
    }

    public Date getHistoFechatur() {
        return histoFechatur;
    }

    public void setHistoFechatur(Date histoFechatur) {
        this.histoFechatur = histoFechatur;
    }

    public String getHistoAreaEnvio() {
        return histoAreaEnvio;
    }

    public void setHistoAreaEnvio(String histoAreaEnvio) {
        this.histoAreaEnvio = histoAreaEnvio;
    }

    public String getHistoAreaRecibe() {
        return histoAreaRecibe;
    }

    public void setHistoAreaRecibe(String histoAreaRecibe) {
        this.histoAreaRecibe = histoAreaRecibe;
    }

    public String getHistoPmoralRecibe() {
        return histoPmoralRecibe;
    }

    public void setHistoPmoralRecibe(String histoPmoralRecibe) {
        this.histoPmoralRecibe = histoPmoralRecibe;
    }

    public String getHistoPmoralEnvio() {
        return histoPmoralEnvio;
    }

    public void setHistoPmoralEnvio(String histoPmoralEnvio) {
        this.histoPmoralEnvio = histoPmoralEnvio;
    }

    public Integer getHistoIdUsuarioEnvio() {
        return histoIdUsuarioEnvio;
    }

    public void setHistoIdUsuarioEnvio(Integer histoIdUsuarioEnvio) {
        this.histoIdUsuarioEnvio = histoIdUsuarioEnvio;
    }

    public Integer getHistoIdUsuarioRecibe() {
        return histoIdUsuarioRecibe;
    }

    public void setHistoIdUsuarioRecibe(Integer histoIdUsuarioRecibe) {
        this.histoIdUsuarioRecibe = histoIdUsuarioRecibe;
    }

    public String getHistoRespEnvio() {
        return histoRespEnvio;
    }

    public void setHistoRespEnvio(String histoRespEnvio) {
        this.histoRespEnvio = histoRespEnvio;
    }

    public String getHistoRespRecibe() {
        return histoRespRecibe;
    }

    public void setHistoRespRecibe(String histoRespRecibe) {
        this.histoRespRecibe = histoRespRecibe;
    }

    public short getHistoGestion() {
        return histoGestion;
    }

    public void setHistoGestion(short histoGestion) {
        this.histoGestion = histoGestion;
    }

    public Integer getHistoDiasTramite() {
        return histoDiasTramite;
    }

    public void setHistoDiasTramite(Integer histoDiasTramite) {
        this.histoDiasTramite = histoDiasTramite;
    }

    public Long getHistoDiasProceso() {
        return histoDiasProceso;
    }

    public void setHistoDiasProceso(Long histoDiasProceso) {
        this.histoDiasProceso = histoDiasProceso;
    }

    public short getHistoNotificar() {
        return histoNotificar;
    }

    public void setHistoNotificar(short histoNotificar) {
        this.histoNotificar = histoNotificar;
    }

    public int getHistoDiasNotificar() {
        return histoDiasNotificar;
    }

    public void setHistoDiasNotificar(int histoDiasNotificar) {
        this.histoDiasNotificar = histoDiasNotificar;
    }

    public int getHistoDiasProcesoNotificar() {
        return histoDiasProcesoNotificar;
    }

    public void setHistoDiasProcesoNotificar(int histoDiasProcesoNotificar) {
        this.histoDiasProcesoNotificar = histoDiasProcesoNotificar;
    }

    public short getHistoNotificado() {
        return histoNotificado;
    }

    public void setHistoNotificado(short histoNotificado) {
        this.histoNotificado = histoNotificado;
    }

    public int getHistoDiasNotificado() {
        return histoDiasNotificado;
    }

    public void setHistoDiasNotificado(int histoDiasNotificado) {
        this.histoDiasNotificado = histoDiasNotificado;
    }

    public int getHistoDiasProcesoNotificado() {
        return histoDiasProcesoNotificado;
    }

    public void setHistoDiasProcesoNotificado(int histoDiasProcesoNotificado) {
        this.histoDiasProcesoNotificado = histoDiasProcesoNotificado;
    }

    public short getHistoSuspension() {
        return histoSuspension;
    }

    public void setHistoSuspension(short histoSuspension) {
        this.histoSuspension = histoSuspension;
    }

    public short getHistoConcluido() {
        return histoConcluido;
    }

    public void setHistoConcluido(short histoConcluido) {
        this.histoConcluido = histoConcluido;
    }

    public Date getCreacion() {
        return creacion;
    }

    public void setCreacion(Date creacion) {
        this.creacion = creacion;
    }

    public String getHistoObserva() {
        return histoObserva;
    }

    public void setHistoObserva(String histoObserva) {
        this.histoObserva = histoObserva;
    }

    public String getHistoCveDocto() {
        return histoCveDocto;
    }

    public void setHistoCveDocto(String histoCveDocto) {
        this.histoCveDocto = histoCveDocto;
    }

    public Date getHistoFechRegistro() {
        return histoFechRegistro;
    }

    public void setHistoFechRegistro(Date histoFechRegistro) {
        this.histoFechRegistro = histoFechRegistro;
    }

    public Date getHistoFechaprogramada() {
        return histoFechaprogramada;
    }

    public void setHistoFechaprogramada(Date histoFechaprogramada) {
        this.histoFechaprogramada = histoFechaprogramada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialPK != null ? historialPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Historial)) {
            return false;
        }
        Historial other = (Historial) object;
        if ((this.historialPK == null && other.historialPK != null) || (this.historialPK != null && !this.historialPK.equals(other.historialPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.Historial[ historialPK=" + historialPK + " ]";
    }
    
}
