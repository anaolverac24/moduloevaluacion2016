/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "TBSITUACION", schema="CATALOGO_FLUJOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tbsituacion.findAll", query = "SELECT t FROM Tbsituacion t"),
    @NamedQuery(name = "Tbsituacion.findByIdsituacion", query = "SELECT t FROM Tbsituacion t WHERE t.idsituacion = :idsituacion"),
    @NamedQuery(name = "Tbsituacion.findByDescripcion", query = "SELECT t FROM Tbsituacion t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "Tbsituacion.findByTextoTitulo", query = "SELECT t FROM Tbsituacion t WHERE t.textoTitulo = :textoTitulo"),
    @NamedQuery(name = "Tbsituacion.findByTextoBoton", query = "SELECT t FROM Tbsituacion t WHERE t.textoBoton = :textoBoton"),
    @NamedQuery(name = "Tbsituacion.findByResolucion", query = "SELECT t FROM Tbsituacion t WHERE t.resolucion = :resolucion"),
    @NamedQuery(name = "Tbsituacion.findByDetieneReloj", query = "SELECT t FROM Tbsituacion t WHERE t.detieneReloj = :detieneReloj"),
    @NamedQuery(name = "Tbsituacion.findByAmpliacion", query = "SELECT t FROM Tbsituacion t WHERE t.ampliacion = :ampliacion"),
    @NamedQuery(name = "Tbsituacion.findByIntegraExpediente", query = "SELECT t FROM Tbsituacion t WHERE t.integraExpediente = :integraExpediente"),
    @NamedQuery(name = "Tbsituacion.findByNotificacion", query = "SELECT t FROM Tbsituacion t WHERE t.notificacion = :notificacion"),
    @NamedQuery(name = "Tbsituacion.findByInformacionAdicional", query = "SELECT t FROM Tbsituacion t WHERE t.informacionAdicional = :informacionAdicional"),
    @NamedQuery(name = "Tbsituacion.findByInformacionFaltante", query = "SELECT t FROM Tbsituacion t WHERE t.informacionFaltante = :informacionFaltante"),
    @NamedQuery(name = "Tbsituacion.findByRequiereIfai", query = "SELECT t FROM Tbsituacion t WHERE t.requiereIfai = :requiereIfai"),
    @NamedQuery(name = "Tbsituacion.findByRequiereSat", query = "SELECT t FROM Tbsituacion t WHERE t.requiereSat = :requiereSat"),
    @NamedQuery(name = "Tbsituacion.findByInicialFinal", query = "SELECT t FROM Tbsituacion t WHERE t.inicialFinal = :inicialFinal"),
    @NamedQuery(name = "Tbsituacion.findByEstatus", query = "SELECT t FROM Tbsituacion t WHERE t.estatus = :estatus"),
    @NamedQuery(name = "Tbsituacion.findByEvaluacion", query = "SELECT t FROM Tbsituacion t WHERE t.evaluacion = :evaluacion"),
    @NamedQuery(name = "Tbsituacion.findByDatosProyecto", query = "SELECT t FROM Tbsituacion t WHERE t.datosProyecto = :datosProyecto"),
    @NamedQuery(name = "Tbsituacion.findByIdtiposuspension", query = "SELECT t FROM Tbsituacion t WHERE t.idtiposuspension = :idtiposuspension")})
public class Tbsituacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IDSITUACION")
    private String idsituacion;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "TEXTO_TITULO")
    private String textoTitulo;
    @Column(name = "TEXTO_BOTON")
    private String textoBoton;
    @Basic(optional = false)
    @Column(name = "RESOLUCION")
    private Character resolucion;
    @Basic(optional = false)
    @Column(name = "DETIENE_RELOJ")
    private Character detieneReloj;
    @Basic(optional = false)
    @Column(name = "AMPLIACION")
    private Character ampliacion;
    @Basic(optional = false)
    @Column(name = "INTEGRA_EXPEDIENTE")
    private Character integraExpediente;
    @Basic(optional = false)
    @Column(name = "NOTIFICACION")
    private Character notificacion;
    @Basic(optional = false)
    @Column(name = "INFORMACION_ADICIONAL")
    private Character informacionAdicional;
    @Basic(optional = false)
    @Column(name = "INFORMACION_FALTANTE")
    private Character informacionFaltante;
    @Basic(optional = false)
    @Column(name = "REQUIERE_IFAI")
    private Character requiereIfai;
    @Basic(optional = false)
    @Column(name = "REQUIERE_SAT")
    private Character requiereSat;
    @Basic(optional = false)
    @Column(name = "INICIAL_FINAL")
    private BigInteger inicialFinal;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Column(name = "EVALUACION")
    private Character evaluacion;
    @Column(name = "DATOS_PROYECTO")
    private Short datosProyecto;
    @Column(name = "IDTIPOSUSPENSION")
    private Short idtiposuspension;
    //@JoinColumn(name = "IDSITUACIONGENERAL", referencedColumnName = "IDSITUACIONGENERAL")
    //@ManyToOne(optional = false)
    @Column(name = "IDSITUACIONGENERAL")
    private Integer idsituaciongeneral;

    public Tbsituacion() {
    }

    public Tbsituacion(String idsituacion) {
        this.idsituacion = idsituacion;
    }

    public Tbsituacion(String idsituacion, String descripcion, Character resolucion, Character detieneReloj, Character ampliacion, Character integraExpediente, Character notificacion, Character informacionAdicional, Character informacionFaltante, Character requiereIfai, Character requiereSat, BigInteger inicialFinal, BigInteger estatus) {
        this.idsituacion = idsituacion;
        this.descripcion = descripcion;
        this.resolucion = resolucion;
        this.detieneReloj = detieneReloj;
        this.ampliacion = ampliacion;
        this.integraExpediente = integraExpediente;
        this.notificacion = notificacion;
        this.informacionAdicional = informacionAdicional;
        this.informacionFaltante = informacionFaltante;
        this.requiereIfai = requiereIfai;
        this.requiereSat = requiereSat;
        this.inicialFinal = inicialFinal;
        this.estatus = estatus;
    }

    public String getIdsituacion() {
        return idsituacion;
    }

    public void setIdsituacion(String idsituacion) {
        this.idsituacion = idsituacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTextoTitulo() {
        return textoTitulo;
    }

    public void setTextoTitulo(String textoTitulo) {
        this.textoTitulo = textoTitulo;
    }

    public String getTextoBoton() {
        return textoBoton;
    }

    public void setTextoBoton(String textoBoton) {
        this.textoBoton = textoBoton;
    }

    public Character getResolucion() {
        return resolucion;
    }

    public void setResolucion(Character resolucion) {
        this.resolucion = resolucion;
    }

    public Character getDetieneReloj() {
        return detieneReloj;
    }

    public void setDetieneReloj(Character detieneReloj) {
        this.detieneReloj = detieneReloj;
    }

    public Character getAmpliacion() {
        return ampliacion;
    }

    public void setAmpliacion(Character ampliacion) {
        this.ampliacion = ampliacion;
    }

    public Character getIntegraExpediente() {
        return integraExpediente;
    }

    public void setIntegraExpediente(Character integraExpediente) {
        this.integraExpediente = integraExpediente;
    }

    public Character getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(Character notificacion) {
        this.notificacion = notificacion;
    }

    public Character getInformacionAdicional() {
        return informacionAdicional;
    }

    public void setInformacionAdicional(Character informacionAdicional) {
        this.informacionAdicional = informacionAdicional;
    }

    public Character getInformacionFaltante() {
        return informacionFaltante;
    }

    public void setInformacionFaltante(Character informacionFaltante) {
        this.informacionFaltante = informacionFaltante;
    }

    public Character getRequiereIfai() {
        return requiereIfai;
    }

    public void setRequiereIfai(Character requiereIfai) {
        this.requiereIfai = requiereIfai;
    }

    public Character getRequiereSat() {
        return requiereSat;
    }

    public void setRequiereSat(Character requiereSat) {
        this.requiereSat = requiereSat;
    }

    public BigInteger getInicialFinal() {
        return inicialFinal;
    }

    public void setInicialFinal(BigInteger inicialFinal) {
        this.inicialFinal = inicialFinal;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public Character getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Character evaluacion) {
        this.evaluacion = evaluacion;
    }

    public Short getDatosProyecto() {
        return datosProyecto;
    }

    public void setDatosProyecto(Short datosProyecto) {
        this.datosProyecto = datosProyecto;
    }

    public Short getIdtiposuspension() {
        return idtiposuspension;
    }

    public void setIdtiposuspension(Short idtiposuspension) {
        this.idtiposuspension = idtiposuspension;
    }

    public Integer getIdsituaciongeneral() {
        return idsituaciongeneral;
    }

    public void setIdsituaciongeneral(Integer idsituaciongeneral) {
        this.idsituaciongeneral = idsituaciongeneral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsituacion != null ? idsituacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tbsituacion)) {
            return false;
        }
        Tbsituacion other = (Tbsituacion) object;
        if ((this.idsituacion == null && other.idsituacion != null) || (this.idsituacion != null && !this.idsituacion.equals(other.idsituacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Tbsituacion[ idsituacion=" + idsituacion + " ]";
    }
    
}
