/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "TBEMPLEADO", schema="SEGURIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tbempleado.findAll", query = "SELECT t FROM Tbempleado t"),
    @NamedQuery(name = "Tbempleado.findByIdempleado", query = "SELECT t FROM Tbempleado t WHERE t.idempleado = :idempleado"),
    @NamedQuery(name = "Tbempleado.findByNombre", query = "SELECT t FROM Tbempleado t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "Tbempleado.findByApellidopaterno", query = "SELECT t FROM Tbempleado t WHERE t.apellidopaterno = :apellidopaterno"),
    @NamedQuery(name = "Tbempleado.findByApellidomaterno", query = "SELECT t FROM Tbempleado t WHERE t.apellidomaterno = :apellidomaterno"),
    @NamedQuery(name = "Tbempleado.findByRfc", query = "SELECT t FROM Tbempleado t WHERE t.rfc = :rfc"),
    @NamedQuery(name = "Tbempleado.findByCurp", query = "SELECT t FROM Tbempleado t WHERE t.curp = :curp"),
    @NamedQuery(name = "Tbempleado.findByCorreo", query = "SELECT t FROM Tbempleado t WHERE t.correo = :correo"),
    @NamedQuery(name = "Tbempleado.findByExt", query = "SELECT t FROM Tbempleado t WHERE t.ext = :ext"),
    @NamedQuery(name = "Tbempleado.findByEstatus", query = "SELECT t FROM Tbempleado t WHERE t.estatus = :estatus"),
    @NamedQuery(name = "Tbempleado.findByNumeroEmpleado", query = "SELECT t FROM Tbempleado t WHERE t.numeroEmpleado = :numeroEmpleado"),
    @NamedQuery(name = "Tbempleado.findByCargo", query = "SELECT t FROM Tbempleado t WHERE t.cargo = :cargo"),
    @NamedQuery(name = "Tbempleado.findByValidado", query = "SELECT t FROM Tbempleado t WHERE t.validado = :validado")})
public class Tbempleado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IDEMPLEADO")
    private String idempleado;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "APELLIDOPATERNO")
    private String apellidopaterno;
    @Column(name = "APELLIDOMATERNO")
    private String apellidomaterno;
    @Basic(optional = false)
    @Column(name = "RFC")
    private String rfc;
    @Column(name = "CURP")
    private String curp;
    @Column(name = "CORREO")
    private String correo;
    @Column(name = "EXT")
    private String ext;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Column(name = "NUMERO_EMPLEADO")
    private BigInteger numeroEmpleado;
    @Column(name = "CARGO")
    private String cargo;
    @Column(name = "VALIDADO")
    private BigInteger validado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idempleado")
    private Collection<Tbusuario> tbusuarioCollection;

    public Tbempleado() {
    }

    public Tbempleado(String idempleado) {
        this.idempleado = idempleado;
    }

    public Tbempleado(String idempleado, String nombre, String apellidopaterno, String rfc, BigInteger estatus) {
        this.idempleado = idempleado;
        this.nombre = nombre;
        this.apellidopaterno = apellidopaterno;
        this.rfc = rfc;
        this.estatus = estatus;
    }

    public String getIdempleado() {
        return idempleado;
    }

    public void setIdempleado(String idempleado) {
        this.idempleado = idempleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidopaterno() {
        return apellidopaterno;
    }

    public void setApellidopaterno(String apellidopaterno) {
        this.apellidopaterno = apellidopaterno;
    }

    public String getApellidomaterno() {
        return apellidomaterno;
    }

    public void setApellidomaterno(String apellidomaterno) {
        this.apellidomaterno = apellidomaterno;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public BigInteger getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(BigInteger numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public BigInteger getValidado() {
        return validado;
    }

    public void setValidado(BigInteger validado) {
        this.validado = validado;
    }

    @XmlTransient
    public Collection<Tbusuario> getTbusuarioCollection() {
        return tbusuarioCollection;
    }

    public void setTbusuarioCollection(Collection<Tbusuario> tbusuarioCollection) {
        this.tbusuarioCollection = tbusuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idempleado != null ? idempleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tbempleado)) {
            return false;
        }
        Tbempleado other = (Tbempleado) object;
        if ((this.idempleado == null && other.idempleado != null) || (this.idempleado != null && !this.idempleado.equals(other.idempleado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Tbempleado[ idempleado=" + idempleado + " ]";
    }
    
}
