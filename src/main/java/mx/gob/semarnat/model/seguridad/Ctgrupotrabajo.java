/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "CTGRUPOTRABAJO", schema="SEGURIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ctgrupotrabajo.findAll", query = "SELECT c FROM Ctgrupotrabajo c"),
    @NamedQuery(name = "Ctgrupotrabajo.findByIdgrupotrabajo", query = "SELECT c FROM Ctgrupotrabajo c WHERE c.idgrupotrabajo = :idgrupotrabajo"),
    @NamedQuery(name = "Ctgrupotrabajo.findByGrupotrabajo", query = "SELECT c FROM Ctgrupotrabajo c WHERE c.grupotrabajo = :grupotrabajo"),
    @NamedQuery(name = "Ctgrupotrabajo.findByEstatus", query = "SELECT c FROM Ctgrupotrabajo c WHERE c.estatus = :estatus")})
public class Ctgrupotrabajo implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDGRUPOTRABAJO")
    private BigDecimal idgrupotrabajo;
    @Basic(optional = false)
    @Column(name = "GRUPOTRABAJO")
    private String grupotrabajo;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @OneToMany(mappedBy = "idgrupotrabajo")
    private Collection<Tbarea> tbareaCollection;

    public Ctgrupotrabajo() {
    }

    public Ctgrupotrabajo(BigDecimal idgrupotrabajo) {
        this.idgrupotrabajo = idgrupotrabajo;
    }

    public Ctgrupotrabajo(BigDecimal idgrupotrabajo, String grupotrabajo, BigInteger estatus) {
        this.idgrupotrabajo = idgrupotrabajo;
        this.grupotrabajo = grupotrabajo;
        this.estatus = estatus;
    }

    public BigDecimal getIdgrupotrabajo() {
        return idgrupotrabajo;
    }

    public void setIdgrupotrabajo(BigDecimal idgrupotrabajo) {
        this.idgrupotrabajo = idgrupotrabajo;
    }

    public String getGrupotrabajo() {
        return grupotrabajo;
    }

    public void setGrupotrabajo(String grupotrabajo) {
        this.grupotrabajo = grupotrabajo;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    @XmlTransient
    public Collection<Tbarea> getTbareaCollection() {
        return tbareaCollection;
    }

    public void setTbareaCollection(Collection<Tbarea> tbareaCollection) {
        this.tbareaCollection = tbareaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgrupotrabajo != null ? idgrupotrabajo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ctgrupotrabajo)) {
            return false;
        }
        Ctgrupotrabajo other = (Ctgrupotrabajo) object;
        if ((this.idgrupotrabajo == null && other.idgrupotrabajo != null) || (this.idgrupotrabajo != null && !this.idgrupotrabajo.equals(other.idgrupotrabajo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Ctgrupotrabajo[ idgrupotrabajo=" + idgrupotrabajo + " ]";
    }
    
}
