package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.inject.Default;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "TOKENGO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({ 
	@NamedQuery(name = "TokenGO.findByIdUsiario", query = "SELECT t FROM TokenGO t WHERE t.idUsuario = ?1")
})
public class TokenGO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Basic(optional = false)
	@Column(name = "ID_USUARIO")
	private Integer idUsuario;
	
	@Basic(optional = false)
	@Column(name = "TOKEN")
	private String token;
	
	@Basic(optional = true)
    @Column(name = "FECHA_CREACION",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

	public TokenGO() {
		
	}
	
	public TokenGO(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public TokenGO(Integer idUsuario, String token) {
		this.idUsuario = idUsuario;
		this.token = token;
	}
	
	public TokenGO(Integer idUsuario, String token, Date fechaCreacion) {
		this.idUsuario = idUsuario;
		this.token = token;
		this.fechaCreacion = fechaCreacion;
	}
	
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	
}
