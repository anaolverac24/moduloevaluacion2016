/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Paty
 */
@SuppressWarnings("serial")
@Embeddable
public class TbareaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "IDDEPENDENCIA")
    private String iddependencia;
    @Basic(optional = false)
    @Column(name = "IDAREA")
    private String idarea;

    public TbareaPK() {
    }

    public TbareaPK(String iddependencia, String idarea) {
        this.iddependencia = iddependencia;
        this.idarea = idarea;
    }

    public String getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(String iddependencia) {
        this.iddependencia = iddependencia;
    }

    public String getIdarea() {
        return idarea;
    }

    public void setIdarea(String idarea) {
        this.idarea = idarea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddependencia != null ? iddependencia.hashCode() : 0);
        hash += (idarea != null ? idarea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbareaPK)) {
            return false;
        }
        TbareaPK other = (TbareaPK) object;
        if ((this.iddependencia == null && other.iddependencia != null) || (this.iddependencia != null && !this.iddependencia.equals(other.iddependencia))) {
            return false;
        }
        if ((this.idarea == null && other.idarea != null) || (this.idarea != null && !this.idarea.equals(other.idarea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.TbareaPK[ iddependencia=" + iddependencia + ", idarea=" + idarea + " ]";
    }
    
}
