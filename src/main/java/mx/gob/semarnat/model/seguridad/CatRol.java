/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Manejo del catalogo de roles
 * @author dante
 */
@Entity
@Table(name = "CAT_ROL", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatRol.findAll", query = "SELECT cr FROM CatRol cr"),
    @NamedQuery(name = "CatRol.findById", query = "SELECT cr FROM CatRol cr WHERE cr.idRol = :idRol"),
    @NamedQuery(name = "CatRol.findByNombre", query = "SELECT cr FROM CatRol cr WHERE cr.nombre = :nombre"),
    @NamedQuery(name = "CatRol.findByNombreCorto", query = "SELECT cr FROM CatRol cr WHERE cr.nombreCorto = :nombreCorto")})
public class CatRol implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ROL_ID")
    private int idRol;
    @Basic(optional = false)
    @Column(name = "ROL_NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "ROL_NOMBRE_CORTO")
    private String nombreCorto;
    @Basic(optional = true)
    @Column(name = "ROL_DESCRIPCION")
    private String descripcion;
    

    public CatRol() {
    }

    public CatRol(int idRol) {
        this.idRol = idRol;
    }

    public CatRol(int idRol, String nombre, String nombreCorto, String descripcion) {
        this.idRol = idRol;
        this.nombre = nombre;
        this.nombreCorto = nombreCorto;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatRol)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatRol[ idRol=" + idRol + " ]";
    }

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
    
}
