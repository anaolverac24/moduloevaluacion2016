/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Entidad de UsuarioRol, que son la relacion de roles asignados a usuarios
 * @author dante
 */
@Entity
@Table(name = "USUARIO_ROL", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioRol.findAll", query = "SELECT ur FROM UsuarioRol ur"),
    @NamedQuery(name = "UsuarioRol.findByIdUsuarioRol", query = "SELECT ur FROM UsuarioRol ur WHERE ur.usuarioRolId = :usuarioRolId"),
    @NamedQuery(name = "UsuarioRol.findByIdUsuario", query = "SELECT ur FROM UsuarioRol ur WHERE ur.usuarioId = :usuarioId"),
    @NamedQuery(name = "UsuarioRol.findByIdRol", query = "SELECT ur FROM UsuarioRol ur WHERE ur.rolId = :rolId"),
    @NamedQuery(name = "UsuarioRol.findByIdFechaVencimiento", query = "SELECT ur FROM UsuarioRol ur WHERE ur.fechaVencimiento = :fechaVencimiento")})
public class UsuarioRol implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USUARIO_ROL_ID")
    private int usuarioRolId;
    @JoinColumn(name = "USUARIO_ID", referencedColumnName = "IDUSUARIO")
    @ManyToOne
    private Tbusuario usuarioId;
    @JoinColumn(name = "ROL_ID", referencedColumnName = "ROL_ID")
    @ManyToOne
    private CatRol rolId;
    @Column(name = "FECHA_VENCIMIENTO_ROL")
    private String fechaVencimiento;
    

    public UsuarioRol() {
    }

    public UsuarioRol(int usuarioRolId) {
        this.usuarioRolId = usuarioRolId;
    }

    public UsuarioRol(int usuarioRolId, Tbusuario usuarioId, CatRol rolId, String fechaVencimiento) {
        this.usuarioRolId = usuarioRolId;
        this.usuarioId = usuarioId;
        this.rolId = rolId;
        this.fechaVencimiento = fechaVencimiento;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioRol)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.UsuarioRol[ usuarioRolId=" + usuarioRolId + " ]";
    }

	public int getUsuarioRolId() {
		return usuarioRolId;
	}

	public void setUsuarioRolId(int usuarioRolId) {
		this.usuarioRolId = usuarioRolId;
	}

	public Tbusuario getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Tbusuario usuarioId) {
		this.usuarioId = usuarioId;
	}

	public CatRol getRolId() {
		return rolId;
	}

	public void setRolId(CatRol rolId) {
		this.rolId = rolId;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
    
}
