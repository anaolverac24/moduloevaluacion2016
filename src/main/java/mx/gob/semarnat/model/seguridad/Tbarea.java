/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "TBAREA", schema="SEGURIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tbarea.findAll", query = "SELECT t FROM Tbarea t"),
    @NamedQuery(name = "Tbarea.findByIddependencia", query = "SELECT t FROM Tbarea t WHERE t.tbareaPK.iddependencia = :iddependencia"),
    @NamedQuery(name = "Tbarea.findByIdarea", query = "SELECT t FROM Tbarea t WHERE t.tbareaPK.idarea = :idarea"),
    @NamedQuery(name = "Tbarea.findByIdareaDependencia", query = "SELECT t FROM Tbarea t WHERE t.tbareaPK.idarea = :idarea AND t.tbareaPK.iddependencia = :iddependencia AND t.estatus = 1"),
    @NamedQuery(name = "Tbarea.findByIddependenciapadre", query = "SELECT t FROM Tbarea t WHERE t.iddependenciapadre = :iddependenciapadre"),
    @NamedQuery(name = "Tbarea.findByIdareapadre", query = "SELECT t FROM Tbarea t WHERE t.idareapadre = :idareapadre"),
    @NamedQuery(name = "Tbarea.findByIdentidadfederativa", query = "SELECT t FROM Tbarea t WHERE t.identidadfederativa = :identidadfederativa"),
    @NamedQuery(name = "Tbarea.findByArea", query = "SELECT t FROM Tbarea t WHERE t.area = :area"),
    @NamedQuery(name = "Tbarea.findBySiglas", query = "SELECT t FROM Tbarea t WHERE t.siglas = :siglas"),
    @NamedQuery(name = "Tbarea.findByDescripcion", query = "SELECT t FROM Tbarea t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "Tbarea.findByOficinaregional", query = "SELECT t FROM Tbarea t WHERE t.oficinaregional = :oficinaregional"),
    @NamedQuery(name = "Tbarea.findByEstatus", query = "SELECT t FROM Tbarea t WHERE t.estatus = :estatus"),
    @NamedQuery(name = "Tbarea.findByJerarquia", query = "SELECT t FROM Tbarea t WHERE t.jerarquia = :jerarquia")})
public class Tbarea implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TbareaPK tbareaPK;
    @Column(name = "IDDEPENDENCIAPADRE")
    private String iddependenciapadre;
    @Column(name = "IDAREAPADRE")
    private String idareapadre;
    @Column(name = "IDENTIDADFEDERATIVA")
    private String identidadfederativa;
    @Basic(optional = false)
    @Column(name = "AREA")
    private String area;
    @Column(name = "SIGLAS")
    private String siglas;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "OFICINAREGIONAL")
    private BigInteger oficinaregional;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Column(name = "JERARQUIA")
    private String jerarquia;
    @JoinColumn(name = "IDDEPENDENCIA", referencedColumnName = "IDDEPENDENCIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ctdependencia ctdependencia;
    @JoinColumn(name = "IDGRUPOTRABAJO", referencedColumnName = "IDGRUPOTRABAJO")
    @ManyToOne
    private Ctgrupotrabajo idgrupotrabajo;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "tbarea1")
    private Tbarea tbarea;
    @JoinColumns({
        @JoinColumn(name = "IDAREA", referencedColumnName = "IDAREA", insertable = false, updatable = false),
        @JoinColumn(name = "IDDEPENDENCIA", referencedColumnName = "IDDEPENDENCIA", insertable = false, updatable = false)})
    @OneToOne(optional = false)
    private Tbarea tbarea1;

    public Tbarea() {
    }

    public Tbarea(TbareaPK tbareaPK) {
        this.tbareaPK = tbareaPK;
    }

    public Tbarea(TbareaPK tbareaPK, String area, BigInteger estatus) {
        this.tbareaPK = tbareaPK;
        this.area = area;
        this.estatus = estatus;
    }

    public Tbarea(String iddependencia, String idarea) {
        this.tbareaPK = new TbareaPK(iddependencia, idarea);
    }

    public TbareaPK getTbareaPK() {
        return tbareaPK;
    }

    public void setTbareaPK(TbareaPK tbareaPK) {
        this.tbareaPK = tbareaPK;
    }

    public String getIddependenciapadre() {
        return iddependenciapadre;
    }

    public void setIddependenciapadre(String iddependenciapadre) {
        this.iddependenciapadre = iddependenciapadre;
    }

    public String getIdareapadre() {
        return idareapadre;
    }

    public void setIdareapadre(String idareapadre) {
        this.idareapadre = idareapadre;
    }

    public String getIdentidadfederativa() {
        return identidadfederativa;
    }

    public void setIdentidadfederativa(String identidadfederativa) {
        this.identidadfederativa = identidadfederativa;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getOficinaregional() {
        return oficinaregional;
    }

    public void setOficinaregional(BigInteger oficinaregional) {
        this.oficinaregional = oficinaregional;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public String getJerarquia() {
        return jerarquia;
    }

    public void setJerarquia(String jerarquia) {
        this.jerarquia = jerarquia;
    }

    public Ctdependencia getCtdependencia() {
        return ctdependencia;
    }

    public void setCtdependencia(Ctdependencia ctdependencia) {
        this.ctdependencia = ctdependencia;
    }

    public Ctgrupotrabajo getIdgrupotrabajo() {
        return idgrupotrabajo;
    }

    public void setIdgrupotrabajo(Ctgrupotrabajo idgrupotrabajo) {
        this.idgrupotrabajo = idgrupotrabajo;
    }

    public Tbarea getTbarea() {
        return tbarea;
    }

    public void setTbarea(Tbarea tbarea) {
        this.tbarea = tbarea;
    }

    public Tbarea getTbarea1() {
        return tbarea1;
    }

    public void setTbarea1(Tbarea tbarea1) {
        this.tbarea1 = tbarea1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tbareaPK != null ? tbareaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tbarea)) {
            return false;
        }
        Tbarea other = (Tbarea) object;
        if ((this.tbareaPK == null && other.tbareaPK != null) || (this.tbareaPK != null && !this.tbareaPK.equals(other.tbareaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Tbarea[ tbareaPK=" + tbareaPK + " ]";
    }
    
}
