/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "TBUSUARIO", schema="SEGURIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tbusuario.findAll", query = "SELECT t FROM Tbusuario t"),
    @NamedQuery(name = "Tbusuario.findByIdusuario", query = "SELECT t FROM Tbusuario t WHERE t.idusuario = :idusuario"),
    @NamedQuery(name = "Tbusuario.findByIddependencia", query = "SELECT t FROM Tbusuario t WHERE t.iddependencia = :iddependencia"),
    @NamedQuery(name = "Tbusuario.findByIdarea", query = "SELECT t FROM Tbusuario t WHERE t.idarea = :idarea"),
    @NamedQuery(name = "Tbusuario.findByUsuario", query = "SELECT t FROM Tbusuario t WHERE t.usuario = :usuario"),
    @NamedQuery(name = "Tbusuario.findByContrasena", query = "SELECT t FROM Tbusuario t WHERE t.contrasena = :contrasena"),
    @NamedQuery(name = "Tbusuario.findByFechaalta", query = "SELECT t FROM Tbusuario t WHERE t.fechaalta = :fechaalta"),
    @NamedQuery(name = "Tbusuario.findByFechabaja", query = "SELECT t FROM Tbusuario t WHERE t.fechabaja = :fechabaja"),
    @NamedQuery(name = "Tbusuario.findByFechamodificacioncontrasena", query = "SELECT t FROM Tbusuario t WHERE t.fechamodificacioncontrasena = :fechamodificacioncontrasena"),
    @NamedQuery(name = "Tbusuario.findByBloqueado", query = "SELECT t FROM Tbusuario t WHERE t.bloqueado = :bloqueado"),
    @NamedQuery(name = "Tbusuario.findBySesioniniciada", query = "SELECT t FROM Tbusuario t WHERE t.sesioniniciada = :sesioniniciada"),
    @NamedQuery(name = "Tbusuario.findByIntentos", query = "SELECT t FROM Tbusuario t WHERE t.intentos = :intentos"),
    @NamedQuery(name = "Tbusuario.findByEstatus", query = "SELECT t FROM Tbusuario t WHERE t.estatus = :estatus"),
    @NamedQuery(name = "Tbusuario.findByAcceso", query = "SELECT t FROM Tbusuario t WHERE t.acceso = :acceso"),
    @NamedQuery(name = "Tbusuario.findByBloqueadoSirap", query = "SELECT t FROM Tbusuario t WHERE t.bloqueadoSirap = :bloqueadoSirap"),
    @NamedQuery(name = "Tbusuario.findBySesioniniciadaSirap", query = "SELECT t FROM Tbusuario t WHERE t.sesioniniciadaSirap = :sesioniniciadaSirap"),
    @NamedQuery(name = "Tbusuario.findByIntentosSirap", query = "SELECT t FROM Tbusuario t WHERE t.intentosSirap = :intentosSirap"),
    @NamedQuery(name = "Tbusuario.findByFechabajaSirap", query = "SELECT t FROM Tbusuario t WHERE t.fechabajaSirap = :fechabajaSirap"),
	// @NamedQuery(name = "Tbusuario.findAll2", query = "SELECT t FROM Tbusuario t, Tbarea ta WHERE tu.idarea = ta.tbareaPK.idarea and tu.iddependencia = ta.tbareaPK.iddependencia")
    })
public class Tbusuario implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "IDUSUARIO") //BigDecimal
    private Integer idusuario;
    @Basic(optional = false)
    @Column(name = "IDDEPENDENCIA")
    private String iddependencia;
    @Basic(optional = false)
    @Column(name = "IDAREA")
    private String idarea;
    @Basic(optional = false)
    @Column(name = "USUARIO")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "CONTRASENA")
    private String contrasena;
    @Basic(optional = false)
    @Column(name = "FECHAALTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaalta;
    @Column(name = "FECHABAJA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechabaja;
    @Column(name = "FECHAMODIFICACIONCONTRASENA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechamodificacioncontrasena;
    @Basic(optional = false)
    @Column(name = "BLOQUEADO")
    private BigInteger bloqueado;
    @Basic(optional = false)
    @Column(name = "SESIONINICIADA")
    private BigInteger sesioniniciada;
    @Column(name = "INTENTOS")
    private BigInteger intentos;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Column(name = "ACCESO")
    private BigInteger acceso;
    @Column(name = "BLOQUEADO_SIRAP")
    private BigInteger bloqueadoSirap;
    @Column(name = "SESIONINICIADA_SIRAP")
    private BigInteger sesioniniciadaSirap;
    @Column(name = "INTENTOS_SIRAP")
    private BigInteger intentosSirap;
    @Column(name = "FECHABAJA_SIRAP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechabajaSirap;
    @JoinColumn(name = "IDEMPLEADO", referencedColumnName = "IDEMPLEADO")
    @ManyToOne(optional = false)
    private Tbempleado idempleado;

    public Tbusuario() {
    }

    public Tbusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public Tbusuario(Integer idusuario, String iddependencia, String idarea, String usuario, String contrasena, Date fechaalta, BigInteger bloqueado, BigInteger sesioniniciada, BigInteger estatus) {
        this.idusuario = idusuario;
        this.iddependencia = iddependencia;
        this.idarea = idarea;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.fechaalta = fechaalta;
        this.bloqueado = bloqueado;
        this.sesioniniciada = sesioniniciada;
        this.estatus = estatus;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public String getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(String iddependencia) {
        this.iddependencia = iddependencia;
    }

    public String getIdarea() {
        return idarea;
    }

    public void setIdarea(String idarea) {
        this.idarea = idarea;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Date getFechaalta() {
        return fechaalta;
    }

    public void setFechaalta(Date fechaalta) {
        this.fechaalta = fechaalta;
    }

    public Date getFechabaja() {
        return fechabaja;
    }

    public void setFechabaja(Date fechabaja) {
        this.fechabaja = fechabaja;
    }

    public Date getFechamodificacioncontrasena() {
        return fechamodificacioncontrasena;
    }

    public void setFechamodificacioncontrasena(Date fechamodificacioncontrasena) {
        this.fechamodificacioncontrasena = fechamodificacioncontrasena;
    }

    public BigInteger getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(BigInteger bloqueado) {
        this.bloqueado = bloqueado;
    }

    public BigInteger getSesioniniciada() {
        return sesioniniciada;
    }

    public void setSesioniniciada(BigInteger sesioniniciada) {
        this.sesioniniciada = sesioniniciada;
    }

    public BigInteger getIntentos() {
        return intentos;
    }

    public void setIntentos(BigInteger intentos) {
        this.intentos = intentos;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public BigInteger getAcceso() {
        return acceso;
    }

    public void setAcceso(BigInteger acceso) {
        this.acceso = acceso;
    }

    public BigInteger getBloqueadoSirap() {
        return bloqueadoSirap;
    }

    public void setBloqueadoSirap(BigInteger bloqueadoSirap) {
        this.bloqueadoSirap = bloqueadoSirap;
    }

    public BigInteger getSesioniniciadaSirap() {
        return sesioniniciadaSirap;
    }

    public void setSesioniniciadaSirap(BigInteger sesioniniciadaSirap) {
        this.sesioniniciadaSirap = sesioniniciadaSirap;
    }

    public BigInteger getIntentosSirap() {
        return intentosSirap;
    }

    public void setIntentosSirap(BigInteger intentosSirap) {
        this.intentosSirap = intentosSirap;
    }

    public Date getFechabajaSirap() {
        return fechabajaSirap;
    }

    public void setFechabajaSirap(Date fechabajaSirap) {
        this.fechabajaSirap = fechabajaSirap;
    }

    public Tbempleado getIdempleado() {
        return idempleado;
    }

    public void setIdempleado(Tbempleado idempleado) {
        this.idempleado = idempleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idusuario != null ? idusuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tbusuario)) {
            return false;
        }
        Tbusuario other = (Tbusuario) object;
        if ((this.idusuario == null && other.idusuario != null) || (this.idusuario != null && !this.idusuario.equals(other.idusuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Tbusuario[ idusuario=" + idusuario + " ]";
    }
    
}
