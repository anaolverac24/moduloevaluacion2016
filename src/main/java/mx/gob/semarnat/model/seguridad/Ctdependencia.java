/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.seguridad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "CTDEPENDENCIA", schema="SEGURIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ctdependencia.findAll", query = "SELECT c FROM Ctdependencia c"),
    @NamedQuery(name = "Ctdependencia.findByIddependencia", query = "SELECT c FROM Ctdependencia c WHERE c.iddependencia = :iddependencia"),
    @NamedQuery(name = "Ctdependencia.findByNombredependencia", query = "SELECT c FROM Ctdependencia c WHERE c.nombredependencia = :nombredependencia")})
public class Ctdependencia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IDDEPENDENCIA")
    private String iddependencia;
    @Column(name = "NOMBREDEPENDENCIA")
    private String nombredependencia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ctdependencia")
    private Collection<Tbarea> tbareaCollection;

    public Ctdependencia() {
    }

    public Ctdependencia(String iddependencia) {
        this.iddependencia = iddependencia;
    }

    public String getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(String iddependencia) {
        this.iddependencia = iddependencia;
    }

    public String getNombredependencia() {
        return nombredependencia;
    }

    public void setNombredependencia(String nombredependencia) {
        this.nombredependencia = nombredependencia;
    }

    @XmlTransient
    public Collection<Tbarea> getTbareaCollection() {
        return tbareaCollection;
    }

    public void setTbareaCollection(Collection<Tbarea> tbareaCollection) {
        this.tbareaCollection = tbareaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddependencia != null ? iddependencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ctdependencia)) {
            return false;
        }
        Ctdependencia other = (Ctdependencia) object;
        if ((this.iddependencia == null && other.iddependencia != null) || (this.iddependencia != null && !this.iddependencia.equals(other.iddependencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Ctdependencia[ iddependencia=" + iddependencia + " ]";
    }
    
}
