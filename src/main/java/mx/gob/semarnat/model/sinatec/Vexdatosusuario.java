/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.sinatec;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Case Solutions 10
 */
@Entity
@Table(name = "VEXDATOSUSUARIO", schema="SINATEC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vexdatosusuario.findAll", query = "SELECT v FROM Vexdatosusuario v"),
    @NamedQuery(name = "Vexdatosusuario.findByBgtramiteid", query = "SELECT v FROM Vexdatosusuario v WHERE v.bgtramiteid = :bgtramiteid"),
    @NamedQuery(name = "Vexdatosusuario.findByVcrfc", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcrfc = :vcrfc"),
    @NamedQuery(name = "Vexdatosusuario.findByBtentidadgobierno", query = "SELECT v FROM Vexdatosusuario v WHERE v.btentidadgobierno = :btentidadgobierno"),
    @NamedQuery(name = "Vexdatosusuario.findByVctipopersona", query = "SELECT v FROM Vexdatosusuario v WHERE v.vctipopersona = :vctipopersona"),
    @NamedQuery(name = "Vexdatosusuario.findByVctipoentidad", query = "SELECT v FROM Vexdatosusuario v WHERE v.vctipoentidad = :vctipoentidad"),
    @NamedQuery(name = "Vexdatosusuario.findByVcrazonsocial", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcrazonsocial = :vcrazonsocial"),
    @NamedQuery(name = "Vexdatosusuario.findByVcnombre", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcnombre = :vcnombre"),
    @NamedQuery(name = "Vexdatosusuario.findByVcapellidopaterno", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcapellidopaterno = :vcapellidopaterno"),
    @NamedQuery(name = "Vexdatosusuario.findByVcapellidomaterno", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcapellidomaterno = :vcapellidomaterno"),
    @NamedQuery(name = "Vexdatosusuario.findByVccurp", query = "SELECT v FROM Vexdatosusuario v WHERE v.vccurp = :vccurp"),
    @NamedQuery(name = "Vexdatosusuario.findByVcidEntidadFederativaLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcidEntidadFederativaLk = :vcidEntidadFederativaLk"),
    @NamedQuery(name = "Vexdatosusuario.findByVcidDelegacionMunicipioLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcidDelegacionMunicipioLk = :vcidDelegacionMunicipioLk"),
    @NamedQuery(name = "Vexdatosusuario.findByVclocalidad", query = "SELECT v FROM Vexdatosusuario v WHERE v.vclocalidad = :vclocalidad"),
    @NamedQuery(name = "Vexdatosusuario.findByVcidAsentaCpconsLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcidAsentaCpconsLk = :vcidAsentaCpconsLk"),
    @NamedQuery(name = "Vexdatosusuario.findByVcdAsentaLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcdAsentaLk = :vcdAsentaLk"),
    @NamedQuery(name = "Vexdatosusuario.findByVcdCodigoLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcdCodigoLk = :vcdCodigoLk"),
    @NamedQuery(name = "Vexdatosusuario.findByVcdTipoAsentaLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcdTipoAsentaLk = :vcdTipoAsentaLk"),
    @NamedQuery(name = "Vexdatosusuario.findByBgcveTipoAsenLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.bgcveTipoAsenLk = :bgcveTipoAsenLk"),
    @NamedQuery(name = "Vexdatosusuario.findByVctipoasentamiento", query = "SELECT v FROM Vexdatosusuario v WHERE v.vctipoasentamiento = :vctipoasentamiento"),
    @NamedQuery(name = "Vexdatosusuario.findByBgcveTipoVialLk", query = "SELECT v FROM Vexdatosusuario v WHERE v.bgcveTipoVialLk = :bgcveTipoVialLk"),
    @NamedQuery(name = "Vexdatosusuario.findByVcnombrevialidad", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcnombrevialidad = :vcnombrevialidad"),
    @NamedQuery(name = "Vexdatosusuario.findByVcnumexterior", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcnumexterior = :vcnumexterior"),
    @NamedQuery(name = "Vexdatosusuario.findByVcnuminterior", query = "SELECT v FROM Vexdatosusuario v WHERE v.vcnuminterior = :vcnuminterior")})
public class Vexdatosusuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Id
    @Column(name = "BGTRAMITEID")
    private Integer bgtramiteid;
    @Basic(optional = false)
    @Column(name = "VCRFC")
    private String vcrfc;
    @Column(name = "BTENTIDADGOBIERNO")
    private BigInteger btentidadgobierno;
    @Column(name = "VCTIPOPERSONA")
    private String vctipopersona;
    @Column(name = "VCTIPOENTIDAD")
    private String vctipoentidad;
    @Column(name = "VCRAZONSOCIAL")
    private String vcrazonsocial;
    @Column(name = "VCNOMBRE")
    private String vcnombre;
    @Column(name = "VCAPELLIDOPATERNO")
    private String vcapellidopaterno;
    @Column(name = "VCAPELLIDOMATERNO")
    private String vcapellidomaterno;
    @Column(name = "VCCURP")
    private String vccurp;
    @Basic(optional = false)
    @Column(name = "VCID_ENTIDAD_FEDERATIVA_LK")
    private String vcidEntidadFederativaLk;
    @Basic(optional = false)
    @Column(name = "VCID_DELEGACION_MUNICIPIO_LK")
    private String vcidDelegacionMunicipioLk;
    @Column(name = "VCLOCALIDAD")
    private String vclocalidad;
    @Basic(optional = false)
    @Column(name = "VCID_ASENTA_CPCONS_LK")
    private String vcidAsentaCpconsLk;
    @Basic(optional = false)
    @Column(name = "VCD_ASENTA_LK")
    private String vcdAsentaLk;
    @Basic(optional = false)
    @Column(name = "VCD_CODIGO_LK")
    private String vcdCodigoLk;
    @Column(name = "VCD_TIPO_ASENTA_LK")
    private String vcdTipoAsentaLk;
    @Basic(optional = false)
    @Column(name = "BGCVE_TIPO_ASEN_LK")
    private BigInteger bgcveTipoAsenLk;
    @Basic(optional = false)
    @Column(name = "VCTIPOASENTAMIENTO")
    private String vctipoasentamiento;
    @Basic(optional = false)
    @Column(name = "BGCVE_TIPO_VIAL_LK")
    private BigInteger bgcveTipoVialLk;
    @Basic(optional = false)
    @Column(name = "VCNOMBREVIALIDAD")
    private String vcnombrevialidad;
    @Basic(optional = false)
    @Column(name = "VCNUMEXTERIOR")
    private String vcnumexterior;
    @Basic(optional = false)
    @Column(name = "VCNUMINTERIOR")
    private String vcnuminterior;

    public Vexdatosusuario() {
    }

    public Integer getBgtramiteid() {
        return bgtramiteid;
    }

    public void setBgtramiteid(Integer bgtramiteid) {
        this.bgtramiteid = bgtramiteid;
    }

    public String getVcrfc() {
        return vcrfc;
    }

    public void setVcrfc(String vcrfc) {
        this.vcrfc = vcrfc;
    }

    public BigInteger getBtentidadgobierno() {
        return btentidadgobierno;
    }

    public void setBtentidadgobierno(BigInteger btentidadgobierno) {
        this.btentidadgobierno = btentidadgobierno;
    }

    public String getVctipopersona() {
        return vctipopersona;
    }

    public void setVctipopersona(String vctipopersona) {
        this.vctipopersona = vctipopersona;
    }

    public String getVctipoentidad() {
        return vctipoentidad;
    }

    public void setVctipoentidad(String vctipoentidad) {
        this.vctipoentidad = vctipoentidad;
    }

    public String getVcrazonsocial() {
        return vcrazonsocial;
    }

    public void setVcrazonsocial(String vcrazonsocial) {
        this.vcrazonsocial = vcrazonsocial;
    }

    public String getVcnombre() {
        return vcnombre;
    }

    public void setVcnombre(String vcnombre) {
        this.vcnombre = vcnombre;
    }

    public String getVcapellidopaterno() {
        return vcapellidopaterno;
    }

    public void setVcapellidopaterno(String vcapellidopaterno) {
        this.vcapellidopaterno = vcapellidopaterno;
    }

    public String getVcapellidomaterno() {
        return vcapellidomaterno;
    }

    public void setVcapellidomaterno(String vcapellidomaterno) {
        this.vcapellidomaterno = vcapellidomaterno;
    }

    public String getVccurp() {
        return vccurp;
    }

    public void setVccurp(String vccurp) {
        this.vccurp = vccurp;
    }

    public String getVcidEntidadFederativaLk() {
        return vcidEntidadFederativaLk;
    }

    public void setVcidEntidadFederativaLk(String vcidEntidadFederativaLk) {
        this.vcidEntidadFederativaLk = vcidEntidadFederativaLk;
    }

    public String getVcidDelegacionMunicipioLk() {
        return vcidDelegacionMunicipioLk;
    }

    public void setVcidDelegacionMunicipioLk(String vcidDelegacionMunicipioLk) {
        this.vcidDelegacionMunicipioLk = vcidDelegacionMunicipioLk;
    }

    public String getVclocalidad() {
        return vclocalidad;
    }

    public void setVclocalidad(String vclocalidad) {
        this.vclocalidad = vclocalidad;
    }

    public String getVcidAsentaCpconsLk() {
        return vcidAsentaCpconsLk;
    }

    public void setVcidAsentaCpconsLk(String vcidAsentaCpconsLk) {
        this.vcidAsentaCpconsLk = vcidAsentaCpconsLk;
    }

    public String getVcdAsentaLk() {
        return vcdAsentaLk;
    }

    public void setVcdAsentaLk(String vcdAsentaLk) {
        this.vcdAsentaLk = vcdAsentaLk;
    }

    public String getVcdCodigoLk() {
        return vcdCodigoLk;
    }

    public void setVcdCodigoLk(String vcdCodigoLk) {
        this.vcdCodigoLk = vcdCodigoLk;
    }

    public String getVcdTipoAsentaLk() {
        return vcdTipoAsentaLk;
    }

    public void setVcdTipoAsentaLk(String vcdTipoAsentaLk) {
        this.vcdTipoAsentaLk = vcdTipoAsentaLk;
    }

    public BigInteger getBgcveTipoAsenLk() {
        return bgcveTipoAsenLk;
    }

    public void setBgcveTipoAsenLk(BigInteger bgcveTipoAsenLk) {
        this.bgcveTipoAsenLk = bgcveTipoAsenLk;
    }

    public String getVctipoasentamiento() {
        return vctipoasentamiento;
    }

    public void setVctipoasentamiento(String vctipoasentamiento) {
        this.vctipoasentamiento = vctipoasentamiento;
    }

    public BigInteger getBgcveTipoVialLk() {
        return bgcveTipoVialLk;
    }

    public void setBgcveTipoVialLk(BigInteger bgcveTipoVialLk) {
        this.bgcveTipoVialLk = bgcveTipoVialLk;
    }

    public String getVcnombrevialidad() {
        return vcnombrevialidad;
    }

    public void setVcnombrevialidad(String vcnombrevialidad) {
        this.vcnombrevialidad = vcnombrevialidad;
    }

    public String getVcnumexterior() {
        return vcnumexterior;
    }

    public void setVcnumexterior(String vcnumexterior) {
        this.vcnumexterior = vcnumexterior;
    }

    public String getVcnuminterior() {
        return vcnuminterior;
    }

    public void setVcnuminterior(String vcnuminterior) {
        this.vcnuminterior = vcnuminterior;
    }
    
}
