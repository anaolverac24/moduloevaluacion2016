/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.sinatec;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Case Solutions 10
 */
@Entity
@Table(name = "VEXDATOSUSUARIOREP", schema="SINATEC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vexdatosusuariorep.findAll", query = "SELECT v FROM Vexdatosusuariorep v"),
    @NamedQuery(name = "Vexdatosusuariorep.findByBgtramiteid", query = "SELECT v FROM Vexdatosusuariorep v WHERE v.bgtramiteid = :bgtramiteid"),
    @NamedQuery(name = "Vexdatosusuariorep.findByVccurp", query = "SELECT v FROM Vexdatosusuariorep v WHERE v.vccurp = :vccurp"),
    @NamedQuery(name = "Vexdatosusuariorep.findByVcnombre", query = "SELECT v FROM Vexdatosusuariorep v WHERE v.vcnombre = :vcnombre"),
    @NamedQuery(name = "Vexdatosusuariorep.findByVcapellido1", query = "SELECT v FROM Vexdatosusuariorep v WHERE v.vcapellido1 = :vcapellido1"),
    @NamedQuery(name = "Vexdatosusuariorep.findByVcapellido2", query = "SELECT v FROM Vexdatosusuariorep v WHERE v.vcapellido2 = :vcapellido2"),
    @NamedQuery(name = "Vexdatosusuariorep.findByVccorreo", query = "SELECT v FROM Vexdatosusuariorep v WHERE v.vccorreo = :vccorreo")})
public class Vexdatosusuariorep implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Id
    @Column(name = "BGTRAMITEID")
    private Integer bgtramiteid;
    @Basic(optional = false)
    @Column(name = "VCCURP")
    private String vccurp;
    @Basic(optional = false)
    @Column(name = "VCNOMBRE")
    private String vcnombre;
    @Basic(optional = false)
    @Column(name = "VCAPELLIDO1")
    private String vcapellido1;
    @Basic(optional = false)
    @Column(name = "VCAPELLIDO2")
    private String vcapellido2;
    @Basic(optional = false)
    @Column(name = "VCCORREO")
    private String vccorreo;

    public Vexdatosusuariorep() {
    }

    public Integer getBgtramiteid() {
        return bgtramiteid;
    }

    public void setBgtramiteid(Integer bgtramiteid) {
        this.bgtramiteid = bgtramiteid;
    }

    public String getVccurp() {
        return vccurp;
    }

    public void setVccurp(String vccurp) {
        this.vccurp = vccurp;
    }

    public String getVcnombre() {
        return vcnombre;
    }

    public void setVcnombre(String vcnombre) {
        this.vcnombre = vcnombre;
    }

    public String getVcapellido1() {
        return vcapellido1;
    }

    public void setVcapellido1(String vcapellido1) {
        this.vcapellido1 = vcapellido1;
    }

    public String getVcapellido2() {
        return vcapellido2;
    }

    public void setVcapellido2(String vcapellido2) {
        this.vcapellido2 = vcapellido2;
    }

    public String getVccorreo() {
        return vccorreo;
    }

    public void setVccorreo(String vccorreo) {
        this.vccorreo = vccorreo;
    }

    @Override
    public String toString() {
        return this.vccurp;
    }
    
}
