/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.sinatec;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Case Solutions 10
 */
@Entity
@Table(name = "VEXDATOSTRAMITE", schema="SINATEC")  
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vexdatostramite.findAll", query = "SELECT v FROM Vexdatostramite v"),
    @NamedQuery(name = "Vexdatostramite.findByBgtramiteid", query = "SELECT v FROM Vexdatostramite v WHERE v.bgtramiteid = :bgtramiteid"),
    @NamedQuery(name = "Vexdatostramite.findByBgtramiteidOrderByFecha", query = "SELECT v FROM Vexdatostramite v WHERE v.bgtramiteid = :bgtramiteid ORDER BY v.dtfecharegistro DESC"),
    @NamedQuery(name = "Vexdatostramite.findByDtfecharegistro", query = "SELECT v FROM Vexdatostramite v WHERE v.dtfecharegistro = :dtfecharegistro"),
    @NamedQuery(name = "Vexdatostramite.findByBgidTramiteLk", query = "SELECT v FROM Vexdatostramite v WHERE v.bgidTramiteLk = :bgidTramiteLk"),
    @NamedQuery(name = "Vexdatostramite.findByVcclaveTramite", query = "SELECT v FROM Vexdatostramite v WHERE v.vcclaveTramite = :vcclaveTramite"),
    @NamedQuery(name = "Vexdatostramite.findByVcbitacora", query = "SELECT v FROM Vexdatostramite v WHERE v.vcbitacora = :vcbitacora"),
    @NamedQuery(name = "Vexdatostramite.findByVcproyecto", query = "SELECT v FROM Vexdatostramite v WHERE v.vcproyecto = :vcproyecto"),
    @NamedQuery(name = "Vexdatostramite.findByVcnombre", query = "SELECT v FROM Vexdatostramite v WHERE v.vcnombre = :vcnombre"),
    @NamedQuery(name = "Vexdatostramite.findByVcapellido1", query = "SELECT v FROM Vexdatostramite v WHERE v.vcapellido1 = :vcapellido1"),
    @NamedQuery(name = "Vexdatostramite.findByVcapellido2", query = "SELECT v FROM Vexdatostramite v WHERE v.vcapellido2 = :vcapellido2"),
    @NamedQuery(name = "Vexdatostramite.findByVcreferencia", query = "SELECT v FROM Vexdatostramite v WHERE v.vcreferencia = :vcreferencia"),
    @NamedQuery(name = "Vexdatostramite.findByMontoConredondeo", query = "SELECT v FROM Vexdatostramite v WHERE v.montoConredondeo = :montoConredondeo")})
public class Vexdatostramite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Id
    @Column(name = "BGTRAMITEID")
    private Integer bgtramiteid;
    @Basic(optional = false)
    @Column(name = "DTFECHAREGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtfecharegistro;
    @Basic(optional = false)
    @Column(name = "BGID_TRAMITE_LK")
    private Integer bgidTramiteLk;
    @Basic(optional = false)
    @Column(name = "VCCLAVE_TRAMITE")
    private String vcclaveTramite;
    @Column(name = "VCBITACORA")
    private String vcbitacora;
    @Column(name = "VCPROYECTO")
    private String vcproyecto;
    @Basic(optional = false)
    @Column(name = "VCNOMBRE")
    private String vcnombre;
    @Basic(optional = false)
    @Column(name = "VCAPELLIDO1")
    private String vcapellido1;
    @Basic(optional = false)
    @Column(name = "VCAPELLIDO2")
    private String vcapellido2;
    @Column(name = "VCREFERENCIA")
    private String vcreferencia;
    @Column(name = "MONTO_CONREDONDEO")
    private BigInteger montoConredondeo;

    public Vexdatostramite() {
    }

    public Integer getBgtramiteid() {
        return bgtramiteid;
    }

    public void setBgtramiteid(Integer bgtramiteid) {
        this.bgtramiteid = bgtramiteid;
    }

    public Date getDtfecharegistro() {
        return dtfecharegistro;
    }

    public void setDtfecharegistro(Date dtfecharegistro) {
        this.dtfecharegistro = dtfecharegistro;
    }

    public  Integer getBgidTramiteLk() {
        return bgidTramiteLk;
    }

    public void setBgidTramiteLk( Integer bgidTramiteLk) {
        this.bgidTramiteLk = bgidTramiteLk;
    }

    public String getVcclaveTramite() {
        return vcclaveTramite;
    }

    public void setVcclaveTramite(String vcclaveTramite) {
        this.vcclaveTramite = vcclaveTramite;
    }

    public String getVcbitacora() {
        return vcbitacora;
    }

    public void setVcbitacora(String vcbitacora) {
        this.vcbitacora = vcbitacora;
    }

    public String getVcproyecto() {
        return vcproyecto;
    }

    public void setVcproyecto(String vcproyecto) {
        this.vcproyecto = vcproyecto;
    }

    public String getVcnombre() {
        return vcnombre;
    }

    public void setVcnombre(String vcnombre) {
        this.vcnombre = vcnombre;
    }

    public String getVcapellido1() {
        return vcapellido1;
    }

    public void setVcapellido1(String vcapellido1) {
        this.vcapellido1 = vcapellido1;
    }

    public String getVcapellido2() {
        return vcapellido2;
    }

    public void setVcapellido2(String vcapellido2) {
        this.vcapellido2 = vcapellido2;
    }

    public String getVcreferencia() {
        return vcreferencia;
    }

    public void setVcreferencia(String vcreferencia) {
        this.vcreferencia = vcreferencia;
    }

    public BigInteger getMontoConredondeo() {
        return montoConredondeo;
    }

    public void setMontoConredondeo(BigInteger montoConredondeo) {
        this.montoConredondeo = montoConredondeo;
    }
    
}
