/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "SUBSECTOR_PROYECTO", schema="CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubsectorProyecto.findAll", query = "SELECT s FROM SubsectorProyecto s"),
    @NamedQuery(name = "SubsectorProyecto.findByNsub", query = "SELECT s FROM SubsectorProyecto s WHERE s.nsub = :nsub"),
    @NamedQuery(name = "SubsectorProyecto.findByNsec", query = "SELECT s FROM SubsectorProyecto s WHERE s.nsec = :nsec"),
    @NamedQuery(name = "SubsectorProyecto.findBySubsector", query = "SELECT s FROM SubsectorProyecto s WHERE s.subsector = :subsector"),
    @NamedQuery(name = "SubsectorProyecto.findByLetra", query = "SELECT s FROM SubsectorProyecto s WHERE s.letra = :letra"),
    @NamedQuery(name = "SubsectorProyecto.findByEstatus", query = "SELECT s FROM SubsectorProyecto s WHERE s.estatus = :estatus")})
public class SubsectorProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NSUB")
    private Short nsub;
    @Column(name = "NSEC")
    private Short nsec;
    @Basic(optional = false)
    @Column(name = "SUBSECTOR")
    private String subsector;
    @Basic(optional = false)
    @Column(name = "LETRA")
    private Character letra;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private short estatus;

    public SubsectorProyecto() {
    }

    public SubsectorProyecto(Short nsub) {
        this.nsub = nsub;
    }

    public SubsectorProyecto(Short nsub, String subsector, Character letra, short estatus) {
        this.nsub = nsub;
        this.subsector = subsector;
        this.letra = letra;
        this.estatus = estatus;
    }

    public Short getNsub() {
        return nsub;
    }

    public void setNsub(Short nsub) {
        this.nsub = nsub;
    }

    public Short getNsec() {
        return nsec;
    }

    public void setNsec(Short nsec) {
        this.nsec = nsec;
    }

    public String getSubsector() {
        return subsector;
    }

    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

    public Character getLetra() {
        return letra;
    }

    public void setLetra(Character letra) {
        this.letra = letra;
    }

    public short getEstatus() {
        return estatus;
    }

    public void setEstatus(short estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nsub != null ? nsub.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubsectorProyecto)) {
            return false;
        }
        SubsectorProyecto other = (SubsectorProyecto) object;
        if ((this.nsub == null && other.nsub != null) || (this.nsub != null && !this.nsub.equals(other.nsub))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.SubsectorProyecto[ nsub=" + nsub + " ]";
    }
    
}
