/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "SECTOR_PROYECTO", schema="CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SectorProyecto.findAll", query = "SELECT s FROM SectorProyecto s"),
    @NamedQuery(name = "SectorProyecto.findByNsec", query = "SELECT s FROM SectorProyecto s WHERE s.nsec = :nsec"),
    @NamedQuery(name = "SectorProyecto.findBySector", query = "SELECT s FROM SectorProyecto s WHERE s.sector = :sector"),
    @NamedQuery(name = "SectorProyecto.findByEstatus", query = "SELECT s FROM SectorProyecto s WHERE s.estatus = :estatus")})
public class SectorProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NSEC")
    private Short nsec;
    @Basic(optional = false)
    @Column(name = "SECTOR")
    private String sector;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private short estatus;

    public SectorProyecto() {
    }

    public SectorProyecto(Short nsec) {
        this.nsec = nsec;
    }

    public SectorProyecto(Short nsec, String sector, short estatus) {
        this.nsec = nsec;
        this.sector = sector;
        this.estatus = estatus;
    }

    public Short getNsec() {
        return nsec;
    }

    public void setNsec(Short nsec) {
        this.nsec = nsec;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public short getEstatus() {
        return estatus;
    }

    public void setEstatus(short estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nsec != null ? nsec.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SectorProyecto)) {
            return false;
        }
        SectorProyecto other = (SectorProyecto) object;
        if ((this.nsec == null && other.nsec != null) || (this.nsec != null && !this.nsec.equals(other.nsec))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.SectorProyecto[ nsec=" + nsec + " ]";
    }
    
}
