package mx.gob.semarnat.model.catalogos;

import java.util.ArrayList;
import java.util.List;

// clase publica para guardar informacion de usuario y jerarquia, asi como estructura que lo acompaña
public class UsuarioDelegacion {

	
	private Integer idUsuario;
	private String usuario;
	private String idArea;
	private String idEmpleado;
	private String identidadfederativa;
	private String nombreentidadfederativa;
	
	private List<String> listaRoles;
		
	
	public UsuarioDelegacion(){
		
	}
	
	/***
	 * 
	 * @param idUsuario
	 * @param usuario
	 * @param jerarquia
	 * @param idEmpleado
	 * @param lista roles
	 */
	public UsuarioDelegacion(Integer idUsuario, String usuario, String idEmpleado, Object[] listaRolesParam, String identidadfederativa, String nombreentidadfederativa){
		
		this.idUsuario = idUsuario;
		this.usuario = usuario;
		this.idEmpleado = idEmpleado;
		//this.listaRoles = listaRoles;
		this.identidadfederativa = identidadfederativa;
		this.nombreentidadfederativa = nombreentidadfederativa;
		
		
		listaRoles = new ArrayList<String>();
		for (Object cad : listaRolesParam ){
			listaRoles.add(cad.toString());
		}
	}
	
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getIdArea() {
		return idArea;
	}
	public void setIdArea(String idArea) {
		this.idArea = idArea;
	}
	public String getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public List<String> getListaRoles() {
		return listaRoles;
	}

	public String getIdentidadfederativa() {
		return identidadfederativa;
	}

	public void setIdentidadfederativa(String identidadfederativa) {
		this.identidadfederativa = identidadfederativa;
	}

	public String getNombreentidadfederativa() {
		return nombreentidadfederativa;
	}

	public void setNombreentidadfederativa(String nombreentidadfederativa) {
		this.nombreentidadfederativa = nombreentidadfederativa;
	}

//	public void setListaRoles(List<String> listaRoles) {
//		this.listaRoles = listaRoles;
//	}
	
	
	
	
	
}

