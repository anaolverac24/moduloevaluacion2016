/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "ENTIDAD_FEDERATIVA", schema = "CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EntidadFederativa.findAll", query = "SELECT e FROM EntidadFederativa e"),
    @NamedQuery(name = "EntidadFederativa.findByIdEntidadFederativa", query = "SELECT e FROM EntidadFederativa e WHERE e.idEntidadFederativa = :idEntidadFederativa"),
    @NamedQuery(name = "EntidadFederativa.findByNombreEntidadFederativa", query = "SELECT e FROM EntidadFederativa e WHERE e.nombreEntidadFederativa = :nombreEntidadFederativa"),
    @NamedQuery(name = "EntidadFederativa.findBySiglas", query = "SELECT e FROM EntidadFederativa e WHERE e.siglas = :siglas"),
    @NamedQuery(name = "EntidadFederativa.findByCostero", query = "SELECT e FROM EntidadFederativa e WHERE e.costero = :costero"),
    @NamedQuery(name = "EntidadFederativa.findByAbreviacion", query = "SELECT e FROM EntidadFederativa e WHERE e.abreviacion = :abreviacion"),
    @NamedQuery(name = "EntidadFederativa.findByAreaHectarea", query = "SELECT e FROM EntidadFederativa e WHERE e.areaHectarea = :areaHectarea"),
    @NamedQuery(name = "EntidadFederativa.findByCapital", query = "SELECT e FROM EntidadFederativa e WHERE e.capital = :capital"),
    @NamedQuery(name = "EntidadFederativa.findByEntgeolamnn", query = "SELECT e FROM EntidadFederativa e WHERE e.entgeolamnn = :entgeolamnn"),
    @NamedQuery(name = "EntidadFederativa.findByEntgeolamxn", query = "SELECT e FROM EntidadFederativa e WHERE e.entgeolamxn = :entgeolamxn"),
    @NamedQuery(name = "EntidadFederativa.findByEntgeolomnn", query = "SELECT e FROM EntidadFederativa e WHERE e.entgeolomnn = :entgeolomnn"),
    @NamedQuery(name = "EntidadFederativa.findByEntgeolomxn", query = "SELECT e FROM EntidadFederativa e WHERE e.entgeolomxn = :entgeolomxn")})
public class EntidadFederativa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ID_ENTIDAD_FEDERATIVA")
    private String idEntidadFederativa;
    @Size(max = 40)
    @Column(name = "NOMBRE_ENTIDAD_FEDERATIVA")
    private String nombreEntidadFederativa;
    @Size(max = 5)
    @Column(name = "SIGLAS")
    private String siglas;
    @Column(name = "COSTERO")
    private Character costero;
    @Size(max = 10)
    @Column(name = "ABREVIACION")
    private String abreviacion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AREA_HECTAREA")
    private BigDecimal areaHectarea;
    @Size(max = 40)
    @Column(name = "CAPITAL")
    private String capital;
    @Column(name = "ENTGEOLAMNN")
    private BigDecimal entgeolamnn;
    @Column(name = "ENTGEOLAMXN")
    private BigDecimal entgeolamxn;
    @Column(name = "ENTGEOLOMNN")
    private BigDecimal entgeolomnn;
    @Column(name = "ENTGEOLOMXN")
    private BigDecimal entgeolomxn;
    @OneToMany(mappedBy = "entidadFederativa")
    private List<DelegacionMunicipio> delegacionMunicipioList;

    public EntidadFederativa() {
    }

    public EntidadFederativa(String idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    public String getIdEntidadFederativa() {
        return idEntidadFederativa;
    }

    public void setIdEntidadFederativa(String idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    public String getNombreEntidadFederativa() {
        return nombreEntidadFederativa;
    }

    public void setNombreEntidadFederativa(String nombreEntidadFederativa) {
        this.nombreEntidadFederativa = nombreEntidadFederativa;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public Character getCostero() {
        return costero;
    }

    public void setCostero(Character costero) {
        this.costero = costero;
    }

    public String getAbreviacion() {
        return abreviacion;
    }

    public void setAbreviacion(String abreviacion) {
        this.abreviacion = abreviacion;
    }

    public BigDecimal getAreaHectarea() {
        return areaHectarea;
    }

    public void setAreaHectarea(BigDecimal areaHectarea) {
        this.areaHectarea = areaHectarea;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public BigDecimal getEntgeolamnn() {
        return entgeolamnn;
    }

    public void setEntgeolamnn(BigDecimal entgeolamnn) {
        this.entgeolamnn = entgeolamnn;
    }

    public BigDecimal getEntgeolamxn() {
        return entgeolamxn;
    }

    public void setEntgeolamxn(BigDecimal entgeolamxn) {
        this.entgeolamxn = entgeolamxn;
    }

    public BigDecimal getEntgeolomnn() {
        return entgeolomnn;
    }

    public void setEntgeolomnn(BigDecimal entgeolomnn) {
        this.entgeolomnn = entgeolomnn;
    }

    public BigDecimal getEntgeolomxn() {
        return entgeolomxn;
    }

    public void setEntgeolomxn(BigDecimal entgeolomxn) {
        this.entgeolomxn = entgeolomxn;
    }

    @XmlTransient
    public List<DelegacionMunicipio> getDelegacionMunicipioList() {
        return delegacionMunicipioList;
    }

    public void setDelegacionMunicipioList(List<DelegacionMunicipio> delegacionMunicipioList) {
        this.delegacionMunicipioList = delegacionMunicipioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntidadFederativa != null ? idEntidadFederativa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntidadFederativa)) {
            return false;
        }
        EntidadFederativa other = (EntidadFederativa) object;
        if ((this.idEntidadFederativa == null && other.idEntidadFederativa != null) || (this.idEntidadFederativa != null && !this.idEntidadFederativa.equals(other.idEntidadFederativa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.catalogos.EntidadFederativa[ idEntidadFederativa=" + idEntidadFederativa + " ]";
    }
    
}
