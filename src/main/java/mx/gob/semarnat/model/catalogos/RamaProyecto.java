/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "RAMA_PROYECTO", schema="CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RamaProyecto.findAll", query = "SELECT r FROM RamaProyecto r"),
    @NamedQuery(name = "RamaProyecto.findByNrama", query = "SELECT r FROM RamaProyecto r WHERE r.nrama = :nrama"),
    @NamedQuery(name = "RamaProyecto.findByNsub", query = "SELECT r FROM RamaProyecto r WHERE r.nsub = :nsub"),
    @NamedQuery(name = "RamaProyecto.findByRama", query = "SELECT r FROM RamaProyecto r WHERE r.rama = :rama"),
    @NamedQuery(name = "RamaProyecto.findByEstatus", query = "SELECT r FROM RamaProyecto r WHERE r.estatus = :estatus")})
public class RamaProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NRAMA")
    private Short nrama;
    @Column(name = "NSUB")
    private Short nsub;
    @Basic(optional = false)
    @Column(name = "RAMA")
    private String rama;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private short estatus;

    public RamaProyecto() {
    }

    public RamaProyecto(Short nrama) {
        this.nrama = nrama;
    }

    public RamaProyecto(Short nrama, String rama, short estatus) {
        this.nrama = nrama;
        this.rama = rama;
        this.estatus = estatus;
    }

    public Short getNrama() {
        return nrama;
    }

    public void setNrama(Short nrama) {
        this.nrama = nrama;
    }

    public Short getNsub() {
        return nsub;
    }

    public void setNsub(Short nsub) {
        this.nsub = nsub;
    }

    public String getRama() {
        return rama;
    }

    public void setRama(String rama) {
        this.rama = rama;
    }

    public short getEstatus() {
        return estatus;
    }

    public void setEstatus(short estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nrama != null ? nrama.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RamaProyecto)) {
            return false;
        }
        RamaProyecto other = (RamaProyecto) object;
        if ((this.nrama == null && other.nrama != null) || (this.nrama != null && !this.nrama.equals(other.nrama))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.RamaProyecto[ nrama=" + nrama + " ]";
    }
    
}
