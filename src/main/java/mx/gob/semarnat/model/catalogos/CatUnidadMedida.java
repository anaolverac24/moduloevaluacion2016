/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "CAT_UNIDAD_MEDIDA", schema="CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatUnidadMedida.findAll", query = "SELECT c FROM CatUnidadMedida c"),
    @NamedQuery(name = "CatUnidadMedida.findByCtunClve", query = "SELECT c FROM CatUnidadMedida c WHERE c.ctunClve = :ctunClve"),
    @NamedQuery(name = "CatUnidadMedida.findByCtunAbre", query = "SELECT c FROM CatUnidadMedida c WHERE c.ctunAbre = :ctunAbre"),
    @NamedQuery(name = "CatUnidadMedida.findByCtunDesc", query = "SELECT c FROM CatUnidadMedida c WHERE c.ctunDesc = :ctunDesc"),
    @NamedQuery(name = "CatUnidadMedida.findByCtunTipo", query = "SELECT c FROM CatUnidadMedida c WHERE c.ctunTipo = :ctunTipo"),
    @NamedQuery(name = "CatUnidadMedida.findByCtunFac", query = "SELECT c FROM CatUnidadMedida c WHERE c.ctunFac = :ctunFac"),
    @NamedQuery(name = "CatUnidadMedida.findByEstatus", query = "SELECT c FROM CatUnidadMedida c WHERE c.estatus = :estatus")})
public class CatUnidadMedida implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CTUN_CLVE")
    private Short ctunClve;
    @Basic(optional = false)
    @Column(name = "CTUN_ABRE")
    private String ctunAbre;
    @Basic(optional = false)
    @Column(name = "CTUN_DESC")
    private String ctunDesc;
    @Column(name = "CTUN_TIPO")
    private String ctunTipo;
    @Column(name = "CTUN_FAC")
    private String ctunFac;
    @Basic(optional = false)
    @Column(name = "ESTATUS")
    private short estatus;

    public CatUnidadMedida() {
    }

    public CatUnidadMedida(Short ctunClve) {
        this.ctunClve = ctunClve;
    }

    public CatUnidadMedida(Short ctunClve, String ctunAbre, String ctunDesc, short estatus) {
        this.ctunClve = ctunClve;
        this.ctunAbre = ctunAbre;
        this.ctunDesc = ctunDesc;
        this.estatus = estatus;
    }

    public Short getCtunClve() {
        return ctunClve;
    }

    public void setCtunClve(Short ctunClve) {
        this.ctunClve = ctunClve;
    }

    public String getCtunAbre() {
        return ctunAbre;
    }

    public void setCtunAbre(String ctunAbre) {
        this.ctunAbre = ctunAbre;
    }

    public String getCtunDesc() {
        return ctunDesc;
    }

    public void setCtunDesc(String ctunDesc) {
        this.ctunDesc = ctunDesc;
    }

    public String getCtunTipo() {
        return ctunTipo;
    }

    public void setCtunTipo(String ctunTipo) {
        this.ctunTipo = ctunTipo;
    }

    public String getCtunFac() {
        return ctunFac;
    }

    public void setCtunFac(String ctunFac) {
        this.ctunFac = ctunFac;
    }

    public short getEstatus() {
        return estatus;
    }

    public void setEstatus(short estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ctunClve != null ? ctunClve.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatUnidadMedida)) {
            return false;
        }
        CatUnidadMedida other = (CatUnidadMedida) object;
        if ((this.ctunClve == null && other.ctunClve != null) || (this.ctunClve != null && !this.ctunClve.equals(other.ctunClve))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ctunClve+"";
    }
    
}
