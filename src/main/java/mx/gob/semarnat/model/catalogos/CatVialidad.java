/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Case Solutions 10
 */
@Entity
@Table(name = "CAT_VIALIDAD", schema="CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatVialidad.findAll", query = "SELECT c FROM CatVialidad c"),
    @NamedQuery(name = "CatVialidad.findByCveTipoVial", query = "SELECT c FROM CatVialidad c WHERE c.cveTipoVial = :cveTipoVial"),
    @NamedQuery(name = "CatVialidad.findByDescripcion", query = "SELECT c FROM CatVialidad c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CatVialidad.findByEstatus", query = "SELECT c FROM CatVialidad c WHERE c.estatus = :estatus")})
public class CatVialidad implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CVE_TIPO_VIAL")
    private Short cveTipoVial;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ESTATUS")
    private BigInteger estatus;

    public CatVialidad() {
    }

    public CatVialidad(Short cveTipoVial) {
        this.cveTipoVial = cveTipoVial;
    }

    public CatVialidad(Short cveTipoVial, String descripcion) {
        this.cveTipoVial = cveTipoVial;
        this.descripcion = descripcion;
    }

    public Short getCveTipoVial() {
        return cveTipoVial;
    }

    public void setCveTipoVial(Short cveTipoVial) {
        this.cveTipoVial = cveTipoVial;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cveTipoVial != null ? cveTipoVial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatVialidad)) {
            return false;
        }
        CatVialidad other = (CatVialidad) object;
        if ((this.cveTipoVial == null && other.cveTipoVial != null) || (this.cveTipoVial != null && !this.cveTipoVial.equals(other.cveTipoVial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.catalogos.CatVialidad[ cveTipoVial=" + cveTipoVial + " ]";
    }
    
}
