package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAT_TRATAMIENTOS")
public class CatTratamientos implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_TRATAMIENTO")
	public int id;
	
	@Column(name = "TRATAMIENTO")
	public String degree;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}
		
}
