/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "DELEGACION_MUNICIPIO", schema = "CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DelegacionMunicipio.findAll", query = "SELECT d FROM DelegacionMunicipio d"),
    @NamedQuery(name = "DelegacionMunicipio.findByIdDelegacionMunicipio", query = "SELECT d FROM DelegacionMunicipio d WHERE d.idDelegacionMunicipio = :idDelegacionMunicipio"),
    @NamedQuery(name = "DelegacionMunicipio.findByNombreDelegacionMunicipio", query = "SELECT d FROM DelegacionMunicipio d WHERE d.nombreDelegacionMunicipio = :nombreDelegacionMunicipio"),
    @NamedQuery(name = "DelegacionMunicipio.findByCostero", query = "SELECT d FROM DelegacionMunicipio d WHERE d.costero = :costero")})
public class DelegacionMunicipio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "ID_DELEGACION_MUNICIPIO")
    private String idDelegacionMunicipio;
    @Size(max = 100)
    @Column(name = "NOMBRE_DELEGACION_MUNICIPIO")
    private String nombreDelegacionMunicipio;
    @Column(name = "COSTERO")
    private Short costero;
    @JoinColumn(name = "ENTIDAD_FEDERATIVA", referencedColumnName = "ID_ENTIDAD_FEDERATIVA")
    @ManyToOne
    private EntidadFederativa entidadFederativa;

    public DelegacionMunicipio() {
    }

    public DelegacionMunicipio(String idDelegacionMunicipio) {
        this.idDelegacionMunicipio = idDelegacionMunicipio;
    }

    public String getIdDelegacionMunicipio() {
        return idDelegacionMunicipio;
    }

    public void setIdDelegacionMunicipio(String idDelegacionMunicipio) {
        this.idDelegacionMunicipio = idDelegacionMunicipio;
    }

    public String getNombreDelegacionMunicipio() {
        return nombreDelegacionMunicipio;
    }

    public void setNombreDelegacionMunicipio(String nombreDelegacionMunicipio) {
        this.nombreDelegacionMunicipio = nombreDelegacionMunicipio;
    }

    public Short getCostero() {
        return costero;
    }

    public void setCostero(Short costero) {
        this.costero = costero;
    }

    public EntidadFederativa getEntidadFederativa() {
        return entidadFederativa;
    }

    public void setEntidadFederativa(EntidadFederativa entidadFederativa) {
        this.entidadFederativa = entidadFederativa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDelegacionMunicipio != null ? idDelegacionMunicipio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DelegacionMunicipio)) {
            return false;
        }
        DelegacionMunicipio other = (DelegacionMunicipio) object;
        if ((this.idDelegacionMunicipio == null && other.idDelegacionMunicipio != null) || (this.idDelegacionMunicipio != null && !this.idDelegacionMunicipio.equals(other.idDelegacionMunicipio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.catalogos.DelegacionMunicipio[ idDelegacionMunicipio=" + idDelegacionMunicipio + " ]";
    }
    
}
