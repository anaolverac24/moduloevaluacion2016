package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Entity
@Table(name = "SECTOR", schema = "CATALOGOS")
@XmlRootElement
public class Sector implements Serializable {

	@Id
	@Column(name = "SEC_ID")
	@Basic(optional = false)
	private Short secId;

	@Column(name = "SEC_DESCR")
	private String secDescripcion;

	@Column(name = "ESTATUS")
	private int estatus;

	public Sector() {
	}

	public Sector(Short secId) {
		this.secId = secId;
	}

	public Short getSecId() {
		return secId;
	}

	public void setSecId(Short secId) {
		this.secId = secId;
	}

	public String getSecDescripcion() {
		return secDescripcion;
	}

	public void setSecDescripcion(String secDescripcion) {
		this.secDescripcion = secDescripcion;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "Sector [secId=" + secId + ", secDescripcion=" + secDescripcion + ", estatus=" + estatus + "]";
	}

}
