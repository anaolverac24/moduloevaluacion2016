/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "TIPO_PROYECTO", schema = "CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoProyecto.findAll", query = "SELECT t FROM TipoProyecto t"),
    @NamedQuery(name = "TipoProyecto.findByNtipo", query = "SELECT t FROM TipoProyecto t WHERE t.ntipo = :ntipo"),
    @NamedQuery(name = "TipoProyecto.findByNrama", query = "SELECT t FROM TipoProyecto t WHERE t.nrama = :nrama"),
    @NamedQuery(name = "TipoProyecto.findByTipoProyecto", query = "SELECT t FROM TipoProyecto t WHERE t.tipoProyecto = :tipoProyecto"),
    @NamedQuery(name = "TipoProyecto.findByEstatus", query = "SELECT t FROM TipoProyecto t WHERE t.estatus = :estatus")})
public class TipoProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "NTIPO")
    private Short ntipo;
    @Column(name = "NRAMA")
    private Short nrama;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TIPO_PROYECTO")
    private String tipoProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTATUS")
    private short estatus;

    public TipoProyecto() {
    }

    public TipoProyecto(Short ntipo) {
        this.ntipo = ntipo;
    }

    public TipoProyecto(Short ntipo, String tipoProyecto, short estatus) {
        this.ntipo = ntipo;
        this.tipoProyecto = tipoProyecto;
        this.estatus = estatus;
    }

    public Short getNtipo() {
        return ntipo;
    }

    public void setNtipo(Short ntipo) {
        this.ntipo = ntipo;
    }

    public Short getNrama() {
        return nrama;
    }

    public void setNrama(Short nrama) {
        this.nrama = nrama;
    }

    public String getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    public short getEstatus() {
        return estatus;
    }

    public void setEstatus(short estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ntipo != null ? ntipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoProyecto)) {
            return false;
        }
        TipoProyecto other = (TipoProyecto) object;
        if ((this.ntipo == null && other.ntipo != null) || (this.ntipo != null && !this.ntipo.equals(other.ntipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.catalogos.TipoProyecto[ ntipo=" + ntipo + " ]";
    }
    
}
