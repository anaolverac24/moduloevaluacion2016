/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogos;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Case Solutions 10
 */
@Entity
@Table(name = "CAT_TIPO_ASEN", schema="CATALOGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoAsen.findAll", query = "SELECT c FROM CatTipoAsen c"),
    @NamedQuery(name = "CatTipoAsen.findByCveTipoAsen", query = "SELECT c FROM CatTipoAsen c WHERE c.cveTipoAsen = :cveTipoAsen"),
    @NamedQuery(name = "CatTipoAsen.findByNombre", query = "SELECT c FROM CatTipoAsen c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CatTipoAsen.findByEstatus", query = "SELECT c FROM CatTipoAsen c WHERE c.estatus = :estatus")})
public class CatTipoAsen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CVE_TIPO_ASEN")
    private Short cveTipoAsen;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "ESTATUS")
    private BigInteger estatus;

    public CatTipoAsen() {
    }

    public CatTipoAsen(Short cveTipoAsen) {
        this.cveTipoAsen = cveTipoAsen;
    }

    public CatTipoAsen(Short cveTipoAsen, String nombre) {
        this.cveTipoAsen = cveTipoAsen;
        this.nombre = nombre;
    }

    public Short getCveTipoAsen() {
        return cveTipoAsen;
    }

    public void setCveTipoAsen(Short cveTipoAsen) {
        this.cveTipoAsen = cveTipoAsen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cveTipoAsen != null ? cveTipoAsen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoAsen)) {
            return false;
        }
        CatTipoAsen other = (CatTipoAsen) object;
        if ((this.cveTipoAsen == null && other.cveTipoAsen != null) || (this.cveTipoAsen != null && !this.cveTipoAsen.equals(other.cveTipoAsen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.catalogos.CatTipoAsen[ cveTipoAsen=" + cveTipoAsen + " ]";
    }
    
}
