/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.sinat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "SINAT_DGIRA", schema="SINAT_USDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SinatDgira.findAll", query = "SELECT s FROM SinatDgira s"),
    @NamedQuery(name = "SinatDgira.findById", query = "SELECT s FROM SinatDgira s WHERE s.id = :id"),
    @NamedQuery(name = "SinatDgira.findBySituacion", query = "SELECT s FROM SinatDgira s WHERE s.situacion = :situacion"),
    @NamedQuery(name = "SinatDgira.findByBitacora", query = "SELECT s FROM SinatDgira s WHERE s.bitacora = :bitacora"),
    @NamedQuery(name = "SinatDgira.findByIdEntidadFederativa", query = "SELECT s FROM SinatDgira s WHERE s.idEntidadFederativa = :idEntidadFederativa"),
    @NamedQuery(name = "SinatDgira.findByIdClaveTramite", query = "SELECT s FROM SinatDgira s WHERE s.idClaveTramite = :idClaveTramite"),
    @NamedQuery(name = "SinatDgira.findByIdTramite", query = "SELECT s FROM SinatDgira s WHERE s.idTramite = :idTramite"),
    @NamedQuery(name = "SinatDgira.findByFecha", query = "SELECT s FROM SinatDgira s WHERE s.fecha = :fecha"),
    @NamedQuery(name = "SinatDgira.findByIdAreaEnvio", query = "SELECT s FROM SinatDgira s WHERE s.idAreaEnvio = :idAreaEnvio"),
    @NamedQuery(name = "SinatDgira.findByIdAreaRecibe", query = "SELECT s FROM SinatDgira s WHERE s.idAreaRecibe = :idAreaRecibe"),
    @NamedQuery(name = "SinatDgira.findByIdUsuarioEnvio", query = "SELECT s FROM SinatDgira s WHERE s.idUsuarioEnvio = :idUsuarioEnvio"),
    @NamedQuery(name = "SinatDgira.findByIdUsuarioRecibe", query = "SELECT s FROM SinatDgira s WHERE s.idUsuarioRecibe = :idUsuarioRecibe"),
    @NamedQuery(name = "SinatDgira.findByObservaciones", query = "SELECT s FROM SinatDgira s WHERE s.observaciones = :observaciones"),
    @NamedQuery(name = "SinatDgira.findByCveDocto", query = "SELECT s FROM SinatDgira s WHERE s.cveDocto = :cveDocto"),
    @NamedQuery(name = "SinatDgira.findByGrupoTrabajo", query = "SELECT s FROM SinatDgira s WHERE s.grupoTrabajo = :grupoTrabajo")})
public class SinatDgira implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "SITUACION")
    private String situacion;
    @Basic(optional = false)
    @Column(name = "BITACORA")
    private String bitacora;
    @Basic(optional = false)
    @Column(name = "ID_ENTIDAD_FEDERATIVA")
    private String idEntidadFederativa;
    @Basic(optional = false)
    @Column(name = "ID_CLAVE_TRAMITE")
    private String idClaveTramite;
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE")
    private int idTramite;
    @Basic(optional = false)
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "ID_AREA_ENVIO")
    private String idAreaEnvio;
    @Basic(optional = false)
    @Column(name = "ID_AREA_RECIBE")
    private String idAreaRecibe;
    @Basic(optional = false)
    @Column(name = "ID_USUARIO_ENVIO")
    private int idUsuarioEnvio;
    @Basic(optional = false)
    @Column(name = "ID_USUARIO_RECIBE")
    private int idUsuarioRecibe;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "CVE_DOCTO")
    private String cveDocto;
    @Basic(optional = false)
    @Column(name = "GRUPO_TRABAJO")
    private short grupoTrabajo;

    public SinatDgira() {
    }

    public SinatDgira(Integer id) {
        this.id = id;
    }

    public SinatDgira(Integer id, String situacion, String bitacora, String idEntidadFederativa, String idClaveTramite, int idTramite, Date fecha, String idAreaEnvio, String idAreaRecibe, int idUsuarioEnvio, int idUsuarioRecibe, short grupoTrabajo) {
        this.id = id;
        this.situacion = situacion;
        this.bitacora = bitacora;
        this.idEntidadFederativa = idEntidadFederativa;
        this.idClaveTramite = idClaveTramite;
        this.idTramite = idTramite;
        this.fecha = fecha;
        this.idAreaEnvio = idAreaEnvio;
        this.idAreaRecibe = idAreaRecibe;
        this.idUsuarioEnvio = idUsuarioEnvio;
        this.idUsuarioRecibe = idUsuarioRecibe;
        this.grupoTrabajo = grupoTrabajo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getIdEntidadFederativa() {
        return idEntidadFederativa;
    }

    public void setIdEntidadFederativa(String idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    public String getIdClaveTramite() {
        return idClaveTramite;
    }

    public void setIdClaveTramite(String idClaveTramite) {
        this.idClaveTramite = idClaveTramite;
    }

    public int getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(int idTramite) {
        this.idTramite = idTramite;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIdAreaEnvio() {
        return idAreaEnvio;
    }

    public void setIdAreaEnvio(String idAreaEnvio) {
        this.idAreaEnvio = idAreaEnvio;
    }

    public String getIdAreaRecibe() {
        return idAreaRecibe;
    }

    public void setIdAreaRecibe(String idAreaRecibe) {
        this.idAreaRecibe = idAreaRecibe;
    }

    public int getIdUsuarioEnvio() {
        return idUsuarioEnvio;
    }

    public void setIdUsuarioEnvio(int idUsuarioEnvio) {
        this.idUsuarioEnvio = idUsuarioEnvio;
    }

    public int getIdUsuarioRecibe() {
        return idUsuarioRecibe;
    }

    public void setIdUsuarioRecibe(int idUsuarioRecibe) {
        this.idUsuarioRecibe = idUsuarioRecibe;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getCveDocto() {
        return cveDocto;
    }

    public void setCveDocto(String cveDocto) {
        this.cveDocto = cveDocto;
    }

    public short getGrupoTrabajo() {
        return grupoTrabajo;
    }

    public void setGrupoTrabajo(short grupoTrabajo) {
        this.grupoTrabajo = grupoTrabajo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SinatDgira)) {
            return false;
        }
        SinatDgira other = (SinatDgira) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.SinatDgira[ id=" + id + " ]";
    }
    
}
