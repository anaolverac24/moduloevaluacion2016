package mx.gob.semarnat.model.sinat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ana Olvera
 */
@Entity
@Table(name = "SINAT_RESOLUCION", schema = "SINAT_USDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SinatResolucion.findAll", query = "SELECT s FROM SinatResolucion s")
    , @NamedQuery(name = "SinatResolucion.findById", query = "SELECT s FROM SinatResolucion s WHERE s.id = :id")
    , @NamedQuery(name = "SinatResolucion.findByBitacora", query = "SELECT s FROM SinatResolucion s WHERE s.bitacora = :bitacora")
    , @NamedQuery(name = "SinatResolucion.findByFolioResolucion", query = "SELECT s FROM SinatResolucion s WHERE s.folioResolucion = :folioResolucion")
    , @NamedQuery(name = "SinatResolucion.findByIdModoResolucion", query = "SELECT s FROM SinatResolucion s WHERE s.idModoResolucion = :idModoResolucion")
    , @NamedQuery(name = "SinatResolucion.findByVigIndefinida", query = "SELECT s FROM SinatResolucion s WHERE s.vigIndefinida = :vigIndefinida")
    , @NamedQuery(name = "SinatResolucion.findByFormatoVigencia", query = "SELECT s FROM SinatResolucion s WHERE s.formatoVigencia = :formatoVigencia")
    , @NamedQuery(name = "SinatResolucion.findByInicioVigencia", query = "SELECT s FROM SinatResolucion s WHERE s.inicioVigencia = :inicioVigencia")
    , @NamedQuery(name = "SinatResolucion.findByFinVigencia", query = "SELECT s FROM SinatResolucion s WHERE s.finVigencia = :finVigencia")
    , @NamedQuery(name = "SinatResolucion.findByFechaResolucion", query = "SELECT s FROM SinatResolucion s WHERE s.fechaResolucion = :fechaResolucion")
    , @NamedQuery(name = "SinatResolucion.findByFechaRegistro", query = "SELECT s FROM SinatResolucion s WHERE s.fechaRegistro = :fechaRegistro")
    , @NamedQuery(name = "SinatResolucion.findByIdUsuarioRegistro", query = "SELECT s FROM SinatResolucion s WHERE s.idUsuarioRegistro = :idUsuarioRegistro")
    , @NamedQuery(name = "SinatResolucion.findByIdUsuarioModifica", query = "SELECT s FROM SinatResolucion s WHERE s.idUsuarioModifica = :idUsuarioModifica")
    , @NamedQuery(name = "SinatResolucion.findByAnioVigencia", query = "SELECT s FROM SinatResolucion s WHERE s.anioVigencia = :anioVigencia")
    , @NamedQuery(name = "SinatResolucion.findByMesVigencia", query = "SELECT s FROM SinatResolucion s WHERE s.mesVigencia = :mesVigencia")
    , @NamedQuery(name = "SinatResolucion.findByDiaVigencia", query = "SELECT s FROM SinatResolucion s WHERE s.diaVigencia = :diaVigencia")})
public class SinatResolucion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private int id;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "BITACORA")
    private String bitacora;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "FOLIO_RESOLUCION")
    private String folioResolucion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_MODO_RESOLUCION")
    private short idModoResolucion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIG_INDEFINIDA")
    private short vigIndefinida;
    @Column(name = "FORMATO_VIGENCIA")
    private Character formatoVigencia;
    @Column(name = "INICIO_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicioVigencia;
    @Column(name = "FIN_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finVigencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_RESOLUCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaResolucion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USUARIO_REGISTRO")
    private int idUsuarioRegistro;
    @Column(name = "ID_USUARIO_MODIFICA")
    private Integer idUsuarioModifica;
    @Column(name = "ANIO_VIGENCIA")
    private Short anioVigencia;
    @Column(name = "MES_VIGENCIA")
    private Short mesVigencia;
    @Column(name = "DIA_VIGENCIA")
    private Short diaVigencia;

    public SinatResolucion() {
    }

    public SinatResolucion(String bitacora) {
        this.bitacora = bitacora;
    }

    public SinatResolucion(String bitacora, int id, String folioResolucion, short idModoResolucion, short vigIndefinida, Date fechaResolucion, Date fechaRegistro, int idUsuarioRegistro) {
        this.bitacora = bitacora;
        this.id = id;
        this.folioResolucion = folioResolucion;
        this.idModoResolucion = idModoResolucion;
        this.vigIndefinida = vigIndefinida;
        this.fechaResolucion = fechaResolucion;
        this.fechaRegistro = fechaRegistro;
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getFolioResolucion() {
        return folioResolucion;
    }

    public void setFolioResolucion(String folioResolucion) {
        this.folioResolucion = folioResolucion;
    }

    public short getIdModoResolucion() {
        return idModoResolucion;
    }

    public void setIdModoResolucion(short idModoResolucion) {
        this.idModoResolucion = idModoResolucion;
    }

    public short getVigIndefinida() {
        return vigIndefinida;
    }

    public void setVigIndefinida(short vigIndefinida) {
        this.vigIndefinida = vigIndefinida;
    }

    public Character getFormatoVigencia() {
        return formatoVigencia;
    }

    public void setFormatoVigencia(Character formatoVigencia) {
        this.formatoVigencia = formatoVigencia;
    }

    public Date getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(Date inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public Date getFinVigencia() {
        return finVigencia;
    }

    public void setFinVigencia(Date finVigencia) {
        this.finVigencia = finVigencia;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public int getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(int idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Integer getIdUsuarioModifica() {
        return idUsuarioModifica;
    }

    public void setIdUsuarioModifica(Integer idUsuarioModifica) {
        this.idUsuarioModifica = idUsuarioModifica;
    }

    public Short getAnioVigencia() {
        return anioVigencia;
    }

    public void setAnioVigencia(Short anioVigencia) {
        this.anioVigencia = anioVigencia;
    }

    public Short getMesVigencia() {
        return mesVigencia;
    }

    public void setMesVigencia(Short mesVigencia) {
        this.mesVigencia = mesVigencia;
    }

    public Short getDiaVigencia() {
        return diaVigencia;
    }

    public void setDiaVigencia(Short diaVigencia) {
        this.diaVigencia = diaVigencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacora != null ? bitacora.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SinatResolucion)) {
            return false;
        }
        SinatResolucion other = (SinatResolucion) object;
        if ((this.bitacora == null && other.bitacora != null) || (this.bitacora != null && !this.bitacora.equals(other.bitacora))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.sinat.SinatResolucion[ bitacora=" + bitacora + " ]";
    }

}
