/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.sinat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "SINAT_SINATEC", schema="SINAT_USDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SinatSinatec.findAll", query = "SELECT s FROM SinatSinatec s"),
    @NamedQuery(name = "SinatSinatec.findByFolio", query = "SELECT s FROM SinatSinatec s WHERE s.folio = :folio"),
    @NamedQuery(name = "SinatSinatec.findByBitacora", query = "SELECT s FROM SinatSinatec s WHERE s.bitacora = :bitacora"),
    @NamedQuery(name = "SinatSinatec.findByProyecto", query = "SELECT s FROM SinatSinatec s WHERE s.proyecto = :proyecto"),
    @NamedQuery(name = "SinatSinatec.findByUrlConstancia", query = "SELECT s FROM SinatSinatec s WHERE s.urlConstancia = :urlConstancia"),
    @NamedQuery(name = "SinatSinatec.findByMensaje", query = "SELECT s FROM SinatSinatec s WHERE s.mensaje = :mensaje"),
    @NamedQuery(name = "SinatSinatec.findByFecha", query = "SELECT s FROM SinatSinatec s WHERE s.fecha = :fecha")})
public class SinatSinatec implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "FOLIO")
    private BigDecimal folio;
    @Column(name = "BITACORA")
    private String bitacora;
    @Column(name = "PROYECTO")
    private String proyecto;
    @Column(name = "URL_CONSTANCIA")
    private String urlConstancia;
    @Column(name = "MENSAJE")
    private String mensaje;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public SinatSinatec() {
    }

    public SinatSinatec(BigDecimal folio) {
        this.folio = folio;
    }

    public BigDecimal getFolio() {
        return folio;
    }

    public void setFolio(BigDecimal folio) {
        this.folio = folio;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getUrlConstancia() {
        return urlConstancia;
    }

    public void setUrlConstancia(String urlConstancia) {
        this.urlConstancia = urlConstancia;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folio != null ? folio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SinatSinatec)) {
            return false;
        }
        SinatSinatec other = (SinatSinatec) object;
        if ((this.folio == null && other.folio != null) || (this.folio != null && !this.folio.equals(other.folio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.SinatSinatec[ folio=" + folio + " ]";
    }
    
}
