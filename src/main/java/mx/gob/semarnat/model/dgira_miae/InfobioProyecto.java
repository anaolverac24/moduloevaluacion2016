/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author EdgarSC
 */
@Entity
@Table(name = "INFOBIO_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InfobioProyecto.findAll", query = "SELECT i FROM InfobioProyecto i"),
    
    @NamedQuery(name = "InfobioProyecto.findByFolioProyecto", query = "SELECT i FROM InfobioProyecto i WHERE i.infobioProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "InfobioProyecto.countByFolioProyecto", query = "SELECT COUNT(i) FROM InfobioProyecto i WHERE i.infobioProyectoPK.folioProyecto = :folioProyecto"),
    
    
    @NamedQuery(name = "InfobioProyecto.findByPK", query = "SELECT i FROM InfobioProyecto i WHERE i.infobioProyectoPK.serialProyecto = :serialProyecto"
    		+ " AND i.infobioProyectoPK.folioProyecto = :folioProyecto AND i.infobioProyectoPK.infoBioId = :infoBioId"),
    
    @NamedQuery(name = "InfobioProyecto.findBySerialProyecto", query = "SELECT i FROM InfobioProyecto i WHERE i.infobioProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "InfobioProyecto.findByInfoBioId", query = "SELECT i FROM InfobioProyecto i WHERE i.infobioProyectoPK.infoBioId = :infoBioId"),
    @NamedQuery(name = "InfobioProyecto.findByBioNombre", query = "SELECT i FROM InfobioProyecto i WHERE i.bioNombre = :bioNombre"),
    @NamedQuery(name = "InfobioProyecto.findByBioOrigen", query = "SELECT i FROM InfobioProyecto i WHERE i.bioOrigen = :bioOrigen"),
    @NamedQuery(name = "InfobioProyecto.findByBioNumorgCultivar", query = "SELECT i FROM InfobioProyecto i WHERE i.bioNumorgCultivar = :bioNumorgCultivar"),
    @NamedQuery(name = "InfobioProyecto.findByBioDescAtributosAme", query = "SELECT i FROM InfobioProyecto i WHERE i.bioDescAtributosAme = :bioDescAtributosAme"),
    @NamedQuery(name = "InfobioProyecto.findByBioEspForrajera", query = "SELECT i FROM InfobioProyecto i WHERE i.bioEspForrajera = :bioEspForrajera"),
    @NamedQuery(name = "InfobioProyecto.findByBioCrias", query = "SELECT i FROM InfobioProyecto i WHERE i.bioCrias = :bioCrias"),
    @NamedQuery(name = "InfobioProyecto.findByBioSemillas", query = "SELECT i FROM InfobioProyecto i WHERE i.bioSemillas = :bioSemillas"),
    @NamedQuery(name = "InfobioProyecto.findByBioPostlarvas", query = "SELECT i FROM InfobioProyecto i WHERE i.bioPostlarvas = :bioPostlarvas"),
    @NamedQuery(name = "InfobioProyecto.findByBioJuveniles", query = "SELECT i FROM InfobioProyecto i WHERE i.bioJuveniles = :bioJuveniles"),
    @NamedQuery(name = "InfobioProyecto.findByBioAdultReprod", query = "SELECT i FROM InfobioProyecto i WHERE i.bioAdultReprod = :bioAdultReprod"),
    @NamedQuery(name = "InfobioProyecto.findByBioNumciclos", query = "SELECT i FROM InfobioProyecto i WHERE i.bioNumciclos = :bioNumciclos"),
    @NamedQuery(name = "InfobioProyecto.findByBioBiomasaInic", query = "SELECT i FROM InfobioProyecto i WHERE i.bioBiomasaInic = :bioBiomasaInic"),
    @NamedQuery(name = "InfobioProyecto.findByBioUnidadInic", query = "SELECT i FROM InfobioProyecto i WHERE i.bioUnidadInic = :bioUnidadInic"),
    @NamedQuery(name = "InfobioProyecto.findByBioBiomasaEsperada", query = "SELECT i FROM InfobioProyecto i WHERE i.bioBiomasaEsperada = :bioBiomasaEsperada"),
    @NamedQuery(name = "InfobioProyecto.findByBioUnidadEsperada", query = "SELECT i FROM InfobioProyecto i WHERE i.bioUnidadEsperada = :bioUnidadEsperada"),
    @NamedQuery(name = "InfobioProyecto.findByBioTipoAlimento", query = "SELECT i FROM InfobioProyecto i WHERE i.bioTipoAlimento = :bioTipoAlimento"),
    @NamedQuery(name = "InfobioProyecto.findByBioCantAlimento", query = "SELECT i FROM InfobioProyecto i WHERE i.bioCantAlimento = :bioCantAlimento"),
    @NamedQuery(name = "InfobioProyecto.findByBioFormaAlmacen", query = "SELECT i FROM InfobioProyecto i WHERE i.bioFormaAlmacen = :bioFormaAlmacen"),
    @NamedQuery(name = "InfobioProyecto.findByBioTipabono", query = "SELECT i FROM InfobioProyecto i WHERE i.bioTipabono = :bioTipabono"),
    @NamedQuery(name = "InfobioProyecto.findByBioUsoAbono", query = "SELECT i FROM InfobioProyecto i WHERE i.bioUsoAbono = :bioUsoAbono"),
    @NamedQuery(name = "InfobioProyecto.findByBioCantSuministro", query = "SELECT i FROM InfobioProyecto i WHERE i.bioCantSuministro = :bioCantSuministro"),
    @NamedQuery(name = "InfobioProyecto.findByBioFormAlmacAbono", query = "SELECT i FROM InfobioProyecto i WHERE i.bioFormAlmacAbono = :bioFormAlmacAbono")})
public class InfobioProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InfobioProyectoPK infobioProyectoPK;
    @Column(name = "BIO_NOMBRE")
    private String bioNombre;
    @Column(name = "BIO_ORIGEN")
    private String bioOrigen;
    @Column(name = "BIO_NUMORG_CULTIVAR")
    private String bioNumorgCultivar;
    @Column(name = "BIO_DESC_ATRIBUTOS_AME")
    private String bioDescAtributosAme;
    @Column(name = "BIO_ESP_FORRAJERA")
    private String bioEspForrajera;
    @Column(name = "BIO_CRIAS")
    private String bioCrias;
    @Column(name = "BIO_SEMILLAS")
    private String bioSemillas;
    @Column(name = "BIO_POSTLARVAS")
    private String bioPostlarvas;
    @Column(name = "BIO_JUVENILES")
    private String bioJuveniles;
    @Column(name = "BIO_ADULT_REPROD")
    private String bioAdultReprod;
    @Column(name = "BIO_NUMCICLOS")
    private Long bioNumciclos;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BIO_BIOMASA_INIC")
    private BigDecimal bioBiomasaInic;
    @Column(name = "BIO_UNIDAD_INIC")
    private String bioUnidadInic;
    @Column(name = "BIO_BIOMASA_ESPERADA")
    private BigDecimal bioBiomasaEsperada;
    @Column(name = "BIO_UNIDAD_ESPERADA")
    private String bioUnidadEsperada;
    @Column(name = "BIO_TIPO_ALIMENTO")
    private String bioTipoAlimento;
    @Column(name = "BIO_CANT_ALIMENTO")
    private BigDecimal bioCantAlimento;
    @Column(name = "BIO_FORMA_ALMACEN")
    private String bioFormaAlmacen;
    
    @Column(name = "BIO_UNIDAD_ALIMENTO")
    private String bioUnidadAlmacen;
    
    @Column(name = "BIO_TIPABONO")
    private String bioTipabono;
    @Column(name = "BIO_USO_ABONO")
    private String bioUsoAbono;
    @Column(name = "BIO_CANT_SUMINISTRO")
    private BigDecimal bioCantSuministro;
    
    @Column(name = "BIO_UNIDAD_SUMINISTRO")
    private String bioUnidadSuministro;
    
    @Column(name = "BIO_FORM_ALMAC_ABONO")
    private String bioFormAlmacAbono;
    
    
    
    
    
    @JoinColumns({
        @JoinColumn(name = "ID_ESPECIES", referencedColumnName = "ID", insertable = true, updatable = true)})
    @ManyToOne(optional = true)
    private CatEspeciesAcuiferos especiesAcuiferos;
    
    
    public InfobioProyecto() {
    }

    public InfobioProyecto(InfobioProyectoPK infobioProyectoPK) {
        this.infobioProyectoPK = infobioProyectoPK;
    }

    public InfobioProyecto(String folioProyecto, short serialProyecto, short infoBioId) {
        this.infobioProyectoPK = new InfobioProyectoPK(folioProyecto, serialProyecto, infoBioId);
    }

    public InfobioProyectoPK getInfobioProyectoPK() {
        return infobioProyectoPK;
    }

    public void setInfobioProyectoPK(InfobioProyectoPK infobioProyectoPK) {
        this.infobioProyectoPK = infobioProyectoPK;
    }

    public String getBioNombre() {
        return bioNombre;
    }

    public void setBioNombre(String bioNombre) {
        this.bioNombre = bioNombre;
    }

    public String getBioOrigen() {
        return bioOrigen;
    }

    public void setBioOrigen(String bioOrigen) {
        this.bioOrigen = bioOrigen;
    }

    public String getBioNumorgCultivar() {
        return bioNumorgCultivar;
    }

    public void setBioNumorgCultivar(String bioNumorgCultivar) {
        this.bioNumorgCultivar = bioNumorgCultivar;
    }

    public String getBioDescAtributosAme() {
        return bioDescAtributosAme;
    }

    public void setBioDescAtributosAme(String bioDescAtributosAme) {
        this.bioDescAtributosAme = bioDescAtributosAme;
    }

    public String getBioEspForrajera() {
    	if (bioEspForrajera != null) {
			if (bioEspForrajera.equals("S")) {
				return "Si";
			}
			if (bioEspForrajera.equals("N")) {
				return "No";
			}
		}
        return bioEspForrajera;
    }

    public void setBioEspForrajera(String bioEspForrajera) {
        this.bioEspForrajera = bioEspForrajera;
    }

    public String getBioCrias() {
    	if (bioCrias != null) {
			if (bioCrias.equals("S")) {
				return "Si";
			}
			if (bioCrias.equals("N")) {
				return "No";
			}
		}
        return bioCrias;
    }

    public void setBioCrias(String bioCrias) {
        this.bioCrias = bioCrias;
    }

    public String getBioSemillas() {
    	if (bioSemillas != null) {
			if (bioSemillas.equals("S")) {
				return "Si";
			}
			if (bioSemillas.equals("N")) {
				return "No";
			}
		}
        return bioSemillas;
    }

    public void setBioSemillas(String bioSemillas) {
        this.bioSemillas = bioSemillas;
    }

    public String getBioPostlarvas() {
    	if (bioPostlarvas != null) {
			if (bioPostlarvas.equals("S")) {
				return "Si";
			}
			if (bioPostlarvas.equals("N")) {
				return "No";
			}
		}
        return bioPostlarvas;
    }

    public void setBioPostlarvas(String bioPostlarvas) {
        this.bioPostlarvas = bioPostlarvas;
    }

    public String getBioJuveniles() {
    	if (bioJuveniles != null) {
			if (bioJuveniles.equals("S")) {
				return "Si";
			}
			if (bioJuveniles.equals("N")) {
				return "No";
			}
		}
        return bioJuveniles;
    }

    public void setBioJuveniles(String bioJuveniles) {
        this.bioJuveniles = bioJuveniles;
    }

    public String getBioAdultReprod() {
    	if (bioAdultReprod != null) {
			if (bioAdultReprod.equals("S")) {
				return "Si";
			}
			if (bioAdultReprod.equals("N")) {
				return "No";
			}
		}
        return bioAdultReprod;
    }

    public void setBioAdultReprod(String bioAdultReprod) {
        this.bioAdultReprod = bioAdultReprod;
    }

    public Long getBioNumciclos() {
        return bioNumciclos;
    }

    public void setBioNumciclos(Long bioNumciclos) {
        this.bioNumciclos = bioNumciclos;
    }

    public BigDecimal getBioBiomasaInic() {
        return bioBiomasaInic;
    }

    public void setBioBiomasaInic(BigDecimal bioBiomasaInic) {
        this.bioBiomasaInic = bioBiomasaInic;
    }

    public String getBioUnidadInic() {
        return bioUnidadInic;
    }

    public void setBioUnidadInic(String bioUnidadInic) {
        this.bioUnidadInic = bioUnidadInic;
    }

    public BigDecimal getBioBiomasaEsperada() {
        return bioBiomasaEsperada;
    }

    public void setBioBiomasaEsperada(BigDecimal bioBiomasaEsperada) {
        this.bioBiomasaEsperada = bioBiomasaEsperada;
    }

    public String getBioUnidadEsperada() {
        return bioUnidadEsperada;
    }

    public void setBioUnidadEsperada(String bioUnidadEsperada) {
        this.bioUnidadEsperada = bioUnidadEsperada;
    }

    public String getBioTipoAlimento() {
        return bioTipoAlimento;
    }

    public void setBioTipoAlimento(String bioTipoAlimento) {
        this.bioTipoAlimento = bioTipoAlimento;
    }

    public BigDecimal getBioCantAlimento() {
        return bioCantAlimento;
    }

    public void setBioCantAlimento(BigDecimal bioCantAlimento) {
        this.bioCantAlimento = bioCantAlimento;
    }

    public String getBioFormaAlmacen() {
        return bioFormaAlmacen;
    }

    public void setBioFormaAlmacen(String bioFormaAlmacen) {
        this.bioFormaAlmacen = bioFormaAlmacen;
    }

    public String getBioTipabono() {
        return bioTipabono;
    }

    public void setBioTipabono(String bioTipabono) {
        this.bioTipabono = bioTipabono;
    }

    public String getBioUsoAbono() {
        return bioUsoAbono;
    }

    public void setBioUsoAbono(String bioUsoAbono) {
        this.bioUsoAbono = bioUsoAbono;
    }

    public BigDecimal getBioCantSuministro() {
        return bioCantSuministro;
    }

    public void setBioCantSuministro(BigDecimal bioCantSuministro) {
        this.bioCantSuministro = bioCantSuministro;
    }

    public String getBioFormAlmacAbono() {
        return bioFormAlmacAbono;
    }

    public void setBioFormAlmacAbono(String bioFormAlmacAbono) {
        this.bioFormAlmacAbono = bioFormAlmacAbono;
    }

    
    
    
    /**
	 * @return the especiesAcuiferos
	 */
	public CatEspeciesAcuiferos getEspeciesAcuiferos() {
		return especiesAcuiferos;
	}

	/**
	 * @param especiesAcuiferos the especiesAcuiferos to set
	 */
	public void setEspeciesAcuiferos(CatEspeciesAcuiferos especiesAcuiferos) {
		this.especiesAcuiferos = especiesAcuiferos;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (infobioProyectoPK != null ? infobioProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InfobioProyecto)) {
            return false;
        }
        InfobioProyecto other = (InfobioProyecto) object;
        if ((this.infobioProyectoPK == null && other.infobioProyectoPK != null) || (this.infobioProyectoPK != null && !this.infobioProyectoPK.equals(other.infobioProyectoPK))) {
            return false;
        }
        return true;
    }


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InfobioProyecto [infobioProyectoPK=" + infobioProyectoPK + ", bioNombre=" + bioNombre + ", bioOrigen="
				+ bioOrigen + ", bioNumorgCultivar=" + bioNumorgCultivar + ", bioDescAtributosAme="
				+ bioDescAtributosAme + ", bioEspForrajera=" + bioEspForrajera + ", bioCrias=" + bioCrias
				+ ", bioSemillas=" + bioSemillas + ", bioPostlarvas=" + bioPostlarvas + ", bioJuveniles=" + bioJuveniles
				+ ", bioAdultReprod=" + bioAdultReprod + ", bioNumciclos=" + bioNumciclos + ", bioBiomasaInic="
				+ bioBiomasaInic + ", bioUnidadInic=" + bioUnidadInic + ", bioBiomasaEsperada=" + bioBiomasaEsperada
				+ ", bioUnidadEsperada=" + bioUnidadEsperada + ", bioTipoAlimento=" + bioTipoAlimento
				+ ", bioCantAlimento=" + bioCantAlimento + ", bioFormaAlmacen=" + bioFormaAlmacen
				+ ", bioUnidadAlmacen=" + bioUnidadAlmacen + ", bioTipabono=" + bioTipabono + ", bioUsoAbono="
				+ bioUsoAbono + ", bioCantSuministro=" + bioCantSuministro + ", bioUnidadSuministro="
				+ bioUnidadSuministro + ", bioFormAlmacAbono=" + bioFormAlmacAbono + ", especiesAcuiferos="
				+ especiesAcuiferos + "]";
	}

	/**
	 * @return the bioUnidadAlmacen
	 */
	public String getBioUnidadAlmacen() {
		return bioUnidadAlmacen;
	}

	/**
	 * @param bioUnidadAlmacen the bioUnidadAlmacen to set
	 */
	public void setBioUnidadAlmacen(String bioUnidadAlmacen) {
		this.bioUnidadAlmacen = bioUnidadAlmacen;
	}

	/**
	 * @return the bioUnidadSuministro
	 */
	public String getBioUnidadSuministro() {
		return bioUnidadSuministro;
	}

	/**
	 * @param bioUnidadSuministro the bioUnidadSuministro to set
	 */
	public void setBioUnidadSuministro(String bioUnidadSuministro) {
		this.bioUnidadSuministro = bioUnidadSuministro;
	}

    
}
