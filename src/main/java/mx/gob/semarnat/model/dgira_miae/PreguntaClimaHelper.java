/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class PreguntaClimaHelper implements Serializable{
    private static final long serialVersionUID = 1L;
    private Integer id;
    private PreguntaClima model;

    public PreguntaClimaHelper() {
    }

    public PreguntaClimaHelper(Integer id, PreguntaClima model) {
        this.id = id;
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PreguntaClima getModel() {
        return model;
    }

    public void setModel(PreguntaClima model) {
        this.model = model;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PreguntaClimaHelper)) {
            return false;
        }
        PreguntaClimaHelper ob = (PreguntaClimaHelper) obj;
        if (this.id != ob.id) {
            return false;
        }
        return true;
    }
    
    
    
}
