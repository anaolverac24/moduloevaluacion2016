/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "CAT_OBRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatObra.findAll", query = "SELECT c FROM CatObra c"),
    @NamedQuery(name = "CatObra.findByObraId", query = "SELECT c FROM CatObra c WHERE c.obraId = :obraId"),
    @NamedQuery(name = "CatObra.findByObraDescripcion", query = "SELECT c FROM CatObra c WHERE c.obraDescripcion = :obraDescripcion")})
public class CatObra implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "OBRA_ID")
    private Short obraId;
    @Column(name = "OBRA_DESCRIPCION")
    private String obraDescripcion;

    public CatObra() {
    }

    public CatObra(Short obraId) {
        this.obraId = obraId;
    }

    public Short getObraId() {
        return obraId;
    }

    public void setObraId(Short obraId) {
        this.obraId = obraId;
    }

    public String getObraDescripcion() {
        return obraDescripcion;
    }

    public void setObraDescripcion(String obraDescripcion) {
        this.obraDescripcion = obraDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (obraId != null ? obraId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatObra)) {
            return false;
        }
        CatObra other = (CatObra) object;
        if ((this.obraId == null && other.obraId != null) || (this.obraId != null && !this.obraId.equals(other.obraId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return obraId + "";
    }
    
}
