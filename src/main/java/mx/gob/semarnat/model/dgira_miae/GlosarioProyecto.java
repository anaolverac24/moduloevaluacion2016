/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.semarnat.model.bitacora.Proyecto;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "GLOSARIO_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GlosarioProyecto.findAll", query = "SELECT g FROM GlosarioProyecto g"),
    @NamedQuery(name = "GlosarioProyecto.findByFolioProyecto", query = "SELECT g FROM GlosarioProyecto g WHERE g.glosarioProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "GlosarioProyecto.findBySerialProyecto", query = "SELECT g FROM GlosarioProyecto g WHERE g.glosarioProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "GlosarioProyecto.findByGlosproyId", query = "SELECT g FROM GlosarioProyecto g WHERE g.glosarioProyectoPK.glosproyId = :glosproyId"),
    @NamedQuery(name = "GlosarioProyecto.findByTerminoNom", query = "SELECT g FROM GlosarioProyecto g WHERE g.terminoNom = :terminoNom"),
    @NamedQuery(name = "GlosarioProyecto.findByDescripcionTerm", query = "SELECT g FROM GlosarioProyecto g WHERE g.descripcionTerm = :descripcionTerm")})
public class GlosarioProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GlosarioProyectoPK glosarioProyectoPK;
    @Size(max = 300)
    @Column(name = "TERMINO_NOM")
    private String terminoNom;
    @Size(max = 3000)
    @Column(name = "DESCRIPCION_TERM")
    private String descripcionTerm;
//    @JoinColumns({
//        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
//        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
//    @ManyToOne(optional = false)
//    private Proyecto proyecto;

    public GlosarioProyecto() {
    }

    public GlosarioProyecto(GlosarioProyectoPK glosarioProyectoPK) {
        this.glosarioProyectoPK = glosarioProyectoPK;
    }

    public GlosarioProyecto(String folioProyecto, short serialProyecto, short glosproyId) {
        this.glosarioProyectoPK = new GlosarioProyectoPK(folioProyecto, serialProyecto, glosproyId);
    }

    public GlosarioProyectoPK getGlosarioProyectoPK() {
        return glosarioProyectoPK;
    }

    public void setGlosarioProyectoPK(GlosarioProyectoPK glosarioProyectoPK) {
        this.glosarioProyectoPK = glosarioProyectoPK;
    }

    public String getTerminoNom() {
        return terminoNom;
    }

    public void setTerminoNom(String terminoNom) {
        this.terminoNom = terminoNom;
    }

    public String getDescripcionTerm() {
        return descripcionTerm;
    }

    public void setDescripcionTerm(String descripcionTerm) {
        this.descripcionTerm = descripcionTerm;
    }

//    public Proyecto getProyecto() {
//        return proyecto;
//    }
//
//    public void setProyecto(Proyecto proyecto) {
//        this.proyecto = proyecto;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (glosarioProyectoPK != null ? glosarioProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GlosarioProyecto)) {
            return false;
        }
        GlosarioProyecto other = (GlosarioProyecto) object;
        if ((this.glosarioProyectoPK == null && other.glosarioProyectoPK != null) || (this.glosarioProyectoPK != null && !this.glosarioProyectoPK.equals(other.glosarioProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.GlosarioProyecto[ glosarioProyectoPK=" + glosarioProyectoPK + " ]";
    }
    
}
