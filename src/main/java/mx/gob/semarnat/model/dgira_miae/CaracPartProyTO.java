/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class CaracPartProyTO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Short id;
    private Short caracteristicaID;
    private String nombreObra;
    private String obraActividad;
    private String descripcion;
    private String descTrunked;
    private String superficie;
    private Short idObraActividad;
    private CatalogoTO naturaleza;
    private CatalogoTO temporalidad;
    private CatalogoTO unidad;
    private String estatusSigProm;
    
    public CaracPartProyTO() {
    }

	public String getObraActividad() {
		return obraActividad;
	}

	public void setObraActividad(String obraActividad) {
		this.obraActividad = obraActividad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getDescripcionCorta() {
		if (descripcion != null) {
			if (descripcion.length() < 199) {
				return descripcion;
			}
		}
		if (descripcion != null && descripcion.length() > 199) {
			return descripcion.substring(0, 199);
		}
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSuperficie() {
		return superficie;
	}

	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}

	public String getSuperficieUnidad() {
		superficie = superficie + " " + unidad.getDescripcion();
		return superficie;
	}
	
	/**
	 * @return the naturaleza
	 */
	public CatalogoTO getNaturaleza() {
		return naturaleza;
	}

	/**
	 * @param naturaleza the naturaleza to set
	 */
	public void setNaturaleza(CatalogoTO naturaleza) {
		this.naturaleza = naturaleza;
	}

	/**
	 * @return the temporalidad
	 */
	public CatalogoTO getTemporalidad() {
		return temporalidad;
	}

	/**
	 * @param temporalidad the temporalidad to set
	 */
	public void setTemporalidad(CatalogoTO temporalidad) {
		this.temporalidad = temporalidad;
	}

	/**
	 * @return the unidad
	 */
	public CatalogoTO getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(CatalogoTO unidad) {
		this.unidad = unidad;
	}

	public Short getId() {
		return id;
	}

	public void setId(Short id) {
		this.id = id;
	}

	public String getNombreObra() {
		return nombreObra;
	}

	public void setNombreObra(String nombreObra) {
		this.nombreObra = nombreObra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CaracPartProyTO other = (CaracPartProyTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * @return the descTrunked
	 */
	public String getDescTrunked() {
		return descTrunked;
	}

	/**
	 * @param descTrunked the descTrunked to set
	 */
	public void setDescTrunked(String descTrunked) {
		this.descTrunked = descTrunked;
	}

	/**
	 * @return the idObraActividad
	 */
	public Short getIdObraActividad() {
		return idObraActividad;
	}

	/**
	 * @param idObraActividad the idObraActividad to set
	 */
	public void setIdObraActividad(Short idObraActividad) {
		this.idObraActividad = idObraActividad;
	}

	/**
	 * @return the estatusSigProm
	 */
	public String getEstatusSigProm() {
		return estatusSigProm;
	}

	/**
	 * @param estatusSigProm the estatusSigProm to set
	 */
	public void setEstatusSigProm(String estatusSigProm) {
		this.estatusSigProm = estatusSigProm;
	}

	/**
	 * @return the caracteristicaID
	 */
	public Short getCaracteristicaID() {
		return caracteristicaID;
	}

	/**
	 * @param caracteristicaID the caracteristicaID to set
	 */
	public void setCaracteristicaID(Short caracteristicaID) {
		this.caracteristicaID = caracteristicaID;
	}


    
}
