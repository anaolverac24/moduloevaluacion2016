/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Edgar
 */
@Entity
@Table(name = "INVERSION_ETAPAS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InversionEtapas.findAll", query = "SELECT i FROM InversionEtapas i"),
    @NamedQuery(name = "InversionEtapas.findByFolioProyecto", query = "SELECT i FROM InversionEtapas i WHERE i.inversionEtapasPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "InversionEtapas.findBySerialProyecto", query = "SELECT i FROM InversionEtapas i WHERE i.inversionEtapasPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "InversionEtapas.findByInversionEtapaId", query = "SELECT i FROM InversionEtapas i WHERE i.inversionEtapasPK.inversionEtapaId = :inversionEtapaId"),
    @NamedQuery(name = "InversionEtapas.findByInversionPreparacion", query = "SELECT i FROM InversionEtapas i WHERE i.inversionPreparacion = :inversionPreparacion"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosTemporalesPrep", query = "SELECT i FROM InversionEtapas i WHERE i.empleosTemporalesPrep = :empleosTemporalesPrep"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosPermanentesPrep", query = "SELECT i FROM InversionEtapas i WHERE i.empleosPermanentesPrep = :empleosPermanentesPrep"),
    @NamedQuery(name = "InversionEtapas.findByInversionConstruccion", query = "SELECT i FROM InversionEtapas i WHERE i.inversionConstruccion = :inversionConstruccion"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosTemporalesCons", query = "SELECT i FROM InversionEtapas i WHERE i.empleosTemporalesCons = :empleosTemporalesCons"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosPermanentesCons", query = "SELECT i FROM InversionEtapas i WHERE i.empleosPermanentesCons = :empleosPermanentesCons"),
    @NamedQuery(name = "InversionEtapas.findByInversionOperacion", query = "SELECT i FROM InversionEtapas i WHERE i.inversionOperacion = :inversionOperacion"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosTemporalesOper", query = "SELECT i FROM InversionEtapas i WHERE i.empleosTemporalesOper = :empleosTemporalesOper"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosPermanentesOper", query = "SELECT i FROM InversionEtapas i WHERE i.empleosPermanentesOper = :empleosPermanentesOper"),
    @NamedQuery(name = "InversionEtapas.findByInversionAbandono", query = "SELECT i FROM InversionEtapas i WHERE i.inversionAbandono = :inversionAbandono"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosTemporalesAban", query = "SELECT i FROM InversionEtapas i WHERE i.empleosTemporalesAban = :empleosTemporalesAban"),
    @NamedQuery(name = "InversionEtapas.findByEmpleosPermanentesAban", query = "SELECT i FROM InversionEtapas i WHERE i.empleosPermanentesAban = :empleosPermanentesAban"),
    @NamedQuery(name = "InversionEtapas.findByMedidasMitigacion", query = "SELECT i FROM InversionEtapas i WHERE i.medidasMitigacion = :medidasMitigacion")})
public class InversionEtapas implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InversionEtapasPK inversionEtapasPK;
    @Column(name = "INVERSION_PREPARACION")
    private BigDecimal inversionPreparacion;
    @Column(name = "EMPLEOS_TEMPORALES_PREP")
    private BigDecimal empleosTemporalesPrep;
    @Column(name = "EMPLEOS_PERMANENTES_PREP")
    private BigDecimal empleosPermanentesPrep;
    @Column(name = "INVERSION_CONSTRUCCION")
    private BigDecimal inversionConstruccion;
    @Column(name = "EMPLEOS_TEMPORALES_CONS")
    private BigDecimal empleosTemporalesCons;
    @Column(name = "EMPLEOS_PERMANENTES_CONS")
    private BigDecimal empleosPermanentesCons;
    @Column(name = "INVERSION_OPERACION")
    private BigDecimal inversionOperacion;
    @Column(name = "EMPLEOS_TEMPORALES_OPER")
    private BigDecimal empleosTemporalesOper;
    @Column(name = "EMPLEOS_PERMANENTES_OPER")
    private BigDecimal empleosPermanentesOper;
    @Column(name = "INVERSION_ABANDONO")
    private BigDecimal inversionAbandono;
    @Column(name = "EMPLEOS_TEMPORALES_ABAN")
    private BigDecimal empleosTemporalesAban;
    @Column(name = "EMPLEOS_PERMANENTES_ABAN")
    private BigDecimal empleosPermanentesAban;
    @Column(name = "MEDIDAS_MITIGACION")
    private BigDecimal medidasMitigacion;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public InversionEtapas() {
    }

    public InversionEtapas(InversionEtapasPK inversionEtapasPK) {
        this.inversionEtapasPK = inversionEtapasPK;
    }

    public InversionEtapas(String folioProyecto, short serialProyecto, short inversionEtapaId) {
        this.inversionEtapasPK = new InversionEtapasPK(folioProyecto, serialProyecto, inversionEtapaId);
    }

    public InversionEtapasPK getInversionEtapasPK() {
        return inversionEtapasPK;
    }

    public void setInversionEtapasPK(InversionEtapasPK inversionEtapasPK) {
        this.inversionEtapasPK = inversionEtapasPK;
    }

    /**
	 * @return the inversionPreparacion
	 */
	public BigDecimal getInversionPreparacion() {
		return inversionPreparacion;
	}

	/**
	 * @param inversionPreparacion the inversionPreparacion to set
	 */
	public void setInversionPreparacion(BigDecimal inversionPreparacion) {
		this.inversionPreparacion = inversionPreparacion;
	}

	/**
	 * @return the empleosTemporalesPrep
	 */
	public BigDecimal getEmpleosTemporalesPrep() {
		return empleosTemporalesPrep;
	}

	/**
	 * @param empleosTemporalesPrep the empleosTemporalesPrep to set
	 */
	public void setEmpleosTemporalesPrep(BigDecimal empleosTemporalesPrep) {
		this.empleosTemporalesPrep = empleosTemporalesPrep;
	}

	/**
	 * @return the empleosPermanentesPrep
	 */
	public BigDecimal getEmpleosPermanentesPrep() {
		return empleosPermanentesPrep;
	}

	/**
	 * @param empleosPermanentesPrep the empleosPermanentesPrep to set
	 */
	public void setEmpleosPermanentesPrep(BigDecimal empleosPermanentesPrep) {
		this.empleosPermanentesPrep = empleosPermanentesPrep;
	}

	/**
	 * @return the inversionConstruccion
	 */
	public BigDecimal getInversionConstruccion() {
		return inversionConstruccion;
	}

	/**
	 * @param inversionConstruccion the inversionConstruccion to set
	 */
	public void setInversionConstruccion(BigDecimal inversionConstruccion) {
		this.inversionConstruccion = inversionConstruccion;
	}

	/**
	 * @return the empleosTemporalesCons
	 */
	public BigDecimal getEmpleosTemporalesCons() {
		return empleosTemporalesCons;
	}

	/**
	 * @param empleosTemporalesCons the empleosTemporalesCons to set
	 */
	public void setEmpleosTemporalesCons(BigDecimal empleosTemporalesCons) {
		this.empleosTemporalesCons = empleosTemporalesCons;
	}

	/**
	 * @return the empleosPermanentesCons
	 */
	public BigDecimal getEmpleosPermanentesCons() {
		return empleosPermanentesCons;
	}

	/**
	 * @param empleosPermanentesCons the empleosPermanentesCons to set
	 */
	public void setEmpleosPermanentesCons(BigDecimal empleosPermanentesCons) {
		this.empleosPermanentesCons = empleosPermanentesCons;
	}

	/**
	 * @return the inversionOperacion
	 */
	public BigDecimal getInversionOperacion() {
		return inversionOperacion;
	}

	/**
	 * @param inversionOperacion the inversionOperacion to set
	 */
	public void setInversionOperacion(BigDecimal inversionOperacion) {
		this.inversionOperacion = inversionOperacion;
	}

	/**
	 * @return the empleosTemporalesOper
	 */
	public BigDecimal getEmpleosTemporalesOper() {
		return empleosTemporalesOper;
	}

	/**
	 * @param empleosTemporalesOper the empleosTemporalesOper to set
	 */
	public void setEmpleosTemporalesOper(BigDecimal empleosTemporalesOper) {
		this.empleosTemporalesOper = empleosTemporalesOper;
	}

	/**
	 * @return the empleosPermanentesOper
	 */
	public BigDecimal getEmpleosPermanentesOper() {
		return empleosPermanentesOper;
	}

	/**
	 * @param empleosPermanentesOper the empleosPermanentesOper to set
	 */
	public void setEmpleosPermanentesOper(BigDecimal empleosPermanentesOper) {
		this.empleosPermanentesOper = empleosPermanentesOper;
	}

	/**
	 * @return the inversionAbandono
	 */
	public BigDecimal getInversionAbandono() {
		return inversionAbandono;
	}

	/**
	 * @param inversionAbandono the inversionAbandono to set
	 */
	public void setInversionAbandono(BigDecimal inversionAbandono) {
		this.inversionAbandono = inversionAbandono;
	}

	/**
	 * @return the empleosTemporalesAban
	 */
	public BigDecimal getEmpleosTemporalesAban() {
		return empleosTemporalesAban;
	}

	/**
	 * @param empleosTemporalesAban the empleosTemporalesAban to set
	 */
	public void setEmpleosTemporalesAban(BigDecimal empleosTemporalesAban) {
		this.empleosTemporalesAban = empleosTemporalesAban;
	}

	/**
	 * @return the empleosPermanentesAban
	 */
	public BigDecimal getEmpleosPermanentesAban() {
		return empleosPermanentesAban;
	}

	/**
	 * @param empleosPermanentesAban the empleosPermanentesAban to set
	 */
	public void setEmpleosPermanentesAban(BigDecimal empleosPermanentesAban) {
		this.empleosPermanentesAban = empleosPermanentesAban;
	}

	/**
	 * @return the medidasMitigacion
	 */
	public BigDecimal getMedidasMitigacion() {
		return medidasMitigacion;
	}

	/**
	 * @param medidasMitigacion the medidasMitigacion to set
	 */
	public void setMedidasMitigacion(BigDecimal medidasMitigacion) {
		this.medidasMitigacion = medidasMitigacion;
	}

	public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inversionEtapasPK != null ? inversionEtapasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InversionEtapas)) {
            return false;
        }
        InversionEtapas other = (InversionEtapas) object;
        if ((this.inversionEtapasPK == null && other.inversionEtapasPK != null) || (this.inversionEtapasPK != null && !this.inversionEtapasPK.equals(other.inversionEtapasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.InversionEtapas[ inversionEtapasPK=" + inversionEtapasPK + " ]";
    }
    
}
