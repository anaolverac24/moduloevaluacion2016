/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PREGUNTA_VEGETACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaVegetacion.findAll", query = "SELECT p FROM PreguntaVegetacion p"),
    @NamedQuery(name = "PreguntaVegetacion.findByFolioProyecto", query = "SELECT p FROM PreguntaVegetacion p WHERE p.preguntaVegetacionPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PreguntaVegetacion.findBySerialProyecto", query = "SELECT p FROM PreguntaVegetacion p WHERE p.preguntaVegetacionPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PreguntaVegetacion.findByPreguntaId", query = "SELECT p FROM PreguntaVegetacion p WHERE p.preguntaVegetacionPK.preguntaId = :preguntaId"),
    @NamedQuery(name = "PreguntaVegetacion.findByVegetacionId", query = "SELECT p FROM PreguntaVegetacion p WHERE p.preguntaVegetacionPK.vegetacionId = :vegetacionId")})
public class PreguntaVegetacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreguntaVegetacionPK preguntaVegetacionPK;

    public PreguntaVegetacion() {
    }

    public PreguntaVegetacion(PreguntaVegetacionPK preguntaVegetacionPK) {
        this.preguntaVegetacionPK = preguntaVegetacionPK;
    }

    public PreguntaVegetacion(String folioProyecto, short serialProyecto, short preguntaId, short vegetacionId) {
        this.preguntaVegetacionPK = new PreguntaVegetacionPK(folioProyecto, serialProyecto, preguntaId, vegetacionId);
    }

    public PreguntaVegetacionPK getPreguntaVegetacionPK() {
        return preguntaVegetacionPK;
    }

    public void setPreguntaVegetacionPK(PreguntaVegetacionPK preguntaVegetacionPK) {
        this.preguntaVegetacionPK = preguntaVegetacionPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaVegetacionPK != null ? preguntaVegetacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaVegetacion)) {
            return false;
        }
        PreguntaVegetacion other = (PreguntaVegetacion) object;
        if ((this.preguntaVegetacionPK == null && other.preguntaVegetacionPK != null) || (this.preguntaVegetacionPK != null && !this.preguntaVegetacionPK.equals(other.preguntaVegetacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "package mx.gob.semarnat.model.dgira_miae.PreguntaVegetacion[ preguntaVegetacionPK=" + preguntaVegetacionPK + " ]";
    }
    
}
