/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "PREDIOCOLIN_PROY", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrediocolinProy.findAll", query = "SELECT p FROM PrediocolinProy p"),
    @NamedQuery(name = "PrediocolinProy.findByFolioProyecto", query = "SELECT p FROM PrediocolinProy p WHERE p.prediocolinProyPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PrediocolinProy.findBySerialProyecto", query = "SELECT p FROM PrediocolinProy p WHERE p.prediocolinProyPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PrediocolinProy.findByPredioColinId", query = "SELECT p FROM PrediocolinProy p WHERE p.prediocolinProyPK.predioColinId = :predioColinId"),
    @NamedQuery(name = "PrediocolinProy.findByPredioNombre", query = "SELECT p FROM PrediocolinProy p WHERE p.predioNombre = :predioNombre"),
    @NamedQuery(name = "PrediocolinProy.findByPredioDescripcion", query = "SELECT p FROM PrediocolinProy p WHERE p.predioDescripcion = :predioDescripcion"),
    @NamedQuery(name="PrediocolinProy.findByFolioSerial", query = "SELECT e FROM PrediocolinProy e WHERE e.prediocolinProyPK.folioProyecto = :folio and e.prediocolinProyPK.serialProyecto = :serial"
)})
public class PrediocolinProy implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PrediocolinProyPK prediocolinProyPK;
    @Size(max = 100)
    @Column(name = "PREDIO_NOMBRE")
    private String predioNombre;
    @Size(max = 1000)
    @Column(name = "PREDIO_DESCRIPCION")
    private String predioDescripcion;
    @JoinColumn(name = "CLASIFICACION_ID", referencedColumnName = "CLASIFICACION_ID")
    @ManyToOne
    private CatClasificacion clasificacionId;
    @JoinColumn(name = "REFERENCIA_ID", referencedColumnName = "REFERENCIA_ID")
    @ManyToOne
    private CatReferencia referenciaId;
    @JoinColumn(name = "TEMPORALIDAD_ID", referencedColumnName = "TEMPORALIDAD_ID")
    @ManyToOne
    private CatTemporalidad temporalidadId;

    public PrediocolinProy() {
    }

    public PrediocolinProy(PrediocolinProyPK prediocolinProyPK) {
        this.prediocolinProyPK = prediocolinProyPK;
    }

    public PrediocolinProy(String folioProyecto, short serialProyecto, int predioColinId) {
        this.prediocolinProyPK = new PrediocolinProyPK(folioProyecto, serialProyecto, predioColinId);
    }

    public PrediocolinProyPK getPrediocolinProyPK() {
        return prediocolinProyPK;
    }

    public void setPrediocolinProyPK(PrediocolinProyPK prediocolinProyPK) {
        this.prediocolinProyPK = prediocolinProyPK;
    }

    public String getPredioNombre() {
        return predioNombre;
    }

    public void setPredioNombre(String predioNombre) {
        this.predioNombre = predioNombre;
    }

    public String getPredioDescripcion() {
        return predioDescripcion;
    }

    public void setPredioDescripcion(String predioDescripcion) {
        this.predioDescripcion = predioDescripcion;
    }

    public CatClasificacion getClasificacionId() {
        return clasificacionId;
    }

    public void setClasificacionId(CatClasificacion clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public CatReferencia getReferenciaId() {
        return referenciaId;
    }

    public void setReferenciaId(CatReferencia referenciaId) {
        this.referenciaId = referenciaId;
    }

    public CatTemporalidad getTemporalidadId() {
        return temporalidadId;
    }

    public void setTemporalidadId(CatTemporalidad temporalidadId) {
        this.temporalidadId = temporalidadId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prediocolinProyPK != null ? prediocolinProyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrediocolinProy)) {
            return false;
        }
        PrediocolinProy other = (PrediocolinProy) object;
        if ((this.prediocolinProyPK == null && other.prediocolinProyPK != null) || (this.prediocolinProyPK != null && !this.prediocolinProyPK.equals(other.prediocolinProyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.PrediocolinProy[ prediocolinProyPK=" + prediocolinProyPK + " ]";
    }
    
}
