/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_USOSUELO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatUsosuelo.findAll", query = "SELECT c FROM CatUsosuelo c"),
    @NamedQuery(name = "CatUsosuelo.findByUsosueloId", query = "SELECT c FROM CatUsosuelo c WHERE c.usosueloId = :usosueloId"),
    @NamedQuery(name = "CatUsosuelo.findByUsoSueloDesc", query = "SELECT c FROM CatUsosuelo c WHERE c.usoSueloDesc = :usoSueloDesc")})
public class CatUsosuelo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "USOSUELO_ID")
    private Short usosueloId;
    @Size(max = 30)
    @Column(name = "USO_SUELO_DESC")
    private String usoSueloDesc;

    public CatUsosuelo() {
    }

    public CatUsosuelo(Short usosueloId) {
        this.usosueloId = usosueloId;
    }

    public Short getUsosueloId() {
        return usosueloId;
    }

    public void setUsosueloId(Short usosueloId) {
        this.usosueloId = usosueloId;
    }

    public String getUsoSueloDesc() {
        return usoSueloDesc;
    }

    public void setUsoSueloDesc(String usoSueloDesc) {
        this.usoSueloDesc = usoSueloDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usosueloId != null ? usosueloId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatUsosuelo)) {
            return false;
        }
        CatUsosuelo other = (CatUsosuelo) object;
        if ((this.usosueloId == null && other.usosueloId != null) || (this.usosueloId != null && !this.usosueloId.equals(other.usosueloId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatUsosuelo[ usosueloId=" + usosueloId + " ]";
    }
    
}
