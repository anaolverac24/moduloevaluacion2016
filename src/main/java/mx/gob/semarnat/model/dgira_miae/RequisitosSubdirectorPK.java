/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Chely
 */
@Embeddable
public class RequisitosSubdirectorPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "REQSD_BITACORA")
    private String reqsdBitacora;
    @Basic(optional = false)
    @Column(name = "REQSD_NUMERO_REQUISITO")
    private BigInteger reqsdNumeroRequisito;
    @Basic(optional = false)
    @Column(name = "REQSD_CLAVE_TRAMITE")
    private String reqsdClaveTramite;

    public RequisitosSubdirectorPK() {
    }

    public RequisitosSubdirectorPK(String reqsdBitacora, BigInteger reqsdNumeroRequisito, String reqsdClaveTramite) {
        this.reqsdBitacora = reqsdBitacora;
        this.reqsdNumeroRequisito = reqsdNumeroRequisito;
        this.reqsdClaveTramite = reqsdClaveTramite;
    }

    public String getReqsdBitacora() {
        return reqsdBitacora;
    }

    public void setReqsdBitacora(String reqsdBitacora) {
        this.reqsdBitacora = reqsdBitacora;
    }

    public BigInteger getReqsdNumeroRequisito() {
        return reqsdNumeroRequisito;
    }

    public void setReqsdNumeroRequisito(BigInteger reqsdNumeroRequisito) {
        this.reqsdNumeroRequisito = reqsdNumeroRequisito;
    }

    public String getReqsdClaveTramite() {
        return reqsdClaveTramite;
    }

    public void setReqsdClaveTramite(String reqsdClaveTramite) {
        this.reqsdClaveTramite = reqsdClaveTramite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reqsdBitacora != null ? reqsdBitacora.hashCode() : 0);
        hash += (reqsdNumeroRequisito != null ? reqsdNumeroRequisito.hashCode() : 0);
        hash += (reqsdClaveTramite != null ? reqsdClaveTramite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequisitosSubdirectorPK)) {
            return false;
        }
        RequisitosSubdirectorPK other = (RequisitosSubdirectorPK) object;
        if ((this.reqsdBitacora == null && other.reqsdBitacora != null) || (this.reqsdBitacora != null && !this.reqsdBitacora.equals(other.reqsdBitacora))) {
            return false;
        }
        if ((this.reqsdNumeroRequisito == null && other.reqsdNumeroRequisito != null) || (this.reqsdNumeroRequisito != null && !this.reqsdNumeroRequisito.equals(other.reqsdNumeroRequisito))) {
            return false;
        }
        if ((this.reqsdClaveTramite == null && other.reqsdClaveTramite != null) || (this.reqsdClaveTramite != null && !this.reqsdClaveTramite.equals(other.reqsdClaveTramite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.RequisitosSubdirectorPK[ reqsdBitacora=" + reqsdBitacora + ", reqsdNumeroRequisito=" + reqsdNumeroRequisito + ", reqsdClaveTramite=" + reqsdClaveTramite + " ]";
    }
    
}
