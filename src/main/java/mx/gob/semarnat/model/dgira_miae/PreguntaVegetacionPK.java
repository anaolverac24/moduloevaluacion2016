/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Admin
 */
@Embeddable
public class PreguntaVegetacionPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "PREGUNTA_ID")
    private short preguntaId;
    @Basic(optional = false)
    @Column(name = "VEGETACION_ID")
    private short vegetacionId;

    public PreguntaVegetacionPK() {
    }

    public PreguntaVegetacionPK(String folioProyecto, short serialProyecto, short preguntaId, short vegetacionId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.preguntaId = preguntaId;
        this.vegetacionId = vegetacionId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getPreguntaId() {
        return preguntaId;
    }

    public void setPreguntaId(short preguntaId) {
        this.preguntaId = preguntaId;
    }

    public short getVegetacionId() {
        return vegetacionId;
    }

    public void setVegetacionId(short vegetacionId) {
        this.vegetacionId = vegetacionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) preguntaId;
        hash += (int) vegetacionId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaVegetacionPK)) {
            return false;
        }
        PreguntaVegetacionPK other = (PreguntaVegetacionPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.preguntaId != other.preguntaId) {
            return false;
        }
        if (this.vegetacionId != other.vegetacionId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "package mx.gob.semarnat.model.dgira_miae.PreguntaVegetacionPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", preguntaId=" + preguntaId + ", vegetacionId=" + vegetacionId + " ]";
    }
    
}
