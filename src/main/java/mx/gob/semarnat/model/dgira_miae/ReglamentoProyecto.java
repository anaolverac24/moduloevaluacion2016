/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "REGLAMENTO_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReglamentoProyecto.findAll", query = "SELECT r FROM ReglamentoProyecto r"),
    @NamedQuery(name = "ReglamentoProyecto.findByFolioProyecto", query = "SELECT r FROM ReglamentoProyecto r WHERE r.reglamentoProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ReglamentoProyecto.findBySerialProyecto", query = "SELECT r FROM ReglamentoProyecto r WHERE r.reglamentoProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ReglamentoProyecto.findByReglamentoProyId", query = "SELECT r FROM ReglamentoProyecto r WHERE r.reglamentoProyectoPK.reglamentoProyId = :reglamentoProyId and r.reglamentoProyectoPK.serialProyecto = :serialProyecto and r.reglamentoProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ReglamentoProyecto.findByReglamentoFechaPublicacion", query = "SELECT r FROM ReglamentoProyecto r WHERE r.reglamentoFechaPublicacion = :reglamentoFechaPublicacion"),
    @NamedQuery(name = "ReglamentoProyecto.findByReglamentoArticulo", query = "SELECT r FROM ReglamentoProyecto r WHERE r.reglamentoArticulo = :reglamentoArticulo"),
    @NamedQuery(name = "ReglamentoProyecto.findByReglamentoDescripcion", query = "SELECT r FROM ReglamentoProyecto r WHERE r.reglamentoDescripcion = :reglamentoDescripcion")})
public class ReglamentoProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReglamentoProyectoPK reglamentoProyectoPK;
    @Column(name = "REGLAMENTO_FECHA_PUBLICACION")
    private String reglamentoFechaPublicacion;
    @Column(name = "REGLAMENTO_ARTICULO")
    private String reglamentoArticulo;
    @Lob
    @Column(name = "REGLAMENTO_VICULACION")
    private String reglamentoViculacion;
    @Column(name = "REGLAMENTO_DESCRIPCION")
    private String reglamentoDescripcion;
    @JoinColumn(name = "REGLAMENTO_ID", referencedColumnName = "REGLAMENTO_ID")
    @ManyToOne
    private CatReglamento reglamentoId;    
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    
    //-Se hace la union para la tabla de archivos proyecto
    @JoinColumn(name = "ID_ARCHIVO_PROYECTO", referencedColumnName = "SEQ_ID")
    @ManyToOne
    private ArchivosProyecto idArchivoProyecto;
    //============================================================================

    
    
    @Transient
    private Date fechaPublicacion;

    public ReglamentoProyecto() {
    }

    public ReglamentoProyecto(ReglamentoProyectoPK reglamentoProyectoPK) {
        this.reglamentoProyectoPK = reglamentoProyectoPK;
    }

    public ReglamentoProyecto(String folioProyecto, short serialProyecto, short reglamentoProyId) {
        this.reglamentoProyectoPK = new ReglamentoProyectoPK(folioProyecto, serialProyecto, reglamentoProyId);
    }

    public ReglamentoProyectoPK getReglamentoProyectoPK() {
        return reglamentoProyectoPK;
    }

    public void setReglamentoProyectoPK(ReglamentoProyectoPK reglamentoProyectoPK) {
        this.reglamentoProyectoPK = reglamentoProyectoPK;
    }

    public String getReglamentoFechaPublicacion() {
        return reglamentoFechaPublicacion;
    }

    public void setReglamentoFechaPublicacion(String reglamentoFechaPublicacion) {
        this.reglamentoFechaPublicacion = reglamentoFechaPublicacion;
    }

    public String getReglamentoArticulo() {
        return reglamentoArticulo;
    }

    public void setReglamentoArticulo(String reglamentoArticulo) {
        this.reglamentoArticulo = reglamentoArticulo;
    }

    public String getReglamentoViculacion() {
        return reglamentoViculacion;
    }
    
    public String getReglamentoViculacionCorta(){
        if(reglamentoViculacion.length()<199){
            return reglamentoViculacion;
        }
        return reglamentoViculacion.substring(0, 199);
    }

    public void setReglamentoViculacion(String reglamentoViculacion) {
        this.reglamentoViculacion = reglamentoViculacion;
    }

    public String getReglamentoDescripcion() {
        return reglamentoDescripcion;
    }

    public void setReglamentoDescripcion(String reglamentoDescripcion) {
        this.reglamentoDescripcion = reglamentoDescripcion;
    }

    public CatReglamento getReglamentoId() {
        return reglamentoId;
    }

    public void setReglamentoId(CatReglamento reglamentoId) {
        this.reglamentoId = reglamentoId;
    }

    public ArchivosProyecto getIdArchivoProyecto() {
		return idArchivoProyecto;
	}

	public void setIdArchivoProyecto(ArchivosProyecto idArchivoProyecto) {
		this.idArchivoProyecto = idArchivoProyecto;
	}

	public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reglamentoProyectoPK != null ? reglamentoProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglamentoProyecto)) {
            return false;
        }
        ReglamentoProyecto other = (ReglamentoProyecto) object;
        if ((this.reglamentoProyectoPK == null && other.reglamentoProyectoPK != null) || (this.reglamentoProyectoPK != null && !this.reglamentoProyectoPK.equals(other.reglamentoProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ReglamentoProyecto[ reglamentoProyectoPK=" + reglamentoProyectoPK + " ]";
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }
    
}
