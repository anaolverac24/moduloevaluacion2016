/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "INSUMOS_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InsumosProyecto.findAll", query = "SELECT i FROM InsumosProyecto i"),
    @NamedQuery(name = "InsumosProyecto.findByFolioProyecto", query = "SELECT i FROM InsumosProyecto i WHERE i.insumosProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "InsumosProyecto.findBySerialProyecto", query = "SELECT i FROM InsumosProyecto i WHERE i.insumosProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "InsumosProyecto.findByEtapaId", query = "SELECT i FROM InsumosProyecto i WHERE i.insumosProyectoPK.etapaId = :etapaId"),
    @NamedQuery(name = "InsumosProyecto.findByInsumosId", query = "SELECT i FROM InsumosProyecto i WHERE i.insumosProyectoPK.insumosId = :insumosId"),
    @NamedQuery(name = "InsumosProyecto.findByInsumoNomCmoun", query = "SELECT i FROM InsumosProyecto i WHERE i.insumoNomCmoun = :insumoNomCmoun"),
    @NamedQuery(name = "InsumosProyecto.findByInsumoNomTecnico", query = "SELECT i FROM InsumosProyecto i WHERE i.insumoNomTecnico = :insumoNomTecnico"),
    @NamedQuery(name = "InsumosProyecto.findByInsumoEstadoFisico", query = "SELECT i FROM InsumosProyecto i WHERE i.insumoEstadoFisico = :insumoEstadoFisico"),
    @NamedQuery(name = "InsumosProyecto.findByInsumoCantAlm", query = "SELECT i FROM InsumosProyecto i WHERE i.insumoCantAlm = :insumoCantAlm"),
    @NamedQuery(name = "InsumosProyecto.findByCveClve", query = "SELECT i FROM InsumosProyecto i WHERE i.cveClve = :cveClve"),
    @NamedQuery(name = "InsumosProyecto.findByInsumoCantMensual", query = "SELECT i FROM InsumosProyecto i WHERE i.insumoCantMensual = :insumoCantMensual"),
    @NamedQuery(name = "InsumosProyecto.findByFolioSerial", query = "SELECT e FROM InsumosProyecto e WHERE e.insumosProyectoPK.folioProyecto = :folio and e.insumosProyectoPK.serialProyecto = :serial"),
    @NamedQuery(name = "InsumosProyecto.findByClveCantMensual", query = "SELECT i FROM InsumosProyecto i WHERE i.clveCantMensual = :clveCantMensual")})
public class InsumosProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InsumosProyectoPK insumosProyectoPK;
    @Size(max = 100)
    @Column(name = "INSUMO_NOM_CMOUN")
    private String insumoNomCmoun;
    @Size(max = 100)
    @Column(name = "INSUMO_NOM_TECNICO")
    private String insumoNomTecnico;
    @Size(max = 100)
    @Column(name = "INSUMO_ESTADO_FISICO")
    private String insumoEstadoFisico;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "INSUMO_CANT_ALM")
    private BigDecimal insumoCantAlm;
    @Column(name = "CVE_CLVE")
    private Short cveClve;
    @Column(name = "INSUMO_CANT_MENSUAL")
    private BigDecimal insumoCantMensual;
    @Column(name = "CLVE_CANT_MENSUAL")
    private Short clveCantMensual;
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatEtapa catEtapa;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public InsumosProyecto() {
    }

    public InsumosProyecto(InsumosProyectoPK insumosProyectoPK) {
        this.insumosProyectoPK = insumosProyectoPK;
    }

    public InsumosProyecto(String folioProyecto, short serialProyecto, short etapaId, short insumosId) {
        this.insumosProyectoPK = new InsumosProyectoPK(folioProyecto, serialProyecto, etapaId, insumosId);
    }

    public InsumosProyectoPK getInsumosProyectoPK() {
        return insumosProyectoPK;
    }

    public void setInsumosProyectoPK(InsumosProyectoPK insumosProyectoPK) {
        this.insumosProyectoPK = insumosProyectoPK;
    }

    public String getInsumoNomCmoun() {
        return insumoNomCmoun;
    }

    public void setInsumoNomCmoun(String insumoNomCmoun) {
        this.insumoNomCmoun = insumoNomCmoun;
    }

    public String getInsumoNomTecnico() {
        return insumoNomTecnico;
    }

    public void setInsumoNomTecnico(String insumoNomTecnico) {
        this.insumoNomTecnico = insumoNomTecnico;
    }

    public String getInsumoEstadoFisico() {
        return insumoEstadoFisico;
    }

    public void setInsumoEstadoFisico(String insumoEstadoFisico) {
        this.insumoEstadoFisico = insumoEstadoFisico;
    }

    public BigDecimal getInsumoCantAlm() {
        return insumoCantAlm;
    }

    public void setInsumoCantAlm(BigDecimal insumoCantAlm) {
        this.insumoCantAlm = insumoCantAlm;
    }

    public Short getCveClve() {
        return cveClve;
    }

    public void setCveClve(Short cveClve) {
        this.cveClve = cveClve;
    }

    public BigDecimal getInsumoCantMensual() {
        return insumoCantMensual;
    }

    public void setInsumoCantMensual(BigDecimal insumoCantMensual) {
        this.insumoCantMensual = insumoCantMensual;
    }

    public Short getClveCantMensual() {
        return clveCantMensual;
    }

    public void setClveCantMensual(Short clveCantMensual) {
        this.clveCantMensual = clveCantMensual;
    }

    public CatEtapa getCatEtapa() {
        return catEtapa;
    }

    public void setCatEtapa(CatEtapa catEtapa) {
        this.catEtapa = catEtapa;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (insumosProyectoPK != null ? insumosProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InsumosProyecto)) {
            return false;
        }
        InsumosProyecto other = (InsumosProyecto) object;
        if ((this.insumosProyectoPK == null && other.insumosProyectoPK != null) || (this.insumosProyectoPK != null && !this.insumosProyectoPK.equals(other.insumosProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.InsumosProyecto[ insumosProyectoPK=" + insumosProyectoPK + " ]";
    }

}
