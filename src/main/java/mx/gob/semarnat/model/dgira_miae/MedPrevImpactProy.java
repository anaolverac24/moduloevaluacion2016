/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "MED_PREV_IMPACT_PROY", schema ="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedPrevImpactProy.findAll", query = "SELECT m FROM MedPrevImpactProy m"),
    @NamedQuery(name = "MedPrevImpactProy.findByFolioProyecto", query = "SELECT m FROM MedPrevImpactProy m WHERE m.medPrevImpactProyPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "MedPrevImpactProy.findBySerialProyecto", query = "SELECT m FROM MedPrevImpactProy m WHERE m.medPrevImpactProyPK.folioProyecto = :folioProyecto AND m.medPrevImpactProyPK.serialProyecto = :serialProyecto ORDER BY m.etapaId.etapaId"),
    @NamedQuery(name = "MedPrevImpactProy.findByMedPrevImpactId", query = "SELECT m FROM MedPrevImpactProy m WHERE m.medPrevImpactProyPK.medPrevImpactId = :medPrevImpactId"),
    @NamedQuery(name = "MedPrevImpactProy.findByPrevImpactonom", query = "SELECT m FROM MedPrevImpactProy m WHERE m.prevImpactonom = :prevImpactonom"),
    @NamedQuery(name = "MedPrevImpactProy.findByPrevMedProp", query = "SELECT m FROM MedPrevImpactProy m WHERE m.prevMedProp = :prevMedProp"),
    @NamedQuery(name = "MedPrevImpactProy.findByPrecRecNece", query = "SELECT m FROM MedPrevImpactProy m WHERE m.precRecNece = :precRecNece"),
    @NamedQuery(name = "MedPrevImpactProy.findByPrevEficCamb", query = "SELECT m FROM MedPrevImpactProy m WHERE m.prevEficCamb = :prevEficCamb")})
public class MedPrevImpactProy implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MedPrevImpactProyPK medPrevImpactProyPK;
    @Column(name = "PREV_IMPACTONOM")
    private String prevImpactonom;
    @Column(name = "PREV_MED_PROP")
    private String prevMedProp;
    @Column(name = "PREC_REC_NECE")
    private String precRecNece;
    @Column(name = "PREV_EFIC_CAMB")
    private String prevEficCamb;
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID")
    @ManyToOne(optional = false)
    private CatEtapa etapaId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public MedPrevImpactProy() {
    }

    public MedPrevImpactProy(MedPrevImpactProyPK medPrevImpactProyPK) {
        this.medPrevImpactProyPK = medPrevImpactProyPK;
    }

    public MedPrevImpactProy(String folioProyecto, short serialProyecto, short medPrevImpactId) {
        this.medPrevImpactProyPK = new MedPrevImpactProyPK(folioProyecto, serialProyecto, medPrevImpactId);
    }

    public MedPrevImpactProyPK getMedPrevImpactProyPK() {
        return medPrevImpactProyPK;
    }

    public void setMedPrevImpactProyPK(MedPrevImpactProyPK medPrevImpactProyPK) {
        this.medPrevImpactProyPK = medPrevImpactProyPK;
    }

    public String getPrevImpactonom() {
        return prevImpactonom;
    }

    public void setPrevImpactonom(String prevImpactonom) {
        this.prevImpactonom = prevImpactonom;
    }
    
    public String getPrevMedProp() {
        return prevMedProp;
    }

    public void setPrevMedProp(String prevMedProp) {
        this.prevMedProp = prevMedProp;
    }

	  //Metodo para acotar la informacion 
	    public String getImpactCaraCorta(){
	    	
			 if(prevMedProp!=null)
			  {	
		        if(prevMedProp.length()<199)
		        {
		            return prevMedProp;
		        }
		        return prevMedProp.substring(0, 199);
		      }
			 else
				 return " ";
	    }
    
    public String getPrecRecNece() {
        return precRecNece;
    }

    public void setPrecRecNece(String precRecNece) {
        this.precRecNece = precRecNece;
    }

    public String getPrevEficCamb() {
        return prevEficCamb;
    }

    public void setPrevEficCamb(String prevEficCamb) {
        this.prevEficCamb = prevEficCamb;
    }

    public CatEtapa getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(CatEtapa etapaId) {
        this.etapaId = etapaId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medPrevImpactProyPK != null ? medPrevImpactProyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedPrevImpactProy)) {
            return false;
        }
        MedPrevImpactProy other = (MedPrevImpactProy) object;
        if ((this.medPrevImpactProyPK == null && other.medPrevImpactProyPK != null) || (this.medPrevImpactProyPK != null && !this.medPrevImpactProyPK.equals(other.medPrevImpactProyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.MedPrevImpactProy[ medPrevImpactProyPK=" + medPrevImpactProyPK + " ]";
    }
    
}
