package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@SuppressWarnings("serial")
@Embeddable
public class ExencionRequisitosEvaluadorPk implements Serializable {
	@Basic(optional = false)
	@Column(name = "ID_EXENCION")
	private Long idExencion;
	@Basic(optional = false)
	@Column(name = "NSECCION")
	private short nSeccion;
	@Basic(optional = false)
	@Column(name = "NREQUISITO")
	private short nRequisito;

	public ExencionRequisitosEvaluadorPk() {
		// TODO Auto-generated constructor stub
	}
	
	public ExencionRequisitosEvaluadorPk(Long idExencion, short nSeccion, short nRequisito) {
		this.idExencion = idExencion;
		this.nSeccion = nSeccion;
		this.nRequisito = nRequisito;
	}
	
	public short getnSeccion() {
		return nSeccion;
	}

	public void setnSeccion(short nSeccion) {
		this.nSeccion = nSeccion;
	}

	public short getnRequisito() {
		return nRequisito;
	}

	public void setnRequisito(short nRequisito) {
		this.nRequisito = nRequisito;
	}

	public Long getIdExencion() {
		return idExencion;
	}

	public void setIdExencion(Long idExencion) {
		this.idExencion = idExencion;
	}
}
