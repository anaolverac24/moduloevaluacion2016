/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ANP_FED_ZONANUCLEO",schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnpFedZonanucleo.findAll", query = "SELECT a FROM AnpFedZonanucleo a")})
public class AnpFedZonanucleo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnpFedZonanucleoPK anpFedZonanucleoPK;
    @Column(name = "NOM_ZNUCLE")
    private String nomZnucle;
    @Column(name = "NOMBRE_ANP")
    private String nombreAnp;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "anpFedZonanucleo")
//    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public AnpFedZonanucleo() {
    }

    public AnpFedZonanucleo(AnpFedZonanucleoPK anpFedZonanucleoPK) {
        this.anpFedZonanucleoPK = anpFedZonanucleoPK;
    }

    public AnpFedZonanucleo(String numFolio, String cveProy, String cveArea, short version) {
        this.anpFedZonanucleoPK = new AnpFedZonanucleoPK(numFolio, cveProy, cveArea, version);
    }

    public AnpFedZonanucleoPK getAnpFedZonanucleoPK() {
        return anpFedZonanucleoPK;
    }

    public void setAnpFedZonanucleoPK(AnpFedZonanucleoPK anpFedZonanucleoPK) {
        this.anpFedZonanucleoPK = anpFedZonanucleoPK;
    }

    public String getNomZnucle() {
        return nomZnucle;
    }

    public void setNomZnucle(String nomZnucle) {
        this.nomZnucle = nomZnucle;
    }

    public String getNombreAnp() {
        return nombreAnp;
    }

    public void setNombreAnp(String nombreAnp) {
        this.nombreAnp = nombreAnp;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

//    @XmlTransient
//    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
//        return sigeiaAnalisisList;
//    }
//
//    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
//        this.sigeiaAnalisisList = sigeiaAnalisisList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anpFedZonanucleoPK != null ? anpFedZonanucleoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnpFedZonanucleo)) {
            return false;
        }
        AnpFedZonanucleo other = (AnpFedZonanucleo) object;
        if ((this.anpFedZonanucleoPK == null && other.anpFedZonanucleoPK != null) || (this.anpFedZonanucleoPK != null && !this.anpFedZonanucleoPK.equals(other.anpFedZonanucleoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AnpFedZonanucleo[ anpFedZonanucleoPK=" + anpFedZonanucleoPK + " ]";
    }
    
}
