/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class ActividadEtapaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ETAPA_ID")
    private short etapaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVIDAD_ETAPA_ID")
    private short actividadEtapaId;

    public ActividadEtapaPK() {
    }

    public ActividadEtapaPK(String folioProyecto, short serialProyecto, short etapaId, short actividadEtapaId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.etapaId = etapaId;
        this.actividadEtapaId = actividadEtapaId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(short etapaId) {
        this.etapaId = etapaId;
    }

    public short getActividadEtapaId() {
        return actividadEtapaId;
    }

    public void setActividadEtapaId(short actividadEtapaId) {
        this.actividadEtapaId = actividadEtapaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) etapaId;
        hash += (int) actividadEtapaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActividadEtapaPK)) {
            return false;
        }
        ActividadEtapaPK other = (ActividadEtapaPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.etapaId != other.etapaId) {
            return false;
        }
        if (this.actividadEtapaId != other.actividadEtapaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.ActividadEtapaPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", etapaId=" + etapaId + ", actividadEtapaId=" + actividadEtapaId + " ]";
    }
    
}
