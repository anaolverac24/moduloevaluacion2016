/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_CATEGORIAS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaCategorias.findAll", query = "SELECT r FROM ReiaCategorias r"),
    @NamedQuery(name = "ReiaCategorias.findByIdCategoria", query = "SELECT r FROM ReiaCategorias r WHERE r.idCategoria = :idCategoria"),
    @NamedQuery(name = "ReiaCategorias.findByInciso", query = "SELECT r FROM ReiaCategorias r WHERE r.inciso = :inciso"),
    @NamedQuery(name = "ReiaCategorias.findByCategoria", query = "SELECT r FROM ReiaCategorias r WHERE r.categoria = :categoria"),
    @NamedQuery(name = "ReiaCategorias.findByEstatus", query = "SELECT r FROM ReiaCategorias r WHERE r.estatus = :estatus"),
    @NamedQuery(name = "ReiaCategorias.findByEstatusFracionNotNull", query = "SELECT r FROM ReiaCategorias r WHERE r.estatus = :estatus"),    
    @NamedQuery(name = "ReiaCategorias.findByCategoriaSh", query = "SELECT r FROM ReiaCategorias r WHERE r.categoriaSh = :categoriaSh")})
public class ReiaCategorias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_CATEGORIA")
    private Long idCategoria;
    @Column(name = "INCISO")
    private String inciso;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "ESTATUS")
    private Long estatus;
    @Column(name = "CATEGORIA_SH")
    private String categoriaSh;
    @OneToMany(mappedBy = "idCategoria")
    private Collection<ReiaObras> reiaObrasCollection;

    public ReiaCategorias() {
    }

    public ReiaCategorias(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Long getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getInciso() {
        return inciso;
    }

    public void setInciso(String inciso) {
        this.inciso = inciso;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Long getEstatus() {
        return estatus;
    }

    public void setEstatus(Long estatus) {
        this.estatus = estatus;
    }

    public String getCategoriaSh() {
        return categoriaSh;
    }

    public void setCategoriaSh(String categoriaSh) {
        this.categoriaSh = categoriaSh;
    }

    @XmlTransient
    public Collection<ReiaObras> getReiaObrasCollection() {
        return reiaObrasCollection;
    }

    public void setReiaObrasCollection(Collection<ReiaObras> reiaObrasCollection) {
        this.reiaObrasCollection = reiaObrasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoria != null ? idCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReiaCategorias)) {
            return false;
        }
        ReiaCategorias other = (ReiaCategorias) object;
        if ((this.idCategoria == null && other.idCategoria != null) || (this.idCategoria != null && !this.idCategoria.equals(other.idCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaCategorias[ idCategoria=" + idCategoria + " ]";
    }
    
}
