/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "PARQUE_INDUSTRIAL_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParqueIndustrialProyecto.findAll", query = "SELECT p FROM ParqueIndustrialProyecto p"),
    @NamedQuery(name = "ParqueIndustrialProyecto.findByFolioProyecto", query = "SELECT p FROM ParqueIndustrialProyecto p WHERE p.parqueIndustrialProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ParqueIndustrialProyecto.findBySerialProyecto", query = "SELECT p FROM ParqueIndustrialProyecto p WHERE p.parqueIndustrialProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ParqueIndustrialProyecto.findByParqueId", query = "SELECT p FROM ParqueIndustrialProyecto p WHERE p.parqueIndustrialProyectoPK.parqueId = :parqueId")})
public class ParqueIndustrialProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ParqueIndustrialProyectoPK parqueIndustrialProyectoPK;
    @Lob
    @Column(name = "PARQUE_IND_JUSTIFICACION")
    private String parqueIndJustificacion;
    @JoinColumn(name = "PARQUE_ID", referencedColumnName = "PARQUE_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatParqueIndustrial catParqueIndustrial;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public ParqueIndustrialProyecto() {
    }

    public ParqueIndustrialProyecto(ParqueIndustrialProyectoPK parqueIndustrialProyectoPK) {
        this.parqueIndustrialProyectoPK = parqueIndustrialProyectoPK;
    }

    public ParqueIndustrialProyecto(String folioProyecto, short serialProyecto, short parqueId) {
        this.parqueIndustrialProyectoPK = new ParqueIndustrialProyectoPK(folioProyecto, serialProyecto, parqueId);
    }

    public ParqueIndustrialProyectoPK getParqueIndustrialProyectoPK() {
        return parqueIndustrialProyectoPK;
    }

    public void setParqueIndustrialProyectoPK(ParqueIndustrialProyectoPK parqueIndustrialProyectoPK) {
        this.parqueIndustrialProyectoPK = parqueIndustrialProyectoPK;
    }

    public String getParqueIndJustificacion() {
        return parqueIndJustificacion;
    }

    public void setParqueIndJustificacion(String parqueIndJustificacion) {
        this.parqueIndJustificacion = parqueIndJustificacion;
    }

    public CatParqueIndustrial getCatParqueIndustrial() {
        return catParqueIndustrial;
    }

    public void setCatParqueIndustrial(CatParqueIndustrial catParqueIndustrial) {
        this.catParqueIndustrial = catParqueIndustrial;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parqueIndustrialProyectoPK != null ? parqueIndustrialProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParqueIndustrialProyecto)) {
            return false;
        }
        ParqueIndustrialProyecto other = (ParqueIndustrialProyecto) object;
        if ((this.parqueIndustrialProyectoPK == null && other.parqueIndustrialProyectoPK != null) || (this.parqueIndustrialProyectoPK != null && !this.parqueIndustrialProyectoPK.equals(other.parqueIndustrialProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelEmia.ParqueIndustrialProyecto[ parqueIndustrialProyectoPK=" + parqueIndustrialProyectoPK + " ]";
    }
    
}
