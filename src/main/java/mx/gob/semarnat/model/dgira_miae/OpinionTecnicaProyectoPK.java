/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class OpinionTecnicaProyectoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPINIONT_ID")
    private short opiniontId;

    public OpinionTecnicaProyectoPK() {
    }

    public OpinionTecnicaProyectoPK(String bitacoraProyecto, short opiniontId) {
        this.bitacoraProyecto = bitacoraProyecto;
        this.opiniontId = opiniontId;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public short getOpiniontId() {
        return opiniontId;
    }

    public void setOpiniontId(short opiniontId) {
        this.opiniontId = opiniontId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacoraProyecto != null ? bitacoraProyecto.hashCode() : 0);
        hash += (int) opiniontId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OpinionTecnicaProyectoPK)) {
            return false;
        }
        OpinionTecnicaProyectoPK other = (OpinionTecnicaProyectoPK) object;
        if ((this.bitacoraProyecto == null && other.bitacoraProyecto != null) || (this.bitacoraProyecto != null && !this.bitacoraProyecto.equals(other.bitacoraProyecto))) {
            return false;
        }
        if (this.opiniontId != other.opiniontId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.OpinionTecnicaProyectoPK[ bitacoraProyecto=" + bitacoraProyecto + ", opiniontId=" + opiniontId + " ]";
    }
    
}
