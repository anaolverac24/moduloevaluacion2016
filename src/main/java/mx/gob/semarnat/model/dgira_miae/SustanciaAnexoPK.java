/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class SustanciaAnexoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUSTANCIA_PROY_ID")
    private short sustanciaProyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUSTANCIA_ANEXO_ID")
    private short sustanciaAnexoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;

    public SustanciaAnexoPK() {
    }

    public SustanciaAnexoPK(short sustanciaProyId, short sustanciaAnexoId, String folioProyecto, short serialProyecto) {
        this.sustanciaProyId = sustanciaProyId;
        this.sustanciaAnexoId = sustanciaAnexoId;
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
    }

    public short getSustanciaProyId() {
        return sustanciaProyId;
    }

    public void setSustanciaProyId(short sustanciaProyId) {
        this.sustanciaProyId = sustanciaProyId;
    }

    public short getSustanciaAnexoId() {
        return sustanciaAnexoId;
    }

    public void setSustanciaAnexoId(short sustanciaAnexoId) {
        this.sustanciaAnexoId = sustanciaAnexoId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) sustanciaProyId;
        hash += (int) sustanciaAnexoId;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SustanciaAnexoPK)) {
            return false;
        }
        SustanciaAnexoPK other = (SustanciaAnexoPK) object;
        if (this.sustanciaProyId != other.sustanciaProyId) {
            return false;
        }
        if (this.sustanciaAnexoId != other.sustanciaAnexoId) {
            return false;
        }
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.SustanciaAnexoPK[ sustanciaProyId=" + sustanciaProyId + ", sustanciaAnexoId=" + sustanciaAnexoId + ", folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + " ]";
    }
    
}
