package mx.gob.semarnat.model.dgira_miae;

public class CatalogoTO {
    
	private Short id;    
    
	private String descripcion;

	private Long idLong;    
    
	public CatalogoTO() {
		// TODO Auto-generated constructor stub
	}
	
	public CatalogoTO(Short id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}
	public CatalogoTO(Long idLong, String descripcion) {
		super();
		this.idLong = idLong;
		this.descripcion = descripcion;
	}
	/**
	 * @return the id
	 */
	public Short getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the idLong
	 */
	public Long getIdLong() {
		return idLong;
	}
	/**
	 * @param idLong the idLong to set
	 */
	public void setIdLong(Long idLong) {
		this.idLong = idLong;
	}
    
    
    
}
