/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class ArticuloReglaAnpPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "ANP_ID")
    private short anpId;
    @Basic(optional = false)
    @Column(name = "ANP_ART_REG_ID")
    private short anpArtRegId;

    public ArticuloReglaAnpPK() {
    }

    public ArticuloReglaAnpPK(String folioProyecto, short serialProyecto, short anpId, short anpArtRegId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.anpId = anpId;
        this.anpArtRegId = anpArtRegId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getAnpId() {
        return anpId;
    }

    public void setAnpId(short anpId) {
        this.anpId = anpId;
    }

    public short getAnpArtRegId() {
        return anpArtRegId;
    }

    public void setAnpArtRegId(short anpArtRegId) {
        this.anpArtRegId = anpArtRegId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) anpId;
        hash += (int) anpArtRegId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArticuloReglaAnpPK)) {
            return false;
        }
        ArticuloReglaAnpPK other = (ArticuloReglaAnpPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.anpId != other.anpId) {
            return false;
        }
        if (this.anpArtRegId != other.anpArtRegId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ArticuloReglaAnpPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", anpId=" + anpId + ", anpArtRegId=" + anpArtRegId + " ]";
    }
    
}
