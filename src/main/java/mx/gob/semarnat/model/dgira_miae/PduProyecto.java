/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "PDU_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PduProyecto.findAll", query = "SELECT p FROM PduProyecto p"),
    @NamedQuery(name = "PduProyecto.findByFolioProyecto", query = "SELECT p FROM PduProyecto p WHERE p.pduProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PduProyecto.findBySerialProyecto", query = "SELECT p FROM PduProyecto p WHERE p.pduProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PduProyecto.findByPduId", query = "SELECT p FROM PduProyecto p WHERE p.pduProyectoPK.pduId = :pduId")})
public class PduProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PduProyectoPK pduProyectoPK;
    @Lob
    @Column(name = "PDU_JUSTIFICACION")
    private String pduJustificacion;
    @JoinColumn(name = "PDU_ID", referencedColumnName = "PDU_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatPdu catPdu;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public PduProyecto() {
    }

    public PduProyecto(PduProyectoPK pduProyectoPK) {
        this.pduProyectoPK = pduProyectoPK;
    }

    public PduProyecto(String folioProyecto, short serialProyecto, short pduId) {
        this.pduProyectoPK = new PduProyectoPK(folioProyecto, serialProyecto, pduId);
    }

    public PduProyectoPK getPduProyectoPK() {
        return pduProyectoPK;
    }

    public void setPduProyectoPK(PduProyectoPK pduProyectoPK) {
        this.pduProyectoPK = pduProyectoPK;
    }

    public String getPduJustificacion() {
        return pduJustificacion;
    }

    public void setPduJustificacion(String pduJustificacion) {
        this.pduJustificacion = pduJustificacion;
    }

    public CatPdu getCatPdu() {
        return catPdu;
    }

    public void setCatPdu(CatPdu catPdu) {
        this.catPdu = catPdu;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pduProyectoPK != null ? pduProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PduProyecto)) {
            return false;
        }
        PduProyecto other = (PduProyecto) object;
        if ((this.pduProyectoPK == null && other.pduProyectoPK != null) || (this.pduProyectoPK != null && !this.pduProyectoPK.equals(other.pduProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelEmia.PduProyecto[ pduProyectoPK=" + pduProyectoPK + " ]";
    }
    
}
