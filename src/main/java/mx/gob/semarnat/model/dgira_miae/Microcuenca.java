/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "MICROCUENCA", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Microcuenca.findAll", query = "SELECT m FROM Microcuenca m"),
    @NamedQuery(name = "Microcuenca.findByNumFolio", query = "SELECT m FROM Microcuenca m WHERE m.microcuencaPK.numFolio = :numFolio"),
    @NamedQuery(name = "Microcuenca.findByCveProy", query = "SELECT m FROM Microcuenca m WHERE m.microcuencaPK.cveProy = :cveProy"),
    @NamedQuery(name = "Microcuenca.findByCveArea", query = "SELECT m FROM Microcuenca m WHERE m.microcuencaPK.cveArea = :cveArea"),
    @NamedQuery(name = "Microcuenca.findByVersion", query = "SELECT m FROM Microcuenca m WHERE m.microcuencaPK.version = :version"),
    @NamedQuery(name = "Microcuenca.findByCueHid", query = "SELECT m FROM Microcuenca m WHERE m.cueHid = :cueHid"),
    @NamedQuery(name = "Microcuenca.findBySubcHid", query = "SELECT m FROM Microcuenca m WHERE m.subcHid = :subcHid"),
    @NamedQuery(name = "Microcuenca.findByNomMic", query = "SELECT m FROM Microcuenca m WHERE m.nomMic = :nomMic"),
    @NamedQuery(name = "Microcuenca.findBySupEa", query = "SELECT m FROM Microcuenca m WHERE m.supEa = :supEa"),
    @NamedQuery(name = "Microcuenca.findByProy", query = "SELECT m FROM Microcuenca m WHERE m.proy = :proy"),
    @NamedQuery(name = "Microcuenca.findByComp", query = "SELECT m FROM Microcuenca m WHERE m.comp = :comp"),
    @NamedQuery(name = "Microcuenca.findByDescrip", query = "SELECT m FROM Microcuenca m WHERE m.descrip = :descrip"),
    @NamedQuery(name = "Microcuenca.findByAreabuffer", query = "SELECT m FROM Microcuenca m WHERE m.areabuffer = :areabuffer"),
    @NamedQuery(name = "Microcuenca.findByArea", query = "SELECT m FROM Microcuenca m WHERE m.area = :area"),
    @NamedQuery(name = "Microcuenca.findByFechaHora", query = "SELECT m FROM Microcuenca m WHERE m.fechaHora = :fechaHora"),
    @NamedQuery(name = "Microcuenca.findByIdr", query = "SELECT m FROM Microcuenca m WHERE m.microcuencaPK.idr = :idr")})
public class Microcuenca implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MicrocuencaPK microcuencaPK;
    @Size(max = 80)
    @Column(name = "CUE_HID")
    private String cueHid;
    @Size(max = 80)
    @Column(name = "SUBC_HID")
    private String subcHid;
    @Size(max = 100)
    @Column(name = "NOM_MIC")
    private String nomMic;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUP_EA")
    private BigDecimal supEa;
    @Size(max = 80)
    @Column(name = "PROY")
    private String proy;
    @Size(max = 80)
    @Column(name = "COMP")
    private String comp;
    @Size(max = 80)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigDecimal areabuffer;
    @Column(name = "AREA")
    private BigDecimal area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public Microcuenca() {
    }

    public Microcuenca(MicrocuencaPK microcuencaPK) {
        this.microcuencaPK = microcuencaPK;
    }

    public Microcuenca(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.microcuencaPK = new MicrocuencaPK(numFolio, cveProy, cveArea, version, idr);
    }

    public MicrocuencaPK getMicrocuencaPK() {
        return microcuencaPK;
    }

    public void setMicrocuencaPK(MicrocuencaPK microcuencaPK) {
        this.microcuencaPK = microcuencaPK;
    }

    public String getCueHid() {
        return cueHid;
    }

    public void setCueHid(String cueHid) {
        this.cueHid = cueHid;
    }

    public String getSubcHid() {
        return subcHid;
    }

    public void setSubcHid(String subcHid) {
        this.subcHid = subcHid;
    }

    public String getNomMic() {
        return nomMic;
    }

    public void setNomMic(String nomMic) {
        this.nomMic = nomMic;
    }

    public BigDecimal getSupEa() {
        return supEa;
    }

    public void setSupEa(BigDecimal supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigDecimal getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigDecimal areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (microcuencaPK != null ? microcuencaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Microcuenca)) {
            return false;
        }
        Microcuenca other = (Microcuenca) object;
        if ((this.microcuencaPK == null && other.microcuencaPK != null) || (this.microcuencaPK != null && !this.microcuencaPK.equals(other.microcuencaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.Microcuenca[ microcuencaPK=" + microcuencaPK + " ]";
    }
    
}
