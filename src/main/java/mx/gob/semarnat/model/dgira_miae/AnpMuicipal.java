/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ANP_MUICIPAL", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnpMuicipal.findAll", query = "SELECT a FROM AnpMuicipal a"),
    @NamedQuery(name = "AnpMuicipal.findByFolioCveVersion", query = "SELECT e FROM AnpMuicipal e WHERE e.anpMuicipalPK.numFolio = :folio and e.anpMuicipalPK.version = :version")})
public class AnpMuicipal implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnpMuicipalPK anpMuicipalPK;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "F_DEC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fDec;
    @Column(name = "FUENTE")
    private String fuente;
    @Column(name = "CATEGORIA")
    private String categoria;
    @Column(name = "SUP_EA")
    private Double supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private Double areabuffer;
    @Column(name = "AREA")
    private Double area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "anpMuicipal")
//    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public AnpMuicipal() {
    }

    public AnpMuicipal(AnpMuicipalPK anpMuicipalPK) {
        this.anpMuicipalPK = anpMuicipalPK;
    }

    public AnpMuicipal(String numFolio, String cveProy, String cveArea, short version) {
        this.anpMuicipalPK = new AnpMuicipalPK(numFolio, cveProy, cveArea, version);
    }

    public AnpMuicipalPK getAnpMuicipalPK() {
        return anpMuicipalPK;
    }

    public void setAnpMuicipalPK(AnpMuicipalPK anpMuicipalPK) {
        this.anpMuicipalPK = anpMuicipalPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFDec() {
        return fDec;
    }

    public void setFDec(Date fDec) {
        this.fDec = fDec;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Double getSupEa() {
        return supEa;
    }

    public void setSupEa(Double supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Double getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(Double areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

//    @XmlTransient
//    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
//        return sigeiaAnalisisList;
//    }
//
//    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
//        this.sigeiaAnalisisList = sigeiaAnalisisList;
//    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anpMuicipalPK != null ? anpMuicipalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnpMuicipal)) {
            return false;
        }
        AnpMuicipal other = (AnpMuicipal) object;
        if ((this.anpMuicipalPK == null && other.anpMuicipalPK != null) || (this.anpMuicipalPK != null && !this.anpMuicipalPK.equals(other.anpMuicipalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AnpMuicipal[ anpMuicipalPK=" + anpMuicipalPK + " ]";
    }

}
