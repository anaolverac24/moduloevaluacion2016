/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class PreguntaObraHelper{
    
    private Integer id;
    private PreguntaObra model;
    private String porcentajeMax;
    private HashMap<Short, String> porcentajes = new HashMap<Short, String>();

    public PreguntaObraHelper() {
    }

    public PreguntaObraHelper(Integer id, PreguntaObra model) {
        this.id = id;
        this.model = model;
        porcentajes.put((short)1, "7.68%");
        porcentajes.put((short)2, "9%");
        porcentajes.put((short)3, "1.5%");
        porcentajes.put((short)4, "1.5%");
        porcentajes.put((short)5, "-");
    }
    

    public String getPorcentajeMax() {
//        if(model.getPreguntaObraPK().getObraId()!=null){
        return porcentajes.get(getModel().getPreguntaObraPK().getObraId());
//        }else{
//            return "";
//        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PreguntaObra getModel() {
        return model;
    }

    public void setModel(PreguntaObra model) {
        this.model = model;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PreguntaObraHelper)) {
            return false;
        }
        PreguntaObraHelper ob = (PreguntaObraHelper) obj;
        if (this.id != ob.id) {
            return false;
        }
        return true;
    }
}
