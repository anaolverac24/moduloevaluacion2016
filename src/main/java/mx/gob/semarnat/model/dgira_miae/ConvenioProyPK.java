/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class ConvenioProyPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "CONVENIO_PROY_ID")
    private short convenioProyId;

    public ConvenioProyPK() {
    }

    public ConvenioProyPK(String folioProyecto, short serialProyecto, short convenioProyId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.convenioProyId = convenioProyId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getConvenioProyId() {
        return convenioProyId;
    }

    public void setConvenioProyId(short convenioProyId) {
        this.convenioProyId = convenioProyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) convenioProyId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConvenioProyPK)) {
            return false;
        }
        ConvenioProyPK other = (ConvenioProyPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.convenioProyId != other.convenioProyId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ConvenioProyPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", convenioProyId=" + convenioProyId + " ]";
    }
    
}
