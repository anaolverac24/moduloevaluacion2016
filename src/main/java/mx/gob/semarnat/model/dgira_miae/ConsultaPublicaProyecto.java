/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CONSULTA_PUBLICA_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConsultaPublicaProyecto.findAll", query = "SELECT c FROM ConsultaPublicaProyecto c"),
    @NamedQuery(name = "ConsultaPublicaProyecto.findByEvaBitacoraProyecto", query = "SELECT c FROM ConsultaPublicaProyecto c WHERE c.consultaPublicaProyectoPK.evaBitacoraProyecto = :evaBitacoraProyecto"),
    @NamedQuery(name = "ConsultaPublicaProyecto.findByClaveDgira", query = "SELECT c FROM ConsultaPublicaProyecto c WHERE c.consultaPublicaProyectoPK.claveDgira = :claveDgira"),
    @NamedQuery(name = "ConsultaPublicaProyecto.findByEvaBitacoraProyectoId", query = "SELECT c FROM ConsultaPublicaProyecto c WHERE c.consultaPublicaProyectoPK.evaBitacoraProyecto = :evaBitacoraProyecto and c.consultaPublicaProyectoPK.claveDgira = :claveDgira"),
    @NamedQuery(name = "ConsultaPublicaProyecto.findByConsultaPublicaRespuesta", query = "SELECT c FROM ConsultaPublicaProyecto c WHERE c.consultaPublicaRespuesta = :consultaPublicaRespuesta")})
public class ConsultaPublicaProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConsultaPublicaProyectoPK consultaPublicaProyectoPK;
    @Size(max = 1)
    @Column(name = "CONSULTA_PUBLICA_RESPUESTA")
    private String consultaPublicaRespuesta;
    @JoinColumn(name = "EVA_BITACORA_PROYECTO", referencedColumnName = "BITACORA_PROYECTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private EvaluacionProyecto evaluacionProyecto;

    public ConsultaPublicaProyecto() {
    }

    public ConsultaPublicaProyecto(ConsultaPublicaProyectoPK consultaPublicaProyectoPK) {
        this.consultaPublicaProyectoPK = consultaPublicaProyectoPK;
    }

    public ConsultaPublicaProyecto(String evaBitacoraProyecto, String claveDgira) {
        this.consultaPublicaProyectoPK = new ConsultaPublicaProyectoPK(evaBitacoraProyecto, claveDgira);
    }

    public ConsultaPublicaProyectoPK getConsultaPublicaProyectoPK() {
        return consultaPublicaProyectoPK;
    }

    public void setConsultaPublicaProyectoPK(ConsultaPublicaProyectoPK consultaPublicaProyectoPK) {
        this.consultaPublicaProyectoPK = consultaPublicaProyectoPK;
    }

    public String getConsultaPublicaRespuesta() {
        return consultaPublicaRespuesta;
    }

    public void setConsultaPublicaRespuesta(String consultaPublicaRespuesta) {
        this.consultaPublicaRespuesta = consultaPublicaRespuesta;
    }

    public EvaluacionProyecto getEvaluacionProyecto() {
        return evaluacionProyecto;
    }

    public void setEvaluacionProyecto(EvaluacionProyecto evaluacionProyecto) {
        this.evaluacionProyecto = evaluacionProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consultaPublicaProyectoPK != null ? consultaPublicaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultaPublicaProyecto)) {
            return false;
        }
        ConsultaPublicaProyecto other = (ConsultaPublicaProyecto) object;
        if ((this.consultaPublicaProyectoPK == null && other.consultaPublicaProyectoPK != null) || (this.consultaPublicaProyectoPK != null && !this.consultaPublicaProyectoPK.equals(other.consultaPublicaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.ConsultaPublicaProyecto[ consultaPublicaProyectoPK=" + consultaPublicaProyectoPK + " ]";
    }
    
}
