/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "ESPECIFICACION_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EspecificacionProyecto.findAll", query = "SELECT e FROM EspecificacionProyecto e"),
    @NamedQuery(name = "EspecificacionProyecto.findByFolioProyecto", query = "SELECT e FROM EspecificacionProyecto e WHERE e.especificacionProyectoPK.folioProyecto = :folioProyecto ORDER BY e.catEspecificacion.especificacionOrden"),
    @NamedQuery(name = "EspecificacionProyecto.findBySerialProyecto", query = "SELECT e FROM EspecificacionProyecto e WHERE e.especificacionProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "EspecificacionProyecto.findByEspecificacionId", query = "SELECT e FROM EspecificacionProyecto e WHERE e.especificacionProyectoPK.especificacionId = :especificacionId"),
    @NamedQuery(name = "EspecificacionProyecto.findByNormaId", query = "SELECT e FROM EspecificacionProyecto e WHERE e.especificacionProyectoPK.normaId = :normaId"),
    @NamedQuery(name = "EspecificacionProyecto.findByEspecificacionAplica", query = "SELECT e FROM EspecificacionProyecto e WHERE e.especificacionAplica = :especificacionAplica"),
    @NamedQuery(name = "EspecificacionProyecto.findByEspFechaCompromisoFin", query = "SELECT e FROM EspecificacionProyecto e WHERE e.espFechaCompromisoFin = :espFechaCompromisoFin")})
public class EspecificacionProyecto implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "especificacionProyecto")
    private List<EspecificacionAnexo> especificacionAnexoList;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EspecificacionProyectoPK especificacionProyectoPK;
    @Lob
    @Column(name = "ESPECIFICACION_JUSTIFICACION")
    private String especificacionJustificacion;
    @Column(name = "ESPECIFICACION_APLICA")
    private Character especificacionAplica;
    @Column(name = "ESP_FECHA_COMPROMISO_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date espFechaCompromisoFin;
    @JoinColumns({
        @JoinColumn(name = "ESPECIFICACION_ID", referencedColumnName = "ESPECIFICACION_ID", insertable = false, updatable = false),
        @JoinColumn(name = "NORMA_ID", referencedColumnName = "NORMA_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CatEspecificacion catEspecificacion;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public EspecificacionProyecto() {
    }
    
    public int getCaracteres() {
        if (especificacionJustificacion != null) {
            return catEspecificacion.getEspecificacionLimite() - especificacionJustificacion.length();
        }
        return catEspecificacion.getEspecificacionLimite();
    }
    public EspecificacionProyecto(EspecificacionProyectoPK especificacionProyectoPK) {
        this.especificacionProyectoPK = especificacionProyectoPK;
    }

    public EspecificacionProyecto(String folioProyecto, short serialProyecto, String especificacionId, short normaId) {
        this.especificacionProyectoPK = new EspecificacionProyectoPK(folioProyecto, serialProyecto, especificacionId, normaId);
    }

    public EspecificacionProyectoPK getEspecificacionProyectoPK() {
        return especificacionProyectoPK;
    }

    public void setEspecificacionProyectoPK(EspecificacionProyectoPK especificacionProyectoPK) {
        this.especificacionProyectoPK = especificacionProyectoPK;
    }

    public String getEspecificacionJustificacion() {
        return especificacionJustificacion;
    }

    public void setEspecificacionJustificacion(String especificacionJustificacion) {
        this.especificacionJustificacion = especificacionJustificacion;
    }

    public Character getEspecificacionAplica() {
        return especificacionAplica;
    }

    public void setEspecificacionAplica(Character especificacionAplica) {
        this.especificacionAplica = especificacionAplica;
    }

    public Date getEspFechaCompromisoFin() {
        return espFechaCompromisoFin;
    }

    public void setEspFechaCompromisoFin(Date espFechaCompromisoFin) {
        this.espFechaCompromisoFin = espFechaCompromisoFin;
    }

    public CatEspecificacion getCatEspecificacion() {
        return catEspecificacion;
    }

    public void setCatEspecificacion(CatEspecificacion catEspecificacion) {
        this.catEspecificacion = catEspecificacion;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (especificacionProyectoPK != null ? especificacionProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EspecificacionProyecto)) {
            return false;
        }
        EspecificacionProyecto other = (EspecificacionProyecto) object;
        if ((this.especificacionProyectoPK == null && other.especificacionProyectoPK != null) || (this.especificacionProyectoPK != null && !this.especificacionProyectoPK.equals(other.especificacionProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.EspecificacionProyecto[ especificacionProyectoPK=" + especificacionProyectoPK + " ]";
    }

    @XmlTransient
    public List<EspecificacionAnexo> getEspecificacionAnexoList() {
        return especificacionAnexoList;
    }

    public void setEspecificacionAnexoList(List<EspecificacionAnexo> especificacionAnexoList) {
        this.especificacionAnexoList = especificacionAnexoList;
    }
    
}
