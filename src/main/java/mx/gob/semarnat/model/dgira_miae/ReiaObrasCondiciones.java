/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_OBRAS_CONDICIONES", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaObrasCondiciones.findAll", query = "SELECT r FROM ReiaObrasCondiciones r"),
    @NamedQuery(name = "ReiaObrasCondiciones.findByIdObCon", query = "SELECT r FROM ReiaObrasCondiciones r WHERE r.idObCon = :idObCon"),
    
    @NamedQuery(name = "ReiaObrasCondiciones.findCondicionByIdOb", query = "SELECT r FROM ReiaObrasCondiciones r WHERE r.idObra.idObra = :idObra"),
    
    
    @NamedQuery(name = "ReiaObrasCondiciones.findByCompuerta", query = "SELECT r FROM ReiaObrasCondiciones r WHERE r.compuerta = :compuerta")})
public class ReiaObrasCondiciones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_OB_CON")
    private Long idObCon;
    @Column(name = "COMPUERTA")
    private String compuerta;
    @JoinColumn(name = "ID_CONDICION", referencedColumnName = "ID_CONDICION")
    @ManyToOne
    private ReiaCondiciones idCondicion;
    @JoinColumn(name = "ID_OBRA", referencedColumnName = "ID_OBRA")
    @ManyToOne
    private ReiaObras idObra;

    public ReiaObrasCondiciones() {
    }

    public ReiaObrasCondiciones(Long idObCon) {
        this.idObCon = idObCon;
    }

    public Long getIdObCon() {
        return idObCon;
    }

    public void setIdObCon(Long idObCon) {
        this.idObCon = idObCon;
    }

    public String getCompuerta() {
        return compuerta;
    }

    public void setCompuerta(String compuerta) {
        this.compuerta = compuerta;
    }

    public ReiaCondiciones getIdCondicion() {
        return idCondicion;
    }

    public void setIdCondicion(ReiaCondiciones idCondicion) {
        this.idCondicion = idCondicion;
    }

    public ReiaObras getIdObra() {
        return idObra;
    }

    public void setIdObra(ReiaObras idObra) {
        this.idObra = idObra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObCon != null ? idObCon.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReiaObrasCondiciones)) {
            return false;
        }
        ReiaObrasCondiciones other = (ReiaObrasCondiciones) object;
        if ((this.idObCon == null && other.idObCon != null) || (this.idObCon != null && !this.idObCon.equals(other.idObCon))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaObrasCondiciones[ idObCon=" + idObCon + " ]";
    }
    
}
