package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Entity
@Table(name = "EXENCION_REQUISITOS_EVALUADOR", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({ 
	@NamedQuery(name = "ExencionRequisitosEvaluador.findAll", query = "SELECT e FROM ExencionRequisitosEvaluador e")
//	@NamedQuery(name = "ExencionRequisitosEvaluador.findByExeBySeccByReq", query = "SELECT e FROM ExencionRequisitosEvaluador e WHERE e.exencionRequisitoEvaluadorPK.idExencion = ?1 AND e.exencionRequisitoEvaluadorPK.nSeccion = ?2 AND e.exencionRequisitoEvaluadorPK.nRequisito = ?3")
	})
public class ExencionRequisitosEvaluador implements Serializable {

	@EmbeddedId
	private ExencionRequisitosEvaluadorPk exencionRequisitoEvaluadorPK;

	@Column(name = "VALIDADO")
	private Boolean validado;
	
	@Column(name = "OBSERVACIONES")
	private String observaciones;
	
	@Column(name = "RESPUESTA")
	private String respuesta;
	
	public ExencionRequisitosEvaluador() {
		// TODO Auto-generated constructor stub
	}
	
	public ExencionRequisitosEvaluadorPk getExencionRequisitoEvaluadorPK() {
		return exencionRequisitoEvaluadorPK;
	}

	public void setExencionRequisitoEvaluadorPK(ExencionRequisitosEvaluadorPk exencionRequisitoEvaluadorPK) {
		this.exencionRequisitoEvaluadorPK = exencionRequisitoEvaluadorPK;
	}

	public Boolean getValidado() {
		return validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
}
