/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chely
 */
@Entity
@Table(name = "REQUISITOS_SUBDIRECTOR",schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequisitosSubdirector.findAll", query = "SELECT r FROM RequisitosSubdirector r"),
    @NamedQuery(name = "RequisitosSubdirector.findByReqsdBitacora", query = "SELECT r FROM RequisitosSubdirector r WHERE r.requisitosSubdirectorPK.reqsdBitacora = :reqsdBitacora"),
    @NamedQuery(name = "RequisitosSubdirector.findByReqsdNumeroRequisito", query = "SELECT r FROM RequisitosSubdirector r WHERE r.requisitosSubdirectorPK.reqsdNumeroRequisito = :reqsdNumeroRequisito"),
    @NamedQuery(name = "RequisitosSubdirector.findByReqsdClaveTramite", query = "SELECT r FROM RequisitosSubdirector r WHERE r.requisitosSubdirectorPK.reqsdClaveTramite = :reqsdClaveTramite"),
    @NamedQuery(name = "RequisitosSubdirector.findByReqsdIdTramite", query = "SELECT r FROM RequisitosSubdirector r WHERE r.reqsdIdTramite = :reqsdIdTramite"),
    @NamedQuery(name = "RequisitosSubdirector.findByReqsdEntregadoEcc", query = "SELECT r FROM RequisitosSubdirector r WHERE r.reqsdEntregadoEcc = :reqsdEntregadoEcc"),
    @NamedQuery(name = "RequisitosSubdirector.findByReqsdObservaciones", query = "SELECT r FROM RequisitosSubdirector r WHERE r.reqsdObservaciones = :reqsdObservaciones"),
    @NamedQuery(name = "RequisitosSubdirector.findByReqsdEntregadoSd", query = "SELECT r FROM RequisitosSubdirector r WHERE r.reqsdEntregadoSd = :reqsdEntregadoSd")})
public class RequisitosSubdirector implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RequisitosSubdirectorPK requisitosSubdirectorPK;
    @Basic(optional = false)
    @Column(name = "REQSD_ID_TRAMITE")
    private BigInteger reqsdIdTramite;
    @Column(name = "REQSD_ENTREGADO_ECC")
    private BigInteger reqsdEntregadoEcc;
    @Column(name = "REQSD_OBSERVACIONES")
    private String reqsdObservaciones;
    @Column(name = "REQSD_ENTREGADO_SD")
    private BigInteger reqsdEntregadoSd;
    @Column(name = "REQSD_NOMBRE_REQUISITO")
    private String reqsdNombreRequisito;

    public RequisitosSubdirector() {
    }

    public RequisitosSubdirector(RequisitosSubdirectorPK requisitosSubdirectorPK) {
        this.requisitosSubdirectorPK = requisitosSubdirectorPK;
    }

    public RequisitosSubdirector(RequisitosSubdirectorPK requisitosSubdirectorPK, BigInteger reqsdIdTramite) {
        this.requisitosSubdirectorPK = requisitosSubdirectorPK;
        this.reqsdIdTramite = reqsdIdTramite;
        this.reqsdNombreRequisito = null;
    }

    public RequisitosSubdirector(RequisitosSubdirectorPK requisitosSubdirectorPK, BigInteger reqsdIdTramite, String reqsdNombreRequisito) {
        this.requisitosSubdirectorPK = requisitosSubdirectorPK;
        this.reqsdIdTramite = reqsdIdTramite;
        this.reqsdNombreRequisito = reqsdNombreRequisito;
    }

    public RequisitosSubdirector(String reqsdBitacora, BigInteger reqsdNumeroRequisito, String reqsdClaveTramite) {
        this.requisitosSubdirectorPK = new RequisitosSubdirectorPK(reqsdBitacora, reqsdNumeroRequisito, reqsdClaveTramite);
    }

    public RequisitosSubdirectorPK getRequisitosSubdirectorPK() {
        return requisitosSubdirectorPK;
    }

    public void setRequisitosSubdirectorPK(RequisitosSubdirectorPK requisitosSubdirectorPK) {
        this.requisitosSubdirectorPK = requisitosSubdirectorPK;
    }

    public BigInteger getReqsdIdTramite() {
        return reqsdIdTramite;
    }

    public void setReqsdIdTramite(BigInteger reqsdIdTramite) {
        this.reqsdIdTramite = reqsdIdTramite;
    }

    public BigInteger getReqsdEntregadoEcc() {
        return reqsdEntregadoEcc;
    }

    public void setReqsdEntregadoEcc(BigInteger reqsdEntregadoEcc) {
        this.reqsdEntregadoEcc = reqsdEntregadoEcc;
    }

    public String getReqsdObservaciones() {
        return reqsdObservaciones;
    }

    public void setReqsdObservaciones(String reqsdObservaciones) {
        this.reqsdObservaciones = reqsdObservaciones;
    }

    public BigInteger getReqsdEntregadoSd() {
        return reqsdEntregadoSd;
    }

    public void setReqsdEntregadoSd(BigInteger reqsdEntregadoSd) {
        this.reqsdEntregadoSd = reqsdEntregadoSd;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (requisitosSubdirectorPK != null ? requisitosSubdirectorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequisitosSubdirector)) {
            return false;
        }
        RequisitosSubdirector other = (RequisitosSubdirector) object;
        if ((this.requisitosSubdirectorPK == null && other.requisitosSubdirectorPK != null) || (this.requisitosSubdirectorPK != null && !this.requisitosSubdirectorPK.equals(other.requisitosSubdirectorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.RequisitosSubdirector[ requisitosSubdirectorPK=" + requisitosSubdirectorPK + " ]";
    }

    public String getReqsdNombreRequisito() {
        return reqsdNombreRequisito;
    }

    public void setReqsdNombreRequisito(String reqsdNombreRequisito) {
        this.reqsdNombreRequisito = reqsdNombreRequisito;
    }
    
}
