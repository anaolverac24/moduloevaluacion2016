/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "DOCUMENTO_DGIRA_FLUJO_HIST", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findAll", query = "SELECT d FROM DocumentoDgiraFlujoHist d"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByBitacoraProyecto", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByBitacoraProyectoTipoDoc", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.bitacoraProyecto = :bitacoraProyecto AND d.documentoDgiraFlujoHistPK.tipoDocId = :tipoDocId"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByClaveTramite", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.claveTramite = :claveTramite"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByIdTramite", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.idTramite = :idTramite"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByTipoDocId", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.tipoDocId = :tipoDocId"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByIdEntidadFederativa", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.idEntidadFederativa = :idEntidadFederativa"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByDocumentoId", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.documentoId = :documentoId"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByDocumentoDgiraIdHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFlujoHistPK.documentoDgiraIdHist = :documentoDgiraIdHist"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByIdAreaEnvioHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.idAreaEnvioHist = :idAreaEnvioHist"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByIdAreaRecibeHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.idAreaRecibeHist = :idAreaRecibeHist"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByIdUsuarioEnvioHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.idUsuarioEnvioHist = :idUsuarioEnvioHist"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByIdUsuarioRecibeHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.idUsuarioRecibeHist = :idUsuarioRecibeHist"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByDocumentoDgiraFeEnvHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFeEnvHist = :documentoDgiraFeEnvHist"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByDocumentoDgiraFeRecHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraFeRecHist = :documentoDgiraFeRecHist"),
    @NamedQuery(name = "DocumentoDgiraFlujoHist.findByDocumentoDgiraObservHist", query = "SELECT d FROM DocumentoDgiraFlujoHist d WHERE d.documentoDgiraObservHist = :documentoDgiraObservHist")})
public class DocumentoDgiraFlujoHist implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DocumentoDgiraFlujoHistPK documentoDgiraFlujoHistPK;
    @Size(max = 8)
    @Column(name = "ID_AREA_ENVIO_HIST")
    private String idAreaEnvioHist;
    @Size(max = 8)
    @Column(name = "ID_AREA_RECIBE_HIST")
    private String idAreaRecibeHist;
    @Column(name = "ID_USUARIO_ENVIO_HIST")
    private Integer idUsuarioEnvioHist;
    @Column(name = "ID_USUARIO_RECIBE_HIST")
    private Integer idUsuarioRecibeHist;
    @Column(name = "DOCUMENTO_DGIRA_FE_ENV_HIST")
    @Temporal(TemporalType.TIMESTAMP)
    private Date documentoDgiraFeEnvHist;
    @Column(name = "DOCUMENTO_DGIRA_FE_REC_HIST")
    @Temporal(TemporalType.TIMESTAMP)
    private Date documentoDgiraFeRecHist;
    @Size(max = 4000)
    @Column(name = "DOCUMENTO_DGIRA_OBSERV_HIST")
    private String documentoDgiraObservHist;
    @JoinColumn(name = "ESTATUS_DOCUMENTO_ID_HIST", referencedColumnName = "ESTATUS_DOCUMENTO_ID")
    @ManyToOne
    private CatEstatusDocumento estatusDocumentoIdHist;
    @JoinColumn(name = "BITACORA_PROYECTO", referencedColumnName = "BITACORA_PROYECTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private EvaluacionProyecto evaluacionProyecto;

    public DocumentoDgiraFlujoHist() {
    }

    public DocumentoDgiraFlujoHist(DocumentoDgiraFlujoHistPK documentoDgiraFlujoHistPK) {
        this.documentoDgiraFlujoHistPK = documentoDgiraFlujoHistPK;
    }

    public DocumentoDgiraFlujoHist(String bitacoraProyecto, String claveTramite, int idTramite, short tipoDocId, String idEntidadFederativa, String documentoId, short documentoDgiraIdHist) {
        this.documentoDgiraFlujoHistPK = new DocumentoDgiraFlujoHistPK(bitacoraProyecto, claveTramite, idTramite, tipoDocId, idEntidadFederativa, documentoId, documentoDgiraIdHist);
    }

    public DocumentoDgiraFlujoHistPK getDocumentoDgiraFlujoHistPK() {
        return documentoDgiraFlujoHistPK;
    }

    public void setDocumentoDgiraFlujoHistPK(DocumentoDgiraFlujoHistPK documentoDgiraFlujoHistPK) {
        this.documentoDgiraFlujoHistPK = documentoDgiraFlujoHistPK;
    }

    public String getIdAreaEnvioHist() {
        return idAreaEnvioHist;
    }

    public void setIdAreaEnvioHist(String idAreaEnvioHist) {
        this.idAreaEnvioHist = idAreaEnvioHist;
    }

    public String getIdAreaRecibeHist() {
        return idAreaRecibeHist;
    }

    public void setIdAreaRecibeHist(String idAreaRecibeHist) {
        this.idAreaRecibeHist = idAreaRecibeHist;
    }

    public Integer getIdUsuarioEnvioHist() {
        return idUsuarioEnvioHist;
    }

    public void setIdUsuarioEnvioHist(Integer idUsuarioEnvioHist) {
        this.idUsuarioEnvioHist = idUsuarioEnvioHist;
    }

    public Integer getIdUsuarioRecibeHist() {
        return idUsuarioRecibeHist;
    }

    public void setIdUsuarioRecibeHist(Integer idUsuarioRecibeHist) {
        this.idUsuarioRecibeHist = idUsuarioRecibeHist;
    }

    public Date getDocumentoDgiraFeEnvHist() {
        return documentoDgiraFeEnvHist;
    }

    public void setDocumentoDgiraFeEnvHist(Date documentoDgiraFeEnvHist) {
        this.documentoDgiraFeEnvHist = documentoDgiraFeEnvHist;
    }

    public Date getDocumentoDgiraFeRecHist() {
        return documentoDgiraFeRecHist;
    }

    public void setDocumentoDgiraFeRecHist(Date documentoDgiraFeRecHist) {
        this.documentoDgiraFeRecHist = documentoDgiraFeRecHist;
    }

    public String getDocumentoDgiraObservHist() {
        return documentoDgiraObservHist;
    }

    public void setDocumentoDgiraObservHist(String documentoDgiraObservHist) {
        this.documentoDgiraObservHist = documentoDgiraObservHist;
    }

    public CatEstatusDocumento getEstatusDocumentoIdHist() {
        return estatusDocumentoIdHist;
    }

    public void setEstatusDocumentoIdHist(CatEstatusDocumento estatusDocumentoIdHist) {
        this.estatusDocumentoIdHist = estatusDocumentoIdHist;
    }

    public EvaluacionProyecto getEvaluacionProyecto() {
        return evaluacionProyecto;
    }

    public void setEvaluacionProyecto(EvaluacionProyecto evaluacionProyecto) {
        this.evaluacionProyecto = evaluacionProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentoDgiraFlujoHistPK != null ? documentoDgiraFlujoHistPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoDgiraFlujoHist)) {
            return false;
        }
        DocumentoDgiraFlujoHist other = (DocumentoDgiraFlujoHist) object;
        if ((this.documentoDgiraFlujoHistPK == null && other.documentoDgiraFlujoHistPK != null) || (this.documentoDgiraFlujoHistPK != null && !this.documentoDgiraFlujoHistPK.equals(other.documentoDgiraFlujoHistPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujoHist[ documentoDgiraFlujoHistPK=" + documentoDgiraFlujoHistPK + " ]";
    }
    
}
