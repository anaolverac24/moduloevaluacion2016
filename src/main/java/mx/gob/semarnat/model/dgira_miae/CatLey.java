/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_LEY", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatLey.findAll", query = "SELECT c FROM CatLey c"),
    @NamedQuery(name = "CatLey.findByLeyId", query = "SELECT c FROM CatLey c WHERE c.leyId = :leyId"),
    @NamedQuery(name = "CatLey.findByLeyTipo", query = "SELECT c FROM CatLey c WHERE c.leyTipo = :leyTipo"),
    @NamedQuery(name = "CatLey.findByLeyNombre", query = "SELECT c FROM CatLey c WHERE c.leyNombre = :leyNombre"),
    @NamedQuery(name = "CatLey.findByLeyFechaPublicacion", query = "SELECT c FROM CatLey c WHERE c.leyFechaPublicacion = :leyFechaPublicacion"),
    @NamedQuery(name = "CatLey.findByLeyFechaModificacion", query = "SELECT c FROM CatLey c WHERE c.leyFechaModificacion = :leyFechaModificacion"),
    @NamedQuery(name = "CatLey.findByLeyUrl", query = "SELECT c FROM CatLey c WHERE c.leyUrl = :leyUrl")})
public class CatLey implements Serializable {
    @OneToMany(mappedBy = "leyId")
    private List<LeyFedEstProyecto> leyFedEstProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LEY_ID")
    private Short leyId;
    @Column(name = "LEY_TIPO")
    private Character leyTipo;
    @Column(name = "LEY_NOMBRE")
    private String leyNombre;
    @Column(name = "LEY_FECHA_PUBLICACION")
    private String leyFechaPublicacion;
    @Column(name = "LEY_FECHA_MODIFICACION")
    private String leyFechaModificacion;
    @Column(name = "LEY_URL")
    private String leyUrl;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "leyId")
    private List<LeyProyecto> leyProyectoList;

    public CatLey() {
    }

    public CatLey(Short leyId) {
        this.leyId = leyId;
    }

    public Short getLeyId() {
        return leyId;
    }

    public void setLeyId(Short leyId) {
        this.leyId = leyId;
    }

//    public Character getLeyTipo() {
//        return leyTipo;
//    }
//
//    public void setLeyTipo(Character leyTipo) {
//        this.leyTipo = leyTipo;
//    }

    public String getLeyNombre() {
        return leyNombre;
    }

    public void setLeyNombre(String leyNombre) {
        this.leyNombre = leyNombre;
    }

    public String getLeyFechaPublicacion() {
        return leyFechaPublicacion;
    }

    public void setLeyFechaPublicacion(String leyFechaPublicacion) {
        this.leyFechaPublicacion = leyFechaPublicacion;
    }

    public String getLeyFechaModificacion() {
        return leyFechaModificacion;
    }

    public void setLeyFechaModificacion(String leyFechaModificacion) {
        this.leyFechaModificacion = leyFechaModificacion;
    }

    public String getLeyUrl() {
        return leyUrl;
    }

    public void setLeyUrl(String leyUrl) {
        this.leyUrl = leyUrl;
    }

    @XmlTransient
    public List<LeyProyecto> getLeyProyectoList() {
        return leyProyectoList;
    }

    public void setLeyProyectoList(List<LeyProyecto> leyProyectoList) {
        this.leyProyectoList = leyProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leyId != null ? leyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatLey)) {
            return false;
        }
        CatLey other = (CatLey) object;
        if ((this.leyId == null && other.leyId != null) || (this.leyId != null && !this.leyId.equals(other.leyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return leyNombre;
    }

    @XmlTransient
    public List<LeyFedEstProyecto> getLeyFedEstProyectoList() {
        return leyFedEstProyectoList;
    }

    public void setLeyFedEstProyectoList(List<LeyFedEstProyecto> leyFedEstProyectoList) {
        this.leyFedEstProyectoList = leyFedEstProyectoList;
    }
    
}
