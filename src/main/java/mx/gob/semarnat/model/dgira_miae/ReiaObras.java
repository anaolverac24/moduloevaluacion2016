/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_OBRAS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaObras.findAll", query = "SELECT r FROM ReiaObras r"),
    @NamedQuery(name = "ReiaObras.findByIdObra", query = "SELECT r FROM ReiaObras r WHERE r.idObra = :idObra"),
    @NamedQuery(name = "ReiaObras.findByObra", query = "SELECT r FROM ReiaObras r WHERE r.obra = :obra"),
    @NamedQuery(name = "ReiaObras.findByFraccion", query = "SELECT r FROM ReiaObras r WHERE r.fraccion = :fraccion"),
    
    @NamedQuery(name = "ReiaObras.getByCategoria", query = "SELECT o FROM ReiaObras o WHERE o.idCategoria.idCategoria = :categoria and o.fraccion != null"),
    
    @NamedQuery(name = "ReiaObras.findByExcepcion", query = "SELECT r FROM ReiaObras r WHERE r.excepcion = :excepcion")})
public class ReiaObras implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_OBRA")
    private Long idObra;
    @Column(name = "OBRA")
    private String obra;
    @Column(name = "FRACCION")
    private String fraccion;
    @Column(name = "EXCEPCION")
    private Long excepcion;
    @JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID_CATEGORIA")
    @ManyToOne
    private ReiaCategorias idCategoria;
    @OneToMany(mappedBy = "idObra")
    private Collection<ReiaObrasCondiciones> reiaObrasCondicionesCollection;
    
    @Column(name = "SUBSECTOR")
    private Long subsector;
    
    @Column(name = "RAMA")
    private Long rama;
    
    @Column(name = "TIPO")
    private Long tipo;
    

    public ReiaObras() {
    }

    public ReiaObras(Long idObra) {
        this.idObra = idObra;
    }

    public Long getIdObra() {
        return idObra;
    }

    public void setIdObra(Long idObra) {
        this.idObra = idObra;
    }

    public String getObra() {
        return obra;
    }

    public void setObra(String obra) {
        this.obra = obra;
    }

    public String getFraccion() {
        return fraccion;
    }

    public void setFraccion(String fraccion) {
        this.fraccion = fraccion;
    }

    public Long getExcepcion() {
        return excepcion;
    }

    public void setExcepcion(Long excepcion) {
        this.excepcion = excepcion;
    }

    public ReiaCategorias getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(ReiaCategorias idCategoria) {
        this.idCategoria = idCategoria;
    }

    @XmlTransient
    public Collection<ReiaObrasCondiciones> getReiaObrasCondicionesCollection() {
        return reiaObrasCondicionesCollection;
    }

    public void setReiaObrasCondicionesCollection(Collection<ReiaObrasCondiciones> reiaObrasCondicionesCollection) {
        this.reiaObrasCondicionesCollection = reiaObrasCondicionesCollection;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObra != null ? idObra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReiaObras)) {
            return false;
        }
        ReiaObras other = (ReiaObras) object;
        if ((this.idObra == null && other.idObra != null) || (this.idObra != null && !this.idObra.equals(other.idObra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaObras[ idObra=" + idObra + " ]";
    }

    public Long getSubsector() {
        return subsector;
    }

    public void setSubsector(Long subsector) {
        this.subsector = subsector;
    }

    public Long getRama() {
        return rama;
    }

    public void setRama(Long rama) {
        this.rama = rama;
    }

    public Long getTipo() {
        return tipo;
    }

    public void setTipo(Long tipo) {
        this.tipo = tipo;
    }
    
}
