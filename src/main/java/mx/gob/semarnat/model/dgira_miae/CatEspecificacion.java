/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_ESPECIFICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEspecificacion.findAll", query = "SELECT c FROM CatEspecificacion c"),
    @NamedQuery(name = "CatEspecificacion.findByEspecificacionId", query = "SELECT c FROM CatEspecificacion c WHERE c.catEspecificacionPK.especificacionId = :especificacionId"),
    @NamedQuery(name = "CatEspecificacion.findByNormaId", query = "SELECT c FROM CatEspecificacion c WHERE c.catEspecificacionPK.normaId = :normaId"),


    @NamedQuery(name = "CatEspecificacion.findByEspecificacionDescripcion", query = "SELECT c FROM CatEspecificacion c WHERE c.especificacionDescripcion = :especificacionDescripcion"),
    @NamedQuery(name = "CatEspecificacion.findByEspecificacionAnexoRequest", query = "SELECT c FROM CatEspecificacion c WHERE c.especificacionAnexoRequest = :especificacionAnexoRequest")})
public class CatEspecificacion implements Serializable {



    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatEspecificacionPK catEspecificacionPK;

    @Basic(optional = false)
    @Column(name = "ESPECIFICACION_DESCRIPCION")
    private String especificacionDescripcion;


    @Column(name = "ESPECIFICACION_LIMITE")
    private Short especificacionLimite;
    @Column(name = "ESPECIFICACION_ORDEN")
    private Short especificacionOrden;

    @Basic(optional = false)
    @Column(name = "ESPECIFICACION_ANEXO_REQUEST")
    private Character especificacionAnexoRequest;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catEspecificacion")
    private List<EspecificacionProyecto> especificacionProyectoList;
    @JoinColumn(name = "NORMA_ID", referencedColumnName = "NORMA_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatNorma catNorma;

    public CatEspecificacion() {
    }

    public CatEspecificacion(CatEspecificacionPK catEspecificacionPK) {
        this.catEspecificacionPK = catEspecificacionPK;
    }

    public CatEspecificacion(CatEspecificacionPK catEspecificacionPK, String especificacionDescripcion, Character especificacionAnexoRequest) {
        this.catEspecificacionPK = catEspecificacionPK;
        this.especificacionDescripcion = especificacionDescripcion;
        this.especificacionAnexoRequest = especificacionAnexoRequest;
    }

    public CatEspecificacion(String especificacionId, short normaId) {
        this.catEspecificacionPK = new CatEspecificacionPK(especificacionId, normaId);
    }

    public CatEspecificacionPK getCatEspecificacionPK() {
        return catEspecificacionPK;
    }

    public void setCatEspecificacionPK(CatEspecificacionPK catEspecificacionPK) {
        this.catEspecificacionPK = catEspecificacionPK;
    }

    public String getEspecificacionDescripcion() {
        return especificacionDescripcion;
    }

    public void setEspecificacionDescripcion(String especificacionDescripcion) {
        this.especificacionDescripcion = especificacionDescripcion;
    }

    public Character getEspecificacionAnexoRequest() {
        return especificacionAnexoRequest;
    }

    public void setEspecificacionAnexoRequest(Character especificacionAnexoRequest) {
        this.especificacionAnexoRequest = especificacionAnexoRequest;
    }











    @XmlTransient
    public List<EspecificacionProyecto> getEspecificacionProyectoList() {
        return especificacionProyectoList;
    }



    public void setEspecificacionProyectoList(List<EspecificacionProyecto> especificacionProyectoList) {
        this.especificacionProyectoList = especificacionProyectoList;
    }

    public CatNorma getCatNorma() {
        return catNorma;
    }

    public void setCatNorma(CatNorma catNorma) {
        this.catNorma = catNorma;
    }

    public Boolean getAnexo() {
        return (especificacionAnexoRequest == 'S');
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catEspecificacionPK != null ? catEspecificacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEspecificacion)) {
            return false;
        }
        CatEspecificacion other = (CatEspecificacion) object;
        if ((this.catEspecificacionPK == null && other.catEspecificacionPK != null) || (this.catEspecificacionPK != null && !this.catEspecificacionPK.equals(other.catEspecificacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelEmia.CatEspecificacion[ catEspecificacionPK=" + catEspecificacionPK + " ]";
    }




    /**
     * @return the especificacionLimite
     */
    public Short getEspecificacionLimite() {
        return especificacionLimite;
    }



    /**
     * @param especificacionLimite the especificacionLimite to set
     */
    public void setEspecificacionLimite(Short especificacionLimite) {
        this.especificacionLimite = especificacionLimite;
    }


    /**
     * @return the especificacionOrden
     */
    public Short getEspecificacionOrden() {
        return especificacionOrden;
    }

    /**
     * @param especificacionOrden the especificacionOrden to set
     */
    public void setEspecificacionOrden(Short especificacionOrden) {
        this.especificacionOrden = especificacionOrden;
    }

}
