/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_CONTAMINANTE", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatContaminante.findAll", query = "SELECT c FROM CatContaminante c"),
    @NamedQuery(name = "CatContaminante.findByContaminanteId", query = "SELECT c FROM CatContaminante c WHERE c.contaminanteId = :contaminanteId"),
    @NamedQuery(name = "CatContaminante.findByContaminanteNombre", query = "SELECT c FROM CatContaminante c WHERE c.contaminanteNombre = :contaminanteNombre")})
public class CatContaminante implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catContaminante")
    private List<ContaminanteProyecto> contaminanteProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONTAMINANTE_ID")
    private Short contaminanteId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONTAMINANTE_NOMBRE")
    private String contaminanteNombre;
    @JoinColumn(name = "TIPO_CONTAMINANTE_ID", referencedColumnName = "TIPO_CONTAMINANTE_ID")
    @ManyToOne(optional = false)
    private CatTipoContaminante tipoContaminanteId;

    public CatContaminante() {
    }

    public CatContaminante(Short contaminanteId) {
        this.contaminanteId = contaminanteId;
    }

    public CatContaminante(Short contaminanteId, String contaminanteNombre) {
        this.contaminanteId = contaminanteId;
        this.contaminanteNombre = contaminanteNombre;
    }

    public Short getContaminanteId() {
        return contaminanteId;
    }

    public void setContaminanteId(Short contaminanteId) {
        this.contaminanteId = contaminanteId;
    }

    public String getContaminanteNombre() {
        return contaminanteNombre;
    }

    public void setContaminanteNombre(String contaminanteNombre) {
        this.contaminanteNombre = contaminanteNombre;
    }

    public CatTipoContaminante getTipoContaminanteId() {
        return tipoContaminanteId;
    }

    public void setTipoContaminanteId(CatTipoContaminante tipoContaminanteId) {
        this.tipoContaminanteId = tipoContaminanteId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contaminanteId != null ? contaminanteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatContaminante)) {
            return false;
        }
        CatContaminante other = (CatContaminante) object;
        if ((this.contaminanteId == null && other.contaminanteId != null) || (this.contaminanteId != null && !this.contaminanteId.equals(other.contaminanteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return contaminanteNombre;
    }

    @XmlTransient
    public List<ContaminanteProyecto> getContaminanteProyectoList() {
        return contaminanteProyectoList;
    }

    public void setContaminanteProyectoList(List<ContaminanteProyecto> contaminanteProyectoList) {
        this.contaminanteProyectoList = contaminanteProyectoList;
    }
    
}
