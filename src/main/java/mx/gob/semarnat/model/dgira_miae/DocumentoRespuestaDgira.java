/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "DOCUMENTO_RESPUESTA_DGIRA", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoRespuestaDgira.findAll", query = "SELECT d FROM DocumentoRespuestaDgira d"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByBitacoraProyecto", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByClaveTramite", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.claveTramite = :claveTramite"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByIdTramite", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.idTramite = :idTramite"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByTipoDocId", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.tipoDocId = :tipoDocId"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByIdEntidadFederativa", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.idEntidadFederativa = :idEntidadFederativa"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByDocumentoId", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.documentoId = :documentoId"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByDocumentoDgira", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoDgira = :documentoDgira"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByDocumentoNombre", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoNombre = :documentoNombre"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByDocumentoUrl", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoUrl = :documentoUrl"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByDocumentoUbicacion", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoUbicacion = :documentoUbicacion"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByBitacoraDocumento", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto AND d.documentoRespuestaDgiraPK.tipoDocId = :tipoDocId AND d.documentoRespuestaDgiraPK.documentoId = :documentoId"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByBitacoraTipoDocumento", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto AND d.documentoRespuestaDgiraPK.tipoDocId = :tipoDocId"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByResolutivoMIA", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto AND (d.documentoRespuestaDgiraPK.tipoDocId = 6 or d.documentoRespuestaDgiraPK.tipoDocId = 12 or d.documentoRespuestaDgiraPK.tipoDocId = 13 or d.documentoRespuestaDgiraPK.tipoDocId = 14 or d.documentoRespuestaDgiraPK.tipoDocId = 21)"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByPrevencionMIA", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto AND (d.documentoRespuestaDgiraPK.tipoDocId = 3 or d.documentoRespuestaDgiraPK.tipoDocId = 4 or d.documentoRespuestaDgiraPK.tipoDocId = 5)"),
    @NamedQuery(name = "DocumentoRespuestaDgira.findByResolutivoDC", query = "SELECT d FROM DocumentoRespuestaDgira d WHERE d.documentoRespuestaDgiraPK.bitacoraProyecto = :bitacoraProyecto AND (d.documentoRespuestaDgiraPK.tipoDocId = 1 or d.documentoRespuestaDgiraPK.tipoDocId = 2)")
})
public class DocumentoRespuestaDgira implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DocumentoRespuestaDgiraPK documentoRespuestaDgiraPK;
    @Size(max = 31)
    @Column(name = "DOCUMENTO_DGIRA")
    private String documentoDgira;
    @Size(max = 300)
    @Column(name = "DOCUMENTO_NOMBRE")
    private String documentoNombre;
    @Size(max = 1500)
    @Column(name = "DOCUMENTO_URL")
    private String documentoUrl;
    @Size(max = 1500)
    @Column(name = "DOCUMENTO_UBICACION")
    private String documentoUbicacion;
    @JoinColumn(name = "BITACORA_PROYECTO", referencedColumnName = "BITACORA_PROYECTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private EvaluacionProyecto evaluacionProyecto;

    public DocumentoRespuestaDgira() {
    }

    public DocumentoRespuestaDgira(DocumentoRespuestaDgiraPK documentoRespuestaDgiraPK) {
        this.documentoRespuestaDgiraPK = documentoRespuestaDgiraPK;
    }

    public DocumentoRespuestaDgira(String bitacoraProyecto, String claveTramite, int idTramite, short tipoDocId, String idEntidadFederativa, String documentoId) {
        this.documentoRespuestaDgiraPK = new DocumentoRespuestaDgiraPK(bitacoraProyecto, claveTramite, idTramite, tipoDocId, idEntidadFederativa, documentoId);
    }

    public DocumentoRespuestaDgiraPK getDocumentoRespuestaDgiraPK() {
        return documentoRespuestaDgiraPK;
    }

    public void setDocumentoRespuestaDgiraPK(DocumentoRespuestaDgiraPK documentoRespuestaDgiraPK) {
        this.documentoRespuestaDgiraPK = documentoRespuestaDgiraPK;
    }

    public String getDocumentoDgira() {
        return documentoDgira;
    }

    public void setDocumentoDgira(String documentoDgira) {
        this.documentoDgira = documentoDgira;
    }

    public String getDocumentoNombre() {
        return documentoNombre;
    }

    public void setDocumentoNombre(String documentoNombre) {
        this.documentoNombre = documentoNombre;
    }

    public String getDocumentoUrl() {
        return documentoUrl;
    }

    public void setDocumentoUrl(String documentoUrl) {
        this.documentoUrl = documentoUrl;
    }

    public String getDocumentoUbicacion() {
        return documentoUbicacion;
    }

    public void setDocumentoUbicacion(String documentoUbicacion) {
        this.documentoUbicacion = documentoUbicacion;
    }

    public EvaluacionProyecto getEvaluacionProyecto() {
        return evaluacionProyecto;
    }

    public void setEvaluacionProyecto(EvaluacionProyecto evaluacionProyecto) {
        this.evaluacionProyecto = evaluacionProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentoRespuestaDgiraPK != null ? documentoRespuestaDgiraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoRespuestaDgira)) {
            return false;
        }
        DocumentoRespuestaDgira other = (DocumentoRespuestaDgira) object;
        if ((this.documentoRespuestaDgiraPK == null && other.documentoRespuestaDgiraPK != null) || (this.documentoRespuestaDgiraPK != null && !this.documentoRespuestaDgiraPK.equals(other.documentoRespuestaDgiraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgira[ documentoRespuestaDgiraPK=" + documentoRespuestaDgiraPK + " ]";
    }
    
}
