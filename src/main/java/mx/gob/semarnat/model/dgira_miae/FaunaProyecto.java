/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "FAUNA_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FaunaProyecto.findAll", query = "SELECT f FROM FaunaProyecto f"),
    @NamedQuery(name = "FaunaProyecto.findByFolioProyecto", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "FaunaProyecto.findBySerialProyecto", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaProyid", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaProyectoPK.faunaProyid = :faunaProyid"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaFamilia", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaFamilia = :faunaFamilia"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaNomCient", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaNomCient = :faunaNomCient"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaNomComun", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaNomComun = :faunaNomComun"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaCatNom59", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaCatNom59 = :faunaCatNom59"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaEndemico", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaEndemico = :faunaEndemico"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaCites", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaCites = :faunaCites"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaRegistro", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaRegistro = :faunaRegistro"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaAbundancia", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaAbundancia = :faunaAbundancia"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaDensidad", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaDensidad = :faunaDensidad"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaDistribucion", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaDistribucion = :faunaDistribucion"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaNumIndvAfec", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaNumIndvAfec = :faunaNumIndvAfec"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaAnexoNombre", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaAnexoNombre = :faunaAnexoNombre"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaAnexoUrl", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaAnexoUrl = :faunaAnexoUrl"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaAnexoExtension", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaAnexoExtension = :faunaAnexoExtension"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaAnexoTamanio", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaAnexoTamanio = :faunaAnexoTamanio"),
    @NamedQuery(name = "FaunaProyecto.findByFaunaAnexoDescripcion", query = "SELECT f FROM FaunaProyecto f WHERE f.faunaAnexoDescripcion = :faunaAnexoDescripcion")})
public class FaunaProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FaunaProyectoPK faunaProyectoPK;
    @Column(name = "FAUNA_FAMILIA")
    private String faunaFamilia;
    @Column(name = "FAUNA_NOM_CIENT")
    private String faunaNomCient;
    @Column(name = "FAUNA_NOM_COMUN")
    private String faunaNomComun;
    @Column(name = "FAUNA_CAT_NOM59")
    private String faunaCatNom59;
    @Column(name = "FAUNA_ENDEMICO")
    private String faunaEndemico;
    @Column(name = "FAUNA_CITES")
    private String faunaCites;
//    @Column(name = "FAUNA_PRESENCIA")
//    private String faunaPresencia;
    @Column(name = "FAUNA_REGISTRO")
    private String faunaRegistro;
    @Column(name = "FAUNA_ABUNDANCIA")
    private String faunaAbundancia;
    @Column(name = "FAUNA_DENSIDAD")
    private String faunaDensidad;
    @Column(name = "FAUNA_DISTRIBUCION")
    private String faunaDistribucion;
    @Column(name = "FAUNA_NUM_INDV_AFEC")
    private String faunaNumIndvAfec;
    @Column(name = "FAUNA_ANEXO_NOMBRE")
    private String faunaAnexoNombre;
    @Column(name = "FAUNA_ANEXO_URL")
    private String faunaAnexoUrl;
    @Column(name = "FAUNA_ANEXO_EXTENSION")
    private String faunaAnexoExtension;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FAUNA_ANEXO_TAMANIO")
    private BigDecimal faunaAnexoTamanio;
    @Column(name = "FAUNA_ANEXO_DESCRIPCION")
    private String faunaAnexoDescripcion;

    @Column(name = "FAUNA_PRESENCIA_SA")
    private String faunaPresenciaSa;
    @Column(name = "FAUNA_PRESENCIA_AI")
    private String faunaPresenciaAi;
    @Column(name = "FAUNA_PRESENCIA_PR")
    private String faunaPresenciaPr;
    @Column(name = "FAUNA_GRUPO")
    private String faunaGrupo;

    @Transient
    private Boolean presenciaSa;
    @Transient
    private Boolean presenciaAi;
    @Transient
    private Boolean presenciaPr;

    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    
    @Column(name = "FAUNA_CLASE")
    private String faunaClase;
    
    @Column(name = "ID_FAUNA_SEQ")
    private Short idFaunaSeq;
    
    //-Se hace la unión para la tabla de archivos proyecto
    @JoinColumn(name = "ID_ARCHIVO_PROGRAMA", referencedColumnName = "SEQ_ID")
    @ManyToOne
    private ArchivosProyecto idArchivoProyecto;    

    public FaunaProyecto() {
    }

    public FaunaProyecto(FaunaProyectoPK faunaProyectoPK) {
        this.faunaProyectoPK = faunaProyectoPK;
    }

    public FaunaProyecto(String folioProyecto, short serialProyecto, int faunaProyid) {
        this.faunaProyectoPK = new FaunaProyectoPK(folioProyecto, serialProyecto, faunaProyid);
    }

    public FaunaProyectoPK getFaunaProyectoPK() {
        return faunaProyectoPK;
    }

    public void setFaunaProyectoPK(FaunaProyectoPK faunaProyectoPK) {
        this.faunaProyectoPK = faunaProyectoPK;
    }

    public String getFaunaFamilia() {
        return faunaFamilia;
    }

    public void setFaunaFamilia(String faunaFamilia) {
        this.faunaFamilia = faunaFamilia;
    }

    public String getFaunaNomCient() {
        return faunaNomCient;
    }

    public void setFaunaNomCient(String faunaNomCient) {
        this.faunaNomCient = faunaNomCient;
    }

    public String getFaunaNomComun() {
        return faunaNomComun;
    }

    public void setFaunaNomComun(String faunaNomComun) {
        this.faunaNomComun = faunaNomComun;
    }

    public String getFaunaCatNom59() {
        return faunaCatNom59;
    }

    public void setFaunaCatNom59(String faunaCatNom59) {
        this.faunaCatNom59 = faunaCatNom59;
    }

    public String getFaunaEndemico() {
        return faunaEndemico;
    }

    public void setFaunaEndemico(String faunaEndemico) {
        this.faunaEndemico = faunaEndemico;
    }

    public String getFaunaCites() {
        return faunaCites;
    }

    public void setFaunaCites(String faunaCites) {
        this.faunaCites = faunaCites;
    }

//    public String getFaunaPresencia() {
//        return faunaPresencia;
//    }
//
//    public void setFaunaPresencia(String faunaPresencia) {
//        this.faunaPresencia = faunaPresencia;
//    }
    public String getFaunaRegistro() {
        return faunaRegistro;
    }

    public void setFaunaRegistro(String faunaRegistro) {
        this.faunaRegistro = faunaRegistro;
    }

    public String getFaunaAbundancia() {
        return faunaAbundancia;
    }

    public void setFaunaAbundancia(String faunaAbundancia) {
        this.faunaAbundancia = faunaAbundancia;
    }

    public String getFaunaDensidad() {
        return faunaDensidad;
    }

    public void setFaunaDensidad(String faunaDensidad) {
        this.faunaDensidad = faunaDensidad;
    }

    public String getFaunaDistribucion() {
        return faunaDistribucion;
    }

    public void setFaunaDistribucion(String faunaDistribucion) {
        this.faunaDistribucion = faunaDistribucion;
    }

    public String getFaunaNumIndvAfec() {
        return faunaNumIndvAfec;
    }

    public void setFaunaNumIndvAfec(String faunaNumIndvAfec) {
        this.faunaNumIndvAfec = faunaNumIndvAfec;
    }

    public String getFaunaAnexoNombre() {
        return faunaAnexoNombre;
    }

    public void setFaunaAnexoNombre(String faunaAnexoNombre) {
        this.faunaAnexoNombre = faunaAnexoNombre;
    }

    public String getFaunaAnexoUrl() {
        return faunaAnexoUrl;
    }

    public void setFaunaAnexoUrl(String faunaAnexoUrl) {
        this.faunaAnexoUrl = faunaAnexoUrl;
    }

    public String getFaunaAnexoExtension() {
        return faunaAnexoExtension;
    }

    public void setFaunaAnexoExtension(String faunaAnexoExtension) {
        this.faunaAnexoExtension = faunaAnexoExtension;
    }

    public BigDecimal getFaunaAnexoTamanio() {
        return faunaAnexoTamanio;
    }

    public void setFaunaAnexoTamanio(BigDecimal faunaAnexoTamanio) {
        this.faunaAnexoTamanio = faunaAnexoTamanio;
    }

    public String getFaunaAnexoDescripcion() {
        return faunaAnexoDescripcion;
    }

    public void setFaunaAnexoDescripcion(String faunaAnexoDescripcion) {
        this.faunaAnexoDescripcion = faunaAnexoDescripcion;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (faunaProyectoPK != null ? faunaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FaunaProyecto)) {
            return false;
        }
        FaunaProyecto other = (FaunaProyecto) object;
        if ((this.faunaProyectoPK == null && other.faunaProyectoPK != null) || (this.faunaProyectoPK != null && !this.faunaProyectoPK.equals(other.faunaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.FaunaProyecto[ faunaProyectoPK=" + faunaProyectoPK + " ]";
    }

    /**
     * @return the faunaPresenciaSa
     */
    public String getFaunaPresenciaSa() {
    	if (faunaPresenciaSa != null) {
			if (faunaPresenciaSa.equals("S")) {
				return "Si";
			}
			if (faunaPresenciaSa.equals("N")) {
				return "No";
			}
		}
        return faunaPresenciaSa;
    }

    /**
     * @param faunaPresenciaSa the faunaPresenciaSa to set
     */
    public void setFaunaPresenciaSa(String faunaPresenciaSa) {
        this.faunaPresenciaSa = faunaPresenciaSa;
    }

    /**
     * @return the faunaPresenciaAi
     */
    public String getFaunaPresenciaAi() {
    	if (faunaPresenciaAi != null) {
			if (faunaPresenciaAi.equals("S")) {
				return "Si";
			}
			if (faunaPresenciaAi.equals("N")) {
				return "No";
			}
		}
        return faunaPresenciaAi;
    }

    /**
     * @param faunaPresenciaAi the faunaPresenciaAi to set
     */
    public void setFaunaPresenciaAi(String faunaPresenciaAi) {
        this.faunaPresenciaAi = faunaPresenciaAi;
    }

    /**
     * @return the faunaPresenciaPr
     */
    public String getFaunaPresenciaPr() {
    	if (faunaPresenciaPr != null) {
			if (faunaPresenciaPr.equals("S")) {
				return "Si";
			}
			if (faunaPresenciaPr.equals("N")) {
				return "No";
			}
		}
        return faunaPresenciaPr;
    }

    /**
     * @param faunaPresenciaPr the faunaPresenciaPr to set
     */
    public void setFaunaPresenciaPr(String faunaPresenciaPr) {
        this.faunaPresenciaPr = faunaPresenciaPr;
    }

    /**
     * @return the faunaGrupo
     */
    public String getFaunaGrupo() {
        return faunaGrupo;
    }

    /**
     * @param faunaGrupo the faunaGrupo to set
     */
    public void setFaunaGrupo(String faunaGrupo) {
        this.faunaGrupo = faunaGrupo;
    }

    /**
     * @return the presenciaSa
     */
    public Boolean getPresenciaSa() {
        presenciaSa = (faunaPresenciaSa != null && faunaPresenciaSa.equals('S'));
        return presenciaSa;
    }

    /**
     * @return the presenciaAi
     */
    public Boolean getPresenciaAi() {
        presenciaAi = (faunaPresenciaAi != null && faunaPresenciaAi.equals('S'));
        return presenciaAi;
    }

    /**
     * @return the presenciaPr
     */
    public Boolean getPresenciaPr() {
        presenciaPr = (faunaPresenciaPr != null && faunaPresenciaPr.equals('S'));
        return presenciaPr;
    }

	/**
	 * @return the faunaClase
	 */
	public String getFaunaClase() {
		return faunaClase;
	}

	/**
	 * @param faunaClase the faunaClase to set
	 */
	public void setFaunaClase(String faunaClase) {
		this.faunaClase = faunaClase;
	}

	/**
	 * @return the idFaunaSeq
	 */
	public Short getIdFaunaSeq() {
		return idFaunaSeq;
	}

	/**
	 * @param idFaunaSeq the idFaunaSeq to set
	 */
	public void setIdFaunaSeq(Short idFaunaSeq) {
		this.idFaunaSeq = idFaunaSeq;
	}

	/**
	 * @return the idArchivoProyecto
	 */
	public ArchivosProyecto getIdArchivoProyecto() {
		return idArchivoProyecto;
	}

	/**
	 * @param idArchivoProyecto the idArchivoProyecto to set
	 */
	public void setIdArchivoProyecto(ArchivosProyecto idArchivoProyecto) {
		this.idArchivoProyecto = idArchivoProyecto;
	}

}

