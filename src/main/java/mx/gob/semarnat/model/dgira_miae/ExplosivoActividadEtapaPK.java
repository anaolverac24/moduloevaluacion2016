/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Paul Montoya
 */
@SuppressWarnings("serial")
@Embeddable
public class ExplosivoActividadEtapaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "EXPLOSIVO_ETAPA_ID")
    private short explosivoEtapaId;
    @Basic(optional = false)
    @Column(name = "ETAPA_ID")
    private short etapaId;
    @Basic(optional = false)
    @Column(name = "ACTIVIDAD_ETAPA_ID")
    private short actividadEtapaId;

    public ExplosivoActividadEtapaPK() {
    }

    public ExplosivoActividadEtapaPK(String folioProyecto, short serialProyecto, short explosivoEtapaId, short etapaId, short actividadEtapaId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.explosivoEtapaId = explosivoEtapaId;
        this.etapaId = etapaId;
        this.actividadEtapaId = actividadEtapaId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getExplosivoEtapaId() {
        return explosivoEtapaId;
    }

    public void setExplosivoEtapaId(short explosivoEtapaId) {
        this.explosivoEtapaId = explosivoEtapaId;
    }

    public short getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(short etapaId) {
        this.etapaId = etapaId;
    }

    public short getActividadEtapaId() {
        return actividadEtapaId;
    }

    public void setActividadEtapaId(short actividadEtapaId) {
        this.actividadEtapaId = actividadEtapaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) explosivoEtapaId;
        hash += (int) etapaId;
        hash += (int) actividadEtapaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExplosivoActividadEtapaPK)) {
            return false;
        }
        ExplosivoActividadEtapaPK other = (ExplosivoActividadEtapaPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.explosivoEtapaId != other.explosivoEtapaId) {
            return false;
        }
        if (this.etapaId != other.etapaId) {
            return false;
        }
        if (this.actividadEtapaId != other.actividadEtapaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ExplosivoActividadEtapaPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", explosivoEtapaId=" + explosivoEtapaId + ", etapaId=" + etapaId + ", actividadEtapaId=" + actividadEtapaId + " ]";
    }
    
}
