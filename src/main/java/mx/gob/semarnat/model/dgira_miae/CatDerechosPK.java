/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Rengerden
 */
@Embeddable
public class CatDerechosPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_NIVEL")
    private Character idNivel;
    @Basic(optional = false)
    @Column(name = "ID_ESTUDIO")
    private String idEstudio;

    public CatDerechosPK() {
    }

    public CatDerechosPK(Character idNivel, String idEstudio) {
        this.idNivel = idNivel;
        this.idEstudio = idEstudio;
    }

    public Character getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Character idNivel) {
        this.idNivel = idNivel;
    }

    public String getIdEstudio() {
        return idEstudio;
    }

    public void setIdEstudio(String idEstudio) {
        this.idEstudio = idEstudio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        hash += (idEstudio != null ? idEstudio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatDerechosPK)) {
            return false;
        }
        CatDerechosPK other = (CatDerechosPK) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        if ((this.idEstudio == null && other.idEstudio != null) || (this.idEstudio != null && !this.idEstudio.equals(other.idEstudio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatDerechosPK[ idNivel=" + idNivel + ", idEstudio=" + idEstudio + " ]";
    }
    
}
