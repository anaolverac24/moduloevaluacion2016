/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_POET", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPoet.findAll", query = "SELECT c FROM CatPoet c"),
    @NamedQuery(name = "CatPoet.findByPoetId", query = "SELECT c FROM CatPoet c WHERE c.poetId = :poetId"),
    @NamedQuery(name = "CatPoet.findByPoetDescripcion", query = "SELECT c FROM CatPoet c WHERE c.poetDescripcion = :poetDescripcion"),
    @NamedQuery(name = "CatPoet.findByPoetFecha", query = "SELECT c FROM CatPoet c WHERE c.poetFecha = :poetFecha"),
    @NamedQuery(name = "CatPoet.findByPoetOficio", query = "SELECT c FROM CatPoet c WHERE c.poetOficio = :poetOficio")})
public class CatPoet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "POET_ID")
    private Short poetId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "POET_DESCRIPCION")
    private String poetDescripcion;
    @Column(name = "POET_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date poetFecha;
    @Size(max = 40)
    @Column(name = "POET_OFICIO")
    private String poetOficio;

    public CatPoet() {
    }

    public CatPoet(Short poetId) {
        this.poetId = poetId;
    }

    public CatPoet(Short poetId, String poetDescripcion) {
        this.poetId = poetId;
        this.poetDescripcion = poetDescripcion;
    }

    public Short getPoetId() {
        return poetId;
    }

    public void setPoetId(Short poetId) {
        this.poetId = poetId;
    }

    public String getPoetDescripcion() {
        return poetDescripcion;
    }

    public void setPoetDescripcion(String poetDescripcion) {
        this.poetDescripcion = poetDescripcion;
    }

    public Date getPoetFecha() {
        return poetFecha;
    }

    public void setPoetFecha(Date poetFecha) {
        this.poetFecha = poetFecha;
    }

    public String getPoetOficio() {
        return poetOficio;
    }

    public void setPoetOficio(String poetOficio) {
        this.poetOficio = poetOficio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poetId != null ? poetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPoet)) {
            return false;
        }
        CatPoet other = (CatPoet) object;
        if ((this.poetId == null && other.poetId != null) || (this.poetId != null && !this.poetId.equals(other.poetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatPoet[ poetId=" + poetId + " ]";
    }
    
}
