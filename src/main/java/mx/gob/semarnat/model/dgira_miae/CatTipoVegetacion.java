/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_TIPO_VEGETACION", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoVegetacion.findAll", query = "SELECT c FROM CatTipoVegetacion c"),
    @NamedQuery(name = "CatTipoVegetacion.findByVegetacionId", query = "SELECT c FROM CatTipoVegetacion c WHERE c.vegetacionId = :vegetacionId"),
    @NamedQuery(name = "CatTipoVegetacion.findByVegetacionDescripcion", query = "SELECT c FROM CatTipoVegetacion c WHERE c.vegetacionDescripcion = :vegetacionDescripcion")})
public class CatTipoVegetacion implements Serializable {
    @ManyToMany(mappedBy = "catTipoVegetacionList")
    private List<PreguntaProyecto> preguntaProyectoList;
    @OneToMany(mappedBy = "vegetacionId")
    private List<PreguntaProyecto> preguntaProyectoList1;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "VEGETACION_ID")
    private Short vegetacionId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "VEGETACION_DESCRIPCION")
    private String vegetacionDescripcion;

    public CatTipoVegetacion() {
    }

    public CatTipoVegetacion(Short vegetacionId) {
        this.vegetacionId = vegetacionId;
    }

    public CatTipoVegetacion(Short vegetacionId, String vegetacionDescripcion) {
        this.vegetacionId = vegetacionId;
        this.vegetacionDescripcion = vegetacionDescripcion;
    }

    public Short getVegetacionId() {
        return vegetacionId;
    }

    public void setVegetacionId(Short vegetacionId) {
        this.vegetacionId = vegetacionId;
    }

    public String getVegetacionDescripcion() {
        return vegetacionDescripcion;
    }

    public void setVegetacionDescripcion(String vegetacionDescripcion) {
        this.vegetacionDescripcion = vegetacionDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vegetacionId != null ? vegetacionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoVegetacion)) {
            return false;
        }
        CatTipoVegetacion other = (CatTipoVegetacion) object;
        if ((this.vegetacionId == null && other.vegetacionId != null) || (this.vegetacionId != null && !this.vegetacionId.equals(other.vegetacionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatTipoVegetacion[ vegetacionId=" + vegetacionId + " ]";
    }

    @XmlTransient
    public List<PreguntaProyecto> getPreguntaProyectoList() {
        return preguntaProyectoList;
    }

    public void setPreguntaProyectoList(List<PreguntaProyecto> preguntaProyectoList) {
        this.preguntaProyectoList = preguntaProyectoList;
    }

    @XmlTransient
    public List<PreguntaProyecto> getPreguntaProyectoList1() {
        return preguntaProyectoList1;
    }

    public void setPreguntaProyectoList1(List<PreguntaProyecto> preguntaProyectoList1) {
        this.preguntaProyectoList1 = preguntaProyectoList1;
    }
    
}
