/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "ACUIFEROS", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acuiferos.findAll", query = "SELECT a FROM Acuiferos a"),
    @NamedQuery(name = "Acuiferos.findByNumFolio", query = "SELECT a FROM Acuiferos a WHERE a.acuiferosPK.numFolio = :numFolio"),
    @NamedQuery(name = "Acuiferos.findByCveProy", query = "SELECT a FROM Acuiferos a WHERE a.acuiferosPK.cveProy = :cveProy"),
    @NamedQuery(name = "Acuiferos.findByCveArea", query = "SELECT a FROM Acuiferos a WHERE a.acuiferosPK.cveArea = :cveArea"),
    @NamedQuery(name = "Acuiferos.findByVersion", query = "SELECT a FROM Acuiferos a WHERE a.acuiferosPK.version = :version"),
    @NamedQuery(name = "Acuiferos.findByClvAcui", query = "SELECT a FROM Acuiferos a WHERE a.clvAcui = :clvAcui"),
    @NamedQuery(name = "Acuiferos.findByNomAcui", query = "SELECT a FROM Acuiferos a WHERE a.nomAcui = :nomAcui"),
    @NamedQuery(name = "Acuiferos.findByDescDispo", query = "SELECT a FROM Acuiferos a WHERE a.descDispo = :descDispo"),
    @NamedQuery(name = "Acuiferos.findByFechaDof", query = "SELECT a FROM Acuiferos a WHERE a.fechaDof = :fechaDof"),
    @NamedQuery(name = "Acuiferos.findBySobreexp", query = "SELECT a FROM Acuiferos a WHERE a.sobreexp = :sobreexp"),
    @NamedQuery(name = "Acuiferos.findBySupEa", query = "SELECT a FROM Acuiferos a WHERE a.supEa = :supEa"),
    @NamedQuery(name = "Acuiferos.findByProy", query = "SELECT a FROM Acuiferos a WHERE a.proy = :proy"),
    @NamedQuery(name = "Acuiferos.findByComp", query = "SELECT a FROM Acuiferos a WHERE a.comp = :comp"),
    @NamedQuery(name = "Acuiferos.findByDescrip", query = "SELECT a FROM Acuiferos a WHERE a.descrip = :descrip"),
    @NamedQuery(name = "Acuiferos.findByAreabuffer", query = "SELECT a FROM Acuiferos a WHERE a.areabuffer = :areabuffer"),
    @NamedQuery(name = "Acuiferos.findByArea", query = "SELECT a FROM Acuiferos a WHERE a.area = :area"),
    @NamedQuery(name = "Acuiferos.findByFechaHora", query = "SELECT a FROM Acuiferos a WHERE a.fechaHora = :fechaHora"),
    @NamedQuery(name = "Acuiferos.findByIdr", query = "SELECT a FROM Acuiferos a WHERE a.acuiferosPK.idr = :idr")})
public class Acuiferos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AcuiferosPK acuiferosPK;
    @Size(max = 80)
    @Column(name = "CLV_ACUI")
    private String clvAcui;
    @Size(max = 80)
    @Column(name = "NOM_ACUI")
    private String nomAcui;
    @Size(max = 80)
    @Column(name = "DESC_DISPO")
    private String descDispo;
    @Column(name = "FECHA_DOF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDof;
    @Size(max = 80)
    @Column(name = "SOBREEXP")
    private String sobreexp;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Size(max = 80)
    @Column(name = "PROY")
    private String proy;
    @Size(max = 80)
    @Column(name = "COMP")
    private String comp;
    @Size(max = 80)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public Acuiferos() {
    }

    public Acuiferos(AcuiferosPK acuiferosPK) {
        this.acuiferosPK = acuiferosPK;
    }

    public Acuiferos(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.acuiferosPK = new AcuiferosPK(numFolio, cveProy, cveArea, version, idr);
    }

    public AcuiferosPK getAcuiferosPK() {
        return acuiferosPK;
    }

    public void setAcuiferosPK(AcuiferosPK acuiferosPK) {
        this.acuiferosPK = acuiferosPK;
    }

    public String getClvAcui() {
        return clvAcui;
    }

    public void setClvAcui(String clvAcui) {
        this.clvAcui = clvAcui;
    }

    public String getNomAcui() {
        return nomAcui;
    }

    public void setNomAcui(String nomAcui) {
        this.nomAcui = nomAcui;
    }

    public String getDescDispo() {
        return descDispo;
    }

    public void setDescDispo(String descDispo) {
        this.descDispo = descDispo;
    }

    public Date getFechaDof() {
        return fechaDof;
    }

    public void setFechaDof(Date fechaDof) {
        this.fechaDof = fechaDof;
    }

    public String getSobreexp() {
        return sobreexp;
    }

    public void setSobreexp(String sobreexp) {
        this.sobreexp = sobreexp;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (acuiferosPK != null ? acuiferosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acuiferos)) {
            return false;
        }
        Acuiferos other = (Acuiferos) object;
        if ((this.acuiferosPK == null && other.acuiferosPK != null) || (this.acuiferosPK != null && !this.acuiferosPK.equals(other.acuiferosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.Acuiferos[ acuiferosPK=" + acuiferosPK + " ]";
    }
    
}
