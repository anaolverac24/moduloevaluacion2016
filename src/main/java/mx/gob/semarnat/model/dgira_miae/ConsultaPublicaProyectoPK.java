/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class ConsultaPublicaProyectoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "EVA_BITACORA_PROYECTO")
    private String evaBitacoraProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "CLAVE_DGIRA")
    private String claveDgira;

    public ConsultaPublicaProyectoPK() {
    }

    public ConsultaPublicaProyectoPK(String evaBitacoraProyecto, String claveDgira) {
        this.evaBitacoraProyecto = evaBitacoraProyecto;
        this.claveDgira = claveDgira;
    }

    public String getEvaBitacoraProyecto() {
        return evaBitacoraProyecto;
    }

    public void setEvaBitacoraProyecto(String evaBitacoraProyecto) {
        this.evaBitacoraProyecto = evaBitacoraProyecto;
    }

    public String getClaveDgira() {
        return claveDgira;
    }

    public void setClaveDgira(String claveDgira) {
        this.claveDgira = claveDgira;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evaBitacoraProyecto != null ? evaBitacoraProyecto.hashCode() : 0);
        hash += (claveDgira != null ? claveDgira.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultaPublicaProyectoPK)) {
            return false;
        }
        ConsultaPublicaProyectoPK other = (ConsultaPublicaProyectoPK) object;
        if ((this.evaBitacoraProyecto == null && other.evaBitacoraProyecto != null) || (this.evaBitacoraProyecto != null && !this.evaBitacoraProyecto.equals(other.evaBitacoraProyecto))) {
            return false;
        }
        if ((this.claveDgira == null && other.claveDgira != null) || (this.claveDgira != null && !this.claveDgira.equals(other.claveDgira))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.ConsultaPublicaProyectoPK[ evaBitacoraProyecto=" + evaBitacoraProyecto + ", claveDgira=" + claveDgira + " ]";
    }
    
}
