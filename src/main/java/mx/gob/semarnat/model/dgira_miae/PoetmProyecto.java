/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "POETM_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PoetmProyecto.findAll", query = "SELECT p FROM PoetmProyecto p"),
    @NamedQuery(name = "PoetmProyecto.findByFolioProyecto", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PoetmProyecto.findBySerialProyecto", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmId", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmProyectoPK.poetmId = :poetmId"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmTipo", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmTipo = :poetmTipo"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmNombreInstrumento", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmNombreInstrumento = :poetmNombreInstrumento"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmNombreUga", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmNombreUga = :poetmNombreUga"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmPoliticaAmbiental", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmPoliticaAmbiental = :poetmPoliticaAmbiental"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmUsoPredominante", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmUsoPredominante = :poetmUsoPredominante"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmCriterio", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmCriterio = :poetmCriterio"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmCompatible", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmCompatible = :poetmCompatible"),
    @NamedQuery(name = "PoetmProyecto.findByFolioSerial", query = "SELECT e FROM PoetmProyecto e  WHERE e.poetmProyectoPK.folioProyecto = :folio and e.poetmProyectoPK.serialProyecto = :serial"),
    @NamedQuery(name = "PoetmProyecto.findByPoetmVinculacion", query = "SELECT p FROM PoetmProyecto p WHERE p.poetmVinculacion = :poetmVinculacion")})
public class PoetmProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PoetmProyectoPK poetmProyectoPK;
    @Column(name = "POETM_TIPO")
    private String poetmTipo;
    @Column(name = "POETM_NOMBRE_INSTRUMENTO")
    private String poetmNombreInstrumento;
    @Column(name = "POETM_NOMBRE_UGA")
    private String poetmNombreUga;
    @Column(name = "POETM_POLITICA_AMBIENTAL")
    private String poetmPoliticaAmbiental;
    @Column(name = "POETM_USO_PREDOMINANTE")
    private String poetmUsoPredominante;
    @Column(name = "POETM_CRITERIO")
    private String poetmCriterio;
    @Column(name = "POETM_COMPATIBLE")
    private String poetmCompatible;
    @Column(name = "POETM_VINCULACION")
    private String poetmVinculacion;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public PoetmProyecto() {
    }

    public PoetmProyecto(PoetmProyectoPK poetmProyectoPK) {
        this.poetmProyectoPK = poetmProyectoPK;
    }

    public PoetmProyecto(String folioProyecto, short serialProyecto, short poetmId) {
        this.poetmProyectoPK = new PoetmProyectoPK(folioProyecto, serialProyecto, poetmId);
    }

    public PoetmProyectoPK getPoetmProyectoPK() {
        return poetmProyectoPK;
    }

    public void setPoetmProyectoPK(PoetmProyectoPK poetmProyectoPK) {
        this.poetmProyectoPK = poetmProyectoPK;
    }

    public String getPoetmTipo() {
        return poetmTipo;
    }

    public void setPoetmTipo(String poetmTipo) {
        this.poetmTipo = poetmTipo;
    }

    public String getPoetmNombreInstrumento() {
        return poetmNombreInstrumento;
    }

    public void setPoetmNombreInstrumento(String poetmNombreInstrumento) {
        this.poetmNombreInstrumento = poetmNombreInstrumento;
    }

    public String getPoetmNombreUga() {
        return poetmNombreUga;
    }

    public void setPoetmNombreUga(String poetmNombreUga) {
        this.poetmNombreUga = poetmNombreUga;
    }

    public String getPoetmPoliticaAmbiental() {
        return poetmPoliticaAmbiental;
    }

    public void setPoetmPoliticaAmbiental(String poetmPoliticaAmbiental) {
        this.poetmPoliticaAmbiental = poetmPoliticaAmbiental;
    }

    public String getPoetmUsoPredominante() {
        return poetmUsoPredominante;
    }

    public void setPoetmUsoPredominante(String poetmUsoPredominante) {
        this.poetmUsoPredominante = poetmUsoPredominante;
    }

    public String getPoetmCriterio() {
        return poetmCriterio;
    }

    public void setPoetmCriterio(String poetmCriterio) {
        this.poetmCriterio = poetmCriterio;
    }

    public String getPoetmCompatible() {
        return poetmCompatible;
    }

    public void setPoetmCompatible(String poetmCompatible) {
        this.poetmCompatible = poetmCompatible;
    }

    //==========================================================
    
    public String getPoetmVinculacion() {
        return poetmVinculacion;
    }

    public void setPoetmVinculacion(String poetmVinculacion) {
        this.poetmVinculacion = poetmVinculacion;
    }
    
    public String getPoetmVinculacionCorta(){
    	if (poetmVinculacion != null) {
            if(poetmVinculacion.length()<20){
                return poetmVinculacion;
            }
            return poetmVinculacion.substring(0, 20);			
		} else {
			return poetmVinculacion;
		}
    	
    }
    
    //===================================================

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poetmProyectoPK != null ? poetmProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PoetmProyecto)) {
            return false;
        }
        PoetmProyecto other = (PoetmProyecto) object;
        if ((this.poetmProyectoPK == null && other.poetmProyectoPK != null) || (this.poetmProyectoPK != null && !this.poetmProyectoPK.equals(other.poetmProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.PoetmProyecto[ poetmProyectoPK=" + poetmProyectoPK + " ]";
    }
    
}
