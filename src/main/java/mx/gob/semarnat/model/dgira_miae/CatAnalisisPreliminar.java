/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "CAT_ANALISIS_PRELIMINAR", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatAnalisisPreliminar.findAll", query = "SELECT c FROM CatAnalisisPreliminar c"),
    @NamedQuery(name = "CatAnalisisPreliminar.findByCatAnalisisId", query = "SELECT c FROM CatAnalisisPreliminar c WHERE c.catAnalisisId = :catAnalisisId"),
    @NamedQuery(name = "CatAnalisisPreliminar.findByCatAnalisisDescripcion", query = "SELECT c FROM CatAnalisisPreliminar c WHERE c.catAnalisisDescripcion = :catAnalisisDescripcion")})
public class CatAnalisisPreliminar implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catAnalisisPreliminar")
    private Collection<AnalisisPreliminarProyecto> analisisPreliminarProyectoCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CAT_ANALISIS_ID")
    private Short catAnalisisId;
    @Basic(optional = false)
    @Column(name = "CAT_ANALISIS_DESCRIPCION")
    private String catAnalisisDescripcion;
    @JoinColumn(name = "CAT_CATEGORIA_ID", referencedColumnName = "CAT_CATEGORIA_ID")
    @ManyToOne(optional = false)
    private CatCategoriaAnalisis catCategoriaId;

    public CatAnalisisPreliminar() {
    }

    public CatAnalisisPreliminar(Short catAnalisisId) {
        this.catAnalisisId = catAnalisisId;
    }

    public CatAnalisisPreliminar(Short catAnalisisId, String catAnalisisDescripcion) {
        this.catAnalisisId = catAnalisisId;
        this.catAnalisisDescripcion = catAnalisisDescripcion;
    }

    public Short getCatAnalisisId() {
        return catAnalisisId;
    }

    public void setCatAnalisisId(Short catAnalisisId) {
        this.catAnalisisId = catAnalisisId;
    }

    public String getCatAnalisisDescripcion() {
        return catAnalisisDescripcion;
    }

    public void setCatAnalisisDescripcion(String catAnalisisDescripcion) {
        this.catAnalisisDescripcion = catAnalisisDescripcion;
    }

    public CatCategoriaAnalisis getCatCategoriaId() {
        return catCategoriaId;
    }

    public void setCatCategoriaId(CatCategoriaAnalisis catCategoriaId) {
        this.catCategoriaId = catCategoriaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catAnalisisId != null ? catAnalisisId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatAnalisisPreliminar)) {
            return false;
        }
        CatAnalisisPreliminar other = (CatAnalisisPreliminar) object;
        if ((this.catAnalisisId == null && other.catAnalisisId != null) || (this.catAnalisisId != null && !this.catAnalisisId.equals(other.catAnalisisId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatAnalisisPreliminar[ catAnalisisId=" + catAnalisisId + " ]";
    }

    @XmlTransient
    public Collection<AnalisisPreliminarProyecto> getAnalisisPreliminarProyectoCollection() {
        return analisisPreliminarProyectoCollection;
    }

    public void setAnalisisPreliminarProyectoCollection(Collection<AnalisisPreliminarProyecto> analisisPreliminarProyectoCollection) {
        this.analisisPreliminarProyectoCollection = analisisPreliminarProyectoCollection;
    }
    
}
