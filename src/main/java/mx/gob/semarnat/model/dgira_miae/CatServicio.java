/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author EdgarSC
 */
@Entity
@Table(name = "CAT_SERVICIO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatServicio.findAll", query = "SELECT c FROM CatServicio c"),
    @NamedQuery(name = "CatServicio.findByServicioId", query = "SELECT c FROM CatServicio c WHERE c.servicioId = :servicioId"),
    @NamedQuery(name = "CatServicio.findByServicioNombre", query = "SELECT c FROM CatServicio c WHERE c.servicioNombre = :servicioNombre")})
public class CatServicio implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "servicioId")
    private List<ServicioProyecto> servicioProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SERVICIO_ID")
    private Short servicioId;
    @Basic(optional = false)
    @Column(name = "SERVICIO_NOMBRE")
    private String servicioNombre;

    public CatServicio() {
    }

    public CatServicio(Short servicioId) {
        this.servicioId = servicioId;
    }

    public CatServicio(Short servicioId, String servicioNombre) {
        this.servicioId = servicioId;
        this.servicioNombre = servicioNombre;
    }

    public Short getServicioId() {
        return servicioId;
    }

    public void setServicioId(Short servicioId) {
        this.servicioId = servicioId;
    }

    public String getServicioNombre() {
        return servicioNombre;
    }

    public void setServicioNombre(String servicioNombre) {
        this.servicioNombre = servicioNombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (servicioId != null ? servicioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatServicio)) {
            return false;
        }
        CatServicio other = (CatServicio) object;
        if ((this.servicioId == null && other.servicioId != null) || (this.servicioId != null && !this.servicioId.equals(other.servicioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatServicio[ servicioId=" + servicioId + " ]";
    }

    @XmlTransient
    public List<ServicioProyecto> getServicioProyectoList() {
        return servicioProyectoList;
    }

    public void setServicioProyectoList(List<ServicioProyecto> servicioProyectoList) {
        this.servicioProyectoList = servicioProyectoList;
    }
    
}
