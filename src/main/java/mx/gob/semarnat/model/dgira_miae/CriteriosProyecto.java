/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.print.DocFlavor.STRING;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.mchange.v2.async.StrandedTaskReporting;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "CRITERIOS_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CriteriosProyecto.findAll", query = "SELECT c FROM CriteriosProyecto c"),
    @NamedQuery(name = "CriteriosProyecto.findByFolioProyecto", query = "SELECT c FROM CriteriosProyecto c WHERE c.criteriosProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "CriteriosProyecto.findBySerialProyecto", query = "SELECT c FROM CriteriosProyecto c WHERE c.criteriosProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "CriteriosProyecto.findByCriterioProyId", query = "SELECT c FROM CriteriosProyecto c WHERE c.criteriosProyectoPK.criterioProyId = :criterioProyId"),
    @NamedQuery(name = "CriteriosProyecto.findByCriterioDescripcion", query = "SELECT c FROM CriteriosProyecto c WHERE c.criterioDescripcion = :criterioDescripcion")})
public class CriteriosProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CriteriosProyectoPK criteriosProyectoPK;
    @Size(max = 1000)
    @Column(name = "CRITERIO_DESCRIPCION")
    private String criterioDescripcion;
    @JoinColumn(name = "CRITERIO_ID", referencedColumnName = "CRITERIO_ID")
    @ManyToOne(optional = false)
    private CatCriterio criterioId;

    //CAMPO FALTANTE AGREGADO DURANTE EL DESARROLLO DE SMIA
    @Column(name="CRITERIO")
    private String criterio;
    
    public CriteriosProyecto() {
    }

    public CriteriosProyecto(CriteriosProyectoPK criteriosProyectoPK) {
        this.criteriosProyectoPK = criteriosProyectoPK;
    }

    public CriteriosProyecto(String folioProyecto, short serialProyecto, short criterioProyId) {
        this.criteriosProyectoPK = new CriteriosProyectoPK(folioProyecto, serialProyecto, criterioProyId);
    }

    public CriteriosProyectoPK getCriteriosProyectoPK() {
        return criteriosProyectoPK;
    }

    public void setCriteriosProyectoPK(CriteriosProyectoPK criteriosProyectoPK) {
        this.criteriosProyectoPK = criteriosProyectoPK;
    }

    public String getCriterioDescripcion() {
        return criterioDescripcion;
    }

    public void setCriterioDescripcion(String criterioDescripcion) {
        this.criterioDescripcion = criterioDescripcion;
    }

    public CatCriterio getCriterioId() {
        return criterioId;
    }

    public void setCriterioId(CatCriterio criterioId) {
        this.criterioId = criterioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criteriosProyectoPK != null ? criteriosProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CriteriosProyecto)) {
            return false;
        }
        CriteriosProyecto other = (CriteriosProyecto) object;
        if ((this.criteriosProyectoPK == null && other.criteriosProyectoPK != null) || (this.criteriosProyectoPK != null && !this.criteriosProyectoPK.equals(other.criteriosProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CriteriosProyecto[ criteriosProyectoPK=" + criteriosProyectoPK + " ]";
    }    
    //GETTER AND SETTER DE LA COLUMNA FALTANTE

	public String getCriterio() {
		return criterio;
	}

	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}
    
    
    
    
    
}
