/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "PROYMPUN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proympun.findAll", query = "SELECT p FROM Proympun p"),
    @NamedQuery(name = "Proympun.findByObjectid", query = "SELECT p FROM Proympun p WHERE p.objectid = :objectid"),
    @NamedQuery(name = "Proympun.findByProy", query = "SELECT p FROM Proympun p WHERE p.proy = :proy"),
    @NamedQuery(name = "Proympun.findByComp", query = "SELECT p FROM Proympun p WHERE p.comp = :comp"),
    @NamedQuery(name = "Proympun.findByDescrip", query = "SELECT p FROM Proympun p WHERE p.descrip = :descrip"),
    @NamedQuery(name = "Proympun.findByResolucion", query = "SELECT p FROM Proympun p WHERE p.resolucion = :resolucion"),
    @NamedQuery(name = "Proympun.findByIdRes", query = "SELECT p FROM Proympun p WHERE p.idRes = :idRes"),
    @NamedQuery(name = "Proympun.findById", query = "SELECT p FROM Proympun p WHERE p.id = :id"),
    @NamedQuery(name = "Proympun.findByRowlayer", query = "SELECT p FROM Proympun p WHERE p.rowlayer = :rowlayer"),
    @NamedQuery(name = "Proympun.findByObjectid1", query = "SELECT p FROM Proympun p WHERE p.objectid1 = :objectid1"),
    @NamedQuery(name = "Proympun.findByObjectid2", query = "SELECT p FROM Proympun p WHERE p.objectid2 = :objectid2"),
    @NamedQuery(name = "Proympun.findByShape", query = "SELECT p FROM Proympun p WHERE p.shape = :shape")})
public class Proympun implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OBJECTID")
    private BigInteger objectid;
    @Size(max = 13)
    @Column(name = "PROY")
    private String proy;
    @Size(max = 254)
    @Column(name = "COMP")
    private String comp;
    @Size(max = 254)
    @Column(name = "DESCRIP")
    private String descrip;
    @Size(max = 254)
    @Column(name = "RESOLUCION")
    private String resolucion;
    @Column(name = "ID_RES")
    private Integer idRes;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "ROWLAYER")
    private Long rowlayer;
    @Column(name = "OBJECTID_1")
    private Long objectid1;
    @Column(name = "OBJECTID_2")
    private Long objectid2;
    @Column(name = "SHAPE")
    private BigInteger shape;

    public Proympun() {
    }

    public Proympun(Long id) {
        this.id = id;
    }

    public Proympun(Long id, BigInteger objectid) {
        this.id = id;
        this.objectid = objectid;
    }

    public BigInteger getObjectid() {
        return objectid;
    }

    public void setObjectid(BigInteger objectid) {
        this.objectid = objectid;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public Integer getIdRes() {
        return idRes;
    }

    public void setIdRes(Integer idRes) {
        this.idRes = idRes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRowlayer() {
        return rowlayer;
    }

    public void setRowlayer(Long rowlayer) {
        this.rowlayer = rowlayer;
    }

    public Long getObjectid1() {
        return objectid1;
    }

    public void setObjectid1(Long objectid1) {
        this.objectid1 = objectid1;
    }

    public Long getObjectid2() {
        return objectid2;
    }

    public void setObjectid2(Long objectid2) {
        this.objectid2 = objectid2;
    }

    public BigInteger getShape() {
        return shape;
    }

    public void setShape(BigInteger shape) {
        this.shape = shape;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proympun)) {
            return false;
        }
        Proympun other = (Proympun) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.Proympun[ id=" + id + " ]";
    }
    
}
