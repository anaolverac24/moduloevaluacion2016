/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CRITERIO_VALORES_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CriterioValoresProyecto.findAll", query = "SELECT c FROM CriterioValoresProyecto c"),    
    @NamedQuery(name = "CriterioValoresProyecto.findByFolioProyecto", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "CriterioValoresProyecto.findBySerialProyecto", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "CriterioValoresProyecto.findByBitacoraProyecto", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "CriterioValoresProyecto.findByBitacoraProyecto2", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.folioProyecto = :folioProyecto and  c.criterioValoresProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "CriterioValoresProyecto.findByIdCriterio", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.idCriterio = :idCriterio"),
    @NamedQuery(name = "CriterioValoresProyecto.findByIdValor", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.idValor = :idValor"),
    @NamedQuery(name = "CriterioValoresProyecto.findByCriterioValorUsuario", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValorUsuario = :criterioValorUsuario"),
    @NamedQuery(name = "CriterioValoresProyecto.findByIdCriterioANDBitacora", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.idCriterio = :idCriterio AND c.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "CriterioValoresProyecto.findByIdCriterioANDFolioSerial", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.idCriterio = :idCriterio AND c.criterioValoresProyectoPK.folioProyecto = :folioProyecto and  c.criterioValoresProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "CriterioValoresProyecto.findByCriterioValorEvaluacion", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValorEvaluacion = :criterioValorEvaluacion"),
    @NamedQuery(name = "CriterioValoresProyecto.findByCriterioObservacionEva", query = "SELECT c FROM CriterioValoresProyecto c WHERE c.criterioObservacionEva = :criterioObservacionEva")})
public class CriterioValoresProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CriterioValoresProyectoPK criterioValoresProyectoPK;
    @Size(max = 16)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Column(name = "CRITERIO_VALOR_USUARIO")
    private BigInteger criterioValorUsuario;
    @Column(name = "CRITERIO_VALOR_EVALUACION")
    private BigInteger criterioValorEvaluacion;
    @Size(max = 4000)
    @Column(name = "CRITERIO_OBSERVACION_EVA")
    private String criterioObservacionEva;
    @JoinColumns({
        @JoinColumn(name = "ID_CRITERIO", referencedColumnName = "ID_CRITERIO", insertable = false, updatable = false),
        @JoinColumn(name = "ID_VALOR", referencedColumnName = "ID_VALOR", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CatDerechosCriteriosvalores catDerechosCriteriosvalores;
    
    public CriterioValoresProyecto() {
    }

    public CriterioValoresProyecto(CriterioValoresProyectoPK criterioValoresProyectoPK) {
        this.criterioValoresProyectoPK = criterioValoresProyectoPK;
    }

    public CriterioValoresProyecto(String folioProyecto, short serialProyecto, BigInteger idCriterio, BigInteger idValor) {
        this.criterioValoresProyectoPK = new CriterioValoresProyectoPK(folioProyecto, serialProyecto, idCriterio, idValor);
    }

    public CriterioValoresProyectoPK getCriterioValoresProyectoPK() {
        return criterioValoresProyectoPK;
    }

    public void setCriterioValoresProyectoPK(CriterioValoresProyectoPK criterioValoresProyectoPK) {
        this.criterioValoresProyectoPK = criterioValoresProyectoPK;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public BigInteger getCriterioValorUsuario() {
        return criterioValorUsuario;
    }

    public void setCriterioValorUsuario(BigInteger criterioValorUsuario) {
        this.criterioValorUsuario = criterioValorUsuario;
    }

    public BigInteger getCriterioValorEvaluacion() {
        return criterioValorEvaluacion;
    }

    public void setCriterioValorEvaluacion(BigInteger criterioValorEvaluacion) {
        this.criterioValorEvaluacion = criterioValorEvaluacion;
    }

    public String getCriterioObservacionEva() {
        return criterioObservacionEva;
    }

    public void setCriterioObservacionEva(String criterioObservacionEva) {
        this.criterioObservacionEva = criterioObservacionEva;
    }

    public CatDerechosCriteriosvalores getCatDerechosCriteriosvalores() {
        return catDerechosCriteriosvalores;
    }

    public void setCatDerechosCriteriosvalores(CatDerechosCriteriosvalores catDerechosCriteriosvalores) {
        this.catDerechosCriteriosvalores = catDerechosCriteriosvalores;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criterioValoresProyectoPK != null ? criterioValoresProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CriterioValoresProyecto)) {
            return false;
        }
        CriterioValoresProyecto other = (CriterioValoresProyecto) object;
        if ((this.criterioValoresProyectoPK == null && other.criterioValoresProyectoPK != null) || (this.criterioValoresProyectoPK != null && !this.criterioValoresProyectoPK.equals(other.criterioValoresProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CriterioValoresProyecto[ criterioValoresProyectoPK=" + criterioValoresProyectoPK + " ]";
    }

}
