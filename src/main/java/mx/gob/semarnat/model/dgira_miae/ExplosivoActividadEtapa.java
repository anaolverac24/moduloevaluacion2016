/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Montoya
 */
@Entity
@Table(name = "EXPLOSIVO_ACTIVIDAD_ETAPA", schema ="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExplosivoActividadEtapa.findAll", query = "SELECT e FROM ExplosivoActividadEtapa e"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findByFolioProyecto", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findBySerialProyecto", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findByFolioAndSerialProyecto", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.folioProyecto = :folioProyecto and e.explosivoActividadEtapaPK.serialProyecto = :serialProyecto ORDER BY e.explosivoActividadEtapaPK.explosivoEtapaId"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findByExplosivoEtapaId", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.explosivoEtapaId = :explosivoEtapaId"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findByEtapaId", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.etapaId = :etapaId"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findByActividadEtapaId", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.actividadEtapaId = :actividadEtapaId"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findByActividadJustificacion", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.actividadJustificacion = :actividadJustificacion"),
    @NamedQuery(name = "ExplosivoActividadEtapa.findByActividadMetodologia", query = "SELECT e FROM ExplosivoActividadEtapa e WHERE e.actividadMetodologia = :actividadMetodologia")})
public class ExplosivoActividadEtapa implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ExplosivoActividadEtapaPK explosivoActividadEtapaPK;
    @Column(name = "ACTIVIDAD_JUSTIFICACION")
    private String actividadJustificacion;
    @Column(name = "ACTIVIDAD_METODOLOGIA")
    private String actividadMetodologia;
    @JoinColumns({
        @JoinColumn(name = "ACTIVIDAD_ETAPA_ID", referencedColumnName = "ACTIVIDAD_ETAPA_ID", insertable = false, updatable = false),
        @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private ActividadEtapa actividadEtapa;
        
    @Column(name = "CONSECUTIVO")
    private Long consecutivo;

    public ExplosivoActividadEtapa() {
    }
    
    
    public ExplosivoActividadEtapa(ExplosivoActividadEtapaPK explosivoActividadEtapaPK) {
        this.explosivoActividadEtapaPK = explosivoActividadEtapaPK;
    }

    public ExplosivoActividadEtapa(String folioProyecto, short serialProyecto, short explosivoEtapaId, short etapaId, short actividadEtapaId) {
        this.explosivoActividadEtapaPK = new ExplosivoActividadEtapaPK(folioProyecto, serialProyecto, explosivoEtapaId, etapaId, actividadEtapaId);
    }
    
    public ExplosivoActividadEtapa(String folioProyecto, short serialProyecto, short explosivoEtapaId, short etapaId, short actividadEtapaId, String justificacion, String metodologia){
    	this.explosivoActividadEtapaPK = new ExplosivoActividadEtapaPK(folioProyecto, serialProyecto, explosivoEtapaId, etapaId, actividadEtapaId);
    	this.actividadJustificacion = justificacion;
    	this.actividadMetodologia = metodologia;
    	
    }

    public ExplosivoActividadEtapaPK getExplosivoActividadEtapaPK() {
        return explosivoActividadEtapaPK;
    }

    public void setExplosivoActividadEtapaPK(ExplosivoActividadEtapaPK explosivoActividadEtapaPK) {
        this.explosivoActividadEtapaPK = explosivoActividadEtapaPK;
    }

    public String getActividadJustificacion() {
        return actividadJustificacion;
    }
    
    public String getActividadJustificacionCorta(){
        if(actividadJustificacion.length()<199){
            return actividadJustificacion;
        }
        return actividadJustificacion.substring(0, 199);
    }

    public void setActividadJustificacion(String actividadJustificacion) {
        this.actividadJustificacion = actividadJustificacion;
    }

    public String getActividadMetodologia() {
        return actividadMetodologia;
    }
    
    public String getActividadMetodologiaCorta(){
        if(actividadMetodologia.length()<199){
            return actividadMetodologia;
        }
        return actividadMetodologia.substring(0, 199);
    }

    public void setActividadMetodologia(String actividadMetodologia) {
        this.actividadMetodologia = actividadMetodologia;
    }

    public ActividadEtapa getActividadEtapa() {
        return actividadEtapa;
    }

    public void setActividadEtapa(ActividadEtapa actividadEtapa) {
        this.actividadEtapa = actividadEtapa;
    }
    
    

    public Long getConsecutivo() {
		return consecutivo;
	}


	public void setConsecutivo(Long consecutivo) {
		this.consecutivo = consecutivo;
	}


	@Override
    public int hashCode() {
        int hash = 0;
        hash += (explosivoActividadEtapaPK != null ? explosivoActividadEtapaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExplosivoActividadEtapa)) {
            return false;
        }
        ExplosivoActividadEtapa other = (ExplosivoActividadEtapa) object;
        if ((this.explosivoActividadEtapaPK == null && other.explosivoActividadEtapaPK != null) || (this.explosivoActividadEtapaPK != null && !this.explosivoActividadEtapaPK.equals(other.explosivoActividadEtapaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ExplosivoActividadEtapa[ explosivoActividadEtapaPK=" + explosivoActividadEtapaPK + " ]";
    }
    
}
