/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "REP_LEGAL_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RepLegalProyecto.findAll", query = "SELECT r FROM RepLegalProyecto r"),
    @NamedQuery(name = "RepLegalProyecto.findByFolioProyecto", query = "SELECT r FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "RepLegalProyecto.findBySerialProyecto", query = "SELECT r FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "RepLegalProyecto.findByRfc", query = "SELECT r FROM RepLegalProyecto r WHERE r.rfc = :rfc"),
    @NamedQuery(name = "RepLegalProyecto.findByMismoEstudio", query = "SELECT r FROM RepLegalProyecto r WHERE r.mismoEstudio = :mismoEstudio"),
    @NamedQuery(name = "RepLegalProyecto.findByRepresentanteCurp", query = "SELECT r FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.representanteCurp = :representanteCurp")})
public class RepLegalProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RepLegalProyectoPK repLegalProyectoPK;
    @Basic(optional = false)
    @Column(name = "RFC")
    private String rfc;
    @Column(name = "MISMO_ESTUDIO")
    private String mismoEstudio;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public RepLegalProyecto() {
    }

    public RepLegalProyecto(RepLegalProyectoPK repLegalProyectoPK) {
        this.repLegalProyectoPK = repLegalProyectoPK;
    }

    public RepLegalProyecto(RepLegalProyectoPK repLegalProyectoPK, String rfc) {
        this.repLegalProyectoPK = repLegalProyectoPK;
        this.rfc = rfc;
    }

    public RepLegalProyecto(String folioProyecto, short serialProyecto, String representanteCurp) {
        this.repLegalProyectoPK = new RepLegalProyectoPK(folioProyecto, serialProyecto, representanteCurp);
    }

    public RepLegalProyectoPK getRepLegalProyectoPK() {
        return repLegalProyectoPK;
    }

    public void setRepLegalProyectoPK(RepLegalProyectoPK repLegalProyectoPK) {
        this.repLegalProyectoPK = repLegalProyectoPK;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getMismoEstudio() {
        return mismoEstudio;
    }

    public void setMismoEstudio(String mismoEstudio) {
        this.mismoEstudio = mismoEstudio;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (repLegalProyectoPK != null ? repLegalProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RepLegalProyecto)) {
            return false;
        }
        RepLegalProyecto other = (RepLegalProyecto) object;
        if ((this.repLegalProyectoPK == null && other.repLegalProyectoPK != null) || (this.repLegalProyectoPK != null && !this.repLegalProyectoPK.equals(other.repLegalProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.RepLegalProyecto[ repLegalProyectoPK=" + repLegalProyectoPK + " ]";
    }
    
}
