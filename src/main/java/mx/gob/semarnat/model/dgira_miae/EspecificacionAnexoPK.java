/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class EspecificacionAnexoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ESPECIFICACION_ID")
    private String especificacionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NORMA_ID")
    private short normaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESPECIFICACION_ANEXO_ID")
    private short especificacionAnexoId;

    public EspecificacionAnexoPK() {
    }

    public EspecificacionAnexoPK(String folioProyecto, short serialProyecto, String especificacionId, short normaId, short especificacionAnexoId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.especificacionId = especificacionId;
        this.normaId = normaId;
        this.especificacionAnexoId = especificacionAnexoId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public String getEspecificacionId() {
        return especificacionId;
    }

    public void setEspecificacionId(String especificacionId) {
        this.especificacionId = especificacionId;
    }

    public short getNormaId() {
        return normaId;
    }

    public void setNormaId(short normaId) {
        this.normaId = normaId;
    }

    public short getEspecificacionAnexoId() {
        return especificacionAnexoId;
    }

    public void setEspecificacionAnexoId(short especificacionAnexoId) {
        this.especificacionAnexoId = especificacionAnexoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (especificacionId != null ? especificacionId.hashCode() : 0);
        hash += (int) normaId;
        hash += (int) especificacionAnexoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EspecificacionAnexoPK)) {
            return false;
        }
        EspecificacionAnexoPK other = (EspecificacionAnexoPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if ((this.especificacionId == null && other.especificacionId != null) || (this.especificacionId != null && !this.especificacionId.equals(other.especificacionId))) {
            return false;
        }
        if (this.normaId != other.normaId) {
            return false;
        }
        if (this.especificacionAnexoId != other.especificacionAnexoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.EspecificacionAnexoPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", especificacionId=" + especificacionId + ", normaId=" + normaId + ", especificacionAnexoId=" + especificacionAnexoId + " ]";
    }
    
}
