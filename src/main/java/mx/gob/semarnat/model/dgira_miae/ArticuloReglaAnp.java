/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ARTICULO_REGLA_ANP", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArticuloReglaAnp.findAll", query = "SELECT a FROM ArticuloReglaAnp a"),
    @NamedQuery(name = "ArticuloReglaAnp.findByFolioProyecto", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.articuloReglaAnpPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ArticuloReglaAnp.findBySerialProyecto", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.articuloReglaAnpPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ArticuloReglaAnp.findBySerialSerialProyecto", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.articuloReglaAnpPK.folioProyecto = :folio AND a.articuloReglaAnpPK.serialProyecto = :serial"),
    @NamedQuery(name = "ArticuloReglaAnp.findByAnpId", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.articuloReglaAnpPK.anpId = :anpId"),
    @NamedQuery(name = "ArticuloReglaAnp.findByAnpArticuloRegla", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.anpArticuloRegla = :anpArticuloRegla"),
    @NamedQuery(name = "ArticuloReglaAnp.findByAnpArtNomRegla", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.anpArtNomRegla = :anpArtNomRegla"),
    @NamedQuery(name = "ArticuloReglaAnp.findByAnpArtRegId", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.articuloReglaAnpPK.anpArtRegId = :anpArtRegId"),
    @NamedQuery(name = "ArticuloReglaAnp.findByAnpArtNomVinculacion", query = "SELECT a FROM ArticuloReglaAnp a WHERE a.anpArtNomVinculacion = :anpArtNomVinculacion")})
public class ArticuloReglaAnp implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ArticuloReglaAnpPK articuloReglaAnpPK;
    @Column(name = "ANP_ARTICULO_REGLA")
    private String anpArticuloRegla;
    @Column(name = "ANP_ART_NOM_REGLA")
    private String anpArtNomRegla;
    @Column(name = "ANP_ART_NOM_VINCULACION")
    private String anpArtNomVinculacion;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "ANP_ID", referencedColumnName = "ANP_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private AnpProyecto anpProyecto;

    public ArticuloReglaAnp() {
    }

    public ArticuloReglaAnp(ArticuloReglaAnpPK articuloReglaAnpPK) {
        this.articuloReglaAnpPK = articuloReglaAnpPK;
    }

    public ArticuloReglaAnp(String folioProyecto, short serialProyecto, short anpId, short anpArtRegId) {
        this.articuloReglaAnpPK = new ArticuloReglaAnpPK(folioProyecto, serialProyecto, anpId, anpArtRegId);
    }

    public ArticuloReglaAnpPK getArticuloReglaAnpPK() {
        return articuloReglaAnpPK;
    }

    public void setArticuloReglaAnpPK(ArticuloReglaAnpPK articuloReglaAnpPK) {
        this.articuloReglaAnpPK = articuloReglaAnpPK;
    }

    public String getAnpArticuloRegla() {
        return anpArticuloRegla;
    }

    public void setAnpArticuloRegla(String anpArticuloRegla) {
        this.anpArticuloRegla = anpArticuloRegla;
    }

    public String getAnpArtNomRegla() {
        return anpArtNomRegla;
    }

    public void setAnpArtNomRegla(String anpArtNomRegla) {
        this.anpArtNomRegla = anpArtNomRegla;
    }

    public String getAnpArtNomVinculacion() {
        return anpArtNomVinculacion;
    }

    public void setAnpArtNomVinculacion(String anpArtNomVinculacion) {
        this.anpArtNomVinculacion = anpArtNomVinculacion;
    }

    public AnpProyecto getAnpProyecto() {
        return anpProyecto;
    }

    public void setAnpProyecto(AnpProyecto anpProyecto) {
        this.anpProyecto = anpProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (articuloReglaAnpPK != null ? articuloReglaAnpPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArticuloReglaAnp)) {
            return false;
        }
        ArticuloReglaAnp other = (ArticuloReglaAnp) object;
        if ((this.articuloReglaAnpPK == null && other.articuloReglaAnpPK != null) || (this.articuloReglaAnpPK != null && !this.articuloReglaAnpPK.equals(other.articuloReglaAnpPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ArticuloReglaAnp[ articuloReglaAnpPK=" + articuloReglaAnpPK + " ]";
    }
    
}
