/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CONVENIO_PROY", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConvenioProy.findAll", query = "SELECT c FROM ConvenioProy c"),
    @NamedQuery(name = "ConvenioProy.findByFolioProyecto", query = "SELECT c FROM ConvenioProy c WHERE c.convenioProyPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ConvenioProy.findBySerialProyecto", query = "SELECT c FROM ConvenioProy c WHERE c.convenioProyPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ConvenioProy.findByConvenioProyId", query = "SELECT c FROM ConvenioProy c WHERE c.convenioProyPK.convenioProyId = :convenioProyId"),
    @NamedQuery(name = "ConvenioProy.findByFolioSerial", query = "SELECT e FROM ConvenioProy e WHERE e.convenioProyPK.folioProyecto = :folio and e.convenioProyPK.serialProyecto = :serial"),
    @NamedQuery(name = "ConvenioProy.findByConvenioDescripcion", query = "SELECT c FROM ConvenioProy c WHERE c.convenioDescripcion = :convenioDescripcion")})
public class ConvenioProy implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConvenioProyPK convenioProyPK;
    @Lob
    @Column(name = "CONVENIO_VINCULACION")
    private String convenioVinculacion;
    @Column(name = "CONVENIO_DESCRIPCION")
    private String convenioDescripcion;
    @Column(name="CONVENIO_FECHA_PUBLICACION")
    private String convenioFechaPublicacion;
    @JoinColumn(name = "CONVENIO_ID", referencedColumnName = "CONVENIO_ID")
    @ManyToOne
    private CatConvenios convenioId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public ConvenioProy() {
    }

    public ConvenioProy(ConvenioProyPK convenioProyPK) {
        this.convenioProyPK = convenioProyPK;
    }

    public ConvenioProy(String folioProyecto, short serialProyecto, short convenioProyId) {
        this.convenioProyPK = new ConvenioProyPK(folioProyecto, serialProyecto, convenioProyId);
    }

    public ConvenioProyPK getConvenioProyPK() {
        return convenioProyPK;
    }

    public void setConvenioProyPK(ConvenioProyPK convenioProyPK) {
        this.convenioProyPK = convenioProyPK;
    }

    public String getConvenioVinculacion() {
        return convenioVinculacion;
    }

    public void setConvenioVinculacion(String convenioVinculacion) {
        this.convenioVinculacion = convenioVinculacion;
    }

    public String getConvenioDescripcion() {
        return convenioDescripcion;
    }

    public void setConvenioDescripcion(String convenioDescripcion) {
        this.convenioDescripcion = convenioDescripcion;
    }

    public CatConvenios getConvenioId() {
        return convenioId;
    }

    public void setConvenioId(CatConvenios convenioId) {
        this.convenioId = convenioId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
	 * @return the convenioFechaPublicacion
	 */
	public String getConvenioFechaPublicacion() {
		return convenioFechaPublicacion;
	}

	/**
	 * @param convenioFechaPublicacion the convenioFechaPublicacion to set
	 */
	public void setConvenioFechaPublicacion(String convenioFechaPublicacion) {
		this.convenioFechaPublicacion = convenioFechaPublicacion;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (convenioProyPK != null ? convenioProyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConvenioProy)) {
            return false;
        }
        ConvenioProy other = (ConvenioProy) object;
        if ((this.convenioProyPK == null && other.convenioProyPK != null) || (this.convenioProyPK != null && !this.convenioProyPK.equals(other.convenioProyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ConvenioProy[ convenioProyPK=" + convenioProyPK + " ]";
    }
    
}
