/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_REGLAMENTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatReglamento.findAll", query = "SELECT c FROM CatReglamento c"),
    @NamedQuery(name = "CatReglamento.findByReglamentoId", query = "SELECT c FROM CatReglamento c WHERE c.reglamentoId = :reglamentoId"),
    @NamedQuery(name = "CatReglamento.findByReglamentoDescripcion", query = "SELECT c FROM CatReglamento c WHERE c.reglamentoDescripcion = :reglamentoDescripcion"),
    @NamedQuery(name = "CatReglamento.findByReglamentoFecha", query = "SELECT c FROM CatReglamento c WHERE c.reglamentoFecha = :reglamentoFecha"),
    @NamedQuery(name = "CatReglamento.findByReglamentoFechaModificacion", query = "SELECT c FROM CatReglamento c WHERE c.reglamentoFechaModificacion = :reglamentoFechaModificacion"),
    @NamedQuery(name = "CatReglamento.findByReglamentoUrl", query = "SELECT c FROM CatReglamento c WHERE c.reglamentoUrl = :reglamentoUrl")})
public class CatReglamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REGLAMENTO_ID")
    private Short reglamentoId;
    @Column(name = "REGLAMENTO_DESCRIPCION")
    private String reglamentoDescripcion;
    @Column(name = "REGLAMENTO_FECHA")
    private String reglamentoFecha;
    @Column(name = "REGLAMENTO_FECHA_MODIFICACION")
    private String reglamentoFechaModificacion;
    @Column(name = "REGLAMENTO_URL")
    private String reglamentoUrl;
    @OneToMany(mappedBy = "reglamentoId")
    private List<ReglamentoProyecto> reglamentoProyectoList;

    public CatReglamento() {
    }

    public CatReglamento(Short reglamentoId) {
        this.reglamentoId = reglamentoId;
    }

    public Short getReglamentoId() {
        return reglamentoId;
    }

    public void setReglamentoId(Short reglamentoId) {
        this.reglamentoId = reglamentoId;
    }

    public String getReglamentoDescripcion() {
        return reglamentoDescripcion;
    }

    public void setReglamentoDescripcion(String reglamentoDescripcion) {
        this.reglamentoDescripcion = reglamentoDescripcion;
    }

    public String getReglamentoFecha() {
        return reglamentoFecha;
    }

    public void setReglamentoFecha(String reglamentoFecha) {
        this.reglamentoFecha = reglamentoFecha;
    }

    public String getReglamentoFechaModificacion() {
        return reglamentoFechaModificacion;
    }

    public void setReglamentoFechaModificacion(String reglamentoFechaModificacion) {
        this.reglamentoFechaModificacion = reglamentoFechaModificacion;
    }

    public String getReglamentoUrl() {
        return reglamentoUrl;
    }

    public void setReglamentoUrl(String reglamentoUrl) {
        this.reglamentoUrl = reglamentoUrl;
    }

    @XmlTransient
    public List<ReglamentoProyecto> getReglamentoProyectoList() {
        return reglamentoProyectoList;
    }

    public void setReglamentoProyectoList(List<ReglamentoProyecto> reglamentoProyectoList) {
        this.reglamentoProyectoList = reglamentoProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reglamentoId != null ? reglamentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatReglamento)) {
            return false;
        }
        CatReglamento other = (CatReglamento) object;
        if ((this.reglamentoId == null && other.reglamentoId != null) || (this.reglamentoId != null && !this.reglamentoId.equals(other.reglamentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return reglamentoDescripcion;
    }
    
}
