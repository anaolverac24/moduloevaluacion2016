/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "IMPAC_AMB_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImpacAmbProyecto.findAll", query = "SELECT i FROM ImpacAmbProyecto i"),
    @NamedQuery(name = "ImpacAmbProyecto.findByFolioProyecto", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impacAmbProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ImpacAmbProyecto.findBySerialProyecto", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impacAmbProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ImpacAmbProyecto.findByFolioSerial", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impacAmbProyectoPK.folioProyecto = :folioProyecto AND i.impacAmbProyectoPK.serialProyecto = :serialProyecto ORDER BY i.etapaId.etapaId"),
    @NamedQuery(name = "ImpacAmbProyecto.findByImpacAmbproyId", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impacAmbProyectoPK.impacAmbproyId = :impacAmbproyId"),
    @NamedQuery(name = "ImpacAmbProyecto.findByImpactIndent", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impactIndent = :impactIndent"),
    @NamedQuery(name = "ImpacAmbProyecto.findByImpactDesc", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impactDesc = :impactDesc"),
    @NamedQuery(name = "ImpacAmbProyecto.findByImpacCarac", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impacCarac = :impacCarac"),
    @NamedQuery(name = "ImpacAmbProyecto.findByImpacEva", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impacEva = :impacEva"),
    @NamedQuery(name = "ImpacAmbProyecto.findByImpacIndic", query = "SELECT i FROM ImpacAmbProyecto i WHERE i.impacIndic = :impacIndic")})
public class ImpacAmbProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ImpacAmbProyectoPK impacAmbProyectoPK;
    @Column(name = "IMPACT_INDENT")
    private String impactIndent;
    @Column(name = "IMPACT_DESC")
    private String impactDesc;
    @Column(name = "IMPAC_CARAC")
    private String impacCarac;
    @Column(name = "IMPAC_EVA")
    private String impacEva;
    @Column(name = "IMPAC_INDIC")
    private String impacIndic;
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID")
    @ManyToOne(optional = false)
    private CatEtapa etapaId;
    @JoinColumn(name = "IMPACTO_ID", referencedColumnName = "TIPO_IMPACTO_ID")
    @ManyToOne(optional = false)
    private CatTipoImpacto impactoId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public ImpacAmbProyecto() {
    }

    public ImpacAmbProyecto(ImpacAmbProyectoPK impacAmbProyectoPK) {
        this.impacAmbProyectoPK = impacAmbProyectoPK;
    }

    public ImpacAmbProyecto(String folioProyecto, short serialProyecto, short impacAmbproyId) {
        this.impacAmbProyectoPK = new ImpacAmbProyectoPK(folioProyecto, serialProyecto, impacAmbproyId);
    }

    public ImpacAmbProyectoPK getImpacAmbProyectoPK() {
        return impacAmbProyectoPK;
    }

    public void setImpacAmbProyectoPK(ImpacAmbProyectoPK impacAmbProyectoPK) {
        this.impacAmbProyectoPK = impacAmbProyectoPK;
    }

    public String getImpactIndent() {
        return impactIndent;
    }

    public void setImpactIndent(String impactIndent) {
        this.impactIndent = impactIndent;
    }

    public String getImpactDesc() {
        return impactDesc;
    }

    public void setImpactDesc(String impactDesc) {
        this.impactDesc = impactDesc;
    }

    public String getImpacCarac() {
        return impacCarac;
    }

    public void setImpacCarac(String impacCarac) {
        this.impacCarac = impacCarac;
    }

    public String getImpacEva() {
        return impacEva;
    }

    public void setImpacEva(String impacEva) {
        this.impacEva = impacEva;
    }

    public String getImpacIndic() {
        return impacIndic;
    }

    public void setImpacIndic(String impacIndic) {
        this.impacIndic = impacIndic;
    }

    public CatEtapa getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(CatEtapa etapaId) {
        this.etapaId = etapaId;
    }

    public CatTipoImpacto getImpactoId() {
        return impactoId;
    }

    public void setImpactoId(CatTipoImpacto impactoId) {
        this.impactoId = impactoId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (impacAmbProyectoPK != null ? impacAmbProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImpacAmbProyecto)) {
            return false;
        }
        ImpacAmbProyecto other = (ImpacAmbProyecto) object;
        if ((this.impacAmbProyectoPK == null && other.impacAmbProyectoPK != null) || (this.impacAmbProyectoPK != null && !this.impacAmbProyectoPK.equals(other.impacAmbProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ImpacAmbProyecto[ impacAmbProyectoPK=" + impacAmbProyectoPK + " ]";
    }
    
}
