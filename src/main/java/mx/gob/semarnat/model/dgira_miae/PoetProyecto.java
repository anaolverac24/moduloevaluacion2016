/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "POET_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PoetProyecto.findAll", query = "SELECT p FROM PoetProyecto p"),
    @NamedQuery(name = "PoetProyecto.findByFolioProyecto", query = "SELECT p FROM PoetProyecto p WHERE p.poetProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PoetProyecto.findBySerialProyecto", query = "SELECT p FROM PoetProyecto p WHERE p.poetProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PoetProyecto.findByPoetId", query = "SELECT p FROM PoetProyecto p WHERE p.poetProyectoPK.poetId = :poetId")})
public class PoetProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PoetProyectoPK poetProyectoPK;
    @Lob
    @Column(name = "POET_JUSTIFICACION")
    private String poetJustificacion;
    @JoinColumn(name = "POET_ID", referencedColumnName = "POET_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatPoet catPoet;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public PoetProyecto() {
    }

    public PoetProyecto(PoetProyectoPK poetProyectoPK) {
        this.poetProyectoPK = poetProyectoPK;
    }

    public PoetProyecto(String folioProyecto, short serialProyecto, short poetId) {
        this.poetProyectoPK = new PoetProyectoPK(folioProyecto, serialProyecto, poetId);
    }

    public PoetProyectoPK getPoetProyectoPK() {
        return poetProyectoPK;
    }

    public void setPoetProyectoPK(PoetProyectoPK poetProyectoPK) {
        this.poetProyectoPK = poetProyectoPK;
    }

    public String getPoetJustificacion() {
        return poetJustificacion;
    }

    public void setPoetJustificacion(String poetJustificacion) {
        this.poetJustificacion = poetJustificacion;
    }

    public CatPoet getCatPoet() {
        return catPoet;
    }

    public void setCatPoet(CatPoet catPoet) {
        this.catPoet = catPoet;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poetProyectoPK != null ? poetProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PoetProyecto)) {
            return false;
        }
        PoetProyecto other = (PoetProyecto) object;
        if ((this.poetProyectoPK == null && other.poetProyectoPK != null) || (this.poetProyectoPK != null && !this.poetProyectoPK.equals(other.poetProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelEmia.PoetProyecto[ poetProyectoPK=" + poetProyectoPK + " ]";
    }
    
}
