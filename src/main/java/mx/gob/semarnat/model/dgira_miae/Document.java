package mx.gob.semarnat.model.dgira_miae;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@Entity
@Table(name = "DOCUMENT_LAYOUT")
@XmlRootElement
@NamedQueries({ 
	@NamedQuery(name = "Document.findByIdAndType", query = "SELECT d FROM Document d WHERE d.type = 'expedient' AND d.documentId = ?1 AND d.projectId = ?2"),
        @NamedQuery(name = "Document.findByIdAndPrevencion", query = "SELECT d FROM Document d WHERE d.type = 'expedient' AND d.documentId in (94,118,124,1176) AND d.projectId = ?1 "),
        @NamedQuery(name = "Document.findByProjectIdAndDocumentId", query = "select d from Document d where project_id = ?1 and document_id = ?2"),
        @NamedQuery(name = "Document.findByIdAndTypeAndWeb", query = "SELECT d FROM Document d WHERE d.type = 'expedient' AND d.documentId = ?1 AND d.projectId = ?2 and d.documentWeb = ?3")
})
public class Document implements Serializable  {
	@Id
	@SequenceGenerator(name = "SEQ_DOCUMENT_LAYOUT", sequenceName = "SEQ_DOCUMENT_LAYOUT", allocationSize = 1)
	@GeneratedValue(generator = "SEQ_DOCUMENT_LAYOUT", strategy = GenerationType.SEQUENCE)
	public Long id;

	@Lob
	@Column(name = "DOCUMENT_BODY")
	public String body;

	@Lob
	@Column(name = "DOCUMENT_HEADER")
	public String header;

	@Lob
	@Column(name = "DOCUMENT_FOOTER")
	public String footer;

	@Column(name = "DOCUMENT_NAME")
	public String name;

	@Column(name = "CREATED_AT")
	public Date createdAt;

	@Column(name = "UPDATED_AT")
	public Date updatedAt;

	@Column(name = "DOCUMENT_TYPE")
	public String type;

	@Column(name = "OWNER_ID")
	public Long ownerId;

	@Column(name = "ASSIGNEE_ID")
	public Long assigneeId;

	@Column(name = "DOCUMENT_ID")
	public Long documentId;

	@Column(name = "PROJECT_ID")
	public String projectId;
        
        @Column(name = "DOCUMENT_WEB")
        public int documentWeb;

	public void setCreatedAt() {
		this.createdAt = new Date();
	}

	public void setUpdatedAt() {
		this.updatedAt = new Date();
	}

	public Long getId() {
		return id;
	}
}