/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import mx.gob.semarnat.model.catalogos.CatUnidadMedida;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CONTAMINANTE_PROYECTO", catalog = "", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContaminanteProyecto.findAll", query = "SELECT c FROM ContaminanteProyecto c"),
    @NamedQuery(name = "ContaminanteProyecto.findByFolioProyecto", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ContaminanteProyecto.findBySerialProyecto", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ContaminanteProyecto.findByContaminanteId", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteProyectoPK.contaminanteId = :contaminanteId"),
    @NamedQuery(name = "ContaminanteProyecto.findByContaminanteCantidad", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteCantidad = :contaminanteCantidad"),
    @NamedQuery(name = "ContaminanteProyecto.findByCtunClave", query = "SELECT c FROM ContaminanteProyecto c WHERE c.ctunClave = :ctunClave"),
    @NamedQuery(name = "ContaminanteProyecto.findByContaminanteMedidaControl", query = "SELECT c FROM ContaminanteProyecto c WHERE c.contaminanteMedidaControl = :contaminanteMedidaControl"),
    @NamedQuery(name = "ContaminanteProyecto.findByFuenteEmisora", query = "SELECT c FROM ContaminanteProyecto c WHERE c.fuenteEmisora = :fuenteEmisora"),
    @NamedQuery(name = "ContaminanteProyecto.findByFolioSerial", query = "SELECT e FROM ContaminanteProyecto e WHERE e.contaminanteProyectoPK.folioProyecto = :folio and e.contaminanteProyectoPK.serialProyecto = :serial"),
    @NamedQuery(name = "ContaminanteProyecto.findByObservaciones", query = "SELECT c FROM ContaminanteProyecto c WHERE c.observaciones = :observaciones")})
public class ContaminanteProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContaminanteProyectoPK contaminanteProyectoPK;
    @Column(name = "ETAPA_ID")
    private Short etapaId;
    @Column(name = "CONTAMINANTE_TIPO")
    private Short contaminanteTipo;
    @Column(name = "CONTAMINANTE_EMISIONES")
    private Short contaminanteEmisiones;
    @Column(name = "CONTAMINANTE_CANTIDAD")
    private BigDecimal contaminanteCantidad;    
    /**
     * Union de tabla con schema 
     */
    @JoinColumn(name = "CTUN_CLAVE", referencedColumnName = "CTUN_CLVE")
    @ManyToOne
    private CatUnidadMedida ctunClave;
    
    @Column(name = "CONTAMINANTE_MEDIDA_CONTROL")
    private String contaminanteMedidaControl;
    @Column(name = "FUENTE_EMISORA")
    private String fuenteEmisora;
    @Column(name = "OBSERVACIONES")
    private String observaciones; 
    
    @Column(name="DESC_EMI_RESI_DES")
    private String descEmiResiDes;
  
    
//    @Column(name = "CONTAMINANTE_ID")
//    private Short contaminanteId;

    @JoinColumn(name = "CONTAMINANTE_EMISIONES", referencedColumnName = "CONTAMINANTE_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatContaminante catContaminante;
    @JoinColumns({
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private EtapaProyecto etapaProyecto;

    public ContaminanteProyecto() {
    }

    public ContaminanteProyecto(ContaminanteProyectoPK contaminanteProyectoPK) {
        this.contaminanteProyectoPK = contaminanteProyectoPK;
    }

    public ContaminanteProyectoPK getContaminanteProyectoPK() {
        return contaminanteProyectoPK;
    }

    public void setContaminanteProyectoPK(ContaminanteProyectoPK contaminanteProyectoPK) {
        this.contaminanteProyectoPK = contaminanteProyectoPK;
    }

    public BigDecimal getContaminanteCantidad() {
        return contaminanteCantidad;
    }

    public void setContaminanteCantidad(BigDecimal contaminanteCantidad) {
        this.contaminanteCantidad = contaminanteCantidad;
    }

    public CatUnidadMedida getCtunClave() {
        return ctunClave;
    }

    public void setCtunClave(CatUnidadMedida ctunClave) {
        this.ctunClave = ctunClave;
    }

    public String getFuenteEmisora() {
        return fuenteEmisora;
    }

    public void setFuenteEmisora(String fuenteEmisora) {
        this.fuenteEmisora = fuenteEmisora;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

//    public CatContaminante getCatContaminante() {
//        return catContaminante;
//    }
//
//    public void setCatContaminante(CatContaminante catContaminante) {
//        this.catContaminante = catContaminante;
//    }

    public EtapaProyecto getEtapaProyecto() {
        return etapaProyecto;
    }

    public void setEtapaProyecto(EtapaProyecto etapaProyecto) {
        this.etapaProyecto = etapaProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contaminanteProyectoPK != null ? contaminanteProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContaminanteProyecto)) {
            return false;
        }
        ContaminanteProyecto other = (ContaminanteProyecto) object;
        if ((this.contaminanteProyectoPK == null && other.contaminanteProyectoPK != null) || (this.contaminanteProyectoPK != null && !this.contaminanteProyectoPK.equals(other.contaminanteProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ContaminanteProyecto[ contaminanteProyectoPK=" + contaminanteProyectoPK + " ]";
    }


//    /**
//     * @return the contaminanteId
//     */
//    public Short getContaminanteId() {
//        return contaminanteId;
//    }
//
//    /**
//     * @param contaminanteId the contaminanteId to set
//     */
//    public void setContaminanteId(Short contaminanteId) {
//        this.contaminanteId = contaminanteId;
//    }

    public Short getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(Short etapaId) {
        this.etapaId = etapaId;
    }

    public Short getContaminanteTipo() {
        return contaminanteTipo;
    }

    public void setContaminanteTipo(Short contaminanteTipo) {
        this.contaminanteTipo = contaminanteTipo;
    }

    public Short getContaminanteEmisiones() {
        return contaminanteEmisiones;
    }

    public void setContaminanteEmisiones(Short contaminanteEmisiones) {
        this.contaminanteEmisiones = contaminanteEmisiones;
    }

    public String getContaminanteMedidaControl() {
        return contaminanteMedidaControl;
    }

    public void setContaminanteMedidaControl(String contaminanteMedidaControl) {
        this.contaminanteMedidaControl = contaminanteMedidaControl;
    }

    public CatContaminante getCatContaminante() {
        return catContaminante;
    }

    public void setCatContaminante(CatContaminante catContaminante) {
        this.catContaminante = catContaminante;
    }

	/**
	 * @return the descEmiResiDes
	 */
	public String getDescEmiResiDes() {
		return descEmiResiDes;
	}

	/**
	 * @param descEmiResiDes the descEmiResiDes to set
	 */
	public void setDescEmiResiDes(String descEmiResiDes) {
		this.descEmiResiDes = descEmiResiDes;
	}

    
}
