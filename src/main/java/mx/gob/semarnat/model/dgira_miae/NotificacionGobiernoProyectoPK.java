/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class NotificacionGobiernoProyectoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "NOTIFICACIONG_ID")
    private String notificaciongId;

    public NotificacionGobiernoProyectoPK() {
    }

    public NotificacionGobiernoProyectoPK(String bitacoraProyecto, String notificaciongId) {
        this.bitacoraProyecto = bitacoraProyecto;
        this.notificaciongId = notificaciongId;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getNotificaciongId() {
        return notificaciongId;
    }

    public void setNotificaciongId(String notificaciongId) {
        this.notificaciongId = notificaciongId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacoraProyecto != null ? bitacoraProyecto.hashCode() : 0);
        hash += (notificaciongId != null ? notificaciongId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionGobiernoProyectoPK)) {
            return false;
        }
        NotificacionGobiernoProyectoPK other = (NotificacionGobiernoProyectoPK) object;
        if ((this.bitacoraProyecto == null && other.bitacoraProyecto != null) || (this.bitacoraProyecto != null && !this.bitacoraProyecto.equals(other.bitacoraProyecto))) {
            return false;
        }
        if ((this.notificaciongId == null && other.notificaciongId != null) || (this.notificaciongId != null && !this.notificaciongId.equals(other.notificaciongId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.NotificacionGobiernoProyectoPK[ bitacoraProyecto=" + bitacoraProyecto + ", notificaciongId=" + notificaciongId + " ]";
    }
    
}
