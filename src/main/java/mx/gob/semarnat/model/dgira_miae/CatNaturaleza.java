/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_NATURALEZA", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatNaturaleza.findAll", query = "SELECT c FROM CatNaturaleza c"),
    @NamedQuery(name = "CatNaturaleza.findByNaturalezaId", query = "SELECT c FROM CatNaturaleza c WHERE c.naturalezaId = :naturalezaId"),
    @NamedQuery(name = "CatNaturaleza.findByNaturalezaDescripcion", query = "SELECT c FROM CatNaturaleza c WHERE c.naturalezaDescripcion = :naturalezaDescripcion")})
public class CatNaturaleza implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "NATURALEZA_ID")
    private Short naturalezaId;
    @Column(name = "NATURALEZA_DESCRIPCION")
    private String naturalezaDescripcion;


    public CatNaturaleza() {
    }

    public CatNaturaleza(Short naturalezaId) {
        this.naturalezaId = naturalezaId;
    }

    public Short getNaturalezaId() {
        return naturalezaId;
    }

    public void setNaturalezaId(Short naturalezaId) {
        this.naturalezaId = naturalezaId;
    }

    public String getNaturalezaDescripcion() {
        return naturalezaDescripcion;
    }

    public void setNaturalezaDescripcion(String naturalezaDescripcion) {
        this.naturalezaDescripcion = naturalezaDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (naturalezaId != null ? naturalezaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatNaturaleza)) {
            return false;
        }
        CatNaturaleza other = (CatNaturaleza) object;
        if ((this.naturalezaId == null && other.naturalezaId != null) || (this.naturalezaId != null && !this.naturalezaId.equals(other.naturalezaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatNaturaleza[ naturalezaId=" + naturalezaId + " ]";
    }
    
}
