/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class PreguntaVegetacionHelper implements Serializable{
    private static final long serialVersionUID = 1L;
    private Integer id;
    private PreguntaVegetacion model;

    public PreguntaVegetacionHelper() {
    }

    public PreguntaVegetacionHelper(Integer id, PreguntaVegetacion model) {
        this.id = id;
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PreguntaVegetacion getModel() {
        return model;
    }

    public void setModel(PreguntaVegetacion model) {
        this.model = model;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PreguntaVegetacionHelper)) {
            return false;
        }
        PreguntaVegetacionHelper ob = (PreguntaVegetacionHelper) obj;
        if (this.id != ob.id) {
            return false;
        }
        return true;
    }
    
    
    

}
