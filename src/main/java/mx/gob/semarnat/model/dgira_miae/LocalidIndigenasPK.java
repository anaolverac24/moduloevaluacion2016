/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class LocalidIndigenasPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "NUM_FOLIO")
    private String numFolio;
    @Basic(optional = false)
    @Column(name = "CVE_PROY")
    private String cveProy;
    @Basic(optional = false)
    @Column(name = "CVE_AREA")
    private String cveArea;
    @Basic(optional = false)
    @Column(name = "VERSION")
    private short version;

    public LocalidIndigenasPK() {
    }

    public LocalidIndigenasPK(String numFolio, String cveProy, String cveArea, short version) {
        this.numFolio = numFolio;
        this.cveProy = cveProy;
        this.cveArea = cveArea;
        this.version = version;
    }

    public String getNumFolio() {
        return numFolio;
    }

    public void setNumFolio(String numFolio) {
        this.numFolio = numFolio;
    }

    public String getCveProy() {
        return cveProy;
    }

    public void setCveProy(String cveProy) {
        this.cveProy = cveProy;
    }

    public String getCveArea() {
        return cveArea;
    }

    public void setCveArea(String cveArea) {
        this.cveArea = cveArea;
    }

    public short getVersion() {
        return version;
    }

    public void setVersion(short version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numFolio != null ? numFolio.hashCode() : 0);
        hash += (cveProy != null ? cveProy.hashCode() : 0);
        hash += (cveArea != null ? cveArea.hashCode() : 0);
        hash += (int) version;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalidIndigenasPK)) {
            return false;
        }
        LocalidIndigenasPK other = (LocalidIndigenasPK) object;
        if ((this.numFolio == null && other.numFolio != null) || (this.numFolio != null && !this.numFolio.equals(other.numFolio))) {
            return false;
        }
        if ((this.cveProy == null && other.cveProy != null) || (this.cveProy != null && !this.cveProy.equals(other.cveProy))) {
            return false;
        }
        if ((this.cveArea == null && other.cveArea != null) || (this.cveArea != null && !this.cveArea.equals(other.cveArea))) {
            return false;
        }
        if (this.version != other.version) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.LocalidIndigenasPK[ numFolio=" + numFolio + ", cveProy=" + cveProy + ", cveArea=" + cveArea + ", version=" + version + " ]";
    }
    
}
