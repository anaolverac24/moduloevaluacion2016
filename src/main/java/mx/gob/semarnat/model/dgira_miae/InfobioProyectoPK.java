/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author Administrador
 */
@Embeddable
public class InfobioProyectoPK implements Serializable {
	@Basic(optional = false)
	@Column(name = "FOLIO_PROYECTO")
	private String folioProyecto;
	@Basic(optional = false)
	@Column(name = "SERIAL_PROYECTO")
	private short serialProyecto;

	@Column(name = "INFO_BIO_ID")	
	private short infoBioId;

	public InfobioProyectoPK() {
	}

	public InfobioProyectoPK(String folioProyecto, short serialProyecto, short infoBioId) {
		this.folioProyecto = folioProyecto;
		this.serialProyecto = serialProyecto;
		this.infoBioId = infoBioId;
	}

	public InfobioProyectoPK(String folioProyecto, short serialProyecto) {
		this.folioProyecto = folioProyecto;
		this.serialProyecto = serialProyecto;
	}
	public String getFolioProyecto() {
		return folioProyecto;
	}

	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}

	public short getSerialProyecto() {
		return serialProyecto;
	}

	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}

	public short getInfoBioId() {
		return infoBioId;
	}

	public void setInfoBioId(short infoBioId) {
		this.infoBioId = infoBioId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
		hash += (int) serialProyecto;
		hash += (int) infoBioId;
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof InfobioProyectoPK)) {
			return false;
		}
		InfobioProyectoPK other = (InfobioProyectoPK) object;
		if ((this.folioProyecto == null && other.folioProyecto != null)
				|| (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
			return false;
		}
		if (this.serialProyecto != other.serialProyecto) {
			return false;
		}
		if (this.infoBioId != other.infoBioId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "mx.gob.semarnat.mia.model.InfobioProyectoPK[ folioProyecto=" + folioProyecto + ", serialProyecto="
				+ serialProyecto + ", infoBioId=" + infoBioId + " ]";
	}

}
