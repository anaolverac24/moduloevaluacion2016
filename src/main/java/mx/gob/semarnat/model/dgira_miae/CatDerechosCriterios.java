/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "CAT_DERECHOS_CRITERIOS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatDerechosCriterios.findAll", query = "SELECT c FROM CatDerechosCriterios c"),
    @NamedQuery(name = "CatDerechosCriterios.findByCriterioId", query = "SELECT c FROM CatDerechosCriterios c WHERE c.criterioId = :criterioId"),
    @NamedQuery(name = "CatDerechosCriterios.findByCriterioDescripcion", query = "SELECT c FROM CatDerechosCriterios c WHERE c.criterioDescripcion = :criterioDescripcion"),
    @NamedQuery(name = "CatDerechosCriterios.findByCriterioOrden", query = "SELECT c FROM CatDerechosCriterios c WHERE c.criterioOrden = :criterioOrden"),
    @NamedQuery(name = "CatDerechosCriterios.findByCriterioIdestatus", query = "SELECT c FROM CatDerechosCriterios c WHERE c.criterioIdestatus = :criterioIdestatus")})
public class CatDerechosCriterios implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catDerechosCriterios")
    private List<CatDerechosCriteriosvalores> catDerechosCriteriosvaloresList;
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "CRITERIO_ID")
    private BigDecimal criterioId;
    @Basic(optional = false)
    @Column(name = "CRITERIO_DESCRIPCION")
    private String criterioDescripcion;
    @Basic(optional = false)
    @Column(name = "CRITERIO_ORDEN")
    private BigInteger criterioOrden;
    @Basic(optional = false)
    @Column(name = "CRITERIO_IDESTATUS")
    private Character criterioIdestatus;

    public CatDerechosCriterios() {
    }

    public CatDerechosCriterios(BigDecimal criterioId) {
        this.criterioId = criterioId;
    }

    public CatDerechosCriterios(BigDecimal criterioId, String criterioDescripcion, BigInteger criterioOrden, Character criterioIdestatus) {
        this.criterioId = criterioId;
        this.criterioDescripcion = criterioDescripcion;
        this.criterioOrden = criterioOrden;
        this.criterioIdestatus = criterioIdestatus;
    }

    public BigDecimal getCriterioId() {
        return criterioId;
    }

    public void setCriterioId(BigDecimal criterioId) {
        this.criterioId = criterioId;
    }

    public String getCriterioDescripcion() {
        return criterioDescripcion;
    }

    public void setCriterioDescripcion(String criterioDescripcion) {
        this.criterioDescripcion = criterioDescripcion;
    }

    public BigInteger getCriterioOrden() {
        return criterioOrden;
    }

    public void setCriterioOrden(BigInteger criterioOrden) {
        this.criterioOrden = criterioOrden;
    }

    public Character getCriterioIdestatus() {
        return criterioIdestatus;
    }

    public void setCriterioIdestatus(Character criterioIdestatus) {
        this.criterioIdestatus = criterioIdestatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criterioId != null ? criterioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatDerechosCriterios)) {
            return false;
        }
        CatDerechosCriterios other = (CatDerechosCriterios) object;
        if ((this.criterioId == null && other.criterioId != null) || (this.criterioId != null && !this.criterioId.equals(other.criterioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatDerechosCriterios[ criterioId=" + criterioId + " ]";
    }

    @XmlTransient
    public List<CatDerechosCriteriosvalores> getCatDerechosCriteriosvaloresList() {
        return catDerechosCriteriosvaloresList;
    }

    public void setCatDerechosCriteriosvaloresList(List<CatDerechosCriteriosvalores> catDerechosCriteriosvaloresList) {
        this.catDerechosCriteriosvaloresList = catDerechosCriteriosvaloresList;
    }
    
}
