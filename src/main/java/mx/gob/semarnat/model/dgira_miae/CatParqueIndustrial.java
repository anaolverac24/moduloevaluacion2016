/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_PARQUE_INDUSTRIAL", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatParqueIndustrial.findAll", query = "SELECT c FROM CatParqueIndustrial c"),
    @NamedQuery(name = "CatParqueIndustrial.findByParqueId", query = "SELECT c FROM CatParqueIndustrial c WHERE c.parqueId = :parqueId"),
    @NamedQuery(name = "CatParqueIndustrial.findByParqueDescripcion", query = "SELECT c FROM CatParqueIndustrial c WHERE c.parqueDescripcion = :parqueDescripcion"),
    @NamedQuery(name = "CatParqueIndustrial.findByParqueFecha", query = "SELECT c FROM CatParqueIndustrial c WHERE c.parqueFecha = :parqueFecha"),
    @NamedQuery(name = "CatParqueIndustrial.findByParqueOficio", query = "SELECT c FROM CatParqueIndustrial c WHERE c.parqueOficio = :parqueOficio")})
public class CatParqueIndustrial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PARQUE_ID")
    private Short parqueId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "PARQUE_DESCRIPCION")
    private String parqueDescripcion;
    @Column(name = "PARQUE_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date parqueFecha;
    @Size(max = 40)
    @Column(name = "PARQUE_OFICIO")
    private String parqueOficio;

    public CatParqueIndustrial() {
    }

    public CatParqueIndustrial(Short parqueId) {
        this.parqueId = parqueId;
    }

    public CatParqueIndustrial(Short parqueId, String parqueDescripcion) {
        this.parqueId = parqueId;
        this.parqueDescripcion = parqueDescripcion;
    }

    public Short getParqueId() {
        return parqueId;
    }

    public void setParqueId(Short parqueId) {
        this.parqueId = parqueId;
    }

    public String getParqueDescripcion() {
        return parqueDescripcion;
    }

    public void setParqueDescripcion(String parqueDescripcion) {
        this.parqueDescripcion = parqueDescripcion;
    }

    public Date getParqueFecha() {
        return parqueFecha;
    }

    public void setParqueFecha(Date parqueFecha) {
        this.parqueFecha = parqueFecha;
    }

    public String getParqueOficio() {
        return parqueOficio;
    }

    public void setParqueOficio(String parqueOficio) {
        this.parqueOficio = parqueOficio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parqueId != null ? parqueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatParqueIndustrial)) {
            return false;
        }
        CatParqueIndustrial other = (CatParqueIndustrial) object;
        if ((this.parqueId == null && other.parqueId != null) || (this.parqueId != null && !this.parqueId.equals(other.parqueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatParqueIndustrial[ parqueId=" + parqueId + " ]";
    }
    
}
