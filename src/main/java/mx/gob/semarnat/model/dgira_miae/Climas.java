/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CLIMAS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Climas.findAll", query = "SELECT c FROM Climas c"),
    @NamedQuery(name = "Climas.findByNumFolio", query = "SELECT c FROM Climas c WHERE c.climasPK.numFolio = :numFolio"),
    @NamedQuery(name = "Climas.findByCveProy", query = "SELECT c FROM Climas c WHERE c.climasPK.cveProy = :cveProy"),
    @NamedQuery(name = "Climas.findByCveArea", query = "SELECT c FROM Climas c WHERE c.climasPK.cveArea = :cveArea"),
    @NamedQuery(name = "Climas.findByVersion", query = "SELECT c FROM Climas c WHERE c.climasPK.version = :version"),
    @NamedQuery(name = "Climas.findByDesTem", query = "SELECT c FROM Climas c WHERE c.desTem = :desTem"),
    @NamedQuery(name = "Climas.findByDescPrec", query = "SELECT c FROM Climas c WHERE c.descPrec = :descPrec"),
    @NamedQuery(name = "Climas.findByGrupomapa2", query = "SELECT c FROM Climas c WHERE c.grupomapa2 = :grupomapa2"),
    @NamedQuery(name = "Climas.findByClimaTipo", query = "SELECT c FROM Climas c WHERE c.climaTipo = :climaTipo"),
    @NamedQuery(name = "Climas.findBySupEa", query = "SELECT c FROM Climas c WHERE c.supEa = :supEa"),
    @NamedQuery(name = "Climas.findByProy", query = "SELECT c FROM Climas c WHERE c.proy = :proy"),
    @NamedQuery(name = "Climas.findByComp", query = "SELECT c FROM Climas c WHERE c.comp = :comp"),
    @NamedQuery(name = "Climas.findByDescrip", query = "SELECT c FROM Climas c WHERE c.descrip = :descrip"),
    @NamedQuery(name = "Climas.findByAreabuffer", query = "SELECT c FROM Climas c WHERE c.areabuffer = :areabuffer"),
    @NamedQuery(name = "Climas.findByArea", query = "SELECT c FROM Climas c WHERE c.area = :area"),
    @NamedQuery(name = "Climas.findByFechaHora", query = "SELECT c FROM Climas c WHERE c.fechaHora = :fechaHora"),
    @NamedQuery(name = "Climas.findByIdr", query = "SELECT c FROM Climas c WHERE c.climasPK.idr = :idr")})
public class Climas implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ClimasPK climasPK;
    @Size(max = 1000)
    @Column(name = "DES_TEM")
    private String desTem;
    @Size(max = 1000)
    @Column(name = "DESC_PREC")
    private String descPrec;
    @Size(max = 800)
    @Column(name = "GRUPOMAPA2")
    private String grupomapa2;
    @Size(max = 800)
    @Column(name = "CLIMA_TIPO")
    private String climaTipo;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Size(max = 80)
    @Column(name = "PROY")
    private String proy;
    @Size(max = 80)
    @Column(name = "COMP")
    private String comp;
    @Size(max = 800)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public Climas() {
    }

    public Climas(ClimasPK climasPK) {
        this.climasPK = climasPK;
    }

    public Climas(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.climasPK = new ClimasPK(numFolio, cveProy, cveArea, version, idr);
    }

    public ClimasPK getClimasPK() {
        return climasPK;
    }

    public void setClimasPK(ClimasPK climasPK) {
        this.climasPK = climasPK;
    }

    public String getDesTem() {
        return desTem;
    }

    public void setDesTem(String desTem) {
        this.desTem = desTem;
    }

    public String getDescPrec() {
        return descPrec;
    }

    public void setDescPrec(String descPrec) {
        this.descPrec = descPrec;
    }

    public String getGrupomapa2() {
        return grupomapa2;
    }

    public void setGrupomapa2(String grupomapa2) {
        this.grupomapa2 = grupomapa2;
    }

    public String getClimaTipo() {
        return climaTipo;
    }

    public void setClimaTipo(String climaTipo) {
        this.climaTipo = climaTipo;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (climasPK != null ? climasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Climas)) {
            return false;
        }
        Climas other = (Climas) object;
        if ((this.climasPK == null && other.climasPK != null) || (this.climasPK != null && !this.climasPK.equals(other.climasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.Climas[ climasPK=" + climasPK + " ]";
    }
    
}
