/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "EVALUACION_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EvaluacionProyecto.findAll", query = "SELECT e FROM EvaluacionProyecto e"),
    @NamedQuery(name = "EvaluacionProyecto.findByBitacoraProyecto", query = "SELECT e FROM EvaluacionProyecto e WHERE e.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaSistesisProyecto", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaSistesisProyecto = :evaSistesisProyecto"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaValCritPromovente", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaValCritPromovente = :evaValCritPromovente"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaValCritEvaluador", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaValCritEvaluador = :evaValCritEvaluador"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaMontCritPromovente", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaMontCritPromovente = :evaMontCritPromovente"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaMontCritEvaluador", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaMontCritEvaluador = :evaMontCritEvaluador")})
public class EvaluacionProyecto implements Serializable {
    @Column(name = "EVA_LOTE_PREVENCION")
    private Integer evaLotePrevencion;
    @Column(name = "EVA_LOTE_INFO_ADICIONAL")
    private Integer evaLoteInfoAdicional;
    @Size(max = 2)
    @Column(name = "EVA_CLAVE_TRAMITE2")
    private String evaClaveTramite2;
    @Column(name = "EVA_ID_TRAMITE2")
    private Integer evaIdTramite2;
    @Column(name = "EVA_TIPO_DOC_ID")
    private Short evaTipoDocId;
    @Column(name = "EVA_GACETA_FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaGacetaFechaPublicacion;
    @Size(max = 300)
    @Column(name = "EVA_GACETA_SEPARATA")
    private String evaGacetaSeparata;
    @Column(name = "EVA_GACETA_PERIODO_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaGacetaPeriodoInicio;
    @Column(name = "EVA_GACETA_PERIODO_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaGacetaPeriodoFinal;
    @Column(name = "EVA_EXTRACTO_FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaExtractoFechaPublicacion;
    @Size(max = 2000)
    @Column(name = "EVA_EXTRACTO_DIARIOS")
    private String evaExtractoDiarios;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionProyecto")
    private Collection<DocumentoDgiraFlujoHist> documentoDgiraFlujoHistCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionProyecto")
    private Collection<DocumentoRespuestaDgira> documentoRespuestaDgiraCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionProyecto")
    private Collection<DocumentoDgiraFlujo> documentoDgiraFlujoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionProyecto")
    private Collection<ConsultaPublicaProyecto> consultaPublicaProyectoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacionProyecto")
    private Collection<ComentarioProyecto> comentarioProyectoCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Size(max = 4000)
    @Column(name = "EVA_SISTESIS_PROYECTO")
    private String evaSistesisProyecto;
    @Column(name = "EVA_VAL_CRIT_PROMOVENTE")
    private Short evaValCritPromovente;
    @Column(name = "EVA_VAL_CRIT_EVALUADOR")
    private Short evaValCritEvaluador;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "EVA_MONT_CRIT_PROMOVENTE")
    private BigDecimal evaMontCritPromovente;
    @Column(name = "EVA_MONT_CRIT_EVALUADOR")
    private BigDecimal evaMontCritEvaluador;
    @Size(max = 1)
    @Column(name = "EVA_CHECK_LIST_REALIZADO")
    private String evaCheckListRealizado;
    @Size(max = 1)
    @Column(name = "EVA_PAGO_DERECHOS_REALIZADO")
    private String evaPagoDerechosRealizado;
    


    public EvaluacionProyecto() {
    }

    public EvaluacionProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getEvaSistesisProyecto() {
        return evaSistesisProyecto;
    }

    public void setEvaSistesisProyecto(String evaSistesisProyecto) {
        this.evaSistesisProyecto = evaSistesisProyecto;
    }

    public Short getEvaValCritPromovente() {
        return evaValCritPromovente;
    }

    public void setEvaValCritPromovente(Short evaValCritPromovente) {
        this.evaValCritPromovente = evaValCritPromovente;
    }

    public Short getEvaValCritEvaluador() {
        return evaValCritEvaluador;
    }

    public void setEvaValCritEvaluador(Short evaValCritEvaluador) {
        this.evaValCritEvaluador = evaValCritEvaluador;
    }

    public BigDecimal getEvaMontCritPromovente() {
        return evaMontCritPromovente;
    }

    public void setEvaMontCritPromovente(BigDecimal evaMontCritPromovente) {
        this.evaMontCritPromovente = evaMontCritPromovente;
    }

    public BigDecimal getEvaMontCritEvaluador() {
        return evaMontCritEvaluador;
    }

    public void setEvaMontCritEvaluador(BigDecimal evaMontCritEvaluador) {
        this.evaMontCritEvaluador = evaMontCritEvaluador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacoraProyecto != null ? bitacoraProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EvaluacionProyecto)) {
            return false;
        }
        EvaluacionProyecto other = (EvaluacionProyecto) object;
        if ((this.bitacoraProyecto == null && other.bitacoraProyecto != null) || (this.bitacoraProyecto != null && !this.bitacoraProyecto.equals(other.bitacoraProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.EvaluacionProyecto[ bitacoraProyecto=" + bitacoraProyecto + " ]";
    }

    @XmlTransient
    public Collection<ComentarioProyecto> getComentarioProyectoCollection() {
        return comentarioProyectoCollection;
    }

    public void setComentarioProyectoCollection(Collection<ComentarioProyecto> comentarioProyectoCollection) {
        this.comentarioProyectoCollection = comentarioProyectoCollection;
    }

    /**
     * @return the evaCheckListRealizado
     */
    public String getEvaCheckListRealizado() {
        return evaCheckListRealizado;
    }

    /**
     * @param evaCheckListRealizado the evaCheckListRealizado to set
     */
    public void setEvaCheckListRealizado(String evaCheckListRealizado) {
        this.evaCheckListRealizado = evaCheckListRealizado;
    }

    /**
     * @return the evaPagoDerechosRealizado
     */
    public String getEvaPagoDerechosRealizado() {
        return evaPagoDerechosRealizado;
    }

    /**
     * @param evaPagoDerechosRealizado the evaPagoDerechosRealizado to set
     */
    public void setEvaPagoDerechosRealizado(String evaPagoDerechosRealizado) {
        this.evaPagoDerechosRealizado = evaPagoDerechosRealizado;
    }

    @XmlTransient
    public Collection<ConsultaPublicaProyecto> getConsultaPublicaProyectoCollection() {
        return consultaPublicaProyectoCollection;
    }

    public void setConsultaPublicaProyectoCollection(Collection<ConsultaPublicaProyecto> consultaPublicaProyectoCollection) {
        this.consultaPublicaProyectoCollection = consultaPublicaProyectoCollection;
    }

    public Date getEvaGacetaFechaPublicacion() {
        return evaGacetaFechaPublicacion;
    }

    public void setEvaGacetaFechaPublicacion(Date evaGacetaFechaPublicacion) {
        this.evaGacetaFechaPublicacion = evaGacetaFechaPublicacion;
    }

    public String getEvaGacetaSeparata() {
        return evaGacetaSeparata;
    }

    public void setEvaGacetaSeparata(String evaGacetaSeparata) {
        this.evaGacetaSeparata = evaGacetaSeparata;
    }

    public Date getEvaGacetaPeriodoInicio() {
        return evaGacetaPeriodoInicio;
    }

    public void setEvaGacetaPeriodoInicio(Date evaGacetaPeriodoInicio) {
        this.evaGacetaPeriodoInicio = evaGacetaPeriodoInicio;
    }

    public Date getEvaGacetaPeriodoFinal() {
        return evaGacetaPeriodoFinal;
    }

    public void setEvaGacetaPeriodoFinal(Date evaGacetaPeriodoFinal) {
        this.evaGacetaPeriodoFinal = evaGacetaPeriodoFinal;
    }

    public Date getEvaExtractoFechaPublicacion() {
        return evaExtractoFechaPublicacion;
    }

    public void setEvaExtractoFechaPublicacion(Date evaExtractoFechaPublicacion) {
        this.evaExtractoFechaPublicacion = evaExtractoFechaPublicacion;
    }

    public String getEvaExtractoDiarios() {
        return evaExtractoDiarios;
    }

    public void setEvaExtractoDiarios(String evaExtractoDiarios) {
        this.evaExtractoDiarios = evaExtractoDiarios;
    }

    public Integer getEvaLotePrevencion() {
        return evaLotePrevencion;
    }

    public void setEvaLotePrevencion(Integer evaLotePrevencion) {
        this.evaLotePrevencion = evaLotePrevencion;
    }

    public Integer getEvaLoteInfoAdicional() {
        return evaLoteInfoAdicional;
    }

    public void setEvaLoteInfoAdicional(Integer evaLoteInfoAdicional) {
        this.evaLoteInfoAdicional = evaLoteInfoAdicional;
    }

    @XmlTransient
    public Collection<DocumentoDgiraFlujoHist> getDocumentoDgiraFlujoHistCollection() {
        return documentoDgiraFlujoHistCollection;
    }

    public void setDocumentoDgiraFlujoHistCollection(Collection<DocumentoDgiraFlujoHist> documentoDgiraFlujoHistCollection) {
        this.documentoDgiraFlujoHistCollection = documentoDgiraFlujoHistCollection;
    }

    @XmlTransient
    public Collection<DocumentoRespuestaDgira> getDocumentoRespuestaDgiraCollection() {
        return documentoRespuestaDgiraCollection;
    }

    public void setDocumentoRespuestaDgiraCollection(Collection<DocumentoRespuestaDgira> documentoRespuestaDgiraCollection) {
        this.documentoRespuestaDgiraCollection = documentoRespuestaDgiraCollection;
    }

    @XmlTransient
    public Collection<DocumentoDgiraFlujo> getDocumentoDgiraFlujoCollection() {
        return documentoDgiraFlujoCollection;
    }

    public void setDocumentoDgiraFlujoCollection(Collection<DocumentoDgiraFlujo> documentoDgiraFlujoCollection) {
        this.documentoDgiraFlujoCollection = documentoDgiraFlujoCollection;
    }

    public String getEvaClaveTramite2() {
        return evaClaveTramite2;
    }

    public void setEvaClaveTramite2(String evaClaveTramite2) {
        this.evaClaveTramite2 = evaClaveTramite2;
    }

    public Integer getEvaIdTramite2() {
        return evaIdTramite2;
    }

    public void setEvaIdTramite2(Integer evaIdTramite2) {
        this.evaIdTramite2 = evaIdTramite2;
    }

    public Short getEvaTipoDocId() {
        return evaTipoDocId;
    }

    public void setEvaTipoDocId(Short evaTipoDocId) {
        this.evaTipoDocId = evaTipoDocId;
    }
    
}
