/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class CatEspecificacionPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ESPECIFICACION_ID")
    private String especificacionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NORMA_ID")
    private short normaId;

    public CatEspecificacionPK() {
    }

    public CatEspecificacionPK(String especificacionId, short normaId) {
        this.especificacionId = especificacionId;
        this.normaId = normaId;
    }

    public String getEspecificacionIdComp() {
        return especificacionId.replaceAll("\\s", "").trim();
    }
    
    public String getEspecificacionId() {
        return especificacionId;
    }

    public void setEspecificacionId(String especificacionId) {
        this.especificacionId = especificacionId;
    }

    public short getNormaId() {
        return normaId;
    }

    public void setNormaId(short normaId) {
        this.normaId = normaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (especificacionId != null ? especificacionId.hashCode() : 0);
        hash += (int) normaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEspecificacionPK)) {
            return false;
        }
        CatEspecificacionPK other = (CatEspecificacionPK) object;
        if ((this.especificacionId == null && other.especificacionId != null) || (this.especificacionId != null && !this.especificacionId.equals(other.especificacionId))) {
            return false;
        }
        if (this.normaId != other.normaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatEspecificacionPK[ especificacionId=" + especificacionId + ", normaId=" + normaId + " ]";
    }
    
}
