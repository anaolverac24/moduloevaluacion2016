/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "SUSTANCIA_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SustanciaProyecto.findAll", query = "SELECT s FROM SustanciaProyecto s"),
    @NamedQuery(name = "SustanciaProyecto.findByFolioProyecto", query = "SELECT s FROM SustanciaProyecto s WHERE s.sustanciaProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "SustanciaProyecto.findBySerialProyecto", query = "SELECT s FROM SustanciaProyecto s WHERE s.sustanciaProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "SustanciaProyecto.findBySustProyId", query = "SELECT s FROM SustanciaProyecto s WHERE s.sustanciaProyectoPK.sustProyId = :sustProyId"),
    @NamedQuery(name = "SustanciaProyecto.findByCtunClve", query = "SELECT s FROM SustanciaProyecto s WHERE s.ctunClve = :ctunClve"),
    @NamedQuery(name = "SustanciaProyecto.findBySustanciaCantidadAlmacenada", query = "SELECT s FROM SustanciaProyecto s WHERE s.sustanciaCantidadAlmacenada = :sustanciaCantidadAlmacenada"),
    @NamedQuery(name = "SustanciaProyecto.findByFolioSerial", query = "SELECT e FROM SustanciaProyecto e WHERE e.sustanciaProyectoPK.folioProyecto = :folio and e.sustanciaProyectoPK.serialProyecto = :serial"),
    @NamedQuery(name = "SustanciaProyecto.findBySustanciaPromovente", query = "SELECT s FROM SustanciaProyecto s WHERE s.sustanciaPromovente = :sustanciaPromovente")})
public class SustanciaProyecto implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sustanciaProyecto")
    private List<SustanciaAnexo> sustanciaAnexoList;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SustanciaProyectoPK sustanciaProyectoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CTUN_CLVE")
    private short ctunClve;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUSTANCIA_CANTIDAD_ALMACENADA")
    private BigDecimal sustanciaCantidadAlmacenada;
    @Size(max = 300)
    @Column(name = "SUSTANCIA_PROMOVENTE")
    private String sustanciaPromovente;
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID")
    @ManyToOne
    private CatEtapa etapaId;
    @JoinColumn(name = "SUSTANCIA_ID", referencedColumnName = "SUSTANCIA_ID")
    @ManyToOne(optional = false)
    private CatSustanciaAltamRiesgosa sustanciaId;

    public SustanciaProyecto() {
    }

    public SustanciaProyecto(SustanciaProyectoPK sustanciaProyectoPK) {
        this.sustanciaProyectoPK = sustanciaProyectoPK;
    }

    public SustanciaProyecto(SustanciaProyectoPK sustanciaProyectoPK, short ctunClve) {
        this.sustanciaProyectoPK = sustanciaProyectoPK;
        this.ctunClve = ctunClve;
    }

    public SustanciaProyecto(String folioProyecto, short serialProyecto, short sustProyId) {
        this.sustanciaProyectoPK = new SustanciaProyectoPK(folioProyecto, serialProyecto, sustProyId);
    }

    public SustanciaProyectoPK getSustanciaProyectoPK() {
        return sustanciaProyectoPK;
    }

    public void setSustanciaProyectoPK(SustanciaProyectoPK sustanciaProyectoPK) {
        this.sustanciaProyectoPK = sustanciaProyectoPK;
    }

    public short getCtunClve() {
        return ctunClve;
    }

    public void setCtunClve(short ctunClve) {
        this.ctunClve = ctunClve;
    }

    public BigDecimal getSustanciaCantidadAlmacenada() {
        return sustanciaCantidadAlmacenada;
    }

    public void setSustanciaCantidadAlmacenada(BigDecimal sustanciaCantidadAlmacenada) {
        this.sustanciaCantidadAlmacenada = sustanciaCantidadAlmacenada;
    }

    public String getSustanciaPromovente() {
        return sustanciaPromovente;
    }

    public void setSustanciaPromovente(String sustanciaPromovente) {
        this.sustanciaPromovente = sustanciaPromovente;
    }

    public CatEtapa getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(CatEtapa etapaId) {
        this.etapaId = etapaId;
    }

    public CatSustanciaAltamRiesgosa getSustanciaId() {
        return sustanciaId;
    }

    public void setSustanciaId(CatSustanciaAltamRiesgosa sustanciaId) {
        this.sustanciaId = sustanciaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sustanciaProyectoPK != null ? sustanciaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SustanciaProyecto)) {
            return false;
        }
        SustanciaProyecto other = (SustanciaProyecto) object;
        if ((this.sustanciaProyectoPK == null && other.sustanciaProyectoPK != null) || (this.sustanciaProyectoPK != null && !this.sustanciaProyectoPK.equals(other.sustanciaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelEmia.SustanciaProyecto[ sustanciaProyectoPK=" + sustanciaProyectoPK + " ]";
    }

    @XmlTransient
    public List<SustanciaAnexo> getSustanciaAnexoList() {
        return sustanciaAnexoList;
    }

    public void setSustanciaAnexoList(List<SustanciaAnexo> sustanciaAnexoList) {
        this.sustanciaAnexoList = sustanciaAnexoList;
    }
    
}
