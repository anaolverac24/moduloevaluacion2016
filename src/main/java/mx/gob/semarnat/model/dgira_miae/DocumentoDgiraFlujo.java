/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import mx.gob.semarnat.model.seguridad.CatRol;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "DOCUMENTO_DGIRA_FLUJO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoDgiraFlujo.findAll", query = "SELECT d FROM DocumentoDgiraFlujo d"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByBitacoraProyecto", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFlujoPK.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByClaveTramite", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFlujoPK.claveTramite = :claveTramite"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByIdTramite", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFlujoPK.idTramite = :idTramite"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByTipoDocId", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFlujoPK.tipoDocId = :tipoDocId"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByIdEntidadFederativa", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFlujoPK.idEntidadFederativa = :idEntidadFederativa"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByDocumentoId", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFlujoPK.documentoId = :documentoId"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByIdAreaEnvio", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.idAreaEnvio = :idAreaEnvio"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByIdAreaRecibe", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.idAreaRecibe = :idAreaRecibe"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByIdUsuarioEnvio", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.idUsuarioEnvio = :idUsuarioEnvio"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByIdUsuarioRecibe", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.idUsuarioRecibe = :idUsuarioRecibe"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByDocumentoDgiraFechaEnvio", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFechaEnvio = :documentoDgiraFechaEnvio"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByDocumentoDgiraFechaRecibe", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFechaRecibe = :documentoDgiraFechaRecibe"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByDocumentoDgiraObservacion", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraObservacion = :documentoDgiraObservacion"),
    @NamedQuery(name = "DocumentoDgiraFlujo.findByDocumentoDgiraPK", query = "SELECT d FROM DocumentoDgiraFlujo d WHERE d.documentoDgiraFlujoPK.bitacoraProyecto = :bitacoraProyecto AND d.documentoDgiraFlujoPK.tipoDocId = :tipoDocId")})
public class DocumentoDgiraFlujo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DocumentoDgiraFlujoPK documentoDgiraFlujoPK;
    @Size(max = 8)
    @Column(name = "ID_AREA_ENVIO")
    private String idAreaEnvio;
    @Size(max = 8)
    @Column(name = "ID_AREA_RECIBE")
    private String idAreaRecibe;
    @Column(name = "ID_USUARIO_ENVIO")
    private Integer idUsuarioEnvio;
    @Column(name = "ID_USUARIO_RECIBE")
    private Integer idUsuarioRecibe;
    @Column(name = "DOCUMENTO_DGIRA_FECHA_ENVIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date documentoDgiraFechaEnvio;
    @Column(name = "DOCUMENTO_DGIRA_FECHA_RECIBE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date documentoDgiraFechaRecibe;
    @Size(max = 4000)
    @Column(name = "DOCUMENTO_DGIRA_OBSERVACION")
    private String documentoDgiraObservacion;
    @JoinColumn(name = "ESTATUS_DOCUMENTO_ID", referencedColumnName = "ESTATUS_DOCUMENTO_ID")
    @ManyToOne
    private CatEstatusDocumento estatusDocumentoId;
    @JoinColumn(name = "ROL_ID", referencedColumnName = "ROL_ID")
    @ManyToOne
    private CatRol rolId;
    @JoinColumn(name = "BITACORA_PROYECTO", referencedColumnName = "BITACORA_PROYECTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private EvaluacionProyecto evaluacionProyecto;

    public DocumentoDgiraFlujo() {
    }

    public DocumentoDgiraFlujo(DocumentoDgiraFlujoPK documentoDgiraFlujoPK) {
        this.documentoDgiraFlujoPK = documentoDgiraFlujoPK;
    }

    public DocumentoDgiraFlujo(String bitacoraProyecto, String claveTramite, int idTramite, short tipoDocId, String idEntidadFederativa, String documentoId) {
        this.documentoDgiraFlujoPK = new DocumentoDgiraFlujoPK(bitacoraProyecto, claveTramite, idTramite, tipoDocId, idEntidadFederativa, documentoId);
    }

    public DocumentoDgiraFlujoPK getDocumentoDgiraFlujoPK() {
        return documentoDgiraFlujoPK;
    }

    public void setDocumentoDgiraFlujoPK(DocumentoDgiraFlujoPK documentoDgiraFlujoPK) {
        this.documentoDgiraFlujoPK = documentoDgiraFlujoPK;
    }

    public String getIdAreaEnvio() {
        return idAreaEnvio;
    }

    public void setIdAreaEnvio(String idAreaEnvio) {
        this.idAreaEnvio = idAreaEnvio;
    }

    public String getIdAreaRecibe() {
        return idAreaRecibe;
    }

    public void setIdAreaRecibe(String idAreaRecibe) {
        this.idAreaRecibe = idAreaRecibe;
    }

    public Integer getIdUsuarioEnvio() {
        return idUsuarioEnvio;
    }

    public void setIdUsuarioEnvio(Integer idUsuarioEnvio) {
        this.idUsuarioEnvio = idUsuarioEnvio;
    }

    public Integer getIdUsuarioRecibe() {
        return idUsuarioRecibe;
    }

    public void setIdUsuarioRecibe(Integer idUsuarioRecibe) {
        this.idUsuarioRecibe = idUsuarioRecibe;
    }

    public Date getDocumentoDgiraFechaEnvio() {
        return documentoDgiraFechaEnvio;
    }

    public void setDocumentoDgiraFechaEnvio(Date documentoDgiraFechaEnvio) {
        this.documentoDgiraFechaEnvio = documentoDgiraFechaEnvio;
    }

    public Date getDocumentoDgiraFechaRecibe() {
        return documentoDgiraFechaRecibe;
    }

    public void setDocumentoDgiraFechaRecibe(Date documentoDgiraFechaRecibe) {
        this.documentoDgiraFechaRecibe = documentoDgiraFechaRecibe;
    }

    public String getDocumentoDgiraObservacion() {
        return documentoDgiraObservacion;
    }

    public void setDocumentoDgiraObservacion(String documentoDgiraObservacion) {
        this.documentoDgiraObservacion = documentoDgiraObservacion;
    }

    public CatEstatusDocumento getEstatusDocumentoId() {
        return estatusDocumentoId;
    }

    public void setEstatusDocumentoId(CatEstatusDocumento estatusDocumentoId) {
        this.estatusDocumentoId = estatusDocumentoId;
    }

    public CatRol getRolId() {
        return rolId;
    }

    public void setRolId(CatRol rolId) {
        this.rolId = rolId;
    }
    
    public EvaluacionProyecto getEvaluacionProyecto() {
        return evaluacionProyecto;
    }

    public void setEvaluacionProyecto(EvaluacionProyecto evaluacionProyecto) {
        this.evaluacionProyecto = evaluacionProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentoDgiraFlujoPK != null ? documentoDgiraFlujoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoDgiraFlujo)) {
            return false;
        }
        DocumentoDgiraFlujo other = (DocumentoDgiraFlujo) object;
        if ((this.documentoDgiraFlujoPK == null && other.documentoDgiraFlujoPK != null) || (this.documentoDgiraFlujoPK != null && !this.documentoDgiraFlujoPK.equals(other.documentoDgiraFlujoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.DocumentoDgiraFlujo[ documentoDgiraFlujoPK=" + documentoDgiraFlujoPK + " ]";
    }
    
}
