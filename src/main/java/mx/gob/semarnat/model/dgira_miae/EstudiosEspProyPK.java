/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class EstudiosEspProyPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_SERIAL")
    private String folioSerial;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "ESTUDIO_ID")
    private short estudioId;

    public EstudiosEspProyPK() {
    }

    public EstudiosEspProyPK(String folioSerial, short serialProyecto, short estudioId) {
        this.folioSerial = folioSerial;
        this.serialProyecto = serialProyecto;
        this.estudioId = estudioId;
    }

    public String getFolioSerial() {
        return folioSerial;
    }

    public void setFolioSerial(String folioSerial) {
        this.folioSerial = folioSerial;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(short estudioId) {
        this.estudioId = estudioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioSerial != null ? folioSerial.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) estudioId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudiosEspProyPK)) {
            return false;
        }
        EstudiosEspProyPK other = (EstudiosEspProyPK) object;
        if ((this.folioSerial == null && other.folioSerial != null) || (this.folioSerial != null && !this.folioSerial.equals(other.folioSerial))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.estudioId != other.estudioId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.EstudiosEspProyPK[ folioSerial=" + folioSerial + ", serialProyecto=" + serialProyecto + ", estudioId=" + estudioId + " ]";
    }
    
}
