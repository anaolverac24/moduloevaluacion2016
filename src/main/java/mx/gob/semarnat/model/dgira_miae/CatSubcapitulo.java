/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_SUBCAPITULO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatSubcapitulo.findAll", query = "SELECT c FROM CatSubcapitulo c"),
    @NamedQuery(name = "CatSubcapitulo.findByIdTramite", query = "SELECT c FROM CatSubcapitulo c WHERE c.catSubcapituloPK.idTramite = :idTramite"),
    @NamedQuery(name = "CatSubcapitulo.findByNsub", query = "SELECT c FROM CatSubcapitulo c WHERE c.catSubcapituloPK.nsub = :nsub"),
    @NamedQuery(name = "CatSubcapitulo.findByCapituloId", query = "SELECT c FROM CatSubcapitulo c WHERE c.catSubcapituloPK.capituloId = :capituloId"),
    @NamedQuery(name = "CatSubcapitulo.findBySubcapituloId", query = "SELECT c FROM CatSubcapitulo c WHERE c.catSubcapituloPK.subcapituloId = :subcapituloId"),
    @NamedQuery(name = "CatSubcapitulo.findBySucbapituloDescripcion", query = "SELECT c FROM CatSubcapitulo c WHERE c.sucbapituloDescripcion = :sucbapituloDescripcion"),
    @NamedQuery(name = "CatSubcapitulo.findBySupcapituloAyuda", query = "SELECT c FROM CatSubcapitulo c WHERE c.supcapituloAyuda = :supcapituloAyuda")})
public class CatSubcapitulo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatSubcapituloPK catSubcapituloPK;
    @Size(max = 100)
    @Column(name = "SUCBAPITULO_DESCRIPCION")
    private String sucbapituloDescripcion;
    @Size(max = 4000)
    @Column(name = "SUPCAPITULO_AYUDA")
    private String supcapituloAyuda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catSubcapitulo")
    private Collection<CatSeccion> catSeccionCollection;
    @JoinColumns({
        @JoinColumn(name = "ID_TRAMITE", referencedColumnName = "ID_TRAMITE", insertable = false, updatable = false),
        @JoinColumn(name = "NSUB", referencedColumnName = "NSUB", insertable = false, updatable = false),
        @JoinColumn(name = "CAPITULO_ID", referencedColumnName = "CAPITULO_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CatCapitulo catCapitulo;

    public CatSubcapitulo() {
    }

    public CatSubcapitulo(CatSubcapituloPK catSubcapituloPK) {
        this.catSubcapituloPK = catSubcapituloPK;
    }

    public CatSubcapitulo(int idTramite, short nsub, short capituloId, short subcapituloId) {
        this.catSubcapituloPK = new CatSubcapituloPK(idTramite, nsub, capituloId, subcapituloId);
    }

    public CatSubcapituloPK getCatSubcapituloPK() {
        return catSubcapituloPK;
    }

    public void setCatSubcapituloPK(CatSubcapituloPK catSubcapituloPK) {
        this.catSubcapituloPK = catSubcapituloPK;
    }

    public String getSucbapituloDescripcion() {
        return sucbapituloDescripcion;
    }

    public void setSucbapituloDescripcion(String sucbapituloDescripcion) {
        this.sucbapituloDescripcion = sucbapituloDescripcion;
    }

    public String getSupcapituloAyuda() {
        return supcapituloAyuda;
    }

    public void setSupcapituloAyuda(String supcapituloAyuda) {
        this.supcapituloAyuda = supcapituloAyuda;
    }

    @XmlTransient
    public Collection<CatSeccion> getCatSeccionCollection() {
        return catSeccionCollection;
    }

    public void setCatSeccionCollection(Collection<CatSeccion> catSeccionCollection) {
        this.catSeccionCollection = catSeccionCollection;
    }

    public CatCapitulo getCatCapitulo() {
        return catCapitulo;
    }

    public void setCatCapitulo(CatCapitulo catCapitulo) {
        this.catCapitulo = catCapitulo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catSubcapituloPK != null ? catSubcapituloPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatSubcapitulo)) {
            return false;
        }
        CatSubcapitulo other = (CatSubcapitulo) object;
        if ((this.catSubcapituloPK == null && other.catSubcapituloPK != null) || (this.catSubcapituloPK != null && !this.catSubcapituloPK.equals(other.catSubcapituloPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatSubcapitulo[ catSubcapituloPK=" + catSubcapituloPK + " ]";
    }
    
}
