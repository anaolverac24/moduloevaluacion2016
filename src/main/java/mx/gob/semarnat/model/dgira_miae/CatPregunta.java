/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_PREGUNTA", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPregunta.findAll", query = "SELECT c FROM CatPregunta c"),
    @NamedQuery(name = "CatPregunta.findByPreguntaId", query = "SELECT c FROM CatPregunta c WHERE c.preguntaId = :preguntaId"),
    @NamedQuery(name = "CatPregunta.findByPreguntaDescripcion", query = "SELECT c FROM CatPregunta c WHERE c.preguntaDescripcion = :preguntaDescripcion"),
    @NamedQuery(name = "CatPregunta.findByPreguntaContinua", query = "SELECT c FROM CatPregunta c WHERE c.preguntaContinua = :preguntaContinua")})
public class CatPregunta implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catPregunta")
    private List<PreguntaProyecto> preguntaProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PREGUNTA_ID")
    private Short preguntaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "PREGUNTA_DESCRIPCION")
    private String preguntaDescripcion;
    @Column(name = "PREGUNTA_CONTINUA")
    private Character preguntaContinua;
    @JoinColumn(name = "NORMA_ID", referencedColumnName = "NORMA_ID")
    @ManyToOne(optional = false)
    private CatNorma normaId;

    public CatPregunta() {
    }

    public CatPregunta(Short preguntaId) {
        this.preguntaId = preguntaId;
    }

    public CatPregunta(Short preguntaId, String preguntaDescripcion) {
        this.preguntaId = preguntaId;
        this.preguntaDescripcion = preguntaDescripcion;
    }

    public Short getPreguntaId() {
        return preguntaId;
    }

    public void setPreguntaId(Short preguntaId) {
        this.preguntaId = preguntaId;
    }

    public String getPreguntaDescripcion() {
        return preguntaDescripcion;
    }

    public void setPreguntaDescripcion(String preguntaDescripcion) {
        this.preguntaDescripcion = preguntaDescripcion;
    }

    public Character getPreguntaContinua() {
        return preguntaContinua;
    }

    public void setPreguntaContinua(Character preguntaContinua) {
        this.preguntaContinua = preguntaContinua;
    }

    public CatNorma getNormaId() {
        return normaId;
    }

    public void setNormaId(CatNorma normaId) {
        this.normaId = normaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaId != null ? preguntaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPregunta)) {
            return false;
        }
        CatPregunta other = (CatPregunta) object;
        if ((this.preguntaId == null && other.preguntaId != null) || (this.preguntaId != null && !this.preguntaId.equals(other.preguntaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatPregunta[ preguntaId=" + preguntaId + " ]";
    }

    @XmlTransient
    public List<PreguntaProyecto> getPreguntaProyectoList() {
        return preguntaProyectoList;
    }

    public void setPreguntaProyectoList(List<PreguntaProyecto> preguntaProyectoList) {
        this.preguntaProyectoList = preguntaProyectoList;
    }
    
}
