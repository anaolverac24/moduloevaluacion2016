/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "TRAMITE_SUBSECTOR", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TramiteSubsector.findAll", query = "SELECT t FROM TramiteSubsector t"),
    @NamedQuery(name = "TramiteSubsector.findByIdTramite", query = "SELECT t FROM TramiteSubsector t WHERE t.tramiteSubsectorPK.idTramite = :idTramite"),
    @NamedQuery(name = "TramiteSubsector.findByClaveTramite", query = "SELECT t FROM TramiteSubsector t WHERE t.claveTramite = :claveTramite"),
    @NamedQuery(name = "TramiteSubsector.findByNsub", query = "SELECT t FROM TramiteSubsector t WHERE t.tramiteSubsectorPK.nsub = :nsub")})
public class TramiteSubsector implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TramiteSubsectorPK tramiteSubsectorPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CLAVE_TRAMITE")
    private String claveTramite;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tramiteSubsector")
    private Collection<CatCapitulo> catCapituloCollection;

    public TramiteSubsector() {
    }

    public TramiteSubsector(TramiteSubsectorPK tramiteSubsectorPK) {
        this.tramiteSubsectorPK = tramiteSubsectorPK;
    }

    public TramiteSubsector(TramiteSubsectorPK tramiteSubsectorPK, String claveTramite) {
        this.tramiteSubsectorPK = tramiteSubsectorPK;
        this.claveTramite = claveTramite;
    }

    public TramiteSubsector(int idTramite, short nsub) {
        this.tramiteSubsectorPK = new TramiteSubsectorPK(idTramite, nsub);
    }

    public TramiteSubsectorPK getTramiteSubsectorPK() {
        return tramiteSubsectorPK;
    }

    public void setTramiteSubsectorPK(TramiteSubsectorPK tramiteSubsectorPK) {
        this.tramiteSubsectorPK = tramiteSubsectorPK;
    }

    public String getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(String claveTramite) {
        this.claveTramite = claveTramite;
    }

    @XmlTransient
    public Collection<CatCapitulo> getCatCapituloCollection() {
        return catCapituloCollection;
    }

    public void setCatCapituloCollection(Collection<CatCapitulo> catCapituloCollection) {
        this.catCapituloCollection = catCapituloCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tramiteSubsectorPK != null ? tramiteSubsectorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TramiteSubsector)) {
            return false;
        }
        TramiteSubsector other = (TramiteSubsector) object;
        if ((this.tramiteSubsectorPK == null && other.tramiteSubsectorPK != null) || (this.tramiteSubsectorPK != null && !this.tramiteSubsectorPK.equals(other.tramiteSubsectorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.TramiteSubsector[ tramiteSubsectorPK=" + tramiteSubsectorPK + " ]";
    }
    
}
