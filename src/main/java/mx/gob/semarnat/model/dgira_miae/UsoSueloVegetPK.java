/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class UsoSueloVegetPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NUM_FOLIO")
    private String numFolio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CVE_PROY")
    private String cveProy;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CVE_AREA")
    private String cveArea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VERSION")
    private short version;

    public UsoSueloVegetPK() {
    }

    public UsoSueloVegetPK(String numFolio, String cveProy, String cveArea, short version) {
        this.numFolio = numFolio;
        this.cveProy = cveProy;
        this.cveArea = cveArea;
        this.version = version;
    }

    public String getNumFolio() {
        return numFolio;
    }

    public void setNumFolio(String numFolio) {
        this.numFolio = numFolio;
    }

    public String getCveProy() {
        return cveProy;
    }

    public void setCveProy(String cveProy) {
        this.cveProy = cveProy;
    }

    public String getCveArea() {
        return cveArea;
    }

    public void setCveArea(String cveArea) {
        this.cveArea = cveArea;
    }

    public short getVersion() {
        return version;
    }

    public void setVersion(short version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numFolio != null ? numFolio.hashCode() : 0);
        hash += (cveProy != null ? cveProy.hashCode() : 0);
        hash += (cveArea != null ? cveArea.hashCode() : 0);
        hash += (int) version;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsoSueloVegetPK)) {
            return false;
        }
        UsoSueloVegetPK other = (UsoSueloVegetPK) object;
        if ((this.numFolio == null && other.numFolio != null) || (this.numFolio != null && !this.numFolio.equals(other.numFolio))) {
            return false;
        }
        if ((this.cveProy == null && other.cveProy != null) || (this.cveProy != null && !this.cveProy.equals(other.cveProy))) {
            return false;
        }
        if ((this.cveArea == null && other.cveArea != null) || (this.cveArea != null && !this.cveArea.equals(other.cveArea))) {
            return false;
        }
        if (this.version != other.version) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.UsoSueloVegetPK[ numFolio=" + numFolio + ", cveProy=" + cveProy + ", cveArea=" + cveArea + ", version=" + version + " ]";
    }
    
}
