/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

/**
 *
 * @author Admin
 */
public class PreguntaMineralHelper {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private PreguntaMineral model;

    public PreguntaMineralHelper() {
    }

    public PreguntaMineralHelper(Integer id, PreguntaMineral model) {
        this.id = id;
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PreguntaMineral getModel() {
        return model;
    }

    public void setModel(PreguntaMineral model) {
        this.model = model;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PreguntaMineralHelper)) {
            return false;
        }
        PreguntaMineralHelper ob = (PreguntaMineralHelper) obj;
        if (this.id != ob.id) {
            return false;
        }
        return true;
    }
}
