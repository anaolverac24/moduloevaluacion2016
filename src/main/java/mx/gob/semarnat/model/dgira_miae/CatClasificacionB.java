/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author efrainc
 *
 */
@Entity
@Table(name = "CAT_CLASIFICACION_B")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatClasificacionB.findAll", query = "SELECT c FROM CatClasificacionB c"),
    @NamedQuery(name = "CatClasificacionB.findByClasificacionId", query = "SELECT c FROM CatClasificacionB c WHERE c.clasificacionId = :clasificacionId"),
    @NamedQuery(name = "CatClasificacionB.findByClasificacionBId", query = "SELECT c FROM CatClasificacionB c WHERE c.clasificacionBId = :clasificacionBId"),
    @NamedQuery(name = "CatClasificacionB.findByClasificacionB", query = "SELECT c FROM CatClasificacionB c WHERE c.clasificacionB = :clasificacionB")})
public class CatClasificacionB implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CLASIFICACION_B_ID")
    private Short clasificacionBId;
    @Column(name = "CLASIFICACION_ID")
    private Short clasificacionId;
    @Column(name = "CLASIFICACION_B")
    private String clasificacionB;

    public CatClasificacionB() {
    }

    public CatClasificacionB(Short clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public Short getClasificacionId() {
        return clasificacionId;
    }

	public void setClasificacionId(Short clasificacionId) {
        this.clasificacionId = clasificacionId;
    }
	
    public Short getClasificacionBId() {
		return clasificacionBId;
	}

	public void setClasificacionBId(Short clasificacionBId) {
		this.clasificacionBId = clasificacionBId;
	}

    public String getClasificacionB() {
        return clasificacionB;
    }

    public void setClasificacionB(String clasificacionB) {
        this.clasificacionB = clasificacionB;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clasificacionBId != null ? clasificacionBId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CatClasificacionB)) {
            return false;
        }
        CatClasificacionB other = (CatClasificacionB) object;
        if ((this.clasificacionBId == null && other.clasificacionBId != null) || (this.clasificacionBId != null && !this.clasificacionBId.equals(other.clasificacionBId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatClasificacionB[ clasificacionBId=" + clasificacionBId + " ]";
    }
    
}
