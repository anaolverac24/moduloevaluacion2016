/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "OE_GRAL_TERRIT", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OeGralTerrit.findAll", query = "SELECT o FROM OeGralTerrit o"),
    @NamedQuery(name = "OeGralTerrit.findByNumFolio", query = "SELECT o FROM OeGralTerrit o WHERE o.oeGralTerritPK.numFolio = :numFolio"),
    @NamedQuery(name = "OeGralTerrit.findByCveProy", query = "SELECT o FROM OeGralTerrit o WHERE o.oeGralTerritPK.cveProy = :cveProy"),
    @NamedQuery(name = "OeGralTerrit.findByCveArea", query = "SELECT o FROM OeGralTerrit o WHERE o.oeGralTerritPK.cveArea = :cveArea"),
    @NamedQuery(name = "OeGralTerrit.findByVersion", query = "SELECT o FROM OeGralTerrit o WHERE o.oeGralTerritPK.version = :version"),
    @NamedQuery(name = "OeGralTerrit.findByRegion", query = "SELECT o FROM OeGralTerrit o WHERE o.region = :region"),
    @NamedQuery(name = "OeGralTerrit.findByUab", query = "SELECT o FROM OeGralTerrit o WHERE o.uab = :uab"),
    @NamedQuery(name = "OeGralTerrit.findByNom", query = "SELECT o FROM OeGralTerrit o WHERE o.nom = :nom"),
    @NamedQuery(name = "OeGralTerrit.findByPolitCve", query = "SELECT o FROM OeGralTerrit o WHERE o.politCve = :politCve"),
    @NamedQuery(name = "OeGralTerrit.findByPolitica", query = "SELECT o FROM OeGralTerrit o WHERE o.politica = :politica"),
    @NamedQuery(name = "OeGralTerrit.findByAapNom", query = "SELECT o FROM OeGralTerrit o WHERE o.aapNom = :aapNom"),
    @NamedQuery(name = "OeGralTerrit.findByRector", query = "SELECT o FROM OeGralTerrit o WHERE o.rector = :rector"),
    @NamedQuery(name = "OeGralTerrit.findByCoadyuvant", query = "SELECT o FROM OeGralTerrit o WHERE o.coadyuvant = :coadyuvant"),
    @NamedQuery(name = "OeGralTerrit.findByAsociados", query = "SELECT o FROM OeGralTerrit o WHERE o.asociados = :asociados"),
    @NamedQuery(name = "OeGralTerrit.findByOtrosSect", query = "SELECT o FROM OeGralTerrit o WHERE o.otrosSect = :otrosSect"),
    @NamedQuery(name = "OeGralTerrit.findByPob2010", query = "SELECT o FROM OeGralTerrit o WHERE o.pob2010 = :pob2010"),
    @NamedQuery(name = "OeGralTerrit.findByRegIndig", query = "SELECT o FROM OeGralTerrit o WHERE o.regIndig = :regIndig"),
    @NamedQuery(name = "OeGralTerrit.findByEdoActual", query = "SELECT o FROM OeGralTerrit o WHERE o.edoActual = :edoActual"),
    @NamedQuery(name = "OeGralTerrit.findByCtoPla20", query = "SELECT o FROM OeGralTerrit o WHERE o.ctoPla20 = :ctoPla20"),
    @NamedQuery(name = "OeGralTerrit.findByMedPla20", query = "SELECT o FROM OeGralTerrit o WHERE o.medPla20 = :medPla20"),
    @NamedQuery(name = "OeGralTerrit.findByLarPla20", query = "SELECT o FROM OeGralTerrit o WHERE o.larPla20 = :larPla20"),
    @NamedQuery(name = "OeGralTerrit.findByEstrategia", query = "SELECT o FROM OeGralTerrit o WHERE o.estrategia = :estrategia"),
    @NamedQuery(name = "OeGralTerrit.findBySupEa", query = "SELECT o FROM OeGralTerrit o WHERE o.supEa = :supEa"),
    @NamedQuery(name = "OeGralTerrit.findByProy", query = "SELECT o FROM OeGralTerrit o WHERE o.proy = :proy"),
    @NamedQuery(name = "OeGralTerrit.findByComp", query = "SELECT o FROM OeGralTerrit o WHERE o.comp = :comp"),
    @NamedQuery(name = "OeGralTerrit.findByDescrip", query = "SELECT o FROM OeGralTerrit o WHERE o.descrip = :descrip"),
    @NamedQuery(name = "OeGralTerrit.findByAreabuffer", query = "SELECT o FROM OeGralTerrit o WHERE o.areabuffer = :areabuffer"),
    @NamedQuery(name = "OeGralTerrit.findByArea", query = "SELECT o FROM OeGralTerrit o WHERE o.area = :area"),
    @NamedQuery(name = "OeGralTerrit.findByFechaHora", query = "SELECT o FROM OeGralTerrit o WHERE o.fechaHora = :fechaHora"),
    @NamedQuery(name = "OeGralTerrit.findByIdr", query = "SELECT o FROM OeGralTerrit o WHERE o.oeGralTerritPK.idr = :idr")})
public class OeGralTerrit implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OeGralTerritPK oeGralTerritPK;
    @Size(max = 80)
    @Column(name = "REGION")
    private String region;
    @Column(name = "UAB")
    private BigInteger uab;
    @Size(max = 180)
    @Column(name = "NOM")
    private String nom;
    @Column(name = "POLIT_CVE")
    private BigInteger politCve;
    @Size(max = 180)
    @Column(name = "POLITICA")
    private String politica;
    @Size(max = 180)
    @Column(name = "AAP_NOM")
    private String aapNom;
    @Size(max = 180)
    @Column(name = "RECTOR")
    private String rector;
    @Size(max = 80)
    @Column(name = "COADYUVANT")
    private String coadyuvant;
    @Size(max = 80)
    @Column(name = "ASOCIADOS")
    private String asociados;
    @Size(max = 80)
    @Column(name = "OTROS_SECT")
    private String otrosSect;
    @Size(max = 80)
    @Column(name = "POB_2010")
    private String pob2010;
    @Size(max = 80)
    @Column(name = "REG_INDIG")
    private String regIndig;
    @Size(max = 80)
    @Column(name = "EDO_ACTUAL")
    private String edoActual;
    @Size(max = 80)
    @Column(name = "CTO_PLA_20")
    private String ctoPla20;
    @Size(max = 80)
    @Column(name = "MED_PLA_20")
    private String medPla20;
    @Size(max = 80)
    @Column(name = "LAR_PLA_20")
    private String larPla20;
    @Size(max = 254)
    @Column(name = "ESTRATEGIA")
    private String estrategia;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Size(max = 80)
    @Column(name = "PROY")
    private String proy;
    @Size(max = 80)
    @Column(name = "COMP")
    private String comp;
    @Size(max = 80)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public OeGralTerrit() {
    }

    public OeGralTerrit(OeGralTerritPK oeGralTerritPK) {
        this.oeGralTerritPK = oeGralTerritPK;
    }

    public OeGralTerrit(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.oeGralTerritPK = new OeGralTerritPK(numFolio, cveProy, cveArea, version, idr);
    }

    public OeGralTerritPK getOeGralTerritPK() {
        return oeGralTerritPK;
    }

    public void setOeGralTerritPK(OeGralTerritPK oeGralTerritPK) {
        this.oeGralTerritPK = oeGralTerritPK;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public BigInteger getUab() {
        return uab;
    }

    public void setUab(BigInteger uab) {
        this.uab = uab;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public BigInteger getPolitCve() {
        return politCve;
    }

    public void setPolitCve(BigInteger politCve) {
        this.politCve = politCve;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

    public String getAapNom() {
        return aapNom;
    }

    public void setAapNom(String aapNom) {
        this.aapNom = aapNom;
    }

    public String getRector() {
        return rector;
    }

    public void setRector(String rector) {
        this.rector = rector;
    }

    public String getCoadyuvant() {
        return coadyuvant;
    }

    public void setCoadyuvant(String coadyuvant) {
        this.coadyuvant = coadyuvant;
    }

    public String getAsociados() {
        return asociados;
    }

    public void setAsociados(String asociados) {
        this.asociados = asociados;
    }

    public String getOtrosSect() {
        return otrosSect;
    }

    public void setOtrosSect(String otrosSect) {
        this.otrosSect = otrosSect;
    }

    public String getPob2010() {
        return pob2010;
    }

    public void setPob2010(String pob2010) {
        this.pob2010 = pob2010;
    }

    public String getRegIndig() {
        return regIndig;
    }

    public void setRegIndig(String regIndig) {
        this.regIndig = regIndig;
    }

    public String getEdoActual() {
        return edoActual;
    }

    public void setEdoActual(String edoActual) {
        this.edoActual = edoActual;
    }

    public String getCtoPla20() {
        return ctoPla20;
    }

    public void setCtoPla20(String ctoPla20) {
        this.ctoPla20 = ctoPla20;
    }

    public String getMedPla20() {
        return medPla20;
    }

    public void setMedPla20(String medPla20) {
        this.medPla20 = medPla20;
    }

    public String getLarPla20() {
        return larPla20;
    }

    public void setLarPla20(String larPla20) {
        this.larPla20 = larPla20;
    }

    public String getEstrategia() {
        return estrategia;
    }

    public void setEstrategia(String estrategia) {
        this.estrategia = estrategia;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oeGralTerritPK != null ? oeGralTerritPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OeGralTerrit)) {
            return false;
        }
        OeGralTerrit other = (OeGralTerrit) object;
        if ((this.oeGralTerritPK == null && other.oeGralTerritPK != null) || (this.oeGralTerritPK != null && !this.oeGralTerritPK.equals(other.oeGralTerritPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.OeGralTerrit[ oeGralTerritPK=" + oeGralTerritPK + " ]";
    }
    
}
