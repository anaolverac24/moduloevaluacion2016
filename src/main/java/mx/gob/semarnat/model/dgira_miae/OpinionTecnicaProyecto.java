/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import mx.go.semarnat.services.UnidadAdministrativa;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "OPINION_TECNICA_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OpinionTecnicaProyecto.findAll", query = "SELECT o FROM OpinionTecnicaProyecto o")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByBitacoraProyecto", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opinionTecnicaProyectoPK.bitacoraProyecto = :bitacoraProyecto")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontId", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opinionTecnicaProyectoPK.opiniontId = :opiniontId")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontDependenciaSiglas", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opiniontDependenciaSiglas = :opiniontDependenciaSiglas")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontDependenciaDesc", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opiniontDependenciaDesc = :opiniontDependenciaDesc")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontOficioNumero", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opiniontOficioNumero = :opiniontOficioNumero")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontOficioFecha", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opiniontOficioFecha = :opiniontOficioFecha")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontMotivoConsulta", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opiniontMotivoConsulta = :opiniontMotivoConsulta")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontOpinionSolicitada", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opiniontOpinionSolicitada = :opiniontOpinionSolicitada")
    ,
    @NamedQuery(name = "OpinionTecnicaProyecto.findByOpiniontEnviaRespuesta", query = "SELECT o FROM OpinionTecnicaProyecto o WHERE o.opiniontEnviaRespuesta = :opiniontEnviaRespuesta")})
public class OpinionTecnicaProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OpinionTecnicaProyectoPK opinionTecnicaProyectoPK;
    @Size(max = 100)
    @Column(name = "OPINIONT_DEPENDENCIA_SIGLAS")
    private String opiniontDependenciaSiglas;
    @Size(max = 300)
    @Column(name = "OPINIONT_DEPENDENCIA_DESC")
    private String opiniontDependenciaDesc;
    @Size(max = 100)
    @Column(name = "OPINIONT_OFICIO_NUMERO")
    private String opiniontOficioNumero;
    @Column(name = "OPINIONT_OFICIO_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date opiniontOficioFecha;
    @Size(max = 4000)
    @Column(name = "OPINIONT_MOTIVO_CONSULTA")
    private String opiniontMotivoConsulta;
    @Size(max = 4000)
    @Column(name = "OPINIONT_OPINION_SOLICITADA")
    private String opiniontOpinionSolicitada;
    @Size(max = 1)
    @Column(name = "OPINIONT_ENVIA_RESPUESTA")
    private String opiniontEnviaRespuesta;
    @JoinColumn(name = "CAT_DEPENDENCIA_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private UnidadAdministrativa catDependenciaId;
    @Size(max = 4000)
    @Column(name = "OPINIONT_OBSERVACION")
    private String opiniontObservacion; //  
    @Size(max = 31)
    @Column(name = "OPINIONT_OFICIO_NUMERO_R")
    private String opiniontOficioNumeroR;
    @Column(name = "OPINIONT_OFICIO_FECHA_R")
    @Temporal(TemporalType.TIMESTAMP)
    private Date opiniontOficioFechaR;

    @Size(max = 4000)
    @Column(name = "OPINIONT_RESPUESTA")
    private String opiniontRespuesta;

    @Size(max = 4000)
    @Column(name = "OPINIONT_DGIRA")
    private String opiniontDgira;

    @Column(name = "OPINIONT_SELECCION")
    private int opiniontSeleccion;

    @Column(name = "OPINIONT_OFICIO")
    private int opiniontOficio;
    
    
    public OpinionTecnicaProyecto() {
    }

    public OpinionTecnicaProyecto(OpinionTecnicaProyectoPK opinionTecnicaProyectoPK) {
        this.opinionTecnicaProyectoPK = opinionTecnicaProyectoPK;
    }

    public OpinionTecnicaProyecto(String bitacoraProyecto, short opiniontId) {
        this.opinionTecnicaProyectoPK = new OpinionTecnicaProyectoPK(bitacoraProyecto, opiniontId);
    }

    public OpinionTecnicaProyectoPK getOpinionTecnicaProyectoPK() {
        return opinionTecnicaProyectoPK;
    }

    public void setOpinionTecnicaProyectoPK(OpinionTecnicaProyectoPK opinionTecnicaProyectoPK) {
        this.opinionTecnicaProyectoPK = opinionTecnicaProyectoPK;
    }

    public String getOpiniontDependenciaSiglas() {
        return opiniontDependenciaSiglas;
    }

    public void setOpiniontDependenciaSiglas(String opiniontDependenciaSiglas) {
        this.opiniontDependenciaSiglas = opiniontDependenciaSiglas;
    }

    public String getOpiniontDependenciaDesc() {
        return opiniontDependenciaDesc;
    }

    public void setOpiniontDependenciaDesc(String opiniontDependenciaDesc) {
        this.opiniontDependenciaDesc = opiniontDependenciaDesc;
    }

    public String getOpiniontOficioNumero() {
        return opiniontOficioNumero;
    }

    public void setOpiniontOficioNumero(String opiniontOficioNumero) {
        this.opiniontOficioNumero = opiniontOficioNumero;
    }

    public Date getOpiniontOficioFecha() {
        return opiniontOficioFecha;
    }

    public void setOpiniontOficioFecha(Date opiniontOficioFecha) {
        this.opiniontOficioFecha = opiniontOficioFecha;
    }

    public String getOpiniontMotivoConsulta() {
        return opiniontMotivoConsulta;
    }

    public void setOpiniontMotivoConsulta(String opiniontMotivoConsulta) {
        this.opiniontMotivoConsulta = opiniontMotivoConsulta;
    }

    public String getOpiniontOpinionSolicitada() {
        return opiniontOpinionSolicitada;
    }

    public void setOpiniontOpinionSolicitada(String opiniontOpinionSolicitada) {
        this.opiniontOpinionSolicitada = opiniontOpinionSolicitada;
    }

    public String getOpiniontEnviaRespuesta() {
        return opiniontEnviaRespuesta;
    }

    public void setOpiniontEnviaRespuesta(String opiniontEnviaRespuesta) {
        this.opiniontEnviaRespuesta = opiniontEnviaRespuesta;
    }

    public UnidadAdministrativa getCatDependenciaId() {
        return catDependenciaId;
    }

    public void setCatDependenciaId(UnidadAdministrativa catDependenciaId) {
        this.catDependenciaId = catDependenciaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (opinionTecnicaProyectoPK != null ? opinionTecnicaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OpinionTecnicaProyecto)) {
            return false;
        }
        OpinionTecnicaProyecto other = (OpinionTecnicaProyecto) object;
        if ((this.opinionTecnicaProyectoPK == null && other.opinionTecnicaProyectoPK != null) || (this.opinionTecnicaProyectoPK != null && !this.opinionTecnicaProyectoPK.equals(other.opinionTecnicaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.OpinionTecnicaProyecto[ opinionTecnicaProyectoPK=" + opinionTecnicaProyectoPK + " ]";
    }

    /**
     * @return the opiniontObservacion
     */
    public String getOpiniontObservacion() {
        return opiniontObservacion;
    }

    /**
     * @param opiniontObservacion the opiniontObservacion to set
     */
    public void setOpiniontObservacion(String opiniontObservacion) {
        this.opiniontObservacion = opiniontObservacion;
    }

    /**
     * @return the opiniontOficioNumeroR
     */
    public String getOpiniontOficioNumeroR() {
        return opiniontOficioNumeroR;
    }

    /**
     * @param opiniontOficioNumeroR the opiniontOficioNumeroR to set
     */
    public void setOpiniontOficioNumeroR(String opiniontOficioNumeroR) {
        this.opiniontOficioNumeroR = opiniontOficioNumeroR;
    }

    /**
     * @return the opiniontOficioFechaR
     */
    public Date getOpiniontOficioFechaR() {
        return opiniontOficioFechaR;
    }

    /**
     * @param opiniontOficioFechaR the opiniontOficioFechaR to set
     */
    public void setOpiniontOficioFechaR(Date opiniontOficioFechaR) {
        this.opiniontOficioFechaR = opiniontOficioFechaR;
    }

    /**
     * @return the opiniontRespuesta
     */
    public String getOpiniontRespuesta() {
        return opiniontRespuesta;
    }

    /**
     * @param opiniontRespuesta the opiniontRespuesta to set
     */
    public void setOpiniontRespuesta(String opiniontRespuesta) {
        this.opiniontRespuesta = opiniontRespuesta;
    }

    /**
     * @return the opiniontDgira
     */
    public String getOpiniontDgira() {
        return opiniontDgira;
    }

    /**
     * @param opiniontDgira the opiniontDgira to set
     */
    public void setOpiniontDgira(String opiniontDgira) {
        this.opiniontDgira = opiniontDgira;
    }

    public int getOpiniontSeleccion() {
        return opiniontSeleccion;
    }

    public void setOpiniontSeleccion(int opiniontSeleccion) {
        this.opiniontSeleccion = opiniontSeleccion;
    }

    /**
     * @return the opiniontOficio
     */
    public int getOpiniontOficio() {
        return opiniontOficio;
    }

    /**
     * @param opiniontOficio the opiniontOficio to set
     */
    public void setOpiniontOficio(int opiniontOficio) {
        this.opiniontOficio = opiniontOficio;
    }
    
    

}
