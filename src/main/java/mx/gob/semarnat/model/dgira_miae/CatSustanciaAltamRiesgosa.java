/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_SUSTANCIA_ALTAM_RIESGOSA", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatSustanciaAltamRiesgosa.findAll", query = "SELECT c FROM CatSustanciaAltamRiesgosa c"),
    @NamedQuery(name = "CatSustanciaAltamRiesgosa.findBySustanciaId", query = "SELECT c FROM CatSustanciaAltamRiesgosa c WHERE c.sustanciaId = :sustanciaId"),
    @NamedQuery(name = "CatSustanciaAltamRiesgosa.findBySustanciaDescripcion", query = "SELECT c FROM CatSustanciaAltamRiesgosa c WHERE c.sustanciaDescripcion = :sustanciaDescripcion"),
    @NamedQuery(name = "CatSustanciaAltamRiesgosa.findBySustanciaCantidad", query = "SELECT c FROM CatSustanciaAltamRiesgosa c WHERE c.sustanciaCantidad = :sustanciaCantidad"),
    @NamedQuery(name = "CatSustanciaAltamRiesgosa.findBySustanciaUnidad", query = "SELECT c FROM CatSustanciaAltamRiesgosa c WHERE c.sustanciaUnidad = :sustanciaUnidad"),
    @NamedQuery(name = "CatSustanciaAltamRiesgosa.findBySustanciaEstado", query = "SELECT c FROM CatSustanciaAltamRiesgosa c WHERE c.sustanciaEstado = :sustanciaEstado")})
public class CatSustanciaAltamRiesgosa implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sustanciaId")
    private List<SustanciaProyecto> sustanciaProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUSTANCIA_ID")
    private Short sustanciaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "SUSTANCIA_DESCRIPCION")
    private String sustanciaDescripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUSTANCIA_CANTIDAD")
    private BigDecimal sustanciaCantidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "SUSTANCIA_UNIDAD")
    private String sustanciaUnidad;
    @Size(max = 100)
    @Column(name = "SUSTANCIA_ESTADO")
    private String sustanciaEstado;

    public CatSustanciaAltamRiesgosa() {
    }

    public CatSustanciaAltamRiesgosa(Short sustanciaId) {
        this.sustanciaId = sustanciaId;
    }

    public CatSustanciaAltamRiesgosa(Short sustanciaId, String sustanciaDescripcion, BigDecimal sustanciaCantidad, String sustanciaUnidad) {
        this.sustanciaId = sustanciaId;
        this.sustanciaDescripcion = sustanciaDescripcion;
        this.sustanciaCantidad = sustanciaCantidad;
        this.sustanciaUnidad = sustanciaUnidad;
    }

    public Short getSustanciaId() {
        return sustanciaId;
    }

    public void setSustanciaId(Short sustanciaId) {
        this.sustanciaId = sustanciaId;
    }

    public String getSustanciaDescripcion() {
        return sustanciaDescripcion;
    }

    public void setSustanciaDescripcion(String sustanciaDescripcion) {
        this.sustanciaDescripcion = sustanciaDescripcion;
    }

    public BigDecimal getSustanciaCantidad() {
        return sustanciaCantidad;
    }

    public void setSustanciaCantidad(BigDecimal sustanciaCantidad) {
        this.sustanciaCantidad = sustanciaCantidad;
    }

    public String getSustanciaUnidad() {
        return sustanciaUnidad;
    }

    public void setSustanciaUnidad(String sustanciaUnidad) {
        this.sustanciaUnidad = sustanciaUnidad;
    }

    public String getSustanciaEstado() {
        return sustanciaEstado;
    }

    public void setSustanciaEstado(String sustanciaEstado) {
        this.sustanciaEstado = sustanciaEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sustanciaId != null ? sustanciaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatSustanciaAltamRiesgosa)) {
            return false;
        }
        CatSustanciaAltamRiesgosa other = (CatSustanciaAltamRiesgosa) object;
        if ((this.sustanciaId == null && other.sustanciaId != null) || (this.sustanciaId != null && !this.sustanciaId.equals(other.sustanciaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return sustanciaDescripcion;
    }

    @XmlTransient
    public List<SustanciaProyecto> getSustanciaProyectoList() {
        return sustanciaProyectoList;
    }

    public void setSustanciaProyectoList(List<SustanciaProyecto> sustanciaProyectoList) {
        this.sustanciaProyectoList = sustanciaProyectoList;
    }
    
}
