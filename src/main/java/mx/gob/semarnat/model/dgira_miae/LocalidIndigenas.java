/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "LOCALID_INDIGENAS", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocalidIndigenas.findAll", query = "SELECT l FROM LocalidIndigenas l")})
public class LocalidIndigenas implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LocalidIndigenasPK localidIndigenasPK;
    @Column(name = "CLAVE")
    private String clave;
    @Column(name = "NOM_LOC")
    private String nomLoc;
    @Column(name = "NOM_ENT")
    private String nomEnt;
    @Column(name = "NOM_MUN")
    private String nomMun;
    @Column(name = "LATITUD")
    private String latitud;
    @Column(name = "LONGITUD")
    private String longitud;
    @Column(name = "ALTITUD")
    private String altitud;
    @Column(name = "POBMAS")
    private String pobmas;
    @Column(name = "POBFEM")
    private String pobfem;
    @Column(name = "TOTVIVIEND")
    private String totviviend;
    @Column(name = "POBTOT2")
    private BigInteger pobtot2;
    @Column(name = "AMBITO")
    private String ambito;
    @Column(name = "NOMTIPO")
    private String nomtipo;
    @Column(name = "TIPOLOC")
    private String tipoloc;
    @Column(name = "GM_2010")
    private String gm2010;
    @Column(name = "POB_INDI")
    private BigInteger pobIndi;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "localidIndigenas")
//    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public LocalidIndigenas() {
    }

    public LocalidIndigenas(LocalidIndigenasPK localidIndigenasPK) {
        this.localidIndigenasPK = localidIndigenasPK;
    }

    public LocalidIndigenas(String numFolio, String cveProy, String cveArea, short version) {
        this.localidIndigenasPK = new LocalidIndigenasPK(numFolio, cveProy, cveArea, version);
    }

    public LocalidIndigenasPK getLocalidIndigenasPK() {
        return localidIndigenasPK;
    }

    public void setLocalidIndigenasPK(LocalidIndigenasPK localidIndigenasPK) {
        this.localidIndigenasPK = localidIndigenasPK;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNomLoc() {
        return nomLoc;
    }

    public void setNomLoc(String nomLoc) {
        this.nomLoc = nomLoc;
    }

    public String getNomEnt() {
        return nomEnt;
    }

    public void setNomEnt(String nomEnt) {
        this.nomEnt = nomEnt;
    }

    public String getNomMun() {
        return nomMun;
    }

    public void setNomMun(String nomMun) {
        this.nomMun = nomMun;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getAltitud() {
        return altitud;
    }

    public void setAltitud(String altitud) {
        this.altitud = altitud;
    }

    public String getPobmas() {
        return pobmas;
    }

    public void setPobmas(String pobmas) {
        this.pobmas = pobmas;
    }

    public String getPobfem() {
        return pobfem;
    }

    public void setPobfem(String pobfem) {
        this.pobfem = pobfem;
    }

    public String getTotviviend() {
        return totviviend;
    }

    public void setTotviviend(String totviviend) {
        this.totviviend = totviviend;
    }

    public BigInteger getPobtot2() {
        return pobtot2;
    }

    public void setPobtot2(BigInteger pobtot2) {
        this.pobtot2 = pobtot2;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getNomtipo() {
        return nomtipo;
    }

    public void setNomtipo(String nomtipo) {
        this.nomtipo = nomtipo;
    }

    public String getTipoloc() {
        return tipoloc;
    }

    public void setTipoloc(String tipoloc) {
        this.tipoloc = tipoloc;
    }

    public String getGm2010() {
        return gm2010;
    }

    public void setGm2010(String gm2010) {
        this.gm2010 = gm2010;
    }

    public BigInteger getPobIndi() {
        return pobIndi;
    }

    public void setPobIndi(BigInteger pobIndi) {
        this.pobIndi = pobIndi;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

//    @XmlTransient
//    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
//        return sigeiaAnalisisList;
//    }
//
//    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
//        this.sigeiaAnalisisList = sigeiaAnalisisList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (localidIndigenasPK != null ? localidIndigenasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalidIndigenas)) {
            return false;
        }
        LocalidIndigenas other = (LocalidIndigenas) object;
        if ((this.localidIndigenasPK == null && other.localidIndigenasPK != null) || (this.localidIndigenasPK != null && !this.localidIndigenasPK.equals(other.localidIndigenasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.LocalidIndigenas[ localidIndigenasPK=" + localidIndigenasPK + " ]";
    }
    
}
