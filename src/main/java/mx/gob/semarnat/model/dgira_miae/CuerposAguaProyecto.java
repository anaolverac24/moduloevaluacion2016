/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * @author efrainc
 *
 */
@Entity
@Table(name = "CUERPOS_AGUA_PROYECTO" , schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuerposAguaProyecto.findAll", query = "SELECT p FROM CuerposAguaProyecto p"),
    @NamedQuery(name = "CuerposAguaProyecto.findByFolioProyecto", query = "SELECT p FROM CuerposAguaProyecto p WHERE p.cuerposAguaProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "CuerposAguaProyecto.findBySerialProyecto", query = "SELECT p FROM CuerposAguaProyecto p WHERE p.cuerposAguaProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "CuerposAguaProyecto.findByCuerposaguaId", query = "SELECT p FROM CuerposAguaProyecto p WHERE p.cuerposAguaProyectoPK.cuerposAguaId = :cuerposAguaId"),
    @NamedQuery(name = "CuerposAguaProyecto.findByNombre", query = "SELECT p FROM CuerposAguaProyecto p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "CuerposAguaProyecto.findByTipo", query = "SELECT p FROM CuerposAguaProyecto p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "CuerposAguaProyecto.findByDistancia", query = "SELECT p FROM CuerposAguaProyecto p WHERE p.distancia = :distancia")})
public class CuerposAguaProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CuerposAguaProyectoPK cuerposAguaProyectoPK;
    @Column(name = "CUERPOS_AGUA_NOMBRE")
    private String nombre;
    @JoinColumn(name = "CUERPOS_AGUA_TIPO", referencedColumnName = "CLASIFICACION_B_ID")
    @ManyToOne(optional = false)
    private CatClasificacionB tipo;
    @Column(name = "CUERPOS_AGUA_DISTANCIA")
    private String distancia;
    
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public CuerposAguaProyecto() {
    }

    public CuerposAguaProyecto(CuerposAguaProyectoPK cuerposAguaProyectoPK) {
        this.cuerposAguaProyectoPK = cuerposAguaProyectoPK;
    }

    public CuerposAguaProyecto(String folioProyecto, short serialProyecto, short cuerposAguaId) {
        this.cuerposAguaProyectoPK = new  CuerposAguaProyectoPK(folioProyecto, serialProyecto, cuerposAguaId);
    }

	public CuerposAguaProyectoPK getCuerposAguaProyectoPK() {
		return cuerposAguaProyectoPK;
	}

	public void setCuerposAguaProyectoPK(CuerposAguaProyectoPK cuerposAguaProyectoPK) {
		this.cuerposAguaProyectoPK = cuerposAguaProyectoPK;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public CatClasificacionB getTipo() {
		return tipo;
	}

	public void setTipo(CatClasificacionB tipo) {
		this.tipo = tipo;
	}

	public String getDistancia() {
		return distancia;
	}

	public void setDistancia(String distancia) {
		this.distancia = distancia;
	}
	
	public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (cuerposAguaProyectoPK != null ? cuerposAguaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        
        if (!(object instanceof CuerposAguaProyecto)) {
            return false;
        }
        CuerposAguaProyecto other = (CuerposAguaProyecto) object;
        if ((this.cuerposAguaProyectoPK == null && other.cuerposAguaProyectoPK != null) || (this.cuerposAguaProyectoPK != null && !this.cuerposAguaProyectoPK.equals(other.cuerposAguaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CuerposAguaProyecto[ cuerposAguaProyectoPK=" + cuerposAguaProyectoPK + " ]";
    }
    
}
