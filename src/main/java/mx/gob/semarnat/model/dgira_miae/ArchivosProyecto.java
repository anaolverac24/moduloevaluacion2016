/**
 * 
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Cesar
 *
 */
@Entity
@Table(name = "ARCHIVOS_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
public class ArchivosProyecto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "FOLIO_SERIAL")
	private String folioSerial;
	
	@Column(name = "SERIAL_PROYECTO")
	private short serialProyecto;
	
	@Column(name = "CAPITULO_ID")
	private short capituloId;
	
	@Column(name = "SUBCAPITULO_ID")
	private short subCapituloId;
	
	@Column(name = "SECCION_ID")
	private short seccionId;
	
	@Column(name = "APARTADO_ID")
	private short apartadoId;
	
	@Id
    @GeneratedValue(generator="InvSeq") 
    @SequenceGenerator(name="InvSeq",sequenceName="SEQ_ID_ARCHIVO") 
	@Column(name = "SEQ_ID")
	private short seqId;
	
	@Column(name = "ID")
	private short id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "FILENAME")
	private String fileName;
	
	@Column(name = "URL")
	private String url;
	
	@Column(name = "TAMANIO")
	private BigDecimal tamanio;
	
	@Column(name = "EXTENSION")
	private String extension;
	
	@Column(name = "RESERVADO")
	private String reservado;
	
	/**
	 * Constructor por defatult
	 */
	public ArchivosProyecto() {		
	}
	
	/**
	 * Constructor con llaves
	 * @param folioProyecto
	 * @param serialProyecto
	 * @param capituloId
	 * @param subCapituloId
	 * @param seccionId
	 * @param apartadoId
	 */
	public ArchivosProyecto(String folioProyecto, short serialProyecto, short capituloId, short subCapituloId, short seccionId, 
			short apartadoId) {
		this.folioSerial = folioProyecto;
		this.serialProyecto = serialProyecto;
		this.capituloId = capituloId;
		this.subCapituloId = subCapituloId;
		this.seccionId = seccionId;
		this.apartadoId = apartadoId;
	}

	/**
	 * @return the folioSerial
	 */
	public String getFolioSerial() {
		return folioSerial;
	}

	/**
	 * @param folioSerial the folioSerial to set
	 */
	public void setFolioSerial(String folioSerial) {
		this.folioSerial = folioSerial;
	}

	/**
	 * @return the capituloId
	 */
	public short getCapituloId() {
		return capituloId;
	}

	/**
	 * @param capituloId the capituloId to set
	 */
	public void setCapituloId(short capituloId) {
		this.capituloId = capituloId;
	}

	/**
	 * @return the apartadoId
	 */
	public short getApartadoId() {
		return apartadoId;
	}

	/**
	 * @param apartadoId the apartadoId to set
	 */
	public void setApartadoId(short apartadoId) {
		this.apartadoId = apartadoId;
	}

	/**
	 * @return the seqId
	 */
	public short getSeqId() {
		return seqId;
	}

	/**
	 * @param seqId the seqId to set
	 */
	public void setSeqId(short seqId) {
		this.seqId = seqId;
	}

	/**
	 * @return the id
	 */
	public short getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(short id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the tamanio
	 */
	public BigDecimal getTamanio() {
		return tamanio;
	}

	/**
	 * @param tamanio the tamanio to set
	 */
	public void setTamanio(BigDecimal tamanio) {
		this.tamanio = tamanio;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the reservado
	 */
	public String getReservado() {
		return reservado;
	}

	/**
	 * @param reservado the reservado to set
	 */
	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}

	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}

	/**
	 * @return the subCapituloId
	 */
	public short getSubCapituloId() {
		return subCapituloId;
	}

	/**
	 * @param subCapituloId the subCapituloId to set
	 */
	public void setSubCapituloId(short subCapituloId) {
		this.subCapituloId = subCapituloId;
	}

	/**
	 * @return the seccionId
	 */
	public short getSeccionId() {
		return seccionId;
	}

	/**
	 * @param seccionId the seccionId to set
	 */
	public void setSeccionId(short seccionId) {
		this.seccionId = seccionId;
	}
}
