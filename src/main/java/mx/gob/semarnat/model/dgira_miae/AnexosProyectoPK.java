/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class AnexosProyectoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "CAPITULO_ID")
    private short capituloId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUBCAPITULO_ID")
    private short subcapituloId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SECCION_ID")
    private short seccionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APARTADO_ID")
    private short apartadoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ANEXO_ID")
    private short anexoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;

    public AnexosProyectoPK() {
    }

    public AnexosProyectoPK(short capituloId, short subcapituloId, short seccionId, short apartadoId, short anexoId, String folioProyecto, short serialProyecto) {
        this.capituloId = capituloId;
        this.subcapituloId = subcapituloId;
        this.seccionId = seccionId;
        this.apartadoId = apartadoId;
        this.anexoId = anexoId;
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
    }

    public short getCapituloId() {
        return capituloId;
    }

    public void setCapituloId(short capituloId) {
        this.capituloId = capituloId;
    }

    public short getSubcapituloId() {
        return subcapituloId;
    }

    public void setSubcapituloId(short subcapituloId) {
        this.subcapituloId = subcapituloId;
    }

    public short getSeccionId() {
        return seccionId;
    }

    public void setSeccionId(short seccionId) {
        this.seccionId = seccionId;
    }

    public short getApartadoId() {
        return apartadoId;
    }

    public void setApartadoId(short apartadoId) {
        this.apartadoId = apartadoId;
    }

    public short getAnexoId() {
        return anexoId;
    }

    public void setAnexoId(short anexoId) {
        this.anexoId = anexoId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) capituloId;
        hash += (int) subcapituloId;
        hash += (int) seccionId;
        hash += (int) apartadoId;
        hash += (int) anexoId;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnexosProyectoPK)) {
            return false;
        }
        AnexosProyectoPK other = (AnexosProyectoPK) object;
        if (this.capituloId != other.capituloId) {
            return false;
        }
        if (this.subcapituloId != other.subcapituloId) {
            return false;
        }
        if (this.seccionId != other.seccionId) {
            return false;
        }
        if (this.apartadoId != other.apartadoId) {
            return false;
        }
        if (this.anexoId != other.anexoId) {
            return false;
        }
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.AnexosProyectoPK[ capituloId=" + capituloId + ", subcapituloId=" + subcapituloId + ", seccionId=" + seccionId + ", apartadoId=" + apartadoId + ", anexoId=" + anexoId + ", folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + " ]";
    }
    
}
