/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "SUSTANCIA_ANEXO", catalog = "", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SustanciaAnexo.findAll", query = "SELECT s FROM SustanciaAnexo s"),
    @NamedQuery(name = "SustanciaAnexo.findBySustanciaProyId", query = "SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.sustanciaProyId = :sustanciaProyId"),
    @NamedQuery(name = "SustanciaAnexo.findByAnexoUrl", query = "SELECT s FROM SustanciaAnexo s WHERE s.anexoUrl = :anexoUrl"),
    @NamedQuery(name = "SustanciaAnexo.findByAnexoDesc", query = "SELECT s FROM SustanciaAnexo s WHERE s.anexoDesc = :anexoDesc"),
    @NamedQuery(name = "SustanciaAnexo.findByAnexoNombre", query = "SELECT s FROM SustanciaAnexo s WHERE s.anexoNombre = :anexoNombre"),
    @NamedQuery(name = "SustanciaAnexo.findByAnexoTamanio", query = "SELECT s FROM SustanciaAnexo s WHERE s.anexoTamanio = :anexoTamanio"),
    @NamedQuery(name = "SustanciaAnexo.findByAnexoExtension", query = "SELECT s FROM SustanciaAnexo s WHERE s.anexoExtension = :anexoExtension"),
    @NamedQuery(name = "SustanciaAnexo.findBySustanciaAnexoId", query = "SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.sustanciaAnexoId = :sustanciaAnexoId"),
    @NamedQuery(name = "SustanciaAnexo.findByFolioProyecto", query = "SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "SustanciaAnexo.findBySerialProyecto", query = "SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.serialProyecto = :serialProyecto")})
public class SustanciaAnexo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SustanciaAnexoPK sustanciaAnexoPK;
    @Size(max = 300)
    @Column(name = "ANEXO_URL")
    private String anexoUrl;
    @Size(max = 1000)
    @Column(name = "ANEXO_DESC")
    private String anexoDesc;
    @Size(max = 300)
    @Column(name = "ANEXO_NOMBRE")
    private String anexoNombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ANEXO_TAMANIO")
    private BigDecimal anexoTamanio;
    @Size(max = 30)
    @Column(name = "ANEXO_EXTENSION")
    private String anexoExtension;
    @JoinColumns({
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SUSTANCIA_PROY_ID", referencedColumnName = "SUST_PROY_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private SustanciaProyecto sustanciaProyecto;

    public SustanciaAnexo() {
    }

    public SustanciaAnexo(SustanciaAnexoPK sustanciaAnexoPK) {
        this.sustanciaAnexoPK = sustanciaAnexoPK;
    }

    public SustanciaAnexo(short sustanciaProyId, short sustanciaAnexoId, String folioProyecto, short serialProyecto) {
        this.sustanciaAnexoPK = new SustanciaAnexoPK(sustanciaProyId, sustanciaAnexoId, folioProyecto, serialProyecto);
    }

    public SustanciaAnexoPK getSustanciaAnexoPK() {
        return sustanciaAnexoPK;
    }

    public void setSustanciaAnexoPK(SustanciaAnexoPK sustanciaAnexoPK) {
        this.sustanciaAnexoPK = sustanciaAnexoPK;
    }

    public String getAnexoUrl() {
        return anexoUrl;
    }

    public void setAnexoUrl(String anexoUrl) {
        this.anexoUrl = anexoUrl;
    }

    public String getAnexoDesc() {
        return anexoDesc;
    }

    public void setAnexoDesc(String anexoDesc) {
        this.anexoDesc = anexoDesc;
    }

    public String getAnexoNombre() {
        return anexoNombre;
    }

    public void setAnexoNombre(String anexoNombre) {
        this.anexoNombre = anexoNombre;
    }

    public BigDecimal getAnexoTamanio() {
        return anexoTamanio;
    }

    public void setAnexoTamanio(BigDecimal anexoTamanio) {
        this.anexoTamanio = anexoTamanio;
    }

    public String getAnexoExtension() {
        return anexoExtension;
    }

    public void setAnexoExtension(String anexoExtension) {
        this.anexoExtension = anexoExtension;
    }

    public SustanciaProyecto getSustanciaProyecto() {
        return sustanciaProyecto;
    }

    public void setSustanciaProyecto(SustanciaProyecto sustanciaProyecto) {
        this.sustanciaProyecto = sustanciaProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sustanciaAnexoPK != null ? sustanciaAnexoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SustanciaAnexo)) {
            return false;
        }
        SustanciaAnexo other = (SustanciaAnexo) object;
        if ((this.sustanciaAnexoPK == null && other.sustanciaAnexoPK != null) || (this.sustanciaAnexoPK != null && !this.sustanciaAnexoPK.equals(other.sustanciaAnexoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.SustanciaAnexo[ sustanciaAnexoPK=" + sustanciaAnexoPK + " ]";
    }
    
}
