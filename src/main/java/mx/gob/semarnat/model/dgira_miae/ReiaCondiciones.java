/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_CONDICIONES", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaCondiciones.findAll", query = "SELECT r FROM ReiaCondiciones r"),
    @NamedQuery(name = "ReiaCondiciones.findByIdCondicion", query = "SELECT r FROM ReiaCondiciones r WHERE r.idCondicion = :idCondicion"),
    @NamedQuery(name = "ReiaCondiciones.findByCondicion", query = "SELECT r FROM ReiaCondiciones r WHERE r.condicion = :condicion"),
    @NamedQuery(name = "ReiaCondiciones.findByRespuesta", query = "SELECT r FROM ReiaCondiciones r WHERE r.respuesta = :respuesta"),
    @NamedQuery(name = "ReiaCondiciones.findByInciso", query = "SELECT r FROM ReiaCondiciones r WHERE r.inciso = :inciso"),
    @NamedQuery(name = "ReiaCondiciones.findByAntecedente", query = "SELECT r FROM ReiaCondiciones r WHERE r.antecedente = :antecedente")})
public class ReiaCondiciones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_CONDICION")
    private Long idCondicion;
    @Column(name = "CONDICION")
    private String condicion;
    @Column(name = "RESPUESTA")
    private Long respuesta;
    @Column(name = "INCISO")
    private Long inciso;
    @Column(name = "ANTECEDENTE")
    private Long antecedente;
    
        
    
    /*
    @OneToMany(mappedBy = "idCondicion")
    private Collection<ReiaObrasCondiciones> reiaObrasCondicionesCollection;
*/
    public ReiaCondiciones() {
    }

    public ReiaCondiciones(Long idCondicion) {
        this.idCondicion = idCondicion;
    }

    public Long getIdCondicion() {
        return idCondicion;
    }

    public void setIdCondicion(Long idCondicion) {
        this.idCondicion = idCondicion;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public Long getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Long respuesta) {
        this.respuesta = respuesta;
    }

    public Long getInciso() {
        return inciso;
    }

    public void setInciso(Long inciso) {
        this.inciso = inciso;
    }

    public Long getAntecedente() {
        return antecedente;
    }

    public void setAntecedente(Long antecedente) {
        this.antecedente = antecedente;
    }

 /*   @XmlTransient
    public Collection<ReiaObrasCondiciones> getReiaObrasCondicionesCollection() {
        return reiaObrasCondicionesCollection;
    }

    public void setReiaObrasCondicionesCollection(Collection<ReiaObrasCondiciones> reiaObrasCondicionesCollection) {
        this.reiaObrasCondicionesCollection = reiaObrasCondicionesCollection;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCondicion != null ? idCondicion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReiaCondiciones)) {
            return false;
        }
        ReiaCondiciones other = (ReiaCondiciones) object;
        if ((this.idCondicion == null && other.idCondicion != null) || (this.idCondicion != null && !this.idCondicion.equals(other.idCondicion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaCondiciones[ idCondicion=" + idCondicion + " ]";
    }
    
}
