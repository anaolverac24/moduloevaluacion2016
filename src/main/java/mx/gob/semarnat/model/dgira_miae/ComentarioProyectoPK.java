/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Paty
 */
@Embeddable
public class ComentarioProyectoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CAPITULO_ID")
    private short capituloId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUBCAPITULO_ID")
    private short subcapituloId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SECCION_ID")
    private short seccionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APARTADO_ID")
    private short apartadoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMENTARIO_IDUSUARIO")
    private Integer comentarioIdusuario;

    public ComentarioProyectoPK() {
    }

    public ComentarioProyectoPK(String bitacoraProyecto, short capituloId, short subcapituloId, short seccionId, short apartadoId, Integer comentarioIdusuario) {
        this.bitacoraProyecto = bitacoraProyecto;
        this.capituloId = capituloId;
        this.subcapituloId = subcapituloId;
        this.seccionId = seccionId;
        this.apartadoId = apartadoId;
        this.comentarioIdusuario = comentarioIdusuario;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public short getCapituloId() {
        return capituloId;
    }

    public void setCapituloId(short capituloId) {
        this.capituloId = capituloId;
    }

    public short getSubcapituloId() {
        return subcapituloId;
    }

    public void setSubcapituloId(short subcapituloId) {
        this.subcapituloId = subcapituloId;
    }

    public short getSeccionId() {
        return seccionId;
    }

    public void setSeccionId(short seccionId) {
        this.seccionId = seccionId;
    }

    public short getApartadoId() {
        return apartadoId;
    }

    public void setApartadoId(short apartadoId) {
        this.apartadoId = apartadoId;
    }

    public Integer getComentarioIdusuario() {
        return comentarioIdusuario;
    }

    public void setComentarioIdusuario(Integer comentarioIdusuario) {
        this.comentarioIdusuario = comentarioIdusuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacoraProyecto != null ? bitacoraProyecto.hashCode() : 0);
        hash += (int) capituloId;
        hash += (int) subcapituloId;
        hash += (int) seccionId;
        hash += (int) apartadoId;
        hash += (comentarioIdusuario != null ? comentarioIdusuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComentarioProyectoPK)) {
            return false;
        }
        ComentarioProyectoPK other = (ComentarioProyectoPK) object;
        if ((this.bitacoraProyecto == null && other.bitacoraProyecto != null) || (this.bitacoraProyecto != null && !this.bitacoraProyecto.equals(other.bitacoraProyecto))) {
            return false;
        }
        if (this.capituloId != other.capituloId) {
            return false;
        }
        if (this.subcapituloId != other.subcapituloId) {
            return false;
        }
        if (this.seccionId != other.seccionId) {
            return false;
        }
        if (this.apartadoId != other.apartadoId) {
            return false;
        }
        if ((this.comentarioIdusuario == null && other.comentarioIdusuario != null) || (this.comentarioIdusuario != null && !this.comentarioIdusuario.equals(other.comentarioIdusuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.ComentarioProyectoPK[ bitacoraProyecto=" + bitacoraProyecto + ", capituloId=" + capituloId + ", subcapituloId=" + subcapituloId + ", seccionId=" + seccionId + ", apartadoId=" + apartadoId + ", comentarioIdusuario=" + comentarioIdusuario + " ]";
    }
    
}
