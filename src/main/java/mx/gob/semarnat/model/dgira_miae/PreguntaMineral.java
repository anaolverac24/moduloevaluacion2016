/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PREGUNTA_MINERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaMineral.findAll", query = "SELECT p FROM PreguntaMineral p"),
    @NamedQuery(name = "PreguntaMineral.findByFolioProyecto", query = "SELECT p FROM PreguntaMineral p WHERE p.preguntaMineralPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PreguntaMineral.findBySerialProyecto", query = "SELECT p FROM PreguntaMineral p WHERE p.preguntaMineralPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PreguntaMineral.findByPreguntaId", query = "SELECT p FROM PreguntaMineral p WHERE p.preguntaMineralPK.preguntaId = :preguntaId"),
    @NamedQuery(name = "PreguntaMineral.findByIdMinerales", query = "SELECT p FROM PreguntaMineral p WHERE p.preguntaMineralPK.idMinerales = :idMinerales")})
public class PreguntaMineral implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreguntaMineralPK preguntaMineralPK;

    public PreguntaMineral() {
    }

    public PreguntaMineral(PreguntaMineralPK preguntaMineralPK) {
        this.preguntaMineralPK = preguntaMineralPK;
    }

    public PreguntaMineral(String folioProyecto, short serialProyecto, short preguntaId, short idMinerales) {
        this.preguntaMineralPK = new PreguntaMineralPK(folioProyecto, serialProyecto, preguntaId, idMinerales);
    }

    public PreguntaMineralPK getPreguntaMineralPK() {
        return preguntaMineralPK;
    }

    public void setPreguntaMineralPK(PreguntaMineralPK preguntaMineralPK) {
        this.preguntaMineralPK = preguntaMineralPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaMineralPK != null ? preguntaMineralPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaMineral)) {
            return false;
        }
        PreguntaMineral other = (PreguntaMineral) object;
        if ((this.preguntaMineralPK == null && other.preguntaMineralPK != null) || (this.preguntaMineralPK != null && !this.preguntaMineralPK.equals(other.preguntaMineralPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "package mx.gob.semarnat.model.dgira_miae.PreguntaMineral[ preguntaMineralPK=" + preguntaMineralPK + " ]";
    }
    
}
