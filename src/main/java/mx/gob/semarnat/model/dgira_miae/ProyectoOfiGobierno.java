package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "PROYECTO_OFI_GOBIERNO")
@XmlRootElement
public class ProyectoOfiGobierno implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "BITACORA_PROYECTO")
	private String bitacoraProyecto;
	
	@Id
	@Column(name = "DEPENDENCIA_ID NUMBER")
	private long depenciaId;

	public String getBitacoraProyecto() {
		return bitacoraProyecto;
	}

	public ProyectoOfiGobierno() {
	}

	public void setBitacoraProyecto(String bitacoraProyecto) {
		this.bitacoraProyecto = bitacoraProyecto;
	}

	public long getDepenciaId() {
		return depenciaId;
	}

	public void setDepenciaId(long depenciaId) {
		this.depenciaId = depenciaId;
	}

	public ProyectoOfiGobierno(String bitacoraProyecto, long depenciaId) {
		this.bitacoraProyecto = bitacoraProyecto;
		this.depenciaId = depenciaId;
	}
	
}
