package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RESULTANDOS", schema = "DGIRA_MIAE2")
@NamedQueries({ @NamedQuery(name = "Resultando.findAll", query = "SELECT r FROM Resultando r") })
public class Resultando implements Serializable{

	private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "RESULTANDO_ID", nullable = false)
    @SequenceGenerator(name = "SeqResultando", sequenceName = "SEQ_RESULTANDO", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "SeqResultando", strategy = GenerationType.SEQUENCE)
    private Long resultandoId;
    
	@Column(name = "CATEGORIA_DOCUMENTO")
	private String categoriaDoc;
	
	@Column(name = "FOLIO_DOCUMENTO")
	private String folioDoc;
	
	@Column(name = "REFERENCIA")
	private String referencia;
	
	@Column(name = "NOMBRE_REMITENTE")
	private String nombreRemi;
	
	@Column(name = "DEPENDENCIA_REMITENTE")
	private String dependenciaRemi;
	
	@Column(name = "OBSERVACIONES")
	private String observaciones;
	
	@Column(name = "REDACCION")
	private String redaccion;
	
	@Column(name = "ALTA_MANUAL")
	private Boolean altaManual;
	
	@Column(name = "FOLIO_PROYECTO")
	private String folioProy;
	
	@Column(name = "BITACORA_PROYECTO")
	private String bitacoraProy;
	
	@Column(name = "RELATIVO_INGRESO_POYECTO")
	private Boolean relIngresoProy;
	
	@Column(name = "RELATIVO_PUBLICACION_GACETA")
	private Boolean relPublicGaceta;

	@Column(name = "FECHA_ELABORACION_DOCUMENTO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaElaboracionDoc;
	
	@Column(name = "FECHA_REGISTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	
	public Long getResultandoId() {
		return resultandoId;
	}

	public void setResultandoId(Long resultandoId) {
		this.resultandoId = resultandoId;
	}

	public String getCategoriaDoc() {
		return categoriaDoc;
	}

	public void setCategoriaDoc(String categoriaDoc) {
		this.categoriaDoc = categoriaDoc;
	}

	public String getFolioDoc() {
		return folioDoc;
	}

	public void setFolioDoc(String folioDoc) {
		this.folioDoc = folioDoc;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNombreRemi() {
		return nombreRemi;
	}

	public void setNombreRemi(String nombreRemi) {
		this.nombreRemi = nombreRemi;
	}

	public String getDependenciaRemi() {
		return dependenciaRemi;
	}

	public void setDependenciaRemi(String dependenciaRemi) {
		this.dependenciaRemi = dependenciaRemi;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getRedaccion() {
		return redaccion;
	}

	public void setRedaccion(String redaccion) {
		this.redaccion = redaccion;
	}

	public Boolean getAltaManual() {
		return altaManual;
	}

	public void setAltaManual(Boolean altaManual) {
		this.altaManual = altaManual;
	}

	public String getFolioProy() {
		return folioProy;
	}

	public void setFolioProy(String folioProy) {
		this.folioProy = folioProy;
	}

	public String getBitacoraProy() {
		return bitacoraProy;
	}

	public void setBitacoraProy(String bitacoraProy) {
		this.bitacoraProy = bitacoraProy;
	}

	public Boolean getRelIngresoProy() {
		return relIngresoProy;
	}

	public void setRelIngresoProy(Boolean relIngresoProy) {
		this.relIngresoProy = relIngresoProy;
	}

	public Boolean getRelPublicGaceta() {
		return relPublicGaceta;
	}

	public void setRelPublicGaceta(Boolean relPublicGaceta) {
		this.relPublicGaceta = relPublicGaceta;
	}

	public Date getFechaElaboracionDoc() {
		return fechaElaboracionDoc;
	}

	public void setFechaElaboracionDoc(Date fechaElaboracionDoc) {
		this.fechaElaboracionDoc = fechaElaboracionDoc;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

}
