/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class CatCapituloPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TRAMITE")
    private int idTramite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NSUB")
    private short nsub;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CAPITULO_ID")
    private short capituloId;

    public CatCapituloPK() {
    }

    public CatCapituloPK(int idTramite, short nsub, short capituloId) {
        this.idTramite = idTramite;
        this.nsub = nsub;
        this.capituloId = capituloId;
    }

    public int getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(int idTramite) {
        this.idTramite = idTramite;
    }

    public short getNsub() {
        return nsub;
    }

    public void setNsub(short nsub) {
        this.nsub = nsub;
    }

    public short getCapituloId() {
        return capituloId;
    }

    public void setCapituloId(short capituloId) {
        this.capituloId = capituloId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idTramite;
        hash += (int) nsub;
        hash += (int) capituloId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatCapituloPK)) {
            return false;
        }
        CatCapituloPK other = (CatCapituloPK) object;
        if (this.idTramite != other.idTramite) {
            return false;
        }
        if (this.nsub != other.nsub) {
            return false;
        }
        if (this.capituloId != other.capituloId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatCapituloPK[ idTramite=" + idTramite + ", nsub=" + nsub + ", capituloId=" + capituloId + " ]";
    }
    
}
