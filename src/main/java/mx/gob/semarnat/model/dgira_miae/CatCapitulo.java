/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_CAPITULO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatCapitulo.findAll", query = "SELECT c FROM CatCapitulo c"),
    @NamedQuery(name = "CatCapitulo.findByIdTramite", query = "SELECT c FROM CatCapitulo c WHERE c.catCapituloPK.idTramite = :idTramite"),
    @NamedQuery(name = "CatCapitulo.findByNsub", query = "SELECT c FROM CatCapitulo c WHERE c.catCapituloPK.nsub = :nsub"),
    @NamedQuery(name = "CatCapitulo.findByCapituloId", query = "SELECT c FROM CatCapitulo c WHERE c.catCapituloPK.capituloId = :capituloId"),
    @NamedQuery(name = "CatCapitulo.findByCapituloDescripcion", query = "SELECT c FROM CatCapitulo c WHERE c.capituloDescripcion = :capituloDescripcion"),
    @NamedQuery(name = "CatCapitulo.findByCapituloAyuda", query = "SELECT c FROM CatCapitulo c WHERE c.capituloAyuda = :capituloAyuda")})
public class CatCapitulo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatCapituloPK catCapituloPK;
    @Size(max = 300)
    @Column(name = "CAPITULO_DESCRIPCION")
    private String capituloDescripcion;
    @Size(max = 4000)
    @Column(name = "CAPITULO_AYUDA")
    private String capituloAyuda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catCapitulo")
    private Collection<CatSubcapitulo> catSubcapituloCollection;
    @JoinColumns({
        @JoinColumn(name = "ID_TRAMITE", referencedColumnName = "ID_TRAMITE", insertable = false, updatable = false),
        @JoinColumn(name = "NSUB", referencedColumnName = "NSUB", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private TramiteSubsector tramiteSubsector;

    public CatCapitulo() {
    }

    public CatCapitulo(CatCapituloPK catCapituloPK) {
        this.catCapituloPK = catCapituloPK;
    }

    public CatCapitulo(int idTramite, short nsub, short capituloId) {
        this.catCapituloPK = new CatCapituloPK(idTramite, nsub, capituloId);
    }

    public CatCapituloPK getCatCapituloPK() {
        return catCapituloPK;
    }

    public void setCatCapituloPK(CatCapituloPK catCapituloPK) {
        this.catCapituloPK = catCapituloPK;
    }

    public String getCapituloDescripcion() {
        return capituloDescripcion;
    }

    public void setCapituloDescripcion(String capituloDescripcion) {
        this.capituloDescripcion = capituloDescripcion;
    }

    public String getCapituloAyuda() {
        return capituloAyuda;
    }

    public void setCapituloAyuda(String capituloAyuda) {
        this.capituloAyuda = capituloAyuda;
    }

    @XmlTransient
    public Collection<CatSubcapitulo> getCatSubcapituloCollection() {
        return catSubcapituloCollection;
    }

    public void setCatSubcapituloCollection(Collection<CatSubcapitulo> catSubcapituloCollection) {
        this.catSubcapituloCollection = catSubcapituloCollection;
    }

    public TramiteSubsector getTramiteSubsector() {
        return tramiteSubsector;
    }

    public void setTramiteSubsector(TramiteSubsector tramiteSubsector) {
        this.tramiteSubsector = tramiteSubsector;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catCapituloPK != null ? catCapituloPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatCapitulo)) {
            return false;
        }
        CatCapitulo other = (CatCapitulo) object;
        if ((this.catCapituloPK == null && other.catCapituloPK != null) || (this.catCapituloPK != null && !this.catCapituloPK.equals(other.catCapituloPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatCapitulo[ catCapituloPK=" + catCapituloPK + " ]";
    }
    
}
