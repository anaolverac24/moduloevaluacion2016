/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class ImpacAmbProyectoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "IMPAC_AMBPROY_ID")
    private short impacAmbproyId;

    public ImpacAmbProyectoPK() {
    }

    public ImpacAmbProyectoPK(String folioProyecto, short serialProyecto, short impacAmbproyId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.impacAmbproyId = impacAmbproyId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getImpacAmbproyId() {
        return impacAmbproyId;
    }

    public void setImpacAmbproyId(short impacAmbproyId) {
        this.impacAmbproyId = impacAmbproyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) impacAmbproyId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImpacAmbProyectoPK)) {
            return false;
        }
        ImpacAmbProyectoPK other = (ImpacAmbProyectoPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.impacAmbproyId != other.impacAmbproyId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ImpacAmbProyectoPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", impacAmbproyId=" + impacAmbproyId + " ]";
    }
    
}
