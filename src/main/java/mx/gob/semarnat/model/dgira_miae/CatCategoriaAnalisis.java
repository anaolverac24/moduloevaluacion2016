/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "CAT_CATEGORIA_ANALISIS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatCategoriaAnalisis.findAll", query = "SELECT c FROM CatCategoriaAnalisis c"),
    @NamedQuery(name = "CatCategoriaAnalisis.findByCatCategoriaId", query = "SELECT c FROM CatCategoriaAnalisis c WHERE c.catCategoriaId = :catCategoriaId"),
    @NamedQuery(name = "CatCategoriaAnalisis.findByCatCategoriaDescripcion", query = "SELECT c FROM CatCategoriaAnalisis c WHERE c.catCategoriaDescripcion = :catCategoriaDescripcion")})
public class CatCategoriaAnalisis implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CAT_CATEGORIA_ID")
    private Short catCategoriaId;
    @Basic(optional = false)
    @Column(name = "CAT_CATEGORIA_DESCRIPCION")
    private String catCategoriaDescripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catCategoriaId")
    private Collection<CatAnalisisPreliminar> catAnalisisPreliminarCollection;

    public CatCategoriaAnalisis() {
    }

    public CatCategoriaAnalisis(Short catCategoriaId) {
        this.catCategoriaId = catCategoriaId;
    }

    public CatCategoriaAnalisis(Short catCategoriaId, String catCategoriaDescripcion) {
        this.catCategoriaId = catCategoriaId;
        this.catCategoriaDescripcion = catCategoriaDescripcion;
    }

    public Short getCatCategoriaId() {
        return catCategoriaId;
    }

    public void setCatCategoriaId(Short catCategoriaId) {
        this.catCategoriaId = catCategoriaId;
    }

    public String getCatCategoriaDescripcion() {
        return catCategoriaDescripcion;
    }

    public void setCatCategoriaDescripcion(String catCategoriaDescripcion) {
        this.catCategoriaDescripcion = catCategoriaDescripcion;
    }

    @XmlTransient
    public Collection<CatAnalisisPreliminar> getCatAnalisisPreliminarCollection() {
        return catAnalisisPreliminarCollection;
    }

    public void setCatAnalisisPreliminarCollection(Collection<CatAnalisisPreliminar> catAnalisisPreliminarCollection) {
        this.catAnalisisPreliminarCollection = catAnalisisPreliminarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catCategoriaId != null ? catCategoriaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatCategoriaAnalisis)) {
            return false;
        }
        CatCategoriaAnalisis other = (CatCategoriaAnalisis) object;
        if ((this.catCategoriaId == null && other.catCategoriaId != null) || (this.catCategoriaId != null && !this.catCategoriaId.equals(other.catCategoriaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatCategoriaAnalisis[ catCategoriaId=" + catCategoriaId + " ]";
    }
    
}
