/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "USO_SUELO_VEGET", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsoSueloVeget.findAll", query = "SELECT u FROM UsoSueloVeget u"),
    @NamedQuery(name = "UsoSueloVeget.findByNumFolio", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.numFolio = :numFolio"),
    @NamedQuery(name = "UsoSueloVeget.findByCveProy", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.cveProy = :cveProy"),
    @NamedQuery(name = "UsoSueloVeget.findByCveArea", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.cveArea = :cveArea"),
    @NamedQuery(name = "UsoSueloVeget.findByVersion", query = "SELECT u FROM UsoSueloVeget u WHERE u.usoSueloVegetPK.version = :version"),
    @NamedQuery(name = "UsoSueloVeget.findByClave", query = "SELECT u FROM UsoSueloVeget u WHERE u.clave = :clave"),
    @NamedQuery(name = "UsoSueloVeget.findByClavefot", query = "SELECT u FROM UsoSueloVeget u WHERE u.clavefot = :clavefot"),
    @NamedQuery(name = "UsoSueloVeget.findByTipInfo", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipInfo = :tipInfo"),
    @NamedQuery(name = "UsoSueloVeget.findByTipEcov", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipEcov = :tipEcov"),
    @NamedQuery(name = "UsoSueloVeget.findByAgecosis", query = "SELECT u FROM UsoSueloVeget u WHERE u.agecosis = :agecosis"),
    @NamedQuery(name = "UsoSueloVeget.findByTipages", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipages = :tipages"),
    @NamedQuery(name = "UsoSueloVeget.findByTipVeg", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipVeg = :tipVeg"),
    @NamedQuery(name = "UsoSueloVeget.findByDesveg", query = "SELECT u FROM UsoSueloVeget u WHERE u.desveg = :desveg"),
    @NamedQuery(name = "UsoSueloVeget.findByFaseVs", query = "SELECT u FROM UsoSueloVeget u WHERE u.faseVs = :faseVs"),
    @NamedQuery(name = "UsoSueloVeget.findByTipPlan", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipPlan = :tipPlan"),
    @NamedQuery(name = "UsoSueloVeget.findByTipCul1", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipCul1 = :tipCul1"),
    @NamedQuery(name = "UsoSueloVeget.findByTipCul2", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipCul2 = :tipCul2"),
    @NamedQuery(name = "UsoSueloVeget.findByOtros", query = "SELECT u FROM UsoSueloVeget u WHERE u.otros = :otros"),
    @NamedQuery(name = "UsoSueloVeget.findByCUYV", query = "SELECT u FROM UsoSueloVeget u WHERE u.cUYV = :cUYV"),
    @NamedQuery(name = "UsoSueloVeget.findByTipoGen", query = "SELECT u FROM UsoSueloVeget u WHERE u.tipoGen = :tipoGen"),
    @NamedQuery(name = "UsoSueloVeget.findBySupEa", query = "SELECT u FROM UsoSueloVeget u WHERE u.supEa = :supEa"),
    @NamedQuery(name = "UsoSueloVeget.findByProy", query = "SELECT u FROM UsoSueloVeget u WHERE u.proy = :proy"),
    @NamedQuery(name = "UsoSueloVeget.findByComp", query = "SELECT u FROM UsoSueloVeget u WHERE u.comp = :comp"),
    @NamedQuery(name = "UsoSueloVeget.findByDescrip", query = "SELECT u FROM UsoSueloVeget u WHERE u.descrip = :descrip"),
    @NamedQuery(name = "UsoSueloVeget.findByAreabuffer", query = "SELECT u FROM UsoSueloVeget u WHERE u.areabuffer = :areabuffer"),
    @NamedQuery(name = "UsoSueloVeget.findByArea", query = "SELECT u FROM UsoSueloVeget u WHERE u.area = :area"),
    @NamedQuery(name = "UsoSueloVeget.findByFechaHora", query = "SELECT u FROM UsoSueloVeget u WHERE u.fechaHora = :fechaHora"),
    @NamedQuery(name = "UsoSueloVeget.findByFolCveVer", query = "SELECT a FROM UsoSueloVeget a  WHERE a.usoSueloVegetPK.numFolio = :fol and a.usoSueloVegetPK.version = :versions ")})
public class UsoSueloVeget implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsoSueloVegetPK usoSueloVegetPK;
    @Size(max = 80)
    @Column(name = "CLAVE")
    private String clave;
    @Size(max = 80)
    @Column(name = "CLAVEFOT")
    private String clavefot;
    @Size(max = 80)
    @Column(name = "TIP_INFO")
    private String tipInfo;
    @Size(max = 80)
    @Column(name = "TIP_ECOV")
    private String tipEcov;
    @Size(max = 150)
    @Column(name = "AGECOSIS")
    private String agecosis;
    @Size(max = 80)
    @Column(name = "TIPAGES")
    private String tipages;
    @Size(max = 80)
    @Column(name = "TIP_VEG")
    private String tipVeg;
    @Size(max = 80)
    @Column(name = "DESVEG")
    private String desveg;
    @Size(max = 80)
    @Column(name = "FASE_VS")
    private String faseVs;
    @Size(max = 80)
    @Column(name = "TIP_PLAN")
    private String tipPlan;
    @Size(max = 80)
    @Column(name = "TIP_CUL1")
    private String tipCul1;
    @Size(max = 80)
    @Column(name = "TIP_CUL2")
    private String tipCul2;
    @Size(max = 80)
    @Column(name = "OTROS")
    private String otros;
    @Size(max = 80)
    @Column(name = "C_U_Y_V")
    private String cUYV;
    @Size(max = 80)
    @Column(name = "TIPO_GEN")
    private String tipoGen;
    @Column(name = "SUP_EA")
    private BigDecimal supEa;
    @Size(max = 80)
    @Column(name = "PROY")
    private String proy;
    @Size(max = 80)
    @Column(name = "COMP")
    private String comp;
    @Size(max = 80)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigDecimal areabuffer;
    @Column(name = "AREA")
    private BigDecimal area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public UsoSueloVeget() {
    }

    public UsoSueloVeget(UsoSueloVegetPK usoSueloVegetPK) {
        this.usoSueloVegetPK = usoSueloVegetPK;
    }

    public UsoSueloVeget(String numFolio, String cveProy, String cveArea, short version) {
        this.usoSueloVegetPK = new UsoSueloVegetPK(numFolio, cveProy, cveArea, version);
    }

    public UsoSueloVegetPK getUsoSueloVegetPK() {
        return usoSueloVegetPK;
    }

    public void setUsoSueloVegetPK(UsoSueloVegetPK usoSueloVegetPK) {
        this.usoSueloVegetPK = usoSueloVegetPK;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClavefot() {
        return clavefot;
    }

    public void setClavefot(String clavefot) {
        this.clavefot = clavefot;
    }

    public String getTipInfo() {
        return tipInfo;
    }

    public void setTipInfo(String tipInfo) {
        this.tipInfo = tipInfo;
    }

    public String getTipEcov() {
        return tipEcov;
    }

    public void setTipEcov(String tipEcov) {
        this.tipEcov = tipEcov;
    }

    public String getAgecosis() {
        return agecosis;
    }

    public void setAgecosis(String agecosis) {
        this.agecosis = agecosis;
    }

    public String getTipages() {
        return tipages;
    }

    public void setTipages(String tipages) {
        this.tipages = tipages;
    }

    public String getTipVeg() {
        return tipVeg;
    }

    public void setTipVeg(String tipVeg) {
        this.tipVeg = tipVeg;
    }

    public String getDesveg() {
        return desveg;
    }

    public void setDesveg(String desveg) {
        this.desveg = desveg;
    }

    public String getFaseVs() {
        return faseVs;
    }

    public void setFaseVs(String faseVs) {
        this.faseVs = faseVs;
    }

    public String getTipPlan() {
        return tipPlan;
    }

    public void setTipPlan(String tipPlan) {
        this.tipPlan = tipPlan;
    }

    public String getTipCul1() {
        return tipCul1;
    }

    public void setTipCul1(String tipCul1) {
        this.tipCul1 = tipCul1;
    }

    public String getTipCul2() {
        return tipCul2;
    }

    public void setTipCul2(String tipCul2) {
        this.tipCul2 = tipCul2;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public String getCUYV() {
        return cUYV;
    }

    public void setCUYV(String cUYV) {
        this.cUYV = cUYV;
    }

    public String getTipoGen() {
        return tipoGen;
    }

    public void setTipoGen(String tipoGen) {
        this.tipoGen = tipoGen;
    }

    public BigDecimal getSupEa() {
        return supEa;
    }

    public void setSupEa(BigDecimal supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigDecimal getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigDecimal areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usoSueloVegetPK != null ? usoSueloVegetPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsoSueloVeget)) {
            return false;
        }
        UsoSueloVeget other = (UsoSueloVeget) object;
        if ((this.usoSueloVegetPK == null && other.usoSueloVegetPK != null) || (this.usoSueloVegetPK != null && !this.usoSueloVegetPK.equals(other.usoSueloVegetPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descrip;
    }
    
}
