/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_ESTATUS_DOCUMENTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEstatusDocumento.findAll", query = "SELECT c FROM CatEstatusDocumento c"),
    @NamedQuery(name = "CatEstatusDocumento.findByEstatusDocumentoId", query = "SELECT c FROM CatEstatusDocumento c WHERE c.estatusDocumentoId = :estatusDocumentoId"),
    @NamedQuery(name = "CatEstatusDocumento.findByEstatusDocumentoDescripcion", query = "SELECT c FROM CatEstatusDocumento c WHERE c.estatusDocumentoDescripcion = :estatusDocumentoDescripcion")})
public class CatEstatusDocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTATUS_DOCUMENTO_ID")
    private Short estatusDocumentoId;
    @Size(max = 1000)
    @Column(name = "ESTATUS_DOCUMENTO_DESCRIPCION")
    private String estatusDocumentoDescripcion;
    @OneToMany(mappedBy = "estatusDocumentoIdHist")
    private Collection<DocumentoDgiraFlujoHist> documentoDgiraFlujoHistCollection;
    @OneToMany(mappedBy = "estatusDocumentoId")
    private Collection<DocumentoDgiraFlujo> documentoDgiraFlujoCollection;

    public CatEstatusDocumento() {
    }

    public CatEstatusDocumento(Short estatusDocumentoId) {
        this.estatusDocumentoId = estatusDocumentoId;
    }

    public Short getEstatusDocumentoId() {
        return estatusDocumentoId;
    }

    public void setEstatusDocumentoId(Short estatusDocumentoId) {
        this.estatusDocumentoId = estatusDocumentoId;
    }

    public String getEstatusDocumentoDescripcion() {
        return estatusDocumentoDescripcion;
    }

    public void setEstatusDocumentoDescripcion(String estatusDocumentoDescripcion) {
        this.estatusDocumentoDescripcion = estatusDocumentoDescripcion;
    }

    @XmlTransient
    public Collection<DocumentoDgiraFlujoHist> getDocumentoDgiraFlujoHistCollection() {
        return documentoDgiraFlujoHistCollection;
    }

    public void setDocumentoDgiraFlujoHistCollection(Collection<DocumentoDgiraFlujoHist> documentoDgiraFlujoHistCollection) {
        this.documentoDgiraFlujoHistCollection = documentoDgiraFlujoHistCollection;
    }

    @XmlTransient
    public Collection<DocumentoDgiraFlujo> getDocumentoDgiraFlujoCollection() {
        return documentoDgiraFlujoCollection;
    }

    public void setDocumentoDgiraFlujoCollection(Collection<DocumentoDgiraFlujo> documentoDgiraFlujoCollection) {
        this.documentoDgiraFlujoCollection = documentoDgiraFlujoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estatusDocumentoId != null ? estatusDocumentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEstatusDocumento)) {
            return false;
        }
        CatEstatusDocumento other = (CatEstatusDocumento) object;
        if ((this.estatusDocumentoId == null && other.estatusDocumentoId != null) || (this.estatusDocumentoId != null && !this.estatusDocumentoId.equals(other.estatusDocumentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatEstatusDocumento[ estatusDocumentoId=" + estatusDocumentoId + " ]";
    }
    
}
