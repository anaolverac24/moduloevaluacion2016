/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PREGUNTA_CLIMA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaClima.findAll", query = "SELECT p FROM PreguntaClima p"),
    @NamedQuery(name = "PreguntaClima.findByFolioProyecto", query = "SELECT p FROM PreguntaClima p WHERE p.preguntaClimaPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PreguntaClima.findBySerialProyecto", query = "SELECT p FROM PreguntaClima p WHERE p.preguntaClimaPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PreguntaClima.findByPreguntaId", query = "SELECT p FROM PreguntaClima p WHERE p.preguntaClimaPK.preguntaId = :preguntaId"),
    @NamedQuery(name = "PreguntaClima.findByClimaId", query = "SELECT p FROM PreguntaClima p WHERE p.preguntaClimaPK.climaId = :climaId")})
public class PreguntaClima implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreguntaClimaPK preguntaClimaPK;

    public PreguntaClima() {
    }

    public PreguntaClima(PreguntaClimaPK preguntaClimaPK) {
        this.preguntaClimaPK = preguntaClimaPK;
    }

    public PreguntaClima(String folioProyecto, short serialProyecto, short preguntaId, short climaId) {
        this.preguntaClimaPK = new PreguntaClimaPK(folioProyecto, serialProyecto, preguntaId, climaId);
    }

    public PreguntaClimaPK getPreguntaClimaPK() {
        return preguntaClimaPK;
    }

    public void setPreguntaClimaPK(PreguntaClimaPK preguntaClimaPK) {
        this.preguntaClimaPK = preguntaClimaPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaClimaPK != null ? preguntaClimaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaClima)) {
            return false;
        }
        PreguntaClima other = (PreguntaClima) object;
        if ((this.preguntaClimaPK == null && other.preguntaClimaPK != null) || (this.preguntaClimaPK != null && !this.preguntaClimaPK.equals(other.preguntaClimaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "package mx.gob.semarnat.model.dgira_miae.PreguntaClima[ preguntaClimaPK=" + preguntaClimaPK + " ]";
    }
    
}
