/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "ANALISIS_PRELIMINAR_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnalisisPreliminarProyecto.findAll", query = "SELECT a FROM AnalisisPreliminarProyecto a"),
    @NamedQuery(name = "AnalisisPreliminarProyecto.findByBitacoraProyecto", query = "SELECT a FROM AnalisisPreliminarProyecto a WHERE a.analisisPreliminarProyectoPK.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "AnalisisPreliminarProyecto.findByBitacoraProyectoId", query = "SELECT a FROM AnalisisPreliminarProyecto a WHERE a.analisisPreliminarProyectoPK.bitacoraProyecto =:bitacoraProyecto and a.analisisPreliminarProyectoPK.catAnalisisId =:catAnalisisId"),
    @NamedQuery(name = "AnalisisPreliminarProyecto.findByCatAnalisisId", query = "SELECT a FROM AnalisisPreliminarProyecto a WHERE a.analisisPreliminarProyectoPK.catAnalisisId = :catAnalisisId"),
    @NamedQuery(name = "AnalisisPreliminarProyecto.findByAnalisisRespuesta", query = "SELECT a FROM AnalisisPreliminarProyecto a WHERE a.analisisRespuesta = :analisisRespuesta"),
    @NamedQuery(name = "AnalisisPreliminarProyecto.findByAnalisisObservacion", query = "SELECT a FROM AnalisisPreliminarProyecto a WHERE a.analisisObservacion = :analisisObservacion")})
public class AnalisisPreliminarProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnalisisPreliminarProyectoPK analisisPreliminarProyectoPK;
    //@Size(max = 1)
    @Column(name = "ANALISIS_RESPUESTA")
    private String analisisRespuesta;
    @Size(max = 4000)
    @Column(name = "ANALISIS_OBSERVACION")
    private String analisisObservacion;
    @JoinColumn(name = "CAT_ANALISIS_ID", referencedColumnName = "CAT_ANALISIS_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatAnalisisPreliminar catAnalisisPreliminar;

    public AnalisisPreliminarProyecto() {
    }

    public AnalisisPreliminarProyecto(AnalisisPreliminarProyectoPK analisisPreliminarProyectoPK) {
        this.analisisPreliminarProyectoPK = analisisPreliminarProyectoPK;
    }

    public AnalisisPreliminarProyecto(String bitacoraProyecto, short catAnalisisId) {
        this.analisisPreliminarProyectoPK = new AnalisisPreliminarProyectoPK(bitacoraProyecto, catAnalisisId);
    }

    public AnalisisPreliminarProyectoPK getAnalisisPreliminarProyectoPK() {
        return analisisPreliminarProyectoPK;
    }

    public void setAnalisisPreliminarProyectoPK(AnalisisPreliminarProyectoPK analisisPreliminarProyectoPK) {
        this.analisisPreliminarProyectoPK = analisisPreliminarProyectoPK;
    }

    public String getAnalisisRespuesta() {
        return analisisRespuesta;
    }

    public void setAnalisisRespuesta(String analisisRespuesta) {
        this.analisisRespuesta = analisisRespuesta;
    }

    public String getAnalisisObservacion() {
        return analisisObservacion;
    }

    public void setAnalisisObservacion(String analisisObservacion) {
        this.analisisObservacion = analisisObservacion;
    }

    public CatAnalisisPreliminar getCatAnalisisPreliminar() {
        return catAnalisisPreliminar;
    }

    public void setCatAnalisisPreliminar(CatAnalisisPreliminar catAnalisisPreliminar) {
        this.catAnalisisPreliminar = catAnalisisPreliminar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (analisisPreliminarProyectoPK != null ? analisisPreliminarProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalisisPreliminarProyecto)) {
            return false;
        }
        AnalisisPreliminarProyecto other = (AnalisisPreliminarProyecto) object;
        if ((this.analisisPreliminarProyectoPK == null && other.analisisPreliminarProyectoPK != null) || (this.analisisPreliminarProyectoPK != null && !this.analisisPreliminarProyectoPK.equals(other.analisisPreliminarProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.AnalisisPreliminarProyecto[ analisisPreliminarProyectoPK=" + analisisPreliminarProyectoPK + " ]";
    }
    
}
