/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "CAT_DERECHOS_CRITERIOSVALORES",  schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatDerechosCriteriosvalores.findAll", query = "SELECT c FROM CatDerechosCriteriosvalores c"),
    @NamedQuery(name = "CatDerechosCriteriosvalores.findByIdCriterio", query = "SELECT c FROM CatDerechosCriteriosvalores c WHERE c.catDerechosCriteriosvaloresPK.idCriterio = :idCriterio"),
    @NamedQuery(name = "CatDerechosCriteriosvalores.findByIdValor", query = "SELECT c FROM CatDerechosCriteriosvalores c WHERE c.catDerechosCriteriosvaloresPK.idValor = :idValor"),
    @NamedQuery(name = "CatDerechosCriteriosvalores.findByDescripcionValor", query = "SELECT c FROM CatDerechosCriteriosvalores c WHERE c.descripcionValor = :descripcionValor"),
    @NamedQuery(name = "CatDerechosCriteriosvalores.findByValor", query = "SELECT c FROM CatDerechosCriteriosvalores c WHERE c.valor = :valor"),
    @NamedQuery(name = "CatDerechosCriteriosvalores.findByReferenciaDoc", query = "SELECT c FROM CatDerechosCriteriosvalores c WHERE c.referenciaDoc = :referenciaDoc")})
public class CatDerechosCriteriosvalores implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "VALOR")
    private BigInteger valor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catDerechosCriteriosvalores")
    private List<CriterioValoresProyecto> criterioValoresProyectoList;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatDerechosCriteriosvaloresPK catDerechosCriteriosvaloresPK;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION_VALOR")
    private String descripcionValor;
    @Basic(optional = false)
    @Column(name = "REFERENCIA_DOC")
    private String referenciaDoc;
    @JoinColumn(name = "ID_CRITERIO", referencedColumnName = "CRITERIO_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatDerechosCriterios catDerechosCriterios;

    public CatDerechosCriteriosvalores() {
    }

    public CatDerechosCriteriosvalores(CatDerechosCriteriosvaloresPK catDerechosCriteriosvaloresPK) {
        this.catDerechosCriteriosvaloresPK = catDerechosCriteriosvaloresPK;
    }

    public CatDerechosCriteriosvalores(CatDerechosCriteriosvaloresPK catDerechosCriteriosvaloresPK, String descripcionValor, BigInteger valor, String referenciaDoc) {
        this.catDerechosCriteriosvaloresPK = catDerechosCriteriosvaloresPK;
        this.descripcionValor = descripcionValor;
        this.valor = valor;
        this.referenciaDoc = referenciaDoc;
    }

    public CatDerechosCriteriosvalores(Integer idCriterio, Integer idValor) {
        this.catDerechosCriteriosvaloresPK = new CatDerechosCriteriosvaloresPK(idCriterio, idValor);
    }

    public CatDerechosCriteriosvaloresPK getCatDerechosCriteriosvaloresPK() {
        return catDerechosCriteriosvaloresPK;
    }

    public void setCatDerechosCriteriosvaloresPK(CatDerechosCriteriosvaloresPK catDerechosCriteriosvaloresPK) {
        this.catDerechosCriteriosvaloresPK = catDerechosCriteriosvaloresPK;
    }

    public String getDescripcionValor() {
        return descripcionValor;
    }

    public void setDescripcionValor(String descripcionValor) {
        this.descripcionValor = descripcionValor;
    }


    public String getReferenciaDoc() {
        return referenciaDoc;
    }

    public void setReferenciaDoc(String referenciaDoc) {
        this.referenciaDoc = referenciaDoc;
    }

    public CatDerechosCriterios getCatDerechosCriterios() {
        return catDerechosCriterios;
    }

    public void setCatDerechosCriterios(CatDerechosCriterios catDerechosCriterios) {
        this.catDerechosCriterios = catDerechosCriterios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catDerechosCriteriosvaloresPK != null ? catDerechosCriteriosvaloresPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatDerechosCriteriosvalores)) {
            return false;
        }
        CatDerechosCriteriosvalores other = (CatDerechosCriteriosvalores) object;
        if ((this.catDerechosCriteriosvaloresPK == null && other.catDerechosCriteriosvaloresPK != null) || (this.catDerechosCriteriosvaloresPK != null && !this.catDerechosCriteriosvaloresPK.equals(other.catDerechosCriteriosvaloresPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatDerechosCriteriosvalores[ catDerechosCriteriosvaloresPK=" + catDerechosCriteriosvaloresPK + " ]";
    }

    public BigInteger getValor() {
        return valor;
    }

    public void setValor(BigInteger valor) {
        this.valor = valor;
    }

    @XmlTransient
    public List<CriterioValoresProyecto> getCriterioValoresProyectoList() {
        return criterioValoresProyectoList;
    }

    public void setCriterioValoresProyectoList(List<CriterioValoresProyecto> criterioValoresProyectoList) {
        this.criterioValoresProyectoList = criterioValoresProyectoList;
    }
    
}
