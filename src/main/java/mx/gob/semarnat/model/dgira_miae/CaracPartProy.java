/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CARAC_PART_PROY", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CaracPartProy.findAll", query = "SELECT c FROM CaracPartProy c"),
    @NamedQuery(name = "CaracPartProy.findByFolioProyecto", query = "SELECT c FROM CaracPartProy c WHERE c.caracPartProyPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "CaracPartProy.findBySerialProyecto", query = "SELECT c FROM CaracPartProy c WHERE c.caracPartProyPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "CaracPartProy.findByCaracteristicaId", query = "SELECT c FROM CaracPartProy c WHERE c.caracPartProyPK.caracteristicaId = :caracteristicaId"),
    @NamedQuery(name = "CaracPartProy.findByObraActividad", query = "SELECT c FROM CaracPartProy c WHERE c.obraActividad = :obraActividad"),
    @NamedQuery(name = "CaracPartProy.findByDescripcion", query = "SELECT c FROM CaracPartProy c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CaracPartProy.findBySuperficie", query = "SELECT c FROM CaracPartProy c WHERE c.superficie = :superficie"),
    @NamedQuery(name = "CaracPartProy.findByCtunClve", query = "SELECT c FROM CaracPartProy c WHERE c.ctunClve = :ctunClve"),
    @NamedQuery(name = "CaracPartProy.findByIdCaracSeq", query = "SELECT c FROM CaracPartProy c WHERE c.idCaracSeq = :idCaracSeq")})
public class CaracPartProy implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CaracPartProyPK caracPartProyPK;
    @Column(name = "OBRA_ACTIVIDAD")
    private String obraActividad;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "SUPERFICIE")
    private Double superficie;
    @JoinColumn(name = "NATURALEZA_ID", referencedColumnName = "NATURALEZA_ID")
    @ManyToOne
    private CatNaturaleza naturalezaId;
    @JoinColumn(name = "TEMPORALIDAD_ID", referencedColumnName = "TEMPORALIDAD_ID")
    @ManyToOne
    private CatTemporalidad temporalidadId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    @Column(name = "CTUN_CLVE")
    private Short ctunClve;
    
    @Column(name = "ID_CARAC_SEQ")
    private Short idCaracSeq;

    @Column(name = "NOMBRE_OBRA")
    private String nombreObra;
    
    @Column(name = "ESTATUS_SIG_PROM")    
    private String estatusSigProm;

    public CaracPartProy() {
    }

    public CaracPartProy(CaracPartProyPK caracPartProyPK) {
        this.caracPartProyPK = caracPartProyPK;
    }

    public CaracPartProy(String folioProyecto, short serialProyecto, short caracteristicaId) {
        this.caracPartProyPK = new CaracPartProyPK(folioProyecto, serialProyecto, caracteristicaId);
    }

    public CaracPartProyPK getCaracPartProyPK() {
        return caracPartProyPK;
    }

    public void setCaracPartProyPK(CaracPartProyPK caracPartProyPK) {
        this.caracPartProyPK = caracPartProyPK;
    }

    public String getObraActividad() {
        return obraActividad;
    }

    public void setObraActividad(String obraActividad) {
        this.obraActividad = obraActividad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getSuperficie() {
        return superficie;
    }

    public void setSuperficie(Double superficie) {
        this.superficie = superficie;
    }

    public Short getCtunClve() {
        return ctunClve;
    }

    public void setCtunClve(Short ctunClve) {
        this.ctunClve = ctunClve;
    }

    public CatNaturaleza getNaturalezaId() {
        return naturalezaId;
    }

    public void setNaturalezaId(CatNaturaleza naturalezaId) {
        this.naturalezaId = naturalezaId;
    }

    public CatTemporalidad getTemporalidadId() {
        return temporalidadId;
    }

    public void setTemporalidadId(CatTemporalidad temporalidadId) {
        this.temporalidadId = temporalidadId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (caracPartProyPK != null ? caracPartProyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaracPartProy)) {
            return false;
        }
        CaracPartProy other = (CaracPartProy) object;
        if ((this.caracPartProyPK == null && other.caracPartProyPK != null) || (this.caracPartProyPK != null && !this.caracPartProyPK.equals(other.caracPartProyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CaracPartProy[ caracPartProyPK=" + caracPartProyPK + " ]";
    }

	/**
	 * @return the idCaracSeq
	 */
	public Short getIdCaracSeq() {
		return idCaracSeq;
	}

	/**
	 * @param idCaracSeq the idCaracSeq to set
	 */
	public void setIdCaracSeq(Short idCaracSeq) {
		this.idCaracSeq = idCaracSeq;
	}

	/**
	 * @return the nombreObra
	 */
	public String getNombreObra() {
		return nombreObra;
	}

	/**
	 * @param nombreObra the nombreObra to set
	 */
	public void setNombreObra(String nombreObra) {
		this.nombreObra = nombreObra;
	}

	/**
	 * @return the estatusSigProm
	 */
	public String getEstatusSigProm() {
		return estatusSigProm;
	}

	/**
	 * @param estatusSigProm the estatusSigProm to set
	 */
	public void setEstatusSigProm(String estatusSigProm) {
		this.estatusSigProm = estatusSigProm;
	}
}
