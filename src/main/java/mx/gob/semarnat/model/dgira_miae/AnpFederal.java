/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ANP_FEDERAL", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnpFederal.findAll", query = "SELECT a FROM AnpFederal a"),
    @NamedQuery(name = "AnpFederal.findByFolioCveVersion", query = "SELECT e FROM AnpFederal e WHERE e.anpFederalPK.numFolio = :folio and e.anpFederalPK.version = :version")})
public class AnpFederal implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnpFederalPK anpFederalPK;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "CAT_DECRET")
    private String catDecret;
    @Column(name = "CAT_MANEJO")
    private String catManejo;
    @Column(name = "ULT_DECRET")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultDecret;
    @Column(name = "SUP_EA")
    private Double supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private Double areabuffer;
    @Column(name = "AREA")
    private Double area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "anpFederal")
//    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public AnpFederal() {
    }

    public AnpFederal(AnpFederalPK anpFederalPK) {
        this.anpFederalPK = anpFederalPK;
    }

    public AnpFederal(String numFolio, String cveProy, String cveArea, short version) {
        this.anpFederalPK = new AnpFederalPK(numFolio, cveProy, cveArea, version);
    }

    public AnpFederalPK getAnpFederalPK() {
        return anpFederalPK;
    }

    public void setAnpFederalPK(AnpFederalPK anpFederalPK) {
        this.anpFederalPK = anpFederalPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCatDecret() {
        return catDecret;
    }

    public void setCatDecret(String catDecret) {
        this.catDecret = catDecret;
    }

    public String getCatManejo() {
        return catManejo;
    }

    public void setCatManejo(String catManejo) {
        this.catManejo = catManejo;
    }

    public Date getUltDecret() {
        return ultDecret;
    }

    public void setUltDecret(Date ultDecret) {
        this.ultDecret = ultDecret;
    }

    public Double getSupEa() {
        return supEa;
    }

    public void setSupEa(Double supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Double getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(Double areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

//    @XmlTransient
//    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
//        return sigeiaAnalisisList;
//    }
//
//    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
//        this.sigeiaAnalisisList = sigeiaAnalisisList;
//    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anpFederalPK != null ? anpFederalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnpFederal)) {
            return false;
        }
        AnpFederal other = (AnpFederal) object;
        if ((this.anpFederalPK == null && other.anpFederalPK != null) || (this.anpFederalPK != null && !this.anpFederalPK.equals(other.anpFederalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AnpFederal[ anpFederalPK=" + anpFederalPK + " ]";
    }

}
