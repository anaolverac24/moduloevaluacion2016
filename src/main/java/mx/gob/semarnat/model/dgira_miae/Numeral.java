/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "NUMERAL", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Numeral.findAll", query = "SELECT n FROM Numeral n"),
    @NamedQuery(name = "Numeral.findByFolioProyecto", query = "SELECT n FROM Numeral n WHERE n.numeralPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "Numeral.findBySerialProyecto", query = "SELECT n FROM Numeral n WHERE n.numeralPK.serialProyecto = :serialProyecto"),
    
    @NamedQuery(name = "Numeral.findByFolioSerialProyecto", query = "SELECT MAX(n.numeralPK.numeralId) FROM Numeral n "
    		+ "WHERE n.numeralPK.folioProyecto = :folioProyecto AND n.numeralPK.serialProyecto = :serialProyecto AND n.numeralPK.normaProyId = :normaProyId"),
    
    @NamedQuery(name = "Numeral.findByFolioSerialProyectoNorma", query = "SELECT n FROM Numeral n "
    		+ "WHERE n.numeralPK.folioProyecto = :folioProyecto AND n.numeralPK.serialProyecto = :serialProyecto AND n.numeralPK.normaProyId = :normaProyId"),
    
    
    @NamedQuery(name = "Numeral.findByNormaProyId", query = "SELECT n FROM Numeral n WHERE n.numeralPK.normaProyId = :normaProyId"),
    @NamedQuery(name = "Numeral.findByNormaNumeral", query = "SELECT n FROM Numeral n WHERE n.normaNumeral = :normaNumeral"),
    @NamedQuery(name = "Numeral.findByNormaRequisito", query = "SELECT n FROM Numeral n WHERE n.normaRequisito = :normaRequisito")})
public class Numeral implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NumeralPK numeralPK;
    @Basic(optional = false)
    @Column(name = "NORMA_NUMERAL")
    private String normaNumeral;
    @Column(name = "NORMA_REQUISITO")
    private String normaRequisito;
    @Lob
    @Column(name = "NORMA_VINCULACION")
    private String normaVinculaciN;
//    @JoinColumns({
//        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
//        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
//        @JoinColumn(name = "NORMA_PROY_ID", referencedColumnName = "NORMA_PROY_ID", insertable = false, updatable = false)})
//    @OneToOne(optional = false)
//    private NormaProyecto normaProyecto;

    public Numeral() {
    }

    public Numeral(NumeralPK numeralPK) {
        this.numeralPK = numeralPK;
    }

    public Numeral(NumeralPK numeralPK, String normaNumeral) {
        this.numeralPK = numeralPK;
        this.normaNumeral = normaNumeral;
    }

    public Numeral(String folioProyecto, short serialProyecto, short normaProyId, short numeralId) {
        this.numeralPK = new NumeralPK(folioProyecto, serialProyecto, normaProyId, numeralId);
    }

    public NumeralPK getNumeralPK() {
        return numeralPK;
    }

    public void setNumeralPK(NumeralPK numeralPK) {
        this.numeralPK = numeralPK;
    }

    public String getNormaNumeral() {
        return normaNumeral;
    }

    public void setNormaNumeral(String normaNumeral) {
        this.normaNumeral = normaNumeral;
    }

    public String getNormaRequisito() {
        return normaRequisito;
    }

    public void setNormaRequisito(String normaRequisito) {
        this.normaRequisito = normaRequisito;
    }

    public String getNormaVinculaciN() {
        return normaVinculaciN;
    }
    
    public String getNormaVinculaciNCorta(){
        if(normaVinculaciN.length()<199){
            return normaVinculaciN;
        }
        return normaVinculaciN.substring(0, 199);
    }

    public void setNormaVinculaciN(String normaVinculaciN) {
        this.normaVinculaciN = normaVinculaciN;
    }

//    public NormaProyecto getNormaProyecto() {
//        return normaProyecto;
//    }
//
//    public void setNormaProyecto(NormaProyecto normaProyecto) {
//        this.normaProyecto = normaProyecto;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numeralPK != null ? numeralPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Numeral)) {
            return false;
        }
        Numeral other = (Numeral) object;
        if ((this.numeralPK == null && other.numeralPK != null) || (this.numeralPK != null && !this.numeralPK.equals(other.numeralPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.Numeral[ numeralPK=" + numeralPK + " ]";
    }   
}
