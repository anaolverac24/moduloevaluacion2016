/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "IMPACTO_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImpactoProyecto.findAll", query = "SELECT i FROM ImpactoProyecto i"),
    @NamedQuery(name = "ImpactoProyecto.findByFolioProyecto", query = "SELECT i FROM ImpactoProyecto i WHERE i.impactoProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ImpactoProyecto.findBySerialProyecto", query = "SELECT i FROM ImpactoProyecto i WHERE i.impactoProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ImpactoProyecto.findByTipoImpactoId", query = "SELECT i FROM ImpactoProyecto i WHERE i.impactoProyectoPK.tipoImpactoId = :tipoImpactoId")})
public class ImpactoProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ImpactoProyectoPK impactoProyectoPK;
    @Lob
    @Column(name = "IMPACTO_DESCRIPCION")
    private String impactoDescripcion;
    @Lob
    @Column(name = "IMPACTO_MEDIDA_MITIGACION")
    private String impactoMedidaMitigacion;
    @Lob
    @Column(name = "IMPACTO_MEDIDA_ADICIONAL")
    private String impactoMedidaAdicional;
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID")
    @ManyToOne
    private CatEtapa etapaId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public ImpactoProyecto() {
    }

    public ImpactoProyecto(ImpactoProyectoPK impactoProyectoPK) {
        this.impactoProyectoPK = impactoProyectoPK;
    }

    public ImpactoProyecto(String folioProyecto, short serialProyecto, short tipoImpactoId) {
        this.impactoProyectoPK = new ImpactoProyectoPK(folioProyecto, serialProyecto, tipoImpactoId);
    }

    public ImpactoProyectoPK getImpactoProyectoPK() {
        return impactoProyectoPK;
    }

    public void setImpactoProyectoPK(ImpactoProyectoPK impactoProyectoPK) {
        this.impactoProyectoPK = impactoProyectoPK;
    }

    public String getImpactoDescripcion() {
        return impactoDescripcion;
    }

    public void setImpactoDescripcion(String impactoDescripcion) {
        this.impactoDescripcion = impactoDescripcion;
    }

    public String getImpactoMedidaMitigacion() {
        return impactoMedidaMitigacion;
    }

    public void setImpactoMedidaMitigacion(String impactoMedidaMitigacion) {
        this.impactoMedidaMitigacion = impactoMedidaMitigacion;
    }

    public String getImpactoMedidaAdicional() {
        return impactoMedidaAdicional;
    }

    public void setImpactoMedidaAdicional(String impactoMedidaAdicional) {
        this.impactoMedidaAdicional = impactoMedidaAdicional;
    }

    public CatEtapa getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(CatEtapa etapaId) {
        this.etapaId = etapaId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (impactoProyectoPK != null ? impactoProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImpactoProyecto)) {
            return false;
        }
        ImpactoProyecto other = (ImpactoProyecto) object;
        if ((this.impactoProyectoPK == null && other.impactoProyectoPK != null) || (this.impactoProyectoPK != null && !this.impactoProyectoPK.equals(other.impactoProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.ImpactoProyecto[ impactoProyectoPK=" + impactoProyectoPK + " ]";
    }
    
}
