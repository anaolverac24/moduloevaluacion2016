/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_DEPENDENCIA_OFICIO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatDependenciaOficio.findAll", query = "SELECT c FROM CatDependenciaOficio c"),
    @NamedQuery(name = "CatDependenciaOficio.findByCatDependenciaId", query = "SELECT c FROM CatDependenciaOficio c WHERE c.catDependenciaId = :catDependenciaId"),
    @NamedQuery(name = "CatDependenciaOficio.findByCatDependenciaDescripcion", query = "SELECT c FROM CatDependenciaOficio c WHERE c.catDependenciaDescripcion = :catDependenciaDescripcion"),
    @NamedQuery(name = "CatDependenciaOficio.findByCatDependenciaSiglas", query = "SELECT c FROM CatDependenciaOficio c WHERE c.catDependenciaSiglas = :catDependenciaSiglas")})
public class CatDependenciaOficio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CAT_DEPENDENCIA_ID")
    private Short catDependenciaId;
    @Size(max = 300)
    @Column(name = "CAT_DEPENDENCIA_DESCRIPCION")
    private String catDependenciaDescripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CAT_DEPENDENCIA_SIGLAS")
    private String catDependenciaSiglas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catDependenciaId")
    private List<OpinionTecnicaProyecto> opinionTecnicaProyectoList;
    @OneToMany(mappedBy = "catDependenciaId")
    private List<NotificacionGobiernoProyecto> notificacionGobiernoProyectoList;

    public CatDependenciaOficio() {
    }

    public CatDependenciaOficio(Short catDependenciaId) {
        this.catDependenciaId = catDependenciaId;
    }

    public CatDependenciaOficio(Short catDependenciaId, String catDependenciaSiglas) {
        this.catDependenciaId = catDependenciaId;
        this.catDependenciaSiglas = catDependenciaSiglas;
    }

    public Short getCatDependenciaId() {
        return catDependenciaId;
    }

    public void setCatDependenciaId(Short catDependenciaId) {
        this.catDependenciaId = catDependenciaId;
    }

    public String getCatDependenciaDescripcion() {
        return catDependenciaDescripcion;
    }

    public void setCatDependenciaDescripcion(String catDependenciaDescripcion) {
        this.catDependenciaDescripcion = catDependenciaDescripcion;
    }

    public String getCatDependenciaSiglas() {
        return catDependenciaSiglas;
    }

    public void setCatDependenciaSiglas(String catDependenciaSiglas) {
        this.catDependenciaSiglas = catDependenciaSiglas;
    }

    @XmlTransient
    public List<OpinionTecnicaProyecto> getOpinionTecnicaProyectoList() {
        return opinionTecnicaProyectoList;
    }

    public void setOpinionTecnicaProyectoList(List<OpinionTecnicaProyecto> opinionTecnicaProyectoList) {
        this.opinionTecnicaProyectoList = opinionTecnicaProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catDependenciaId != null ? catDependenciaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatDependenciaOficio)) {
            return false;
        }
        CatDependenciaOficio other = (CatDependenciaOficio) object;
        if ((this.catDependenciaId == null && other.catDependenciaId != null) || (this.catDependenciaId != null && !this.catDependenciaId.equals(other.catDependenciaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return catDependenciaId.toString();
    }

    @XmlTransient
    public List<NotificacionGobiernoProyecto> getNotificacionGobiernoProyectoList() {
        return notificacionGobiernoProyectoList;
    }

    public void setNotificacionGobiernoProyectoList(List<NotificacionGobiernoProyecto> notificacionGobiernoProyectoList) {
        this.notificacionGobiernoProyectoList = notificacionGobiernoProyectoList;
    }
    
}
