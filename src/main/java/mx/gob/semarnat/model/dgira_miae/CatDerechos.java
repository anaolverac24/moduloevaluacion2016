/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rengerden
 */
@Entity
@Table(name = "CAT_DERECHOS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatDerechos.findAll", query = "SELECT c FROM CatDerechos c"),
    @NamedQuery(name = "CatDerechos.findByIdNivel", query = "SELECT c FROM CatDerechos c WHERE c.catDerechosPK.idNivel = :idNivel"),
    @NamedQuery(name = "CatDerechos.findByMonto", query = "SELECT c FROM CatDerechos c WHERE c.monto = :monto"),
    @NamedQuery(name = "CatDerechos.findByYear", query = "SELECT c FROM CatDerechos c WHERE c.year = :year"),
    @NamedQuery(name = "CatDerechos.findByLimiteInferior", query = "SELECT c FROM CatDerechos c WHERE c.limiteInferior = :limiteInferior"),
    @NamedQuery(name = "CatDerechos.findByLimiteSuperior", query = "SELECT c FROM CatDerechos c WHERE c.limiteSuperior = :limiteSuperior"),
    @NamedQuery(name = "CatDerechos.findByIdEstudio", query = "SELECT c FROM CatDerechos c WHERE c.catDerechosPK.idEstudio = :idEstudio")})
public class CatDerechos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatDerechosPK catDerechosPK;
    @Basic(optional = false)
    @Column(name = "MONTO")
    private double monto;
    @Basic(optional = false)
    @Column(name = "YEAR")
    private BigInteger year;
    @Basic(optional = false)
    @Column(name = "LIMITE_INFERIOR")
    private BigInteger limiteInferior;
    @Basic(optional = false)
    @Column(name = "LIMITE_SUPERIOR")
    private BigInteger limiteSuperior;

    public CatDerechos() {
    }

    public CatDerechos(CatDerechosPK catDerechosPK) {
        this.catDerechosPK = catDerechosPK;
    }

    public CatDerechos(CatDerechosPK catDerechosPK, double monto, BigInteger year, BigInteger limiteInferior, BigInteger limiteSuperior) {
        this.catDerechosPK = catDerechosPK;
        this.monto = monto;
        this.year = year;
        this.limiteInferior = limiteInferior;
        this.limiteSuperior = limiteSuperior;
    }

    public CatDerechos(Character idNivel, String idEstudio) {
        this.catDerechosPK = new CatDerechosPK(idNivel, idEstudio);
    }

    public CatDerechosPK getCatDerechosPK() {
        return catDerechosPK;
    }

    public void setCatDerechosPK(CatDerechosPK catDerechosPK) {
        this.catDerechosPK = catDerechosPK;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public BigInteger getYear() {
        return year;
    }

    public void setYear(BigInteger year) {
        this.year = year;
    }

    public BigInteger getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(BigInteger limiteInferior) {
        this.limiteInferior = limiteInferior;
    }

    public BigInteger getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(BigInteger limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catDerechosPK != null ? catDerechosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatDerechos)) {
            return false;
        }
        CatDerechos other = (CatDerechos) object;
        if ((this.catDerechosPK == null && other.catDerechosPK != null) || (this.catDerechosPK != null && !this.catDerechosPK.equals(other.catDerechosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatDerechos[ catDerechosPK=" + catDerechosPK + " ]";
    }
    
}
