/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_PDU", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPdu.findAll", query = "SELECT c FROM CatPdu c"),
    @NamedQuery(name = "CatPdu.findByPduId", query = "SELECT c FROM CatPdu c WHERE c.pduId = :pduId"),
    @NamedQuery(name = "CatPdu.findByPduDescripcion", query = "SELECT c FROM CatPdu c WHERE c.pduDescripcion = :pduDescripcion"),
    @NamedQuery(name = "CatPdu.findByPduFecha", query = "SELECT c FROM CatPdu c WHERE c.pduFecha = :pduFecha"),
    @NamedQuery(name = "CatPdu.findByPduOficio", query = "SELECT c FROM CatPdu c WHERE c.pduOficio = :pduOficio")})
public class CatPdu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PDU_ID")
    private Short pduId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "PDU_DESCRIPCION")
    private String pduDescripcion;
    @Column(name = "PDU_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pduFecha;
    @Size(max = 40)
    @Column(name = "PDU_OFICIO")
    private String pduOficio;

    public CatPdu() {
    }

    public CatPdu(Short pduId) {
        this.pduId = pduId;
    }

    public CatPdu(Short pduId, String pduDescripcion) {
        this.pduId = pduId;
        this.pduDescripcion = pduDescripcion;
    }

    public Short getPduId() {
        return pduId;
    }

    public void setPduId(Short pduId) {
        this.pduId = pduId;
    }

    public String getPduDescripcion() {
        return pduDescripcion;
    }

    public void setPduDescripcion(String pduDescripcion) {
        this.pduDescripcion = pduDescripcion;
    }

    public Date getPduFecha() {
        return pduFecha;
    }

    public void setPduFecha(Date pduFecha) {
        this.pduFecha = pduFecha;
    }

    public String getPduOficio() {
        return pduOficio;
    }

    public void setPduOficio(String pduOficio) {
        this.pduOficio = pduOficio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pduId != null ? pduId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPdu)) {
            return false;
        }
        CatPdu other = (CatPdu) object;
        if ((this.pduId == null && other.pduId != null) || (this.pduId != null && !this.pduId.equals(other.pduId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatPdu[ pduId=" + pduId + " ]";
    }
    
}
