/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "LEY_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeyProyecto.findAll", query = "SELECT l FROM LeyProyecto l"),
    @NamedQuery(name = "LeyProyecto.findByFolioProyeto", query = "SELECT l FROM LeyProyecto l WHERE l.leyProyectoPK.folioProyeto = :folioProyeto"),
    @NamedQuery(name = "LeyProyecto.findBySerialProyecto", query = "SELECT l FROM LeyProyecto l WHERE l.leyProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "LeyProyecto.findByLeyProyectoId", query = "SELECT l FROM LeyProyecto l WHERE l.leyProyectoPK.leyProyectoId = :leyProyectoId"),
    @NamedQuery(name = "LeyProyecto.findByLeyArticulo", query = "SELECT l FROM LeyProyecto l WHERE l.leyArticulo = :leyArticulo"),
    @NamedQuery(name = "LeyProyecto.findByLeyFraccion", query = "SELECT l FROM LeyProyecto l WHERE l.leyFraccion = :leyFraccion"),
    @NamedQuery(name = "LeyProyecto.findByLeyInciso", query = "SELECT l FROM LeyProyecto l WHERE l.leyInciso = :leyInciso")})
public class LeyProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LeyProyectoPK leyProyectoPK;
    @Column(name = "LEY_ARTICULO")
    private String leyArticulo;
    @Column(name = "LEY_FRACCION")
    private String leyFraccion;
    @Column(name = "LEY_INCISO")
    private String leyInciso;
    @Lob
    @Column(name = "VINCULACION")
    private String vinculacion;
    @JoinColumn(name = "LEY_ID", referencedColumnName = "LEY_ID")
    @ManyToOne(optional = false)
    private CatLey leyId;

    public LeyProyecto() {
    }

    public LeyProyecto(LeyProyectoPK leyProyectoPK) {
        this.leyProyectoPK = leyProyectoPK;
    }

    public LeyProyecto(String folioProyeto, short serialProyecto, short leyProyectoId) {
        this.leyProyectoPK = new LeyProyectoPK(folioProyeto, serialProyecto, leyProyectoId);
    }

    public LeyProyectoPK getLeyProyectoPK() {
        return leyProyectoPK;
    }

    public void setLeyProyectoPK(LeyProyectoPK leyProyectoPK) {
        this.leyProyectoPK = leyProyectoPK;
    }

    public String getLeyArticulo() {
        return leyArticulo;
    }

    public void setLeyArticulo(String leyArticulo) {
        this.leyArticulo = leyArticulo;
    }

    public String getLeyFraccion() {
        return leyFraccion;
    }

    public void setLeyFraccion(String leyFraccion) {
        this.leyFraccion = leyFraccion;
    }

    public String getLeyInciso() {
        return leyInciso;
    }

    public void setLeyInciso(String leyInciso) {
        this.leyInciso = leyInciso;
    }

    public String getVinculacion() {
        return vinculacion;
    }

    public void setVinculacion(String vinculacion) {
        this.vinculacion = vinculacion;
    }

    public CatLey getLeyId() {
        return leyId;
    }

    public void setLeyId(CatLey leyId) {
        this.leyId = leyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leyProyectoPK != null ? leyProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeyProyecto)) {
            return false;
        }
        LeyProyecto other = (LeyProyecto) object;
        if ((this.leyProyectoPK == null && other.leyProyectoPK != null) || (this.leyProyectoPK != null && !this.leyProyectoPK.equals(other.leyProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.LeyProyecto[ leyProyectoPK=" + leyProyectoPK + " ]";
    }
    
}
