/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_TIPO_CLIMA", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoClima.findAll", query = "SELECT c FROM CatTipoClima c"),
    @NamedQuery(name = "CatTipoClima.findByClimaId", query = "SELECT c FROM CatTipoClima c WHERE c.climaId = :climaId"),
    @NamedQuery(name = "CatTipoClima.findByClimaDescripcion", query = "SELECT c FROM CatTipoClima c WHERE c.climaDescripcion = :climaDescripcion")})
public class CatTipoClima implements Serializable {
    @JoinTable(name = "PREGUNTA_CLIMA", joinColumns = {
        @JoinColumn(name = "CLIMA_ID", referencedColumnName = "CLIMA_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO"),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO"),
        @JoinColumn(name = "PREGUNTA_ID", referencedColumnName = "PREGUNTA_ID")})
    @ManyToMany
    private List<PreguntaProyecto> preguntaProyectoList;
    @OneToMany(mappedBy = "climaId")
    private List<PreguntaProyecto> preguntaProyectoList1;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLIMA_ID")
    private Short climaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "CLIMA_DESCRIPCION")
    private String climaDescripcion;

    public CatTipoClima() {
    }

    public CatTipoClima(Short climaId) {
        this.climaId = climaId;
    }

    public CatTipoClima(Short climaId, String climaDescripcion) {
        this.climaId = climaId;
        this.climaDescripcion = climaDescripcion;
    }

    public Short getClimaId() {
        return climaId;
    }

    public void setClimaId(Short climaId) {
        this.climaId = climaId;
    }

    public String getClimaDescripcion() {
        return climaDescripcion;
    }

    public void setClimaDescripcion(String climaDescripcion) {
        this.climaDescripcion = climaDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (climaId != null ? climaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoClima)) {
            return false;
        }
        CatTipoClima other = (CatTipoClima) object;
        if ((this.climaId == null && other.climaId != null) || (this.climaId != null && !this.climaId.equals(other.climaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatTipoClima[ climaId=" + climaId + " ]";
    }

    @XmlTransient
    public List<PreguntaProyecto> getPreguntaProyectoList() {
        return preguntaProyectoList;
    }

    public void setPreguntaProyectoList(List<PreguntaProyecto> preguntaProyectoList) {
        this.preguntaProyectoList = preguntaProyectoList;
    }

    @XmlTransient
    public List<PreguntaProyecto> getPreguntaProyectoList1() {
        return preguntaProyectoList1;
    }

    public void setPreguntaProyectoList1(List<PreguntaProyecto> preguntaProyectoList1) {
        this.preguntaProyectoList1 = preguntaProyectoList1;
    }
    
}
