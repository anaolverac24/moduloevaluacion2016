/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class OeRegionales1PK implements Serializable {
    @Basic(optional = false)
    @Column(name = "NUM_FOLIO")
    private String numFolio;
    @Basic(optional = false)
    @Column(name = "CVE_PROY")
    private String cveProy;
    @Basic(optional = false)
    @Column(name = "CVE_AREA")
    private String cveArea;
    @Basic(optional = false)
    @Column(name = "VERSION")
    private short version;
    @Basic(optional = true)
    @Column(name = "IDR")
    private BigInteger idr;

    public OeRegionales1PK() {
    }

    public OeRegionales1PK(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.numFolio = numFolio;
        this.cveProy = cveProy;
        this.cveArea = cveArea;
        this.version = version;
        this.idr = idr;
    }

    public String getNumFolio() {
        return numFolio;
    }

    public void setNumFolio(String numFolio) {
        this.numFolio = numFolio;
    }

    public String getCveProy() {
        return cveProy;
    }

    public void setCveProy(String cveProy) {
        this.cveProy = cveProy;
    }

    public String getCveArea() {
        return cveArea;
    }

    public void setCveArea(String cveArea) {
        this.cveArea = cveArea;
    }

    public short getVersion() {
        return version;
    }

    public void setVersion(short version) {
        this.version = version;
    }

    public BigInteger getIdr() {
        return idr;
    }

    public void setIdr(BigInteger idr) {
        this.idr = idr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numFolio != null ? numFolio.hashCode() : 0);
        hash += (cveProy != null ? cveProy.hashCode() : 0);
        hash += (cveArea != null ? cveArea.hashCode() : 0);
        hash += (int) version;
        hash += (idr != null ? idr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OeRegionales1PK)) {
            return false;
        }
        OeRegionales1PK other = (OeRegionales1PK) object;
        if ((this.numFolio == null && other.numFolio != null) || (this.numFolio != null && !this.numFolio.equals(other.numFolio))) {
            return false;
        }
        if ((this.cveProy == null && other.cveProy != null) || (this.cveProy != null && !this.cveProy.equals(other.cveProy))) {
            return false;
        }
        if ((this.cveArea == null && other.cveArea != null) || (this.cveArea != null && !this.cveArea.equals(other.cveArea))) {
            return false;
        }
        if (this.version != other.version) {
            return false;
        }
        if ((this.idr == null && other.idr != null) || (this.idr != null && !this.idr.equals(other.idr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.OeRegionales1PK[ numFolio=" + numFolio + ", cveProy=" + cveProy + ", cveArea=" + cveArea + ", version=" + version + ", idr=" + idr + " ]";
    }
    
}
