/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class DocumentoRespuestaDgiraPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CLAVE_TRAMITE")
    private String claveTramite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TRAMITE")
    private int idTramite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_DOC_ID")
    private short tipoDocId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ID_ENTIDAD_FEDERATIVA")
    private String idEntidadFederativa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "DOCUMENTO_ID")
    private String documentoId;

    public DocumentoRespuestaDgiraPK() {
    }

    public DocumentoRespuestaDgiraPK(String bitacoraProyecto, String claveTramite, int idTramite, short tipoDocId, String idEntidadFederativa, String documentoId) {
        this.bitacoraProyecto = bitacoraProyecto;
        this.claveTramite = claveTramite;
        this.idTramite = idTramite;
        this.tipoDocId = tipoDocId;
        this.idEntidadFederativa = idEntidadFederativa;
        this.documentoId = documentoId;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(String claveTramite) {
        this.claveTramite = claveTramite;
    }

    public int getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(int idTramite) {
        this.idTramite = idTramite;
    }

    public short getTipoDocId() {
        return tipoDocId;
    }

    public void setTipoDocId(short tipoDocId) {
        this.tipoDocId = tipoDocId;
    }

    public String getIdEntidadFederativa() {
        return idEntidadFederativa;
    }

    public void setIdEntidadFederativa(String idEntidadFederativa) {
        this.idEntidadFederativa = idEntidadFederativa;
    }

    public String getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(String documentoId) {
        this.documentoId = documentoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacoraProyecto != null ? bitacoraProyecto.hashCode() : 0);
        hash += (claveTramite != null ? claveTramite.hashCode() : 0);
        hash += (int) idTramite;
        hash += (int) tipoDocId;
        hash += (idEntidadFederativa != null ? idEntidadFederativa.hashCode() : 0);
        hash += (documentoId != null ? documentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoRespuestaDgiraPK)) {
            return false;
        }
        DocumentoRespuestaDgiraPK other = (DocumentoRespuestaDgiraPK) object;
        if ((this.bitacoraProyecto == null && other.bitacoraProyecto != null) || (this.bitacoraProyecto != null && !this.bitacoraProyecto.equals(other.bitacoraProyecto))) {
            return false;
        }
        if ((this.claveTramite == null && other.claveTramite != null) || (this.claveTramite != null && !this.claveTramite.equals(other.claveTramite))) {
            return false;
        }
        if (this.idTramite != other.idTramite) {
            return false;
        }
        if (this.tipoDocId != other.tipoDocId) {
            return false;
        }
        if ((this.idEntidadFederativa == null && other.idEntidadFederativa != null) || (this.idEntidadFederativa != null && !this.idEntidadFederativa.equals(other.idEntidadFederativa))) {
            return false;
        }
        if ((this.documentoId == null && other.documentoId != null) || (this.documentoId != null && !this.documentoId.equals(other.documentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.DocumentoRespuestaDgiraPK[ bitacoraProyecto=" + bitacoraProyecto + ", claveTramite=" + claveTramite + ", idTramite=" + idTramite + ", tipoDocId=" + tipoDocId + ", idEntidadFederativa=" + idEntidadFederativa + ", documentoId=" + documentoId + " ]";
    }
    
}
