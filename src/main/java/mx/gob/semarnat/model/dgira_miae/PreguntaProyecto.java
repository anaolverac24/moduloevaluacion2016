/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "PREGUNTA_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaProyecto.findAll", query = "SELECT p FROM PreguntaProyecto p"),
    @NamedQuery(name = "PreguntaProyecto.findByFolioProyecto", query = "SELECT p FROM PreguntaProyecto p WHERE p.preguntaProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PreguntaProyecto.findBySerialProyecto", query = "SELECT p FROM PreguntaProyecto p WHERE p.preguntaProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PreguntaProyecto.findByPreguntaId", query = "SELECT p FROM PreguntaProyecto p WHERE p.preguntaProyectoPK.preguntaId = :preguntaId"),
    @NamedQuery(name = "PreguntaProyecto.findByPreguntaRespuesta", query = "SELECT p FROM PreguntaProyecto p WHERE p.preguntaRespuesta = :preguntaRespuesta")})
public class PreguntaProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreguntaProyectoPK preguntaProyectoPK;
    @Column(name = "PREGUNTA_RESPUESTA")
    private Character preguntaRespuesta;
    @ManyToMany(mappedBy = "preguntaProyectoList")
    private List<CatTipoClima> catTipoClimaList;
    @JoinTable(name = "PREGUNTA_VEGETACION", joinColumns = {
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO"),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO"),
        @JoinColumn(name = "PREGUNTA_ID", referencedColumnName = "PREGUNTA_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "VEGETACION_ID", referencedColumnName = "VEGETACION_ID")})
    @ManyToMany
    private List<CatTipoVegetacion> catTipoVegetacionList;
    @JoinColumn(name = "PREGUNTA_ID", referencedColumnName = "PREGUNTA_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatPregunta catPregunta;
    @JoinColumn(name = "CLIMA_ID", referencedColumnName = "CLIMA_ID")
    @ManyToOne
    private CatTipoClima climaId;
    @JoinColumn(name = "VEGETACION_ID", referencedColumnName = "VEGETACION_ID")
    @ManyToOne
    private CatTipoVegetacion vegetacionId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public PreguntaProyecto() {
    }

    public PreguntaProyecto(PreguntaProyectoPK preguntaProyectoPK) {
        this.preguntaProyectoPK = preguntaProyectoPK;
    }

    public PreguntaProyecto(String folioProyecto, short serialProyecto, short preguntaId) {
        this.preguntaProyectoPK = new PreguntaProyectoPK(folioProyecto, serialProyecto, preguntaId);
    }

    public PreguntaProyectoPK getPreguntaProyectoPK() {
        return preguntaProyectoPK;
    }

    public void setPreguntaProyectoPK(PreguntaProyectoPK preguntaProyectoPK) {
        this.preguntaProyectoPK = preguntaProyectoPK;
    }

    public Character getPreguntaRespuesta() {
        return preguntaRespuesta;
    }

    public void setPreguntaRespuesta(Character preguntaRespuesta) {
        this.preguntaRespuesta = preguntaRespuesta;
    }

    @XmlTransient
    public List<CatTipoClima> getCatTipoClimaList() {
        return catTipoClimaList;
    }

    public void setCatTipoClimaList(List<CatTipoClima> catTipoClimaList) {
        this.catTipoClimaList = catTipoClimaList;
    }

    @XmlTransient
    public List<CatTipoVegetacion> getCatTipoVegetacionList() {
        return catTipoVegetacionList;
    }

    public void setCatTipoVegetacionList(List<CatTipoVegetacion> catTipoVegetacionList) {
        this.catTipoVegetacionList = catTipoVegetacionList;
    }

    public CatPregunta getCatPregunta() {
        return catPregunta;
    }

    public void setCatPregunta(CatPregunta catPregunta) {
        this.catPregunta = catPregunta;
    }

    public CatTipoClima getClimaId() {
        return climaId;
    }

    public void setClimaId(CatTipoClima climaId) {
        this.climaId = climaId;
    }

    public CatTipoVegetacion getVegetacionId() {
        return vegetacionId;
    }

    public void setVegetacionId(CatTipoVegetacion vegetacionId) {
        this.vegetacionId = vegetacionId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaProyectoPK != null ? preguntaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaProyecto)) {
            return false;
        }
        PreguntaProyecto other = (PreguntaProyecto) object;
        if ((this.preguntaProyectoPK == null && other.preguntaProyectoPK != null) || (this.preguntaProyectoPK != null && !this.preguntaProyectoPK.equals(other.preguntaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.PreguntaProyecto[ preguntaProyectoPK=" + preguntaProyectoPK + " ]";
    }
    
}
