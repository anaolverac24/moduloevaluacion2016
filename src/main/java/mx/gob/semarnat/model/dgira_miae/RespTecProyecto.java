/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Case Solutions 10
 */
@Entity
@Table(name = "RESP_TEC_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RespTecProyecto.findAll", query = "SELECT r FROM RespTecProyecto r"),
    @NamedQuery(name = "RespTecProyecto.findByFolioProyecto", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "RespTecProyecto.findBySerialProyecto", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "RespTecProyecto.findByRespTecNombre", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecNombre = :respTecNombre"),
    @NamedQuery(name = "RespTecProyecto.findByRespTecApellido1", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecApellido1 = :respTecApellido1"),
    @NamedQuery(name = "RespTecProyecto.findByRespTecApellido2", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecApellido2 = :respTecApellido2"),
    @NamedQuery(name = "RespTecProyecto.findByRespTecCartaVerdad", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecCartaVerdad = :respTecCartaVerdad"),
    @NamedQuery(name = "RespTecProyecto.findByRespTecRfc", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecRfc = :respTecRfc"),
    @NamedQuery(name = "RespTecProyecto.findByRespTecCurp", query = "SELECT r FROM RespTecProyecto r WHERE r.respTecCurp = :respTecCurp")})
public class RespTecProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RespTecProyectoPK respTecProyectoPK;
    @Column(name = "RESP_TEC_NOMBRE")
    private String respTecNombre;
    @Column(name = "RESP_TEC_APELLIDO_1")
    private String respTecApellido1;
    @Column(name = "RESP_TEC_APELLIDO_2")
    private String respTecApellido2;
    @Column(name = "RESP_TEC_CARTA_VERDAD")
    private String respTecCartaVerdad;
    @Column(name = "RESP_TEC_RFC")
    private String respTecRfc;
    @Column(name = "RESP_TEC_CURP")
    private String respTecCurp;
//    @JoinColumns({
//        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
//        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
//    @OneToOne(optional = false)
//    private Proyecto proyecto;

    public RespTecProyecto() {
    }

    public RespTecProyecto(RespTecProyectoPK respTecProyectoPK) {
        this.respTecProyectoPK = respTecProyectoPK;
    }

    public RespTecProyecto(String folioProyecto, short serialProyecto) {
        this.respTecProyectoPK = new RespTecProyectoPK(folioProyecto, serialProyecto);
    }

    public RespTecProyectoPK getRespTecProyectoPK() {
        return respTecProyectoPK;
    }

    public void setRespTecProyectoPK(RespTecProyectoPK respTecProyectoPK) {
        this.respTecProyectoPK = respTecProyectoPK;
    }

    public String getRespTecNombre() {
        return respTecNombre;
    }

    public void setRespTecNombre(String respTecNombre) {
        this.respTecNombre = respTecNombre;
    }

    public String getRespTecApellido1() {
        return respTecApellido1;
    }

    public void setRespTecApellido1(String respTecApellido1) {
        this.respTecApellido1 = respTecApellido1;
    }

    public String getRespTecApellido2() {
        return respTecApellido2;
    }

    public void setRespTecApellido2(String respTecApellido2) {
        this.respTecApellido2 = respTecApellido2;
    }

    public String getRespTecCartaVerdad() {
        return respTecCartaVerdad;
    }

    public void setRespTecCartaVerdad(String respTecCartaVerdad) {
        this.respTecCartaVerdad = respTecCartaVerdad;
    }

    public String getRespTecRfc() {
        return respTecRfc;
    }

    public void setRespTecRfc(String respTecRfc) {
        this.respTecRfc = respTecRfc;
    }

    public String getRespTecCurp() {
        return respTecCurp;
    }

    public void setRespTecCurp(String respTecCurp) {
        this.respTecCurp = respTecCurp;
    }

//    public Proyecto getProyecto() {
//        return proyecto;
//    }
//
//    public void setProyecto(Proyecto proyecto) {
//        this.proyecto = proyecto;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (respTecProyectoPK != null ? respTecProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespTecProyecto)) {
            return false;
        }
        RespTecProyecto other = (RespTecProyecto) object;
        if ((this.respTecProyectoPK == null && other.respTecProyectoPK != null) || (this.respTecProyectoPK != null && !this.respTecProyectoPK.equals(other.respTecProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.RespTecProyecto[ respTecProyectoPK=" + respTecProyectoPK + " ]";
    }
    
}
