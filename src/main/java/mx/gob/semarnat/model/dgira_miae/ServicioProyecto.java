/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author EdgarSC
 */
@Entity
@Table(name = "SERVICIO_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name = "ServicioProyecto.findAll", query = "SELECT s FROM ServicioProyecto s"),
	@NamedQuery(name = "ServicioProyecto.findByFolioProyecto", query = "SELECT s FROM ServicioProyecto s WHERE s.servicioProyectoPK.folioProyecto = :folioProyecto"),
	@NamedQuery(name = "ServicioProyecto.findBySerialProyecto", query = "SELECT s FROM ServicioProyecto s WHERE s.servicioProyectoPK.serialProyecto = :serialProyecto"),
	@NamedQuery(name = "ServicioProyecto.findByServicioProyId", query = "SELECT s FROM ServicioProyecto s WHERE s.servicioProyectoPK.servicioProyId = :servicioProyId"),
	@NamedQuery(name = "ServicioProyecto.findByEtapaId", query = "SELECT s FROM ServicioProyecto s WHERE s.etapaId = :etapaId"),
	@NamedQuery(name = "ServicioProyecto.findByServicioNombre", query = "SELECT s FROM ServicioProyecto s WHERE s.servicioNombre = :servicioNombre"),
	@NamedQuery(name = "ServicioProyecto.findByServicioDisponible", query = "SELECT s FROM ServicioProyecto s WHERE s.servicioDisponible = :servicioDisponible"),
	@NamedQuery(name = "ServicioProyecto.findByServicioSuministrado", query = "SELECT s FROM ServicioProyecto s WHERE s.servicioSuministrado = :servicioSuministrado"),
	@NamedQuery(name = "ServicioProyecto.findByServicioDescripcion", query = "SELECT s FROM ServicioProyecto s WHERE s.servicioDescripcion = :servicioDescripcion")})
public class ServicioProyecto implements Serializable {
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected ServicioProyectoPK servicioProyectoPK;
	@Column(name = "ETAPA_ID")
	private short etapaId;
	@Column(name = "SERVICIO_NOMBRE")
	private String servicioNombre;
	@Column(name = "SERVICIO_DISPONIBLE")
	private String servicioDisponible;
	@Column(name = "SERVICIO_SUMINISTRADO")
	private String servicioSuministrado;
	@Column(name = "SERVICIO_DESCRIPCION")
	private String servicioDescripcion;
	@JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false)
	@ManyToOne(optional = false)
	private CatEtapa catEtapa;
	@JoinColumn(name = "SERVICIO_ID", referencedColumnName = "SERVICIO_ID")
	@ManyToOne(optional = false)
	private CatServicio servicioId;
	@JoinColumns({
		@JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
		@JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
	@ManyToOne(optional = false)
	private Proyecto proyecto;


	public ServicioProyecto() {
	}

	public ServicioProyecto(ServicioProyectoPK servicioProyectoPK) {
		this.servicioProyectoPK = servicioProyectoPK;
	}

	public ServicioProyecto(String folioProyecto, short serialProyecto, short servicioProyId) {
		this.servicioProyectoPK = new ServicioProyectoPK(folioProyecto, serialProyecto, servicioProyId);
	}

	public ServicioProyectoPK getServicioProyectoPK() {
		return servicioProyectoPK;
	}

	public void setServicioProyectoPK(ServicioProyectoPK servicioProyectoPK) {
		this.servicioProyectoPK = servicioProyectoPK;
	}

	public String getServicioNombre() {
		return servicioNombre;
	}

	public void setServicioNombre(String servicioNombre) {
		this.servicioNombre = servicioNombre;
	}

	public String getServicioDisponible() {
		return servicioDisponible;
	}
	
	public String getServicioDisponibleCompleta() {
		if (servicioDisponible != null) {
			if (servicioDisponible.equals("S")) {
				return "Si";
			}
			if (servicioDisponible.equals("N")) {
				return "No";
			}
		}
		return servicioDisponible;
	}

	public void setServicioDisponible(String servicioDisponible) {
		this.servicioDisponible = servicioDisponible;
	}

	public String getServicioSuministrado() {
		return servicioSuministrado;
	}

	public void setServicioSuministrado(String servicioSuministrado) {
		this.servicioSuministrado = servicioSuministrado;
	}

	public String getServicioDescripcion() {

		return servicioDescripcion;

	}

	public String getServicioDescripcionCorta(){
		if(servicioDescripcion.length()>199){
			return servicioDescripcion.substring(0, 199);
		}else{
			return servicioDescripcion;
		}
	}
	public void setServicioDescripcion(String servicioDescripcion) {
		this.servicioDescripcion = servicioDescripcion;
	}

	public CatEtapa getCatEtapa() {
		return catEtapa;
	}

	public void setCatEtapa(CatEtapa catEtapa) {
		this.catEtapa = catEtapa;
	}

	public CatServicio getServicioId() {
		return servicioId;
	}

	public void setServicioId(CatServicio servicioId) {
		this.servicioId = servicioId;
	}

	public Proyecto getProyecto() {
		return proyecto;
	}

	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (servicioProyectoPK != null ? servicioProyectoPK.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ServicioProyecto)) {
			return false;
		}
	ServicioProyecto other = (ServicioProyecto) object;
	if ((this.servicioProyectoPK == null && other.servicioProyectoPK != null) || (this.servicioProyectoPK != null && !this.servicioProyectoPK.equals(other.servicioProyectoPK))) {
		return false;
	}
	return true;
	}

	@Override
	public String toString() {
		return "mx.gob.semarnat.mia.model.ServicioProyecto[ servicioProyectoPK=" + servicioProyectoPK + " ]";
	}

	public short getEtapaId() {
		return etapaId;
	}

	public void setEtapaId(short etapaId) {
		this.etapaId = etapaId;
	}

}
