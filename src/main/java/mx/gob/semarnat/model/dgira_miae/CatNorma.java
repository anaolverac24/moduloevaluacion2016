/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_NORMA", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatNorma.findAll", query = "SELECT c FROM CatNorma c"),
    @NamedQuery(name = "CatNorma.findByNormaId", query = "SELECT c FROM CatNorma c WHERE c.normaId = :normaId"),
    @NamedQuery(name = "CatNorma.findByNormaDescripcion", query = "SELECT c FROM CatNorma c WHERE c.normaDescripcion = :normaDescripcion"),
    @NamedQuery(name = "CatNorma.findByTipoTramite", query = "SELECT c FROM CatNorma c WHERE c.tipoTramite = :tipoTramite"),
    @NamedQuery(name = "CatNorma.findByNormaDinamica", query = "SELECT c FROM CatNorma c WHERE c.normaDinamica = :normaDinamica"),
    @NamedQuery(name = "CatNorma.findByNormaNombre", query = "SELECT c FROM CatNorma c WHERE c.normaNombre = :normaNombre"),
    @NamedQuery(name = "CatNorma.findByNormaLink", query = "SELECT c FROM CatNorma c WHERE c.normaLink = :normaLink"),
    @NamedQuery(name = "CatNorma.findByNormaFechapublicacion", query = "SELECT c FROM CatNorma c WHERE c.normaFechapublicacion = :normaFechapublicacion")})
public class CatNorma implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NORMA_ID")
    private Short normaId;
    @Column(name = "NORMA_DESCRIPCION")
    private String normaDescripcion;
    @Column(name = "TIPO_TRAMITE")
    private Character tipoTramite;
    @Column(name = "NORMA_DINAMICA")
    private Character normaDinamica;
    @Column(name = "NORMA_NOMBRE")
    private String normaNombre;
    @Column(name = "NORMA_LINK")
    private String normaLink;
    @Column(name = "NORMA_FECHAPUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date normaFechapublicacion;
    @Column(name = "NORMA_NOMBRECORTO")
    private String normaNombreCorto;
    
    
    public CatNorma() {
    }

    public CatNorma(Short normaId) {
        this.normaId = normaId;
    }

    public Short getNormaId() {
        return normaId;
    }

    public void setNormaId(Short normaId) {
        this.normaId = normaId;
    }

    public String getNormaDescripcion() {
        return normaDescripcion;
    }

    public void setNormaDescripcion(String normaDescripcion) {
        this.normaDescripcion = normaDescripcion;
    }

    public Character getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(Character tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public Character getNormaDinamica() {
        return normaDinamica;
    }

    public void setNormaDinamica(Character normaDinamica) {
        this.normaDinamica = normaDinamica;
    }

    public String getNormaNombre() {
        return normaNombre;
    }

    public void setNormaNombre(String normaNombre) {
        this.normaNombre = normaNombre;
    }

    public String getNormaLink() {
        return normaLink;
    }

    public void setNormaLink(String normaLink) {
        this.normaLink = normaLink;
    }

    public Date getNormaFechapublicacion() {
        return normaFechapublicacion;
    }

    public void setNormaFechapublicacion(Date normaFechapublicacion) {
        this.normaFechapublicacion = normaFechapublicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (normaId != null ? normaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatNorma)) {
            return false;
        }
        CatNorma other = (CatNorma) object;
        if ((this.normaId == null && other.normaId != null) || (this.normaId != null && !this.normaId.equals(other.normaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatNorma[ normaId=" + normaId + " ]";
    }

	/**
	 * @return the normaNombreCorto
	 */
	public String getNormaNombreCorto() {
		return normaNombreCorto;
	}

	/**
	 * @param normaNombreCorto the normaNombreCorto to set
	 */
	public void setNormaNombreCorto(String normaNombreCorto) {
		this.normaNombreCorto = normaNombreCorto;
	}
}
