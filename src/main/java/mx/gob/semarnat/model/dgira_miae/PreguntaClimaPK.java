/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Admin
 */
@Embeddable
public class PreguntaClimaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "PREGUNTA_ID")
    private short preguntaId;
    @Basic(optional = false)
    @Column(name = "CLIMA_ID")
    private short climaId;

    public PreguntaClimaPK() {
    }

    public PreguntaClimaPK(String folioProyecto, short serialProyecto, short preguntaId, short climaId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.preguntaId = preguntaId;
        this.climaId = climaId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getPreguntaId() {
        return preguntaId;
    }

    public void setPreguntaId(short preguntaId) {
        this.preguntaId = preguntaId;
    }

    public short getClimaId() {
        return climaId;
    }

    public void setClimaId(short climaId) {
        this.climaId = climaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) preguntaId;
        hash += (int) climaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaClimaPK)) {
            return false;
        }
        PreguntaClimaPK other = (PreguntaClimaPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.preguntaId != other.preguntaId) {
            return false;
        }
        if (this.climaId != other.climaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "package mx.gob.semarnat.model.dgira_miae.PreguntaClimaPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", preguntaId=" + preguntaId + ", climaId=" + climaId + " ]";
    }
    
}
