/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "OE_REGIONALES_1", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OeRegionales1.findAll", query = "SELECT o FROM OeRegionales1 o"),
    @NamedQuery(name = "OeRegionales1.findByNumFolio", query = "SELECT o FROM OeRegionales1 o WHERE o.oeRegionales1PK.numFolio = :numFolio"),
    @NamedQuery(name = "OeRegionales1.findByCveProy", query = "SELECT o FROM OeRegionales1 o WHERE o.oeRegionales1PK.cveProy = :cveProy"),
    @NamedQuery(name = "OeRegionales1.findByCveArea", query = "SELECT o FROM OeRegionales1 o WHERE o.oeRegionales1PK.cveArea = :cveArea"),
    @NamedQuery(name = "OeRegionales1.findByVersion", query = "SELECT o FROM OeRegionales1 o WHERE o.oeRegionales1PK.version = :version"),
    @NamedQuery(name = "OeRegionales1.findByNomOe", query = "SELECT o FROM OeRegionales1 o WHERE o.nomOe = :nomOe"),
    @NamedQuery(name = "OeRegionales1.findByTipoOe", query = "SELECT o FROM OeRegionales1 o WHERE o.tipoOe = :tipoOe"),
    @NamedQuery(name = "OeRegionales1.findByUga", query = "SELECT o FROM OeRegionales1 o WHERE o.uga = :uga"),
    @NamedQuery(name = "OeRegionales1.findByUgaComp", query = "SELECT o FROM OeRegionales1 o WHERE o.ugaComp = :ugaComp"),
    @NamedQuery(name = "OeRegionales1.findByPolitica", query = "SELECT o FROM OeRegionales1 o WHERE o.politica = :politica"),
    @NamedQuery(name = "OeRegionales1.findByPolMapear", query = "SELECT o FROM OeRegionales1 o WHERE o.polMapear = :polMapear"),
    @NamedQuery(name = "OeRegionales1.findByUsoPred", query = "SELECT o FROM OeRegionales1 o WHERE o.usoPred = :usoPred"),
    @NamedQuery(name = "OeRegionales1.findByCriterios", query = "SELECT o FROM OeRegionales1 o WHERE o.criterios = :criterios"),
    @NamedQuery(name = "OeRegionales1.findBySupEa", query = "SELECT o FROM OeRegionales1 o WHERE o.supEa = :supEa"),
    @NamedQuery(name = "OeRegionales1.findByProy", query = "SELECT o FROM OeRegionales1 o WHERE o.proy = :proy"),
    @NamedQuery(name = "OeRegionales1.findByComp", query = "SELECT o FROM OeRegionales1 o WHERE o.comp = :comp"),
    @NamedQuery(name = "OeRegionales1.findByDescrip", query = "SELECT o FROM OeRegionales1 o WHERE o.descrip = :descrip"),
    @NamedQuery(name = "OeRegionales1.findByAreabuffer", query = "SELECT o FROM OeRegionales1 o WHERE o.areabuffer = :areabuffer"),
    @NamedQuery(name = "OeRegionales1.findByArea", query = "SELECT o FROM OeRegionales1 o WHERE o.area = :area"),
    @NamedQuery(name = "OeRegionales1.findByFechaHora", query = "SELECT o FROM OeRegionales1 o WHERE o.fechaHora = :fechaHora"),
    @NamedQuery(name = "OeRegionales1.findByFolioCveVer", query = "SELECT e FROM OeRegionales1 e WHERE e.oeRegionales1PK.numFolio = :folio and e.oeRegionales1PK.version = :version"),
    @NamedQuery(name = "OeRegionales1.findByIdr", query = "SELECT o FROM OeRegionales1 o WHERE o.oeRegionales1PK.idr = :idr")})
public class OeRegionales1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OeRegionales1PK oeRegionales1PK;
    @Column(name = "NOM_OE")
    private String nomOe;
    @Column(name = "TIPO_OE")
    private String tipoOe;
    @Column(name = "UGA")
    private String uga;
    @Column(name = "UGA_COMP")
    private String ugaComp;
    @Column(name = "POLITICA")
    private String politica;
    @Column(name = "POL_MAPEAR")
    private String polMapear;
    @Column(name = "USO_PRED")
    private String usoPred;
    @Column(name = "CRITERIOS")
    private String criterios;
    @Column(name = "SUP_EA")
    private Double supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private Double areabuffer;
    @Column(name = "AREA")
    private Double area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public OeRegionales1() {
    }

    public OeRegionales1(OeRegionales1PK oeRegionales1PK) {
        this.oeRegionales1PK = oeRegionales1PK;
    }

    public OeRegionales1(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.oeRegionales1PK = new OeRegionales1PK(numFolio, cveProy, cveArea, version, idr);
    }

    public OeRegionales1PK getOeRegionales1PK() {
        return oeRegionales1PK;
    }

    public void setOeRegionales1PK(OeRegionales1PK oeRegionales1PK) {
        this.oeRegionales1PK = oeRegionales1PK;
    }

    public String getNomOe() {
        return nomOe;
    }

    public void setNomOe(String nomOe) {
        this.nomOe = nomOe;
    }

    public String getTipoOe() {
        return tipoOe;
    }

    public void setTipoOe(String tipoOe) {
        this.tipoOe = tipoOe;
    }

    public String getUga() {
        return uga;
    }

    public void setUga(String uga) {
        this.uga = uga;
    }

    public String getUgaComp() {
        return ugaComp;
    }

    public void setUgaComp(String ugaComp) {
        this.ugaComp = ugaComp;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

    public String getPolMapear() {
        return polMapear;
    }

    public void setPolMapear(String polMapear) {
        this.polMapear = polMapear;
    }

    public String getUsoPred() {
        return usoPred;
    }

    public void setUsoPred(String usoPred) {
        this.usoPred = usoPred;
    }

    public String getCriterios() {
        return criterios;
    }

    public void setCriterios(String criterios) {
        this.criterios = criterios;
    }

    public Double getSupEa() {
        return supEa;
    }

    public void setSupEa(Double supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Double getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(Double areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oeRegionales1PK != null ? oeRegionales1PK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OeRegionales1)) {
            return false;
        }
        OeRegionales1 other = (OeRegionales1) object;
        if ((this.oeRegionales1PK == null && other.oeRegionales1PK != null) || (this.oeRegionales1PK != null && !this.oeRegionales1PK.equals(other.oeRegionales1PK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.OeRegionales1[ oeRegionales1PK=" + oeRegionales1PK + " ]";
    }
    
}
