/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Rengerden
 */
@Embeddable
public class CatDerechosCriteriosvaloresPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_CRITERIO")
    private Integer idCriterio;
    @Basic(optional = false)
    @Column(name = "ID_VALOR")
    private Integer idValor;

    public CatDerechosCriteriosvaloresPK() {
    }

    public CatDerechosCriteriosvaloresPK(Integer idCriterio, Integer idValor) {
        this.idCriterio = idCriterio;
        this.idValor = idValor;
    }

    public Integer getIdCriterio() {
        return idCriterio;
    }

    public void setIdCriterio(Integer idCriterio) {
        this.idCriterio = idCriterio;
    }

    public Integer getIdValor() {
        return idValor;
    }

    public void setIdValor(Integer idValor) {
        this.idValor = idValor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCriterio != null ? idCriterio.hashCode() : 0);
        hash += (idValor != null ? idValor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatDerechosCriteriosvaloresPK)) {
            return false;
        }
        CatDerechosCriteriosvaloresPK other = (CatDerechosCriteriosvaloresPK) object;
        if ((this.idCriterio == null && other.idCriterio != null) || (this.idCriterio != null && !this.idCriterio.equals(other.idCriterio))) {
            return false;
        }
        if ((this.idValor == null && other.idValor != null) || (this.idValor != null && !this.idValor.equals(other.idValor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CatDerechosCriteriosvaloresPK[ idCriterio=" + idCriterio + ", idValor=" + idValor + " ]";
    }
    
}
