/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_SECCION", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatSeccion.findAll", query = "SELECT c FROM CatSeccion c"),
    @NamedQuery(name = "CatSeccion.findByIdTramite", query = "SELECT c FROM CatSeccion c WHERE c.catSeccionPK.idTramite = :idTramite"),
    @NamedQuery(name = "CatSeccion.findByNsub", query = "SELECT c FROM CatSeccion c WHERE c.catSeccionPK.nsub = :nsub"),
    @NamedQuery(name = "CatSeccion.findByCapituloId", query = "SELECT c FROM CatSeccion c WHERE c.catSeccionPK.capituloId = :capituloId"),
    @NamedQuery(name = "CatSeccion.findBySubcapituloId", query = "SELECT c FROM CatSeccion c WHERE c.catSeccionPK.subcapituloId = :subcapituloId"),
    @NamedQuery(name = "CatSeccion.findBySeccionId", query = "SELECT c FROM CatSeccion c WHERE c.catSeccionPK.seccionId = :seccionId"),
    @NamedQuery(name = "CatSeccion.findBySeccionDescripcion", query = "SELECT c FROM CatSeccion c WHERE c.seccionDescripcion = :seccionDescripcion"),
    @NamedQuery(name = "CatSeccion.findBySeccionAyuda", query = "SELECT c FROM CatSeccion c WHERE c.seccionAyuda = :seccionAyuda")})
public class CatSeccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatSeccionPK catSeccionPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "SECCION_DESCRIPCION")
    private String seccionDescripcion;
    @Size(max = 4000)
    @Column(name = "SECCION_AYUDA")
    private String seccionAyuda;
    @JoinColumns({
        @JoinColumn(name = "ID_TRAMITE", referencedColumnName = "ID_TRAMITE", insertable = false, updatable = false),
        @JoinColumn(name = "NSUB", referencedColumnName = "NSUB", insertable = false, updatable = false),
        @JoinColumn(name = "CAPITULO_ID", referencedColumnName = "CAPITULO_ID", insertable = false, updatable = false),
        @JoinColumn(name = "SUBCAPITULO_ID", referencedColumnName = "SUBCAPITULO_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CatSubcapitulo catSubcapitulo;

    public CatSeccion() {
    }

    public CatSeccion(CatSeccionPK catSeccionPK) {
        this.catSeccionPK = catSeccionPK;
    }

    public CatSeccion(CatSeccionPK catSeccionPK, String seccionDescripcion) {
        this.catSeccionPK = catSeccionPK;
        this.seccionDescripcion = seccionDescripcion;
    }

    public CatSeccion(int idTramite, short nsub, short capituloId, short subcapituloId, short seccionId) {
        this.catSeccionPK = new CatSeccionPK(idTramite, nsub, capituloId, subcapituloId, seccionId);
    }

    public CatSeccionPK getCatSeccionPK() {
        return catSeccionPK;
    }

    public void setCatSeccionPK(CatSeccionPK catSeccionPK) {
        this.catSeccionPK = catSeccionPK;
    }

    public String getSeccionDescripcion() {
        return seccionDescripcion;
    }

    public void setSeccionDescripcion(String seccionDescripcion) {
        this.seccionDescripcion = seccionDescripcion;
    }

    public String getSeccionAyuda() {
        return seccionAyuda;
    }

    public void setSeccionAyuda(String seccionAyuda) {
        this.seccionAyuda = seccionAyuda;
    }

    public CatSubcapitulo getCatSubcapitulo() {
        return catSubcapitulo;
    }

    public void setCatSubcapitulo(CatSubcapitulo catSubcapitulo) {
        this.catSubcapitulo = catSubcapitulo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catSeccionPK != null ? catSeccionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatSeccion)) {
            return false;
        }
        CatSeccion other = (CatSeccion) object;
        if ((this.catSeccionPK == null && other.catSeccionPK != null) || (this.catSeccionPK != null && !this.catSeccionPK.equals(other.catSeccionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatSeccion[ catSeccionPK=" + catSeccionPK + " ]";
    }
    
}
