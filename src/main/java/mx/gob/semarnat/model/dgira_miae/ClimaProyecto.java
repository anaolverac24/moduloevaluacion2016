/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CLIMA_PROYECTO",schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClimaProyecto.findAll", query = "SELECT c FROM ClimaProyecto c"),
    @NamedQuery(name = "ClimaProyecto.findByFolioProyecto", query = "SELECT c FROM ClimaProyecto c WHERE c.climaProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ClimaProyecto.findBySerialProyecto", query = "SELECT c FROM ClimaProyecto c WHERE c.climaProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ClimaProyecto.findByClaveClimatologica", query = "SELECT c FROM ClimaProyecto c WHERE c.claveClimatologica = :claveClimatologica"),
    @NamedQuery(name = "ClimaProyecto.findByGrupoMapa2", query = "SELECT c FROM ClimaProyecto c WHERE c.grupoMapa2 = :grupoMapa2"),
    @NamedQuery(name = "ClimaProyecto.findByDesTem", query = "SELECT c FROM ClimaProyecto c WHERE c.desTem = :desTem"),
    @NamedQuery(name = "ClimaProyecto.findByDescPrec", query = "SELECT c FROM ClimaProyecto c WHERE c.descPrec = :descPrec"),
    @NamedQuery(name = "ClimaProyecto.findByFolioSerial", query = "SELECT e FROM ClimaProyecto e  WHERE e.climaProyectoPK.folioProyecto = :folio and e.climaProyectoPK.serialProyecto = :serial"),
    @NamedQuery(name = "ClimaProyecto.findByDescripcion", query = "SELECT c FROM ClimaProyecto c WHERE c.descripcion = :descripcion")})
public class ClimaProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ClimaProyectoPK climaProyectoPK;
    @Column(name = "CLAVE_CLIMATOLOGICA")
    private String claveClimatologica;
    @Column(name = "GRUPO_MAPA2")
    private String grupoMapa2;
    @Column(name = "DES_TEM")
    private String desTem;
    @Column(name = "DESC_PREC")
    private String descPrec;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @OneToOne(optional = false)
    private Proyecto proyecto;

    public ClimaProyecto() {
    }

    public ClimaProyecto(ClimaProyectoPK climaProyectoPK) {
        this.climaProyectoPK = climaProyectoPK;
    }

    public ClimaProyecto(String folioProyecto, short serialProyecto) {
        this.climaProyectoPK = new ClimaProyectoPK(folioProyecto, serialProyecto);
    }

    public ClimaProyectoPK getClimaProyectoPK() {
        return climaProyectoPK;
    }

    public void setClimaProyectoPK(ClimaProyectoPK climaProyectoPK) {
        this.climaProyectoPK = climaProyectoPK;
    }

    public String getClaveClimatologica() {
        return claveClimatologica;
    }

    public void setClaveClimatologica(String claveClimatologica) {
        this.claveClimatologica = claveClimatologica;
    }

    public String getGrupoMapa2() {
        return grupoMapa2;
    }

    public void setGrupoMapa2(String grupoMapa2) {
        this.grupoMapa2 = grupoMapa2;
    }

    public String getDesTem() {
        return desTem;
    }

    public void setDesTem(String desTem) {
        this.desTem = desTem;
    }

    public String getDescPrec() {
        return descPrec;
    }

    public void setDescPrec(String descPrec) {
        this.descPrec = descPrec;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (climaProyectoPK != null ? climaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClimaProyecto)) {
            return false;
        }
        ClimaProyecto other = (ClimaProyecto) object;
        if ((this.climaProyectoPK == null && other.climaProyectoPK != null) || (this.climaProyectoPK != null && !this.climaProyectoPK.equals(other.climaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ClimaProyecto[ climaProyectoPK=" + climaProyectoPK + " ]";
    }
    
}
