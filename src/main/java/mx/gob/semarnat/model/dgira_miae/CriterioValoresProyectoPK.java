/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rodrigo
 */
@Embeddable
public class CriterioValoresProyectoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CRITERIO")
    private BigInteger idCriterio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_VALOR")
    private BigInteger idValor;

    public CriterioValoresProyectoPK() {
    }

    public CriterioValoresProyectoPK(String folioProyecto, short serialProyecto, BigInteger idCriterio, BigInteger idValor) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.idCriterio = idCriterio;
        this.idValor = idValor;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public BigInteger getIdCriterio() {
        return idCriterio;
    }

    public void setIdCriterio(BigInteger idCriterio) {
        this.idCriterio = idCriterio;
    }

    public BigInteger getIdValor() {
        return idValor;
    }

    public void setIdValor(BigInteger idValor) {
        this.idValor = idValor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (idCriterio != null ? idCriterio.hashCode() : 0);
        hash += (idValor != null ? idValor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CriterioValoresProyectoPK)) {
            return false;
        }
        CriterioValoresProyectoPK other = (CriterioValoresProyectoPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if ((this.idCriterio == null && other.idCriterio != null) || (this.idCriterio != null && !this.idCriterio.equals(other.idCriterio))) {
            return false;
        }
        if ((this.idValor == null && other.idValor != null) || (this.idValor != null && !this.idValor.equals(other.idValor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.CriterioValoresProyectoPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", idCriterio=" + idCriterio + ", idValor=" + idValor + " ]";
    }
    
}
