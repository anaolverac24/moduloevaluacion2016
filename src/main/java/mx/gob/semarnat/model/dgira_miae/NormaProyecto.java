/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "NORMA_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NormaProyecto.findAll", query = "SELECT n FROM NormaProyecto n"),
    @NamedQuery(name = "NormaProyecto.findByFolioProyecto", query = "SELECT n FROM NormaProyecto n WHERE n.normaProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "NormaProyecto.findBySerialProyecto", query = "SELECT n FROM NormaProyecto n WHERE n.normaProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "NormaProyecto.findByNormaProyId", query = "SELECT n FROM NormaProyecto n WHERE n.normaProyectoPK.normaProyId = :normaProyId"),
    @NamedQuery(name = "NormaProyecto.findByFolioSerial", query = "SELECT e FROM NormaProyecto e WHERE e.normaProyectoPK.folioProyecto = :folio and e.normaProyectoPK.serialProyecto = :serial"),
    @NamedQuery(name = "NormaProyecto.findByNormaFechPublic", query = "SELECT n FROM NormaProyecto n WHERE n.normaFechPublic = :normaFechPublic")})
public class NormaProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NormaProyectoPK normaProyectoPK;
    @Column(name = "NORMA_FECH_PUBLIC")
    private String normaFechPublic;
    @Column(name = "NORMA_DESCRIPCION")
    private String normaDescripcion;
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "normaProyecto")
//    private Numeral numeral;
    @JoinColumn(name = "NORMA_ID", referencedColumnName = "NORMA_ID")
    @ManyToOne
    private CatNorma normaId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    
    @Transient
    private Date fechaPublicacion;
    

    //-Se hace la union para la tabla de archivos proyectp
    @JoinColumn(name = "ID_ARCHIVO_PROGRAMA", referencedColumnName = "SEQ_ID")
    @ManyToOne
    private ArchivosProyecto idArchivoProyecto;
    
    
    @JoinColumns({
	@JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", updatable = false),
	@JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", updatable = false),
	@JoinColumn(name = "NORMA_PROY_ID", referencedColumnName = "NORMA_PROY_ID", updatable = false)})
	@OneToMany(fetch = FetchType.EAGER)
    List<Numeral> listaNumeral;
    
    public NormaProyecto() {
    }

    public NormaProyecto(NormaProyectoPK normaProyectoPK) {
        this.normaProyectoPK = normaProyectoPK;
    }

    public NormaProyecto(String folioProyecto, short serialProyecto, short normaProyId) {
        this.normaProyectoPK = new NormaProyectoPK(folioProyecto, serialProyecto, normaProyId);
    }

    public NormaProyectoPK getNormaProyectoPK() {
        return normaProyectoPK;
    }

    public void setNormaProyectoPK(NormaProyectoPK normaProyectoPK) {
        this.normaProyectoPK = normaProyectoPK;
    }

    public String getNormaFechPublic() {
        return normaFechPublic;
    }

    public void setNormaFechPublic(String normaFechPublic) {
        this.normaFechPublic = normaFechPublic;
    }

//    public Numeral getNumeral() {
//        return numeral;
//    }
//
//    public void setNumeral(Numeral numeral) {
//        this.numeral = numeral;
//    }

    public CatNorma getNormaId() {
        return normaId;
    }

    public void setNormaId(CatNorma normaId) {
        this.normaId = normaId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (normaProyectoPK != null ? normaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NormaProyecto)) {
            return false;
        }
        NormaProyecto other = (NormaProyecto) object;
        if ((this.normaProyectoPK == null && other.normaProyectoPK != null) || (this.normaProyectoPK != null && !this.normaProyectoPK.equals(other.normaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.NormaProyecto[ normaProyectoPK=" + normaProyectoPK + " ]";
    }

    /**
     * @return the normaDescripcion
     */
    public String getNormaDescripcion() {
        return normaDescripcion;
    }

    /**
     * @param normaDescripcion the normaDescripcion to set
     */
    public void setNormaDescripcion(String normaDescripcion) {
        this.normaDescripcion = normaDescripcion;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

	/**
	 * @return the idArchivoProyecto
	 */
	public ArchivosProyecto getIdArchivoProyecto() {
		return idArchivoProyecto;
	}

	/**
	 * @param idArchivoProyecto the idArchivoProyecto to set
	 */
	public void setIdArchivoProyecto(ArchivosProyecto idArchivoProyecto) {
		this.idArchivoProyecto = idArchivoProyecto;
	}

	/**
	 * @return the listaNumeral
	 */
	public List<Numeral> getListaNumeral() {
		return listaNumeral;
	}

	/**
	 * @param listaNumeral the listaNumeral to set
	 */
	public void setListaNumeral(List<Numeral> listaNumeral) {
		this.listaNumeral = listaNumeral;
	}
    
}
