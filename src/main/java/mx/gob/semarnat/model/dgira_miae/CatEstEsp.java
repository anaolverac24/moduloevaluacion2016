/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_EST_ESP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEstEsp.findAll", query = "SELECT c FROM CatEstEsp c"),
    @NamedQuery(name = "CatEstEsp.findByEstudioId", query = "SELECT c FROM CatEstEsp c WHERE c.estudioId = :estudioId"),
    @NamedQuery(name = "CatEstEsp.findByEstudioDescripcion", query = "SELECT c FROM CatEstEsp c WHERE c.estudioDescripcion = :estudioDescripcion")})
public class CatEstEsp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ESTUDIO_ID")
    private Short estudioId;
    @Column(name = "ESTUDIO_DESCRIPCION")
    private String estudioDescripcion;

    public CatEstEsp() {
    }

    public CatEstEsp(Short estudioId) {
        this.estudioId = estudioId;
    }

    public Short getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Short estudioId) {
        this.estudioId = estudioId;
    }

    public String getEstudioDescripcion() {
        return estudioDescripcion;
    }

    public void setEstudioDescripcion(String estudioDescripcion) {
        this.estudioDescripcion = estudioDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estudioId != null ? estudioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEstEsp)) {
            return false;
        }
        CatEstEsp other = (CatEstEsp) object;
        if ((this.estudioId == null && other.estudioId != null) || (this.estudioId != null && !this.estudioId.equals(other.estudioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatEstEsp[ estudioId=" + estudioId + " ]";
    }
    
}
