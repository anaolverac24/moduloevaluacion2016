/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_TIPO_DOC_DGIRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoDocDgira.findAll", query = "SELECT c FROM CatTipoDocDgira c"),
    @NamedQuery(name = "CatTipoDocDgira.findByClaveTramite2", query = "SELECT c FROM CatTipoDocDgira c WHERE c.claveTramite2 = :claveTramite2"),
    @NamedQuery(name = "CatTipoDocDgira.findByIdTramite2", query = "SELECT c FROM CatTipoDocDgira c WHERE c.idTramite2 = :idTramite2"),
    @NamedQuery(name = "CatTipoDocDgira.findByTipoDocId", query = "SELECT c FROM CatTipoDocDgira c WHERE c.tipoDocId = :tipoDocId"),
    @NamedQuery(name = "CatTipoDocDgira.findByTipoDocDescripcion", query = "SELECT c FROM CatTipoDocDgira c WHERE c.tipoDocDescripcion = :tipoDocDescripcion"),
    @NamedQuery(name = "CatTipoDocDgira.findByTipoDocUsuario", query = "SELECT c FROM CatTipoDocDgira c WHERE c.tipoDocUsuario = :tipoDocUsuario")})
public class CatTipoDocDgira implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CLAVE_TRAMITE2")
    private String claveTramite2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TRAMITE2")
    private int idTramite2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_DOC_ID")
    private short tipoDocId;
    
    
    
    
    @Size(max = 300)
    @Column(name = "TIPO_DOC_DESCRIPCION")
    private String tipoDocDescripcion;
    @Size(max = 4)
    @Column(name = "TIPO_DOC_USUARIO")
    private String tipoDocUsuario;

    public CatTipoDocDgira() {
    }

    /**
     * @return the claveTramite2
     */
    public String getClaveTramite2() {
        return claveTramite2;
    }

    /**
     * @param claveTramite2 the claveTramite2 to set
     */
    public void setClaveTramite2(String claveTramite2) {
        this.claveTramite2 = claveTramite2;
    }

    /**
     * @return the idTramite2
     */
    public Integer getIdTramite2() {
        return idTramite2;
    }

    /**
     * @param idTramite2 the idTramite2 to set
     */
    public void setIdTramite2(Integer idTramite2) {
        this.idTramite2 = idTramite2;
    }

    /**
     * @return the tipoDocId
     */
    public short getTipoDocId() {
        return tipoDocId;
    }

    /**
     * @param tipoDocId the tipoDocId to set
     */
    public void setTipoDocId(short tipoDocId) {
        this.tipoDocId = tipoDocId;
    }

    public String getTipoDocDescripcion() {
        return tipoDocDescripcion;
    }

    public void setTipoDocDescripcion(String tipoDocDescripcion) {
        this.tipoDocDescripcion = tipoDocDescripcion;
    }

    public String getTipoDocUsuario() {
        return tipoDocUsuario;
    }

    public void setTipoDocUsuario(String tipoDocUsuario) {
        this.tipoDocUsuario = tipoDocUsuario;
    }
    
}
