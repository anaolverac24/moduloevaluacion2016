/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author EdgarSC
 */
@Entity
@Table(name = "ACCIDENTES_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccidentesProyecto.findAll", query = "SELECT a FROM AccidentesProyecto a"),
    @NamedQuery(name = "AccidentesProyecto.findByFolioProyecto", query = "SELECT a FROM AccidentesProyecto a WHERE a.accidentesProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "AccidentesProyecto.findBySerialProyecto", query = "SELECT a FROM AccidentesProyecto a WHERE a.accidentesProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "AccidentesProyecto.findByAccidenteId", query = "SELECT a FROM AccidentesProyecto a WHERE a.accidentesProyectoPK.accidenteId = :accidenteId"),
    @NamedQuery(name = "AccidentesProyecto.findByTipoAccidente", query = "SELECT a FROM AccidentesProyecto a WHERE a.tipoAccidente = :tipoAccidente"),
    @NamedQuery(name = "AccidentesProyecto.findByEfectosSecundarios", query = "SELECT a FROM AccidentesProyecto a WHERE a.efectosSecundarios = :efectosSecundarios")})
public class AccidentesProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccidentesProyectoPK accidentesProyectoPK;
    @Column(name = "TIPO_ACCIDENTE")
    private String tipoAccidente;
    @Column(name = "EFECTOS_SECUNDARIOS")
    private String efectosSecundarios;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public AccidentesProyecto() {
    }

    public AccidentesProyecto(AccidentesProyectoPK accidentesProyectoPK) {
        this.accidentesProyectoPK = accidentesProyectoPK;
    }

    public AccidentesProyecto(String folioProyecto, short serialProyecto, short accidenteId) {
        this.accidentesProyectoPK = new AccidentesProyectoPK(folioProyecto, serialProyecto, accidenteId);
    }

    public AccidentesProyectoPK getAccidentesProyectoPK() {
        return accidentesProyectoPK;
    }

    public void setAccidentesProyectoPK(AccidentesProyectoPK accidentesProyectoPK) {
        this.accidentesProyectoPK = accidentesProyectoPK;
    }

    public String getTipoAccidente() {
        return tipoAccidente;
    }

    public void setTipoAccidente(String tipoAccidente) {
        this.tipoAccidente = tipoAccidente;
    }

    public String getEfectosSecundarios() {
        return efectosSecundarios;
    }
    
    public String getEfectosSecundariosCorta(){
        if(efectosSecundarios.length()<199){
            return efectosSecundarios;
        }
        return efectosSecundarios.substring(0, 199);
    }

    public void setEfectosSecundarios(String efectosSecundarios) {
        this.efectosSecundarios = efectosSecundarios;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accidentesProyectoPK != null ? accidentesProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccidentesProyecto)) {
            return false;
        }
        AccidentesProyecto other = (AccidentesProyecto) object;
        if ((this.accidentesProyectoPK == null && other.accidentesProyectoPK != null) || (this.accidentesProyectoPK != null && !this.accidentesProyectoPK.equals(other.accidentesProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AccidentesProyecto[ accidentesProyectoPK=" + accidentesProyectoPK + " ]";
    }
    
}
