/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_CONVENIOS",schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatConvenios.findAll", query = "SELECT c FROM CatConvenios c"),
    @NamedQuery(name = "CatConvenios.findByConvenioId", query = "SELECT c FROM CatConvenios c WHERE c.convenioId = :convenioId"),
    @NamedQuery(name = "CatConvenios.findByConvenioDescripcion", query = "SELECT c FROM CatConvenios c WHERE c.convenioDescripcion = :convenioDescripcion")})
public class CatConvenios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CONVENIO_ID")
    private Short convenioId;
    @Column(name = "CONVENIO_DESCRIPCION")
    private String convenioDescripcion;
    @OneToMany(mappedBy = "convenioId")
    private List<ConvenioProy> convenioProyList;

    public CatConvenios() {
    }

    public CatConvenios(Short convenioId) {
        this.convenioId = convenioId;
    }

    public Short getConvenioId() {
        return convenioId;
    }

    public void setConvenioId(Short convenioId) {
        this.convenioId = convenioId;
    }

    public String getConvenioDescripcion() {
        return convenioDescripcion;
    }

    public void setConvenioDescripcion(String convenioDescripcion) {
        this.convenioDescripcion = convenioDescripcion;
    }

    @XmlTransient
    public List<ConvenioProy> getConvenioProyList() {
        return convenioProyList;
    }

    public void setConvenioProyList(List<ConvenioProy> convenioProyList) {
        this.convenioProyList = convenioProyList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (convenioId != null ? convenioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatConvenios)) {
            return false;
        }
        CatConvenios other = (CatConvenios) object;
        if ((this.convenioId == null && other.convenioId != null) || (this.convenioId != null && !this.convenioId.equals(other.convenioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return convenioDescripcion;
    }
    
}
