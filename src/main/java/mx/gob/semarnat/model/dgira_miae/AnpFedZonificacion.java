/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ANP_FED_ZONIFICACION", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnpFedZonificacion.findAll", query = "SELECT a FROM AnpFedZonificacion a")})
public class AnpFedZonificacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnpFedZonificacionPK anpFedZonificacionPK;
    @Column(name = "ZONIF_USO")
    private String zonifUso;
    @Column(name = "ZONIFORIG1")
    private String zoniforig1;
    @Column(name = "ZONIFORIG2")
    private String zoniforig2;
    @Column(name = "ZONIFORIG3")
    private String zoniforig3;
    @Column(name = "NOM_ANP")
    private String nomAnp;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "anpFedZonificacion")
//    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public AnpFedZonificacion() {
    }

    public AnpFedZonificacion(AnpFedZonificacionPK anpFedZonificacionPK) {
        this.anpFedZonificacionPK = anpFedZonificacionPK;
    }

    public AnpFedZonificacion(String numFolio, String cveProy, String cveArea, short version) {
        this.anpFedZonificacionPK = new AnpFedZonificacionPK(numFolio, cveProy, cveArea, version);
    }

    public AnpFedZonificacionPK getAnpFedZonificacionPK() {
        return anpFedZonificacionPK;
    }

    public void setAnpFedZonificacionPK(AnpFedZonificacionPK anpFedZonificacionPK) {
        this.anpFedZonificacionPK = anpFedZonificacionPK;
    }

    public String getZonifUso() {
        return zonifUso;
    }

    public void setZonifUso(String zonifUso) {
        this.zonifUso = zonifUso;
    }

    public String getZoniforig1() {
        return zoniforig1;
    }

    public void setZoniforig1(String zoniforig1) {
        this.zoniforig1 = zoniforig1;
    }

    public String getZoniforig2() {
        return zoniforig2;
    }

    public void setZoniforig2(String zoniforig2) {
        this.zoniforig2 = zoniforig2;
    }

    public String getZoniforig3() {
        return zoniforig3;
    }

    public void setZoniforig3(String zoniforig3) {
        this.zoniforig3 = zoniforig3;
    }

    public String getNomAnp() {
        return nomAnp;
    }

    public void setNomAnp(String nomAnp) {
        this.nomAnp = nomAnp;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

//    @XmlTransient
//    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
//        return sigeiaAnalisisList;
//    }
//
//    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
//        this.sigeiaAnalisisList = sigeiaAnalisisList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anpFedZonificacionPK != null ? anpFedZonificacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnpFedZonificacion)) {
            return false;
        }
        AnpFedZonificacion other = (AnpFedZonificacion) object;
        if ((this.anpFedZonificacionPK == null && other.anpFedZonificacionPK != null) || (this.anpFedZonificacionPK != null && !this.anpFedZonificacionPK.equals(other.anpFedZonificacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AnpFedZonificacion[ anpFedZonificacionPK=" + anpFedZonificacionPK + " ]";
    }
    
}
