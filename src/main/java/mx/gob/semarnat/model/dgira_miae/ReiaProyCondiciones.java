/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_PROY_CONDICIONES", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaProyCondiciones.findAll", query = "SELECT r FROM ReiaProyCondiciones r"),
    @NamedQuery(name = "ReiaProyCondiciones.findByCondicion", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.condicion = :condicion"),
    @NamedQuery(name = "ReiaProyCondiciones.findByRespuesta", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.respuesta = :respuesta"),
    @NamedQuery(name = "ReiaProyCondiciones.findByClave", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.clave = :clave")})
public class ReiaProyCondiciones implements Serializable {
    private static final long serialVersionUID = 1;
    @JoinColumn(name="CLAVE_OBRA_PROY", referencedColumnName="CLAVE")
    @ManyToOne
    private ReiaProyObras claveObraProy;
    @JoinColumn(name="CONDICION", referencedColumnName="ID_CONDICION")
    @ManyToOne
    private ReiaCondiciones condicion;
    @Column(name="RESPUESTA")
    private Long respuesta;
    @Id
    @GeneratedValue(generator="InvSeqC")
    @SequenceGenerator(name="InvSeqC", sequenceName="seq_proy_condiciones")
    @Basic(optional=false)
    private Long clave;

    public ReiaProyCondiciones() {
    }

    public ReiaProyCondiciones(Long clave) {
        this.clave = clave;
    }

    public ReiaCondiciones getCondicion() {
        return this.condicion;
    }

    public void setCondicion(ReiaCondiciones condicion) {
        this.condicion = condicion;
    }

    public Long getRespuesta() {
        return this.respuesta;
    }

    public void setRespuesta(Long respuesta) {
        this.respuesta = respuesta;
    }

    public Long getClave() {
        return this.clave;
    }

    public void setClave(Long clave) {
        this.clave = clave;
    }

    public ReiaProyObras getClaveObraProy() {
        return this.claveObraProy;
    }

    public void setClaveObraProy(ReiaProyObras claveObraProy) {
        this.claveObraProy = claveObraProy;
    }

    public int hashCode() {
        int hash = 0;
        return hash += this.clave != null ? this.clave.hashCode() : 0;
    }

    public boolean equals(Object object) {
        if (!(object instanceof ReiaProyCondiciones)) {
            return false;
        }
        ReiaProyCondiciones other = (ReiaProyCondiciones)object;
        if (this.clave == null && other.clave != null || this.clave != null && !this.clave.equals(other.clave)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaProyCondiciones[ clave=" + this.clave + " ]";
    }
    
    
}
