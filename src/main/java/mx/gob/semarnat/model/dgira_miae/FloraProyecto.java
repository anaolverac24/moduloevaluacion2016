/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author EdgarSC
 */
@Entity
@Table(name = "FLORA_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FloraProyecto.findAll", query = "SELECT f FROM FloraProyecto f"),
    @NamedQuery(name = "FloraProyecto.findByFolioProyecto", query = "SELECT f FROM FloraProyecto f WHERE f.floraProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "FloraProyecto.findBySerialProyecto", query = "SELECT f FROM FloraProyecto f WHERE f.floraProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "FloraProyecto.findByFloraProyid", query = "SELECT f FROM FloraProyecto f WHERE f.floraProyectoPK.floraProyid = :floraProyid"),
    @NamedQuery(name = "FloraProyecto.findByFloraFamilia", query = "SELECT f FROM FloraProyecto f WHERE f.floraFamilia = :floraFamilia"),
    @NamedQuery(name = "FloraProyecto.findByFloraNomCient", query = "SELECT f FROM FloraProyecto f WHERE f.floraNomCient = :floraNomCient"),
    @NamedQuery(name = "FloraProyecto.findByFloraNomComun", query = "SELECT f FROM FloraProyecto f WHERE f.floraNomComun = :floraNomComun"),
    @NamedQuery(name = "FloraProyecto.findByFloraCatNom59", query = "SELECT f FROM FloraProyecto f WHERE f.floraCatNom59 = :floraCatNom59"),
    @NamedQuery(name = "FloraProyecto.findByFloraEndemico", query = "SELECT f FROM FloraProyecto f WHERE f.floraEndemico = :floraEndemico"),
    @NamedQuery(name = "FloraProyecto.findByFloraCites", query = "SELECT f FROM FloraProyecto f WHERE f.floraCites = :floraCites"),
    @NamedQuery(name = "FloraProyecto.findByFloraRegistro", query = "SELECT f FROM FloraProyecto f WHERE f.floraRegistro = :floraRegistro"),
    @NamedQuery(name = "FloraProyecto.findByFloraAbundancia", query = "SELECT f FROM FloraProyecto f WHERE f.floraAbundancia = :floraAbundancia"),
    @NamedQuery(name = "FloraProyecto.findByFloraDensidad", query = "SELECT f FROM FloraProyecto f WHERE f.floraDensidad = :floraDensidad"),
    @NamedQuery(name = "FloraProyecto.findByFloraDistribucion", query = "SELECT f FROM FloraProyecto f WHERE f.floraDistribucion = :floraDistribucion"),
    @NamedQuery(name = "FloraProyecto.findByFloraNumIndvAfec", query = "SELECT f FROM FloraProyecto f WHERE f.floraNumIndvAfec = :floraNumIndvAfec"),
    @NamedQuery(name = "FloraProyecto.findByFloraAnexoNombre", query = "SELECT f FROM FloraProyecto f WHERE f.floraAnexoNombre = :floraAnexoNombre"),
    @NamedQuery(name = "FloraProyecto.findByFloraAnexoUrl", query = "SELECT f FROM FloraProyecto f WHERE f.floraAnexoUrl = :floraAnexoUrl"),
    @NamedQuery(name = "FloraProyecto.findByFloraAnexoExtension", query = "SELECT f FROM FloraProyecto f WHERE f.floraAnexoExtension = :floraAnexoExtension"),
    @NamedQuery(name = "FloraProyecto.findByFloraAnexoTamanio", query = "SELECT f FROM FloraProyecto f WHERE f.floraAnexoTamanio = :floraAnexoTamanio"),
    @NamedQuery(name = "FloraProyecto.findByFloraAnexoDescripcion", query = "SELECT f FROM FloraProyecto f WHERE f.floraAnexoDescripcion = :floraAnexoDescripcion")})
public class FloraProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FloraProyectoPK floraProyectoPK;
    @Column(name = "FLORA_FAMILIA")
    private String floraFamilia;
    @Column(name = "FLORA_NOM_CIENT")
    private String floraNomCient;
    @Column(name = "FLORA_NOM_COMUN")
    private String floraNomComun;
    @Column(name = "FLORA_CAT_NOM59")
    private String floraCatNom59;
    @Column(name = "FLORA_ENDEMICO")
    private String floraEndemico;
    @Column(name = "FLORA_CITES")
    private String floraCites;
//    @Column(name = "FLORA_PRESENCIA")
//    private String floraPresencia;
    @Column(name = "FLORA_REGISTRO")
    private String floraRegistro;
    @Column(name = "FLORA_ABUNDANCIA")
    private String floraAbundancia;
    @Column(name = "FLORA_DENSIDAD")
    private String floraDensidad;
    @Column(name = "FLORA_DISTRIBUCION")
    private String floraDistribucion;
    @Column(name = "FLORA_NUM_INDV_AFEC")
    private String floraNumIndvAfec;
    @Column(name = "FLORA_ANEXO_NOMBRE")
    private String floraAnexoNombre;
    @Column(name = "FLORA_ANEXO_URL")
    private String floraAnexoUrl;
    @Column(name = "FLORA_ANEXO_EXTENSION")
    private String floraAnexoExtension;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FLORA_ANEXO_TAMANIO")
    private BigDecimal floraAnexoTamanio;
    @Column(name = "FLORA_ANEXO_DESCRIPCION")
    private String floraAnexoDescripcion;

    @Column(name = "FLORA_PRESENCIA_SA")
    private String floraPresenciaSa;
    @Column(name = "FLORA_PRESENCIA_AI")
    private String floraPresenciaAi;
    @Column(name = "FLORA_PRESENCIA_PR")
    private String floraPresenciaPr;
    @Column(name = "FLORA_GRUPO")
    private String floraGrupo;
    @Column(name = "ID_FLORA_SEQ")
    private Short idFloraSeq;
    @Column(name = "FLORA_CLASE")    
    private String floraClase;

    @Transient
    private Boolean presenciaSa;
    @Transient
    private Boolean presenciaAi;
    @Transient
    private Boolean presenciaPr;

    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    
    //-Se hace la unión para la tabla de archivos proyecto
    @JoinColumn(name = "ID_ARCHIVO_PROGRAMA", referencedColumnName = "SEQ_ID")
    @ManyToOne
    private ArchivosProyecto idArchivoProyecto;    

    public FloraProyecto() {
    }

    public FloraProyecto(FloraProyectoPK floraProyectoPK) {
        this.floraProyectoPK = floraProyectoPK;
    }

    public FloraProyecto(String folioProyecto, short serialProyecto, int floraProyid) {
        this.floraProyectoPK = new FloraProyectoPK(folioProyecto, serialProyecto, floraProyid);
    }

    public FloraProyectoPK getFloraProyectoPK() {
        return floraProyectoPK;
    }

    public void setFloraProyectoPK(FloraProyectoPK floraProyectoPK) {
        this.floraProyectoPK = floraProyectoPK;
    }

    public String getFloraFamilia() {
        return floraFamilia;
    }

    public void setFloraFamilia(String floraFamilia) {
        this.floraFamilia = floraFamilia;
    }

    public String getFloraNomCient() {
        return floraNomCient;
    }

    public void setFloraNomCient(String floraNomCient) {
        this.floraNomCient = floraNomCient;
    }

    public String getFloraNomComun() {
        return floraNomComun;
    }

    public void setFloraNomComun(String floraNomComun) {
        this.floraNomComun = floraNomComun;
    }

    public String getFloraCatNom59() {
        return floraCatNom59;
    }

    public void setFloraCatNom59(String floraCatNom59) {
        this.floraCatNom59 = floraCatNom59;
    }

    public String getFloraEndemico() {
        return floraEndemico;
    }

    public void setFloraEndemico(String floraEndemico) {
        this.floraEndemico = floraEndemico;
    }

    public String getFloraCites() {
        return floraCites;
    }

    public void setFloraCites(String floraCites) {
        this.floraCites = floraCites;
    }

//    public String getFloraPresencia() {
//        return floraPresencia;
//    }
//
//    public void setFloraPresencia(String floraPresencia) {
//        this.floraPresencia = floraPresencia;
//    }
    public String getFloraRegistro() {
        return floraRegistro;
    }

    public void setFloraRegistro(String floraRegistro) {
        this.floraRegistro = floraRegistro;
    }

    public String getFloraAbundancia() {
        return floraAbundancia;
    }

    public void setFloraAbundancia(String floraAbundancia) {
        this.floraAbundancia = floraAbundancia;
    }

    public String getFloraDensidad() {
        return floraDensidad;
    }

    public void setFloraDensidad(String floraDensidad) {
        this.floraDensidad = floraDensidad;
    }

    public String getFloraDistribucion() {
        return floraDistribucion;
    }

    public void setFloraDistribucion(String floraDistribucion) {
        this.floraDistribucion = floraDistribucion;
    }

    public String getFloraNumIndvAfec() {
        return floraNumIndvAfec;
    }

    public void setFloraNumIndvAfec(String floraNumIndvAfec) {
        this.floraNumIndvAfec = floraNumIndvAfec;
    }

    public String getFloraAnexoNombre() {
        return floraAnexoNombre;
    }

    public void setFloraAnexoNombre(String floraAnexoNombre) {
        this.floraAnexoNombre = floraAnexoNombre;
    }

    public String getFloraAnexoUrl() {
        return floraAnexoUrl;
    }

    public void setFloraAnexoUrl(String floraAnexoUrl) {
        this.floraAnexoUrl = floraAnexoUrl;
    }

    public String getFloraAnexoExtension() {
        return floraAnexoExtension;
    }

    public void setFloraAnexoExtension(String floraAnexoExtension) {
        this.floraAnexoExtension = floraAnexoExtension;
    }

    public BigDecimal getFloraAnexoTamanio() {
        return floraAnexoTamanio;
    }

    public void setFloraAnexoTamanio(BigDecimal floraAnexoTamanio) {
        this.floraAnexoTamanio = floraAnexoTamanio;
    }

    public String getFloraAnexoDescripcion() {
        return floraAnexoDescripcion;
    }

    public void setFloraAnexoDescripcion(String floraAnexoDescripcion) {
        this.floraAnexoDescripcion = floraAnexoDescripcion;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (floraProyectoPK != null ? floraProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FloraProyecto)) {
            return false;
        }
        FloraProyecto other = (FloraProyecto) object;
        if ((this.floraProyectoPK == null && other.floraProyectoPK != null) || (this.floraProyectoPK != null && !this.floraProyectoPK.equals(other.floraProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.FloraProyecto[ floraProyectoPK=" + floraProyectoPK + " ]";
    }

    /**
     * @return the floraPresenciaSa
     */
    public String getFloraPresenciaSa() {
    	if (floraPresenciaSa != null) {
			if (floraPresenciaSa.equals("S")) {
				return "Si";
			}
			if (floraPresenciaSa.equals("N")) {
				return "No";
			}
		}
        return floraPresenciaSa;
    }

    /**
     * @param floraPresenciaSa the floraPresenciaSa to set
     */
    public void setFloraPresenciaSa(String floraPresenciaSa) {
        this.floraPresenciaSa = floraPresenciaSa;
    }

    /**
     * @return the floraPresenciaAi
     */
    public String getFloraPresenciaAi() {
    	if (floraPresenciaAi != null) {
			if (floraPresenciaAi.equals("S")) {
				return "Si";
			}
			if (floraPresenciaAi.equals("N")) {
				return "No";
			}
		}
        return floraPresenciaAi;
    }

    /**
     * @param floraPresenciaAi the floraPresenciaAi to set
     */
    public void setFloraPresenciaAi(String floraPresenciaAi) {
        this.floraPresenciaAi = floraPresenciaAi;
    }

    /**
     * @return the floraPresenciaPr
     */
    public String getFloraPresenciaPr() {
    	if (floraPresenciaPr != null) {
			if (floraPresenciaPr.equals("S")) {
				return "Si";
			}
			if (floraPresenciaPr.equals("N")) {
				return "No";
			}
		}
        return floraPresenciaPr;
    }

    /**
     * @param floraPresenciaPr the floraPresenciaPr to set
     */
    public void setFloraPresenciaPr(String floraPresenciaPr) {
        this.floraPresenciaPr = floraPresenciaPr;
    }

    /**
     * @return the floraGrupo
     */
    public String getFloraGrupo() {
        return floraGrupo;
    }

    /**
     * @param floraGrupo the floraGrupo to set
     */
    public void setFloraGrupo(String floraGrupo) {
        this.floraGrupo = floraGrupo;
    }

    /**
     * @return the presenciaSa
     */
    public Boolean getPresenciaSa() {
        presenciaSa = (floraPresenciaSa != null && floraPresenciaSa.equals('S'));
        return presenciaSa;
    }

    /**
     * @return the presenciaAi
     */
    public Boolean getPresenciaAi() {
        presenciaAi = (floraPresenciaAi != null && floraPresenciaAi.equals('S'));
        return presenciaAi;
    }

    /**
     * @return the presenciaPr
     */
    public Boolean getPresenciaPr() {
        presenciaPr = (floraPresenciaPr != null && floraPresenciaPr.equals('S'));
        return presenciaPr;
    }

	/**
	 * @return the idFloraSeq
	 */
	public Short getIdFloraSeq() {
		return idFloraSeq;
	}

	/**
	 * @param idFloraSeq the idFloraSeq to set
	 */
	public void setIdFloraSeq(Short idFloraSeq) {
		this.idFloraSeq = idFloraSeq;
	}

	/**
	 * @return the floraClase
	 */
	public String getFloraClase() {
		return floraClase;
	}

	/**
	 * @param floraClase the floraClase to set
	 */
	public void setFloraClase(String floraClase) {
		this.floraClase = floraClase;
	}

	/**
	 * @return the idArchivoProyecto
	 */
	public ArchivosProyecto getIdArchivoProyecto() {
		return idArchivoProyecto;
	}

	/**
	 * @param idArchivoProyecto the idArchivoProyecto to set
	 */
	public void setIdArchivoProyecto(ArchivosProyecto idArchivoProyecto) {
		this.idArchivoProyecto = idArchivoProyecto;
	}

}

