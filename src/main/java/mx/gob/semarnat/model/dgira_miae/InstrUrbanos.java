/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "INSTR_URBANOS", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstrUrbanos.findAll", query = "SELECT i FROM InstrUrbanos i"),
    @NamedQuery(name = "InstrUrbanos.findByFolVerCve", query = "SELECT a FROM InstrUrbanos a  WHERE a.instrUrbanosPK.numFolio = :fol and a.instrUrbanosPK.version = :clveProy ")})
public class InstrUrbanos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InstrUrbanosPK instrUrbanosPK;
    @Column(name = "PDU_SITIO")
    private String pduSitio;
    @Column(name = "USOCLASPOL")
    private String usoclaspol;
    @Column(name = "CLV_USOCP")
    private String clvUsocp;
    @Column(name = "INST_URB")
    private String instUrb;
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "SUP_EA")
    private Double supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private Double areabuffer;
    @Column(name = "AREA")
    private Double area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instrUrbanos")
//    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public InstrUrbanos() {
    }

    public InstrUrbanos(InstrUrbanosPK instrUrbanosPK) {
        this.instrUrbanosPK = instrUrbanosPK;
    }

    public InstrUrbanos(String numFolio, String cveProy, String cveArea, short version) {
        this.instrUrbanosPK = new InstrUrbanosPK(numFolio, cveProy, cveArea, version);
    }

    public InstrUrbanosPK getInstrUrbanosPK() {
        return instrUrbanosPK;
    }

    public void setInstrUrbanosPK(InstrUrbanosPK instrUrbanosPK) {
        this.instrUrbanosPK = instrUrbanosPK;
    }

    public String getPduSitio() {
        return pduSitio;
    }

    public void setPduSitio(String pduSitio) {
        this.pduSitio = pduSitio;
    }

    public String getUsoclaspol() {
        return usoclaspol;
    }

    public void setUsoclaspol(String usoclaspol) {
        this.usoclaspol = usoclaspol;
    }

    public String getClvUsocp() {
        return clvUsocp;
    }

    public void setClvUsocp(String clvUsocp) {
        this.clvUsocp = clvUsocp;
    }

    public String getInstUrb() {
        return instUrb;
    }

    public void setInstUrb(String instUrb) {
        this.instUrb = instUrb;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getSupEa() {
        return supEa;
    }

    public void setSupEa(Double supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Double getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(Double areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

//    @XmlTransient
//    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
//        return sigeiaAnalisisList;
//    }
//
//    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
//        this.sigeiaAnalisisList = sigeiaAnalisisList;
//    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instrUrbanosPK != null ? instrUrbanosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstrUrbanos)) {
            return false;
        }
        InstrUrbanos other = (InstrUrbanos) object;
        if ((this.instrUrbanosPK == null && other.instrUrbanosPK != null) || (this.instrUrbanosPK != null && !this.instrUrbanosPK.equals(other.instrUrbanosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.InstrUrbanos[ instrUrbanosPK=" + instrUrbanosPK + " ]";
    }

}
