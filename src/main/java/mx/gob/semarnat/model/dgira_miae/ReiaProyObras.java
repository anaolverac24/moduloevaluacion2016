/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_PROY_OBRAS", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaProyObras.findAll", query = "SELECT r FROM ReiaProyObras r"),
    @NamedQuery(name = "ReiaProyObras.findByFolio", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio"),
    @NamedQuery(name = "ReiaProyObras.findByFolioObrasCapturadas", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio and r.obra.obra != 'NO' ORDER BY r.clave ASC"),
    @NamedQuery(name = "ReiaProyObras.findByObra", query = "SELECT r FROM ReiaProyObras r WHERE r.obra = :obra"),
    @NamedQuery(name = "ReiaProyObras.findByFolioEstatusObra", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio AND r.obra.idCategoria.estatus=:estatus AND r.obra.fraccion IS NOT NULL"),
    @NamedQuery(name = "ReiaProyObras.findByFolioEstatusObraCount", query = "SELECT COUNT(r) FROM ReiaProyObras r WHERE r.folio = :folio AND r.obra.idCategoria.estatus=:estatus AND r.obra.fraccion IS NOT NULL"),
    @NamedQuery(name = "ReiaProyObras.findByFolioCount", query = "SELECT COUNT(r) FROM ReiaProyObras r WHERE r.folio = :folio"),
    @NamedQuery(name = "ReiaProyObras.findByClave", query = "SELECT r FROM ReiaProyObras r WHERE r.clave = :clave"),
    @NamedQuery(name = "ReiaProyObras.findByCategoria", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio AND r.obra.idCategoria.idCategoria = :idCategoria")})
public class ReiaProyObras implements Serializable {
    private static final long serialVersionUID = 1;
    @Column(name="FOLIO")
    private String folio;
    @JoinColumn(name="OBRA", referencedColumnName="ID_OBRA")
    @ManyToOne
    private ReiaObras obra;
    @Id
    @GeneratedValue(generator="InvSeq")
    @SequenceGenerator(name="InvSeq", sequenceName="seq_proy_obras")
    @Basic(optional=false)
    @Column(name="CLAVE")
    private Long clave;
    @Column(name="PRINCIPAL")
    private String principal;
    @Column(name="EXCEPTUADA")
    private String exceptuada;
    @OneToMany(mappedBy="claveObraProy")
    private Collection<ReiaProyCondiciones> proyCondiciones;

    public ReiaProyObras() {
    }

    public ReiaProyObras(Long clave) {
        this.clave = clave;
    }

    public String getFolio() {
        return this.folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public ReiaObras getObra() {
        return this.obra;
    }

    public void setObra(ReiaObras obra) {
        this.obra = obra;
    }

    public Long getClave() {
        return this.clave;
    }

    public void setClave(Long clave) {
        this.clave = clave;
    }

    public String getExceptuada() {
        return this.exceptuada;
    }

    public void setExceptuada(String exceptuada) {
        this.exceptuada = exceptuada;
    }

    public int hashCode() {
        int hash = 0;
        return hash += this.clave != null ? this.clave.hashCode() : 0;
    }

    public boolean equals(Object object) {
        if (!(object instanceof ReiaProyObras)) {
            return false;
        }
        ReiaProyObras other = (ReiaProyObras)object;
        if (this.clave == null && other.clave != null || this.clave != null && !this.clave.equals(other.clave)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaProyObras[ clave=" + this.clave + " ]";
    }

    public String getPrincipal() {
        return this.principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    @XmlTransient
    public Collection<ReiaProyCondiciones> getProyCondiciones() {
        return this.proyCondiciones;
    }

    public void setProyCondiciones(Collection<ReiaProyCondiciones> proyCondiciones) {
        this.proyCondiciones = proyCondiciones;
    }
    
}
