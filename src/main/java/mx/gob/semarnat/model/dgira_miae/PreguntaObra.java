/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "PREGUNTA_OBRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaObra.findAll", query = "SELECT p FROM PreguntaObra p"),
    @NamedQuery(name = "PreguntaObra.findByFolioProyecto", query = "SELECT p FROM PreguntaObra p WHERE p.preguntaObraPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PreguntaObra.findBySerialProyecto", query = "SELECT p FROM PreguntaObra p WHERE p.preguntaObraPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PreguntaObra.findByPreguntaId", query = "SELECT p FROM PreguntaObra p WHERE p.preguntaObraPK.preguntaId = :preguntaId"),
    @NamedQuery(name = "PreguntaObra.findByObraId", query = "SELECT p FROM PreguntaObra p WHERE p.preguntaObraPK.obraId = :obraId"),
    @NamedQuery(name = "PreguntaObra.findByDimensionesObra", query = "SELECT p FROM PreguntaObra p WHERE p.dimensionesObra = :dimensionesObra"),
    @NamedQuery(name = "PreguntaObra.findByDimensionesUnidad", query = "SELECT p FROM PreguntaObra p WHERE p.dimensionesUnidad = :dimensionesUnidad"),
    @NamedQuery(name = "PreguntaObra.findBySuperficieObra", query = "SELECT p FROM PreguntaObra p WHERE p.superficieObra = :superficieObra"),
    @NamedQuery(name = "PreguntaObra.findBySuperficieUnidad", query = "SELECT p FROM PreguntaObra p WHERE p.superficieUnidad = :superficieUnidad"),
    @NamedQuery(name = "PreguntaObra.findByAfectacionObra", query = "SELECT p FROM PreguntaObra p WHERE p.afectacionObra = :afectacionObra")})
public class PreguntaObra implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreguntaObraPK preguntaObraPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DIMENSIONES_OBRA")
    private BigDecimal dimensionesObra;
    @Column(name = "DIMENSIONES_UNIDAD")
    private Short dimensionesUnidad;
    @Column(name = "SUPERFICIE_OBRA")
    private BigDecimal superficieObra;
    @Column(name = "SUPERFICIE_UNIDAD")
    private Short superficieUnidad;
    @Column(name = "AFECTACION_OBRA")
    private Short afectacionObra;
    @JoinColumns({
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "PREGUNTA_ID", referencedColumnName = "PREGUNTA_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private PreguntaProyecto preguntaProyecto;

    public PreguntaObra() {
    }

    public PreguntaObra(PreguntaObraPK preguntaObraPK) {
        this.preguntaObraPK = preguntaObraPK;
    }

    public PreguntaObra(String folioProyecto, short serialProyecto, short preguntaId, short obraId) {
        this.preguntaObraPK = new PreguntaObraPK(folioProyecto, serialProyecto, preguntaId, obraId);
    }

    public PreguntaObraPK getPreguntaObraPK() {
        return preguntaObraPK;
    }

    public void setPreguntaObraPK(PreguntaObraPK preguntaObraPK) {
        this.preguntaObraPK = preguntaObraPK;
    }

    public BigDecimal getDimensionesObra() {
        return dimensionesObra;
    }

    public void setDimensionesObra(BigDecimal dimensionesObra) {
        this.dimensionesObra = dimensionesObra;
    }

    public Short getDimensionesUnidad() {
        return dimensionesUnidad;
    }

    public void setDimensionesUnidad(Short dimensionesUnidad) {
        this.dimensionesUnidad = dimensionesUnidad;
    }

    public BigDecimal getSuperficieObra() {
        return superficieObra;
    }

    public void setSuperficieObra(BigDecimal superficieObra) {
        this.superficieObra = superficieObra;
    }

    public Short getSuperficieUnidad() {
        return superficieUnidad;
    }

    public void setSuperficieUnidad(Short superficieUnidad) {
        this.superficieUnidad = superficieUnidad;
    }

    public Short getAfectacionObra() {
        return afectacionObra;
    }

    public void setAfectacionObra(Short afectacionObra) {
        this.afectacionObra = afectacionObra;
    }

    public PreguntaProyecto getPreguntaProyecto() {
        return preguntaProyecto;
    }

    public void setPreguntaProyecto(PreguntaProyecto preguntaProyecto) {
        this.preguntaProyecto = preguntaProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaObraPK != null ? preguntaObraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaObra)) {
            return false;
        }
        PreguntaObra other = (PreguntaObra) object;
        if ((this.preguntaObraPK == null && other.preguntaObraPK != null) || (this.preguntaObraPK != null && !this.preguntaObraPK.equals(other.preguntaObraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "package mx.gob.semarnat.model.dgira_miae.PreguntaObra[ preguntaObraPK=" + preguntaObraPK + " ]";
    }
    
}
