/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "MPIOS_CRUZADAVSHAMBRE", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MpiosCruzadavshambre.findAll", query = "SELECT m FROM MpiosCruzadavshambre m"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByNumFolio", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.mpiosCruzadavshambrePK.numFolio = :numFolio"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByCveProy", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.mpiosCruzadavshambrePK.cveProy = :cveProy"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByCveArea", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.mpiosCruzadavshambrePK.cveArea = :cveArea"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByVersion", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.mpiosCruzadavshambrePK.version = :version"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByEstado", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.estado = :estado"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByClvMunici", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.clvMunici = :clvMunici"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByNomMunici", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.nomMunici = :nomMunici"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByNomEstado", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.nomEstado = :nomEstado"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByCrzdaHamb", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.crzdaHamb = :crzdaHamb"),
    @NamedQuery(name = "MpiosCruzadavshambre.findBySupEa", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.supEa = :supEa"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByProy", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.proy = :proy"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByComp", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.comp = :comp"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByDescrip", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.descrip = :descrip"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByAreabuffer", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.areabuffer = :areabuffer"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByArea", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.area = :area"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByFechaHora", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.fechaHora = :fechaHora"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByCEdomun", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.cEdomun = :cEdomun"),
    @NamedQuery(name = "MpiosCruzadavshambre.findByIdr", query = "SELECT m FROM MpiosCruzadavshambre m WHERE m.mpiosCruzadavshambrePK.idr = :idr")})
public class MpiosCruzadavshambre implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MpiosCruzadavshambrePK mpiosCruzadavshambrePK;
    @Column(name = "ESTADO")
    private String estado;
    @Column(name = "CLV_MUNICI")
    private String clvMunici;
    @Column(name = "NOM_MUNICI")
    private String nomMunici;
    @Column(name = "NOM_ESTADO")
    private String nomEstado;
    @Column(name = "CRZDA_HAMB")
    private String crzdaHamb;
    @Column(name = "SUP_EA")
    private Integer supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private Integer area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @Column(name = "C_EDOMUN")
    private String cEdomun;

    public MpiosCruzadavshambre() {
    }

    public MpiosCruzadavshambre(MpiosCruzadavshambrePK mpiosCruzadavshambrePK) {
        this.mpiosCruzadavshambrePK = mpiosCruzadavshambrePK;
    }

    public MpiosCruzadavshambre(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.mpiosCruzadavshambrePK = new MpiosCruzadavshambrePK(numFolio, cveProy, cveArea, version, idr);
    }

    public MpiosCruzadavshambrePK getMpiosCruzadavshambrePK() {
        return mpiosCruzadavshambrePK;
    }

    public void setMpiosCruzadavshambrePK(MpiosCruzadavshambrePK mpiosCruzadavshambrePK) {
        this.mpiosCruzadavshambrePK = mpiosCruzadavshambrePK;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getClvMunici() {
        return clvMunici;
    }

    public void setClvMunici(String clvMunici) {
        this.clvMunici = clvMunici;
    }

    public String getNomMunici() {
        return nomMunici;
    }

    public void setNomMunici(String nomMunici) {
        this.nomMunici = nomMunici;
    }

    public String getNomEstado() {
        return nomEstado;
    }

    public void setNomEstado(String nomEstado) {
        this.nomEstado = nomEstado;
    }

    public String getCrzdaHamb() {
        return crzdaHamb;
    }

    public void setCrzdaHamb(String crzdaHamb) {
        this.crzdaHamb = crzdaHamb;
    }

    public Integer getSupEa() {
        return supEa;
    }

    public void setSupEa(Integer supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getCEdomun() {
        return cEdomun;
    }

    public void setCEdomun(String cEdomun) {
        this.cEdomun = cEdomun;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mpiosCruzadavshambrePK != null ? mpiosCruzadavshambrePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MpiosCruzadavshambre)) {
            return false;
        }
        MpiosCruzadavshambre other = (MpiosCruzadavshambre) object;
        if ((this.mpiosCruzadavshambrePK == null && other.mpiosCruzadavshambrePK != null) || (this.mpiosCruzadavshambrePK != null && !this.mpiosCruzadavshambrePK.equals(other.mpiosCruzadavshambrePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.MpiosCruzadavshambre[ mpiosCruzadavshambrePK=" + mpiosCruzadavshambrePK + " ]";
    }
    
}
