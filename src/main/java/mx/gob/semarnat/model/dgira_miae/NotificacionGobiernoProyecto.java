/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "NOTIFICACION_GOBIERNO_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificacionGobiernoProyecto.findAll", query = "SELECT n FROM NotificacionGobiernoProyecto n"),
    @NamedQuery(name = "NotificacionGobiernoProyecto.findByBitacoraProyecto", query = "SELECT n FROM NotificacionGobiernoProyecto n WHERE n.notificacionGobiernoProyectoPK.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "NotificacionGobiernoProyecto.findByNotificaciongId", query = "SELECT n FROM NotificacionGobiernoProyecto n WHERE n.notificacionGobiernoProyectoPK.notificaciongId = :notificaciongId"),
    @NamedQuery(name = "NotificacionGobiernoProyecto.findByNotificaciongOficioNumero", query = "SELECT n FROM NotificacionGobiernoProyecto n WHERE n.notificaciongOficioNumero = :notificaciongOficioNumero"),
    @NamedQuery(name = "NotificacionGobiernoProyecto.findByNotificaciongOficioFecha", query = "SELECT n FROM NotificacionGobiernoProyecto n WHERE n.notificaciongOficioFecha = :notificaciongOficioFecha"),
    @NamedQuery(name = "NotificacionGobiernoProyecto.findByNotificaciongDepDesc", query = "SELECT n FROM NotificacionGobiernoProyecto n WHERE n.notificaciongDepDesc = :notificaciongDepDesc"),
    @NamedQuery(name = "NotificacionGobiernoProyecto.findByNotificaciongSiglas", query = "SELECT n FROM NotificacionGobiernoProyecto n WHERE n.notificaciongSiglas = :notificaciongSiglas")})
public class NotificacionGobiernoProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotificacionGobiernoProyectoPK notificacionGobiernoProyectoPK;
    @Size(max = 100)
    @Column(name = "NOTIFICACIONG_OFICIO_NUMERO")
    private String notificaciongOficioNumero;
    @Column(name = "NOTIFICACIONG_OFICIO_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date notificaciongOficioFecha;
    @Size(max = 300)
    @Column(name = "NOTIFICACIONG_DEP_DESC")
    private String notificaciongDepDesc;
    @Size(max = 100)
    @Column(name = "NOTIFICACIONG_SIGLAS")
    private String notificaciongSiglas; 
    @Size(max = 4000)
    @Column(name = "NOTIFICACIONG_GOBIERNO")
    private String notificaciongGobierno;
    
    @JoinColumn(name = "CAT_DEPENDENCIA_ID", referencedColumnName = "CAT_DEPENDENCIA_ID")
    @ManyToOne
    private CatDependenciaOficio catDependenciaId;

    public NotificacionGobiernoProyecto() {
    }

    public NotificacionGobiernoProyecto(NotificacionGobiernoProyectoPK notificacionGobiernoProyectoPK) {
        this.notificacionGobiernoProyectoPK = notificacionGobiernoProyectoPK;
    }

    public NotificacionGobiernoProyecto(String bitacoraProyecto, String notificaciongId) {
        this.notificacionGobiernoProyectoPK = new NotificacionGobiernoProyectoPK(bitacoraProyecto, notificaciongId);
    }

    public NotificacionGobiernoProyectoPK getNotificacionGobiernoProyectoPK() {
        return notificacionGobiernoProyectoPK;
    }

    public void setNotificacionGobiernoProyectoPK(NotificacionGobiernoProyectoPK notificacionGobiernoProyectoPK) {
        this.notificacionGobiernoProyectoPK = notificacionGobiernoProyectoPK;
    }

    public String getNotificaciongOficioNumero() {
        return notificaciongOficioNumero;
    }

    public void setNotificaciongOficioNumero(String notificaciongOficioNumero) {
        this.notificaciongOficioNumero = notificaciongOficioNumero;
    }

    public Date getNotificaciongOficioFecha() {
        return notificaciongOficioFecha;
    }

    public void setNotificaciongOficioFecha(Date notificaciongOficioFecha) {
        this.notificaciongOficioFecha = notificaciongOficioFecha;
    }

    public String getNotificaciongDepDesc() {
        return notificaciongDepDesc;
    }

    public void setNotificaciongDepDesc(String notificaciongDepDesc) {
        this.notificaciongDepDesc = notificaciongDepDesc;
    }

    public String getNotificaciongSiglas() {
        return notificaciongSiglas;
    }

    public void setNotificaciongSiglas(String notificaciongSiglas) {
        this.notificaciongSiglas = notificaciongSiglas;
    }

    public CatDependenciaOficio getCatDependenciaId() {
        return catDependenciaId;
    }

    public void setCatDependenciaId(CatDependenciaOficio catDependenciaId) {
        this.catDependenciaId = catDependenciaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificacionGobiernoProyectoPK != null ? notificacionGobiernoProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionGobiernoProyecto)) {
            return false;
        }
        NotificacionGobiernoProyecto other = (NotificacionGobiernoProyecto) object;
        if ((this.notificacionGobiernoProyectoPK == null && other.notificacionGobiernoProyectoPK != null) || (this.notificacionGobiernoProyectoPK != null && !this.notificacionGobiernoProyectoPK.equals(other.notificacionGobiernoProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.NotificacionGobiernoProyecto[ notificacionGobiernoProyectoPK=" + notificacionGobiernoProyectoPK + " ]";
    }

    /**
     * @return the notificaciongGobierno
     */
    public String getNotificaciongGobierno() {
        return notificaciongGobierno;
    }

    /**
     * @param notificaciongGobierno the notificaciongGobierno to set
     */
    public void setNotificaciongGobierno(String notificaciongGobierno) {
        this.notificaciongGobierno = notificaciongGobierno;
    }
    
}
