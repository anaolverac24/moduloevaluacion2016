/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class LeyProyectoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYETO")
    private String folioProyeto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "LEY_PROYECTO_ID")
    private short leyProyectoId;

    public LeyProyectoPK() {
    }

    public LeyProyectoPK(String folioProyeto, short serialProyecto, short leyProyectoId) {
        this.folioProyeto = folioProyeto;
        this.serialProyecto = serialProyecto;
        this.leyProyectoId = leyProyectoId;
    }

    public String getFolioProyeto() {
        return folioProyeto;
    }

    public void setFolioProyeto(String folioProyeto) {
        this.folioProyeto = folioProyeto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public short getLeyProyectoId() {
        return leyProyectoId;
    }

    public void setLeyProyectoId(short leyProyectoId) {
        this.leyProyectoId = leyProyectoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyeto != null ? folioProyeto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) leyProyectoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeyProyectoPK)) {
            return false;
        }
        LeyProyectoPK other = (LeyProyectoPK) object;
        if ((this.folioProyeto == null && other.folioProyeto != null) || (this.folioProyeto != null && !this.folioProyeto.equals(other.folioProyeto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.leyProyectoId != other.leyProyectoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.LeyProyectoPK[ folioProyeto=" + folioProyeto + ", serialProyecto=" + serialProyecto + ", leyProyectoId=" + leyProyectoId + " ]";
    }
    
}
