/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import mx.gob.semarnat.view.visorMia.SueloVegetacionDetalle;


/**
 *
 * @author Paty
 */
@Entity
@Table(name = "SUELO_VEGETACION_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SueloVegetacionProyecto.findAll", query = "SELECT s FROM SueloVegetacionProyecto s"),
    @NamedQuery(name = "SueloVegetacionProyecto.findByFolioProyecto", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloVegetacionProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySerialProyecto", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloVegetacionProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloVegProyId", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloVegetacionProyectoPK.sueloVegProyId = :sueloVegProyId"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloComponente", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloComponente = :sueloComponente"),
    @NamedQuery(name = "SueloVegetacionProyecto.findByUsoSueloInegi", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.usoSueloInegi = :usoSueloInegi"),
    @NamedQuery(name = "SueloVegetacionProyecto.findByUsoSueloActual", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.usoSueloActual = :usoSueloActual"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloDescripcion", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloDescripcion = :sueloDescripcion"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloAreaSigeia", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloAreaSigeia = :sueloAreaSigeia"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloTipoEcov", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloTipoEcov = :sueloTipoEcov"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloTipoGen", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloTipoGen = :sueloTipoGen"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloFaseVs", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloFaseVs = :sueloFaseVs"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloValidaResultado", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloValidaResultado = :sueloValidaResultado"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloCatUso", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloCatUso = :sueloCatUso"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloSuperficeProm", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloSuperficeProm = :sueloSuperficeProm"),
    @NamedQuery(name = "SueloVegetacionProyecto.findBySueloDiagnostico", query = "SELECT s FROM SueloVegetacionProyecto s WHERE s.sueloDiagnostico = :sueloDiagnostico")})
public class SueloVegetacionProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SueloVegetacionProyectoPK sueloVegetacionProyectoPK;
    @Size(max = 254)
    @Column(name = "SUELO_COMPONENTE")
    private String sueloComponente;
    @Size(max = 254)
    @Column(name = "USO_SUELO_INEGI")
    private String usoSueloInegi;
    @Size(max = 300)
    @Column(name = "USO_SUELO_ACTUAL")
    private String usoSueloActual;
    @Size(max = 1000)
    @Column(name = "SUELO_DESCRIPCION")
    private String sueloDescripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUELO_AREA_SIGEIA")
    private Double sueloAreaSigeia;
    @Size(max = 254)
    @Column(name = "SUELO_TIPO_ECOV")
    private String sueloTipoEcov;
    @Size(max = 254)
    @Column(name = "SUELO_TIPO_GEN")
    private String sueloTipoGen;
    @Size(max = 254)
    @Column(name = "SUELO_FASE_VS")
    private String sueloFaseVs;
    @Size(max = 1)
    @Column(name = "SUELO_VALIDA_RESULTADO")
    private String sueloValidaResultado;
    @Size(max = 300)
    @Column(name = "SUELO_CAT_USO")
    private String sueloCatUso;
    @Column(name = "SUELO_SUPERFICE_PROM")
    private BigDecimal sueloSuperficeProm;
    @Size(max = 1500)
    @Column(name = "SUELO_DIAGNOSTICO")
    private String sueloDiagnostico;
    
    @OneToMany(mappedBy = "sueloVegetacionProyecto")
    private List<SueloVegetacionDetalle> listaDetalle = new ArrayList<SueloVegetacionDetalle>();
    

    public SueloVegetacionProyecto() {
    }

    public SueloVegetacionProyecto(SueloVegetacionProyectoPK sueloVegetacionProyectoPK) {
        this.sueloVegetacionProyectoPK = sueloVegetacionProyectoPK;
    }

    public SueloVegetacionProyecto(String folioProyecto, short serialProyecto, short sueloVegProyId) {
        this.sueloVegetacionProyectoPK = new SueloVegetacionProyectoPK(folioProyecto, serialProyecto, sueloVegProyId);
    }

    public SueloVegetacionProyectoPK getSueloVegetacionProyectoPK() {
        return sueloVegetacionProyectoPK;
    }

    public void setSueloVegetacionProyectoPK(SueloVegetacionProyectoPK sueloVegetacionProyectoPK) {
        this.sueloVegetacionProyectoPK = sueloVegetacionProyectoPK;
    }

    public String getSueloComponente() {
        return sueloComponente;
    }

    public void setSueloComponente(String sueloComponente) {
        this.sueloComponente = sueloComponente;
    }

    public String getUsoSueloInegi() {
        return usoSueloInegi;
    }

    public void setUsoSueloInegi(String usoSueloInegi) {
        this.usoSueloInegi = usoSueloInegi;
    }

    public String getUsoSueloActual() {
        return usoSueloActual;
    }

    public void setUsoSueloActual(String usoSueloActual) {
        this.usoSueloActual = usoSueloActual;
    }

    public String getSueloDescripcion() {
        return sueloDescripcion;
    }

    public void setSueloDescripcion(String sueloDescripcion) {
        this.sueloDescripcion = sueloDescripcion;
    }

    public Double getSueloAreaSigeia() {
        return sueloAreaSigeia;
    }

    public void setSueloAreaSigeia(Double sueloAreaSigeia) {
        this.sueloAreaSigeia = sueloAreaSigeia;
    }

    public String getSueloTipoEcov() {
        return sueloTipoEcov;
    }

    public void setSueloTipoEcov(String sueloTipoEcov) {
        this.sueloTipoEcov = sueloTipoEcov;
    }

    public String getSueloTipoGen() {
        return sueloTipoGen;
    }

    public void setSueloTipoGen(String sueloTipoGen) {
        this.sueloTipoGen = sueloTipoGen;
    }

    public String getSueloFaseVs() {
        return sueloFaseVs;
    }

    public void setSueloFaseVs(String sueloFaseVs) {
        this.sueloFaseVs = sueloFaseVs;
    }

    public String getSueloValidaResultado() {
        return sueloValidaResultado;
    }

    public void setSueloValidaResultado(String sueloValidaResultado) {
        this.sueloValidaResultado = sueloValidaResultado;
    }

    public String getSueloCatUso() {
        return sueloCatUso;
    }

    public void setSueloCatUso(String sueloCatUso) {
        this.sueloCatUso = sueloCatUso;
    }

    public BigDecimal getSueloSuperficeProm() {
        return sueloSuperficeProm;
    }

    public void setSueloSuperficeProm(BigDecimal sueloSuperficeProm) {
        this.sueloSuperficeProm = sueloSuperficeProm;
    }

    public String getSueloDiagnostico() {
        return sueloDiagnostico;
    }

    public void setSueloDiagnostico(String sueloDiagnostico) {
        this.sueloDiagnostico = sueloDiagnostico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sueloVegetacionProyectoPK != null ? sueloVegetacionProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SueloVegetacionProyecto)) {
            return false;
        }
        SueloVegetacionProyecto other = (SueloVegetacionProyecto) object;
        if ((this.sueloVegetacionProyectoPK == null && other.sueloVegetacionProyectoPK != null) || (this.sueloVegetacionProyectoPK != null && !this.sueloVegetacionProyectoPK.equals(other.sueloVegetacionProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.SueloVegetacionProyecto[ sueloVegetacionProyectoPK=" + sueloVegetacionProyectoPK + " ]";
    }
    
    
    public List<SueloVegetacionDetalle> getListaDetalle() {
		return listaDetalle;
	}

	/**
	 * @param listaDetalle the listaDetalle to set
	 */
	public void setListaDetalle(List<SueloVegetacionDetalle> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}
    
    
}
