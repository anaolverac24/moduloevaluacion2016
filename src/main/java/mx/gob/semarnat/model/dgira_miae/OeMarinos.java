/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "OE_MARINOS", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OeMarinos.findAll", query = "SELECT o FROM OeMarinos o"),
    @NamedQuery(name = "OeMarinos.findByNumFolio", query = "SELECT o FROM OeMarinos o WHERE o.oeMarinosPK.numFolio = :numFolio"),
    @NamedQuery(name = "OeMarinos.findByCveProy", query = "SELECT o FROM OeMarinos o WHERE o.oeMarinosPK.cveProy = :cveProy"),
    @NamedQuery(name = "OeMarinos.findByCveArea", query = "SELECT o FROM OeMarinos o WHERE o.oeMarinosPK.cveArea = :cveArea"),
    @NamedQuery(name = "OeMarinos.findByVersion", query = "SELECT o FROM OeMarinos o WHERE o.oeMarinosPK.version = :version"),
    @NamedQuery(name = "OeMarinos.findByNomOe", query = "SELECT o FROM OeMarinos o WHERE o.nomOe = :nomOe"),
    @NamedQuery(name = "OeMarinos.findByTipoOe", query = "SELECT o FROM OeMarinos o WHERE o.tipoOe = :tipoOe"),
    @NamedQuery(name = "OeMarinos.findByUga", query = "SELECT o FROM OeMarinos o WHERE o.uga = :uga"),
    @NamedQuery(name = "OeMarinos.findByUgaComp", query = "SELECT o FROM OeMarinos o WHERE o.ugaComp = :ugaComp"),
    @NamedQuery(name = "OeMarinos.findByPolitica", query = "SELECT o FROM OeMarinos o WHERE o.politica = :politica"),
    @NamedQuery(name = "OeMarinos.findByPolMapear", query = "SELECT o FROM OeMarinos o WHERE o.polMapear = :polMapear"),
    @NamedQuery(name = "OeMarinos.findByUsoPred", query = "SELECT o FROM OeMarinos o WHERE o.usoPred = :usoPred"),
    @NamedQuery(name = "OeMarinos.findByCriterios", query = "SELECT o FROM OeMarinos o WHERE o.criterios = :criterios"),
    @NamedQuery(name = "OeMarinos.findBySupEa", query = "SELECT o FROM OeMarinos o WHERE o.supEa = :supEa"),
    @NamedQuery(name = "OeMarinos.findByProy", query = "SELECT o FROM OeMarinos o WHERE o.proy = :proy"),
    @NamedQuery(name = "OeMarinos.findByComp", query = "SELECT o FROM OeMarinos o WHERE o.comp = :comp"),
    @NamedQuery(name = "OeMarinos.findByDescrip", query = "SELECT o FROM OeMarinos o WHERE o.descrip = :descrip"),
    @NamedQuery(name = "OeMarinos.findByAreabuffer", query = "SELECT o FROM OeMarinos o WHERE o.areabuffer = :areabuffer"),
    @NamedQuery(name = "OeMarinos.findByArea", query = "SELECT o FROM OeMarinos o WHERE o.area = :area"),
    @NamedQuery(name = "OeMarinos.findByFechaHora", query = "SELECT o FROM OeMarinos o WHERE o.fechaHora = :fechaHora"),
    @NamedQuery(name = "OeMarinos.findByIdr", query = "SELECT o FROM OeMarinos o WHERE o.oeMarinosPK.idr = :idr")})
public class OeMarinos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OeMarinosPK oeMarinosPK;
    @Size(max = 200)
    @Column(name = "NOM_OE")
    private String nomOe;
    @Size(max = 80)
    @Column(name = "TIPO_OE")
    private String tipoOe;
    @Size(max = 80)
    @Column(name = "UGA")
    private String uga;
    @Size(max = 1000)
    @Column(name = "UGA_COMP")
    private String ugaComp;
    @Size(max = 80)
    @Column(name = "POLITICA")
    private String politica;
    @Size(max = 80)
    @Column(name = "POL_MAPEAR")
    private String polMapear;
    @Size(max = 80)
    @Column(name = "USO_PRED")
    private String usoPred;
    @Size(max = 80)
    @Column(name = "CRITERIOS")
    private String criterios;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Size(max = 80)
    @Column(name = "PROY")
    private String proy;
    @Size(max = 80)
    @Column(name = "COMP")
    private String comp;
    @Size(max = 80)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    public OeMarinos() {
    }

    public OeMarinos(OeMarinosPK oeMarinosPK) {
        this.oeMarinosPK = oeMarinosPK;
    }

    public OeMarinos(String numFolio, String cveProy, String cveArea, short version, BigInteger idr) {
        this.oeMarinosPK = new OeMarinosPK(numFolio, cveProy, cveArea, version, idr);
    }

    public OeMarinosPK getOeMarinosPK() {
        return oeMarinosPK;
    }

    public void setOeMarinosPK(OeMarinosPK oeMarinosPK) {
        this.oeMarinosPK = oeMarinosPK;
    }

    public String getNomOe() {
        return nomOe;
    }

    public void setNomOe(String nomOe) {
        this.nomOe = nomOe;
    }

    public String getTipoOe() {
        return tipoOe;
    }

    public void setTipoOe(String tipoOe) {
        this.tipoOe = tipoOe;
    }

    public String getUga() {
        return uga;
    }

    public void setUga(String uga) {
        this.uga = uga;
    }

    public String getUgaComp() {
        return ugaComp;
    }

    public void setUgaComp(String ugaComp) {
        this.ugaComp = ugaComp;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

    public String getPolMapear() {
        return polMapear;
    }

    public void setPolMapear(String polMapear) {
        this.polMapear = polMapear;
    }

    public String getUsoPred() {
        return usoPred;
    }

    public void setUsoPred(String usoPred) {
        this.usoPred = usoPred;
    }

    public String getCriterios() {
        return criterios;
    }

    public void setCriterios(String criterios) {
        this.criterios = criterios;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oeMarinosPK != null ? oeMarinosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OeMarinos)) {
            return false;
        }
        OeMarinos other = (OeMarinos) object;
        if ((this.oeMarinosPK == null && other.oeMarinosPK != null) || (this.oeMarinosPK != null && !this.oeMarinosPK.equals(other.oeMarinosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.OeMarinos[ oeMarinosPK=" + oeMarinosPK + " ]";
    }
    
}
