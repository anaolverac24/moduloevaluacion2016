/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_TIPO_IMPACTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipoImpacto.findAll", query = "SELECT c FROM CatTipoImpacto c"),
    @NamedQuery(name = "CatTipoImpacto.findByTipoImpactoId", query = "SELECT c FROM CatTipoImpacto c WHERE c.tipoImpactoId = :tipoImpactoId"),
    @NamedQuery(name = "CatTipoImpacto.findByTipoImpactoDescripcion", query = "SELECT c FROM CatTipoImpacto c WHERE c.tipoImpactoDescripcion = :tipoImpactoDescripcion")})
public class CatTipoImpacto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_IMPACTO_ID")
    private Short tipoImpactoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "TIPO_IMPACTO_DESCRIPCION")
    private String tipoImpactoDescripcion;

    public CatTipoImpacto() {
    }

    public CatTipoImpacto(Short tipoImpactoId) {
        this.tipoImpactoId = tipoImpactoId;
    }

    public CatTipoImpacto(Short tipoImpactoId, String tipoImpactoDescripcion) {
        this.tipoImpactoId = tipoImpactoId;
        this.tipoImpactoDescripcion = tipoImpactoDescripcion;
    }

    public Short getTipoImpactoId() {
        return tipoImpactoId;
    }

    public void setTipoImpactoId(Short tipoImpactoId) {
        this.tipoImpactoId = tipoImpactoId;
    }

    public String getTipoImpactoDescripcion() {
        return tipoImpactoDescripcion;
    }

    public void setTipoImpactoDescripcion(String tipoImpactoDescripcion) {
        this.tipoImpactoDescripcion = tipoImpactoDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoImpactoId != null ? tipoImpactoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoImpacto)) {
            return false;
        }
        CatTipoImpacto other = (CatTipoImpacto) object;
        if ((this.tipoImpactoId == null && other.tipoImpactoId != null) || (this.tipoImpactoId != null && !this.tipoImpactoId.equals(other.tipoImpactoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.CatTipoImpacto[ tipoImpactoId=" + tipoImpactoId + " ]";
    }
    
}
