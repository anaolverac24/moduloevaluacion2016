/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.dgira_miae;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "PDUM_PROYECTO", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PdumProyecto.findAll", query = "SELECT p FROM PdumProyecto p"),
    @NamedQuery(name = "PdumProyecto.findByFolioProyecto", query = "SELECT p FROM PdumProyecto p WHERE p.pdumProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PdumProyecto.findBySerialProyecto", query = "SELECT p FROM PdumProyecto p WHERE p.pdumProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PdumProyecto.findByPdumId", query = "SELECT p FROM PdumProyecto p WHERE p.pdumProyectoPK.pdumId = :pdumId"),
    @NamedQuery(name = "PdumProyecto.findByPdumNombre", query = "SELECT p FROM PdumProyecto p WHERE p.pdumNombre = :pdumNombre"),
    @NamedQuery(name = "PdumProyecto.findByPdumUsos", query = "SELECT p FROM PdumProyecto p WHERE p.pdumUsos = :pdumUsos"),
    @NamedQuery(name = "PdumProyecto.findByPdumClaveUsos", query = "SELECT p FROM PdumProyecto p WHERE p.pdumClaveUsos = :pdumClaveUsos"),
    @NamedQuery(name = "PdumProyecto.findByPdumCos", query = "SELECT p FROM PdumProyecto p WHERE p.pdumCos = :pdumCos"),
    @NamedQuery(name = "PdumProyecto.findByPdumCus", query = "SELECT p FROM PdumProyecto p WHERE p.pdumCus = :pdumCus"),
    @NamedQuery(name = "PdumProyecto.findByFolSer", query = "SELECT a FROM PdumProyecto a where a.pdumProyectoPK.folioProyecto = :folio and a.pdumProyectoPK.serialProyecto = :serial"),
    @NamedQuery(name = "PdumProyecto.findByPdumVinculacion", query = "SELECT p FROM PdumProyecto p WHERE p.pdumVinculacion = :pdumVinculacion")})
public class PdumProyecto implements Serializable {
	private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PdumProyectoPK pdumProyectoPK;
    @Column(name = "PDUM_NOMBRE")
    private String pdumNombre;
    @Column(name = "PDUM_USOS")
    private String pdumUsos;
    @Column(name = "PDUM_CLAVE_USOS")
    private String pdumClaveUsos;
    @Column(name = "PDUM_COS")
    private String pdumCos;
    @Column(name = "PDUM_CUS")
    private String pdumCus;
    @Column(name = "PDUM_VINCULACION")
    private String pdumVinculacion;
    @Column(name = "PDUM_COMP")
    private String pdumComp;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    
    @Column(name="PDUM_FECHA_PUBLICACION")
    private String pdumFechaPublicacion;
    @Transient
    private Date fechaPublicacion;
    
    @Column(name="CLAVE_ORIGEN_REGISTRO")
    private Integer claveOrigenRegistro;
    
    @Transient
    private Boolean deSigeia;
    
//-Se hace la union para la tabla de archivos proyecto
    @JoinColumn(name = "ID_ARCHIVO_PROGRAMA", referencedColumnName = "SEQ_ID")
    @ManyToOne
    private ArchivosProyecto idArchivoProyecto;
    

    public PdumProyecto() {
        this.deSigeia = false;
    }

    public PdumProyecto(PdumProyectoPK pdumProyectoPK) {
        this.pdumProyectoPK = pdumProyectoPK;
    }

    public PdumProyecto(String folioProyecto, short serialProyecto, short pdumId) {
        this.pdumProyectoPK = new PdumProyectoPK(folioProyecto, serialProyecto, pdumId);
    }

    public PdumProyectoPK getPdumProyectoPK() {
        return pdumProyectoPK;
    }

    public void setPdumProyectoPK(PdumProyectoPK pdumProyectoPK) {
        this.pdumProyectoPK = pdumProyectoPK;
    }

    public String getPdumNombre() {
        return pdumNombre;
    }

    public void setPdumNombre(String pdumNombre) {
        this.pdumNombre = pdumNombre;
    }

    public String getPdumUsos() {
        return pdumUsos;
    }

    public void setPdumUsos(String pdumUsos) {
        this.pdumUsos = pdumUsos;
    }

    public String getPdumClaveUsos() {
        return pdumClaveUsos;
    }

    public void setPdumClaveUsos(String pdumClaveUsos) {
        this.pdumClaveUsos = pdumClaveUsos;
    }

    public String getPdumCos() {
        return pdumCos;
    }

    public void setPdumCos(String pdumCos) {
        this.pdumCos = pdumCos;
    }

    public String getPdumCus() {
        return pdumCus;
    }

    public void setPdumCus(String pdumCus) {
        this.pdumCus = pdumCus;
    }

    public String getPdumVinculacion() {
        return pdumVinculacion;
    }
    
    public String getPdumVinculacionCorta(){
    	if (pdumVinculacion != null) {
            if(pdumVinculacion.length()<199){
                return pdumVinculacion;
            }
            return pdumVinculacion.substring(0, 199);			
		} else {
	    	return pdumVinculacion;			
		}
    }

    public void setPdumVinculacion(String pdumVinculacion) {
        this.pdumVinculacion = pdumVinculacion;
    }

    public String getPdumComp() {
        return pdumComp;
    }

    public void setPdumComp(String pdumComp) {
        this.pdumComp = pdumComp;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdumProyectoPK != null ? pdumProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PdumProyecto)) {
            return false;
        }
        PdumProyecto other = (PdumProyecto) object;
        if ((this.pdumProyectoPK == null && other.pdumProyectoPK != null) || (this.pdumProyectoPK != null && !this.pdumProyectoPK.equals(other.pdumProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.PdumProyecto[ pdumProyectoPK=" + pdumProyectoPK + " ]";
    }

    public String getPdumFechaPublicacion() {
        return pdumFechaPublicacion;
    }

    public void setPdumFechaPublicacion(String pdumFechaPublicacion) {
        this.pdumFechaPublicacion = pdumFechaPublicacion;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Boolean getDeSigeia() {
        return deSigeia;
    }

    public void setDeSigeia(Boolean deSigeia) {
        this.deSigeia = deSigeia;
    }

	/**
	 * @return the claveOrigenRegistro
	 */
	public Integer getClaveOrigenRegistro() {
		return claveOrigenRegistro;
	}

	/**
	 * @param claveOrigenRegistro the claveOrigenRegistro to set
	 */
	public void setClaveOrigenRegistro(Integer claveOrigenRegistro) {
		this.claveOrigenRegistro = claveOrigenRegistro;
	}

	public ArchivosProyecto getIdArchivoProyecto() {
		return idArchivoProyecto;
	}

	public void setIdArchivoProyecto(ArchivosProyecto idArchivoProyecto) {
		this.idArchivoProyecto = idArchivoProyecto;
	}
}
