/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "COMENTARIO_PROYECTO", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComentarioProyecto.findAll", query = "SELECT c FROM ComentarioProyecto c"),
    @NamedQuery(name = "ComentarioProyecto.findByBitacoraProyecto", query = "SELECT c FROM ComentarioProyecto c WHERE c.comentarioProyectoPK.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "ComentarioProyecto.findByCapituloId", query = "SELECT c FROM ComentarioProyecto c WHERE c.comentarioProyectoPK.capituloId = :capituloId"),
    @NamedQuery(name = "ComentarioProyecto.findBySubcapituloId", query = "SELECT c FROM ComentarioProyecto c WHERE c.comentarioProyectoPK.subcapituloId = :subcapituloId"),
    @NamedQuery(name = "ComentarioProyecto.findBySeccionId", query = "SELECT c FROM ComentarioProyecto c WHERE c.comentarioProyectoPK.seccionId = :seccionId"),
    @NamedQuery(name = "ComentarioProyecto.findByApartadoId", query = "SELECT c FROM ComentarioProyecto c WHERE c.comentarioProyectoPK.apartadoId = :apartadoId"),
    @NamedQuery(name = "ComentarioProyecto.findByComentarioIdusuario", query = "SELECT c FROM ComentarioProyecto c WHERE c.comentarioProyectoPK.comentarioIdusuario = :comentarioIdusuario"),
    @NamedQuery(name = "ComentarioProyecto.findByComentarioInfoAdicional", query = "SELECT c FROM ComentarioProyecto c WHERE c.comentarioInfoAdicional = :comentarioInfoAdicional"),
    @NamedQuery(name = "ComentarioProyecto.findByInfoAdicional", query = "SELECT c FROM ComentarioProyecto c WHERE c.infoAdicional = :infoAdicional"),
    @NamedQuery(name = "ComentarioProyecto.findBySecModificada", query = "SELECT c FROM ComentarioProyecto c WHERE c.secModificada = :secModificada")})
public class ComentarioProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ComentarioProyectoPK comentarioProyectoPK;
    //@Lob
    @Column(name = "COMENTARIO_RESOLUTIVO")
    private String comentarioResolutivo;
    @Column(name = "COMENTARIO_INFO_ADICIONAL")
    private String comentarioInfoAdicional;
    @Lob
    @Column(name = "COMENTARIO_MEMORIA_TECNICA")
    private String comentarioMemoriaTecnica;
    @Column(name = "INFO_ADICIONAL")
    private String infoAdicional;
    @Column(name = "SEC_MODIFICADA")
    private String secModificada;
    @JoinColumn(name = "BITACORA_PROYECTO", referencedColumnName = "BITACORA_PROYECTO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private EvaluacionProyecto evaluacionProyecto;

    public ComentarioProyecto() {
    }

    public ComentarioProyecto(ComentarioProyectoPK comentarioProyectoPK) {
        this.comentarioProyectoPK = comentarioProyectoPK;
    }

    public ComentarioProyecto(String bitacoraProyecto, short capituloId, short subcapituloId, short seccionId, short apartadoId, Integer comentarioIdusuario) {
        this.comentarioProyectoPK = new ComentarioProyectoPK(bitacoraProyecto, capituloId, subcapituloId, seccionId, apartadoId, comentarioIdusuario);
    }

    public ComentarioProyectoPK getComentarioProyectoPK() {
        return comentarioProyectoPK;
    }

    public void setComentarioProyectoPK(ComentarioProyectoPK comentarioProyectoPK) {
        this.comentarioProyectoPK = comentarioProyectoPK;
    }

    public String getComentarioResolutivo() {
        return comentarioResolutivo;
    }

    public void setComentarioResolutivo(String comentarioResolutivo) {
        this.comentarioResolutivo = comentarioResolutivo;
    }

    public String getComentarioInfoAdicional() {
        return comentarioInfoAdicional;
    }

    public void setComentarioInfoAdicional(String comentarioInfoAdicional) {
        this.comentarioInfoAdicional = comentarioInfoAdicional;
    }

    public String getComentarioMemoriaTecnica() {
        return comentarioMemoriaTecnica;
    }

    public void setComentarioMemoriaTecnica(String comentarioMemoriaTecnica) {
        this.comentarioMemoriaTecnica = comentarioMemoriaTecnica;
    }

    public String getInfoAdicional() {
        return infoAdicional;
    }

    public void setInfoAdicional(String infoAdicional) {
        this.infoAdicional = infoAdicional;
    }

    public String getSecModificada() {
        return secModificada;
    }

    public void setSecModificada(String secModificada) {
        this.secModificada = secModificada;
    }

    public EvaluacionProyecto getEvaluacionProyecto() {
        return evaluacionProyecto;
    }

    public void setEvaluacionProyecto(EvaluacionProyecto evaluacionProyecto) {
        this.evaluacionProyecto = evaluacionProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comentarioProyectoPK != null ? comentarioProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComentarioProyecto)) {
            return false;
        }
        ComentarioProyecto other = (ComentarioProyecto) object;
        if ((this.comentarioProyectoPK == null && other.comentarioProyectoPK != null) || (this.comentarioProyectoPK != null && !this.comentarioProyectoPK.equals(other.comentarioProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.dgira_miae.ComentarioProyecto[ comentarioProyectoPK=" + comentarioProyectoPK + " ]";
    }
    
}
