/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.dgira_miae;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Case Solutions 10
 */
@Entity
@Table(name = "PROYSIG", schema="DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proysig.findAll", query = "SELECT p FROM Proysig p"),
    @NamedQuery(name = "Proysig.findByObjectid", query = "SELECT p FROM Proysig p WHERE p.proysigPK.objectid = :objectid"),
    @NamedQuery(name = "Proysig.findByNumFolio", query = "SELECT p FROM Proysig p WHERE p.proysigPK.numFolio = :numFolio"),
    @NamedQuery(name = "Proysig.findByNumFolioOrderVersion", 
            query = "SELECT p FROM Proysig p WHERE p.proysigPK.numFolio = :numFolio ORDER BY p.proysigPK.version DESC"),
    @NamedQuery(name = "Proysig.findByCveProy", query = "SELECT p FROM Proysig p WHERE p.proysigPK.cveProy = :cveProy"),
    @NamedQuery(name = "Proysig.findByCveArea", query = "SELECT p FROM Proysig p WHERE p.proysigPK.cveArea = :cveArea"),
    @NamedQuery(name = "Proysig.findByVersion", query = "SELECT p FROM Proysig p WHERE p.proysigPK.version = :version"),
    @NamedQuery(name = "Proysig.findByProy", query = "SELECT p FROM Proysig p WHERE p.proy = :proy"),
    @NamedQuery(name = "Proysig.findByDescrip", query = "SELECT p FROM Proysig p WHERE p.descrip = :descrip"),
    @NamedQuery(name = "Proysig.findByComp", query = "SELECT p FROM Proysig p WHERE p.comp = :comp"),
    @NamedQuery(name = "Proysig.findByResolucion", query = "SELECT p FROM Proysig p WHERE p.resolucion = :resolucion"),
    @NamedQuery(name = "Proysig.findByIdRes", query = "SELECT p FROM Proysig p WHERE p.idRes = :idRes"),
    @NamedQuery(name = "Proysig.findById", query = "SELECT p FROM Proysig p WHERE p.id = :id"),
    @NamedQuery(name = "Proysig.findByRowlayer", query = "SELECT p FROM Proysig p WHERE p.rowlayer = :rowlayer"),
    @NamedQuery(name = "Proysig.findByObjectid1", query = "SELECT p FROM Proysig p WHERE p.objectid1 = :objectid1"),
    @NamedQuery(name = "Proysig.findByObjectid2", query = "SELECT p FROM Proysig p WHERE p.objectid2 = :objectid2"),
    @NamedQuery(name = "Proysig.findByShape", query = "SELECT p FROM Proysig p WHERE p.shape = :shape"),
    @NamedQuery(name = "Proysig.findByShapeArea", query = "SELECT p FROM Proysig p WHERE p.shapeArea = :shapeArea"),
    @NamedQuery(name = "Proysig.findByShapeLen", query = "SELECT p FROM Proysig p WHERE p.shapeLen = :shapeLen")})
public class Proysig implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProysigPK proysigPK;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "RESOLUCION")
    private String resolucion;
    @Column(name = "ID_RES")
    private Integer idRes;
    @Column(name = "ID")
    private Long id;
    @Column(name = "ROWLAYER")
    private Long rowlayer;
    @Column(name = "OBJECTID_1")
    private Long objectid1;
    @Column(name = "OBJECTID_2")
    private Long objectid2;
    @Column(name = "SHAPE")
    private Integer shape;
    @Column(name = "SHAPE_AREA")
    private Long shapeArea;
    @Column(name = "SHAPE_LEN")
    private BigInteger shapeLen;
    @JoinColumn(name = "PROYLIN_ID", referencedColumnName = "ID")
    @ManyToOne
    private Proylin proylinId;
    @JoinColumn(name = "PROYMPUN_ID", referencedColumnName = "ID")
    @ManyToOne
    private Proympun proympunId;
    @JoinColumn(name = "PROYPOL_ID", referencedColumnName = "ID")
    @ManyToOne
    private Proypol proypolId;

    public Proysig() {
    }

    public Proysig(ProysigPK proysigPK) {
        this.proysigPK = proysigPK;
    }

    public Proysig(BigInteger objectid, String numFolio, String cveProy, String cveArea, short version) {
        this.proysigPK = new ProysigPK(objectid, numFolio, cveProy, cveArea, version);
    }

    public ProysigPK getProysigPK() {
        return proysigPK;
    }

    public void setProysigPK(ProysigPK proysigPK) {
        this.proysigPK = proysigPK;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public Integer getIdRes() {
        return idRes;
    }

    public void setIdRes(Integer idRes) {
        this.idRes = idRes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRowlayer() {
        return rowlayer;
    }

    public void setRowlayer(Long rowlayer) {
        this.rowlayer = rowlayer;
    }

    public Long getObjectid1() {
        return objectid1;
    }

    public void setObjectid1(Long objectid1) {
        this.objectid1 = objectid1;
    }

    public Long getObjectid2() {
        return objectid2;
    }

    public void setObjectid2(Long objectid2) {
        this.objectid2 = objectid2;
    }

    public Integer getShape() {
        return shape;
    }

    public void setShape(Integer shape) {
        this.shape = shape;
    }

    public Long getShapeArea() {
        return shapeArea;
    }

    public void setShapeArea(Long shapeArea) {
        this.shapeArea = shapeArea;
    }

    public BigInteger getShapeLen() {
        return shapeLen;
    }

    public void setShapeLen(BigInteger shapeLen) {
        this.shapeLen = shapeLen;
    }

    public Proylin getProylinId() {
        return proylinId;
    }

    public void setProylinId(Proylin proylinId) {
        this.proylinId = proylinId;
    }

    public Proympun getProympunId() {
        return proympunId;
    }

    public void setProympunId(Proympun proympunId) {
        this.proympunId = proympunId;
    }

    public Proypol getProypolId() {
        return proypolId;
    }

    public void setProypolId(Proypol proypolId) {
        this.proypolId = proypolId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proysigPK != null ? proysigPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proysig)) {
            return false;
        }
        Proysig other = (Proysig) object;
        if ((this.proysigPK == null && other.proysigPK != null) || (this.proysigPK != null && !this.proysigPK.equals(other.proysigPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelEmia.Proysig[ proysigPK=" + proysigPK + " ]";
    }
    
}
