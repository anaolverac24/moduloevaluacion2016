/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.correspondencia;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_TIPODOCUMENTO", schema = "CORRESPONDENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatTipodocumento.findAll", query = "SELECT c FROM CatTipodocumento c"),
    @NamedQuery(name = "CatTipodocumento.findByIdtipodocumento", query = "SELECT c FROM CatTipodocumento c WHERE c.idtipodocumento = :idtipodocumento"),
    @NamedQuery(name = "CatTipodocumento.findByDescripcion", query = "SELECT c FROM CatTipodocumento c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CatTipodocumento.findByEstatus", query = "SELECT c FROM CatTipodocumento c WHERE c.estatus = :estatus"),
    @NamedQuery(name = "CatTipodocumento.findByIdentidadfederativa", query = "SELECT c FROM CatTipodocumento c WHERE c.identidadfederativa = :identidadfederativa"),
    @NamedQuery(name = "CatTipodocumento.findByIdarea", query = "SELECT c FROM CatTipodocumento c WHERE c.idarea = :idarea")})
public class CatTipodocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "IDTIPODOCUMENTO")
    private String idtipodocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Size(max = 2)
    @Column(name = "IDENTIDADFEDERATIVA")
    private String identidadfederativa;
    @Size(max = 12)
    @Column(name = "IDAREA")
    private String idarea;
    @OneToMany(mappedBy = "idtipodocumento")
    private Collection<CatCategoriadocumento> catCategoriadocumentoCollection;

    public CatTipodocumento() {
    }

    public CatTipodocumento(String idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    public CatTipodocumento(String idtipodocumento, String descripcion) {
        this.idtipodocumento = idtipodocumento;
        this.descripcion = descripcion;
    }

    public String getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(String idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public String getIdentidadfederativa() {
        return identidadfederativa;
    }

    public void setIdentidadfederativa(String identidadfederativa) {
        this.identidadfederativa = identidadfederativa;
    }

    public String getIdarea() {
        return idarea;
    }

    public void setIdarea(String idarea) {
        this.idarea = idarea;
    }

    @XmlTransient
    public Collection<CatCategoriadocumento> getCatCategoriadocumentoCollection() {
        return catCategoriadocumentoCollection;
    }

    public void setCatCategoriadocumentoCollection(Collection<CatCategoriadocumento> catCategoriadocumentoCollection) {
        this.catCategoriadocumentoCollection = catCategoriadocumentoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipodocumento != null ? idtipodocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipodocumento)) {
            return false;
        }
        CatTipodocumento other = (CatTipodocumento) object;
        if ((this.idtipodocumento == null && other.idtipodocumento != null) || (this.idtipodocumento != null && !this.idtipodocumento.equals(other.idtipodocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.correspondencia.CatTipodocumento[ idtipodocumento=" + idtipodocumento + " ]";
    }
    
}
