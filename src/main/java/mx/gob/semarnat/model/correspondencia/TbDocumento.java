/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.correspondencia;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "TB_DOCUMENTO", schema = "CORRESPONDENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDocumento.findAll", query = "SELECT t FROM TbDocumento t"),
    @NamedQuery(name = "TbDocumento.findByIddocumento", query = "SELECT t FROM TbDocumento t WHERE t.iddocumento = :iddocumento"),
    @NamedQuery(name = "TbDocumento.findByIdusuarioregistro", query = "SELECT t FROM TbDocumento t WHERE t.idusuarioregistro = :idusuarioregistro"),
    @NamedQuery(name = "TbDocumento.findByIdarea", query = "SELECT t FROM TbDocumento t WHERE t.idarea = :idarea"),
    @NamedQuery(name = "TbDocumento.findByFecharegistro", query = "SELECT t FROM TbDocumento t WHERE t.fecharegistro = :fecharegistro"),
    @NamedQuery(name = "TbDocumento.findByObservaciones", query = "SELECT t FROM TbDocumento t WHERE t.observaciones = :observaciones"),
    @NamedQuery(name = "TbDocumento.findByFechaacuse", query = "SELECT t FROM TbDocumento t WHERE t.fechaacuse = :fechaacuse"),
    @NamedQuery(name = "TbDocumento.findByNumreferencia", query = "SELECT t FROM TbDocumento t WHERE t.numreferencia = :numreferencia"),
    @NamedQuery(name = "TbDocumento.findByFechaelaboracion", query = "SELECT t FROM TbDocumento t WHERE t.fechaelaboracion = :fechaelaboracion"),
    @NamedQuery(name = "TbDocumento.findByFechalimite", query = "SELECT t FROM TbDocumento t WHERE t.fechalimite = :fechalimite"),
    @NamedQuery(name = "TbDocumento.findByIdusuarioresponsable", query = "SELECT t FROM TbDocumento t WHERE t.idusuarioresponsable = :idusuarioresponsable"),
    @NamedQuery(name = "TbDocumento.findByIdentidadfederativa", query = "SELECT t FROM TbDocumento t WHERE t.identidadfederativa = :identidadfederativa"),
    @NamedQuery(name = "TbDocumento.findByBorrador", query = "SELECT t FROM TbDocumento t WHERE t.borrador = :borrador"),
    @NamedQuery(name = "TbDocumento.findByIdcategoria", query = "SELECT t FROM TbDocumento t WHERE t.idcategoria = :idcategoria"),
    @NamedQuery(name = "TbDocumento.findByInstruccionadicional", query = "SELECT t FROM TbDocumento t WHERE t.instruccionadicional = :instruccionadicional"),
    @NamedQuery(name = "TbDocumento.findByNombreremitente", query = "SELECT t FROM TbDocumento t WHERE t.nombreremitente = :nombreremitente"),
    @NamedQuery(name = "TbDocumento.findByDependenciaremitente", query = "SELECT t FROM TbDocumento t WHERE t.dependenciaremitente = :dependenciaremitente"),
    @NamedQuery(name = "TbDocumento.findByTelefonoremitente", query = "SELECT t FROM TbDocumento t WHERE t.telefonoremitente = :telefonoremitente"),
    @NamedQuery(name = "TbDocumento.findByNombredirigidoa", query = "SELECT t FROM TbDocumento t WHERE t.nombredirigidoa = :nombredirigidoa"),
    @NamedQuery(name = "TbDocumento.findByAreadirigidoa", query = "SELECT t FROM TbDocumento t WHERE t.areadirigidoa = :areadirigidoa"),
    @NamedQuery(name = "TbDocumento.findByDocumentorespuesta", query = "SELECT t FROM TbDocumento t WHERE t.documentorespuesta = :documentorespuesta"),
    @NamedQuery(name = "TbDocumento.findByNumexpZofe", query = "SELECT t FROM TbDocumento t WHERE t.numexpZofe = :numexpZofe"),
    @NamedQuery(name = "TbDocumento.findByNumoficialiaZofe", query = "SELECT t FROM TbDocumento t WHERE t.numoficialiaZofe = :numoficialiaZofe"),
    @NamedQuery(name = "TbDocumento.findByIdcatcorresp", query = "SELECT t FROM TbDocumento t WHERE t.idcatcorresp = :idcatcorresp"),
    @NamedQuery(name = "TbDocumento.findByPuestoremitente", query = "SELECT t FROM TbDocumento t WHERE t.puestoremitente = :puestoremitente")})
public class TbDocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "IDDOCUMENTO")
    private String iddocumento;
    @Column(name = "IDUSUARIOREGISTRO")
    private BigInteger idusuarioregistro;
    @Size(max = 12)
    @Column(name = "IDAREA")
    private String idarea;
    @Column(name = "FECHAREGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecharegistro;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "FECHAACUSE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaacuse;
    @Size(max = 100)
    @Column(name = "NUMREFERENCIA")
    private String numreferencia;
    @Column(name = "FECHAELABORACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaelaboracion;
    @Column(name = "FECHALIMITE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechalimite;
    @Column(name = "IDUSUARIORESPONSABLE")
    private BigInteger idusuarioresponsable;
    @Size(max = 2)
    @Column(name = "IDENTIDADFEDERATIVA")
    private String identidadfederativa;
    @Column(name = "BORRADOR")
    private BigInteger borrador;
    @Column(name = "IDCATEGORIA")
    private BigInteger idcategoria;
    @Size(max = 400)
    @Column(name = "INSTRUCCIONADICIONAL")
    private String instruccionadicional;
    @Size(max = 300)
    @Column(name = "NOMBREREMITENTE")
    private String nombreremitente;
    @Size(max = 100)
    @Column(name = "DEPENDENCIAREMITENTE")
    private String dependenciaremitente;
    @Size(max = 50)
    @Column(name = "TELEFONOREMITENTE")
    private String telefonoremitente;
    @Size(max = 200)
    @Column(name = "NOMBREDIRIGIDOA")
    private String nombredirigidoa;
    @Size(max = 122)
    @Column(name = "AREADIRIGIDOA")
    private String areadirigidoa;
    @Column(name = "DOCUMENTORESPUESTA")
    private BigInteger documentorespuesta;
    @Size(max = 20)
    @Column(name = "NUMEXP_ZOFE")
    private String numexpZofe;
    @Size(max = 100)
    @Column(name = "NUMOFICIALIA_ZOFE")
    private String numoficialiaZofe;
    @Column(name = "IDCATCORRESP")
    private BigInteger idcatcorresp;
    @Size(max = 100)
    @Column(name = "PUESTOREMITENTE")
    private String puestoremitente;
    @JoinColumn(name = "IDESTADO", referencedColumnName = "IDESTADO")
    @ManyToOne
    private CatEstados idestado;
    @JoinColumn(name = "IDINSTRUCCION", referencedColumnName = "IDINSTRUCCION")
    @ManyToOne
    private CatInstruccion idinstruccion;
    @JoinColumn(name = "IDPRIORIDAD", referencedColumnName = "IDPRIORIDAD")
    @ManyToOne
    private CatPrioridad idprioridad;

    public TbDocumento() {
    }

    public TbDocumento(String iddocumento) {
        this.iddocumento = iddocumento;
    }

    public String getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(String iddocumento) {
        this.iddocumento = iddocumento;
    }

    public BigInteger getIdusuarioregistro() {
        return idusuarioregistro;
    }

    public void setIdusuarioregistro(BigInteger idusuarioregistro) {
        this.idusuarioregistro = idusuarioregistro;
    }

    public String getIdarea() {
        return idarea;
    }

    public void setIdarea(String idarea) {
        this.idarea = idarea;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaacuse() {
        return fechaacuse;
    }

    public void setFechaacuse(Date fechaacuse) {
        this.fechaacuse = fechaacuse;
    }

    public String getNumreferencia() {
        return numreferencia;
    }

    public void setNumreferencia(String numreferencia) {
        this.numreferencia = numreferencia;
    }

    public Date getFechaelaboracion() {
        return fechaelaboracion;
    }

    public void setFechaelaboracion(Date fechaelaboracion) {
        this.fechaelaboracion = fechaelaboracion;
    }

    public Date getFechalimite() {
        return fechalimite;
    }

    public void setFechalimite(Date fechalimite) {
        this.fechalimite = fechalimite;
    }

    public BigInteger getIdusuarioresponsable() {
        return idusuarioresponsable;
    }

    public void setIdusuarioresponsable(BigInteger idusuarioresponsable) {
        this.idusuarioresponsable = idusuarioresponsable;
    }

    public String getIdentidadfederativa() {
        return identidadfederativa;
    }

    public void setIdentidadfederativa(String identidadfederativa) {
        this.identidadfederativa = identidadfederativa;
    }

    public BigInteger getBorrador() {
        return borrador;
    }

    public void setBorrador(BigInteger borrador) {
        this.borrador = borrador;
    }

    public BigInteger getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(BigInteger idcategoria) {
        this.idcategoria = idcategoria;
    }

    public String getInstruccionadicional() {
        return instruccionadicional;
    }

    public void setInstruccionadicional(String instruccionadicional) {
        this.instruccionadicional = instruccionadicional;
    }

    public String getNombreremitente() {
        return nombreremitente;
    }

    public void setNombreremitente(String nombreremitente) {
        this.nombreremitente = nombreremitente;
    }

    public String getDependenciaremitente() {
        return dependenciaremitente;
    }

    public void setDependenciaremitente(String dependenciaremitente) {
        this.dependenciaremitente = dependenciaremitente;
    }

    public String getTelefonoremitente() {
        return telefonoremitente;
    }

    public void setTelefonoremitente(String telefonoremitente) {
        this.telefonoremitente = telefonoremitente;
    }

    public String getNombredirigidoa() {
        return nombredirigidoa;
    }

    public void setNombredirigidoa(String nombredirigidoa) {
        this.nombredirigidoa = nombredirigidoa;
    }

    public String getAreadirigidoa() {
        return areadirigidoa;
    }

    public void setAreadirigidoa(String areadirigidoa) {
        this.areadirigidoa = areadirigidoa;
    }

    public BigInteger getDocumentorespuesta() {
        return documentorespuesta;
    }

    public void setDocumentorespuesta(BigInteger documentorespuesta) {
        this.documentorespuesta = documentorespuesta;
    }

    public String getNumexpZofe() {
        return numexpZofe;
    }

    public void setNumexpZofe(String numexpZofe) {
        this.numexpZofe = numexpZofe;
    }

    public String getNumoficialiaZofe() {
        return numoficialiaZofe;
    }

    public void setNumoficialiaZofe(String numoficialiaZofe) {
        this.numoficialiaZofe = numoficialiaZofe;
    }

    public BigInteger getIdcatcorresp() {
        return idcatcorresp;
    }

    public void setIdcatcorresp(BigInteger idcatcorresp) {
        this.idcatcorresp = idcatcorresp;
    }

    public String getPuestoremitente() {
        return puestoremitente;
    }

    public void setPuestoremitente(String puestoremitente) {
        this.puestoremitente = puestoremitente;
    }

    public CatEstados getIdestado() {
        return idestado;
    }

    public void setIdestado(CatEstados idestado) {
        this.idestado = idestado;
    }

    public CatInstruccion getIdinstruccion() {
        return idinstruccion;
    }

    public void setIdinstruccion(CatInstruccion idinstruccion) {
        this.idinstruccion = idinstruccion;
    }

    public CatPrioridad getIdprioridad() {
        return idprioridad;
    }

    public void setIdprioridad(CatPrioridad idprioridad) {
        this.idprioridad = idprioridad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddocumento != null ? iddocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDocumento)) {
            return false;
        }
        TbDocumento other = (TbDocumento) object;
        if ((this.iddocumento == null && other.iddocumento != null) || (this.iddocumento != null && !this.iddocumento.equals(other.iddocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.correspondencia.TbDocumento[ iddocumento=" + iddocumento + " ]";
    }
    
}
