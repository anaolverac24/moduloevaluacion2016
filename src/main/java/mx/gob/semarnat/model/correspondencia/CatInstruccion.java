/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.correspondencia;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_INSTRUCCION", schema = "CORRESPONDENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatInstruccion.findAll", query = "SELECT c FROM CatInstruccion c"),
    @NamedQuery(name = "CatInstruccion.findByIdinstruccion", query = "SELECT c FROM CatInstruccion c WHERE c.idinstruccion = :idinstruccion"),
    @NamedQuery(name = "CatInstruccion.findByDescripcion", query = "SELECT c FROM CatInstruccion c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CatInstruccion.findByEstatus", query = "SELECT c FROM CatInstruccion c WHERE c.estatus = :estatus"),
    @NamedQuery(name = "CatInstruccion.findByIdarea", query = "SELECT c FROM CatInstruccion c WHERE c.idarea = :idarea"),
    @NamedQuery(name = "CatInstruccion.findByIdentidadfederativa", query = "SELECT c FROM CatInstruccion c WHERE c.identidadfederativa = :identidadfederativa")})
public class CatInstruccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "IDINSTRUCCION")
    private String idinstruccion;
    @Size(max = 100)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Size(max = 12)
    @Column(name = "IDAREA")
    private String idarea;
    @Size(max = 2)
    @Column(name = "IDENTIDADFEDERATIVA")
    private String identidadfederativa;
    @OneToMany(mappedBy = "idinstruccion")
    private Collection<TbDocumento> tbDocumentoCollection;

    public CatInstruccion() {
    }

    public CatInstruccion(String idinstruccion) {
        this.idinstruccion = idinstruccion;
    }

    public String getIdinstruccion() {
        return idinstruccion;
    }

    public void setIdinstruccion(String idinstruccion) {
        this.idinstruccion = idinstruccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public String getIdarea() {
        return idarea;
    }

    public void setIdarea(String idarea) {
        this.idarea = idarea;
    }

    public String getIdentidadfederativa() {
        return identidadfederativa;
    }

    public void setIdentidadfederativa(String identidadfederativa) {
        this.identidadfederativa = identidadfederativa;
    }

    @XmlTransient
    public Collection<TbDocumento> getTbDocumentoCollection() {
        return tbDocumentoCollection;
    }

    public void setTbDocumentoCollection(Collection<TbDocumento> tbDocumentoCollection) {
        this.tbDocumentoCollection = tbDocumentoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinstruccion != null ? idinstruccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatInstruccion)) {
            return false;
        }
        CatInstruccion other = (CatInstruccion) object;
        if ((this.idinstruccion == null && other.idinstruccion != null) || (this.idinstruccion != null && !this.idinstruccion.equals(other.idinstruccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.correspondencia.CatInstruccion[ idinstruccion=" + idinstruccion + " ]";
    }
    
}
