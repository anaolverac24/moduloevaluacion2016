/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.correspondencia;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "VTBRELDOCTRAMITE", schema = "DGIRA_MIAE2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vtbreldoctramite.findAll", query = "SELECT v FROM Vtbreldoctramite v"),
    @NamedQuery(name = "Vtbreldoctramite.findByBitacoraproyecto", query = "SELECT v FROM Vtbreldoctramite v WHERE v.bitacoraproyecto = :bitacoraproyecto"),
    @NamedQuery(name = "Vtbreldoctramite.findByIddocumento", query = "SELECT v FROM Vtbreldoctramite v WHERE v.iddocumento = :iddocumento"),
    @NamedQuery(name = "Vtbreldoctramite.findByIdtiporelacion", query = "SELECT v FROM Vtbreldoctramite v WHERE v.idtiporelacion = :idtiporelacion"),
    @NamedQuery(name = "Vtbreldoctramite.findByIdentidadfederativa", query = "SELECT v FROM Vtbreldoctramite v WHERE v.identidadfederativa = :identidadfederativa"),
    @NamedQuery(name = "Vtbreldoctramite.findByIdusuariorelaciono", query = "SELECT v FROM Vtbreldoctramite v WHERE v.idusuariorelaciono = :idusuariorelaciono"),
    @NamedQuery(name = "Vtbreldoctramite.findByFecharelaciono", query = "SELECT v FROM Vtbreldoctramite v WHERE v.fecharelaciono = :fecharelaciono")})
public class Vtbreldoctramite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 18)
    @Column(name = "BITACORAPROYECTO")
    private String bitacoraproyecto;
    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 31)
    @Column(name = "IDDOCUMENTO")
    private String iddocumento;
//    @Size(max = 2)
    @Column(name = "IDTIPORELACION")
    private String idtiporelacion;
//    @Size(max = 2)
    @Column(name = "IDENTIDADFEDERATIVA")
    private String identidadfederativa;
    @Column(name = "IDUSUARIORELACIONO")
    private BigInteger idusuariorelaciono;
    @Column(name = "FECHARELACIONO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecharelaciono;

    public Vtbreldoctramite() {
    }

    public String getBitacoraproyecto() {
        return bitacoraproyecto;
    }

    public void setBitacoraproyecto(String bitacoraproyecto) {
        this.bitacoraproyecto = bitacoraproyecto;
    }

    public String getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(String iddocumento) {
        this.iddocumento = iddocumento;
    }

    public String getIdtiporelacion() {
        return idtiporelacion;
    }

    public void setIdtiporelacion(String idtiporelacion) {
        this.idtiporelacion = idtiporelacion;
    }

    public String getIdentidadfederativa() {
        return identidadfederativa;
    }

    public void setIdentidadfederativa(String identidadfederativa) {
        this.identidadfederativa = identidadfederativa;
    }

    public BigInteger getIdusuariorelaciono() {
        return idusuariorelaciono;
    }

    public void setIdusuariorelaciono(BigInteger idusuariorelaciono) {
        this.idusuariorelaciono = idusuariorelaciono;
    }

    public Date getFecharelaciono() {
        return fecharelaciono;
    }

    public void setFecharelaciono(Date fecharelaciono) {
        this.fecharelaciono = fecharelaciono;
    }
    
}
