/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.correspondencia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_CATEGORIADOCUMENTO", schema = "CORRESPONDENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatCategoriadocumento.findAll", query = "SELECT c FROM CatCategoriadocumento c"),
    @NamedQuery(name = "CatCategoriadocumento.findByIdcategoria", query = "SELECT c FROM CatCategoriadocumento c WHERE c.idcategoria = :idcategoria"),
    @NamedQuery(name = "CatCategoriadocumento.findByDescripcion", query = "SELECT c FROM CatCategoriadocumento c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CatCategoriadocumento.findByEstatus", query = "SELECT c FROM CatCategoriadocumento c WHERE c.estatus = :estatus")})
public class CatCategoriadocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCATEGORIA")
    private BigDecimal idcategoria;
    @Size(max = 120)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @JoinColumn(name = "IDTIPODOCUMENTO", referencedColumnName = "IDTIPODOCUMENTO")
    @ManyToOne
    private CatTipodocumento idtipodocumento;

    public CatCategoriadocumento() {
    }

    public CatCategoriadocumento(BigDecimal idcategoria) {
        this.idcategoria = idcategoria;
    }

    public BigDecimal getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(BigDecimal idcategoria) {
        this.idcategoria = idcategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public CatTipodocumento getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(CatTipodocumento idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategoria != null ? idcategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatCategoriadocumento)) {
            return false;
        }
        CatCategoriadocumento other = (CatCategoriadocumento) object;
        if ((this.idcategoria == null && other.idcategoria != null) || (this.idcategoria != null && !this.idcategoria.equals(other.idcategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.correspondencia.CatCategoriadocumento[ idcategoria=" + idcategoria + " ]";
    }
    
}
