/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.correspondencia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_PRIORIDAD", schema = "CORRESPONDENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPrioridad.findAll", query = "SELECT c FROM CatPrioridad c"),
    @NamedQuery(name = "CatPrioridad.findByIdprioridad", query = "SELECT c FROM CatPrioridad c WHERE c.idprioridad = :idprioridad"),
    @NamedQuery(name = "CatPrioridad.findByDescripcion", query = "SELECT c FROM CatPrioridad c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CatPrioridad.findByEstatus", query = "SELECT c FROM CatPrioridad c WHERE c.estatus = :estatus")})
public class CatPrioridad implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPRIORIDAD")
    private BigDecimal idprioridad;
    @Size(max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @OneToMany(mappedBy = "idprioridad")
    private Collection<TbDocumento> tbDocumentoCollection;

    public CatPrioridad() {
    }

    public CatPrioridad(BigDecimal idprioridad) {
        this.idprioridad = idprioridad;
    }

    public BigDecimal getIdprioridad() {
        return idprioridad;
    }

    public void setIdprioridad(BigDecimal idprioridad) {
        this.idprioridad = idprioridad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    @XmlTransient
    public Collection<TbDocumento> getTbDocumentoCollection() {
        return tbDocumentoCollection;
    }

    public void setTbDocumentoCollection(Collection<TbDocumento> tbDocumentoCollection) {
        this.tbDocumentoCollection = tbDocumentoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idprioridad != null ? idprioridad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPrioridad)) {
            return false;
        }
        CatPrioridad other = (CatPrioridad) object;
        if ((this.idprioridad == null && other.idprioridad != null) || (this.idprioridad != null && !this.idprioridad.equals(other.idprioridad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.correspondencia.CatPrioridad[ idprioridad=" + idprioridad + " ]";
    }
    
}
