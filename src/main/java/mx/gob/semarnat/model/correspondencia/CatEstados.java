/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.model.correspondencia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rodrigo
 */
@Entity
@Table(name = "CAT_ESTADOS", schema = "CORRESPONDENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEstados.findAll", query = "SELECT c FROM CatEstados c"),
    @NamedQuery(name = "CatEstados.findByIdestado", query = "SELECT c FROM CatEstados c WHERE c.idestado = :idestado"),
    @NamedQuery(name = "CatEstados.findByDescripcion", query = "SELECT c FROM CatEstados c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CatEstados.findByEstatus", query = "SELECT c FROM CatEstados c WHERE c.estatus = :estatus")})
public class CatEstados implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDESTADO")
    private BigDecimal idestado;
    @Size(max = 20)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @OneToMany(mappedBy = "idestado")
    private Collection<TbDocumento> tbDocumentoCollection;

    public CatEstados() {
    }

    public CatEstados(BigDecimal idestado) {
        this.idestado = idestado;
    }

    public BigDecimal getIdestado() {
        return idestado;
    }

    public void setIdestado(BigDecimal idestado) {
        this.idestado = idestado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    @XmlTransient
    public Collection<TbDocumento> getTbDocumentoCollection() {
        return tbDocumentoCollection;
    }

    public void setTbDocumentoCollection(Collection<TbDocumento> tbDocumentoCollection) {
        this.tbDocumentoCollection = tbDocumentoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestado != null ? idestado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEstados)) {
            return false;
        }
        CatEstados other = (CatEstados) object;
        if ((this.idestado == null && other.idestado != null) || (this.idestado != null && !this.idestado.equals(other.idestado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.correspondencia.CatEstados[ idestado=" + idestado + " ]";
    }
    
}
