/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogo_tramites;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chely
 */
@Entity
@Table(name = "CAT_REQUISITOS",schema="CATALOGO_TRAMITES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatRequisitos.findAll", query = "SELECT c FROM CatRequisitos c"),
    @NamedQuery(name = "CatRequisitos.findByIdRequisito", query = "SELECT c FROM CatRequisitos c WHERE c.idRequisito = :idRequisito"),
    @NamedQuery(name = "CatRequisitos.findByNombreRequisito", query = "SELECT c FROM CatRequisitos c WHERE c.nombreRequisito = :nombreRequisito"),
    @NamedQuery(name = "CatRequisitos.findByDescripcionRequisito", query = "SELECT c FROM CatRequisitos c WHERE c.descripcionRequisito = :descripcionRequisito"),
    @NamedQuery(name = "CatRequisitos.findByTipoRequisito", query = "SELECT c FROM CatRequisitos c WHERE c.tipoRequisito = :tipoRequisito"),
    @NamedQuery(name = "CatRequisitos.findByEstatus", query = "SELECT c FROM CatRequisitos c WHERE c.estatus = :estatus")})
public class CatRequisitos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_REQUISITO")
    private Integer idRequisito;
    @Basic(optional = false)
    @Column(name = "NOMBRE_REQUISITO")
    private String nombreRequisito;
    @Column(name = "DESCRIPCION_REQUISITO")
    private String descripcionRequisito;
    @Column(name = "TIPO_REQUISITO")
    private Character tipoRequisito;
    @Column(name = "ESTATUS")
    private BigInteger estatus;

    public CatRequisitos() {
    }

    public CatRequisitos(Integer idRequisito) {
        this.idRequisito = idRequisito;
    }

    public CatRequisitos(Integer idRequisito, String nombreRequisito) {
        this.idRequisito = idRequisito;
        this.nombreRequisito = nombreRequisito;
    }

    public Integer getIdRequisito() {
        return idRequisito;
    }

    public void setIdRequisito(Integer idRequisito) {
        this.idRequisito = idRequisito;
    }

    public String getNombreRequisito() {
        return nombreRequisito;
    }

    public void setNombreRequisito(String nombreRequisito) {
        this.nombreRequisito = nombreRequisito;
    }

    public String getDescripcionRequisito() {
        return descripcionRequisito;
    }

    public void setDescripcionRequisito(String descripcionRequisito) {
        this.descripcionRequisito = descripcionRequisito;
    }

    public Character getTipoRequisito() {
        return tipoRequisito;
    }

    public void setTipoRequisito(Character tipoRequisito) {
        this.tipoRequisito = tipoRequisito;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRequisito != null ? idRequisito.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatRequisitos)) {
            return false;
        }
        CatRequisitos other = (CatRequisitos) object;
        if ((this.idRequisito == null && other.idRequisito != null) || (this.idRequisito != null && !this.idRequisito.equals(other.idRequisito))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.model.CatRequisitos[ idRequisito=" + idRequisito + " ]";
    }
    
}
