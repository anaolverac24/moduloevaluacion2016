/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogo_tramites;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "MODALIDAD", schema="CATALOGO_TRAMITES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modalidad.findAll", query = "SELECT m FROM Modalidad m"),
    @NamedQuery(name = "Modalidad.findByClaveTramite", query = "SELECT m FROM Modalidad m WHERE m.modalidadPK.claveTramite = :claveTramite"),
    @NamedQuery(name = "Modalidad.findByIdTramite", query = "SELECT m FROM Modalidad m WHERE m.modalidadPK.idTramite = :idTramite"),
    @NamedQuery(name = "Modalidad.findByAfirmativaNegativa", query = "SELECT m FROM Modalidad m WHERE m.afirmativaNegativa = :afirmativaNegativa"),
    @NamedQuery(name = "Modalidad.findByRequiereRespuesta", query = "SELECT m FROM Modalidad m WHERE m.requiereRespuesta = :requiereRespuesta"),
    @NamedQuery(name = "Modalidad.findByRespuestaInmediata", query = "SELECT m FROM Modalidad m WHERE m.respuestaInmediata = :respuestaInmediata"),
    @NamedQuery(name = "Modalidad.findByNombreCorto", query = "SELECT m FROM Modalidad m WHERE m.nombreCorto = :nombreCorto"),
    @NamedQuery(name = "Modalidad.findByTElectronico", query = "SELECT m FROM Modalidad m WHERE m.tElectronico = :tElectronico"),
    @NamedQuery(name = "Modalidad.findByRequiereNra", query = "SELECT m FROM Modalidad m WHERE m.requiereNra = :requiereNra"),
    @NamedQuery(name = "Modalidad.findByRequiereCurp", query = "SELECT m FROM Modalidad m WHERE m.requiereCurp = :requiereCurp"),
    @NamedQuery(name = "Modalidad.findByDescripcionAcuerdo", query = "SELECT m FROM Modalidad m WHERE m.descripcionAcuerdo = :descripcionAcuerdo"),
    @NamedQuery(name = "Modalidad.findByFechaAcuerdo", query = "SELECT m FROM Modalidad m WHERE m.fechaAcuerdo = :fechaAcuerdo"),
    @NamedQuery(name = "Modalidad.findByFechaPresentacionInicio", query = "SELECT m FROM Modalidad m WHERE m.fechaPresentacionInicio = :fechaPresentacionInicio"),
    @NamedQuery(name = "Modalidad.findByFechaPresentacionFin", query = "SELECT m FROM Modalidad m WHERE m.fechaPresentacionFin = :fechaPresentacionFin"),
    @NamedQuery(name = "Modalidad.findByFechaalta", query = "SELECT m FROM Modalidad m WHERE m.fechaalta = :fechaalta"),
    @NamedQuery(name = "Modalidad.findByFechabaja", query = "SELECT m FROM Modalidad m WHERE m.fechabaja = :fechabaja"),
    @NamedQuery(name = "Modalidad.findByVigencia", query = "SELECT m FROM Modalidad m WHERE m.vigencia = :vigencia"),
    @NamedQuery(name = "Modalidad.findByEstatus", query = "SELECT m FROM Modalidad m WHERE m.estatus = :estatus"),
    @NamedQuery(name = "Modalidad.findByRequiereSat", query = "SELECT m FROM Modalidad m WHERE m.requiereSat = :requiereSat"),
    @NamedQuery(name = "Modalidad.findByRequiereIfai", query = "SELECT m FROM Modalidad m WHERE m.requiereIfai = :requiereIfai"),
    @NamedQuery(name = "Modalidad.findByDescentralizado", query = "SELECT m FROM Modalidad m WHERE m.descentralizado = :descentralizado"),
    @NamedQuery(name = "Modalidad.findByDesconcentrado", query = "SELECT m FROM Modalidad m WHERE m.desconcentrado = :desconcentrado"),
    @NamedQuery(name = "Modalidad.findByFechaInicio", query = "SELECT m FROM Modalidad m WHERE m.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Modalidad.findByFechaFin", query = "SELECT m FROM Modalidad m WHERE m.fechaFin = :fechaFin"),
    @NamedQuery(name = "Modalidad.findByDireccionAdicional", query = "SELECT m FROM Modalidad m WHERE m.direccionAdicional = :direccionAdicional"),
    @NamedQuery(name = "Modalidad.findByIdsubtema", query = "SELECT m FROM Modalidad m WHERE m.idsubtema = :idsubtema"),
    @NamedQuery(name = "Modalidad.findByIdiniciovigencia", query = "SELECT m FROM Modalidad m WHERE m.idiniciovigencia = :idiniciovigencia"),
    @NamedQuery(name = "Modalidad.findByRequiereNotificacion", query = "SELECT m FROM Modalidad m WHERE m.requiereNotificacion = :requiereNotificacion"),
    @NamedQuery(name = "Modalidad.findByRequierePago", query = "SELECT m FROM Modalidad m WHERE m.requierePago = :requierePago"),
    @NamedQuery(name = "Modalidad.findByNumeroDiasElec", query = "SELECT m FROM Modalidad m WHERE m.numeroDiasElec = :numeroDiasElec"),
    @NamedQuery(name = "Modalidad.findByLigaElectronica", query = "SELECT m FROM Modalidad m WHERE m.ligaElectronica = :ligaElectronica"),
    @NamedQuery(name = "Modalidad.findByTipoformatofecha", query = "SELECT m FROM Modalidad m WHERE m.tipoformatofecha = :tipoformatofecha"),
    @NamedQuery(name = "Modalidad.findByDatosProyectos", query = "SELECT m FROM Modalidad m WHERE m.datosProyectos = :datosProyectos"),
    @NamedQuery(name = "Modalidad.findByUrlroot", query = "SELECT m FROM Modalidad m WHERE m.urlroot = :urlroot"),
    @NamedQuery(name = "Modalidad.findByWs", query = "SELECT m FROM Modalidad m WHERE m.ws = :ws"),
    @NamedQuery(name = "Modalidad.findByTramite", query = "SELECT m FROM Modalidad m WHERE m.tramite = :tramite")})
public class Modalidad implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ModalidadPK modalidadPK;
    @Column(name = "AFIRMATIVA_NEGATIVA")
    private String afirmativaNegativa;
    @Column(name = "REQUIERE_RESPUESTA")
    private Character requiereRespuesta;
    @Column(name = "RESPUESTA_INMEDIATA")
    private Character respuestaInmediata;
    @Basic(optional = false)
    @Column(name = "NOMBRE_CORTO")
    private String nombreCorto;
    @Column(name = "T_ELECTRONICO")
    private Character tElectronico;
    @Basic(optional = false)
    @Column(name = "REQUIERE_NRA")
    private Character requiereNra;
    @Basic(optional = false)
    @Column(name = "REQUIERE_CURP")
    private Character requiereCurp;
    @Column(name = "DESCRIPCION_ACUERDO")
    private String descripcionAcuerdo;
    @Column(name = "FECHA_ACUERDO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAcuerdo;
    @Column(name = "FECHA_PRESENTACION_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPresentacionInicio;
    @Column(name = "FECHA_PRESENTACION_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPresentacionFin;
    @Basic(optional = false)
    @Column(name = "FECHAALTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaalta;
    @Column(name = "FECHABAJA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechabaja;
    @Column(name = "VIGENCIA")
    private String vigencia;
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Column(name = "REQUIERE_SAT")
    private BigInteger requiereSat;
    @Column(name = "REQUIERE_IFAI")
    private BigInteger requiereIfai;
    @Column(name = "DESCENTRALIZADO")
    private BigInteger descentralizado;
    @Column(name = "DESCONCENTRADO")
    private BigInteger desconcentrado;
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Column(name = "DIRECCION_ADICIONAL")
    private BigInteger direccionAdicional;
    @Column(name = "IDSUBTEMA")
    private BigInteger idsubtema;
    @Column(name = "IDINICIOVIGENCIA")
    private BigInteger idiniciovigencia;
    @Column(name = "REQUIERE_NOTIFICACION")
    private BigInteger requiereNotificacion;
    @Column(name = "REQUIERE_PAGO")
    private Short requierePago;
    @Column(name = "NUMERO_DIAS_ELEC")
    private Short numeroDiasElec;
    @Column(name = "LIGA_ELECTRONICA")
    private String ligaElectronica;
    @Column(name = "TIPOFORMATOFECHA")
    private Character tipoformatofecha;
    @Column(name = "DATOS_PROYECTOS")
    private Short datosProyectos;
    @Column(name = "URLROOT")
    private String urlroot;
    @Column(name = "WS")
    private String ws;
    @Column(name = "TRAMITE")
    private String tramite;
    @JoinColumn(name = "ID_CLASIFICACION", referencedColumnName = "ID_CLASIFICACION")
    @ManyToOne
    private Clasificacion idClasificacion;
    @JoinColumn(name = "CLAVE_TRAMITE", referencedColumnName = "CLAVE_TRAMITE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ClaveTramites claveTramites;
    @JoinColumn(name = "ID_TRAMITE", referencedColumnName = "ID_TRAMITE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Tramite tramite1;

    public Modalidad() {
    }

    public Modalidad(ModalidadPK modalidadPK) {
        this.modalidadPK = modalidadPK;
    }

    public Modalidad(ModalidadPK modalidadPK, String nombreCorto, Character requiereNra, Character requiereCurp, Date fechaalta) {
        this.modalidadPK = modalidadPK;
        this.nombreCorto = nombreCorto;
        this.requiereNra = requiereNra;
        this.requiereCurp = requiereCurp;
        this.fechaalta = fechaalta;
    }

    public Modalidad(String claveTramite, int idTramite) {
        this.modalidadPK = new ModalidadPK(claveTramite, idTramite);
    }

    public ModalidadPK getModalidadPK() {
        return modalidadPK;
    }

    public void setModalidadPK(ModalidadPK modalidadPK) {
        this.modalidadPK = modalidadPK;
    }

    public String getAfirmativaNegativa() {
        return afirmativaNegativa;
    }

    public void setAfirmativaNegativa(String afirmativaNegativa) {
        this.afirmativaNegativa = afirmativaNegativa;
    }

    public Character getRequiereRespuesta() {
        return requiereRespuesta;
    }

    public void setRequiereRespuesta(Character requiereRespuesta) {
        this.requiereRespuesta = requiereRespuesta;
    }

    public Character getRespuestaInmediata() {
        return respuestaInmediata;
    }

    public void setRespuestaInmediata(Character respuestaInmediata) {
        this.respuestaInmediata = respuestaInmediata;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public Character getTElectronico() {
        return tElectronico;
    }

    public void setTElectronico(Character tElectronico) {
        this.tElectronico = tElectronico;
    }

    public Character getRequiereNra() {
        return requiereNra;
    }

    public void setRequiereNra(Character requiereNra) {
        this.requiereNra = requiereNra;
    }

    public Character getRequiereCurp() {
        return requiereCurp;
    }

    public void setRequiereCurp(Character requiereCurp) {
        this.requiereCurp = requiereCurp;
    }

    public String getDescripcionAcuerdo() {
        return descripcionAcuerdo;
    }

    public void setDescripcionAcuerdo(String descripcionAcuerdo) {
        this.descripcionAcuerdo = descripcionAcuerdo;
    }

    public Date getFechaAcuerdo() {
        return fechaAcuerdo;
    }

    public void setFechaAcuerdo(Date fechaAcuerdo) {
        this.fechaAcuerdo = fechaAcuerdo;
    }

    public Date getFechaPresentacionInicio() {
        return fechaPresentacionInicio;
    }

    public void setFechaPresentacionInicio(Date fechaPresentacionInicio) {
        this.fechaPresentacionInicio = fechaPresentacionInicio;
    }

    public Date getFechaPresentacionFin() {
        return fechaPresentacionFin;
    }

    public void setFechaPresentacionFin(Date fechaPresentacionFin) {
        this.fechaPresentacionFin = fechaPresentacionFin;
    }

    public Date getFechaalta() {
        return fechaalta;
    }

    public void setFechaalta(Date fechaalta) {
        this.fechaalta = fechaalta;
    }

    public Date getFechabaja() {
        return fechabaja;
    }

    public void setFechabaja(Date fechabaja) {
        this.fechabaja = fechabaja;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public BigInteger getRequiereSat() {
        return requiereSat;
    }

    public void setRequiereSat(BigInteger requiereSat) {
        this.requiereSat = requiereSat;
    }

    public BigInteger getRequiereIfai() {
        return requiereIfai;
    }

    public void setRequiereIfai(BigInteger requiereIfai) {
        this.requiereIfai = requiereIfai;
    }

    public BigInteger getDescentralizado() {
        return descentralizado;
    }

    public void setDescentralizado(BigInteger descentralizado) {
        this.descentralizado = descentralizado;
    }

    public BigInteger getDesconcentrado() {
        return desconcentrado;
    }

    public void setDesconcentrado(BigInteger desconcentrado) {
        this.desconcentrado = desconcentrado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public BigInteger getDireccionAdicional() {
        return direccionAdicional;
    }

    public void setDireccionAdicional(BigInteger direccionAdicional) {
        this.direccionAdicional = direccionAdicional;
    }

    public BigInteger getIdsubtema() {
        return idsubtema;
    }

    public void setIdsubtema(BigInteger idsubtema) {
        this.idsubtema = idsubtema;
    }

    public BigInteger getIdiniciovigencia() {
        return idiniciovigencia;
    }

    public void setIdiniciovigencia(BigInteger idiniciovigencia) {
        this.idiniciovigencia = idiniciovigencia;
    }

    public BigInteger getRequiereNotificacion() {
        return requiereNotificacion;
    }

    public void setRequiereNotificacion(BigInteger requiereNotificacion) {
        this.requiereNotificacion = requiereNotificacion;
    }

    public Short getRequierePago() {
        return requierePago;
    }

    public void setRequierePago(Short requierePago) {
        this.requierePago = requierePago;
    }

    public Short getNumeroDiasElec() {
        return numeroDiasElec;
    }

    public void setNumeroDiasElec(Short numeroDiasElec) {
        this.numeroDiasElec = numeroDiasElec;
    }

    public String getLigaElectronica() {
        return ligaElectronica;
    }

    public void setLigaElectronica(String ligaElectronica) {
        this.ligaElectronica = ligaElectronica;
    }

    public Character getTipoformatofecha() {
        return tipoformatofecha;
    }

    public void setTipoformatofecha(Character tipoformatofecha) {
        this.tipoformatofecha = tipoformatofecha;
    }

    public Short getDatosProyectos() {
        return datosProyectos;
    }

    public void setDatosProyectos(Short datosProyectos) {
        this.datosProyectos = datosProyectos;
    }

    public String getUrlroot() {
        return urlroot;
    }

    public void setUrlroot(String urlroot) {
        this.urlroot = urlroot;
    }

    public String getWs() {
        return ws;
    }

    public void setWs(String ws) {
        this.ws = ws;
    }

    public String getTramite() {
        return tramite;
    }

    public void setTramite(String tramite) {
        this.tramite = tramite;
    }

    public Clasificacion getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(Clasificacion idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public ClaveTramites getClaveTramites() {
        return claveTramites;
    }

    public void setClaveTramites(ClaveTramites claveTramites) {
        this.claveTramites = claveTramites;
    }

    public Tramite getTramite1() {
        return tramite1;
    }

    public void setTramite1(Tramite tramite1) {
        this.tramite1 = tramite1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modalidadPK != null ? modalidadPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modalidad)) {
            return false;
        }
        Modalidad other = (Modalidad) object;
        if ((this.modalidadPK == null && other.modalidadPK != null) || (this.modalidadPK != null && !this.modalidadPK.equals(other.modalidadPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Modalidad[ modalidadPK=" + modalidadPK + " ]";
    }
    
}
