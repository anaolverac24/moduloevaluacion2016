/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogo_tramites;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Paty
 */
@Embeddable
public class ModalidadPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "CLAVE_TRAMITE")
    private String claveTramite;
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE")
    private int idTramite;

    public ModalidadPK() {
    }

    public ModalidadPK(String claveTramite, int idTramite) {
        this.claveTramite = claveTramite;
        this.idTramite = idTramite;
    }

    public String getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(String claveTramite) {
        this.claveTramite = claveTramite;
    }

    public int getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(int idTramite) {
        this.idTramite = idTramite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claveTramite != null ? claveTramite.hashCode() : 0);
        hash += (int) idTramite;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModalidadPK)) {
            return false;
        }
        ModalidadPK other = (ModalidadPK) object;
        if ((this.claveTramite == null && other.claveTramite != null) || (this.claveTramite != null && !this.claveTramite.equals(other.claveTramite))) {
            return false;
        }
        if (this.idTramite != other.idTramite) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.ModalidadPK[ claveTramite=" + claveTramite + ", idTramite=" + idTramite + " ]";
    }
    
}
