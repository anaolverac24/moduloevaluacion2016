/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogo_tramites;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "CLAVE_TRAMITES", schema="CATALOGO_TRAMITES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaveTramites.findAll", query = "SELECT c FROM ClaveTramites c"),
    @NamedQuery(name = "ClaveTramites.findByClaveTramite", query = "SELECT c FROM ClaveTramites c WHERE c.claveTramite = :claveTramite"),
    @NamedQuery(name = "ClaveTramites.findByOcupado", query = "SELECT c FROM ClaveTramites c WHERE c.ocupado = :ocupado"),
    @NamedQuery(name = "ClaveTramites.findByConsec", query = "SELECT c FROM ClaveTramites c WHERE c.consec = :consec")})
public class ClaveTramites implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CLAVE_TRAMITE")
    private String claveTramite;
    @Column(name = "OCUPADO")
    private Character ocupado;
    @Column(name = "CONSEC")
    private BigInteger consec;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "claveTramites")
    private Collection<Modalidad> modalidadCollection;

    public ClaveTramites() {
    }

    public ClaveTramites(String claveTramite) {
        this.claveTramite = claveTramite;
    }

    public String getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(String claveTramite) {
        this.claveTramite = claveTramite;
    }

    public Character getOcupado() {
        return ocupado;
    }

    public void setOcupado(Character ocupado) {
        this.ocupado = ocupado;
    }

    public BigInteger getConsec() {
        return consec;
    }

    public void setConsec(BigInteger consec) {
        this.consec = consec;
    }

    @XmlTransient
    public Collection<Modalidad> getModalidadCollection() {
        return modalidadCollection;
    }

    public void setModalidadCollection(Collection<Modalidad> modalidadCollection) {
        this.modalidadCollection = modalidadCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claveTramite != null ? claveTramite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaveTramites)) {
            return false;
        }
        ClaveTramites other = (ClaveTramites) object;
        if ((this.claveTramite == null && other.claveTramite != null) || (this.claveTramite != null && !this.claveTramite.equals(other.claveTramite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.ClaveTramites[ claveTramite=" + claveTramite + " ]";
    }
    
}
