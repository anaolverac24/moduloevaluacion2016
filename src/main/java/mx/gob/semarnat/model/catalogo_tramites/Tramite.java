/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.model.catalogo_tramites;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "TRAMITE", schema="CATALOGO_TRAMITES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tramite.findAll", query = "SELECT t FROM Tramite t"),
    @NamedQuery(name = "Tramite.findByIdTramite", query = "SELECT t FROM Tramite t WHERE t.idTramite = :idTramite"),
    @NamedQuery(name = "Tramite.findByIdArea", query = "SELECT t FROM Tramite t WHERE t.idArea = :idArea"),
    @NamedQuery(name = "Tramite.findByIdCofemer", query = "SELECT t FROM Tramite t WHERE t.idCofemer = :idCofemer"),
    @NamedQuery(name = "Tramite.findByCofemerClave", query = "SELECT t FROM Tramite t WHERE t.cofemerClave = :cofemerClave"),
    @NamedQuery(name = "Tramite.findByLigaInternet", query = "SELECT t FROM Tramite t WHERE t.ligaInternet = :ligaInternet"),
    @NamedQuery(name = "Tramite.findByColorTramite", query = "SELECT t FROM Tramite t WHERE t.colorTramite = :colorTramite"),
    @NamedQuery(name = "Tramite.findByDescripcion", query = "SELECT t FROM Tramite t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "Tramite.findByFechaAcuerdo", query = "SELECT t FROM Tramite t WHERE t.fechaAcuerdo = :fechaAcuerdo"),
    @NamedQuery(name = "Tramite.findByFechaalta", query = "SELECT t FROM Tramite t WHERE t.fechaalta = :fechaalta"),
    @NamedQuery(name = "Tramite.findByFechabaja", query = "SELECT t FROM Tramite t WHERE t.fechabaja = :fechabaja"),
    @NamedQuery(name = "Tramite.findByEstatus", query = "SELECT t FROM Tramite t WHERE t.estatus = :estatus"),
    @NamedQuery(name = "Tramite.findByNombreCortotramite", query = "SELECT t FROM Tramite t WHERE t.nombreCortotramite = :nombreCortotramite"),
    @NamedQuery(name = "Tramite.findByDescripcionAcuerdo", query = "SELECT t FROM Tramite t WHERE t.descripcionAcuerdo = :descripcionAcuerdo")})
public class Tramite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE")
    private Integer idTramite;
    @Column(name = "ID_AREA")
    private String idArea;
    @Column(name = "ID_COFEMER")
    private String idCofemer;
    @Column(name = "COFEMER_CLAVE")
    private String cofemerClave;
    @Column(name = "LIGA_INTERNET")
    private String ligaInternet;
    @Column(name = "COLOR_TRAMITE")
    private Character colorTramite;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "FECHA_ACUERDO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAcuerdo;
    @Basic(optional = false)
    @Column(name = "FECHAALTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaalta;
    @Column(name = "FECHABAJA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechabaja;
    @Column(name = "ESTATUS")
    private BigInteger estatus;
    @Column(name = "NOMBRE_CORTOTRAMITE")
    private String nombreCortotramite;
    @Column(name = "DESCRIPCION_ACUERDO")
    private String descripcionAcuerdo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tramite1")
    private Collection<Modalidad> modalidadCollection;

    public Tramite() {
    }

    public Tramite(Integer idTramite) {
        this.idTramite = idTramite;
    }

    public Tramite(Integer idTramite, Date fechaalta) {
        this.idTramite = idTramite;
        this.fechaalta = fechaalta;
    }

    public Integer getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(Integer idTramite) {
        this.idTramite = idTramite;
    }

    public String getIdArea() {
        return idArea;
    }

    public void setIdArea(String idArea) {
        this.idArea = idArea;
    }

    public String getIdCofemer() {
        return idCofemer;
    }

    public void setIdCofemer(String idCofemer) {
        this.idCofemer = idCofemer;
    }

    public String getCofemerClave() {
        return cofemerClave;
    }

    public void setCofemerClave(String cofemerClave) {
        this.cofemerClave = cofemerClave;
    }

    public String getLigaInternet() {
        return ligaInternet;
    }

    public void setLigaInternet(String ligaInternet) {
        this.ligaInternet = ligaInternet;
    }

    public Character getColorTramite() {
        return colorTramite;
    }

    public void setColorTramite(Character colorTramite) {
        this.colorTramite = colorTramite;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaAcuerdo() {
        return fechaAcuerdo;
    }

    public void setFechaAcuerdo(Date fechaAcuerdo) {
        this.fechaAcuerdo = fechaAcuerdo;
    }

    public Date getFechaalta() {
        return fechaalta;
    }

    public void setFechaalta(Date fechaalta) {
        this.fechaalta = fechaalta;
    }

    public Date getFechabaja() {
        return fechabaja;
    }

    public void setFechabaja(Date fechabaja) {
        this.fechabaja = fechabaja;
    }

    public BigInteger getEstatus() {
        return estatus;
    }

    public void setEstatus(BigInteger estatus) {
        this.estatus = estatus;
    }

    public String getNombreCortotramite() {
        return nombreCortotramite;
    }

    public void setNombreCortotramite(String nombreCortotramite) {
        this.nombreCortotramite = nombreCortotramite;
    }

    public String getDescripcionAcuerdo() {
        return descripcionAcuerdo;
    }

    public void setDescripcionAcuerdo(String descripcionAcuerdo) {
        this.descripcionAcuerdo = descripcionAcuerdo;
    }

    @XmlTransient
    public Collection<Modalidad> getModalidadCollection() {
        return modalidadCollection;
    }

    public void setModalidadCollection(Collection<Modalidad> modalidadCollection) {
        this.modalidadCollection = modalidadCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTramite != null ? idTramite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tramite)) {
            return false;
        }
        Tramite other = (Tramite) object;
        if ((this.idTramite == null && other.idTramite != null) || (this.idTramite != null && !this.idTramite.equals(other.idTramite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.modelE.Tramite[ idTramite=" + idTramite + " ]";
    }
    
}
