package mx.gob.semarnat.secure;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import mx.gob.semarnat.model.seguridad.UsuarioRol;
import mx.gob.semarnat.utils.GenericConstants;

/***
 * Clase que toma informacion de roles ( de la sesion ) asignados al usuario actual y muestra informacion de dicha sesion en metodos estaticos
 * @author Paul Montoya
 *
 */
public class AppSession {

	/***
	 * Regresa la lista de roles como una lista de String a partir de la lista de UsuarioRol que trae en session
	 * @return regresa una lista de Strings de roles del tipo {"eva","sd","da","dg"}
	 */
	public static ArrayList<String> GetListaRoles(){
		
		ArrayList<String> lista = new ArrayList<String>();
		HttpSession session = Util.getSession();
        if (session != null) {

        @SuppressWarnings("unchecked")
		List<UsuarioRol> roles = (List<UsuarioRol>) session.getAttribute("roles");
        
        for (UsuarioRol rol : roles) {
            lista.add(rol.getRolId().getNombreCorto().toLowerCase());            
        }
        
        }
	    return lista;
	}

	
	@SuppressWarnings("unchecked")
	/***
	 * obtiene la lista de UsuarioRol de la session (segun los roles del usuario logueado)
	 * @return
	 */
	public static ArrayList<UsuarioRol> GetListaRolesObject(){
		
		ArrayList<UsuarioRol> lista = new ArrayList<UsuarioRol>();
		HttpSession session = Util.getSession();
        if (session != null) {
        	lista = (ArrayList<UsuarioRol>) session.getAttribute("roles");        
        }
	    return lista;
	}
	
	
	/***
	 * Devuelve solo el UsuarioRol que corresponde al rol indicado como string ( de la lista de roles del usuario logueado )
	 * @param rol
	 * @return
	 */
	public static UsuarioRol GetUsuarioRolPorRol(String rol){
		UsuarioRol ur = null;
		ArrayList<UsuarioRol> lista = GetListaRolesObject();
		for(UsuarioRol  obj : lista){
			
			if(obj.getRolId().getNombreCorto().trim().toLowerCase().equals(rol.trim().toLowerCase())){
				ur = obj;
				break;
			}
		}
		
		return ur;
	}
	
	/***
	 * obtener el rol actual con el que el usuario entró a ver la bitacora ( independientemente del rol de su area relacionada al usuario en la BD)
	 * @return
	 */
	public static String GetRolSeleccionado(){
	
		HttpSession session = Util.getSession();
        if (session != null) {
        	return session.getAttribute("rol").toString();
        }
        return "";
	}


	/***
	 * Regresa la lista de roles como una lista de String a partir de una lista de UsuarioRol
	 * @param roles
	 * @return
	 */
	@SuppressWarnings("unused")
	private static ArrayList<String> GetListaRoles(List<UsuarioRol> roles){
		
		ArrayList<String> lista = new ArrayList<String>();
		
        for (UsuarioRol rol : roles) {
            lista.add(rol.getRolId().getNombreCorto().toLowerCase());            
        }
        
        return lista;
	}

	/***
	 * consulta si el usuario tiene el rol de "DG" asignado entre sus roles administrados
	 * @return boolean true or false
	 */
	public static boolean contieneDG() {
		return GetListaRoles().contains("dg");
	}

	/***
	 * consulta si el usuario tiene el rol de "DA" asignado entre sus roles administrados
	 * @return boolean true or false
	 */
	public static boolean contieneDA() {
		return GetListaRoles().contains("da");
	}

    /***
     * consulta si el usuario tiene el rol de "SD" asignado entre sus roles administrados
     * @return boolean true or false
     */
	public static boolean contieneSD() {
		return GetListaRoles().contains("sd");
	}

	/***
	 * consulta si el usuario tiene el rol de "EVA" asignado entre sus roles administrados
	 * @return boolean true or false
	 */
	public static boolean contieneEVA() {
		return GetListaRoles().contains("eva");
	}

	
	/***
	 * Metodo que regresa el perfil siguiente ya sea a corregir o a turnar(firmar) dependiento el rol actual y la accion a realizar (2:corregir, 3:autorizar(firmar))
	 * @param rolActual indica un String con el rol actual con que esta interactuando el usuario ya sea "eva","sd","da","dg"
	 * @param statusDoc  2:corregir, 3:autorizar(firmar)
	 * @return
	 */
	public static String siguienteRol(String rolActual, int statusDoc){
		String sig = rolActual.toLowerCase();
		rolActual = rolActual.toLowerCase();
//		ArrayList<String> listaRoles = GetListaRoles();
//		
//		if(rolActual.equals("eva") || rolActual.equals("jd")){
//			
//			if(statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
//				if(listaRoles.contains("dg")){
//					sig = "dg";	
//				}else if(listaRoles.contains("da")){
//					sig = "da";
//				}else{
//					// default
//					sig = "sd";
//				}
//			}
//			
//		}else if(rolActual.equals("sd")){
//			
//			if(statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
//				if(listaRoles.contains("dg")){
//					sig = "dg";	
//				}else{
//					// default
//					sig = "da";
//				}
//			}else if(statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION){
//				
//				// default
//				sig = "eva";
//			}
//			
//		}else if(rolActual.equals("da")){
//			
//			if(statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
//				
//				// default
//				sig = "dg";
//				
//				
//			}else if(statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION){
//				
//				if(listaRoles.contains("eva")){
//					sig = "eva";
//				}else{
//					// default
//					sig = "sd";
//				}
//			}
//			
//		}else if(rolActual.equals("dg")){
//			
//			if(statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
//				
//				// default
//				sig = "dg";
//				
//			}else if(statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION){
//				
//				if(listaRoles.contains("eva")){
//					sig = "eva";
//				}else if(listaRoles.contains("sd")){
//					sig = "sd";
//				}else{
//					// detault				
//					sig = "da";
//				}
//			}
//			
//		}

		
		  if (rolActual.equals("dg")) {
		      //statusDoc
		      if (statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO) {
		          sig = "dg";
		      }
		      if (statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION) {
		          sig = "da";
		      }
		  }
		  else if (rolActual.equals("da")) {
		      //statusDoc
		      if (statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO) {
		          sig = "dg";
		      }
		      if (statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION) {
		          sig = "sd";
		      }
		  }
		  else if (rolActual.equals("sd")) {
		      //statusDoc
		      if (statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO) {
		          sig = "da";
		      }
		      if (statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION) {
		          sig = "eva";
		      }
		  }
		  else if (rolActual.equals("jd") || rolActual.equals("eva")) {
		      //statusDoc
		      if (statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO) {
		          sig = "sd";
		      }
		  }else{
			  sig = rolActual;
		  }
		  
		return sig;
	}
	
	
//	/***
//	 * Metodo que regresa el perfil siguiente ya sea a corregir o a turnar(firmar) dependiento el rol actual y la accion a realizar (2:corregir, 3:autorizar(firmar))
//	 * @param rolActual indica un String con el rol actual con que esta interactuando el usuario ya sea "eva","sd","da","dg"
//	 * @param statusDoc 2:corregir, 3:autorizar(firmar)
//	 * @param roles lista de UsuarioRol
//	 * @return
//	 */
//	public static String siguienteRol(String rolActual, int statusDoc, List<UsuarioRol> roles){
//		String sig = rolActual.toLowerCase();
//		rolActual = rolActual.toLowerCase();
//		ArrayList<String> listaRoles = GetListaRoles(roles);
//		
//		if(rolActual.equals("eva") || rolActual.equals("jd")){
//			
//			if(listaRoles.contains("dg")){
//				sig = "dg";	
//			}else if(listaRoles.contains("da")){
//				sig = "da";
//			}else{
//				// default
//				sig = "sd";
//			}
//			
//		}else if(rolActual.equals("sd")){
//			
//			if(statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
//				if(listaRoles.contains("dg")){
//					sig = "dg";	
//				}else{
//					// default
//					sig = "da";
//				}
//			}else if(statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION){
//				
//				// default
//				sig = "eva";
//			}
//			
//		}else if(rolActual.equals("da")){
//			
//			if(statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
//				
//				// default
//				sig = "dg";
//				
//				
//			}else if(statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION){
//				
//				if(listaRoles.contains("eva")){
//					sig = "eva";
//				}else{
//					// default
//					sig = "sd";
//				}
//			}
//			
//		}else if(rolActual.equals("dg")){
//			
//			if(statusDoc == GenericConstants.ESTATUS_DOCTO_AUTORIZADO){
//				
//				// default
//				sig = "dg";
//				
//			}else if(statusDoc == GenericConstants.ESTATUS_DOCTO_CORRECCION){
//				
//				if(listaRoles.contains("eva")){
//					sig = "eva";
//				}else if(listaRoles.contains("sd")){
//					sig = "sd";
//				}else{
//					// detault				
//					sig = "da";
//				}
//			}
//			
//		}
//		return sig;
//	}
	
	
}
