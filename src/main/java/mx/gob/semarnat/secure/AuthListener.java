/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.secure;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Rodrigo
 */
@SuppressWarnings("serial")
public class AuthListener implements PhaseListener {

    /**
     *
     * @param event
     */
    @Override
    public void afterPhase(PhaseEvent event) {
        FacesContext facesContext = event.getFacesContext();
        String currentPage = facesContext.getViewRoot() != null ? facesContext.getViewRoot().getViewId() : null;

        boolean isLoginPage = currentPage != null ? (currentPage.lastIndexOf("login.xhtml") > -1) : false;
        HttpSession session = (HttpSession) facesContext.getExternalContext()
                .getSession(false);

        if (session == null) {
            if (!isLoginPage) {
                NavigationHandler nh = facesContext.getApplication()
                        .getNavigationHandler();
                nh.handleNavigation(facesContext, null, "./login.xhtml");
            }
        } else {
            String redir = "";
            Object rol = session.getAttribute("rol");
            if (rol != null) {
                if (!isLoginPage && (rol == null || rol == "")) {
                    NavigationHandler nh = facesContext.getApplication()
                            .getNavigationHandler();
                    nh.handleNavigation(facesContext, null, "./login.xhtml");
                } else {
                    String rolString = rol.toString();
                    if (rolString.length() > 0) {
                        if (rolString.contains("ecc")) {
                            redir = "./Restringido/ecc/pantallaPrincipal.xhtml";
                        }
                        if (rolString.contains("da")) {
                            redir = "./Restringido/directorArea/pantallaPrincipal.xhtml";
                        }
                        if (rolString.contains("sd")) {
                            redir = "./Restringido/subdirectorSubsector/pantallaPrincipal.xhtml";
                        }
                        if (rolString.contains("eva")) {
                            redir = "./Restringido/evaluador/pantallaPrincipal.xhtml";
                        }
                        if (rolString.contains("dg")) {
                            redir = "./Restringido/directorGeneral/Principal.xhtml";
                        }
                    } else {
                        redir = "./login.xhtml";
                    }
                    NavigationHandler nh = facesContext.getApplication()
                            .getNavigationHandler();
                    nh.handleNavigation(facesContext, null, redir);
                }
            }
        }
    }

    /**
     *
     * @param event
     */
    @Override
    public void beforePhase(PhaseEvent event) {
    }

    /**
     *
     * @return
     */
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

}
