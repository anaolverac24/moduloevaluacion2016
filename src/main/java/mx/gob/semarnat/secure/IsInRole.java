/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.secure;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Rodrigo
 */
@SuppressWarnings("serial")
@ManagedBean(name = "isInRole")
@SessionScoped
public class IsInRole implements Serializable {

    private Boolean directorGeneral;
    private Boolean directorArea;
    private Boolean subDirector;
    private Boolean evaluador;
   
	public IsInRole() {
		System.out.println("\nIsInRole...");
        HttpSession sesion = Util.getSession();
        if (sesion != null) {
            String rol = sesion.getAttribute("rol").toString();            
            directorGeneral = rol.equals("dg");
            directorArea = rol.equals("da");
            subDirector = rol.equals("sd");
            evaluador = rol.equals("eva");
            
//            ArrayList<String> listaRoles = (ArrayList<String>) sesion.getAttribute("roles");
//            directorGeneral = listaRoles.contains("dg");
//            directorArea = listaRoles.contains("da");
//            subDirector = listaRoles.contains("sd");
//            evaluador = listaRoles.contains("eva");
            
            System.out.println("directorGeneral : " + directorGeneral);
            System.out.println("directorArea : " + directorArea);
            System.out.println("subDirector : " + subDirector);
            System.out.println("evaluador : " + evaluador);            
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getter & setter">
    public Boolean getDirectorGeneral() {
        return directorGeneral;
    }

    public void setDirectorGeneral(Boolean directorGeneral) {
        this.directorGeneral = directorGeneral;
    }

    public Boolean getDirectorArea() {
        return directorArea;
    }

    public void setDirectorArea(Boolean directorArea) {
        this.directorArea = directorArea;
    }

    public Boolean getSubDirector() {
        return subDirector;
    }

    public void setSubDirector(Boolean subDirector) {
        this.subDirector = subDirector;
    }

    public Boolean getEvaluador() {
        return evaluador;
    }

    public void setEvaluador(Boolean evaluador) {
        this.evaluador = evaluador;
    }
    
    public boolean isVisor() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String currentPage = facesContext.getViewRoot().getViewId();
        return currentPage.contains("Cap");
    }
    
    public boolean isDetalle() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String currentPage = facesContext.getViewRoot().getViewId();
        return currentPage.contains("Inside") && !currentPage.contains("Cap");
    }
    
//</editor-fold>

}
