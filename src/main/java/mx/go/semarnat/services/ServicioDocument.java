package mx.go.semarnat.services;


import java.util.ArrayList;
import java.util.List;
import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.Document;

public class ServicioDocument extends GenericDaoMia<Document, Long> {

    //@SuppressWarnings("unused")
    public Document getDocByIdByType(String id, String bitacora) throws Exception{
            Long lID = Long.parseLong(id);
            List<Document> documents = null;
            Document doc = null;
            try{
                    documents = this.findByNamedQuery("Document.findByIdAndType", lID, bitacora);
                    if(documents != null && !documents.isEmpty())
                            doc = documents.get(0);
                    else{
                            doc = new Document();
                            doc.id = 0L;
                    }
            }catch(MiaExcepcion e){
                    e.printStackTrace();
                    throw new MiaExcepcion("ServicioDocument.errorConsultar");
            }
            return doc;
    }

    public Document getDocByIdByTypeByWeb(String id, String bitacora, int documentWeb) throws Exception{
        Long lID = Long.parseLong(id);
        List<Document> documents = null;
        Document doc = null;
        try {
            documents = this.findByNamedQuery("Document.findByIdAndTypeAndWeb", lID, bitacora, documentWeb);
            if (documents != null && !documents.isEmpty()) {
                doc = documents.get(0);
            } else {
                doc = new Document();
                doc.id = 0L;
            }
        } catch (MiaExcepcion e) {
            e.printStackTrace();
            throw new MiaExcepcion("ServicioDocument.errorConsultar");
        }
        return doc;
    }
    
    //@SuppressWarnings("unused")
    public List<Document> getDocPrevencion(String bitacora) throws Exception{
        List<Document> documents = new ArrayList<>();
        try{
            documents = this.findByNamedQuery("Document.findByIdAndPrevencion", bitacora);
        }catch(MiaExcepcion e){
            e.printStackTrace();
            throw new MiaExcepcion("ServicioDocument.errorConsultar");
        }
        return documents;
    }
    
}
