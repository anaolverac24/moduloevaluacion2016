package mx.go.semarnat.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gob.semarnat.mia.servicios.GenericDaoMia;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.dgira_miae.NotificacionGobiernoProyecto;

public class ServicioUnidadAdministrativa extends GenericDaoMia<UnidadAdministrativa, Long> {

    public UnidadAdministrativa getUnidad(String name) {
        try {
            return (UnidadAdministrativa) this.findByNamedQuery("findByName", name);
        } catch (MiaExcepcion e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UnidadAdministrativa> getAll() throws MiaExcepcion {
        List<UnidadAdministrativa> lista = null;
        try {
            lista = this.findByNamedQuery("UnidadAdministrativa.findAllActive");
        } catch (MiaExcepcion e) {
            e.printStackTrace();
            throw new MiaExcepcion("ServicioProyecto.errorConsultar");
        }
        return lista;
    }

    public UnidadAdministrativa getUnidadesById(Long id) throws MiaExcepcion {
        List<UnidadAdministrativa> lista = null;
        UnidadAdministrativa unidadAdministrativa = null;
        try {
            lista = this.findByNamedQuery("UnidadAdministrativa.findById", id);
            if (lista != null && lista.size() > 0) {
                unidadAdministrativa = lista.get(0);
            }
        } catch (MiaExcepcion e) {
            e.printStackTrace();
            throw new MiaExcepcion("ServicioProyecto.errorConsultar");
        }
        return unidadAdministrativa;
    }

    public UnidadAdministrativa saveUnidad(UnidadAdministrativa unidad) throws MiaExcepcion {
        UnidadAdministrativa unidadNva = null;
        try {
            unidadNva = this.save(unidad);
        } catch (MiaExcepcion e) {
            e.printStackTrace();
            throw new MiaExcepcion("ServicioProyecto.errorConsultar");
        }
        return unidadNva;
    }

    public boolean saveNativeUnidad(UnidadAdministrativa unidad) throws MiaExcepcion {
        String q = "INSERT INTO CAT_UNIDAD_ADMINISTRATIVA ( "
                + " ID, NOMBRE_DE_LA_DEPENCENCIA, NOMBRE_CORTO, DOMICILIO, ID_MPIO, OPINION_TECNICA, ESTATUS, ID_ESTADO) "
                + "VALUES (" + unidad.getId() + ",'" + unidad.getNombreDependencia() + "','" + unidad.getNombreCorto() + "','" + unidad.getDomicilio()
                + "','" + unidad.getMunicipioId() + "'," + unidad.getOpinionTec() + ",'" + unidad.getEstado() + "','" + unidad.getEstadoId() + "')";
        System.out.println(q);

        try {
            int rs = updateNativeQuery(q);
            return rs > 0;
        } catch (MiaExcepcion e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new MiaExcepcion("ServicioProyecto.errorConsultar");
        }
    }

    public boolean updateUnidad(UnidadAdministrativa unidad) throws MiaExcepcion {
        String q = "UPDATE CAT_UNIDAD_ADMINISTRATIVA SET "
                + "NOMBRE_DE_LA_DEPENCENCIA = '" + unidad.getNombreDependencia() + "', "
                + "NOMBRE_CORTO = '" + unidad.getNombreCorto() + "', "
                + "DOMICILIO = '" + unidad.getDomicilio() + "', "
                + "ID_MPIO = '" + unidad.getMunicipioId() + "', "
                + "OPINION_TECNICA = " + unidad.getOpinionTec() + ", "
                + "ESTATUS = '" + unidad.getEstado() + "', "
                + "ID_ESTADO = '" + unidad.getEstadoId() + "' "
                + "WHERE ID = " + unidad.getId();

        System.out.println(q);

        try {
            int rs = updateNativeQuery(q);
            return rs > 0;
        } catch (MiaExcepcion e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new MiaExcepcion("ServicioProyecto.errorConsultar");
        }
    }

    public void deleteUnidad(UnidadAdministrativa unidad) throws MiaExcepcion {
        this.delete(unidad.getId());
    }

    public List<UnidadAdministrativa> buscarEntidadesOpinionTecnica(String bitacoraProyecto) {
        String sql = "SELECT S.ID AS IDT, S.NOMBRE_DE_LA_DEPENCENCIA AS DEPENDENCIA, S.CARGO, S.NOMBRE_DESTINATARIO AS NOMBRE "
                + "FROM DGIRA_MIAE2.OPINION_TECNICA_PROYECTO T "
                + "INNER JOIN DGIRA_MIAE2.CAT_UNIDAD_ADMINISTRATIVA S ON S.ID = T.CAT_DEPENDENCIA_ID "
                + "WHERE T.BITACORA_PROYECTO = '" + bitacoraProyecto + "' ";

        System.out.println(sql);
        List<Object[]> objecList = this.getEntityManager().createNativeQuery(sql).getResultList();
        ArrayList<UnidadAdministrativa> listUnidadAdministrativa = new ArrayList<UnidadAdministrativa>();
        for (Object[] objects : objecList) {
            UnidadAdministrativa unidad = new UnidadAdministrativa();
            unidad.setId(Long.valueOf(objects[0].toString()));
            unidad.setNombreDependencia(objects[1].toString());
            unidad.setCargo(objects[2].toString());
            unidad.setNombbreDestinatario(objects[3].toString());
            listUnidadAdministrativa.add(unidad);
        }
        return listUnidadAdministrativa;
    }

    public List<UnidadAdministrativa> buscarEntidadesOpinionTecnica() {
        List<UnidadAdministrativa> listUnidadAdministrativa = new ArrayList<UnidadAdministrativa>();
        try {
            listUnidadAdministrativa = this.findByNamedQuery("UnidadAdministrativa.findUnidadesTecnicas");
            
        } catch (MiaExcepcion ex) {
            Logger.getLogger(ServicioUnidadAdministrativa.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listUnidadAdministrativa;
    }
    
    public List<UnidadAdministrativa> buscarEntidadesNotiicacionGobierno(String bitacoraProyecto) {
        String sql = "SELECT DISTINCT UA.ID, UA.NOMBRE_DE_LA_DEPENCENCIA AS DEPENDENCIA, UA.CARGO, UA.NOMBRE_DESTINATARIO AS NOMBRE_DEL_TITULAR, ef.id_entidad_federativa,  dm.id_delegacion_municipio, ef.nombre_entidad_federativa||' - '||dm.nombre_delegacion_municipio " +
                    "FROM MPIOS_CRUZADAVSHAMBRE M " +
                    "INNER JOIN PROYECTO P ON P.FOLIO_PROYECTO = M.NUM_FOLIO " +
                    "INNER JOIN CATALOGOS.DELEGACION_MUNICIPIO DM ON DM.ID_DELEGACION_MUNICIPIO = M.CLV_MUNICI " +
                    "INNER JOIN CATALOGOS.ENTIDAD_FEDERATIVA EF ON ef.id_entidad_federativa = DM.ENTIDAD_FEDERATIVA " +
                    "LEFT JOIN cat_unidad_administrativa UA ON UA.ID_MPIO = DM.ID_DELEGACION_MUNICIPIO " +
                    "WHERE P.BITACORA_PROYECTO = '" + bitacoraProyecto  + "' " +
                    "AND M.COMP = 'OBRA' " +
                    "AND M.NOM_MUNICI is not null " + 
                    "UNION " +
                    "SELECT DISTINCT UA.ID, UA.NOMBRE_DE_LA_DEPENCENCIA AS DEPENDENCIA, UA.CARGO, UA.NOMBRE_DESTINATARIO AS NOMBRE_DEL_TITULAR, ef.id_entidad_federativa,  UA.ID_MPIO, ef.nombre_entidad_federativa||' -'||' ' " +
                    "FROM MPIOS_CRUZADAVSHAMBRE M " +
                    "INNER JOIN PROYECTO P ON P.FOLIO_PROYECTO = M.NUM_FOLIO " +
                    "INNER JOIN CATALOGOS.ENTIDAD_FEDERATIVA EF ON ef.NOMBRE_ENTIDAD_FEDERATIVA = M.ESTADO " +
                    "INNER JOIN cat_unidad_administrativa UA ON UA.ID_ESTADO = EF.ID_ENTIDAD_FEDERATIVA " +
                    "WHERE P.BITACORA_PROYECTO = '" + bitacoraProyecto + "' " +
                    "AND M.COMP = 'OBRA' " +
                    "AND M.NOM_MUNICI is not NULL " +
                    "AND UA.OPINION_TECNICA = 2 ";

        System.out.println(sql);
        List<Object[]> objecList = this.getEntityManager().createNativeQuery(sql).getResultList();
        ArrayList<UnidadAdministrativa> listUnidadAdministrativa = new ArrayList<UnidadAdministrativa>();
        for (Object[] objects : objecList) {
            UnidadAdministrativa unidad = new UnidadAdministrativa();
            if (Objects.equals(objects[0], null)) {
                unidad.setId(0L);
            } else {
                unidad.setId(Long.valueOf(objects[0].toString()));
            }
            if (Objects.equals(objects[1], null)) {               
                unidad.setNombreDependencia("");
            } else {
                unidad.setNombreDependencia(objects[1].toString());
            }
            if (Objects.equals(objects[2], null)) {
                unidad.setCargo("");
            } else {
                unidad.setCargo(objects[2].toString());
            }
            if (Objects.equals(objects[3], null)) {
                unidad.setNombbreDestinatario("");
            } else {
                unidad.setNombbreDestinatario(objects[3].toString());
            }
            unidad.setEstadoId(objects[4].toString());
            unidad.setMunicipioId(objects[5].toString());
            unidad.setFundamentoLegalInterno(objects[6].toString());
            listUnidadAdministrativa.add(unidad);
        }
        return listUnidadAdministrativa;
    }
    
    public void cargarNotificacionGobiernoProyecto (List<UnidadAdministrativa> listUnidadAdmin, List<NotificacionGobiernoProyecto> listNotificacion, String bitacoraProyecto) throws MiaExcepcion{
        if (listNotificacion.size() < listUnidadAdmin.size()) {
            for (int i = 1; i <= listUnidadAdmin.size(); i++) {
                String q = "INSERT INTO NOTIFICACION_GOBIERNO_PROYECTO ( "
                + " BITACORA_PROYECTO, NOTIFICACIONG_ID) "
                + "VALUES ('" + bitacoraProyecto + "', " + i + ")";
                System.out.println(q);

                try {
                    int rs = updateNativeQuery(q);
                } catch (MiaExcepcion e) {
                    // TODO Auto-generated catch block
                }
            }   
                                       
        }
        
    }
}
