package mx.go.semarnat.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import mx.gob.semarnat.dao.DgiraMiaeDaoGeneral;
import mx.gob.semarnat.mia.servicios.MiaExcepcion;
import mx.gob.semarnat.model.catalogos.CatTratamientos;
import mx.gob.semarnat.model.catalogos.DelegacionMunicipio;
import mx.gob.semarnat.model.catalogos.EntidadFederativa;

@SuppressWarnings("serial")
@ManagedBean(name = "unidadBean")
@ViewScoped
public class UnidadesAdministrativasController implements Serializable{
	
	private List<UnidadAdministrativa> unidades = new ArrayList<>();
        
	private UnidadAdministrativa unidadSeleccionado = new UnidadAdministrativa();

	private String municipioId;
	private String estadoId;
	private String tratamiento;
	private String tratamientos;
	boolean seleccionEdicion;
	private List<EntidadFederativa> listEstados;
	private List<DelegacionMunicipio> listMunicipios;
	private List<CatTratamientos> listTratamientos;
	

	private List<SelectItem> itemsEstados;
	private List<SelectItem> itemsMunicipios;
	private List<SelectItem> itemsTratamientos;
        private List<SelectItem> itemsUnidadesAdministrativas;
	private int opinionTec;
			

	//servicios
	private ServicioUnidadAdministrativa sua;
	
	public UnidadesAdministrativasController(){
            inicializarBean();
	}

	private void inicializarBean() {		
            try {
                sua = new ServicioUnidadAdministrativa();
                unidades = sua.getAll();			
			
                DgiraMiaeDaoGeneral miaeDao = new DgiraMiaeDaoGeneral();
                listEstados = miaeDao.getEstados();	
                itemsEstados = new ArrayList<>();
                for (EntidadFederativa entidadFederativa : listEstados) {
                    SelectItem name = new SelectItem(entidadFederativa.getIdEntidadFederativa(), entidadFederativa.getNombreEntidadFederativa());
                    itemsEstados.add(name);
                }
                listTratamientos = miaeDao.getTratamientos();
                itemsTratamientos = new ArrayList<>();
                for (CatTratamientos tratamientos : listTratamientos) {
                    SelectItem name = new SelectItem(tratamientos.getId(), tratamientos.getDegree());
                    itemsTratamientos.add(name);
                }
                itemsUnidadesAdministrativas = new ArrayList<>();
                itemsUnidadesAdministrativas.add(new SelectItem(1, "Opinión Técnica"));
                itemsUnidadesAdministrativas.add(new SelectItem(2, "Gobierno Estatal"));
                itemsUnidadesAdministrativas.add(new SelectItem(3, "Gobierno Municipal"));
                itemsUnidadesAdministrativas.add(new SelectItem(0, "Unidad Administrativa"));
            } catch (MiaExcepcion e1) {			
                e1.printStackTrace();
            }
	}
	
	public void creaUnidadAdministrativa() {
		unidadSeleccionado = new UnidadAdministrativa();
	}
	
	public void validarDatos() {
            if (unidadSeleccionado.getNombreDependencia().equals(null) || unidadSeleccionado.getNombreCorto().equals(null) ||
                            unidadSeleccionado.getDomicilio().equals(null) || unidadSeleccionado.getNombbreDestinatario().equals(null) ||
                            unidadSeleccionado.getCargo().equals(null) || unidadSeleccionado.getTelefono().equals(null) ||
                            unidadSeleccionado.getCorreo().equals(null)){
    //    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Llena todos los campos"));
                    System.out.print("faltan datos");
            } else {
                    System.out.print("guardado");
                    agregarRegistro();   		   		
            }
        }
	
	public void agregarRegistro(){
            try {
                sua = new ServicioUnidadAdministrativa();
                System.out.println("unidadSeleccionado ----->>>" + unidadSeleccionado);			
                unidadSeleccionado.setEstado("activo");

                unidadSeleccionado.setOpinionTec(opinionTec);
                sua.saveUnidad(unidadSeleccionado);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Exito",  "se guardo exitosamente el registro") );
                RequestContext.getCurrentInstance().execute("PF('dlgAddUpdate').hide()");
                unidadSeleccionado = new UnidadAdministrativa();
                unidades = sua.getAll();	        
            } catch (MiaExcepcion e) {			
                    e.printStackTrace();
            }
	}
	
	public void cargaUnidadAdministrativa(UnidadAdministrativa ua){
            System.out.println(":::::::::::::::::::::::cargaUnidadAdministrativa:::::::::::::::::::::::::::::::");
            sua = new ServicioUnidadAdministrativa();
            try {
                unidadSeleccionado = sua.findById(ua.getId());
                opinionTec = unidadSeleccionado.getOpinionTec();
                onEstadoChange();
            } catch (MiaExcepcion e) {
                e.printStackTrace();
            }
            System.out.println(" id cargar ------>> " + unidadSeleccionado.getEstadoId());			
	}
	
	public void eliminar(UnidadAdministrativa ua){		
            try {
                sua = new ServicioUnidadAdministrativa();
                ua.setEstado("baja");
                sua.updateUnidad(ua);
                unidades = sua.getAll();	
            } catch (MiaExcepcion e) {
                e.printStackTrace();
            }
	}

	public void onEstadoChange() {
            DgiraMiaeDaoGeneral miaeDao = new DgiraMiaeDaoGeneral();
            System.out.println("estado seleccionado :::: " + unidadSeleccionado.getEstadoId());
            listMunicipios = miaeDao.getMunicipios(unidadSeleccionado.getEstadoId());
            itemsMunicipios = new ArrayList<>();
            for (DelegacionMunicipio municipio : listMunicipios) {
                SelectItem select = new SelectItem(municipio.getIdDelegacionMunicipio(), municipio.getNombreDelegacionMunicipio());
                itemsMunicipios.add(select);
            }
            System.out.println("Municipios :::::::::::::::::::::: " + itemsMunicipios);
	}
	
	public List<EntidadFederativa> getListEstados() {
            return listEstados;
	}

	public void setListEstados(List<EntidadFederativa> listEstados) {
            this.listEstados = listEstados;
	}
	
	public List<UnidadAdministrativa> getUnidades() {
            return unidades;
	}

	public void setUnidades(List<UnidadAdministrativa> unidades) {
            this.unidades = unidades;
	}

	public UnidadAdministrativa getUnidadSeleccionado() {
            return unidadSeleccionado;
	}

	public void setUnidadSeleccionado(UnidadAdministrativa unidadSeleccionado) {
            this.unidadSeleccionado = unidadSeleccionado;
	}
	
	public String getTratamiento() {
            return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
            this.tratamiento = tratamiento;
	}

	public String getTratamientos() {
            return tratamientos;
	}

	public void setTratamientos(String tratamientos) {
            this.tratamientos = tratamientos;
	}

	public String getMunicipioId() {
            return municipioId;
	}

	public void setMunicipioId(String municipioId) {
            this.municipioId = municipioId;
	}

	public String getEstadoId() {
            return estadoId;
	}

	public void setEstadoId(String estadoId) {
            this.estadoId = estadoId;
	}

	
	public List<DelegacionMunicipio> getListMunicipios() {
            return listMunicipios;
	}

	public void setListMunicipios(List<DelegacionMunicipio> listMunicipios) {
            this.listMunicipios = listMunicipios;
	}
	
	public List<SelectItem> getItemsMunicipios() {
            return itemsMunicipios;
	}

	public void setItemsMunicipios(List<SelectItem> itemsMunicipios) {
            this.itemsMunicipios = itemsMunicipios;
	}

	public List<SelectItem> getItemsEstados() {
            return itemsEstados;
	}

	public void setItemsEstados(List<SelectItem> itemsEstados) {
            this.itemsEstados = itemsEstados;
	}

	public int getOpinionTec() {
            return opinionTec;
	}

	public void setOpinionTec(int opinionTec) {
            this.opinionTec = opinionTec;
	}
	
	public List<CatTratamientos> getListTratamientos() {
            return listTratamientos;
	}

	public void setListTratamientos(List<CatTratamientos> listTratamientos) {
            this.listTratamientos = listTratamientos;
	}
	
	public List<SelectItem> getItemsTratamientos() {
            return itemsTratamientos;
	}

	public void setItemsTratamientos(List<SelectItem> itemsTratamientos) {
            this.itemsTratamientos = itemsTratamientos;
	}

        /**
         * @return the itemsUnidadesAdministrativas
         */
        public List<SelectItem> getItemsUnidadesAdministrativas() {
            return itemsUnidadesAdministrativas;
        }

        /**
         * @param itemsUnidadesAdministrativas the itemsUnidadesAdministrativas to set
         */
        public void setItemsUnidadesAdministrativas(List<SelectItem> itemsUnidadesAdministrativas) {
            this.itemsUnidadesAdministrativas = itemsUnidadesAdministrativas;
        }
}
