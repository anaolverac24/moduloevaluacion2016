package mx.go.semarnat.services;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CAT_UNIDAD_ADMINISTRATIVA")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "UnidadAdministrativa.findAllActive", query = "SELECT u FROM UnidadAdministrativa u WHERE u.estatus = 'activo' order by u.id"),
                @NamedQuery(name = "UnidadAdministrativa.findById", query = "SELECT u FROM UnidadAdministrativa u WHERE u.id = ?1"),
                @NamedQuery(name = "UnidadAdministrativa.findByName", query = "SELECT u FROM UnidadAdministrativa u WHERE u.nombreDependencia = ?1"),
                @NamedQuery(name = "UnidadAdministrativa.findUnidadesTecnicas", query = "SELECT u FROM UnidadAdministrativa u WHERE u.opinionTec = '1' AND u.estatus = 'activo' order by u.nombreCorto asc")
})

public class UnidadAdministrativa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SEQ_CAT_UNIDAD_ADMINISTRATIVA", sequenceName = "SEQ_CAT_UNIDAD_ADMINISTRATIVA", allocationSize = 1)
	@GeneratedValue(generator = "SEQ_CAT_UNIDAD_ADMINISTRATIVA", strategy = GenerationType.SEQUENCE)
	private long id;
	@Column(name = "NOMBRE_DE_LA_DEPENCENCIA")
	private String nombreDependencia;
	@Column(name = "NOMBRE_CORTO")
	private String nombreCorto;
	@Column(name = "DOMICILIO")
	private String domicilio;
	@Column(name = "ID_MPIO")
	private String municipioId;
	@Column(name = "OPINION_TECNICA")
	private int opinionTec;
	@Column(name = "ESTATUS")
	private String estatus;
	@Column(name = "ID_ESTADO")
	private String estadoId;
	@Column(name = "ID_DESTINATARIO")
	private String destinatarioId;
	@Column(name = "TRATAMIENTO")
	private String tratamiento;
	@Column(name = "NOMBRE_DESTINATARIO")
	private String nombbreDestinatario;
	@Column(name = "CARGO")
	private String cargo;
	@Column(name = "TELEFONO")
	private String telefono;
	@Column(name = "CORREO")
	private String correo;
	@Column(name = "fundamento_legal_interno")
	private String fundamentoLegalInterno;
        @Transient
        private boolean oficio;
	
	public UnidadAdministrativa() {
		super();
	}


	public UnidadAdministrativa(long id, String nombreDependencia, String nombreCorto, String domicilio, String municipioId, boolean opinionTec, String estado, String estadoId, String destinatarioId,
			String tratamiento, String nombbreDestinatario, String cargo, String telefono, String correo, String fundamentoLegalInterno) {
		super();
		this.id = id;
		this.nombreDependencia = nombreDependencia;
		this.nombreCorto = nombreCorto;
		this.domicilio = domicilio;
		this.municipioId = municipioId;
		int intVal = 0;
		if (opinionTec == true) {
			intVal = 1;
		}
		this.opinionTec = intVal;
		this.estatus = estado;
		this.estadoId = estadoId;
		this.destinatarioId = destinatarioId;
		this.tratamiento = tratamiento;
		this.nombbreDestinatario = nombbreDestinatario;
		this.cargo = cargo;
		this.telefono = telefono;
		this.correo = correo;
		this.fundamentoLegalInterno = fundamentoLegalInterno;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombreDependencia() {
		return nombreDependencia;
	}

	public void setNombreDependencia(String nombreDependencia) {
		this.nombreDependencia = nombreDependencia;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}

	public int getOpinionTec() {
		return opinionTec;
	}

	public void setOpinionTec(int opinionTec) {		
		this.opinionTec = opinionTec;
	}

	public String getEstado() {
		return estatus;
	}

	public void setEstado(String estado) {
		this.estatus = estado;
	}

	public String getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}


	public String getDestinatarioId() {
		return destinatarioId;
	}


	public void setDestinatarioId(String destinatarioId) {
		this.destinatarioId = destinatarioId;
	}


	public String getTratamiento() {
		return tratamiento;
	}


	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}


	public String getNombbreDestinatario() {
		return nombbreDestinatario;
	}


	public void setNombbreDestinatario(String nombbreDestinatario) {
		this.nombbreDestinatario = nombbreDestinatario;
	}


	public String getCargo() {
		return cargo;
	}


	public void setCargo(String cargo) {
		this.cargo = cargo;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getCorreo() {
		return correo;
	}


	public void setCorreo(String correo) {
		this.correo = correo;
	}


	public String getFundamentoLegalInterno() {
		return fundamentoLegalInterno;
	}


	public void setFundamentoLegalInterno(String fundamentoLegalInterno) {
		this.fundamentoLegalInterno = fundamentoLegalInterno;
	}

    /**
     * @return the oficio
     */
    public boolean isOficio() {
        return oficio;
    }

    /**
     * @param oficio the oficio to set
     */
    public void setOficio(boolean oficio) {
        this.oficio = oficio;
    }

}
